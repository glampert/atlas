
## Naming conventions
----

### User Defined Types:

User Defined Type (UDT) names, such as classes, structs, enums
and typedefs, should use `PascalCase` notation. Examples:

    FileReader
    HttpHeader
    XmlWriter
    IdString
    AssetDb
    AsciiTable
    Model3d

Notice that names containing multi-letter abbreviations or initialisms,
such as `HttpHeader`, only employ the first letter of the abbreviation as uppercase:

    class HttpHeader; // Good
    class HTTPHeader; // Incorrect

Typedefs for native types (or any typedef for that matter) should follow the same notation.
There is an exception for types that are used very frequently, such as unsigned integers,
bytes and unsigned longs. Since those types are used very often, there is some gain
in reducing the number of keystrokes required to declare a variable. For that reason,
the following atlas library typedefs:

    int8
    uint8
    int16
    uint16
    int32
    uint32
    int64
    uint64
    byte
    ubyte
    uint
    ulong

Use all lowercase, thus breaking the `PascalCase` rule for types in favor
of avoiding a `[SHIFT]` key press whenever you need to declare a native integer.

Another minor suggestion regarding typedefs, when related to defining a function signature
typedef or a typedef to a functor object, we advise adding the `Func` or `Function` words
to the type name, to make it clearer about its purpose. Example:

    // Signature of a generic hash function.
    typedef uint32 (* HashFuncType)(const void *, uint);

### Variables and compile-time constants:

Variable names are always `camelCase`, independent of scope or purpose. Examples:

    int count;        // Single word, good
	int appleCount;   // Multi-word camelCase, good
	int apple_count:  // Incorrect
	int _apple_count; // Wrong, wrong, wrong!

Variable, type or method names containing underscores (`_`) are forbidden. Underscores are only
used on macro names and template parameters. Names starting with an underscore (including macros
or template params) are not legal C++ names in some contexts, so these are expressly forbidden.

Non-macro compile-time constants are always declared using `PascalCase`, following the same
rules applied to type names. Examples:

    enum VarType
    {
        String,
        Integer,
        Decimal
    };

    const int MaxConnections = 10;
    constexpr int Cpp11Const = 2011;

**Note:** Also remember to favor C++11's `constexpr` whenever practical. If compiling to a target
where C++11 is not available, it is safe to redefine `constexpr` to a plain `const`. However, this
will not work for `constexpr` functions that are used as constant expressions, so `constexpr`
usage on functions should be avoided for this reason. If you do use it on a function, then make
sure not to call the function where a constant expression would otherwise be required.

### Macros and template parameters:

Macro constants and function-like macros should be avoided as much as possible.
If used, they follow the universal macro notation of `ALL_UPPERCASE`. Examples:

    #define LOG_ERROR(errorMessage) logErr((errorMessage), __FILE__, __LINE__)

    #ifndef A_BUILD_SYSTEM_CONST
        #define A_BUILD_SYSTEM_CONST 42
    #endif // A_BUILD_SYSTEM_CONST

The only other category of names that is allowed to use `ALL_UPPERCASE` are template parameter
names. Ideally, you should keep template parameter names short. Single letter parameters,
like the popular `T` for a type, are the ideal. If you need a longer or possibly multi-word
parameter name, then use the `ALL_UPPERCASE` notation.

### Namespaces:

C++ namespace names are always lowercase. Also try to avoid long namespace names. Prefer single word ones.

    namespace engine { } // Good
    namespace Engine { } // Incorrect

For our purposes, a namespace is the equivalent of a module. Nesting namespaces is fine,
but some care should be taken to avoid excessively long namespace nesting. Two to three
levels of nesting is our unofficial limit, such as in `atlas::core::memory`, for example.

### Methods and functions:

Method names or free function names are always `camelCase` and follow the same rules
applied to type names regarding acronyms and initialisms. Examples:

	bool initSystem();
    void loadGame();
    void saveGame();
    int getModel3dCount() const;

**Note:** The rationale behind using `camelCase` names for both variables and functions/methods
is that all C++ entities that can have its memory address taken should share the same naming style.

Lightweight get/set methods should always be prefixed accordingly:

    int getElementCount() const;
	void setElementCount(int count);

**Note:** When the variables being get or set are related to a count or number
of items, prefer a name ending in "Count". Example:

    int getElementCount() const; // Good/preffered
    int getNumElements() const;  // Incorrect
	int getNbElements() const;   // Wrong. 'Nb' is even less clear than 'Num'

### Source files:

Source files are always named using the `snake_case` notation, that is, each word in a name being
separated by an underscore and all letters in lowercase. This might seem like an inconsistency,
since the bulk of the code uses either `PascalCase` or `camelCase`, however, this decision is
a practical one. This is a multi-platform project and not all the platforms we target have
case-sensitive file systems. So, to avoid name conflicts, we choose `snake_case` for file names
as this notation avoids the problem of case-insensitivity.

C++ source files are always named with the `.cpp` extension and, for orthogonality and to
distinguish between C and C++ header files, C++ headers are always named with the `.hpp` extension.

`.inl` files are generally discouraged. Fully inline templates or classes should be
declared and defined inside the same `.hpp` file, to avoid having to maintain extra
files and to keep related things together.

### Miscellaneous:

Hexadecimal literals must be always uppercase, with the exception of the `x` in the `0x` prefix:

    0xBADF00D
    0xCAFEBABE
    0x012345

Type suffixes must be always lowercase (e.g.: u, ul, f, lf):

    // Mersenne Twister tempering constants:
    constexpr ulong A = 0x9908B0DFul;
    constexpr ulong L = 0x7FFFFFFFul;
    constexpr ulong U = 0x80000000ul;

	// Explicit floats:
    const float E     = 2.71828182845904523536f;
    const float Pi    = 3.14159265358979323846f;
    const float TwoPi = 2.0f * Pi;

Recursive functions are recommended to be suffixed with `Recursive` or to have
the word in some part of its name to make this aspect clear to the caller:

    void visitTreePostOrderRecursive(Node * subtreeRoot);

## Spacing and indenting
----

We use real `[TAB]`s to indent code, with each `[TAB]` equal to **four (4)** spaces.
Naturally, always indent to the proper scope on any control-flow statement,
function or class/structure declaration with one `[TAB]` per level. Note: **We do not indent namespaces**.

Line length should aim at a **100** columns soft limit and a **120** columns hard limit.
Special cases where there is a compelling reason not to break the line might exist,
but those should be the exception. So try to keep lines short.

Operators and expressions must be always well spaced, with one space between each operator.

    // Incorrect:
    float epsilon=0.0001f;
	for(int i=0; i<N; ++i) { ... }

    // Correct spacing:
    float epsilon = 0.0001f;
    for (int i = 0; i < N; ++i) { ... }

We put a space on **both sides** of the `*` in a pointer declaration. Same is true for a C++ reference.

    // Examples:

    int * ptr = ...;
    const char * str = "...";

    Mesh & meshRef = getMesh(i);
    ...
    void writeFile(const String & filePath);

Notice in the above that the `*` and `&` are not bound to any side of the expression.
Our rationale is that this makes the punctuation more visible, thus reducing the time it
takes for the reader to scan through the code.

In a pointer dereference or when taking the address of a variable, no extra spacing is needed.
The above only applies to declarations.

## Curly braces positioning and parenthesis
----

### Curly braces:

Curly braces `{ }` are always placed on their own lines. Examples:

    if (foo < bar)
    {
        ...
    }

    while (baz)
    {
        ...
    }

    do
    {
        ...
    } while (false);

    struct Vec3
    {
        ...
	};

    class Matrix4
    {
        ...
    };

    namespace atlas
    {
        ...
    }

**Note:** Curly braces **are mandatory** for all flow control statements, **including single line ones**.
This eases maintenance and shields the code from silly bugs resulting of adding lines to unbraced statements.

### Parenthesis:

Making operator precedence explicit with the use of parenthesis is recommended. Examples:

    int x = (y * z) - w;
    ...
    Tile t = tileMap[x + (y * width)];

Unnecessary parenthesis should be avoided on conditionals, loops or assignments:

    // Good:
    if (a == b || c == d)
    {
    }

    // Excessive use of '( )'!
    if ((a == b) || (c == d))
    {
    }

    // Good:
    for (int i = total - 1; i >= 0; --i)
    {
    }

    // Excessive use of '( )'!
    for (int i = (total - 1); i >= 0; --i)
    {
    }

    // Good:
    const float ratio = width / height;

    // Excessive use of '( )'!
    const float ratio = (width / height);

**Important:** A `return` statement **is not a function**, so never do this:

    return (true); // Don't!

This is also considered excessive use of parenthesis:

    return (x == y); // Wrong
    ...
    return (std::strcmp("hello", data) == 0); // Wrong

Should be just:

    return x == y; // Good
    ...
    return std::strcmp("hello", data) == 0; // Good

## Extended enums
----

Since we target compilers without C++11 support, the use of old-style enums is the norm.
However, old-style enums are quite poor in some aspects and lack scoping. For an improved
use of enumerators, consider the struct+enum workaround:

    struct MaterialType
    {
        enum Enum
        {
            Solid,
            Translucent,
            AlphaTested
        };

        // Optional string conversions:
        static const char * toString();
        static Enum fromString(const char * str);
    };

Then it can be used in a very neat way:

    // (partly) type checked and constants are scoped.
    const MaterialType::Enum mtrType = MaterialType::Solid;

    // toString/fromString conversions are also scoped, if provided.
	// However, would also be fine to decare them as global free functions
	// and let the overload resolution select the best match.
    const char * name = MaterialType::toString(mtrType);

## More miscellaneous, details and other general guidelines
----

- C++ makes no practical distinction between struct and class types. Our convention is
that `struct` should only be used for Plain Old Data (POD) types and behavior-less types.
`class` is for everything else. **Always** use `class` for polymorphic types and interfaces.

- Multiple inheritance should be avoided as much as possible. The only acceptable cases are
in the use of "mixin" classes (non-virtual multiple inheritance or aggregation).

- Virtual methods are also to be used conservatively, since they are not as runtime
efficient as non-virtual methods and increase executable image size.

- Order of appearance for class methods and data is always:
**public**, **protected** and lastly **private**.

- Make consistent and frequent use of `const`. Always mark methods that don't mutate
member data with `const`. Read-only function parameters are always `const`. Also, enforce
single-assignment of variable instances by making them `const` on the declaration.

- **Never add a virtual destructor "just in case"**. If a class is not meant to be inherited from,
then it doesn't need a virtual destructor. Even for classes that are inherited from, prefer
to define the base type's destructor as protected and non-virtual if possible. Virtual destructors
have a runtime cost and add vtables to the classes, which we try to avoid whenever possible.

- Consider marking classes that are not meant to be inherited from with the C++11 `final` specifier.
Using it might enable some compiler optimizations and it is simple and safe to redefine it to do
nothing if we need to compile where C++11 is not available. The same is true for the `override`
specifier. Use it freely. We can redefine it to nothing when compiling for older platforms.

- Always use `nullptr` for null pointers. Don't use `NULL` or `0`. Again, it is easy
to provide a fallback for non-C++11 compilers, and it provides better compiler diagnostics
when building with C++11.

- Assert regularly and liberally, but don't count on the assertions to be always enabled!
Don't use assertions for checks that must be always there (the `ALWAYS_CHECK()` macro is an exception).
Use them freely to aid debugging and catch errors early. Even when an error seems improbable,
add an assertion anyway to be sure.

- Template and inline code should be used conservatively. Too much inlining and template types
will bloat the executable image and bring compile times to a crawl. Use templates where they might
reduce code duplication and use inlining where there might be a performance gain, but judge this
well taking into account code size and the number of source files that might reference your header file.

- Use unnamed/anonymous namespaces for *file scoped* constants, variables, types and functions,
instead of `static`. The use of local/file scoping is encouraged, as it allow the compiler to
better optimize the code and might also speed up link times.

- `namespace`s **should not be indented**. Example:

        namespace module
        {
        class A
        {
            ...
        };
        struct B
        {
            ...
        };
        void funcC();
        } // namespace module {}

 **Note:** It is useful to add a comment to the end of the
namespace, such as `// namespace X {}` to visually mark the end of it.

## Unit tests
----

Unit tests should always be placed inside a namespace called `unittest`, which can be a child of any namespace.
When naming test functions, the notation `Test_<functionName|ClassName>_<Situation>()` is recommended.
Examples:

    namespace unittest
    {

    // Test a function named 'binarySearch()'
    void Test_binarySearch();

    // Test a function named 'strCopy()'
	void Test_strCopy();

    // Test a class/struct named 'SpatialSorter' that sorts a specific 'DrawVertex' type.
    void Test_SpatialSorter_DrawVertexSorting();

    } // namespace unittest {}

**Note:** Naming of test functions does not follow the conventions of regular functions.
They use `PascalCase` for the `Test_` prefix part and are also allowed to use underscores
in the middle of the name. This is intentional, since test functions are not called by
regular application code, only inside test modules. Calling a test function in the application
or library code would be an error, so by using this special naming convention it makes them
stand out more easily if called by accident where they shouldn't.

