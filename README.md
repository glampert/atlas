
## Atlas library

This project is a set of miscellaneous C++ libraries and code fragments focused on Games programming.

### License:

Unless stated otherwise in the source code, this project is released under the
[MIT License](http://opensource.org/licenses/MIT).

