
--
-- TODO This premake is hardcoded for Mac OSX.
-- Shouldn't take much work converting it to Win/Linux.
--

--
-- Debug switches worth enabling:
--
-- _SECURE_SCL    : for Visual Studio, to enable/disable STL bounds checking.
-- _GLIBCXX_DEBUG : for GCC only! Enables several debugging facilities.
-- _LIBCPP_DEBUG  : for Clang (libc++). See: http://stackoverflow.com/a/21847033/1198654
--                  It does not work with std::array<> however! (doesn't seem to work on std::deque either!)
--                  _LIBCPP_DEBUG2 Seems to have no effect, but leaving it there won't harm...
--

-- Specify compiler (for gmake):
--premake.gcc.cc  = "clang";
--premake.gcc.cxx = "clang++";

--------------------------------------------------------
-- Common configurations for all projects:
--------------------------------------------------------
solution("atlas");
	configurations("Debug", "Release");
	location("build");
	targetdir("build");
	defines({
		-- Config switches:
		"HAS_CPP11=1",
		"HAS_GCC_PRAGMA_DIAGNOSTIC=1",
		"IS_GNUC_OR_COMPATIBLE=1",

		"ENABLE_ANSI_COLOR_CODES=1",
		"FILTER_COLOR_CODES_FROM_LOG_OUTPUT=1",
		"DEFAULT_LOG_STDOUT=1",

		"RHI_BACKEND_OPENGL=1",
		"DRAW_INDEX_USE_32BITS=1",
		"GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS=1",
		"DOOM3BFG_MAP_TEXT_COORD_FIX=0",

		-- Other debug switches:
		"_GLIBCXX_DEBUG",
		"_LIBCPP_DEBUG=0",
		"_LIBCPP_DEBUG2=0",
		"_SECURE_SCL"
	});

-- !!! PARANOID !!!
-- List of flags explained: http://tigcc.ticalc.org/doc/comopts.html
-- These are GCC/Clang flags
--
--
local CXXFLAGS_BASIC = {
	-- misc:
	"-fno-exceptions",
	"-fno-rtti",
	"-fstrict-aliasing",
	-- warnings (GCC and Clang compatible):
	"-Wall",
	"-Wextra",
	"-Weffc++",
	"-Winit-self",
	"-Wformat=2",
	"-Wstrict-aliasing",
	"-Wuninitialized",
	"-Wunused",
	"-Wswitch",
	"-Wswitch-default",
	"-Wpointer-arith",
	"-Wwrite-strings",
	"-Wmissing-braces",
	"-Wparentheses",
	"-Wsequence-point",
	"-Wreturn-type",
	"-Wunknown-pragmas",
	"-Wshadow",
	"-Wdisabled-optimization"
};

-- Clang specific flags:
--
-- -Wc++98-compat
-- -std=c++98
--
local CXXFLAGS_EXTRA = {

-- It is interesting to sometimes compile with
-- -Weverything just to check for uncommon warning.
-- However, some are just too noisy. When testing
-- with Weverything, I also disable the following ones:
--
--	"-Weverything",
--	"-Wno-c++98-compat",
--	"-Wno-c++98-compat-pedantic",
--	"-Wno-sign-conversion",
--	"-Wno-missing-prototypes",
--	"-Wno-padded",

	"-std=c++11",
	"-Wgcc-compat",
	"-Wheader-guard",
	"-Waddress-of-array-temporary",
	"-Wglobal-constructors",
	"-Wexit-time-destructors",
	"-Wheader-hygiene",
	"-Woverloaded-virtual",
	"-Wself-assign",
	"-Wweak-vtables",
	"-Wweak-template-vtables",
	"-Wshorten-64-to-32",
	"-g" -- Debug symbols
};

local LIB_CORE_ID   = "AtlasCore";
local LIB_RENDER_ID = "AtlasRender";

--------------------------------------------------------
-- Core Library:
--------------------------------------------------------
project(LIB_CORE_ID);
	language("C++");
	kind("StaticLib");
	location("build");
	configuration("macosx", "linux", "gmake"); --Debug/Release

	buildoptions({
--
-- Ad hoc static analyzer run:
--
--		"--analyze -Xanalyzer -analyzer-output=text",
		CXXFLAGS_BASIC,
		CXXFLAGS_EXTRA
	});

	includedirs("../atlas/source/");
	files({
		-- Core common:
		"source/atlas/core/*.hpp",
		"source/atlas/core/*.cpp",
		-- Containers module:
		"source/atlas/core/containers/*.hpp",
		"source/atlas/core/containers/*.cpp",
		-- File System module:
		"source/atlas/core/fs/*.hpp",
		"source/atlas/core/fs/*.cpp",
		"source/atlas/core/fs/posix/*.cpp",
		-- Maths module:
		"source/atlas/core/maths/*.hpp",
		"source/atlas/core/maths/*.cpp",
		-- Memory Management module:
		"source/atlas/core/memory/*.hpp",
		"source/atlas/core/memory/*.cpp",
		-- Strings module:
		"source/atlas/core/strings/*.hpp",
		"source/atlas/core/strings/*.cpp",
		-- Miscellaneous Utilities:
		"source/atlas/core/utils/*.hpp",
		"source/atlas/core/utils/*.cpp"
	});

--------------------------------------------------------
-- Renderer Library:
--------------------------------------------------------
project(LIB_RENDER_ID);
	language("C++");
	kind("StaticLib");
	location("build");
	configuration("macosx", "linux", "gmake"); --Debug/Release
	buildoptions({ CXXFLAGS_BASIC, CXXFLAGS_EXTRA });
	includedirs({ "../atlas/source/", "../atlas/source/thirdparty/gl3w/include/" });
	files({
		-- Renderer shared:
		"source/atlas/render/*.hpp",
		"source/atlas/render/*.cpp",
		-- OpenGL RHI back-end:
		"source/atlas/render/opengl/*.hpp",
		"source/atlas/render/opengl/*.cpp"
	});

--------------------------------------------------------
-- Executable:
--------------------------------------------------------

project("GL3W");
	language("C");
	kind("StaticLib");
	location("build");
	configuration("macosx", "linux", "gmake"); --Debug/Release
	includedirs({ "../atlas/source/", "../atlas/source/thirdparty/gl3w/include/" });
	files({
		"source/thirdparty/gl3w/src/gl3w.c"
	});

--
-- NOTES:
--
-- SDL2 is currently built "by hand".
-- It is recommended to use CMake. Build it and link as a static library.
-- FIXME: Should try to disable unneeded stuff, like haptics, sound and
-- threads to make the SDL.a build a bit smaller.
--

project("console_test");
	language("C++");
	kind("ConsoleApp");
	location("build");
	configuration("macosx", "linux", "gmake"); --Debug/Release
	buildoptions({ CXXFLAGS_BASIC, CXXFLAGS_EXTRA });
	includedirs({ "../atlas/source/", "../atlas/source/thirdparty/gl3w/include/" });
	files({
		"main_console.cpp",
		-- UNIT TESTS
		"source/atlas/core/tests/strings/*.cpp",
		"source/atlas/core/tests/fs/*.cpp",
		"source/atlas/core/tests/utils/*.cpp",
		"source/atlas/core/tests/containers/*.cpp"
	});
	links({
		-- Mac OS specific stuff must be handled separately!
		"OpenGL.framework",
		"Cocoa.framework",
		"IOKit.framework",
		"CoreFoundation.framework",
		"CoreServices.framework",
		"QuartzCore.framework",
		"Carbon.framework",        -- only needed by SDL
		"ForceFeedback.framework", -- only needed by SDL (should find a way to remove!)

		"SDL2",
		"GL3W",

		LIB_CORE_ID,
		LIB_RENDER_ID
	});

project("gfx_test");
	language("C++");
	kind("ConsoleApp");
	location("build");
	configuration("macosx", "linux", "gmake"); --Debug/Release
	buildoptions({ CXXFLAGS_BASIC, CXXFLAGS_EXTRA });
	includedirs({ "../atlas/source/", "../atlas/source/thirdparty/gl3w/include/" });
	files({
		"main_gfx.cpp"
	});
	links({
		-- Mac OS specific stuff must be handled separately!
		"OpenGL.framework",
		"Cocoa.framework",
		"IOKit.framework",
		"CoreFoundation.framework",
		"CoreServices.framework",
		"QuartzCore.framework",
		"Carbon.framework",        -- only needed by SDL
		"ForceFeedback.framework", -- only needed by SDL (should find a way to remove!)

		"SDL2",
		"GL3W",

		LIB_CORE_ID,
		LIB_RENDER_ID
	});

