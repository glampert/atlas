
The Atlas Library Project (or Lampert's game programming toolbox)
=================================================================

Implementation Roadmap
----------------------

- Containers library.
- Strings and text formatting.
- Utils (sorting, smart-pointers, hashing).
- Math (vectors, matrices, quaternion, random, bounding-geometry).
- File System / file handlers.
- Image file loading, image and pixel manipulation and writing.
- Data compression and decompression.
- Built-in fonts / text rendering.
- JSON serialization (use a third party library, of course).
- Unit Tests for all the above!


Engineering Tasks
-----------------

- Memory management:

  * Decide which memory management scheme will be used.

  * Replace raw malloc/free calls with our functions (possibly also new/delete).

  * A 'DataBlob' type for ref-counted immutable memory blocks (good for asset/resource loading).

  * Memory Tags or similar for accounting.

  * Memory allocation tracing.


- Containers library:

  * [DONE] Array<T>: Default dynamic array type. std::vector-like class, with
    dynamic resizing. Works on User Defined Types (UDTs) as well ad PODs.

  * [DONE] SizedArray<T, N>: Fixed size, statically allocated, array wrapper. Much like std::array.
    Provides bounds checked access. Should always be used instead of a raw array/pointer.

  * CircularArrayQueue<T>: Circular array, AKA ring buffer.
  * SizedCircularArrayQueue<T, N>: Ring buffer with fixed size storage. Allocated inline with the object.

  * PodArray<T>: Optimization of Array<T> that works with Plain Old Data only.
    Performs bitwise copy with memcpy() and does not call constructors/destructors.

  * InPlaceArray<T, N> or SmallArray<T, N>: Small static array of size N. If size exceeds N, then allocate
    on the heap (similar to what std::string does). For UDTs and PODs.

  * [DONE] PtrArray<T>: Dynamic array of pointers that owns every object.
    Deletes the pointers in the destructor. Can be built on top of Array<T>.

  * ChunkedArray<T>: Array-of-arrays structure.
    A linked list of small arrays to avoid copy when growing/shrinking the array.

  * [DONE] IntrusiveSList<T>: Intrusive singly-linked list. Forward iterable.

  * [DONE] IntrusiveDList<T>: Intrusive doubly-linked list. Can be iterated both ways.

  * [DONE] IntrusiveStack<T>: Wrapper over a list that exposes a stack interface.

  * [DONE] IntrusiveQueue<T>: Wrapper over a list that exposes a queue interface.

  * [DONE] IntrusiveHashTable<K, V>: Intrusive chained hash table.

  * HashSet<T>/IntrusiveHashSet<T>: Can probably be build on top of the IntrusiveHashTable...

  * IntrusiveTreeMap<K, V>: Intrusive binary search tree/map (perhaps a red-black tree to practice it...)

  * Queue<T> & PodQueue<T>: Wrappers around Array<T> and PodArray<T> that behaves like a queue.

  * Stack<T> & PodStack<T>: Wrappers around Array<T> and PodArray<T> that behaves like a stack.

  * Map<K, V> & PodMap<K, V>: A Binary Search Tree for cases when disjoint storage is fine.

  * Improvements in the container classes:
    - (Optional) Add sorting capabilites to the list classes. Just need an iterator-based qsort function.
      QuickSort impls for reference:
      http://codereview.stackexchange.com/questions/75508/quicksort-function
      http://codereview.stackexchange.com/questions/75561/quicksort-follow-up
      http://codereview.stackexchange.com/questions/37474/templated-quicksort
      http://codereview.stackexchange.com/questions/85830/is-this-implementation-of-quicksort-good

  * C++11 Move operators and constructors for containers like Array<T>.

  * Tests for the containers submodule.


- Strings and formatting library:

  * [DONE] StringBase (non-template): Base class for string type.
    Has all the algorithms but does not manage memory.

  * [DONE] String (non-template): Default string type. Has a small preallocated buffer
    but can grow at will (like std::string).

  * [DONE] SizedString<N>: Just like SizedArray<T> but for a string.
    Doesn't allocated dynamic memory.

  * [DONE] Misc C-string functions to replace the C library, aiming at safety and portability.

  * [DONE] HashedStr: An immutable String accompanied of its hash value.

  * [DONE] Int/float <=> string conversions.

  * [DONE] Higher level functions dealing with StringBase/String, such as:
    - toString([int/float/double/etc]) => for the native types.
    - parseInt(), parseFloat(), etc.

  * [DONE] String formatting functions to replace sprintf/snprintf.

  * [DONE] A split() function to split strings by token and produce and array as output (probably relies on Array<T>).

  * C++11 Move operator and constructor for `String`.

  * [DONE] Tests for the string submodule.


- Utils library:

  * AutoPtr<T> / APtr<T>: Same as std::auto_ptr. Not ideal, but simplifies things a bit.
    We are targeting C98, so unique_ptr is not available.

  * RefPtr<T> / RPtr<T>: COM-style intrusive reference counting pointer.
    Should be fully compatible with DirectX and Windows COM objects.

  * [DONE] Other helper functions like quickSort(), binarySearch(), min()/max(), swap() etc.

  * [DONE] Common hash functions / CRC32.


- Maths library:

  * [DONE] Random and Rand-Engines: Random number generator with a few choices of engine.

  * [DONE] Vec2, Vec3, Vec4: Float vectors, unpadded.

  * [DONE] Vec2i, Vec3i, Vec4i: General purpose integer vectors, unpadded.

  * [DONE] Degrees/Radians: Types classes to represent angles.

  * Point2, Point3: Represents a point in space (not the same as a vector!)

  * YawPitchRoll: Euler angles for to represent rotation.

  * Rect4, Rect4i: Rectangle of floats (Rect4) and integers (Rect4i): [x, y, width, height] tuple.

  * Quat4: x,y,z,w Quaternion type.

  * Mat4x4: Full 4x4 homogeneous matrix, OpenGL compatible.

  * Mat4x3: Compact transformation matrix that omits the padding of a 4x4 matrix.

  * Aabb: Axis-Aligned Bounding Box.

  * Obb: Oriented Bounding Box.

  * Plane: x,y,z,d plane equation.

  * LineSeg: 3D line segment (start, end).

  * Ray: 3D ray (direction and origin).

  * Frustum: Frustum pyramid for camera culling.

  * Test for the maths library.


- File System library:

  * [DONE] File: Base class for all files. Not directly instantiable.

  * [DONE] ReadableFile: File opened for reading. Has no write methods.

  * [DONE] WritableFile: File opened for writing. Has no read methods.

  * [DONE] ReadWriteFile: File opened for reading, writing or appending. Has all the methods.

  * [DONE] FS helper functions like createDirectory(), createPath(), queryFileSize(), etc.


- Threading library:

  * The default implementation should be a thin wrapper to C++11 threads, easier and cleaner.
    Provide system-specific fallbacks where C++11 is not an option.

  * Explore the usage of 'Fibers' AKA lightweight user-side threads. (Refer to the GDC talk by NaughtyDog)


- Image library:

  * [DONE] PixelFormat: Abstract class that handles different types of pixel formats found in images.
    We Implement it for every new format that we wish to support. Should take extra care to ensure
    memory allocations are cache friendly and properly aligned. Also provide a few basic conversions.

  * [DONE] Image: Represents a pixel map (an image) Should be a value type, rather than a pointer type.
    Let PixelFormat handle the different image data formats. Ensure it supports surfaces (mip-levels).
    The default is 1 or more surfaces for every image.

  * [DONE] Color: Compact representation of a color or pixel, used internally when we need to represent a color.
    Internally, should be compact (RGBA 8bits), but provide all the facilities to convert from/to common
    formats, such as RGB 8bits <=> RGB float <=> HSV, etc.

  * [DONE] Image loading and writing can be done with the help of STB Image and Mini-Z, both public domain libraries.


- Data compression and decompression:

  * [DONE] Mini-Z is a very nice and simple public domain compression library,
    but its interface is C and not very friendly. Should wrap that up into and easier to use set of C++ classes.

  * [DONE] STB image has the same problem, plus, it has some statics that are not thread safe.
    Wrap STB image into a class and also change its source code to fix the threading issues.


- Built-in text rendering / fonts:

  * Simplest and most portable way to embed text fonts in the application:
    - Get a bitmap with the chars (generated by a tool like Hiero).
    - Compress it with Zlib/Mini-Z.
    - Dump that as a C array of bytes.
    - Embed data in the source code.


- UI library:

  * Consider forking the RocketUI (http://librocket.com/) library and starting from there...

