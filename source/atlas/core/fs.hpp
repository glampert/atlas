
// ================================================================================================
// -*- C++ -*-
// File: fs.hpp
// Author: Guilherme R. Lampert
// Created on: 22/12/14
// Brief: Master header of the File System submodule.
//        Including this header makes all the FS tools available.
// ================================================================================================

#ifndef ATLAS_CORE_FS_HPP
#define ATLAS_CORE_FS_HPP

#include "atlas/core/fs/file_handling.hpp"
#include "atlas/core/fs/zip_handling.hpp"
#include "atlas/core/fs/path_utils.hpp"

#endif // ATLAS_CORE_FS_HPP
