
// ================================================================================================
// -*- C++ -*-
// File: maths.hpp
// Author: Guilherme R. Lampert
// Created on: 09/01/15
// Brief: Master header of the maths submodule.
//        Including this header makes all the core math utilities and types available.
// ================================================================================================

#ifndef ATLAS_CORE_MATHS_HPP
#define ATLAS_CORE_MATHS_HPP

#include "atlas/core/maths/math_utils.hpp"
#include "atlas/core/maths/vectors.hpp"
#include "atlas/core/maths/matrices.hpp"
#include "atlas/core/maths/quaternion.hpp"
#include "atlas/core/maths/angles.hpp"
#include "atlas/core/maths/random.hpp"

#endif // ATLAS_CORE_MATHS_HPP
