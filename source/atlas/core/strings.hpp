
// ================================================================================================
// -*- C++ -*-
// File: strings.hpp
// Author: Guilherme R. Lampert
// Created on: 17/12/14
// Brief: Master header of the strings submodule.
//        Including this header makes all the string handling tools available.
// ================================================================================================

#ifndef ATLAS_CORE_STRINGS_HPP
#define ATLAS_CORE_STRINGS_HPP

#include "atlas/core/strings/c_string.hpp"
#include "atlas/core/strings/string_base.hpp"
#include "atlas/core/strings/sized_string.hpp"
#include "atlas/core/strings/dynamic_string.hpp"
#include "atlas/core/strings/hashed_str.hpp"
#include "atlas/core/strings/to_string.hpp"
#include "atlas/core/strings/lexer.hpp"

#endif // ATLAS_CORE_STRINGS_HPP
