
// ================================================================================================
// -*- C++ -*-
// File: random.hpp
// Author: Guilherme R. Lampert
// Created on: 24/03/15
// Brief: Pseudo-random number generators.
// ================================================================================================

#ifndef ATLAS_CORE_MATHS_RANDOM_HPP
#define ATLAS_CORE_MATHS_RANDOM_HPP

#include "atlas/core/maths/math_utils.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// class LcgEngine:
// ========================================================

// Implements a Linear Congruential Generator pseudo-random engine.
// This engine is very lightweight and fast, but not very reliable
// in its distribution and period.
class LcgEngine final
{
public:

	// Constructors:
	LcgEngine();
	explicit LcgEngine(uint newSeed);

	// Reset (seed) the pseudo-random generator engine.
	void seed(uint newSeed);

	// Get the next value in the pseudo-random sequence.
	uint nextValue();

	// Maximum value returned by `nextValue()`.
	static constexpr uint getMaxValue() { return uintMax; }

private:

	// LCG state:
	uint x;
};

// ========================================================
// class XorShiftEngine:
// ========================================================

// Implements a XOR-Shift pseudo-random engine.
// A little better period than the LCG and still blazing fast.
class XorShiftEngine final
{
public:

	// Constructors:
	XorShiftEngine();
	explicit XorShiftEngine(uint newSeed);

	// Reset (seed) the pseudo-random generator engine.
	void seed(uint newSeed);

	// Get the next value in the pseudo-random sequence.
	uint nextValue();

	// Maximum value returned by `nextValue()`.
	static constexpr uint getMaxValue() { return uintMax; }

private:

	// XOR-Shift states:
	uint x, y, z, w;
};

// ========================================================
// class Mt19937Engine:
// ========================================================

// Mersenne Twister pseudo-random engine.
// Very good distribution and period but at a higher performance and storage cost.
class Mt19937Engine final
{
public:

	// Constructors:
	Mt19937Engine();
	explicit Mt19937Engine(uint newSeed);

	// Reset (seed) the pseudo-random generator engine.
	void seed(uint newSeed);

	// Get the next value in the pseudo-random sequence.
	uint nextValue();

	// Maximum value returned by `nextValue()`.
	static constexpr uint getMaxValue() { return uintMax; }

private:

	// Size of the MT-19937 state vector:
	static constexpr ulong N = 624;

	// PRNG states:
	ulong next;
	ulong x[N];
};

// ========================================================
// template class Random<ENGINE>:
// ========================================================

// Pseudo-random number generator class with several distribution functions.
// The Random class is a template that accepts any compliant random generator engine.
// This class implements the most common distribution functions for the underlaying engine.
template<class ENGINE>
class Random final
{
public:

	typedef ENGINE EngineType;

	// Construct with default seed.
	Random();

	// Construct with user defined seed.
	explicit Random(uint newSeed);

	// Construct with a preexistent random generator engine.
	explicit Random(const ENGINE & eng);

	// Reset (seed) the pseudo-random generator.
	void seed(uint newSeed);

	// Pseudo-random boolean value.
	bool nextBoolean();

	// Pseudo-random floating-point number in range [0,1].
	float nextFloat();

	// Pseudo-random integer in range [0, getMaxValue()].
	uint nextInteger();

	// Pseudo-random floating-point number in range [lowerBound, upperBound].
	float nextFloat(float lowerBound, float upperBound);

	// Pseudo-random integer in range [lowerBound, upperBound].
	int nextInteger(int lowerBound, int upperBound);

	// Pseudo-random floating-point number in range [-1,+1].
	float nextSymmetric();

	// Pseudo-random number with exponential distribution.
	float nextExponential(float average);

	// Pseudo-random number with normal (Gaussian) distribution.
	float nextGaussian(float average, float stdDeviation);

	// Access the underlaying random generator engine.
	ENGINE & getRandomEngine();

	// Access the underlaying random generator engine (const overload).
	const ENGINE & getRandomEngine() const;

	// Max integer value that can be generated. Defined by the underlaying engine.
	static constexpr uint getMaxValue() { return ENGINE::getMaxValue(); }

private:

	// The pseudo-random generator engine.
	ENGINE engine;

	// Helper variables used in the normal distribution calculation.
	float leftover;
	bool  useLeftover;
};

// ========================================================
// Shorthand type aliases:
// ========================================================

// Linear Congruential Generator (LCG):
//  See http://en.wikipedia.org/wiki/Linear_congruential_generator
typedef Random<LcgEngine> LcgRandom;

// XOR-shift:
//  See http://en.wikipedia.org/wiki/Xorshift
typedef Random<XorShiftEngine> XorShiftRandom;

// Mersenne Twister:
//  See http://en.wikipedia.org/wiki/Mersenne_twister
typedef Random<Mt19937Engine> Mt19937Random;

// ================================================================================================
// Random inline methods:
// ================================================================================================

// ========================================================
// Random::Random():
// ========================================================

template<class ENGINE>
Random<ENGINE>::Random()
	: leftover(0.0f)
	, useLeftover(false)
{
	// Init with defaults.
}

// ========================================================
// Random::Random():
// ========================================================

template<class ENGINE>
Random<ENGINE>::Random(const uint newSeed)
	: engine(newSeed)
	, leftover(0.0f)
	, useLeftover(false)
{
	// Init with user provided seed.
}

// ========================================================
// Random::Random():
// ========================================================

template<class ENGINE>
Random<ENGINE>::Random(const ENGINE & eng)
	: engine(eng)
	, leftover(0.0f)
	, useLeftover(false)
{
	// Init with existing Engine.
}

// ========================================================
// Random::seed():
// ========================================================

template<class ENGINE>
void Random<ENGINE>::seed(const uint newSeed)
{
	engine.seed(newSeed);
}

// ========================================================
// Random::nextBoolean():
// ========================================================

template<class ENGINE>
bool Random<ENGINE>::nextBoolean()
{
	return nextFloat() > 0.5f;
}

// ========================================================
// Random::nextFloat():
// ========================================================

template<class ENGINE>
float Random<ENGINE>::nextFloat()
{
	return static_cast<float>(engine.nextValue()) / ENGINE::getMaxValue();
}

// ========================================================
// Random::nextInteger():
// ========================================================

template<class ENGINE>
uint Random<ENGINE>::nextInteger()
{
	return engine.nextValue();
}

// ========================================================
// Random::nextFloat():
// ========================================================

template<class ENGINE>
float Random<ENGINE>::nextFloat(const float lowerBound, const float upperBound)
{
	return (nextFloat() * (upperBound - lowerBound)) + lowerBound;
}

// ========================================================
// Random::nextInteger():
// ========================================================

template<class ENGINE>
int Random<ENGINE>::nextInteger(const int lowerBound, const int upperBound)
{
	if (lowerBound == upperBound)
	{
		return lowerBound;
	}
	return (nextInteger() % (upperBound - lowerBound)) + lowerBound;
}

// ========================================================
// Random::nextSymmetric():
// ========================================================

template<class ENGINE>
float Random<ENGINE>::nextSymmetric()
{
	return (2.0f * nextFloat()) - 1.0f;
}

// ========================================================
// Random::nextExponential():
// ========================================================

template<class ENGINE>
float Random<ENGINE>::nextExponential(const float average)
{
	return -average * math::ln(1.0f - nextFloat());
}

// ========================================================
// Random::nextGaussian():
// ========================================================

template<class ENGINE>
float Random<ENGINE>::nextGaussian(const float average, const float stdDeviation)
{
	// See https://www.taygeta.com/random/gaussian.html
	// for a full explanation.

	float x1, x2, y1, y2, w;
	if (useLeftover) // Use leftover value from previous call?
	{
		y1 = leftover;
		useLeftover = false;
	}
	else
	{
		do
		{
			x1 = (2.0f * nextFloat()) - 1.0f;
			x2 = (2.0f * nextFloat()) - 1.0f;
			w = (x1 * x1) + (x2 * x2);
		}
		while (w >= 1.0f);

		w = math::sqrt((-2.0f * math::ln(w)) / w);
		y1 = (x1 * w);
		y2 = (x2 * w);

		leftover = y2;
		useLeftover = true;
	}

	return average + y1 * stdDeviation;
}

// ========================================================
// Random::getRandomEngine():
// ========================================================

template<class ENGINE>
ENGINE & Random<ENGINE>::getRandomEngine()
{
	return engine;
}

// ========================================================
// Random::getRandomEngine():
// ========================================================

template<class ENGINE>
const ENGINE & Random<ENGINE>::getRandomEngine() const
{
	return engine;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_MATHS_RANDOM_HPP
