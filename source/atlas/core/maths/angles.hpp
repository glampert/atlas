
// ================================================================================================
// -*- C++ -*-
// File: angles.hpp
// Author: Guilherme R. Lampert
// Created on: 24/03/15
// Brief: Helper types to represent angles and conversions.
// ================================================================================================

#ifndef ATLAS_CORE_MATHS_ANGLES_HPP
#define ATLAS_CORE_MATHS_ANGLES_HPP

#include "atlas/core/maths/math_utils.hpp"
#include "atlas/core/strings/to_string.hpp"

namespace atlas
{
namespace core
{
namespace internal
{

// ========================================================
// template class CommonAngleOps<DERIVED>:
// ========================================================

template<class DERIVED>
class CommonAngleOps
{
public:

	//
	// Add, subtract and negate angles:
	//

	DERIVED operator - () const { return DERIVED(-angle); }

	DERIVED operator + (const DERIVED & other) const { return DERIVED(angle + other.angle); }
	DERIVED operator - (const DERIVED & other) const { return DERIVED(angle - other.angle); }

	DERIVED & operator += (const DERIVED & other) { setFloatValue(angle + other.angle); return static_cast<DERIVED &>(*this); }
	DERIVED & operator -= (const DERIVED & other) { setFloatValue(angle - other.angle); return static_cast<DERIVED &>(*this); }

	//
	// Multiply and divide angle by a scalar value:
	//

	DERIVED operator * (const float scalar) const { return DERIVED(angle * scalar); }
	DERIVED operator / (const float scalar) const { return DERIVED(angle / scalar); }

	DERIVED & operator *= (const float scalar) { setFloatValue(angle * scalar); return static_cast<DERIVED &>(*this); }
	DERIVED & operator /= (const float scalar) { setFloatValue(angle / scalar); return static_cast<DERIVED &>(*this); }

	//
	// Expose the built-in comparison operators:
	//

	bool operator == (const DERIVED & other) const { return angle == other.angle; }
	bool operator != (const DERIVED & other) const { return angle != other.angle; }
	bool operator <= (const DERIVED & other) const { return angle <= other.angle; }
	bool operator >= (const DERIVED & other) const { return angle >= other.angle; }
	bool operator  < (const DERIVED & other) const { return angle  < other.angle; }
	bool operator  > (const DERIVED & other) const { return angle  > other.angle; }

	//
	// Comparison with tolerance/epsilon:
	//

	bool greaterThanZero(const float tolerance = math::Epsilon) const
	{
		return math::floatGreaterThanZero(angle, tolerance);
	}

	bool equals(const float x, const float tolerance = math::Epsilon) const
	{
		return math::floatEquals(angle, x, tolerance);
	}

	bool lessThan(const float x, const float tolerance = math::Epsilon) const
	{
		return math::floatLessThan(angle, x, tolerance);
	}

	bool greaterThan(const float x, const float tolerance = math::Epsilon) const
	{
		return math::floatGreaterThan(angle, x, tolerance);
	}

	bool lessThanOrEqual(const float x, const float tolerance = math::Epsilon) const
	{
		return math::floatLessThanOrEqual(angle, x, tolerance);
	}

	bool greaterThanOrEqual(const float x, const float tolerance = math::Epsilon) const
	{
		return math::floatGreaterThanOrEqual(angle, x, tolerance);
	}

	//
	// Access the underlaying scalar value:
	//

	float getFloatValue() const
	{
		return angle;
	}

	void setFloatValue(const float ang)
	{
		DEBUG_CHECK(DERIVED::isValidAngle(ang) && "Value is not in a valid range to be used as a degrees or radians angle!");
		angle = ang;
	}

protected:

	CommonAngleOps() : angle(0.0f) { }

	// The actual angle. Interpreted as radians or degrees,
	// depending on the class that implements this.
	float angle;
};

} // namespace internal {}

class Radians;
class Degrees;

// ========================================================
// class Radians:
// ========================================================

class Radians final
	: public internal::CommonAngleOps<Radians>
{
public:

	explicit Radians(float radians);
	explicit Radians(const Degrees & degrees);
	Degrees toDegrees() const;

	// Sine/cosine/tangent of this angle:
	float sin() const;
	float cos() const;
	float tan() const;

	// To angle:
	static Radians asin(float sine);
	static Radians acos(float cosine);
	static Radians atan(float tangent);
	static Radians atan(float x, float y);

	// Map an angle in radians to the [0,2pi] range.
	static Radians normalizeAngleTwoPi(float radians);

	// Map an angle in radians to the [-pi,+pi] range.
	static Radians normalizeAnglePi(float radians);

	// Test if an (absolute) float value is in the [0,2pi] range.
	static bool isValidAngle(float radians);
};

// ========================================================
// class Degrees:
// ========================================================

class Degrees final
	: public internal::CommonAngleOps<Degrees>
{
public:

	explicit Degrees(float degrees);
	explicit Degrees(const Radians & radians);
	Radians toRadians() const;

	// Sine/cosine/tangent of this angle:
	float sin() const;
	float cos() const;
	float tan() const;

	// To angle:
	static Degrees asin(float sine);
	static Degrees acos(float cosine);
	static Degrees atan(float tangent);
	static Degrees atan(float x, float y);

	// Map an angle in degrees to the [0,360] range.
	static Degrees normalizeAngle360(float degrees);

	// Map an angle in degrees to the [-180,+180] range.
	static Degrees normalizeAngle180(float degrees);

	// Test if an (absolute) float value is in the [0,360] range.
	static bool isValidAngle(float degrees);
};

// ================================================================================================
// Radians inline methods:
// ================================================================================================

inline Radians::Radians(const float radians)
{
	setFloatValue(radians);
}

inline Radians::Radians(const Degrees & degrees)
{
	setFloatValue(degrees.toRadians().angle);
}

inline Degrees Radians::toDegrees() const
{
	return Degrees(angle * math::RadToDeg);
}

inline float Radians::sin() const
{
	return math::sin(angle);
}

inline float Radians::cos() const
{
	return math::cos(angle);
}

inline float Radians::tan() const
{
	return math::tan(angle);
}

inline Radians Radians::asin(const float sine)
{
	return Radians(math::asin(sine));
}

inline Radians Radians::acos(const float cosine)
{
	return Radians(math::acos(cosine));
}

inline Radians Radians::atan(const float tangent)
{
	return Radians(math::atan(tangent));
}

inline Radians Radians::atan(const float x, const float y)
{
	return Radians(math::atan(x, y));
}

inline Radians Radians::normalizeAngleTwoPi(float radians)
{
	if (radians >= math::TwoPi || radians < 0.0f)
	{
		radians -= math::floor(radians * (1.0f / math::TwoPi)) * math::TwoPi;
	}
	return Radians(radians);
}

inline Radians Radians::normalizeAnglePi(float radians)
{
	radians = normalizeAngleTwoPi(radians).angle;
	if (radians > math::Pi)
	{
		radians -= math::TwoPi;
	}
	return Radians(radians);
}

inline bool Radians::isValidAngle(const float radians)
{
	return math::floatLessThanOrEqual(math::abs(radians), math::TwoPi);
}

// Global toString() for Radians type:
inline String toString(const Radians & angle, const int decimalDigits = -1)
{
	return core::toString(angle.getFloatValue(), decimalDigits) + " (rad)";
}

// User defined suffix `_rad` for literals with type `Radians` (C++11).
#if ATLAS_COMPILER_HAS_CPP11
inline Radians operator "" _rad (long double radians)
{
	// Note: The standard requires the input parameter to be `long double`!
	return Radians(static_cast<float>(radians));
}
#endif // ATLAS_COMPILER_HAS_CPP11

// ================================================================================================
// Degrees inline methods:
// ================================================================================================

inline Degrees::Degrees(const float degrees)
{
	setFloatValue(degrees);
}

inline Degrees::Degrees(const Radians & radians)
{
	setFloatValue(radians.toDegrees().angle);
}

inline Radians Degrees::toRadians() const
{
	return Radians(angle * math::DegToRad);
}

inline float Degrees::sin() const
{
	return math::sin(angle * math::DegToRad);
}

inline float Degrees::cos() const
{
	return math::cos(angle * math::DegToRad);
}

inline float Degrees::tan() const
{
	return math::tan(angle * math::DegToRad);
}

inline Degrees Degrees::asin(const float sine)
{
	return Degrees(math::asin(sine) * math::RadToDeg);
}

inline Degrees Degrees::acos(const float cosine)
{
	return Degrees(math::acos(cosine) * math::RadToDeg);
}

inline Degrees Degrees::atan(const float tangent)
{
	return Degrees(math::atan(tangent) * math::RadToDeg);
}

inline Degrees Degrees::atan(const float x, const float y)
{
	return Degrees(math::atan(x, y) * math::RadToDeg);
}

inline Degrees Degrees::normalizeAngle360(float degrees)
{
	if (degrees >= 360.0f || degrees < 0.0f)
	{
		degrees -= math::floor(degrees * (1.0f / 360.0f)) * 360.0f;
	}
	return Degrees(degrees);
}

inline Degrees Degrees::normalizeAngle180(float degrees)
{
	degrees = normalizeAngle360(degrees).angle;
	if (degrees > 180.0f)
	{
		degrees -= 360.0f;
	}
	return Degrees(degrees);
}

inline bool Degrees::isValidAngle(const float degrees)
{
	return math::floatLessThanOrEqual(math::abs(degrees), 360.0f);
}

// Global toString() for Degrees type:
inline String toString(const Degrees & angle, const int decimalDigits = -1)
{
	return core::toString(angle.getFloatValue(), decimalDigits) + " (deg)";
}

// User defined suffix `_deg` for literals with type `Degrees` (C++11).
#if ATLAS_COMPILER_HAS_CPP11
inline Degrees operator "" _deg (long double degrees)
{
	// Note: The standard requires the input parameter to be `long double`!
	return Degrees(static_cast<float>(degrees));
}
#endif // ATLAS_COMPILER_HAS_CPP11

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_MATHS_ANGLES_HPP
