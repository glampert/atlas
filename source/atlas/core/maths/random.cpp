
// ================================================================================================
// -*- C++ -*-
// File: random.cpp
// Author: Guilherme R. Lampert
// Created on: 24/03/15
// Brief: Pseudo-random number generators.
// ================================================================================================

#include "atlas/core/maths/random.hpp"

namespace atlas
{
namespace core
{

// ================================================================================================
// LcgEngine class implementation:
// ================================================================================================

LcgEngine::LcgEngine()
	: x(48271u)
{
	// Initial constant from Wikipedia:
	//  http://en.wikipedia.org/wiki/Lehmer_random_number_generator
}

LcgEngine::LcgEngine(const uint newSeed)
{
	seed(newSeed);
}

void LcgEngine::seed(const uint newSeed)
{
	x = newSeed ^ 48271u;
}

uint LcgEngine::nextValue()
{
	// This is Lehmer's LCG formula:
	x = (static_cast<uint64>(x) * 279470273ul) % 4294967291ul;
	return x;
}

// ================================================================================================
// XorShiftEngine class implementation:
// ================================================================================================

XorShiftEngine::XorShiftEngine()
	: x(123456789u)
	, y(362436069u)
	, z(521288629u)
	, w(88675123u)
{
	// Initial constants presented in this paper:
	//  http://www.jstatsoft.org/v08/i14/paper
}

XorShiftEngine::XorShiftEngine(const uint newSeed)
{
	seed(newSeed);
}

void XorShiftEngine::seed(const uint newSeed)
{
	x = newSeed ^ 123456789u;
	y = newSeed ^ 362436069u;
	z = newSeed ^ 521288629u;
	w = newSeed ^ 88675123u;
}

uint XorShiftEngine::nextValue()
{
	// XOR-Shift-128 RNG:
	uint t = x ^ (x << 11);
	x = y;
	y = z;
	z = w;
	w = w ^ (w >> 19) ^ t ^ (t >> 8);
	return w;
}

// ================================================================================================
// Mt19937Engine class implementation:
// ================================================================================================

Mt19937Engine::Mt19937Engine()
	: next(N)
{
	// Same seed used by std random (C++11).
	seed(5489u);
}

Mt19937Engine::Mt19937Engine(const uint newSeed)
	: next(N)
{
	seed(newSeed);
}

void Mt19937Engine::seed(const uint newSeed)
{
	x[0] = newSeed & 0xFFFFFFFFul;
	for (ulong i = 1; i < N; ++i)
	{
		x[i] = (1812433253ul * (x[i - 1] ^ (x[i - 1] >> 30)) + i);
		x[i] &= 0xFFFFFFFFul;
	}
}

uint Mt19937Engine::nextValue()
{
	ulong y, a;

	// Refill x[] if exhausted:
	if (next == N)
	{
		// Mersenne Twister tempering constants:
		constexpr ulong A = 0x9908B0DFul;
		constexpr ulong L = 0x7FFFFFFFul;
		constexpr ulong U = 0x80000000ul;
		constexpr ulong M = 397;

		next = 0;
		for (ulong i = 0; i < (N - 1); ++i)
		{
			y = (x[i] & U) | (x[i + 1] & L);
			a = (y & 0x1ul) ? A : 0x0ul;
			x[i] = x[(i + M) % N] ^ (y >> 1) ^ a;
		}

		y = (x[N - 1] & U) | (x[0] & L);
		a = (y & 0x1ul) ? A : 0x0ul;
		x[N - 1] = x[M - 1] ^ (y >> 1) ^ a;
	}

	y = x[next++];

	// Improve distribution:
	y ^= (y >> 11);
	y ^= (y <<  7) & 0x9D2C5680ul;
	y ^= (y << 15) & 0xEFC60000ul;
	y ^= (y >> 18);

	return static_cast<uint>(y);
}

} // namespace core {}
} // namespace atlas {}
