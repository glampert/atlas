
// ================================================================================================
// -*- C++ -*-
// File: math_utils.cpp
// Author: Guilherme R. Lampert
// Created on: 09/01/15
// Brief: Miscellaneous math utilities and functions.
// ================================================================================================

#include "atlas/core/maths/math_utils.hpp"

namespace atlas
{
namespace core
{
namespace math
{

// ========================================================
// Frequently used mathematical constants:
// ========================================================

const float E          = 2.71828182845904523536f;
const float Pi         = 3.14159265358979323846f;
const float TwoPi      = 2.0f * Pi;
const float HalfPi     = 0.5f * Pi;
const float InvPi      = 1.0f / Pi;
const float InvTwoPi   = 1.0f / TwoPi;
const float LogOf2     = 0.693147180559945309417f;
const float LogOf10    = 2.30258509299404568402f;
const float InvLogOf2  = 1.0f / LogOf2;
const float InvLogOf10 = 1.0f / LogOf10;
const float DegToRad   = Pi / 180.0f;
const float RadToDeg   = 180.0f / Pi;
const float SecToMsec  = 1000.0f;
const float MsecToSec  = 0.001f;
const float Infinity   = HUGE_VAL;
const float Epsilon    = FLT_EPSILON;

} // namespace math {}
} // namespace core {}
} // namespace atlas {}
