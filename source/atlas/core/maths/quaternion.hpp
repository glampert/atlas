
// ================================================================================================
// -*- C++ -*-
// File: quaternion.hpp
// Author: Guilherme R. Lampert
// Created on: 14/08/15
// Brief: Defines a Quaternion type (compact rotation transform).
// ================================================================================================

#ifndef ATLAS_CORE_MATHS_QUATERNION_HPP
#define ATLAS_CORE_MATHS_QUATERNION_HPP

#include "atlas/core/maths/vectors.hpp"
#include "atlas/core/maths/angles.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// class Quat:
// ========================================================

struct Quat final
{
	float x, y, z; // Rotation axis (real part).
	float w;       // Rotation amount (imaginary part).

	//TODO work in progress...

	Quat() { }
	Quat(float xx, float yy, float zz, float ww)
		: x(xx), y(yy), z(zz), w(ww)
	{
	}

	float * getData() { return reinterpret_cast<float *>(this); }
	const float * getData() const { return reinterpret_cast<const float *>(this); }

	Quat & normalize()
	{
		const float invLen = math::invSqrt((x * x) + (y * y) + (z * z) + (w * w));
		x *= invLen;
		y *= invLen;
		z *= invLen;
		w *= invLen;
		return *this;
	}

	float computeW() const
	{
		const float t = 1.0f - (x * x) - (y * y) - (z * z);
		if (t < 0.0f)
		{
			return 0.0f;
		}
		else
		{
			return -math::sqrt(t);
		}
	}

	Vec3 rotatePoint(const Vec3 & in) const
	{
		Quat inv(-x, -y, -z, w);
		inv.normalize();

		const Quat temp((*this) * in); // Quat * Vec3
		const Quat outQ(temp * inv);   // Quat * Quat

		return Vec3(outQ.x, outQ.y, outQ.z);
	}

	Quat operator * (const Vec3 & v) const
	{
		return Quat((w * v.x) + (y * v.z) - (z * v.y),
		            (w * v.y) + (z * v.x) - (x * v.z),
		            (w * v.z) + (x * v.y) - (y * v.x),
		           -(x * v.x) - (y * v.y) - (z * v.z));
	}

	Quat operator * (const Quat & q) const
	{
		return Quat((x * q.w) + (w * q.x) + (y * q.z) - (z * q.y),
		            (y * q.w) + (w * q.y) + (z * q.x) - (x * q.z),
		            (z * q.w) + (w * q.z) + (x * q.y) - (y * q.x),
		            (w * q.w) - (x * q.x) - (y * q.y) - (z * q.z));
	}
};

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_MATHS_QUATERNION_HPP
