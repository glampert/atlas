
// ================================================================================================
// -*- C++ -*-
// File: matrices.cpp
// Author: Guilherme R. Lampert
// Created on: 11/08/15
// Brief: 3x3 and 4x4 floating-point matrices, used for rendering. Row-major order.
// ================================================================================================

#include "atlas/core/maths/matrices.hpp"

namespace atlas
{
namespace core
{

// ================================================================================================
// Mat3x3 class implementation:
// ================================================================================================

COMPILE_TIME_CHECK(sizeof(Mat3x3) == sizeof(float) * 9, "Bad size for Mat3x3!");

// ========================================================
// Mat3x3::transpose():
// ========================================================

Mat3x3 & Mat3x3::transpose()
{
	const Vec3 r0 = getCol(0);
	const Vec3 r1 = getCol(1);
	const Vec3 r2 = getCol(2);
	rows[0] = r0;
	rows[1] = r1;
	rows[2] = r2;
	return *this;
}

// ========================================================
// Mat3x3::rotationX():
// ========================================================

Mat3x3 Mat3x3::rotationX(const Radians radians)
{
	const float c = radians.cos();
	const float s = radians.sin();

	return Mat3x3(Vec3::unitX(),
	              Vec3(0.0f,  c, s),
	              Vec3(0.0f, -s, c));
}

// ========================================================
// Mat3x3::rotationY():
// ========================================================

Mat3x3 Mat3x3::rotationY(const Radians radians)
{
	const float c = radians.cos();
	const float s = radians.sin();

	return Mat3x3(Vec3( c, 0.0f, s),
	              Vec3::unitY(),
	              Vec3(-s, 0.0f, c));
}

// ========================================================
// Mat3x3::rotationZ():
// ========================================================

Mat3x3 Mat3x3::rotationZ(const Radians radians)
{
	const float c = radians.cos();
	const float s = radians.sin();

	return Mat3x3(Vec3( c, s, 0.0f),
	              Vec3(-s, c, 0.0f),
	              Vec3::unitZ());
}

// ========================================================
// Mat3x3::rotationXYZ():
// ========================================================

Mat3x3 Mat3x3::rotationXYZ(const Radians rX, const Radians rY, const Radians rZ)
{
	const Mat3x3 matRx = Mat3x3::rotationX(rX);
	const Mat3x3 matRy = Mat3x3::rotationY(rY);
	const Mat3x3 matRz = Mat3x3::rotationZ(rZ);
	return matRx * matRy * matRz;
}

// ========================================================
// Mat3x3::multiply():
// ========================================================

Mat3x3 Mat3x3::multiply(const Mat3x3 & a, const Mat3x3 & b)
{
	// Algorithm:
	//  result(i,j) = a(i,k) . b(k,j)
	// or in other words:
	//  result(i,j) = dot(row(a, i), col(b, j));

	typedef float Vec3Ptr[3];
	Mat3x3 result;

	Vec3Ptr * R = reinterpret_cast<Vec3Ptr *>(&result);
	const Vec3Ptr * A = reinterpret_cast<const Vec3Ptr *>(&a);
	const Vec3Ptr * B = reinterpret_cast<const Vec3Ptr *>(&b);

	R[0][0] = (A[0][0] * B[0][0]) + (A[0][1] * B[1][0]) + (A[0][2] * B[2][0]);
	R[0][1] = (A[0][0] * B[0][1]) + (A[0][1] * B[1][1]) + (A[0][2] * B[2][1]);
	R[0][2] = (A[0][0] * B[0][2]) + (A[0][1] * B[1][2]) + (A[0][2] * B[2][2]);

	R[1][0] = (A[1][0] * B[0][0]) + (A[1][1] * B[1][0]) + (A[1][2] * B[2][0]);
	R[1][1] = (A[1][0] * B[0][1]) + (A[1][1] * B[1][1]) + (A[1][2] * B[2][1]);
	R[1][2] = (A[1][0] * B[0][2]) + (A[1][1] * B[1][2]) + (A[1][2] * B[2][2]);

	R[2][0] = (A[2][0] * B[0][0]) + (A[2][1] * B[1][0]) + (A[2][2] * B[2][0]);
	R[2][1] = (A[2][0] * B[0][1]) + (A[2][1] * B[1][1]) + (A[2][2] * B[2][1]);
	R[2][2] = (A[2][0] * B[0][2]) + (A[2][1] * B[1][2]) + (A[2][2] * B[2][2]);

	return result;
}

// ========================================================
// Mat3x3::transform():
// ========================================================

Vec3 Mat3x3::transform(const Vec3 & p, const Mat3x3 & m)
{
	typedef float Vec3Ptr[3];
	const Vec3Ptr * M = reinterpret_cast<const Vec3Ptr *>(&m);

	Vec3 result; // Dot product of the row vector/point with each column of the matrix.
	result.x = (M[0][0] * p.x) + (M[1][0] * p.y) + (M[2][0] * p.z);
	result.y = (M[0][1] * p.x) + (M[1][1] * p.y) + (M[2][1] * p.z);
	result.z = (M[0][2] * p.x) + (M[1][2] * p.y) + (M[2][2] * p.z);
	return result;
}

// ================================================================================================
// Mat4x4 class implementation:
// ================================================================================================

COMPILE_TIME_CHECK(sizeof(Mat4x4) == sizeof(float) * 16, "Bad size for Mat4x4!");

// ========================================================
// Mat4x4::transpose():
// ========================================================

Mat4x4 & Mat4x4::transpose()
{
	const Vec4 r0 = getCol(0);
	const Vec4 r1 = getCol(1);
	const Vec4 r2 = getCol(2);
	const Vec4 r3 = getCol(3);
	rows[0] = r0;
	rows[1] = r1;
	rows[2] = r2;
	rows[3] = r3;
	return *this;
}

// ========================================================
// Mat4x4::setIdentity():
// ========================================================

Mat4x4 & Mat4x4::setIdentity()
{
	rows[0] = Vec4::unitX();
	rows[1] = Vec4::unitY();
	rows[2] = Vec4::unitZ();
	rows[3] = Vec4::unitW();
	return *this;
}

// ========================================================
// Mat4x4::rotationX():
// ========================================================

Mat4x4 Mat4x4::rotationX(const Radians radians)
{
	const float c = radians.cos();
	const float s = radians.sin();

	return Mat4x4(Vec4::unitX(),
	              Vec4(0.0f,  c, s, 0.0f),
	              Vec4(0.0f, -s, c, 0.0f),
	              Vec4::unitW());
}

// ========================================================
// Mat4x4::rotationY():
// ========================================================

Mat4x4 Mat4x4::rotationY(const Radians radians)
{
	const float c = radians.cos();
	const float s = radians.sin();

	return Mat4x4(Vec4( c, 0.0f, s, 0.0f),
	              Vec4::unitY(),
	              Vec4(-s, 0.0f, c, 0.0f),
	              Vec4::unitW());
}

// ========================================================
// Mat4x4::rotationZ():
// ========================================================

Mat4x4 Mat4x4::rotationZ(const Radians radians)
{
	const float c = radians.cos();
	const float s = radians.sin();

	return Mat4x4(Vec4( c, s, 0.0f, 0.0f),
	              Vec4(-s, c, 0.0f, 0.0f),
	              Vec4::unitZ(),
	              Vec4::unitW());
}

// ========================================================
// Mat4x4::rotationXYZ():
// ========================================================

Mat4x4 Mat4x4::rotationXYZ(const Radians rX, const Radians rY, const Radians rZ)
{
	const Mat4x4 matRx = Mat4x4::rotationX(rX);
	const Mat4x4 matRy = Mat4x4::rotationY(rY);
	const Mat4x4 matRz = Mat4x4::rotationZ(rZ);
	return matRx * matRy * matRz;
}

// ========================================================
// Mat4x4::lookAt():
// ========================================================

Mat4x4 Mat4x4::lookAt(const Vec3 & eye, const Vec3 & target, const Vec3 & upVector)
{
	// Left-haded, row-major look at matrix:
	//
	// | right.x, up.x, look.x, 0 |
	// | right.y, up.y, look.y, 0 |
	// | right.z, up.z, look.z, 0 |
	// |    A,     B,     C,    1 |
	//
	// Reference:
	//  http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/perspective-projections-in-lh-and-rh-systems-r3598

	const Vec3 look  = target - eye;
	const Vec3 right = upVector.cross(look);
	const Vec3 up    = look.cross(right);

	const float a = -right.dot(eye);
	const float b = -up.dot(eye);
	const float c = -look.dot(eye);

	return Mat4x4(Vec4(right.x, up.x, look.x, 0.0f),
	              Vec4(right.y, up.y, look.y, 0.0f),
	              Vec4(right.z, up.z, look.z, 0.0f),
	              Vec4(   a,     b,     c,    1.0f));
}

// ========================================================
// Mat4x4::perspectiveProjection():
// ========================================================

Mat4x4 Mat4x4::perspectiveProjection(const Radians fovY, const float aspect, const float zNear, const float zFar)
{
	// Standard projection matrix:
	//
	// | A, 0, 0, 0 |
	// | 0, B, 0, 0 |
	// | 0, 0, C, D |
	// | 0, 0, E, 0 |
	//
	// The values above, for a LH matrix:
	//
	// A = aspect * (1 / tan(fovY * 0.5))
	// B = 1 / tan(fovY * 0.5)
	// C = -(zFar + zNear) / (zFar - zNear)
	// D = 1
	// E = (2 * zFar * zNear) / (zFar - zNear)
	//
	// Also note that for OpenGL we must set the depth
	// func to GL_GEQUAL and the depth clear value to 0!
	//
	// Point to screen projection:
	//
	// screenX = (A * x) / (D * z)
	// screenY = (B * y) / (D * z)
	//
	// depth = (C * z + E * w) / (D * z)
	//
	// Reference:
	//  http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/perspective-projections-in-lh-and-rh-systems-r3598

	const float invFovTan = 1.0f / (fovY * 0.5f).tan();
	const float a =  (aspect * invFovTan);
	const float c = -(zFar + zNear) / (zFar - zNear);
	const float e =  (2.0f * zFar * zNear) / (zFar - zNear);

	return Mat4x4(Vec4(a,    0.0f,      0.0f, 0.0f),
	              Vec4(0.0f, invFovTan, 0.0f, 0.0f),
	              Vec4(0.0f, 0.0f,      c,    1.0f),
	              Vec4(0.0f, 0.0f,      e,    0.0f));
}

// ========================================================
// Mat4x4::multiply():
// ========================================================

Mat4x4 Mat4x4::multiply(const Mat4x4 & a, const Mat4x4 & b)
{
	// Algorithm:
	//  result(i,j) = a(i,k) . b(k,j)
	// or in other words:
	//  result(i,j) = dot(row(a, i), col(b, j));

	typedef float Vec4Ptr[4];
	Mat4x4 result;

	Vec4Ptr * R = reinterpret_cast<Vec4Ptr *>(&result);
	const Vec4Ptr * A = reinterpret_cast<const Vec4Ptr *>(&a);
	const Vec4Ptr * B = reinterpret_cast<const Vec4Ptr *>(&b);

	R[0][0] = (A[0][0] * B[0][0]) + (A[0][1] * B[1][0]) + (A[0][2] * B[2][0]) + (A[0][3] * B[3][0]);
	R[0][1] = (A[0][0] * B[0][1]) + (A[0][1] * B[1][1]) + (A[0][2] * B[2][1]) + (A[0][3] * B[3][1]);
	R[0][2] = (A[0][0] * B[0][2]) + (A[0][1] * B[1][2]) + (A[0][2] * B[2][2]) + (A[0][3] * B[3][2]);
	R[0][3] = (A[0][0] * B[0][3]) + (A[0][1] * B[1][3]) + (A[0][2] * B[2][3]) + (A[0][3] * B[3][3]);

	R[1][0] = (A[1][0] * B[0][0]) + (A[1][1] * B[1][0]) + (A[1][2] * B[2][0]) + (A[1][3] * B[3][0]);
	R[1][1] = (A[1][0] * B[0][1]) + (A[1][1] * B[1][1]) + (A[1][2] * B[2][1]) + (A[1][3] * B[3][1]);
	R[1][2] = (A[1][0] * B[0][2]) + (A[1][1] * B[1][2]) + (A[1][2] * B[2][2]) + (A[1][3] * B[3][2]);
	R[1][3] = (A[1][0] * B[0][3]) + (A[1][1] * B[1][3]) + (A[1][2] * B[2][3]) + (A[1][3] * B[3][3]);

	R[2][0] = (A[2][0] * B[0][0]) + (A[2][1] * B[1][0]) + (A[2][2] * B[2][0]) + (A[2][3] * B[3][0]);
	R[2][1] = (A[2][0] * B[0][1]) + (A[2][1] * B[1][1]) + (A[2][2] * B[2][1]) + (A[2][3] * B[3][1]);
	R[2][2] = (A[2][0] * B[0][2]) + (A[2][1] * B[1][2]) + (A[2][2] * B[2][2]) + (A[2][3] * B[3][2]);
	R[2][3] = (A[2][0] * B[0][3]) + (A[2][1] * B[1][3]) + (A[2][2] * B[2][3]) + (A[2][3] * B[3][3]);

	R[3][0] = (A[2][0] * B[0][0]) + (A[3][1] * B[1][0]) + (A[3][2] * B[2][0]) + (A[3][3] * B[3][0]);
	R[3][1] = (A[2][0] * B[0][1]) + (A[3][1] * B[1][1]) + (A[3][2] * B[2][1]) + (A[3][3] * B[3][1]);
	R[3][2] = (A[2][0] * B[0][2]) + (A[3][1] * B[1][2]) + (A[3][2] * B[2][2]) + (A[3][3] * B[3][2]);
	R[3][3] = (A[2][0] * B[0][3]) + (A[3][1] * B[1][3]) + (A[3][2] * B[2][3]) + (A[3][3] * B[3][3]);

	return result;
}

// ========================================================
// Mat4x4::transformPoint():
// ========================================================

Vec4 Mat4x4::transformPoint(const Vec3 & p, const Mat4x4 & m)
{
	typedef float Vec4Ptr[4];
	const Vec4Ptr * M = reinterpret_cast<const Vec4Ptr *>(&m);

	Vec4 result; // Dot product of the row vector with each column of the matrix.
	result.x = (M[0][0] * p.x) + (M[1][0] * p.y) + (M[2][0] * p.z) + M[3][0];
	result.y = (M[0][1] * p.x) + (M[1][1] * p.y) + (M[2][1] * p.z) + M[3][1];
	result.z = (M[0][2] * p.x) + (M[1][2] * p.y) + (M[2][2] * p.z) + M[3][2];
	result.w = (M[0][3] * p.x) + (M[1][3] * p.y) + (M[2][3] * p.z) + M[3][3];
	return result;
}

// ========================================================
// Mat4x4::transformPointAffine():
// ========================================================

Vec3 Mat4x4::transformPointAffine(const Vec3 & p, const Mat4x4 & m)
{
	typedef float Vec4Ptr[4];
	const Vec4Ptr * M = reinterpret_cast<const Vec4Ptr *>(&m);

	Vec3 result; // Dot product of the row vector with each column of the matrix (assume p.w is 1).
	result.x = (M[0][0] * p.x) + (M[1][0] * p.y) + (M[2][0] * p.z) + M[3][0];
	result.y = (M[0][1] * p.x) + (M[1][1] * p.y) + (M[2][1] * p.z) + M[3][1];
	result.z = (M[0][2] * p.x) + (M[1][2] * p.y) + (M[2][2] * p.z) + M[3][2];
	return result;
}

// ========================================================
// Mat4x4::transformVector():
// ========================================================

Vec4 Mat4x4::transformVector(const Vec4 & v, const Mat4x4 & m)
{
	typedef float Vec4Ptr[4];
	const Vec4Ptr * M = reinterpret_cast<const Vec4Ptr *>(&m);

	Vec4 result; // Dot product of the row vector with each column of the matrix.
	result.x = (M[0][0] * v.x) + (M[1][0] * v.y) + (M[2][0] * v.z) + (M[3][0] * v.w);
	result.y = (M[0][1] * v.x) + (M[1][1] * v.y) + (M[2][1] * v.z) + (M[3][1] * v.w);
	result.z = (M[0][2] * v.x) + (M[1][2] * v.y) + (M[2][2] * v.z) + (M[3][2] * v.w);
	result.w = (M[0][3] * v.x) + (M[1][3] * v.y) + (M[2][3] * v.z) + (M[3][3] * v.w);
	return result;
}

} // namespace core {}
} // namespace atlas {}
