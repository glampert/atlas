
// ================================================================================================
// -*- C++ -*-
// File: math_utils.hpp
// Author: Guilherme R. Lampert
// Created on: 09/01/15
// Brief: Miscellaneous math utilities and functions.
// ================================================================================================

#ifndef ATLAS_CORE_MATHS_MATH_UTILS_HPP
#define ATLAS_CORE_MATHS_MATH_UTILS_HPP

#include "atlas/core/utils/basic_types.hpp"
#include <cfloat>
#include <cmath>

namespace atlas
{
namespace core
{
namespace math
{

// ========================================================
// Frequently used mathematical constants:
// ========================================================

extern const float E;          // "Euler's constant" (base of the natural logarithm)
extern const float Pi;         // 4 * atan(1)
extern const float TwoPi;      // 2 * pi
extern const float HalfPi;     // pi / 2
extern const float InvPi;      // 1 / pi
extern const float InvTwoPi;   // 1 / (2 * pi)
extern const float LogOf2;     // ln(2)
extern const float LogOf10;    // ln(10)
extern const float InvLogOf2;  // 1 / ln(2)
extern const float InvLogOf10; // 1 / ln(10)
extern const float DegToRad;   // pi / 180
extern const float RadToDeg;   // 180 / pi
extern const float SecToMsec;  // Seconds to milliseconds multiplier.
extern const float MsecToSec;  // Milliseconds to seconds multiplier.
extern const float Infinity;   // Nothing lasts forever, except this number.
extern const float Epsilon;    // Tiny value for comparison error tolerance.

// ========================================================
// Trigonometry (all angles are in radians):
// ========================================================

// Sine of angle in radians.
ATTRIBUTE_PURE
inline float sin(const float angleRadians)
{
	return sinf(angleRadians);
}

// Cosine of angle in radians.
ATTRIBUTE_PURE
inline float cos(const float angleRadians)
{
	return cosf(angleRadians);
}

// Returns the arc sine of angle, in the interval [-pi/2,+pi/2] radians.
ATTRIBUTE_PURE
inline float asin(const float sine)
{
	if (sine <= -1.0f)
	{
		return -math::HalfPi;
	}
	if (sine >= 1.0f)
	{
		return math::HalfPi;
	}
	return asinf(sine);
}

// Returns the principal arc cosine of angle, in the interval [0,pi] radians.
ATTRIBUTE_PURE
inline float acos(const float cosine)
{
	if (cosine <= -1.0f)
	{
		return math::Pi;
	}
	if (cosine >= 1.0f)
	{
		return 0.0f;
	}
	return acosf(cosine);
}

// Tangent of angle in radians.
ATTRIBUTE_PURE
inline float tan(const float angleRadians)
{
	return tanf(angleRadians);
}

// Returns angle between [-pi/2,+pi/2] radians.
ATTRIBUTE_PURE
inline float atan(const float tangent)
{
	return atanf(tangent);
}

// Computes the arc tangent of y/x using the signs of arguments to determine
// the correct quadrant. Returns angle between [-pi,+pi] radians.
ATTRIBUTE_PURE
inline float atan(const float y, const float x)
{
	return atan2f(y, x);
}

// Computes the hypotenuse of x and y.
ATTRIBUTE_PURE
inline float hypotenuse(const float x, const float y)
{
	// hypotenuse(x, y) => h^2 = x^2 + y^2
	return sqrtf((x * x) + (y * y));
}

// ========================================================
// Miscellaneous floating point arithmetics:
// ========================================================

// Square root of x.
ATTRIBUTE_PURE
inline float sqrt(const float x)
{
	return sqrtf(x);
}

// Inverse square root (Reciprocal square root, I.E. 1 / sqrt(x)).
ATTRIBUTE_PURE
inline float invSqrt(const float x)
{
	return 1.0f / sqrtf(x);
}

// Returns a decimal value representing the smallest integer that is greater than or equal to x.
ATTRIBUTE_PURE
inline float ceiling(const float x)
{
	return ceilf(x);
}

// Returns a decimal value representing the largest integer that is less than or equal to x.
ATTRIBUTE_PURE
inline float floor(const float x)
{
	return floorf(x);
}

// Compute the fractional part of the argument. Same as `x - floor(x)`.
ATTRIBUTE_PURE
inline float fract(const float x)
{
	return x - floorf(x);
}

// Absolute value of floating point.
ATTRIBUTE_PURE
inline float abs(const float x)
{
	return fabsf(x);
}

// Absolute value of any integer type.
template<class T>
ATTRIBUTE_PURE T abs(const T x)
{
	return (x < T(0)) ? -x : x;
}

// Calculates e^x.
ATTRIBUTE_PURE
inline float exp(const float x)
{
	return expf(x);
}

// Returns the natural logarithm of x.
ATTRIBUTE_PURE
inline float ln(const float x)
{
	return logf(x);
}

// Returns the base 2 logarithm of x.
ATTRIBUTE_PURE
inline float log2(const float x)
{
	return math::InvLogOf2 * logf(x);
}

// Returns the base 10 logarithm of x.
ATTRIBUTE_PURE
inline float log10(const float x)
{
	return math::InvLogOf10 * logf(x);
}

// Decimal remainder of numerator/denominator (rounded towards zero). Remainder is: numerator - quotient * denominator.
ATTRIBUTE_PURE
inline float remainder(const float numerator, const float denominator)
{
	return fmodf(numerator, denominator);
}

// Get the sign of a number, I.E.: -1, 0, +1.
template<class T>
ATTRIBUTE_PURE T sign(const T x)
{
	return (x > T(0)) ? T(1) : (x < T(0)) ? T(-1) : T(0);
}

// Maps a numerical range [inMin, inMax] to the range [outMin, outMax].
template<class T>
ATTRIBUTE_PURE T mapRange(const T x, const T inMin, const T inMax, const T outMin, const T outMax)
{
	return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}

// Maps a scalar value within a range to the [0,1] normalized range.
template<class T>
ATTRIBUTE_PURE T normalizeValue(const T x, const T minimum, const T maximum)
{
	return (x - minimum) / (maximum - minimum);
}

// ========================================================
// Powers of two:
// ========================================================

// Test if an integer is a power of two. Always false if the number is negative.
template<class T>
ATTRIBUTE_PURE bool isPowerOfTwo(const T x)
{
	return (x > T(0)) && (x & (x - T(1))) == 0;
}

// Rounds an integer to its next power of two. Example: 37 => 64
template<class T>
ATTRIBUTE_PURE T nextPowerOfTwo(const T x)
{
	if (x == T(0))
	{
		return T(1);
	}
	--x;
	for (T i = T(1); i < sizeof(T) * 8; i <<= 1)
	{
		x = x | x >> i;
	}
	return ++x;
}

// Rounds an integer to its nearest power of two. Example: 37 => 32
template<class T>
ATTRIBUTE_PURE T nearestPowerOfTwo(const T x)
{
	if (x == T(0))
	{
		return T(1);
	}
	T k;
	for (k = sizeof(T) * 8 - 1; ((1 << k) & x) == 0; --k)
	{
	}
	if (((1 << (k - 1)) & x) == 0)
	{
		return 1 << k;
	}
	return 1 << (k + 1);
}

// Round down an integer to a power of two. The output is never greater then the input.
template<class T>
ATTRIBUTE_PURE T roundDownToPowerOfTwo(const T x)
{
	T p2;
	for (p2 = T(1); (p2 * 2) <= x; p2 <<= 1)
	{
	}
	return p2;
}

// ========================================================
// Interpolation functions:
// ========================================================

// Clamp x to [0,1].
template<class T>
ATTRIBUTE_PURE T saturate(const T x)
{
	return (x <= T(0)) ? T(0) : (x >= T(1)) ? T(1) : x;
}

// Generate a step function by comparing two values. Return is in [0,1] range.
template<class T>
ATTRIBUTE_PURE T step(const T edge, const T x)
{
	return (x < edge) ? T(0) : T(1);
}

// Linear interpolation. Does not clamp `t` to [0,1] range.
template<class T>
ATTRIBUTE_PURE T lerp(const T a, const T b, const T t)
{
	return a + t * (b - a);
}

// ========================================================
// Floating-point/numerical properties:
// ========================================================

// Checks if a decimal value is NAN (Not-a-Number).
ATTRIBUTE_PURE
inline bool isNaN(const float x)
{
	return isnan(x);
}

// Returns true if the passed number is a finite decimal number as opposed to INF, NAN, etc.
ATTRIBUTE_PURE
inline bool isFinite(const float x)
{
	return isfinite(x);
}

// Tests if an integer number is even.
template<class T>
ATTRIBUTE_PURE bool isEven(const T x)
{
	return (x & T(1)) == 0;
}

// Tests if an integer number is odd.
template<class T>
ATTRIBUTE_PURE bool isOdd(const T x)
{
	return (x & T(1)) != 0;
}

// Tests if an integer number is a prime number.
template<class T>
ATTRIBUTE_PURE bool isPrime(const T x)
{
	if (((!(x & 1)) && x != 2) || (x < 2) || (x % 3 == 0 && x != 3))
	{
		return false;
	}
	for (T k = T(1); (36 * k * k - 12 * k) < x; ++k)
	{
		if ((x % (6 * k + 1) == 0) || (x % (6 * k - 1) == 0))
		{
			return false;
		}
	}
	return true;
}

// ========================================================
// Floating-point comparison with a tolerance (epsilon):
// ========================================================

// val > 0
ATTRIBUTE_PURE
inline bool floatGreaterThanZero(const float val, const float tolerance = math::Epsilon)
{
	return val >= tolerance;
}

// a == b
ATTRIBUTE_PURE
inline bool floatEquals(const float a, const float b, const float tolerance = math::Epsilon)
{
	return math::abs(a - b) < tolerance;
}

// a < b
ATTRIBUTE_PURE
inline bool floatLessThan(const float a, const float b, const float tolerance = math::Epsilon)
{
	return (a - b) <= (-tolerance);
}

// a > b
ATTRIBUTE_PURE
inline bool floatGreaterThan(const float a, const float b, const float tolerance = math::Epsilon)
{
	return (a - b) >= tolerance;
}

// a <= b
ATTRIBUTE_PURE
inline bool floatLessThanOrEqual(const float a, const float b, const float tolerance = math::Epsilon)
{
	return (a - b) < tolerance;
}

// a >= b
ATTRIBUTE_PURE
inline bool floatGreaterThanOrEqual(const float a, const float b, const float tolerance = math::Epsilon)
{
	return (a - b) > (-tolerance);
}

} // namespace math {}
} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_MATHS_MATH_UTILS_HPP
