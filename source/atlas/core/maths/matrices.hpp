
// ================================================================================================
// -*- C++ -*-
// File: matrices.hpp
// Author: Guilherme R. Lampert
// Created on: 11/08/15
// Brief: 3x3 and 4x4 floating-point matrices, used for rendering. Row-major order.
// ================================================================================================

#ifndef ATLAS_CORE_MATHS_MATRICES_HPP
#define ATLAS_CORE_MATHS_MATRICES_HPP

#include "atlas/core/maths/vectors.hpp"
#include "atlas/core/maths/angles.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// class Mat3x3:
// ========================================================

// Row-major 3x3 rotation matrix of floats.
//
// Each member Vec3 is a row of the matrix.
// Might optionally also contain a (non)uniform scale.
//
// Positive rotations are Counter-Clock-Wise (CCW).
// Negative rotations are Clock-Wise (CW).
//
class Mat3x3 final
{
public:

	// Uninitialized matrix.
	Mat3x3();

	// Identity matrix (if diagonal = 1).
	explicit Mat3x3(float diagonal);

	// Copy array of 9 floats (in row-major order).
	explicit Mat3x3(const float * m);

	// Construct with explicit matrix rows.
	Mat3x3(const Vec3 & row0, const Vec3 & row1, const Vec3 & row2);

	// Grab a pointer to the first float of this matrix.
	float * getData();
	const float * getData() const;

	// Access a copy of one of the rows/columns.
	Vec3 getRow(uint index) const;
	Vec3 getCol(uint index) const;

	// Set a given row/column as a vector.
	void setRow(uint index, const Vec3 & row);
	void setCol(uint index, const Vec3 & col);

	// Row/column element access.
	float & operator () (uint row, uint col);
	float   operator () (uint row, uint col) const;

	// Access a row, which is a vector, thus allowing for m[i][j] style of access.
	Vec3 &       operator [] (uint row);
	const Vec3 & operator [] (uint row) const;

	// Multiply matrix * matrix and matrix * scalar.
	Mat3x3 & operator *= (float scalar);
	Mat3x3 & operator *= (const Mat3x3 & other);
	Mat3x3   operator *  (float scalar) const;
	Mat3x3   operator *  (const Mat3x3 & other) const;

	// Negate each element of the matrix.
	Mat3x3 operator - () const;

	// Swap rows with columns.
	Mat3x3 & transpose();
	Mat3x3   transposed() const;

	// Make this matrix the identity matrix.
	Mat3x3 & setIdentity();

	// Construct scaling matrices.
	static Mat3x3 scaling(float x, float y, float z);
	static Mat3x3 scaling(const Vec3 & xyzScales);

	// Construct rotation matrices.
	// Positive rotation is Counter-Clock-Wise (CCW).
	// Negative rotation is Clock-Wise (CW).
	static Mat3x3 rotationX(Radians radians);
	static Mat3x3 rotationY(Radians radians);
	static Mat3x3 rotationZ(Radians radians);
	static Mat3x3 rotationXYZ(Radians rX, Radians rY, Radians rZ);

	// Multiply (or combine) two matrices.
	static Mat3x3 multiply(const Mat3x3 & a, const Mat3x3 & b);

	// Multiply the 3D vector/point by the matrix, applying rotation and scaling.
	// Vector is multiplied as a row vector, thus the row vector multiplies by
	// each column of the matrix, from left to right.
	static Vec3 transform(const Vec3 & p, const Mat3x3 & m);

private:

	Vec3 rows[3];
};

// ========================================================
// class Mat4x4:
// ========================================================

// Row-major homogeneous 4x4 matrix of floats.
//
// View matrices/coordinate spaces are always left-handed (LH), the
// +X, +Y, and +Z directions point right, up, and forward, respectively.
//
//  +Y   +Z
//   |   /
//   |  /
//   | /
//   + ------ +X
//
// Each member Vec4 is a row of the matrix. Vectors/points also
// multiply as row-vectors, from left to right. E.g.: 'vector * matrix'.
//
//                    | X, Y, Z, W |  ->  row[0]
// V = [X, Y, Z, W] . | X, Y, Z, W |  ->  row[1]
//                    | X, Y, Z, W |  ->  row[2]
//                    | X, Y, Z, W |  ->  row[3]
//
// The upper 3x3 sub-matrix is the rotation component. The translation
// component is comprised of the bottom row of the matrix. The diagonal
// also represents scaling in the X, Y and Z axes.
//
// | Sx, Ry, Rz, 0 |
// | Rx, Sy, Rz, 0 |
// | Rx, Ry, Sz, 0 |
// | Tx, Ty  Tz, 1 |
//
// Positive rotations are Counter-Clock-Wise (CCW).
// Negative rotations are Clock-Wise (CW).
//
class Mat4x4 final
{
public:

	// Uninitialized matrix.
	Mat4x4();

	// Identity matrix (if diagonal = 1).
	explicit Mat4x4(float diagonal);

	// Copy array of 16 floats (in row-major order).
	explicit Mat4x4(const float * m);

	// Construct with explicit matrix rows.
	Mat4x4(const Vec4 & row0, const Vec4 & row1,
	       const Vec4 & row2, const Vec4 & row3);

	// Construct from an upper 3x3 sub-matrix (rotation/scale) and a translation vector.
	Mat4x4(const Mat3x3 & upper3x3, const Vec3 & translationVec = Vec3::origin());

	// Grab a pointer to the first float of this matrix.
	float * getData();
	const float * getData() const;

	// Access a copy of one of the rows/columns.
	Vec4 getRow(uint index) const;
	Vec4 getCol(uint index) const;

	// Set a given row/column as a vector.
	void setRow(uint index, const Vec4 & row);
	void setCol(uint index, const Vec4 & col);

	// Get/set the bottom row of this matrix, which is the translation component.
	Vec4 getTranslation() const;
	void setTranslation(const Vec4 & t);

	// Get/set the upper 3x3 sub-matrix, which is the rotation and scale components.
	Mat3x3 getUpper3x3() const;
	void setUpper3x3(const Mat3x3 & m3x3);

	// Row/column element access.
	float & operator () (uint row, uint col);
	float   operator () (uint row, uint col) const;

	// Access a row, which is a vector, thus allowing for m[i][j] style of access.
	Vec4 &       operator [] (uint row);
	const Vec4 & operator [] (uint row) const;

	// Multiply matrix * matrix and matrix * scalar.
	Mat4x4 & operator *= (float scalar);
	Mat4x4 & operator *= (const Mat4x4 & other);
	Mat4x4   operator *  (float scalar) const;
	Mat4x4   operator *  (const Mat4x4 & other) const;

	// Negate each element of the matrix.
	Mat4x4 operator - () const;

	// Swap rows with columns.
	Mat4x4 & transpose();
	Mat4x4   transposed() const;

	// Make this matrix the identity matrix.
	Mat4x4 & setIdentity();

	// Construct homogeneous translation matrices.
	static Mat4x4 translation(float x, float y, float z);
	static Mat4x4 translation(const Vec3 & xyzOffsets);

	// Construct homogeneous scale matrices.
	static Mat4x4 scaling(float x, float y, float z);
	static Mat4x4 scaling(const Vec3 & xyzScales);

	// Construct homogeneous rotation matrices.
	// Positive rotation is Counter-Clock-Wise (CCW).
	// Negative rotation is Clock-Wise (CW).
	static Mat4x4 rotationX(Radians radians);
	static Mat4x4 rotationY(Radians radians);
	static Mat4x4 rotationZ(Radians radians);
	static Mat4x4 rotationXYZ(Radians rX, Radians rY, Radians rZ);

	// Left-handed look-at view/camera matrix. Up vector is normally the unit Y axis.
	static Mat4x4 lookAt(const Vec3 & eye, const Vec3 & target, const Vec3 & upVector);

	// Left-handed perspective projection matrix.
	static Mat4x4 perspectiveProjection(Radians fovY, float aspect, float zNear, float zFar);

	// Multiply (or combine) two matrices.
	static Mat4x4 multiply(const Mat4x4 & a, const Mat4x4 & b);

	// Multiply the 3D point by the matrix, transforming it. Returns a 4D vector.
	static Vec4 transformPoint(const Vec3 & p, const Mat4x4 & m);

	// Multiply the 3D point by the matrix. Assumes w = 1 and the last column of the matrix is padding.
	static Vec3 transformPointAffine(const Vec3 & p, const Mat4x4 & m);

	// Multiply the homogeneous 4D vector with the given matrix, as a row vector, from left to right.
	static Vec4 transformVector(const Vec4 & v, const Mat4x4 & m);

private:

	Vec4 rows[4];
};

// ================================================================================================
// Mat3x3 inline methods:
// ================================================================================================

inline Mat3x3::Mat3x3()
{
	// Left uninitialized.
}

inline Mat3x3::Mat3x3(const float diagonal)
{
	rows[0].set(diagonal, 0.0f, 0.0f);
	rows[1].set(0.0f, diagonal, 0.0f);
	rows[2].set(0.0f, 0.0f, diagonal);
}

inline Mat3x3::Mat3x3(const float * m)
{
	DEBUG_CHECK(m != nullptr);
	std::memcpy(this, m, sizeof(Mat3x3));
}

inline Mat3x3::Mat3x3(const Vec3 & row0, const Vec3 & row1, const Vec3 & row2)
{
	rows[0] = row0;
	rows[1] = row1;
	rows[2] = row2;
}

inline float * Mat3x3::getData()
{
	return reinterpret_cast<float *>(this);
}

inline const float * Mat3x3::getData() const
{
	return reinterpret_cast<const float *>(this);
}

inline Vec3 Mat3x3::getRow(const uint index) const
{
	DEBUG_CHECK(index < 3);
	return rows[index];
}

inline Vec3 Mat3x3::getCol(const uint index) const
{
	DEBUG_CHECK(index < 3);
	return Vec3(rows[0][index], rows[1][index], rows[2][index]);
}

inline void Mat3x3::setRow(const uint index, const Vec3 & row)
{
	DEBUG_CHECK(index < 3);
	rows[index] = row;
}

inline void Mat3x3::setCol(const uint index, const Vec3 & col)
{
	DEBUG_CHECK(index < 3);
	rows[0][index] = col.x;
	rows[1][index] = col.y;
	rows[2][index] = col.z;
}

inline float & Mat3x3::operator () (const uint row, const uint col)
{
	DEBUG_CHECK(row < 3);
	DEBUG_CHECK(col < 3);
	return rows[row][col];
}

inline float Mat3x3::operator () (const uint row, const uint col) const
{
	DEBUG_CHECK(row < 3);
	DEBUG_CHECK(col < 3);
	return rows[row][col];
}

inline Vec3 & Mat3x3::operator [] (const uint row)
{
	DEBUG_CHECK(row < 3);
	return rows[row];
}

inline const Vec3 & Mat3x3::operator [] (const uint row) const
{
	DEBUG_CHECK(row < 3);
	return rows[row];
}

inline Mat3x3 & Mat3x3::operator *= (const float scalar)
{
	rows[0] *= scalar;
	rows[1] *= scalar;
	rows[2] *= scalar;
	return *this;
}

inline Mat3x3 & Mat3x3::operator *= (const Mat3x3 & other)
{
	*this = multiply(*this, other);
	return *this;
}

inline Mat3x3 Mat3x3::operator * (const float scalar) const
{
	return Mat3x3(rows[0] * scalar, rows[1] * scalar, rows[2] * scalar);
}

inline Mat3x3 Mat3x3::operator * (const Mat3x3 & other) const
{
	return multiply(*this, other);
}

inline Mat3x3 Mat3x3::operator - () const
{
	return Mat3x3(-rows[0], -rows[1], -rows[2]);
}

inline Mat3x3 Mat3x3::transposed() const
{
	return Mat3x3(getCol(0), getCol(1), getCol(2));
}

inline Mat3x3 & Mat3x3::setIdentity()
{
	rows[0] = Vec3::unitX();
	rows[1] = Vec3::unitY();
	rows[2] = Vec3::unitZ();
	return *this;
}

inline Mat3x3 Mat3x3::scaling(const float x, const float y, const float z)
{
	return Mat3x3(Vec3(x, 0.0f, 0.0f),
	              Vec3(0.0f, y, 0.0f),
	              Vec3(0.0f, 0.0f, z));
}

inline Mat3x3 Mat3x3::scaling(const Vec3 & xyzScales)
{
	return Mat3x3(Vec3(xyzScales.x, 0.0f, 0.0f),
	              Vec3(0.0f, xyzScales.y, 0.0f),
	              Vec3(0.0f, 0.0f, xyzScales.z));
}

inline Mat3x3 operator * (const float scalar, const Mat3x3 & m)
{
	return Mat3x3(m[0] * scalar, m[1] * scalar, m[2] * scalar);
}

inline String toString(const Mat3x3 & m)
{
	String str = "{ ";
	for (int row = 0; row < 3; ++row)
	{
		str += toString(m.getRow(row));
		if (row != 2) { str += ", "; }
	}
	str += " }";
	return str;
}

// ================================================================================================
// Mat4x4 inline methods:
// ================================================================================================

inline Mat4x4::Mat4x4()
{
	// Left uninitialized.
}

inline Mat4x4::Mat4x4(const float diagonal)
{
	rows[0].set(diagonal, 0.0f, 0.0f, 0.0f);
	rows[1].set(0.0f, diagonal, 0.0f, 0.0f);
	rows[2].set(0.0f, 0.0f, diagonal, 0.0f);
	rows[3].set(0.0f, 0.0f, 0.0f, diagonal);
}

inline Mat4x4::Mat4x4(const float * m)
{
	DEBUG_CHECK(m != nullptr);
	std::memcpy(this, m, sizeof(Mat4x4));
}

inline Mat4x4::Mat4x4(const Vec4 & row0, const Vec4 & row1,
                      const Vec4 & row2, const Vec4 & row3)
{
	rows[0] = row0;
	rows[1] = row1;
	rows[2] = row2;
	rows[3] = row3;
}

inline Mat4x4::Mat4x4(const Mat3x3 & upper3x3, const Vec3 & translationVec)
{
	rows[0].setXYZ(upper3x3[0], 0.0f);
	rows[1].setXYZ(upper3x3[1], 0.0f);
	rows[2].setXYZ(upper3x3[2], 0.0f);
	rows[3].setXYZ(translationVec, 1.0f);
}

inline float * Mat4x4::getData()
{
	return reinterpret_cast<float *>(this);
}

inline const float * Mat4x4::getData() const
{
	return reinterpret_cast<const float *>(this);
}

inline Vec4 Mat4x4::getRow(const uint index) const
{
	DEBUG_CHECK(index < 4);
	return rows[index];
}

inline Vec4 Mat4x4::getCol(const uint index) const
{
	DEBUG_CHECK(index < 4);
	return Vec4(rows[0][index],
	            rows[1][index],
	            rows[2][index],
	            rows[3][index]);
}

inline void Mat4x4::setRow(const uint index, const Vec4 & row)
{
	DEBUG_CHECK(index < 4);
	rows[index] = row;
}

inline void Mat4x4::setCol(const uint index, const Vec4 & col)
{
	DEBUG_CHECK(index < 4);
	rows[0][index] = col.x;
	rows[1][index] = col.y;
	rows[2][index] = col.z;
	rows[3][index] = col.w;
}

inline Vec4 Mat4x4::getTranslation() const
{
	return rows[3];
}

inline void Mat4x4::setTranslation(const Vec4 & t)
{
	rows[3] = t;
}

inline Mat3x3 Mat4x4::getUpper3x3() const
{
	return Mat3x3(rows[0].getVec3(),
	              rows[1].getVec3(),
	              rows[2].getVec3());
}

inline void Mat4x4::setUpper3x3(const Mat3x3 & m3x3)
{
	rows[0].setXYZ(m3x3[0]);
	rows[1].setXYZ(m3x3[1]);
	rows[2].setXYZ(m3x3[2]);
}

inline Mat4x4 Mat4x4::transposed() const
{
	return Mat4x4(getCol(0),
	              getCol(1),
	              getCol(2),
	              getCol(3));
}

inline Mat4x4 Mat4x4::translation(const float x, const float y, const float z)
{
	return Mat4x4(Vec4::unitX(),
	              Vec4::unitY(),
	              Vec4::unitZ(),
	              Vec4(x, y, z, 1.0f));
}

inline Mat4x4 Mat4x4::translation(const Vec3 & xyzOffsets)
{
	return Mat4x4(Vec4::unitX(),
	              Vec4::unitY(),
	              Vec4::unitZ(),
	              Vec4(xyzOffsets, 1.0f));
}

inline Mat4x4 Mat4x4::scaling(const float x, const float y, const float z)
{
	return Mat4x4(Vec4(x, 0.0f, 0.0f, 0.0f),
	              Vec4(0.0f, y, 0.0f, 0.0f),
	              Vec4(0.0f, 0.0f, z, 0.0f),
	              Vec4::unitW());
}

inline Mat4x4 Mat4x4::scaling(const Vec3 & xyzScales)
{
	return Mat4x4(Vec4(xyzScales.x, 0.0f, 0.0f, 0.0f),
	              Vec4(0.0f, xyzScales.y, 0.0f, 0.0f),
	              Vec4(0.0f, 0.0f, xyzScales.z, 0.0f),
	              Vec4::unitW());
}

inline float & Mat4x4::operator () (const uint row, const uint col)
{
	DEBUG_CHECK(row < 4);
	DEBUG_CHECK(col < 4);
	return rows[row][col];
}

inline float Mat4x4::operator () (const uint row, const uint col) const
{
	DEBUG_CHECK(row < 4);
	DEBUG_CHECK(col < 4);
	return rows[row][col];
}

inline Vec4 & Mat4x4::operator [] (const uint row)
{
	DEBUG_CHECK(row < 4);
	return rows[row];
}

inline const Vec4 & Mat4x4::operator [] (const uint row) const
{
	DEBUG_CHECK(row < 4);
	return rows[row];
}

inline Mat4x4 & Mat4x4::operator *= (const float scalar)
{
	rows[0] *= scalar;
	rows[1] *= scalar;
	rows[2] *= scalar;
	rows[3] *= scalar;
	return *this;
}

inline Mat4x4 & Mat4x4::operator *= (const Mat4x4 & other)
{
	*this = multiply(*this, other);
	return *this;
}

inline Mat4x4 Mat4x4::operator * (const float scalar) const
{
	return Mat4x4(rows[0] * scalar,
	              rows[1] * scalar,
	              rows[2] * scalar,
	              rows[3] * scalar);
}

inline Mat4x4 Mat4x4::operator * (const Mat4x4 & other) const
{
	return multiply(*this, other);
}

inline Mat4x4 Mat4x4::operator - () const
{
	return Mat4x4(-rows[0],
	              -rows[1],
	              -rows[2],
	              -rows[3]);
}

inline Vec4 operator * (const Vec3 & p, const Mat4x4 & m)
{
	return Mat4x4::transformPoint(p, m);
}

inline Vec4 operator * (const Vec4 & v, const Mat4x4 & m)
{
	return Mat4x4::transformVector(v, m);
}

inline Mat4x4 operator * (const float scalar, const Mat4x4 & m)
{
	return Mat4x4(m[0] * scalar,
	              m[1] * scalar,
	              m[2] * scalar,
	              m[3] * scalar);
}

inline String toString(const Mat4x4 & m)
{
	String str = "{ ";
	for (int row = 0; row < 4; ++row)
	{
		str += toString(m.getRow(row));
		if (row != 3) { str += ", "; }
	}
	str += " }";
	return str;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_MATHS_MATRICES_HPP
