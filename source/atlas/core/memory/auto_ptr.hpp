
// ================================================================================================
// -*- C++ -*-
// File: auto_ptr.hpp
// Author: Guilherme R. Lampert
// Created on: 08/09/15
// Brief: Equivalent of C++'s std::auto_ptr. Supports different storage policies.
// ================================================================================================

#ifndef ATLAS_CORE_MEMORY_AUTO_PTR_HPP
#define ATLAS_CORE_MEMORY_AUTO_PTR_HPP

#include "atlas/core/utils/basic_types.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// struct AutoPtrObjectStorage<T>:
// ========================================================

// Single object storage policy for AutoPtr. Frees with operator delete.
template<class T>
struct AutoPtrObjectStorage
{
	static void destroy(T * obj)
	{
		delete obj;
	}
};

// ========================================================
// struct AutoPtrArrayStorage<T>:
// ========================================================

// Array of objects storage policy for AutoPtr. Frees with operator delete[].
template<class T>
struct AutoPtrArrayStorage
{
	static void destroy(T * array)
	{
		delete[] array;
	}
};

// ========================================================
// struct AutoPtrRef<T>:
// ========================================================

// A wrapper type to provide AutoPtr with reference semantics.
// For example, an AutoPtr can be assigned (or constructed from)
// the result of a function which returns an AutoPtr by value.
template<class T>
struct AutoPtrRef
{
	explicit AutoPtrRef(T * p) : pointee(p) { }
	T * pointee;
};

// ========================================================
// template class AutoPtr<T, StoragePolicy>:
// ========================================================

// A simple smart pointer providing strict ownership semantics.
//
// This is basically an extended std::auto_ptr, which adds
// comparison operators and some other small modifications.
//
// An AutoPtr owns the object it holds a pointer to.
// Copying an AutoPtr copies the pointer and transfers ownership to the
// destination (destructive copy). If more than one AutoPtr owns the same
// object at the same time the behavior of the program is unpredictable.
//
// The uses of AutoPtr include providing temporary
// exception-safety for dynamically allocated memory, passing
// ownership of dynamically allocated memory to a function, and
// returning dynamically allocated memory from a function.
//
template
<
	class T,
	template<class> class StoragePolicy = AutoPtrObjectStorage
>
class AutoPtr final
	: public StoragePolicy<T>
{
	// Helper to allow safe "if (ptr)..." style checks.
	class Tester
	{
		void operator delete (void *);
	};

public:

	//
	// Construction:
	//

	explicit AutoPtr(T * p = nullptr)
		: pointee(p)
	{ }

	AutoPtr(AutoPtr & ptr)
		: pointee(giveUpOwnership(ptr))
	{ }

	template<class U, template<class> class SP>
	AutoPtr(AutoPtr<U, SP> & ptr)
		: pointee(giveUpOwnership(ptr))
	{ }

	//
	// Assignment:
	//

	AutoPtr & operator = (AutoPtr & ptr)
	{
		reset(*this, giveUpOwnership(ptr));
		return *this;
	}

	template<class U, template<class> class SP>
	AutoPtr & operator = (AutoPtr<U, SP> & ptr)
	{
		reset(*this, giveUpOwnership(ptr));
		return *this;
	}

	//
	// Checked dereference operators:
	//

	T & operator * ()
	{
		DEBUG_CHECK(pointee != nullptr && "Null pointer dereference!");
		return *pointee;
	}

	const T & operator * () const
	{
		DEBUG_CHECK(pointee != nullptr && "Null pointer dereference!");
		return *pointee;
	}

	T * operator -> ()
	{
		DEBUG_CHECK(pointee != nullptr && "Null pointer access!");
		return pointee;
	}

	const T * operator -> () const
	{
		DEBUG_CHECK(pointee != nullptr && "Null pointer access!");
		return pointee;
	}

	T & operator [] (const std::size_t index)
	{
		DEBUG_CHECK(pointee != nullptr && "Null pointer access!");
		return pointee[index]; // No way to bounds check since we are unaware of the size!
	}

	const T & operator [] (const std::size_t index) const
	{
		DEBUG_CHECK(pointee != nullptr && "Null pointer access!");
		return pointee[index]; // No way to bounds check since we are unaware of the size!
	}

	//
	// Comparison operators:
	//

	// Allows a check such as "if (!ptr) ..."
	bool operator ! () const
	{
		return pointee == nullptr;
	}

	// Allows safe and correct "if (ptr)..." checks.
	operator Tester*() const
	{
		if (!pointee)
		{
			return nullptr;
		}
		static Tester tester;
		return &tester;
	}

	template<class U, template<class> class SP>
	bool operator == (const AutoPtr<U, SP> & rhs) const
	{
		return pointee == rhs.pointee;
	}

	template<class U, template<class> class SP>
	bool operator != (const AutoPtr<U, SP> & rhs) const
	{
		return pointee != rhs.pointee;
	}

	// When the AutoPtr goes out of scope, the object it owns is deleted.
	// If it no longer owns anything (i.e. pointee is null), then this has no effect.
	~AutoPtr()
	{
		this->destroy(pointee);
	}

	//
	// Friend methods:
	//

	template<class U, template<class> class SP>
	friend void swap(AutoPtr<U, SP> & lhs, AutoPtr<U, SP> & rhs)
	{
		if (lhs.pointee != rhs.pointee)
		{
			U * temp    = lhs.pointee;
			lhs.pointee = rhs.pointee;
			rhs.pointee = temp;
		}
	}

	template<class U, template<class> class SP>
	friend U * giveUpOwnership(AutoPtr<U, SP> & ptr)
	{
		U * temp = ptr.pointee;
		ptr.pointee = nullptr;
		return temp;
	}

	template<class U, template<class> class SP>
	friend U *& toRawPointerRef(AutoPtr<U, SP> & ptr)
	{
		return ptr.pointee;
	}

	template<class U, template<class> class SP>
	friend U * toRawPointer(AutoPtr<U, SP> & ptr)
	{
		return ptr.pointee;
	}

	template<class U, template<class> class SP>
	friend const U * toRawPointer(const AutoPtr<U, SP> & ptr)
	{
		return ptr.pointee;
	}

	template<class U, template<class> class SP>
	friend void reset(AutoPtr<U, SP> & ptr, T * p = nullptr)
	{
		// Forcibly deletes the managed object and resets it.
		if (p != ptr.pointee)
		{
			ptr.destroy(ptr.pointee);
			ptr.pointee = p;
		}
	}

	//
	// Automatic conversions:
	//

	/*
	 * These operations convert an AutoPtr into and from an AutoPtrRef
	 * automatically as needed. This allows constructs such as:
	 *
	 *   AutoPtr<Derived> func_returning_AutoPtr(...) { ... }
	 *   ...
	 *   AutoPtr<Base> ptr = func_returning_AutoPtr(...);
	 */

	AutoPtr(AutoPtrRef<T> ptrRef)
		: pointee(ptrRef.pointee)
	{ }

	AutoPtr & operator = (AutoPtrRef<T> ptrRef)
	{
		if (ptrRef.pointee != pointee)
		{
			this->destroy(pointee);
			pointee = ptrRef.pointee;
		}
		return *this;
	}

	template<class U> operator AutoPtrRef<U>()
	{
		return AutoPtrRef<U>(giveUpOwnership(*this));
	}

	template<class U, template<class> class SP> operator AutoPtr<U, SP>()
	{
		return AutoPtr<U, SP>(giveUpOwnership(*this));
	}

private:

	T * pointee;
};

// ================================================================================================

// Disable all operation on void pointers.
template<> class AutoPtr<void, AutoPtrObjectStorage> { };

//
// Solve compile time ambiguity problems with operators == and !=
//
template<class T, template<class> class SP, class U>
bool operator == (const AutoPtr<T, SP> & lhs, U * rhs)
{
	return toRawPointer(lhs) == rhs;
}

template<class T, template<class> class SP, class U>
bool operator == (U * lhs, const AutoPtr<T, SP> & rhs)
{
	return rhs == lhs;
}

template<class T, template<class> class SP, class U>
bool operator != (const AutoPtr<T, SP> & lhs, U * rhs)
{
	return !(lhs == rhs);
}

template<class T, template<class> class SP, class U>
bool operator != (U * lhs, const AutoPtr<T, SP> & rhs)
{
	return rhs != lhs;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_MEMORY_AUTO_PTR_HPP
