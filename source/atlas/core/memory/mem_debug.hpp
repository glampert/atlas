
// ================================================================================================
// -*- C++ -*-
// File: mem_debug.hpp
// Author: Guilherme R. Lampert
// Created on: 19/04/15
// Brief: Memory debugging helpers and tools.
// ================================================================================================

#ifndef ATLAS_CORE_MEMORY_MEM_DEBUG_HPP
#define ATLAS_CORE_MEMORY_MEM_DEBUG_HPP

#include "atlas/core/utils/basic_types.hpp"
#include <cstring>

namespace atlas
{
namespace core
{

//
// Byte patterns used as fill value for
// debug allocations/deallocations.
//
constexpr ubyte MemAllocedPattern   = 0xA7;
constexpr ubyte MemFreedPattern     = 0xFE;
constexpr ubyte MemDestroyedPattern = 0xDE;

//
// `M_DBG_*` stands for 'Memory Debug'.
//
#define M_DBG_FILL_WITH_ALLOCED_PATTERN(ptr, sizeBytes)   std::memset((ptr), MemAllocedPattern,   (sizeBytes))
#define M_DBG_FILL_WITH_FREED_PATTERN(ptr, sizeBytes)     std::memset((ptr), MemFreedPattern,     (sizeBytes))
#define M_DBG_FILL_WITH_DESTROYED_PATTERN(ptr, sizeBytes) std::memset((ptr), MemDestroyedPattern, (sizeBytes))

// TODO add compiler switches to disable the above macros on release setting!!!

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_MEMORY_MEM_DEBUG_HPP
