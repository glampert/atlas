
// ================================================================================================
// -*- C++ -*-
// File: object_pool.hpp
// Author: Guilherme R. Lampert
// Created on: 14/08/15
// Brief: Custom generic pool allocator.
// ================================================================================================

#ifndef ATLAS_CORE_MEMORY_OBJECT_POOL_HPP
#define ATLAS_CORE_MEMORY_OBJECT_POOL_HPP

#include "atlas/core/utils/basic_types.hpp"
#include "atlas/core/memory/mem_debug.hpp"

#include <new>
#include <cstring>

#if ATLAS_COMPILER_HAS_CPP11
	#include <utility>
#endif // ATLAS_COMPILER_HAS_CPP11

namespace atlas
{
namespace core
{

// ========================================================
// class ObjectPool<T, Granularity>
// ========================================================

// Pool of fixed-size memory blocks (similar to a list of arrays).
//
// This pool allocator operates as a linked list of small arrays.
// Each array is a pool of blocks with the size of 'T' template parameter.
// Template parameter `Granularity` defines the size in objects of type 'T'
// of such arrays.
//
// `allocate()` will return an uninitialized memory block.
// The user is responsible for calling `construct()` on it to run class
// constructors if necessary, and `destroy()` to call class destructor
// before deallocating the block.
template
<
	class T,
	uint Granularity
>
class ObjectPool final
	: private NonCopyable
{
public:

	 ObjectPool(); // Empty pool; no allocation until first use.
	~ObjectPool(); // Drains the pool.

	// Allocates a single memory block of size 'T' and
	// returns an uninitialized pointer to it.
	T * allocate();

	// Deallocates a memory block previously allocated by `allocate()`.
	// Pointer may be null, in which case this is a no-op. NOTE: Class destructor NOT called!
	void deallocate(void * ptr);

	// Frees all blocks, reseting the pool allocator to its initial state.
	// Warning: Calling this method will invalidate any memory block still
	// alive that was previously allocated from this pool.
	void drain();

	// Calls constructor for `obj`, using placement new.
	static void construct(T * obj, const T & val);
	template<class U, class... Args> static void construct(U * obj, Args&&... args);

	// Calls destructor for `obj`.
	static void destroy(T * obj);
	template<class U> static void destroy(U * obj);

	// Miscellaneous stats queries:
	uint getTotalAllocs()  const;
	uint getTotalFrees()   const;
	uint getObjectsAlive() const;
	uint getGranularity()  const;
	uint getSize()         const;

private:

	union PoolObj
	{
		ALIGN_AS(T, ubyte userData[sizeof(T)]);
		PoolObj * next;
	};

	struct PoolBlock
	{
		PoolObj objects[Granularity];
		PoolBlock * next;
	};

	PoolBlock * blockList;      // List of all blocks/pools.
	PoolObj   * freeList;       // List of free objects that can be recycled.
	uint        allocCount;     // Total calls to `allocate()`.
	uint        objectCount;    // User objects ('T' instances) currently active.
	uint        poolBlockCount; // Size in blocks of the `blockList`.
};

// ================================================================================================
// ObjectPool inline methods:
// ================================================================================================

template<class T, uint Granularity>
ObjectPool<T, Granularity>::ObjectPool()
	: blockList(nullptr)
	, freeList(nullptr)
	, allocCount(0)
	, objectCount(0)
	, poolBlockCount(0)
{
	// Allocates memory when the first object is requested.
}

template<class T, uint Granularity>
ObjectPool<T, Granularity>::~ObjectPool()
{
	drain();
}

template<class T, uint Granularity>
T * ObjectPool<T, Granularity>::allocate()
{
	if (freeList == nullptr)
	{
		PoolBlock * newBlock = new PoolBlock();
		newBlock->next = blockList;
		blockList = newBlock;

		++poolBlockCount;

		// All objects in the new pool block are appended
		// to the free list, since they are ready to be used.
		for (uint i = 0; i < Granularity; ++i)
		{
			newBlock->objects[i].next = freeList;
			freeList = &newBlock->objects[i];
		}
	}

	++allocCount;
	++objectCount;

	// Fetch one from the free list's head:
	PoolObj * object = freeList;
	freeList = freeList->next;

	// Initializing the object with a known pattern
	// to help detecting memory errors.
	M_DBG_FILL_WITH_ALLOCED_PATTERN(object, sizeof(*object));

	return reinterpret_cast<T *>(object);
}

template<class T, uint Granularity>
void ObjectPool<T, Granularity>::deallocate(void * ptr)
{
	DEBUG_CHECK(objectCount != 0);
	if (ptr == nullptr)
	{
		return;
	}

	// Fill user portion with a known pattern to help
	// detecting post deallocation usage attempts.
	M_DBG_FILL_WITH_FREED_PATTERN(ptr, sizeof(PoolObj));

	// Add back to free list's head. Memory not actually freed now.
	PoolObj * object = reinterpret_cast<PoolObj *>(ptr);
	object->next = freeList;
	freeList = object;

	--objectCount;
}

template<class T, uint Granularity>
void ObjectPool<T, Granularity>::drain()
{
	while (blockList != nullptr)
	{
		PoolBlock * block = blockList;
		blockList = blockList->next;
		delete block;
	}

	blockList      = nullptr;
	freeList       = nullptr;
	allocCount     = 0;
	objectCount    = 0;
	poolBlockCount = 0;
}

template<class T, uint Granularity>
uint ObjectPool<T, Granularity>::getTotalAllocs() const
{
	return allocCount;
}

template<class T, uint Granularity>
uint ObjectPool<T, Granularity>::getTotalFrees() const
{
	return allocCount - objectCount;
}

template<class T, uint Granularity>
uint ObjectPool<T, Granularity>::getObjectsAlive() const
{
	return objectCount;
}

template<class T, uint Granularity>
uint ObjectPool<T, Granularity>::getGranularity() const
{
	return Granularity;
}

template<class T, uint Granularity>
uint ObjectPool<T, Granularity>::getSize() const
{
	return poolBlockCount;
}

template<class T, uint Granularity>
void ObjectPool<T, Granularity>::construct(T * obj, const T & val)
{
	::new(obj) T(val);
}

template<class T, uint Granularity>
template<class U, class... Args>
void ObjectPool<T, Granularity>::construct(U * obj, Args&&... args)
{
	::new(obj) U(std::forward<Args>(args)...);
}

template<class T, uint Granularity>
void ObjectPool<T, Granularity>::destroy(T * obj)
{
	DEBUG_CHECK(obj != nullptr);
	obj->~T();

	// When memory debugging is enable, this fills the memory formerly
	// used by the object with junk. This can make it easier detecting
	// cases when an attempt to reuse an object that was already destroyed is made.
	M_DBG_FILL_WITH_DESTROYED_PATTERN(obj, sizeof(*obj));
}

template<class T, uint Granularity>
template<class U>
void ObjectPool<T, Granularity>::destroy(U * obj)
{
	DEBUG_CHECK(obj != nullptr);
	obj->~U();

	M_DBG_FILL_WITH_DESTROYED_PATTERN(obj, sizeof(*obj));
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_MEMORY_OBJECT_POOL_HPP
