
// ================================================================================================
// -*- C++ -*-
// File: mem_utils.hpp
// Author: Guilherme R. Lampert
// Created on: 19/04/15
// Brief: Miscellaneous memory management related utilities and functions.
// ================================================================================================

#ifndef ATLAS_CORE_MEMORY_MEM_UTILS_HPP
#define ATLAS_CORE_MEMORY_MEM_UTILS_HPP

#include "atlas/core/utils/basic_types.hpp"
#include "atlas/core/memory/mem_debug.hpp"

#include <new>
#include <cstring>

#if ATLAS_COMPILER_HAS_CPP11
	#include <type_traits>
#endif // ATLAS_COMPILER_HAS_CPP11

namespace atlas
{
namespace core
{

// ================================================================================================
// Pointer/memory alignment helpers:
// ================================================================================================

// ========================================================
// isPowerOfTwoSize():
// ========================================================

ATTRIBUTE_PURE
inline bool isPowerOfTwoSize(const std::size_t size)
{
	return size != 0 && (size & (size - 1)) == 0;
}

// ========================================================
// isAlignedPtr():
// ========================================================

ATTRIBUTE_PURE
inline bool isAlignedPtr(const void * ptr, const std::size_t alignment)
{
	DEBUG_CHECK(isPowerOfTwoSize(alignment));

	// Aligned if the pointer is evenly divisible by the alignment value.
	// Same as `(ptr % align) == 0` (This '&' version works with POT alignments only!).
	return (reinterpret_cast<std::uintptr_t>(ptr) & (alignment - 1)) == 0;
}

// ========================================================
// alignSize():
// ========================================================

ATTRIBUTE_PURE
inline std::size_t alignSize(const std::size_t size, const std::size_t alignment)
{
	DEBUG_CHECK(isPowerOfTwoSize(alignment));

	// Add the minimum extra needed to the size for pointer alignment.
	// This size can then be used to `malloc()` some memory
	// and then have the pointer aligned with `alignPtr()`.
	return size + (alignment - 1);
}

// ========================================================
// alignPtr():
// ========================================================

ATTRIBUTE_PURE
inline void * alignPtr(const void * ptr, const std::size_t alignment)
{
	DEBUG_CHECK(isPowerOfTwoSize(alignment));

	// Cast to integer and align:
	const std::uintptr_t uintPtr    = reinterpret_cast<std::uintptr_t>(ptr);
	const std::uintptr_t alignedPtr = (uintPtr + (alignment - 1)) & ~(alignment - 1);

	// Re-cast to `void*`, validate and return:
	void * userPtr = reinterpret_cast<void *>(alignedPtr);
	DEBUG_CHECK(isAlignedPtr(userPtr, alignment));
	return userPtr;
}

// ================================================================================================
// Placement new / construction -- destruction -- object-copy helpers:
// ================================================================================================

// ========================================================
// construct<T>():
// ========================================================

template<class T>
T * construct(T * obj)
{
	DEBUG_CHECK(obj != nullptr);
	return ::new(reinterpret_cast<void *>(obj)) T;
}

// ========================================================
// construct<T, U>():
// ========================================================

template<class T, class U>
T * construct(T * obj, const U & arg)
{
	DEBUG_CHECK(obj != nullptr);
	return ::new(reinterpret_cast<void *>(obj)) T(arg);
}

// ========================================================
// constructSequence<T>():
// ========================================================

template<class T>
void constructSequence(T * sequence, std::size_t count)
{
	DEBUG_CHECK(sequence != nullptr);
	while (count--)
	{
		construct(sequence++);
	}
}

// ========================================================
// destroy<T>():
// ========================================================

template<class T>
void destroy(T * obj)
{
	DEBUG_CHECK(obj != nullptr);
	obj->~T();

	// When memory debugging is enable, this fills the memory formerly
	// used by the object with junk. This can make it easier detecting
	// cases when an attempt to reuse an object that was already destroyed is made.
	M_DBG_FILL_WITH_DESTROYED_PATTERN(obj, sizeof(*obj));
}

// ========================================================
// destroySequence<T>():
// ========================================================

template<class T>
void destroySequence(T * sequence, std::size_t count)
{
	DEBUG_CHECK(sequence != nullptr);
	while (count--)
	{
		destroy(sequence++);
	}
}

// ========================================================
// copyUninitialized<T, U>():
// ========================================================

template<class T, class U>
void copyUninitialized(T * dest, const U * source, std::size_t count)
{
	DEBUG_CHECK(dest   != nullptr);
	DEBUG_CHECK(source != nullptr);
	while (count--)
	{
		// Copy by issuing a call to the copy constructor of dest.
		construct(dest++, *source++);
	}
}

// ========================================================
// fillUninitialized<T, U>():
// ========================================================

template<class T, class U>
void fillUninitialized(T * dest, const U & fillWith, std::size_t count)
{
	DEBUG_CHECK(dest != nullptr);
	while (count--)
	{
		// Fill by issuing a call to the copy constructor of dest.
		construct(dest++, fillWith);
	}
}

// ========================================================
// copyInitialized<T, U>():
// ========================================================

template<class T, class U>
void copyInitialized(T * dest, const U * source, std::size_t count)
{
	DEBUG_CHECK(dest   != nullptr);
	DEBUG_CHECK(source != nullptr);
	while (count--)
	{
		// Copy by issuing a call to the assignment operator.
		// (assume memory pointed by `dest` was previously initialized).
		*dest++ = *source++;
	}
}

// ========================================================
// fillInitialized<T, U>():
// ========================================================

template<class T, class U>
void fillInitialized(T * dest, const U & fillWith, std::size_t count)
{
	DEBUG_CHECK(dest != nullptr);
	while (count--)
	{
		// Fill by issuing a call to the assignment operator.
		// (assume memory pointed by `dest` was previously initialized).
		*dest++ = fillWith;
	}
}

// ================================================================================================
// <cstring>|<string.h> -- C++ wrappers:
// ================================================================================================

// ========================================================
// clearPodObject<T>():
// ========================================================

// Zero fills a POD type, such as a C struct or union.
template<class T>
void clearPodObject(T & obj)
{
	#if ATLAS_COMPILER_HAS_CPP11
	COMPILE_TIME_CHECK(std::is_pod<T>::value, "Type must be Plain Old Data (POD)!");
	#endif // ATLAS_COMPILER_HAS_CPP11

	std::memset(&obj, 0, sizeof(T));
}

// ========================================================
// clearPodArray<T, N>():
// ========================================================

// Zero fills a statically allocated array of POD or built-in types. Array length inferred by the compiler.
template<class T, std::size_t N>
void clearPodArray(T (&array)[N])
{
	#if ATLAS_COMPILER_HAS_CPP11
	COMPILE_TIME_CHECK(std::is_pod<T>::value, "Type must be Plain Old Data (POD)!");
	#endif // ATLAS_COMPILER_HAS_CPP11

	std::memset(array, 0, sizeof(T) * N);
}

// ========================================================
// clearPodArray<T>():
// ========================================================

// Zero fills an array of POD or built-in types, with array length provided by the caller.
template<class T>
void clearPodArray(T * arrayPtr, const std::size_t count)
{
	#if ATLAS_COMPILER_HAS_CPP11
	COMPILE_TIME_CHECK(std::is_pod<T>::value, "Type must be Plain Old Data (POD)!");
	#endif // ATLAS_COMPILER_HAS_CPP11

	DEBUG_CHECK(arrayPtr != nullptr && count != 0);
	std::memset(arrayPtr, 0, sizeof(T) * count);
}

// ========================================================
// copyPodArray<T>():
// ========================================================

// Does a "memcopy" of the array, asserting that the data is POD at compile time.
template<class T>
void copyPodArray(T * dest, const T * source, const std::size_t count)
{
	#if ATLAS_COMPILER_HAS_CPP11
	COMPILE_TIME_CHECK(std::is_pod<T>::value, "Type must be Plain Old Data (POD)!");
	#endif // ATLAS_COMPILER_HAS_CPP11

	DEBUG_CHECK(dest   != nullptr);
	DEBUG_CHECK(source != nullptr);
	DEBUG_CHECK(count  != 0);
	std::memcpy(dest, source, sizeof(T) * count);
}

// ================================================================================================
// Custom general-purpose memory allocators:
// ================================================================================================

// ========================================================
// allocate<T>():
// ========================================================

template<class T>
inline T * allocate(const std::size_t count, const std::size_t alignment = ALIGNMENT_OF(T))
{
	// TODO temp
	// also note that the alignment should be clamped to a "recommended" minimum!
	//void * mem = alignPtr( malloc( alignSize(count * sizeof(T), alignment) ), alignment );
	//
	(void)alignment;
	void * mem = malloc( count * sizeof(T) );
	memset(mem, MemAllocedPattern, count * sizeof(T));
	return (T *)mem;
}

// ========================================================
// reallocate<T>():
// ========================================================

template<class T>
inline T * reallocate(T * oldPtr, const std::size_t newCount, const std::size_t alignment = ALIGNMENT_OF(T))
{
	// TODO temp
	(void)alignment;
	return (T *)realloc( oldPtr, newCount * sizeof(T) );
}

// ========================================================
// deallocate():
// ========================================================

inline void deallocate(void * ptr)
{
	// TODO temp
	// need an unalignPtr()!
	// must be null proof!
	free(ptr);
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_MEMORY_MEM_UTILS_HPP
