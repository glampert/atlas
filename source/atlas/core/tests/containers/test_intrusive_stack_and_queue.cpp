
// ================================================================================================
// -*- C++ -*-
// File: test_intrusive_stack_and_queue.cpp
// Author: Guilherme R. Lampert
// Created on: 06/03/15
// Brief: Unit tests for the IntrusiveStack and IntrusiveQueue data structures.
// ================================================================================================

#include "atlas/core/containers/intrusive_stack.hpp"
#include "atlas/core/containers/intrusive_queue.hpp"

namespace atlas
{
namespace core
{
namespace unittest
{

// ========================================================
// Test_IntrusiveStack():
// ========================================================

void Test_IntrusiveStack()
{
	class MyStackItem
		: public StackNode<MyStackItem>
	{
	public:
		int dummy;
		MyStackItem() DEFAULTED_METHOD;
	};

	IntrusiveStack<MyStackItem> myStack;

	ALWAYS_CHECK(myStack.isEmpty() == true);
	ALWAYS_CHECK(myStack.getSize() == 0);
	ALWAYS_CHECK(myStack.top() == nullptr);

	MyStackItem item0;
	MyStackItem item1;
	MyStackItem item2;
	MyStackItem item3;
	MyStackItem item4;
	MyStackItem item5;
	MyStackItem item6;
	MyStackItem item7;

	// Push some items:
	myStack.push(&item0);
	myStack.push(&item1);
	myStack.push(&item2);
	myStack.push(&item3);
	myStack.push(&item4);
	myStack.push(&item5);
	myStack.push(&item6);
	myStack.push(&item7);

	// Validate:
	ALWAYS_CHECK(myStack.isEmpty() == false);
	ALWAYS_CHECK(myStack.getSize() == 8);
	ALWAYS_CHECK(myStack.top() == &item7);

	// Pop a few:
	ALWAYS_CHECK(myStack.pop() == &item7);
	ALWAYS_CHECK(myStack.pop() == &item6);
	ALWAYS_CHECK(myStack.top() == &item5);
	ALWAYS_CHECK(myStack.pop() == &item5);
	ALWAYS_CHECK(myStack.pop() == &item4);
	ALWAYS_CHECK(myStack.top() == &item3);
	ALWAYS_CHECK(myStack.getSize() == 4);

	// Push a couple more:
	MyStackItem item8;
	MyStackItem item9;
	myStack.push(&item8);
	myStack.push(&item9);
	ALWAYS_CHECK(myStack.top() == &item9);
	ALWAYS_CHECK(myStack.getSize() == 6);

	// Pop again:
	ALWAYS_CHECK(myStack.pop() == &item9);
	ALWAYS_CHECK(myStack.top() == &item8);
	ALWAYS_CHECK(myStack.getSize() == 5);

	// Pop until empty:
	while (!myStack.isEmpty())
	{
		myStack.pop();
	}
	ALWAYS_CHECK(myStack.getSize() == 0);
}

// ========================================================
// Test_IntrusiveQueue():
// ========================================================

void Test_IntrusiveQueue()
{
	class MyQueueItem
		: public QueueNode<MyQueueItem>
	{
	public:
		int dummy;
		MyQueueItem() DEFAULTED_METHOD;
	};

	IntrusiveQueue<MyQueueItem> myQueue;

	ALWAYS_CHECK(myQueue.isEmpty() == true);
	ALWAYS_CHECK(myQueue.getSize() == 0);
	ALWAYS_CHECK(myQueue.front()   == nullptr);
	ALWAYS_CHECK(myQueue.back()    == nullptr);

	MyQueueItem item0;
	MyQueueItem item1;
	MyQueueItem item2;
	MyQueueItem item3;
	MyQueueItem item4;
	MyQueueItem item5;
	MyQueueItem item6;
	MyQueueItem item7;

	// Push some items:
	myQueue.push(&item0);
	myQueue.push(&item1);
	myQueue.push(&item2);
	myQueue.push(&item3);
	myQueue.push(&item4);
	myQueue.push(&item5);
	myQueue.push(&item6);
	myQueue.push(&item7);

	// Validate:
	ALWAYS_CHECK(myQueue.isEmpty() == false);
	ALWAYS_CHECK(myQueue.getSize() == 8);
	ALWAYS_CHECK(myQueue.front()   == &item0);
	ALWAYS_CHECK(myQueue.back()    == &item7);

	// Pop a few:
	ALWAYS_CHECK(myQueue.pop()   == &item0);
	ALWAYS_CHECK(myQueue.pop()   == &item1);
	ALWAYS_CHECK(myQueue.front() == &item2);
	ALWAYS_CHECK(myQueue.back()  == &item7);
	ALWAYS_CHECK(myQueue.pop()   == &item2);
	ALWAYS_CHECK(myQueue.pop()   == &item3);
	ALWAYS_CHECK(myQueue.front() == &item4);
	ALWAYS_CHECK(myQueue.back()  == &item7);
	ALWAYS_CHECK(myQueue.getSize() == 4);

	// Push a couple more:
	MyQueueItem item8;
	MyQueueItem item9;
	myQueue.push(&item8);
	myQueue.push(&item9);
	ALWAYS_CHECK(myQueue.front() == &item4);
	ALWAYS_CHECK(myQueue.back()  == &item9);
	ALWAYS_CHECK(myQueue.getSize() == 6);

	// Pop again:
	ALWAYS_CHECK(myQueue.pop()   == &item4);
	ALWAYS_CHECK(myQueue.front() == &item5);
	ALWAYS_CHECK(myQueue.getSize() == 5);

	// Pop until empty:
	while (!myQueue.isEmpty())
	{
		myQueue.pop();
	}
	ALWAYS_CHECK(myQueue.getSize() == 0);
}

} // namespace unittest {}
} // namespace core {}
} // namespace atlas {}
