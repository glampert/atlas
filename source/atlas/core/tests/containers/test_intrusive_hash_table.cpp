
// ================================================================================================
// -*- C++ -*-
// File: test_intrusive_hash_table.cpp
// Author: Guilherme R. Lampert
// Created on: 04/03/15
// Brief: Unit tests for the IntrusiveHashTable.
// ================================================================================================

#include "atlas/core/containers/intrusive_hash_table.hpp"
#include "atlas/core/strings/dynamic_string.hpp"

namespace atlas
{
namespace core
{
namespace unittest
{

// ========================================================

class MyHItem
	: public HashTableNode<MyHItem>
{
public:
	String name;
	MyHItem() DEFAULTED_METHOD;
	MyHItem(const String & s) : name(s) { }
};

typedef IntrusiveHashTable<const String, MyHItem> MyHTable;

// ========================================================
// Test_noDuplicatesSmallTable():
// ========================================================

void Test_noDuplicatesSmallTable()
{
	// Small table to force keys to hash to the same bucket:
	MyHTable myTable(/* allowDuplicateKeys = */ false, 5);

	ALWAYS_CHECK(myTable.getSize() == 0);
	ALWAYS_CHECK(myTable.isEmpty() == true);
	ALWAYS_CHECK(myTable.getBucketCount() >= 5);
	ALWAYS_CHECK(myTable.isAllowingDuplicateKeys() == false);

	// find() with inexistent keys:
	ALWAYS_CHECK(myTable.find("uno" ) == nullptr);
	ALWAYS_CHECK(myTable.find("dos" ) == nullptr);
	ALWAYS_CHECK(myTable.find("tres") == nullptr);

	MyHItem item0("i0");
	MyHItem item1("i1");
	MyHItem item2("i2");
	MyHItem item3("i3");
	MyHItem item4("i4");
	MyHItem item5("i5");
	MyHItem item6("i6");
	MyHItem item7("i7");

	// insert():
	ALWAYS_CHECK(myTable.insert("item0", &item0) == true);
	ALWAYS_CHECK(myTable.insert("item1", &item1) == true);
	ALWAYS_CHECK(myTable.insert("item2", &item2) == true);
	ALWAYS_CHECK(myTable.insert("item3", &item3) == true);
	ALWAYS_CHECK(myTable.insert("item4", &item4) == true);
	ALWAYS_CHECK(myTable.insert("item5", &item5) == true);
	ALWAYS_CHECK(myTable.insert("item6", &item6) == true);
	ALWAYS_CHECK(myTable.insert("item7", &item7) == true);

	// Fail since key is already present:
	MyHItem item0_dup, item7_dup;
	ALWAYS_CHECK(myTable.insert("item0", &item0_dup) == false);
	ALWAYS_CHECK(myTable.insert("item7", &item7_dup) == false);

	// Validate table states:
	ALWAYS_CHECK(myTable.getSize() == 8);
	ALWAYS_CHECK(myTable.isEmpty() == false);
	ALWAYS_CHECK(myTable.isAllocated() == true);

	// Iterate once:
	{
		uint count = 0;
		const MyHTable::Iterator it = begin(myTable);
		const MyHTable::Iterator en = end(myTable);

		while (it != en)
		{
			const char * name = (*it).name.getCStr();
			ALWAYS_CHECK(name[0] == 'i');

			++it;
			++count;
		}

		ALWAYS_CHECK(count == myTable.getSize());

		// Test iterator operator (== and !=):
		ALWAYS_CHECK(myTable.begin() != myTable.end());
		ALWAYS_CHECK(myTable.begin() == myTable.begin());
		ALWAYS_CHECK(myTable.end()   == myTable.end());
	}

	// operator[] / countAllMatching():
	ALWAYS_CHECK(myTable["item7"] == &item7);
	ALWAYS_CHECK(myTable["item6"] == &item6);
	ALWAYS_CHECK(myTable["item5"] == &item5);
	ALWAYS_CHECK(myTable["item4"] == &item4);
	ALWAYS_CHECK(myTable["item3"] == &item3);
	ALWAYS_CHECK(myTable["item2"] == &item2);
	ALWAYS_CHECK(myTable["item1"] == &item1);
	ALWAYS_CHECK(myTable["item0"] == &item0);
	ALWAYS_CHECK(myTable.countAllMatching("item3") == 1);

	// remove():
	ALWAYS_CHECK(myTable.remove("item5") == &item5);
	ALWAYS_CHECK(myTable.remove("item3") == &item3);
	ALWAYS_CHECK(myTable.remove("item1") == &item1);
	ALWAYS_CHECK(myTable.remove("inexistent") == nullptr);
	ALWAYS_CHECK(myTable.getSize() == 5);

	// removeAllMatching():
	ALWAYS_CHECK(myTable.removeAllMatching("item7") == 1);
	ALWAYS_CHECK(myTable.removeAllMatching("item4") == 1);
	ALWAYS_CHECK(myTable.removeAllMatching("item0") == 1);
	ALWAYS_CHECK(myTable.removeAllMatching("inexistent") == 0);
	ALWAYS_CHECK(myTable.getSize() == 2);

	// Clear whole table:
	myTable.clear();
	ALWAYS_CHECK(myTable.isEmpty());
	ALWAYS_CHECK(myTable.getSize() == 0);

	// Ensure all items have been properly unlinked:
	ALWAYS_CHECK(item0.isLinkedToHashTable() == false && item0.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item1.isLinkedToHashTable() == false && item1.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item2.isLinkedToHashTable() == false && item2.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item3.isLinkedToHashTable() == false && item3.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item4.isLinkedToHashTable() == false && item4.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item5.isLinkedToHashTable() == false && item5.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item6.isLinkedToHashTable() == false && item6.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item7.isLinkedToHashTable() == false && item7.getHashTableNext() == nullptr);
}

// ========================================================
// Test_duplicateKeysSmallTable():
// ========================================================

void Test_duplicateKeysSmallTable()
{
	// Small table to force keys to hash to the same bucket:
	MyHTable myTable(/* allowDuplicateKeys = */ true, 5);

	ALWAYS_CHECK(myTable.getSize() == 0);
	ALWAYS_CHECK(myTable.isEmpty() == true);
	ALWAYS_CHECK(myTable.getBucketCount() >= 5);
	ALWAYS_CHECK(myTable.isAllowingDuplicateKeys() == true);

	MyHItem item0("AAA_0");
	MyHItem item1("AAA_1");
	MyHItem item2("AAA_2");
	MyHItem item3("BBB_3");
	MyHItem item4("BBB_4");
	MyHItem item5("BBB_5");
	MyHItem item6("CCC_6");
	MyHItem item7("DDD_7");

	// insert() with repeated keys but different values:
	ALWAYS_CHECK(myTable.insert("AAA", &item0) == true);
	ALWAYS_CHECK(myTable.insert("AAA", &item1) == true);
	ALWAYS_CHECK(myTable.insert("AAA", &item2) == true);
	ALWAYS_CHECK(myTable.insert("BBB", &item3) == true);
	ALWAYS_CHECK(myTable.insert("BBB", &item4) == true);
	ALWAYS_CHECK(myTable.insert("BBB", &item5) == true);
	ALWAYS_CHECK(myTable.insert("CCC", &item6) == true);
	ALWAYS_CHECK(myTable.insert("DDD", &item7) == true);

	ALWAYS_CHECK(myTable.getSize() == 8);
	ALWAYS_CHECK(myTable.isEmpty() == false);
	ALWAYS_CHECK(myTable.isAllocated() == true);

	// operator[]:
	ALWAYS_CHECK(myTable["AAA"] != nullptr);
	ALWAYS_CHECK(myTable["BBB"] != nullptr);
	ALWAYS_CHECK(myTable["CCC"] != nullptr);
	ALWAYS_CHECK(myTable["DDD"] != nullptr);

	// countAllMatching():
	ALWAYS_CHECK(myTable.countAllMatching("AAA") == 3);
	ALWAYS_CHECK(myTable.countAllMatching("BBB") == 3);
	ALWAYS_CHECK(myTable.countAllMatching("CCC") == 1);
	ALWAYS_CHECK(myTable.countAllMatching("DDD") == 1);

	// findAllMatching():
	MyHItem * itemsFound[3] = { nullptr, nullptr, nullptr };
	ALWAYS_CHECK(myTable.findAllMatching("AAA", itemsFound, 3) == 3);
	ALWAYS_CHECK(itemsFound[0] != nullptr && itemsFound[0]->name.startsWith("AAA"));
	ALWAYS_CHECK(itemsFound[1] != nullptr && itemsFound[1]->name.startsWith("AAA"));
	ALWAYS_CHECK(itemsFound[2] != nullptr && itemsFound[2]->name.startsWith("AAA"));

	// remove() (first occurrence):
	ALWAYS_CHECK(myTable.remove("AAA") != nullptr);
	ALWAYS_CHECK(myTable.remove("BBB") != nullptr);
	ALWAYS_CHECK(myTable.remove("CCC") != nullptr);
	ALWAYS_CHECK(myTable.remove("inexistent") == nullptr);
	ALWAYS_CHECK(myTable.getSize() == 5);

	// removeAllMatching():
	ALWAYS_CHECK(myTable.removeAllMatching("AAA") == 2);
	ALWAYS_CHECK(myTable.removeAllMatching("BBB") == 2);
	ALWAYS_CHECK(myTable.removeAllMatching("CCC") == 0);
	ALWAYS_CHECK(myTable.removeAllMatching("DDD") == 1);
	ALWAYS_CHECK(myTable.removeAllMatching("inexistent") == 0);
	ALWAYS_CHECK(myTable.getSize() == 0);

	// Ensure all items have been properly unlinked:
	ALWAYS_CHECK(item0.isLinkedToHashTable() == false && item0.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item1.isLinkedToHashTable() == false && item1.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item2.isLinkedToHashTable() == false && item2.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item3.isLinkedToHashTable() == false && item3.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item4.isLinkedToHashTable() == false && item4.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item5.isLinkedToHashTable() == false && item5.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item6.isLinkedToHashTable() == false && item6.getHashTableNext() == nullptr);
	ALWAYS_CHECK(item7.isLinkedToHashTable() == false && item7.getHashTableNext() == nullptr);
}

// ========================================================
// Test_IntrusiveHashTable():
// ========================================================

void Test_IntrusiveHashTable()
{
	Test_noDuplicatesSmallTable();
	Test_duplicateKeysSmallTable();
}

} // namespace unittest {}
} // namespace core {}
} // namespace atlas {}
