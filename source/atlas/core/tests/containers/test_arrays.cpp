
// ================================================================================================
// -*- C++ -*-
// File: test_arrays.hpp
// Author: Guilherme R. Lampert
// Created on: 19/04/15
// Brief: Unit tests for the C++ array classes/wrappers.
// ================================================================================================

#include "atlas/core/containers/arrays.hpp"
#include "atlas/core/utils/log_stream.hpp"
#include "atlas/core/utils/default_log.hpp"
#include "atlas/core/strings/to_string.hpp"
#include "atlas/core/strings/dynamic_string.hpp"

namespace atlas
{
namespace core
{
namespace unittest
{

// ========================================================
// printArray():
// ========================================================

template<class ARRAY_TYPE>
void printArray(const ARRAY_TYPE & array, LogStream & logStr)
{
	typedef typename ARRAY_TYPE::ValueType ValueType;

	logStr << "Array<T> {\n";
	logStr << "\tsize: " << array.getSize()     << "\n";
	logStr << "\tcaps: " << array.getCapacity() << "\n";

	// Print the initialized elements as strings:
	//
	String temp;
	for (uint i = 0; i < array.getSize(); ++i)
	{
		temp = toString(array[i]);
		logStr << "\t[" << i << "] = " << temp << "\n";
	}

	// Print all the way down to the allocated
	// capacity as raw bytes for debug inspection:
	//
	const ValueType * ptr = array.getData();
	for (uint i = array.getSize(); i < array.getCapacity(); ++i)
	{
		logStr << "\t[ ";

		const ubyte * bytes = reinterpret_cast<const ubyte *>(&ptr[i]);
		for (uint b = 0; b < sizeof(ValueType); ++b)
		{
			if (ctype::isPrint(bytes[b]))
			{
				logStr << static_cast<char>(bytes[b]);
			}
			else
			{
				temp = toString(static_cast<int>(bytes[b]), 16);
				logStr << temp;
			}

			if (b != sizeof(ValueType) - 1) { logStr << ","; }
		}

		logStr << " ] (" << sizeof(ValueType) << " bytes)\n";
	}

	logStr << "}\n";
}

// ========================================================
// assertContents():
// ========================================================

void assertContents(const Array<String> & array, const char * first, ...)
{
	va_list vaList;
	const char * next = first;
	const Array<String>::Iterator it = begin(array);

	// Input list of items must be null terminated!
	//
	va_start(vaList, first);
	while (next != nullptr)
	{
		ALWAYS_CHECK(*it == next && "Unexpected string in array sequence!");
		next = va_arg(vaList, const char *);
		++it;
	}
	va_end(vaList);
}

// ========================================================
// Test_arrayAllocResizeTransferCopy():
// ========================================================

void Test_arrayAllocResizeTransferCopy()
{
	// Array with 512 strings filled with "testing, 123".
	Array<String> array(512, "testing, 123");

	ALWAYS_CHECK(array.isEmpty()     == false);
	ALWAYS_CHECK(array.isValid()     == true);
	ALWAYS_CHECK(array.isAllocated() == true);
	ALWAYS_CHECK(array.getSize()     == 512);
	ALWAYS_CHECK(array.getCapacity() >= array.getSize());

	const Array<String>::Iterator it = begin(array);
	while (it != end(array))
	{
		ALWAYS_CHECK(*it == "testing, 123");
		++it;
	}

	// Increase capacity:
	array.allocate(1024);
	ALWAYS_CHECK(array.getSize() == 512);
	ALWAYS_CHECK(array.getCapacity() >= 1024);

	// Resize:
	array.resize(1024, "new element");
	ALWAYS_CHECK(array.getSize() == 1024);

	// 0 - 511 must be "testing, 123"
	for (uint i = 0; i < 512; ++i)
	{
		ALWAYS_CHECK(array[i] == "testing, 123");
	}

	// 512 - 1023 must be "new element"
	for (uint i = 512; i < 1024; ++i)
	{
		ALWAYS_CHECK(array[i] == "new element");
	}

	// Create a copy:
	Array<String> arrayCpy;
	array.copyTo(arrayCpy);

	// Validate copied array:
	ALWAYS_CHECK(arrayCpy.isEmpty()     == false);
	ALWAYS_CHECK(arrayCpy.isValid()     == true);
	ALWAYS_CHECK(arrayCpy.isAllocated() == true);
	ALWAYS_CHECK(arrayCpy.getSize()     == array.getSize());
	ALWAYS_CHECK(arrayCpy.getCapacity() >= array.getCapacity());
	for (uint i = 0; i < array.getSize(); ++i)
	{
		ALWAYS_CHECK(array[i] == arrayCpy[i]);
	}

	// Transfer the copy to another array:
	Array<String> arrayXfer;
	arrayCpy.transferTo(arrayXfer);

	// Copy gets cleared:
	ALWAYS_CHECK(arrayCpy.isEmpty()      == true);
	ALWAYS_CHECK(arrayCpy.isValid()      == false);
	ALWAYS_CHECK(arrayCpy.isAllocated()  == false);
	ALWAYS_CHECK(arrayCpy.getSize()      == 0);
	ALWAYS_CHECK(arrayCpy.getCapacity()  == 0);

	// Destination receives the copy:
	ALWAYS_CHECK(arrayXfer.isEmpty()     == false);
	ALWAYS_CHECK(arrayXfer.isValid()     == true);
	ALWAYS_CHECK(arrayXfer.isAllocated() == true);
	ALWAYS_CHECK(arrayXfer.getSize()     != 0);
	ALWAYS_CHECK(arrayXfer.getCapacity() != 0);

	// Clear without deallocating:
	arrayXfer.clear();
	ALWAYS_CHECK(arrayXfer.isEmpty()     == true);
	ALWAYS_CHECK(arrayXfer.isValid()     == true);
	ALWAYS_CHECK(arrayXfer.isAllocated() == true);
	ALWAYS_CHECK(arrayXfer.getSize()     == 0);
	ALWAYS_CHECK(arrayXfer.getCapacity() != 0);
}

// ========================================================
// Test_arrayInsertion():
// ========================================================

void Test_arrayInsertion()
{
	Array<String> array;

	// pushBack()/pushIfUnique():
	{
		array.pushBack("AAA");
		array.pushBack("BBB");
		array.pushBack("CCC");
		array.pushBack("DDD");
		array.pushBack("EEE");
		ALWAYS_CHECK(array.getSize() == 5);
		assertContents(array, "AAA", "BBB", "CCC", "DDD", "EEE", nullptr);

		array.pushIfUniqueSorted("FFF");
		array.pushIfUniqueSorted("GGG");
		ALWAYS_CHECK(array.getSize() == 7);
		assertContents(array, "AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG", nullptr);

		array.pushIfUniqueUnsorted("HHH");
		array.pushIfUniqueUnsorted("III");
		ALWAYS_CHECK(array.getSize() == 9);
		assertContents(array, "AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG", "HHH", "III", nullptr);

		// Already there, fail:
		ALWAYS_CHECK(array.pushIfUniqueSorted("CCC")   == false);
		ALWAYS_CHECK(array.pushIfUniqueSorted("FFF")   == false);
		ALWAYS_CHECK(array.pushIfUniqueUnsorted("HHH") == false);
		ALWAYS_CHECK(array.pushIfUniqueUnsorted("AAA") == false);
		ALWAYS_CHECK(array.getSize() == 9);
		assertContents(array, "AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG", "HHH", "III", nullptr);

		// Push a small sequence:
		const String items[] = { "foo", "bar", "baz" };
		array.pushBack(items, arrayLength(items));
		ALWAYS_CHECK(array.getSize() == 12);
		assertContents(array, "AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG", "HHH", "III", "foo", "bar", "baz", nullptr);
	}

	// Remove the last three items so that the array is:
	//  ["AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG", "HHH", "III"]
	array.popBack(3);
	assertContents(array, "AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG", "HHH", "III", nullptr);

	// insert() single:
	{
		// At front:
		array.insert("hello", 0);

		// At end:
		array.insert("world", array.getSize() - 1);

		// In the middle:
		array.insert("foo", 5);
		array.insert("bar", 6);

		// Validate insertions:
		ALWAYS_CHECK(array.getSize() == 13);
		assertContents(array, "hello", "AAA", "BBB", "CCC", "DDD", "foo", "bar", "EEE", "FFF", "GGG", "HHH", "world", "III", nullptr);

		// Clear and insert one at empty array:
		array.clear();
		array.insert("You are the chosen one, Neo", 0);
		ALWAYS_CHECK(array.getSize() == 1);
		ALWAYS_CHECK(array[0] == "You are the chosen one, Neo");
	}

	// Clear for sequence insertion tests:
	array.deallocate();
	ALWAYS_CHECK(array.isEmpty()     == true);
	ALWAYS_CHECK(array.getSize()     == 0);
	ALWAYS_CHECK(array.getCapacity() == 0);

	// insert() sequence:
	{
		// Insert into empty array:
		const String items0[] = { "1", "2", "3", "4" };
		array.insert(items0, 0);
		ALWAYS_CHECK(array.getSize() == 4);
		assertContents(array, "1", "2", "3", "4", nullptr);

		// Insert at front:
		const String items1[] = { "uno", "dos", "tres", "cuatro" };
		array.insert(items1, 0);
		ALWAYS_CHECK(array.getSize() == 8);
		assertContents(array, "uno", "dos", "tres", "cuatro", "1", "2", "3", "4", nullptr);

		// Insert at end:
		const String items2[] = { "one", "two", "three", "four" };
		array.insert(items2, array.getSize() - 1);
		ALWAYS_CHECK(array.getSize() == 12);
		assertContents(array, "uno", "dos", "tres", "cuatro", "1", "2", "3", "one", "two", "three", "four", "4", nullptr);

		// Insert in the middle:
		const String items3[] = { "um", "dois", "três" };
		array.insert(items3, 4);
		ALWAYS_CHECK(array.getSize() == 15);
		assertContents(array, "uno", "dos", "tres", "cuatro", "um", "dois", "três", "1", "2", "3", "one", "two", "three", "four", "4", nullptr);
	}
}

// ========================================================
// Test_arrayRemoval():
// ========================================================

void Test_arrayRemoval()
{
	Array<String> array;

	// popBack():
	{
		const String items[] = { "testing", "1", "2", "3" };
		array.pushBack(items);
		ALWAYS_CHECK(array.getSize() == 4);
		assertContents(array, "testing", "1", "2", "3", nullptr);

		array.popBack();
		array.popBack();
		ALWAYS_CHECK(array.getSize() == 2);
		assertContents(array, "testing", "1", nullptr);

		// Same as clear().
		array.popBack(array.getSize());
		ALWAYS_CHECK(array.isEmpty() == true);
	}

	// remove() single index:
	{
		const String items[] = { "AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG" };
		array.pushBack(items);
		ALWAYS_CHECK(array.getSize() == 7);
		assertContents(array, "AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG", nullptr);

		// At front:
		array.remove(0);

		// Back:
		array.remove(array.getSize() - 1);

		// In-between:
		array.remove(1);
		array.remove(2);

		// Validate:
		ALWAYS_CHECK(array.getSize() == 3);
		assertContents(array, "BBB", "DDD", "FFF", nullptr);

		// Remove until empty:
		while (!array.isEmpty()) { array.remove(0); }
		array.deallocate();
	}

	// remove() iterator based:
	{
		const String items[] = { "AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG" };
		array.pushBack(items);
		ALWAYS_CHECK(array.getSize() == 7);
		assertContents(array, "AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG", nullptr);

		// At front:
		array.remove(array.begin());

		// Back:
		array.remove(array.end() - 1);

		// In-between:
		array.remove(array.begin() + 1);
		array.remove(array.end()   - 2);

		// Validate:
		ALWAYS_CHECK(array.getSize() == 3);
		assertContents(array, "BBB", "DDD", "FFF", nullptr);

		// Remove until empty:
		while (!array.isEmpty()) { array.remove(array.begin()); }
		array.deallocate();
	}

	// remove() multiple indexes:
	{
		array.clear();

		const String items[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
		array.pushBack(items);
		ALWAYS_CHECK(array.getSize() == 11);
		assertContents(array, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", nullptr);

		// At front:
		array.remove(0, 3);
		ALWAYS_CHECK(array.getSize() == 8);
		assertContents(array, "3", "4", "5", "6", "7", "8", "9", "10", nullptr);

		// In-between (remove all the way to the end):
		array.remove(4, array.getSize());
		ALWAYS_CHECK(array.getSize() == 4);
		assertContents(array, "3", "4", "5", "6", nullptr);

		// At the end:
		array.remove(3, 1);
		ALWAYS_CHECK(array.getSize() == 3);
		assertContents(array, "3", "4", "5", nullptr);

		// Finish removing the rest:
		array.remove(0, array.getSize());
		ALWAYS_CHECK(array.getSize() == 0);
		ALWAYS_CHECK(array.isEmpty() == true);
	}

	// removeAndSwap():
	{
		array.clear();
		array.pushBack("hello");
		array.pushBack("coding");
		array.pushBack("world");

		// Remove at index 1 ("coding") and place the last one into its slot ("world").
		array.removeAndSwap(1);
		ALWAYS_CHECK(array.getSize() == 2);
		assertContents(array, "hello", "world", nullptr);
	}

	// removeAllMatching():
	{
		array.clear();
		array.pushBack("uno");
		array.pushBack("dos");
		array.pushBack("uno");
		array.pushBack("dos");
		array.pushBack("uno");
		array.pushBack("dos");
		array.pushBack("tres");
		array.pushBack("tres");
		array.pushBack("tres");
		array.pushBack("cuatro");
		ALWAYS_CHECK(array.getSize() == 10);

		// Returns the number of elements removed.
		ALWAYS_CHECK(array.removeAllMatching("uno")    == 3);
		ALWAYS_CHECK(array.removeAllMatching("dos")    == 3);
		ALWAYS_CHECK(array.removeAllMatching("tres")   == 3);
		ALWAYS_CHECK(array.removeAllMatching("cuatro") == 1);
		ALWAYS_CHECK(array.isEmpty() == true);
	}

	// removeIf():
	{
		array.clear();
		array.pushBack("AAA");
		array.pushBack("AAA");
		array.pushBack("BBB");
		array.pushBack("AAA");
		array.pushBack("CCC");
		array.pushBack("AAA");
		array.pushBack("DDD");
		array.pushBack("AAA");
		array.pushBack("EEE");
		array.pushBack("AAA");
		array.pushBack("AAA");

		struct Test1
		{
			static bool isAAA(const String & s) { return s == "AAA"; }
		};
		array.removeIf(Test1::isAAA);
		ALWAYS_CHECK(array.getSize() == 4);
		assertContents(array, "BBB", "CCC", "DDD", "EEE", nullptr);

		struct Test2
		{
			static bool isEqual(const String & s, const String & test) { return s == test; }
		};
		array.removeIf(Test2::isEqual, "BBB");
		array.removeIf(Test2::isEqual, "EEE");
		ALWAYS_CHECK(array.getSize() == 2);
		assertContents(array, "CCC", "DDD", nullptr);
	}

	// removeSortedDuplicates():
	{
		array.clear();
		array.pushBack("AAA");
		array.pushBack("AAA");
		array.pushBack("BBB");
		array.pushBack("AAA");
		array.pushBack("CCC");
		array.pushBack("AAA");
		array.pushBack("DDD");
		array.pushBack("AAA");
		array.pushBack("EEE");
		array.pushBack("AAA");
		array.pushBack("AAA");

		array.sort();
		assertContents(array, "AAA", "AAA", "AAA", "AAA", "AAA", "AAA", "AAA", "BBB", "CCC", "DDD", "EEE", nullptr);

		array.removeSortedDuplicates();
		ALWAYS_CHECK(array.getSize() == 5);
		assertContents(array, "AAA", "BBB", "CCC", "DDD", "EEE", nullptr);
	}
}

// ========================================================
// Test_arrayIteration():
// ========================================================

void Test_arrayIteration()
{
	Array<int> array;
	array.pushBack(0);
	array.pushBack(1);
	array.pushBack(2);
	array.pushBack(3);
	array.pushBack(4);
	array.pushBack(5);
	array.pushBack(6);
	array.pushBack(7);
	array.pushBack(8);
	array.pushBack(9);

	int num;

	// Forward iteration:
	num = 0;
	const Array<int>::Iterator fwIt = array.begin();
	while (fwIt != array.end())
	{
		ALWAYS_CHECK(*fwIt == num);
		++fwIt;
		++num;
	}

	// Reverse iteration:
	num = 9;
	const Array<int>::Iterator revIt = array.revBegin();
	while (revIt != array.revEnd())
	{
		ALWAYS_CHECK(*revIt == num);
		++revIt;
		--num;
	}

	// Try modifying the object pointer to by the iterator:
	*(array.begin()) = 42;      // first one
	*(array.begin() + 9) = 666; // last one

	ALWAYS_CHECK(array.front() == 42);
	ALWAYS_CHECK(array.back()  == 666);
}

// ========================================================
// Test_arrayMiscellaneous():
// ========================================================

void Test_arrayMiscellaneous()
{
	Array<String> array;

	// Validate initial states:
	{
		ALWAYS_CHECK(array.isEmpty()     == true);
		ALWAYS_CHECK(array.isValid()     == false);
		ALWAYS_CHECK(array.isAllocated() == false);
		ALWAYS_CHECK(array.getCapacity() == 0);
		ALWAYS_CHECK(array.getSize()     == 0);

		ALWAYS_CHECK(array.begin()       == array.end());
		ALWAYS_CHECK(array.revBegin()    == array.revEnd());
		ALWAYS_CHECK(begin(array)        == end(array));
		ALWAYS_CHECK(revBegin(array)     == revEnd(array));
	}

	// pushBack() sequence:
	{
		const String items[] =
		{
			"AAA", "BBB", "CCC", "DDD", "EEE"
		};
		array.pushBack(items);

		ALWAYS_CHECK(array.isEmpty()     == false);
		ALWAYS_CHECK(array.isValid()     == true);
		ALWAYS_CHECK(array.isAllocated() == true);
		ALWAYS_CHECK(array.getCapacity() != 0);
		ALWAYS_CHECK(array.getSize()     == 5);

		ALWAYS_CHECK(array.begin()       != array.end());
		ALWAYS_CHECK(array.revBegin()    != array.revEnd());
		ALWAYS_CHECK(begin(array)        != end(array));
		ALWAYS_CHECK(revBegin(array)     != revEnd(array));

		assertContents(array, "AAA", "BBB", "CCC", "DDD", "EEE", nullptr);
	}

	// indexOfPointer():
	{
		ALWAYS_CHECK(array.indexOfPointer(&array[0]) == 0);
		ALWAYS_CHECK(array.indexOfPointer(&array[1]) == 1);
		ALWAYS_CHECK(array.indexOfPointer(&array[2]) == 2);
		ALWAYS_CHECK(array.indexOfPointer(&array[3]) == 3);
		ALWAYS_CHECK(array.indexOfPointer(&array[4]) == 4);
	}

	// font()/back()/operator[]:
	{
		ALWAYS_CHECK(array.front() != array.back());
		ALWAYS_CHECK(array.front() == "AAA");
		ALWAYS_CHECK(array.back()  == "EEE");

		ALWAYS_CHECK(array[0] == "AAA");
		ALWAYS_CHECK(array[1] == "BBB");
		ALWAYS_CHECK(array[2] == "CCC");
		ALWAYS_CHECK(array[3] == "DDD");
		ALWAYS_CHECK(array[4] == "EEE");
	}

	// countAllMatching()/findUnsortedMatch():
	{
		ALWAYS_CHECK(array.countAllMatching("CCC")    == 1);
		ALWAYS_CHECK(array.countAllMatching("foobar") == 0);

		Array<String>::Iterator it;

		it = array.findUnsortedMatch("DDD");
		ALWAYS_CHECK(it  != array.end());
		ALWAYS_CHECK(*it == "DDD");

		it = array.findUnsortedMatch("AAA");
		ALWAYS_CHECK(it  != array.end());
		ALWAYS_CHECK(*it == "AAA");

		it = array.findUnsortedMatch("EEE");
		ALWAYS_CHECK(it  != array.end());
		ALWAYS_CHECK(*it == "EEE");
	}

	// sort()/isSorted()/findSorted():
	{
		array.clear();
		ALWAYS_CHECK(array.isEmpty() == true);
		ALWAYS_CHECK(array.getSize() == 0);

		// Some unordered input:
		array.pushBack("2");
		array.pushBack("1");
		array.pushBack("5");
		array.pushBack("3");
		array.pushBack("6");
		array.pushBack("4");
		array.pushBack("8");
		array.pushBack("7");
		array.pushBack("9");
		array.pushBack("0");
		ALWAYS_CHECK(array.isSorted() == false);

		// Sort and validate:
		array.sort();
		ALWAYS_CHECK(array.isSorted() == true);
		assertContents(array, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", nullptr);

		// Make sure sorted arrays are not messed up with re-sorting:
		array.sort();
		ALWAYS_CHECK(array.isSorted() == true);
		assertContents(array, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", nullptr);

		// Find in sorted sequence:
		ALWAYS_CHECK(array.findSortedMatch("0") == array.begin());
		ALWAYS_CHECK(array.findSortedMatch("9") == array.end()   - 1);
		ALWAYS_CHECK(array.findSortedMatch("5") == array.begin() + 5);
	}

	// fill() and count matching:
	{
		array.clear();
		array.resize(15);
		ALWAYS_CHECK(array.getSize() == 15);

		// Make sure we have an array of 15 empty strings:
		for (uint i = 0; i < array.getSize(); ++i)
		{
			ALWAYS_CHECK(array[i].isEmpty() == true);
		}

		array.fill("foobar");

		// Now make sure we have an array of 15 "foobar"s:
		for (uint i = 0; i < array.getSize(); ++i)
		{
			ALWAYS_CHECK(array[i] == "foobar");
		}

		ALWAYS_CHECK(array.findUnsortedMatch("foobar") == array.begin());
		ALWAYS_CHECK(array.countAllMatching("foobar")  == 15);

		// Cleanup and test array states again:
		array.deallocate();
		ALWAYS_CHECK(array.getSize()     == 0);
		ALWAYS_CHECK(array.isEmpty()     == true);
		ALWAYS_CHECK(array.isAllocated() == false);
	}
}

// ========================================================
// Test_PtrArray():
// ========================================================

struct PtrArrayTestType
{
	double dummyA;
	int    dummyB;
	static int instanceCount;

	PtrArrayTestType()
	{
		++instanceCount;
		/*LOG_INFO(" PtrArrayTestType() -- " << instanceCount);*/
	}

	~PtrArrayTestType()
	{
		--instanceCount;
		/*LOG_INFO("~PtrArrayTestType() -- " << instanceCount);*/
	}
};
int PtrArrayTestType::instanceCount = 0;

void Test_PtrArray()
{
	PtrArray<PtrArrayTestType> array;

	// Validate initial states:
	ALWAYS_CHECK(array.isEmpty()     == true);
	ALWAYS_CHECK(array.isValid()     == false);
	ALWAYS_CHECK(array.isAllocated() == false);
	ALWAYS_CHECK(array.getCapacity() == 0);
	ALWAYS_CHECK(array.getSize()     == 0);
	ALWAYS_CHECK(array.begin()       == array.end());
	ALWAYS_CHECK(array.revBegin()    == array.revEnd());
	ALWAYS_CHECK(begin(array)        == end(array));
	ALWAYS_CHECK(revBegin(array)     == revEnd(array));
	ALWAYS_CHECK(PtrArrayTestType::instanceCount == 0);

	// Push a few:
	array.pushBack(new PtrArrayTestType());
	array.pushBack(new PtrArrayTestType());
	array.pushBack(new PtrArrayTestType());
	array.pushBack(new PtrArrayTestType());
	ALWAYS_CHECK(array.getSize() == 4);
	ALWAYS_CHECK(arrayLength(array) == array.getSize());
	ALWAYS_CHECK(PtrArrayTestType::instanceCount == 4);

	// Pop a few:
	array.popBack();
	array.popBack(2);
	ALWAYS_CHECK(array.getSize() == 1);
	ALWAYS_CHECK(arrayLength(array) == array.getSize());
	ALWAYS_CHECK(PtrArrayTestType::instanceCount == 1);

	// Push again:
	array.pushBack(new PtrArrayTestType());
	array.pushBack(new PtrArrayTestType());
	ALWAYS_CHECK(array.getSize() == 3);
	ALWAYS_CHECK(arrayLength(array) == array.getSize());
	ALWAYS_CHECK(PtrArrayTestType::instanceCount == 3);

	// Clear:
	array.clear();
	ALWAYS_CHECK(array.getSize() == 0);
	ALWAYS_CHECK(array.isEmpty() == true);
	ALWAYS_CHECK(arrayLength(array) == array.getSize());
	ALWAYS_CHECK(PtrArrayTestType::instanceCount == 0);

	// Resize:
	array.resize(10);
	ALWAYS_CHECK(array.getSize()     == 10);
	ALWAYS_CHECK(array.getCapacity() >= 10);
	ALWAYS_CHECK(PtrArrayTestType::instanceCount == 0); // All new elements are null, this shouldn't change.
	array.clear();

	// Insert:
	array.insert(new PtrArrayTestType(), 0);
	array.insert(new PtrArrayTestType(), 0);
	array.insert(new PtrArrayTestType(), 1);
	array.insert(new PtrArrayTestType(), 1);
	ALWAYS_CHECK(array.getSize() == 4);
	ALWAYS_CHECK(PtrArrayTestType::instanceCount == 4);

	// Remove:
	array.remove(1);
	ALWAYS_CHECK(array.getSize() == 3);
	ALWAYS_CHECK(PtrArrayTestType::instanceCount == 3);

	array.remove(0, array.getSize());
	ALWAYS_CHECK(array.getSize() == 0);
	ALWAYS_CHECK(array.isEmpty() == true);
	ALWAYS_CHECK(PtrArrayTestType::instanceCount == 0);
}

// ========================================================
// Test_Arrays():
// ========================================================

void Test_Arrays()
{
	// Tests for the Array<T> template class:
	Test_arrayAllocResizeTransferCopy();
	Test_arrayInsertion();
	Test_arrayRemoval();
	Test_arrayIteration();
	Test_arrayMiscellaneous();

	// Tests for the specialized array of pointers template:
	Test_PtrArray();
}

} // namespace unittest {}
} // namespace core {}
} // namespace atlas {}
