
// ================================================================================================
// -*- C++ -*-
// File: test_intrusive_lists.cpp
// Author: Guilherme R. Lampert
// Created on: 06/03/15
// Brief: Unit tests for the intrusive linked lists IntrusiveSList and IntrusiveDList.
// ================================================================================================

#include "atlas/core/containers/intrusive_slist.hpp"
#include "atlas/core/containers/intrusive_dlist.hpp"
#include "atlas/core/strings/dynamic_string.hpp"
#include <cstdarg>

namespace atlas
{
namespace core
{
namespace unittest
{

// ========================================================
// Local helper types:
// ========================================================

class MyListItem
	: public DListNode<MyListItem>
	, public SListNode<MyListItem>
{
public:
	String name;
	MyListItem() DEFAULTED_METHOD;
	MyListItem(const String & s) : name(s) { }
	bool isLinked() const { return isLinkedToDList() || isLinkedToSList(); }
	bool operator == (const MyListItem & other) const { return name == other.name; }
};

bool isAAA(const MyListItem & item)
{
	return item.name == "AAA";
}

typedef IntrusiveDList<MyListItem> MyDList;
typedef IntrusiveSList<MyListItem> MySList;

// ========================================================
// assertContents<LIST_TYPE>():
// ========================================================

template<class LIST_TYPE>
void assertContents(const LIST_TYPE & list, const char * first, ...)
{
	va_list vaList;
	const char * next = first;
	typename LIST_TYPE::Iterator it = begin(list);

	va_start(vaList, first);
	while (next != nullptr)
	{
		ALWAYS_CHECK(it->name == next && "Unexpected item in list!");
		next = va_arg(vaList, const char *);
		++it;
	}
	va_end(vaList);
}

// ========================================================
// genericIntrusiveListTest<LIST_TYPE, ITEM_TYPE>():
// ========================================================

template<class LIST_TYPE, class ITEM_TYPE>
void genericIntrusiveListTest()
{
	LIST_TYPE myList;

	// Test properties of empty list:
	ALWAYS_CHECK(myList.getSize() == 0);
	ALWAYS_CHECK(myList.isEmpty() == true);
	ALWAYS_CHECK(myList.front()   == nullptr);
	ALWAYS_CHECK(myList.back()    == nullptr);
	ALWAYS_CHECK(myList.begin()   == myList.end());

	// Add some items:
	ITEM_TYPE item0("i0");
	ITEM_TYPE item1("i1");
	ITEM_TYPE item2("i2");
	ITEM_TYPE item3("i3");
	ITEM_TYPE item4("i4");
	ITEM_TYPE item5("i5");
	ITEM_TYPE item6("i6");
	ITEM_TYPE item7("i7");

	// pushFront order:
	myList.pushFront(&item0);
	myList.pushFront(&item1);
	myList.pushFront(&item2);
	myList.pushFront(&item3);
	myList.pushFront(&item4);
	myList.pushFront(&item5);
	myList.pushFront(&item6);
	myList.pushFront(&item7);
	ALWAYS_CHECK(myList.getSize() == 8);
	assertContents(myList, "i7", "i6", "i5", "i4", "i3", "i2", "i1", "i0", nullptr);

	// Clear and validate node links:
	myList.clear();
	ALWAYS_CHECK(myList.isEmpty() == true);
	ALWAYS_CHECK(myList.getSize() == 0);
	ALWAYS_CHECK(item0.isLinked() == false);
	ALWAYS_CHECK(item1.isLinked() == false);
	ALWAYS_CHECK(item2.isLinked() == false);
	ALWAYS_CHECK(item3.isLinked() == false);
	ALWAYS_CHECK(item4.isLinked() == false);
	ALWAYS_CHECK(item5.isLinked() == false);
	ALWAYS_CHECK(item6.isLinked() == false);
	ALWAYS_CHECK(item7.isLinked() == false);

	// pushBack order:
	myList.pushBack(&item0);
	myList.pushBack(&item1);
	myList.pushBack(&item2);
	myList.pushBack(&item3);
	myList.pushBack(&item4);
	myList.pushBack(&item5);
	myList.pushBack(&item6);
	myList.pushBack(&item7);
	ALWAYS_CHECK(myList.getSize() == 8);
	assertContents(myList, "i0", "i1", "i2", "i3", "i4", "i5", "i6", "i7", nullptr);

	// insert:
	ITEM_TYPE newItem0("AAA");
	ITEM_TYPE newItem1("AAA");
	myList.insert(begin(myList), &newItem0);
	myList.insert(end(myList),   &newItem1);
	ALWAYS_CHECK(myList.getSize() == 10);
	assertContents(myList, "AAA", "i0", "i1", "i2", "i3", "i4", "i5", "i6", "i7", "AAA", nullptr);

	// find:
	const typename LIST_TYPE::Iterator i4Iter = myList.find(ITEM_TYPE("i4"));
	ALWAYS_CHECK(i4Iter != end(myList));
	ALWAYS_CHECK(i4Iter->name == "i4");

	// insert between "i3" and "i4":
	ITEM_TYPE newItem2("BBB");
	const typename LIST_TYPE::Iterator bbbIter = myList.insert(i4Iter, &newItem2);
	ALWAYS_CHECK(bbbIter->name == "BBB");
	ALWAYS_CHECK(myList.getSize() == 11);
	assertContents(myList, "AAA", "i0", "i1", "i2", "i3", "BBB", "i4", "i5", "i6", "i7", "AAA", nullptr);

	// Find with predicate:
	const typename LIST_TYPE::Iterator aaaIter = myList.find(isAAA);
	ALWAYS_CHECK(aaaIter != end(myList));
	ALWAYS_CHECK(aaaIter->name == "AAA");

	// Remove "BBB" item:
	const typename LIST_TYPE::Iterator bbbNext = myList.remove(bbbIter);
	ALWAYS_CHECK(bbbNext->name == "i4");
	ALWAYS_CHECK(myList.getSize() == 10);
	ALWAYS_CHECK(newItem2.name == "BBB" && newItem2.isLinked() == false);
	assertContents(myList, "AAA", "i0", "i1", "i2", "i3", "i4", "i5", "i6", "i7", "AAA", nullptr);

	// Insert another "AAA" item:
	ITEM_TYPE newItem3("AAA");
	myList.insert(myList.find(ITEM_TYPE("i2")), &newItem3);
	ALWAYS_CHECK(myList.getSize() == 11);
	assertContents(myList, "AAA", "i0", "i1", "AAA", "i2", "i3", "i4", "i5", "i6", "i7", "AAA", nullptr);

	// Remove all matching "AAA", shrinking list back to 8 items:
	ALWAYS_CHECK(myList.removeIf(isAAA) == 3);
	ALWAYS_CHECK(myList.getSize() == 8);
	assertContents(myList, "i0", "i1", "i2", "i3", "i4", "i5", "i6", "i7", nullptr);

	// Test front()/back() and pop:
	ALWAYS_CHECK(myList.front()->name    == "i0");
	ALWAYS_CHECK(myList.back()->name     == "i7");
	ALWAYS_CHECK(myList.popFront()->name == "i0");
	ALWAYS_CHECK(myList.popBack()->name  == "i7");
	ALWAYS_CHECK(myList.front()->name    == "i1");
	ALWAYS_CHECK(myList.back()->name     == "i6");
	ALWAYS_CHECK(myList.getSize() == 6);
	assertContents(myList, "i1", "i2", "i3", "i4", "i5", "i6", nullptr);

	// Misc tests with begin/end/iterators:
	ALWAYS_CHECK(myList.begin() != myList.end());
	ALWAYS_CHECK(myList.begin() == myList.begin());
	ALWAYS_CHECK(myList.end()   == myList.end());

	// Test list transfer:
	LIST_TYPE newList;
	myList.transferTo(newList);
	ALWAYS_CHECK(myList.isEmpty()  == true);
	ALWAYS_CHECK(myList.getSize()  == 0);
	ALWAYS_CHECK(newList.isEmpty() == false);
	ALWAYS_CHECK(newList.getSize() == 6);
	assertContents(newList, "i1", "i2", "i3", "i4", "i5", "i6", nullptr);

	// Test splice:
	ITEM_TYPE newItem4("CCC");
	ITEM_TYPE newItem5("DDD");
	ITEM_TYPE newItem6("EEE");
	ITEM_TYPE newItem7("FFF");
	myList.pushBack(&newItem4);
	myList.pushBack(&newItem5);
	myList.pushBack(&newItem6);
	myList.pushBack(&newItem7);

	// Append myList to the end of newList, clearing myList:
	newList.splice(myList);
	ALWAYS_CHECK(myList.isEmpty()  == true);
	ALWAYS_CHECK(myList.getSize()  == 0);
	ALWAYS_CHECK(newList.isEmpty() == false);
	ALWAYS_CHECK(newList.getSize() == 10);
	assertContents(newList, "i1", "i2", "i3", "i4", "i5", "i6", "CCC", "DDD", "EEE", "FFF", nullptr);

	// Pop until empty:
	while (newList.popFront()) { }
	ALWAYS_CHECK(newList.isEmpty() == true);
	ALWAYS_CHECK(newList.getSize() == 0);
}

// ========================================================
// Test_IntrusiveLists():
// ========================================================

void Test_IntrusiveLists()
{
	// Test IntrusiveDList:
	genericIntrusiveListTest<MyDList, MyListItem>();

	// Test IntrusiveSList:
	genericIntrusiveListTest<MySList, MyListItem>();
}

} // namespace unittest {}
} // namespace core {}
} // namespace atlas {}
