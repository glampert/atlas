
// ================================================================================================
// -*- C++ -*-
// File: test_filesys.cpp
// Author: Guilherme R. Lampert
// Created on: 20/06/15
// Brief: Miscellaneous unit tests for the File System and file access wrappers.
// ================================================================================================

#include "atlas/core/fs/file_handling.hpp"
#include "atlas/core/memory/mem_utils.hpp"

namespace atlas
{
namespace core
{
namespace unittest
{

// ========================================================
// Auxiliary test data:
// ========================================================

const char * testFileName = "test_file.bin";

struct ATTRIBUTE_PACKED TestData
{
	bool   boolValue;
	int8   int8Value;
	uint8  uint8Value;
	int16  int16Value;
	uint16 uint16Value;
	int32  int32Value;
	uint32 uint32Value;
	int64  int64Value;
	uint64 uint64Value;
	float  floatValue;
	double doubleValue;
	ubyte  byteSequence[10];
};

const TestData templateData =
{
	// Just some arbitrary test data we write to file,
	// then read back to compare the results.
	true,
	0x1B,
	0x2D,
	0x0123,
	0xBEEF,
	0x01234567,
	0xCAFECAFE,
	0x0123456789101112,
	0xCAFED00DCAFEBABE,
	3.14f,
	2.71,
	{ 0xA1, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7, 0xA8, 0xA9, 0xB0 }
};

bool compareTestData(const TestData & a, const TestData & b)
{
	if (a.boolValue   != b.boolValue)   { return false; }
	if (a.int8Value   != b.int8Value)   { return false; }
	if (a.uint8Value  != b.uint8Value)  { return false; }
	if (a.int16Value  != b.int16Value)  { return false; }
	if (a.uint16Value != b.uint16Value) { return false; }
	if (a.int32Value  != b.int32Value)  { return false; }
	if (a.uint32Value != b.uint32Value) { return false; }
	if (a.int64Value  != b.int64Value)  { return false; }
	if (a.uint64Value != b.uint64Value) { return false; }
	if (a.floatValue  != b.floatValue)  { return false; }
	if (a.doubleValue != b.doubleValue) { return false; }
	if (std::memcmp(a.byteSequence, b.byteSequence, 10) != 0) { return false; }
	return true;
}

// ========================================================
// Test_WritableFile():
// ========================================================

void Test_WritableFile(WritableFile & outFile)
{
	// Default expected states for a closed file.
	ALWAYS_CHECK(outFile.isOpen()         == false);
	ALWAYS_CHECK(outFile.isAtEndOfFile()  == true );
	ALWAYS_CHECK(outFile.isInErrorState() == true );
	ALWAYS_CHECK(outFile.isText()         == false);
	ALWAYS_CHECK(outFile.isBinary()       == false);
	ALWAYS_CHECK(outFile.isReadable()     == false);
	ALWAYS_CHECK(outFile.isWritable()     == false);
	ALWAYS_CHECK(outFile.getSizeInBytes() == 0    );

	// First attempt with `NoCreate`, should fail.
	uint openFlags = File::Flag::Binary | File::Flag::Writable | File::Flag::NoCreate;
	ALWAYS_CHECK(outFile.open(testFileName, openFlags) == false);

	// Second attempt with no restrictions must succeed.
	openFlags = File::Flag::Binary | File::Flag::Writable | File::Flag::Truncate;
	ALWAYS_CHECK(outFile.open(testFileName, openFlags) == true);

	// Validate internal object states:
	ALWAYS_CHECK(outFile.isOpen()         == true );
	ALWAYS_CHECK(outFile.isAtEndOfFile()  == false);
	ALWAYS_CHECK(outFile.isInErrorState() == false);
	ALWAYS_CHECK(outFile.isText()         == false);
	ALWAYS_CHECK(outFile.isBinary()       == true );
	ALWAYS_CHECK(outFile.isReadable()     == false);
	ALWAYS_CHECK(outFile.isWritable()     == true );
	ALWAYS_CHECK(outFile.getSizeInBytes() == 0    );

	//
	// Write some binary data now:
	//

	ALWAYS_CHECK(outFile.writeBool(templateData.boolValue)     == true);
	ALWAYS_CHECK(outFile.writeInt8(templateData.int8Value)     == true);
	ALWAYS_CHECK(outFile.writeUint8(templateData.uint8Value)   == true);
	ALWAYS_CHECK(outFile.writeInt16(templateData.int16Value)   == true);
	ALWAYS_CHECK(outFile.writeUint16(templateData.uint16Value) == true);
	ALWAYS_CHECK(outFile.writeInt32(templateData.int32Value)   == true);
	ALWAYS_CHECK(outFile.writeUint32(templateData.uint32Value) == true);
	ALWAYS_CHECK(outFile.writeInt64(templateData.int64Value)   == true);
	ALWAYS_CHECK(outFile.writeUint64(templateData.uint64Value) == true);
	ALWAYS_CHECK(outFile.writeFloat(templateData.floatValue)   == true);
	ALWAYS_CHECK(outFile.writeDouble(templateData.doubleValue) == true);

	const uint writeCount = outFile.writeBytes(templateData.byteSequence, arrayLength(templateData.byteSequence));
	ALWAYS_CHECK(writeCount == arrayLength(templateData.byteSequence));

	// Size should have been updated now.
	ALWAYS_CHECK(outFile.getSizeInBytes() == sizeof(TestData));

	// Test_ReadableFile() will now attempt to read the data.
	outFile.close();
	ALWAYS_CHECK(outFile.isOpen() == false);
}

// ========================================================
// Test_ReadableFile():
// ========================================================

void Test_ReadableFile(ReadableFile & inFile)
{
	// Default expected states for a closed file.
	ALWAYS_CHECK(inFile.isOpen()         == false);
	ALWAYS_CHECK(inFile.isAtEndOfFile()  == true );
	ALWAYS_CHECK(inFile.isInErrorState() == true );
	ALWAYS_CHECK(inFile.isText()         == false);
	ALWAYS_CHECK(inFile.isBinary()       == false);
	ALWAYS_CHECK(inFile.isReadable()     == false);
	ALWAYS_CHECK(inFile.isWritable()     == false);
	ALWAYS_CHECK(inFile.getSizeInBytes() == 0    );

	// Open the file created previously by Test_WritableFile():
	ALWAYS_CHECK(inFile.open(testFileName, File::Flag::Readable | File::Flag::Binary) == true);

	// Validate internal object states:
	ALWAYS_CHECK(inFile.isOpen()         == true );
	ALWAYS_CHECK(inFile.isAtEndOfFile()  == false);
	ALWAYS_CHECK(inFile.isInErrorState() == false);
	ALWAYS_CHECK(inFile.isText()         == false);
	ALWAYS_CHECK(inFile.isBinary()       == true );
	ALWAYS_CHECK(inFile.isReadable()     == true );
	ALWAYS_CHECK(inFile.isWritable()     == false);
	ALWAYS_CHECK(inFile.getSizeInBytes() != 0    );

	//
	// Read binary data written by the previous test:
	//

	TestData dataGot;
	clearPodObject(dataGot);

	ALWAYS_CHECK(inFile.readBool(dataGot.boolValue)     == true);
	ALWAYS_CHECK(inFile.readInt8(dataGot.int8Value)     == true);
	ALWAYS_CHECK(inFile.readUint8(dataGot.uint8Value)   == true);
	ALWAYS_CHECK(inFile.readInt16(dataGot.int16Value)   == true);
	ALWAYS_CHECK(inFile.readUint16(dataGot.uint16Value) == true);
	ALWAYS_CHECK(inFile.readInt32(dataGot.int32Value)   == true);
	ALWAYS_CHECK(inFile.readUint32(dataGot.uint32Value) == true);
	ALWAYS_CHECK(inFile.readInt64(dataGot.int64Value)   == true);
	ALWAYS_CHECK(inFile.readUint64(dataGot.uint64Value) == true);
	ALWAYS_CHECK(inFile.readFloat(dataGot.floatValue)   == true);
	ALWAYS_CHECK(inFile.readDouble(dataGot.doubleValue) == true);

	uint readCount = inFile.readBytes(dataGot.byteSequence, arrayLength(dataGot.byteSequence));

	ALWAYS_CHECK(readCount == arrayLength(dataGot.byteSequence));
	ALWAYS_CHECK(compareTestData(templateData, dataGot) == true);

	//
	// Rewind and read again in as a single block.
	// If we don't have endianness or padding issues, it should still succeed.
	//
	inFile.rewind();

	clearPodObject(dataGot);
	readCount = inFile.readBytes(&dataGot, sizeof(dataGot));

	ALWAYS_CHECK(readCount == sizeof(dataGot));
	ALWAYS_CHECK(compareTestData(templateData, dataGot) == true);

	inFile.close();
	ALWAYS_CHECK(inFile.isOpen() == false);
}

// ========================================================
// Test_ReadWriteFile():
// ========================================================

void Test_ReadWriteFile(ReadWriteFile & inOutFile)
{
	// First, make sure that a RW file is fully compatible
	// with its parent types: ReadableFile and WritableFile.
	Test_WritableFile(inOutFile);
	Test_ReadableFile(inOutFile);

	//
	// Now we'll test it in TEXT mode.
	//
	const uint openFlags = File::Flag::Readable | File::Flag::Writable | File::Flag::Text | File::Flag::Truncate;
	ALWAYS_CHECK(inOutFile.open(testFileName, openFlags) == true);

	ALWAYS_CHECK(inOutFile.isOpen()         == true );
	ALWAYS_CHECK(inOutFile.isAtEndOfFile()  == false);
	ALWAYS_CHECK(inOutFile.isInErrorState() == false);
	ALWAYS_CHECK(inOutFile.isText()         == true );
	ALWAYS_CHECK(inOutFile.isBinary()       == false);
	ALWAYS_CHECK(inOutFile.isReadable()     == true );
	ALWAYS_CHECK(inOutFile.isWritable()     == true );
	ALWAYS_CHECK(inOutFile.isReadWrite()    == true );
	ALWAYS_CHECK(inOutFile.getSizeInBytes() == 0    );

	// Write some data:
	ALWAYS_CHECK(inOutFile.writeChar('*')  == true);
	ALWAYS_CHECK(inOutFile.writeChar('\n') == true);
	ALWAYS_CHECK(inOutFile.writeString("Hello File!\n") == true);
	ALWAYS_CHECK(inOutFile.printF("Testing a %s function.\n", "format") != 0);

	// Flush and rewind:
	ALWAYS_CHECK(inOutFile.flush()  == true);
	ALWAYS_CHECK(inOutFile.rewind() == true);

	ALWAYS_CHECK(inOutFile.getSizeInBytes() != 0);
	ALWAYS_CHECK(inOutFile.isAtEndOfFile()  == false);
	ALWAYS_CHECK(inOutFile.isInErrorState() == false);

	// Read data back and validate:
	char ch = 0;
	ALWAYS_CHECK(inOutFile.readChar(ch) == true && ch == '*' );
	ALWAYS_CHECK(inOutFile.readChar(ch) == true && ch == '\n');

	String line1, line2;
	ALWAYS_CHECK(inOutFile.readLine(line1) == true && line1 == "Hello File!\n");
	ALWAYS_CHECK(inOutFile.readLine(line2) == true && line2 == "Testing a format function.\n");

	inOutFile.close();
	ALWAYS_CHECK(inOutFile.isOpen() == false);
}

// ========================================================
// Test_FileSystem():
// ========================================================

void Test_FileSystem()
{
	//
	// A temporary file is created by the following FS tests.
	// This test suite will thus require the application to have
	// write permissions to the current directory. Otherwise
	// the first test function should fail.
	//
	ALWAYS_CHECK(FileSystem::pathExistAndIsFile(testFileName) == false);
	ALWAYS_CHECK(FileSystem::queryFileSize(testFileName) == 0);

	WritableFile  wFile;
	ReadableFile  rFile;
	ReadWriteFile rwFile;

	Test_WritableFile(wFile);
	Test_ReadableFile(rFile);
	FileSystem::removeFile(testFileName); // Remove intermediate test file.

	Test_ReadWriteFile(rwFile);
	FileSystem::removeFile(testFileName); // Remove final test file.

	// Make sure they have cleaned after themselves.
	ALWAYS_CHECK(FileSystem::pathExistAndIsFile(testFileName) == false);
	ALWAYS_CHECK(FileSystem::queryFileSize(testFileName) == 0);
}

} // namespace unittest {}
} // namespace core { }
} // namespace atlas { }
