
// ================================================================================================
// -*- C++ -*-
// File: test_path_utils.cpp
// Author: Guilherme R. Lampert
// Created on: 22/12/14
// Brief: Unit tests for the File System path utils.
// ================================================================================================

#include "atlas/core/fs/path_utils.hpp"
#include "atlas/core/strings.hpp"

namespace atlas
{
namespace core
{
namespace unittest
{

// ========================================================
// Test_PathAndFilenameFunctions():
// ========================================================

void Test_PathAndFilenameFunctions()
{
	const String path0 = "aaa/bbb/ccc/ddd/file.txt"; // ended with filename
	const String path1 = "aaa/bbb/ccc/ddd/";         // ended with a path separator
	const String path2 = "file.txt";                 // just a filename
	const String path3 = "/file.txt";                // filename starting with a separator

	// stripPath():
	{
		bool hadPath = false;
		ALWAYS_CHECK(stripPath(path0, &hadPath) == "file.txt" && hadPath == true );
		ALWAYS_CHECK(stripPath(path1, &hadPath) == ""         && hadPath == true );
		ALWAYS_CHECK(stripPath(path2, &hadPath) == "file.txt" && hadPath == false);
		ALWAYS_CHECK(stripPath(path3, &hadPath) == "file.txt" && hadPath == true );
	}

	// stripFilename():
	{
		bool hadFilename = false;
		ALWAYS_CHECK(stripFilename(path0, &hadFilename) == "aaa/bbb/ccc/ddd/" && hadFilename == true );
		ALWAYS_CHECK(stripFilename(path1, &hadFilename) == "aaa/bbb/ccc/ddd/" && hadFilename == false);
		ALWAYS_CHECK(stripFilename(path2, &hadFilename) == ""                 && hadFilename == true );
		ALWAYS_CHECK(stripFilename(path3, &hadFilename) == "/"                && hadFilename == true );
	}

	// stripFilenameExtension():
	{
		bool hadExt = false;
		ALWAYS_CHECK(stripFilenameExtension(path0, &hadExt) == "aaa/bbb/ccc/ddd/file" && hadExt == true );
		ALWAYS_CHECK(stripFilenameExtension(path1, &hadExt) == "aaa/bbb/ccc/ddd/"     && hadExt == false);
		ALWAYS_CHECK(stripFilenameExtension(path2, &hadExt) == "file"                 && hadExt == true );
		ALWAYS_CHECK(stripFilenameExtension(path3, &hadExt) == "/file"                && hadExt == true );
	}

	// replaceFilenameExtension():
	{
		ALWAYS_CHECK(replaceFilenameExtension(path0, String(".foo")) == "aaa/bbb/ccc/ddd/file.foo");
		ALWAYS_CHECK(replaceFilenameExtension(path1, String(".bar")) == "aaa/bbb/ccc/ddd/"        );
		ALWAYS_CHECK(replaceFilenameExtension(path2, String("fooz")) == "file.fooz"               );
		ALWAYS_CHECK(replaceFilenameExtension(path3, String("barz")) == "/file.barz"              );
	}

	// getFilenameExtension():
	{
		// With the last dot (the default behavior):
		ALWAYS_CHECK(getFilenameExtension(path0) == ".txt");
		ALWAYS_CHECK(getFilenameExtension(path1) == ""    );
		ALWAYS_CHECK(getFilenameExtension(path2) == ".txt");
		ALWAYS_CHECK(getFilenameExtension(path3) == ".txt");
		// Special case, a lost dot with nothing after:
		ALWAYS_CHECK(getFilenameExtension(String("file.")) == ".");

		// Without the last dot:
		ALWAYS_CHECK(getFilenameExtension(path0, false) == "txt");
		ALWAYS_CHECK(getFilenameExtension(path1, false) == ""   );
		ALWAYS_CHECK(getFilenameExtension(path2, false) == "txt");
		ALWAYS_CHECK(getFilenameExtension(path3, false) == "txt");
		// Special case, a lost dot with nothing after:
		ALWAYS_CHECK(getFilenameExtension(String("file."), false) == "");
	}
}

} // namespace unittest {}
} // namespace core {}
} // namespace atlas {}
