
// ================================================================================================
// -*- C++ -*-
// File: test_cpp_strings.cpp
// Author: Guilherme R. Lampert
// Created on: 21/12/14
// Brief: Unit tests for the C++ string classes.
// ================================================================================================

#include "atlas/core/strings/string_base.hpp"
#include "atlas/core/strings/sized_string.hpp"
#include "atlas/core/strings/dynamic_string.hpp"
#include "atlas/core/strings/to_string.hpp"
#include "atlas/core/utils/utility.hpp"

namespace atlas
{
namespace core
{
namespace unittest
{

// ========================================================
// Test_sizedString():
// ========================================================

void Test_sizedString()
{
	typedef SizedString<128> MyStr;

	// An empty string:
	{
		MyStr emptyStr;
		ALWAYS_CHECK(emptyStr.isEmpty());
		ALWAYS_CHECK(emptyStr.isValid());
		ALWAYS_CHECK(emptyStr.getLength()  == 0);
		ALWAYS_CHECK(emptyStr.getCStr()[0] == '\0');
	}

	// indexOf():
	{
		MyStr s("Hello.txt.bmp.txt");
		ALWAYS_CHECK(s.lastIndexOfIgnoreCase(".BMP")  == 9);
		ALWAYS_CHECK(s.firstIndexOfIgnoreCase(".TXT") == 5);
	}

	// fill() and operator == :
	{
		MyStr s("123456789");
		s.fill('#');

		ALWAYS_CHECK(s.getLength() == 9);
		ALWAYS_CHECK(s == "#########");
		ALWAYS_CHECK("#########" == s);
	}

	// replace() and operator == :
	{
		MyStr s;

		s = "foofoofoo";
		s.replace("foo", "bar", 3);
		ALWAYS_CHECK(s.getLength() == 9);
		ALWAYS_CHECK(s == "barbarbar");
		ALWAYS_CHECK("barbarbar" == s);

		s = "foo";
		s.replace("foo", "bar", 3);
		ALWAYS_CHECK(s.getLength() == 3);
		ALWAYS_CHECK(s == "bar");
		ALWAYS_CHECK("bar" == s);

		s = "|fooz|+|fooz|-|fooz|";
		s.replace("foo", "bar", 3);
		ALWAYS_CHECK(s.getLength() == 20);
		ALWAYS_CHECK(s == "|barz|+|barz|-|barz|");
		ALWAYS_CHECK("|barz|+|barz|-|barz|" == s);
	}

	// Iterators:
	{
		MyStr s("Hello World");
		MyStr::Iterator iter = s.begin();
		ALWAYS_CHECK(s.begin() == begin(s));

		iter++;
		++iter;

		iter--;
		--iter;

		ALWAYS_CHECK(iter == iter);
		ALWAYS_CHECK(!(iter != iter));

		(*iter) = 'X';
		ALWAYS_CHECK(*iter == 'X');

		ALWAYS_CHECK(s == "Xello World");
		ALWAYS_CHECK("Xello World" == s);
	}

	// extractSubstring() and constructors:
	{
		MyStr s1("hello, hello, world, worldly");
		ALWAYS_CHECK(s1.extractSubstring("world") == "world");

		// Count overflow:
		MyStr s2 = "ABC";
		ALWAYS_CHECK(s2.extractSubstring(1, 999) == "BC");

		MyStr s3(s1, 21, 7);
		s3 += " + " + s1;
		ALWAYS_CHECK(s3 == "worldly + hello, hello, world, worldly");

		MyStr str0("hello world");
		MyStr str1(str0);
		MyStr str2(str1);

		ALWAYS_CHECK(str0 == str1);
		ALWAYS_CHECK(str1 == str2);
		ALWAYS_CHECK(str1.endsWith("world"));
		ALWAYS_CHECK(str2.endsWith(str1));
	}
}

// ========================================================
// Test_dynamicString():
// ========================================================

void Test_dynamicString()
{
	typedef String MyStr;

	// An empty string:
	{
		MyStr emptyStr;
		ALWAYS_CHECK(emptyStr.isEmpty());
		ALWAYS_CHECK(emptyStr.isValid());
		ALWAYS_CHECK(emptyStr.getLength()  == 0);
		ALWAYS_CHECK(emptyStr.getCStr()[0] == '\0');
	}

	// indexOf():
	{
		MyStr s("Hello.txt.bmp.txt");
		ALWAYS_CHECK(s.lastIndexOfIgnoreCase(".BMP")  == 9);
		ALWAYS_CHECK(s.firstIndexOfIgnoreCase(".TXT") == 5);
	}

	// fill() and operator == :
	{
		MyStr s("123456789");
		s.fill('#');

		ALWAYS_CHECK(s.getLength() == 9);
		ALWAYS_CHECK(s == "#########");
		ALWAYS_CHECK("#########" == s);
	}

	// replace() and operator == :
	{
		MyStr s;

		s = "foofoofoo";
		s.replace("foo", "bar", 3);
		ALWAYS_CHECK(s.getLength() == 9);
		ALWAYS_CHECK(s == "barbarbar");
		ALWAYS_CHECK("barbarbar" == s);

		s = "foo";
		s.replace("foo", "bar", 3);
		ALWAYS_CHECK(s.getLength() == 3);
		ALWAYS_CHECK(s == "bar");
		ALWAYS_CHECK("bar" == s);

		s = "|fooz|+|fooz|-|fooz|";
		s.replace("foo", "bar", 3);
		ALWAYS_CHECK(s.getLength() == 20);
		ALWAYS_CHECK(s == "|barz|+|barz|-|barz|");
		ALWAYS_CHECK("|barz|+|barz|-|barz|" == s);
	}

	// Iterators:
	{
		MyStr s("Hello World");
		MyStr::Iterator iter = s.begin();
		ALWAYS_CHECK(s.begin() == begin(s));

		iter++;
		++iter;

		iter--;
		--iter;

		ALWAYS_CHECK(iter == iter);
		ALWAYS_CHECK(!(iter != iter));

		(*iter) = 'X';
		ALWAYS_CHECK(*iter == 'X');

		ALWAYS_CHECK(s == "Xello World");
		ALWAYS_CHECK("Xello World" == s);
	}

	// extractSubstring() and constructors:
	{
		MyStr s1("hello, hello, world, worldly");
		ALWAYS_CHECK(s1.extractSubstring("world") == "world");

		// Count overflow:
		MyStr s2 = "ABC";
		ALWAYS_CHECK(s2.extractSubstring(1, 999) == "BC");

		MyStr s3(s1, 21, 7);
		s3 += " + " + s1;
		ALWAYS_CHECK(s3 == "worldly + hello, hello, world, worldly");

		MyStr str0("hello world");
		MyStr str1(str0);
		MyStr str2(str1);

		ALWAYS_CHECK(str0 == str1);
		ALWAYS_CHECK(str1 == str2);
		ALWAYS_CHECK(str1.endsWith("world"));
		ALWAYS_CHECK(str2.endsWith(str1));
	}

	// Dynamic growth of the string and empty string with reserved memory:
	{
		MyStr emptyStr(256);
		ALWAYS_CHECK(emptyStr.isEmpty());
		ALWAYS_CHECK(emptyStr.isValid());
		ALWAYS_CHECK(emptyStr.getLength() == 0);
		ALWAYS_CHECK(emptyStr.getCStr()[0] == '\0');
		// Capacity includes the null terminator and possibly
		// some implementation defined extra space for future
		// growth of the string.
		ALWAYS_CHECK(emptyStr.getCapacity() >= 256);

		// Append some chars:
		String s0;
		s0.append("|xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx|");
		s0.append("|yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy|");
		ALWAYS_CHECK(s0.getLength()   == 194);
		ALWAYS_CHECK(s0.getCapacity() >= 194);

		// Construct filled:
		String s1(55, '+');
		ALWAYS_CHECK(s1.getLength() == 55);
	}
}

// ========================================================
// Test_scanValues():
// ========================================================

void Test_scanValues()
{
	const String intStr     = "\n 1234xyz";
	const String intStrHexa = "0xC0FFE";
	const String floatStr   = "0.123qwerty";
	const String notANumber = "Not a Number (NaN)";
	const String intTuple   = "\t ( 3, 2, 1  ) ";
	const String fltTuple   = "\t ( 1.2, 2.3 ) ";

	// int32:
	{
		int32 number = 0;
		uint firstUnread = 0;
		bool success = intStr.scanInt(number, 0, &firstUnread);

		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(firstUnread == 6);
		ALWAYS_CHECK(number == 1234);
	}

	// int64:
	{
		int64 number = 0;
		uint firstUnread = 0;
		bool success = intStr.scanInt(number, 0, &firstUnread);

		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(firstUnread == 6);
		ALWAYS_CHECK(number == 1234);
	}

	// Hexadecimal integer (guess base):
	{
		uint32 number = 0;
		uint firstUnread = 0;
		bool success = intStrHexa.scanInt(number, 0, &firstUnread);

		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(firstUnread == 7);
		ALWAYS_CHECK(number == 0xC0FFE);
	}

	// Float to string:
	{
		float number = 0.0f;
		uint firstUnread = 0;
		bool success = floatStr.scanFloat(number, &firstUnread);

		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(firstUnread == 5);
		ALWAYS_CHECK(number == 0.123f);
	}

	// Non-numerical string, should fail gracefully:
	{
		int number = -1;
		uint firstUnread = 0;
		bool success = notANumber.scanInt(number, 0, &firstUnread);

		ALWAYS_CHECK(success == false);
		ALWAYS_CHECK(firstUnread == 0);
		ALWAYS_CHECK(number == 0); // Value should be set to zero on failure.
	}

	// Int tuple:
	{
		int values[3] = { 0, 0, 0 };
		bool success = intTuple.scanIntTuple(values, arrayLength(values));

		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(values[0] == 3 && values[1] == 2 && values[2] == 1);
	}

	// Float tuple:
	{
		float values[2] = { 0.0f, 0.0f };
		bool success = fltTuple.scanFloatTuple(values, arrayLength(values));

		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(values[0] == 1.2f && values[1] == 2.3f);
	}

	// Scan boolean values:
	{
		bool success, value;

		//
		// "false" values:
		//
		success = String("0000").scanBool(value);
		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(value   == false);

		success = String("\t no ").scanBool(value);
		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(value   == false);

		success = String("false").scanBool(value);
		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(value   == false);

		success = String("FALSE").scanBool(value);
		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(value   == false);

		//
		// "true" values:
		//
		success = String("1111").scanBool(value);
		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(value   == true);

		success = String("\t yes ").scanBool(value);
		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(value   == true);

		success = String("true").scanBool(value);
		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(value   == true);

		success = String("TRUE").scanBool(value);
		ALWAYS_CHECK(success == true);
		ALWAYS_CHECK(value   == true);

		//
		// Non-boolean values:
		//
		success = String("LOL").scanBool(value);
		ALWAYS_CHECK(success == false);
		ALWAYS_CHECK(value   == false);

		success = String("wtf").scanBool(value);
		ALWAYS_CHECK(success == false);
		ALWAYS_CHECK(value   == false);
	}
}

// ========================================================
// Test_splitString():
// ========================================================

void Test_splitString()
{
	// Split and get substring indexes:
	{
		uint tokensFound;
		Array<String::SubIndexes> subIndexes;
		const String delimiters = "| \t\r\n";

		// ----------------------------

		subIndexes.clear();
		tokensFound = split("\t| 0 | 1 | 2 | 3 | 4 | 5 |\r\n", delimiters, subIndexes);

		ALWAYS_CHECK(tokensFound == 6);
		ALWAYS_CHECK(subIndexes.getSize() == 6);

		ALWAYS_CHECK(subIndexes[0].start ==  3 && subIndexes[0].end ==  4);
		ALWAYS_CHECK(subIndexes[1].start ==  7 && subIndexes[1].end ==  8);
		ALWAYS_CHECK(subIndexes[2].start == 11 && subIndexes[2].end == 12);
		ALWAYS_CHECK(subIndexes[3].start == 15 && subIndexes[3].end == 16);
		ALWAYS_CHECK(subIndexes[4].start == 19 && subIndexes[4].end == 20);
		ALWAYS_CHECK(subIndexes[5].start == 23 && subIndexes[5].end == 24);

		// ----------------------------

		subIndexes.clear();
		tokensFound = split("0|1|2|3|4|5", delimiters, subIndexes);

		ALWAYS_CHECK(tokensFound == 6);
		ALWAYS_CHECK(subIndexes.getSize() == 6);

		ALWAYS_CHECK(subIndexes[0].start ==  0 && subIndexes[0].end ==  1);
		ALWAYS_CHECK(subIndexes[1].start ==  2 && subIndexes[1].end ==  3);
		ALWAYS_CHECK(subIndexes[2].start ==  4 && subIndexes[2].end ==  5);
		ALWAYS_CHECK(subIndexes[3].start ==  6 && subIndexes[3].end ==  7);
		ALWAYS_CHECK(subIndexes[4].start ==  8 && subIndexes[4].end ==  9);
		ALWAYS_CHECK(subIndexes[5].start == 10 && subIndexes[5].end == 11);

		// ----------------------------

		subIndexes.clear();
		tokensFound = split("012345", delimiters, subIndexes);

		ALWAYS_CHECK(tokensFound == 1);
		ALWAYS_CHECK(subIndexes.getSize() == 1);

		ALWAYS_CHECK(subIndexes[0].start == 0 && subIndexes[0].end == 6);
	}

	// Split and get substrings:
	{
		uint tokensFound;
		Array<String> substrings;
		const String delimiters = "+ | -";

		// ----------------------------

		substrings.clear();
		tokensFound = split("-| AAA | BBB |+CCC+|+DDD+| EEE | FFF |-", delimiters, substrings);

		ALWAYS_CHECK(tokensFound == 6);
		ALWAYS_CHECK(substrings.getSize() == 6);

		ALWAYS_CHECK(substrings[0] == "AAA");
		ALWAYS_CHECK(substrings[1] == "BBB");
		ALWAYS_CHECK(substrings[2] == "CCC");
		ALWAYS_CHECK(substrings[3] == "DDD");
		ALWAYS_CHECK(substrings[4] == "EEE");
		ALWAYS_CHECK(substrings[5] == "FFF");

		// ----------------------------

		substrings.clear();
		tokensFound = split("AAA|BBB|CCC|DDD|EEE|FFF", delimiters, substrings);

		ALWAYS_CHECK(tokensFound == 6);
		ALWAYS_CHECK(substrings.getSize() == 6);

		ALWAYS_CHECK(substrings[0] == "AAA");
		ALWAYS_CHECK(substrings[1] == "BBB");
		ALWAYS_CHECK(substrings[2] == "CCC");
		ALWAYS_CHECK(substrings[3] == "DDD");
		ALWAYS_CHECK(substrings[4] == "EEE");
		ALWAYS_CHECK(substrings[5] == "FFF");

		// ----------------------------

		substrings.clear();
		tokensFound = split("AAABBBCCCDDDEEEFFF", delimiters, substrings);

		ALWAYS_CHECK(tokensFound == 1);
		ALWAYS_CHECK(substrings.getSize() == 1);

		ALWAYS_CHECK(substrings[0] == "AAABBBCCCDDDEEEFFF");
	}
}

// ========================================================
// Test_padString():
// ========================================================

void Test_padString()
{
	String pad1, pad2, pad3, pad4, pad5;
	typedef SizedString<64> SmallStr;

	// Pad to the left (beginning of the string):
	{
		pad1 = toString(0xAA,    10).padLeft(25, '-');
		pad2 = toString(0xAABBCC, 8).padLeft(25);
		pad3 = toString(0xAABB,  16).padLeft(25);
		pad4 = toString(0xAABBCC, 2).padLeft(25);
		pad5 = SmallStr("hello"    ).padLeft(25);

		ALWAYS_CHECK(pad1 == "----------------------170");
		ALWAYS_CHECK(pad2 == "                 52535714");
		ALWAYS_CHECK(pad3 == "                   0xAABB");
		ALWAYS_CHECK(pad4 == " 101010101011101111001100");
		ALWAYS_CHECK(pad5 == "                    hello");
	}

	// Pad to the right (end of the string):
	{
		pad1 = toString(0xAA,    10).padRight(25, '-');
		pad2 = toString(0xAABBCC, 8).padRight(25);
		pad3 = toString(0xAABB,  16).padRight(25);
		pad4 = toString(0xAABBCC, 2).padRight(25);
		pad5 = SmallStr("hello"    ).padRight(25);

		ALWAYS_CHECK(pad1 == "170----------------------");
		ALWAYS_CHECK(pad2 == "52535714                 ");
		ALWAYS_CHECK(pad3 == "0xAABB                   ");
		ALWAYS_CHECK(pad4 == "101010101011101111001100 ");
		ALWAYS_CHECK(pad5 == "hello                    ");
	}
}

// ========================================================
// Test_CppStrings():
// ========================================================

void Test_CppStrings()
{
	Test_sizedString();
	Test_dynamicString();
	Test_scanValues();
	Test_splitString();
	Test_padString();
}

} // namespace unittest {}
} // namespace core {}
} // namespace atlas {}
