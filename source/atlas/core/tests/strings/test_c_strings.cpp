
// ================================================================================================
// -*- C++ -*-
// File: test_c_strings.cpp
// Author: Guilherme R. Lampert
// Created on: 21/12/14
// Brief: Unit tests for the C-string manipulation functions.
// ================================================================================================

#include "atlas/core/strings/c_string.hpp"
#include "atlas/core/utils/utility.hpp"
#include <cstring>
#include <cstdlib>

namespace atlas
{
namespace core
{
namespace unittest
{

// ========================================================
// Test_strCmp():
// ========================================================

void Test_strCmp()
{
	// Test relies on this assumption.
	ALWAYS_CHECK(sizeof(char) == 1);

	// Equal strings:
	ALWAYS_CHECK(strCmp("foo", "foo") == 0);
	ALWAYS_CHECK(strCmp("FOO", "FOO") == 0);

	// Different strings:
	ALWAYS_CHECK(strCmp("fOo", "FoO") != 0);
	ALWAYS_CHECK(strCmp("FOO", "foo") != 0);
	ALWAYS_CHECK(strCmp("foo", "bar") != 0);
	ALWAYS_CHECK(strCmp("Hello", "Hello@@@") != 0);

	// strEquals() is just an alias for strCmp() == 0.
	ALWAYS_CHECK(strEquals("Hello", "Hello") == true);
	ALWAYS_CHECK(strEquals("Hello", "Hell" ) == false);

	// Test string lexicographical order:
	ALWAYS_CHECK(strCmp("aaa", "bbb") < 0); // a < b
	ALWAYS_CHECK(strCmp("ccc", "bbb") > 0); // c > b
	ALWAYS_CHECK(strCmp("1",   "2")   < 0); // 1 < 2

	// Compare with an explicit char count:
	ALWAYS_CHECK(strCmpN("1234***", "1234+++", 4) == 0); // Just the first 4
	ALWAYS_CHECK(strCmpN("1234***", "1234+++", 5) != 0); // Just the first 5

	// First string is shorter than char count:
	ALWAYS_CHECK(strCmpN("1234", "123456", 6) != 0);

	// Second string is shorter the char count:
	ALWAYS_CHECK(strCmpN("123456", "1234", 6) != 0);

	// Strings are equal but char count is less than string length:
	ALWAYS_CHECK(strCmpN("hello world", "hello world", 4) == 0);

	// Strings are equal and char count is greater than string length:
	ALWAYS_CHECK(strCmpN("hello sunshine", "hello sunshine", 128) == 0);

	// Compare ignoring case:
	ALWAYS_CHECK(strCmpIgnoreCase("AAA", "aaa") == 0);
	ALWAYS_CHECK(strCmpIgnoreCase("AAA", "bbb") != 0);
	ALWAYS_CHECK(strCmpIgnoreCase("123", "123") == 0);
	ALWAYS_CHECK(strCmpIgnoreCase("123", "456") != 0);
	ALWAYS_CHECK(strCmpIgnoreCase("AaA", "aAa") == 0);
	ALWAYS_CHECK(strCmpIgnoreCase("111", "111") <= 0);
	ALWAYS_CHECK(strCmpIgnoreCase("111", "222") <  0);
	ALWAYS_CHECK(strCmpIgnoreCase("aaa", "bbb") <  0); // a < b
	ALWAYS_CHECK(strCmpIgnoreCase("ccc", "bbb") >  0); // c > b

	// Try with explicit count:
	ALWAYS_CHECK(strCmpNIgnoreCase("AAA", "aaa+++", 3) == 0);
	ALWAYS_CHECK(strCmpNIgnoreCase("AAA", "aaa+++", 4) != 0);
}

// ========================================================
// Test_strCopy():
// ========================================================

void Test_strCopy()
{
	// Test relies on this assumption.
	ALWAYS_CHECK(sizeof(char) == 1);

	// Buffer a lot bigger than the source string:
	{
		char s[64];
		uint newLen = strCopy(s, arrayLength(s), "foobar");

		ALWAYS_CHECK(newLen == 6);
		ALWAYS_CHECK(strEquals(s, "foobar"));
	}

	// Just enough space for the string + '\0' at the end:
	{
		char s[4];
		uint newLen = strCopy(s, arrayLength(s), "123");

		ALWAYS_CHECK(newLen == 3);
		ALWAYS_CHECK(strEquals(s, "123"));
	}

	// String will be truncated:
	{
		char s[6];
		uint newLen = strCopy(s, arrayLength(s), "Hello World");

		ALWAYS_CHECK(newLen == 5);
		ALWAYS_CHECK(strEquals(s, "Hello"));
	}

	// Copy less chars than the whole source string:
	{
		char s[64];
		uint newLen = strCopyN(s, arrayLength(s), "Hello World", 5);

		ALWAYS_CHECK(newLen == 5);
		ALWAYS_CHECK(strEquals(s, "Hello"));
	}

	// Source string is shorter than `charsToCopy`:
	{
		char s[64];
		uint newLen = strCopyN(s, arrayLength(s), "World", 40);

		ALWAYS_CHECK(newLen == 5);
		ALWAYS_CHECK(strEquals(s, "World"));
	}

	// Source string is shorter than `charsToCopy` and dest is too small:
	{
		char s[4];
		uint newLen = strCopyN(s, arrayLength(s), "World", 40);

		ALWAYS_CHECK(newLen == 3);
		ALWAYS_CHECK(strEquals(s, "Wor"));
	}

	// Copies nothing when `charsToCopy` is zero:
	{
		char s[64] = {'\0'};
		uint newLen = strCopyN(s, arrayLength(s), "Who is John Galt?", 0);

		ALWAYS_CHECK(newLen == 0);
		ALWAYS_CHECK(strEquals(s, ""));
	}
}

// ========================================================
// Test_strReplace():
// ========================================================

void Test_strReplace()
{
	// Replace end of string:
	{
		char s[] = "Hello World";
		strReplace(s, strLength(s), 5, 6, '+');

		ALWAYS_CHECK(strLength(s) == 11);
		ALWAYS_CHECK(strEquals(s, "Hello++++++"));
	}

	// Replace beginning of string:
	{
		char s[] = "Hello World";
		strReplace(s, strLength(s), 0, 6, '+');

		ALWAYS_CHECK(strLength(s) == 11);
		ALWAYS_CHECK(strEquals(s, "++++++World"));
	}

	// Replace middle of string:
	{
		char s[] = "Hello World";
		strReplace(s, strLength(s), 3, 5, '+');

		ALWAYS_CHECK(strLength(s) == 11);
		ALWAYS_CHECK(strEquals(s, "Hel+++++rld"));
	}

	// Replace end with count overflow:
	{
		char s[] = "Hello World";
		strReplace(s, strLength(s), 5, 999, '+');

		ALWAYS_CHECK(strLength(s) == 11);
		ALWAYS_CHECK(strEquals(s, "Hello++++++"));
	}

	// Replace the whole string from the start:
	{
		char s[] = "Hello World";
		strReplace(s, strLength(s), 0, strLength(s), '+');

		ALWAYS_CHECK(strLength(s) == 11);
		ALWAYS_CHECK(strEquals(s, "+++++++++++"));
	}

	// Replace the whole string from the start with count overflow:
	{
		char s[] = "Hello World";
		strReplace(s, strLength(s), 0, 999, '+');

		ALWAYS_CHECK(strLength(s) == 11);
		ALWAYS_CHECK(strEquals(s, "+++++++++++"));
	}

	// Count equal to zero is a no-op:
	{
		char s[] = "Hello World";
		strReplace(s, strLength(s), 2, 0, '+');

		ALWAYS_CHECK(strLength(s) == 11);
		ALWAYS_CHECK(strEquals(s, "Hello World"));
	}
}

// ========================================================
// Test_strErase():
// ========================================================

void Test_strErase()
{
	// Erase end of string:
	{
		char s[] = "Hello World";
		strErase(s, strLength(s), 5, 6);

		ALWAYS_CHECK(strLength(s) == 5);
		ALWAYS_CHECK(strEquals(s, "Hello"));
	}

	// Erase beginning of string:
	{
		char s[] = "Hello World";
		strErase(s, strLength(s), 0, 6);

		ALWAYS_CHECK(strLength(s) == 5);
		ALWAYS_CHECK(strEquals(s, "World"));
	}

	// Erase from the middle of the string:
	{
		char s[] = "Hello World";
		strErase(s, strLength(s), 3, 5);

		ALWAYS_CHECK(strLength(s) == 6);
		ALWAYS_CHECK(strEquals(s, "Helrld"));
	}

	// Erase end with count overflow:
	{
		char s[] = "Hello World";
		strErase(s, strLength(s), 5, 999);

		ALWAYS_CHECK(strLength(s) == 5);
		ALWAYS_CHECK(strEquals(s, "Hello"));
	}

	// Erase the whole string from the start:
	{
		char s[] = "Hello World";
		strErase(s, strLength(s), 0, strLength(s));

		ALWAYS_CHECK(*s == '\0');
	}

	// Erase the whole string from the start with count overflow:
	{
		char s[] = "Hello World";
		strErase(s, strLength(s), 0, 999);

		ALWAYS_CHECK(*s == '\0');
	}

	// Count equal to zero is a no-op:
	{
		char s[] = "Hello World";
		strErase(s, strLength(s), 2, 0);

		ALWAYS_CHECK(strLength(s) == 11);
		ALWAYS_CHECK(strEquals(s, "Hello World"));
	}
}

// ========================================================
// Test_strFindFirst():
// ========================================================

void Test_strFindFirst()
{
	// Find fist char:
	ALWAYS_CHECK(strFindFirst("ABC 1 DEF", '1')  ==  4);
	ALWAYS_CHECK(strFindFirst("ABC 0 DEF", '1')  == -1);
	ALWAYS_CHECK(strFindFirst("ABC 1 DEF", 'A')  ==  0);
	ALWAYS_CHECK(strFindFirst("ABC 0 DEF", 'F')  ==  8);
	ALWAYS_CHECK(strFindFirst("ABC 1 DEF", '\0') ==  9); // Returns length of string

	// Find first substring:
	ALWAYS_CHECK(strFindFirst("Hello Dr. Chandra, my name is HAL.", "Dr." , 3) ==  6);
	ALWAYS_CHECK(strFindFirst("Hello Dr. Chandra, my name is HAL.", "HAL" , 3) == 30);
	ALWAYS_CHECK(strFindFirst("Hello Dr. Chandra, my name is HAL.", "."   , 1) ==  8);
	ALWAYS_CHECK(strFindFirst("Hello Dr. Chandra, my name is HAL.", "H"   , 1) ==  0);
	ALWAYS_CHECK(strFindFirst("Hello Dr. Chandra, my name is HAL.", "9000", 4) == -1);
	ALWAYS_CHECK(strFindFirst("Hello Dr. Chandra, my name is HAL.", ""    , 0) == -1);
}

// ========================================================
// Test_strFindLast():
// ========================================================

void Test_strFindLast()
{
	// Find last char:
	ALWAYS_CHECK(strFindLast("ABC 1 DEF 1", '1')  ==  10);
	ALWAYS_CHECK(strFindLast("ABC 0 DEF 0", '1')  == -1 );
	ALWAYS_CHECK(strFindLast("ABC 1 DEF 1", 'A')  ==  0 );
	ALWAYS_CHECK(strFindLast("ABC 0 DEF 0", 'F')  ==  8 );
	ALWAYS_CHECK(strFindLast("ABC 1 DEF 1", '\0') ==  11); // Returns length of string

	// Find last substring:
	ALWAYS_CHECK(strFindLast("Hello Dr. Chandra, my name is HAL.", "Dr." , 3) ==  6);
	ALWAYS_CHECK(strFindLast("Hello Dr. Chandra, my name is HAL.", "HAL" , 3) == 30);
	ALWAYS_CHECK(strFindLast("Hello Dr. Chandra, my name is HAL.", "."   , 1) == 33);
	ALWAYS_CHECK(strFindLast("Hello Dr. Chandra, my name is HAL.", "H"   , 1) == 30);
	ALWAYS_CHECK(strFindLast("Hello Dr. Chandra, my name is HAL.", "9000", 4) == -1);
	ALWAYS_CHECK(strFindLast("Hello Dr. Chandra, my name is HAL.", ""    , 0) == -1);

	// Now try with duplicates in the same string:
	ALWAYS_CHECK(strFindLast("AAA; BBB; CCC; AAA; BBB; CCC", "BBB", 3) == 20);
}

// ========================================================
// Test_strStartsWith_strEndsWith():
// ========================================================

void Test_strStartsWith_strEndsWith()
{
	// strStartsWith:
	ALWAYS_CHECK(strStartsWith("Hello World", 11, "H"    , 1) == true);
	ALWAYS_CHECK(strStartsWith("Hello World", 11, "Hello", 5) == true);
	ALWAYS_CHECK(strStartsWith("Hello World", 11, "World", 5) == false);
	ALWAYS_CHECK(strStartsWith("Hello World", 11, ""     , 0) == false);

	// strEndsWith:
	ALWAYS_CHECK(strEndsWith("Hello World", 11, "d"    , 1) == true);
	ALWAYS_CHECK(strEndsWith("Hello World", 11, "World", 5) == true);
	ALWAYS_CHECK(strEndsWith("Hello World", 11, "Hello", 5) == false);
	ALWAYS_CHECK(strEndsWith("Hello World", 11, ""     , 0) == false);
}

// ========================================================
// Test_strStartsWith_strEndsWith():
// ========================================================

void Test_strTrim_strUpperLower()
{
	// Right trim:
	{
		char s[] = "   Hello   World\t\n\v\f\r   ";
		uint newLen = strRTrim(s, strLength(s));

		ALWAYS_CHECK(newLen == 16);
		ALWAYS_CHECK(strEquals(s, "   Hello   World"));
	}

	// Left trim:
	{
		char s[] = "   \t\n\v\f\rHello   World   ";
		uint newLen = strLTrim(s, strLength(s));

		ALWAYS_CHECK(newLen == 16);
		ALWAYS_CHECK(strEquals(s, "Hello   World   "));
	}

	// Trim both sides:
	{
		char s[] = "   \t\n\v\f\rHello World   \t\n\v\f\r";

		uint newLen = strLength(s);
		newLen = strRTrim(s, newLen);
		newLen = strLTrim(s, newLen);

		ALWAYS_CHECK(newLen == 11);
		ALWAYS_CHECK(strEquals(s, "Hello World"));
	}

	// To uppercase:
	{
		char s0[] = "foobar";
		ALWAYS_CHECK(strEquals(strToUpper(s0), "FOOBAR"));

		char s1[] = "FOOBAR";
		ALWAYS_CHECK(strEquals(strToUpper(s1), "FOOBAR"));

		char s2[] = "FoObAr";
		ALWAYS_CHECK(strEquals(strToUpper(s2), "FOOBAR"));
	}

	// To lowercase:
	{
		char s0[] = "foobar";
		ALWAYS_CHECK(strEquals(strToLower(s0), "foobar"));

		char s1[] = "FOOBAR";
		ALWAYS_CHECK(strEquals(strToLower(s1), "foobar"));

		char s2[] = "FoObAr";
		ALWAYS_CHECK(strEquals(strToLower(s2), "foobar"));
	}
}

// ========================================================
// Test_strTokenize():
// ========================================================

void Test_strTokenize()
{
	const char delimiters[] = " ;"; // Split by space and semicolon.
	char input[] = " 1;; 2;; 3;; 4;5;6;";

	char * remaining = nullptr;
	char * ptr = strTokenize(input, delimiters, &remaining);

	// Output should be the integers: 1,2,3,4,5,6
	long expected = 1;
	while (ptr != nullptr)
	{
		const long parsed = std::strtol(ptr, nullptr, 10);
		ALWAYS_CHECK(parsed == expected);
		++expected;

		ptr = strTokenize(nullptr, delimiters, &remaining);
	}
	ALWAYS_CHECK(expected == 7);
}

// ========================================================
// Test_strHash():
// ========================================================

void Test_strHash()
{
	// Empty strings always hash to 0:
	ALWAYS_CHECK(strHash("")           == 0);
	ALWAYS_CHECK(strHashIgnoreCase("") == 0);

	// Hash vs case-insensitive hash:
	ALWAYS_CHECK(strHash("")            == strHashIgnoreCase(""));
	ALWAYS_CHECK(strHash("123")         == strHashIgnoreCase("123"));
	ALWAYS_CHECK(strHash("Hello World") != strHashIgnoreCase("Hello World"));
}

// ========================================================
// Test_strAlloc():
// ========================================================

void Test_strAlloc()
{
	// strAllocate()/strDeallocate():
	{
		const uint length = 128;
		char * s = strAllocate(length); // A default +1 is added for the final '\0'

		std::memset(s, '*', length);
		s[length] = '\0';
		ALWAYS_CHECK(strLength(s) == length);

		strDeallocate(s);
	}

	// strClone():
	{
		const char original[] = "Hello World";

		// Full clone:
		char * clone = strClone(original);
		ALWAYS_CHECK(strEquals(clone, original));
		strDeallocate(clone);

		// Partial clone:
		const uint charsToClone = 5;
		clone = strClone(original, &charsToClone);
		ALWAYS_CHECK(strEquals(clone, "Hello"));
		strDeallocate(clone);
	}
}

// ========================================================
// Test_strToNumber():
// ========================================================

void Test_strToNumber()
{
	// 32bit integer:
	{
		int32 result;

		result = 0;
		strToInt("101010", nullptr, 2, result);
		ALWAYS_CHECK(result == 42);

		result = 0;
		strToInt("52", nullptr, 8, result);
		ALWAYS_CHECK(result == 42);

		result = 0;
		strToInt("42", nullptr, 10, result);
		ALWAYS_CHECK(result == 42);

		result = 0;
		strToInt("0x2A", nullptr, 16, result);
		ALWAYS_CHECK(result == 42);
	}

	// 64bit integer:
	{
		int64 result;

		result = 0;
		strToInt("101010", nullptr, 2, result);
		ALWAYS_CHECK(result == 42);

		result = 0;
		strToInt("52", nullptr, 8, result);
		ALWAYS_CHECK(result == 42);

		result = 0;
		strToInt("42", nullptr, 10, result);
		ALWAYS_CHECK(result == 42);

		result = 0;
		strToInt("0x2A", nullptr, 16, result);
		ALWAYS_CHECK(result == 42);
	}

	// Signed integers:
	{
		int32 i32 = 0;
		strToInt("-42", nullptr, 10, i32);
		ALWAYS_CHECK(i32 == -42);

		int64 i64 = 0;
		strToInt("-42", nullptr, 10, i64);
		ALWAYS_CHECK(i64 == -42);
	}

	// Floats:
	{
		float fPI = 0.0f;
		strToFloat("3.141592", nullptr, fPI);
		ALWAYS_CHECK(fPI == 3.141592f);

		double dNum = 0.0;
		strToFloat("-0.12345", nullptr, dNum);
		ALWAYS_CHECK(dNum == -0.12345);

		// Quick conversion:
		fPI = strToFloatFast("3.141592", nullptr);
		ALWAYS_CHECK(fPI == 3.141592f);
	}
}

// ========================================================
// Test_numberToStr():
// ========================================================

void Test_numberToStr()
{
	// Test relies on this assumption.
	ALWAYS_CHECK(sizeof(char) == 1);

	char buffer[128] = {0};

	// 32bit integer:
	{
		const int32 i32 = 42;

		strFromInt(i32, buffer, arrayLength(buffer), 2);
		ALWAYS_CHECK(strEquals(buffer, "101010"));

		strFromInt(i32, buffer, arrayLength(buffer), 8);
		ALWAYS_CHECK(strEquals(buffer, "52"));

		strFromInt(i32, buffer, arrayLength(buffer), 10);
		ALWAYS_CHECK(strEquals(buffer, "42"));

		strFromInt(i32, buffer, arrayLength(buffer), 16);
		ALWAYS_CHECK(strEquals(buffer, "0x2A"));
	}

	// 64bit integer:
	{
		const int64 i64 = 42;

		strFromInt(i64, buffer, arrayLength(buffer), 2);
		ALWAYS_CHECK(strEquals(buffer, "101010"));

		strFromInt(i64, buffer, arrayLength(buffer), 8);
		ALWAYS_CHECK(strEquals(buffer, "52"));

		strFromInt(i64, buffer, arrayLength(buffer), 10);
		ALWAYS_CHECK(strEquals(buffer, "42"));

		strFromInt(i64, buffer, arrayLength(buffer), 16);
		ALWAYS_CHECK(strEquals(buffer, "0x2A"));
	}

	// Signed integers:
	{
		const int32 i32 = -42;
		strFromInt(i32, buffer, arrayLength(buffer), 10);
		ALWAYS_CHECK(strEquals(buffer, "-42"));

		const int64 i64 = -42;
		strFromInt(i64, buffer, arrayLength(buffer), 10);
		ALWAYS_CHECK(strEquals(buffer, "-42"));
	}

	// Floats:
	{
		const float fPI = 3.141592f;

		strFromFloat(fPI, buffer, arrayLength(buffer), /* decimalDigits = */ 4);
		ALWAYS_CHECK(strEquals(buffer, "3.1416")); // Rounds-up the last digit

		strFromFloat(fPI, buffer, arrayLength(buffer), /* decimalDigits = */ 6);
		ALWAYS_CHECK(strEquals(buffer, "3.141592")); // No rounding

		const double dNum = -0.12345;

		strFromFloat(dNum, buffer, arrayLength(buffer), /* decimalDigits = */ 0);
		ALWAYS_CHECK(strEquals(buffer, "-0"));

		strFromFloat(dNum, buffer, arrayLength(buffer), /* decimalDigits = */ 5);
		ALWAYS_CHECK(strEquals(buffer, "-0.12345"));
	}
}

// ========================================================
// Test_CStrings():
// ========================================================

void Test_CStrings()
{
	Test_strCmp();
	Test_strCopy();
	Test_strReplace();
	Test_strErase();
	Test_strFindFirst();
	Test_strFindLast();
	Test_strStartsWith_strEndsWith();
	Test_strTrim_strUpperLower();
	Test_strTokenize();
	Test_strHash();
	Test_strAlloc();
	Test_strToNumber();
	Test_numberToStr();
}

} // namespace unittest {}
} // namespace core {}
} // namespace atlas {}
