
// ================================================================================================
// -*- C++ -*-
// File: test_utils_algorithms.cpp
// Author: Guilherme R. Lampert
// Created on: 08/01/15
// Brief: Unit tests for the miscellaneous utils algorithms and pure functions.
// ================================================================================================

#include "atlas/core/utils/utility.hpp"
#include "atlas/core/utils/bin_search.hpp"
#include "atlas/core/utils/quick_sort.hpp"
#include <cstring> // For std::memcmp()

namespace atlas
{
namespace core
{
namespace unittest
{

// ========================================================
// Test_min_max_clamp_swap():
// ========================================================

void Test_min_max_clamp_swap()
{
	ALWAYS_CHECK(core::min( 0, 1 ) ==  0);
	ALWAYS_CHECK(core::min(-1, 1 ) == -1);
	ALWAYS_CHECK(core::min( 1, 1 ) ==  1);

	ALWAYS_CHECK(core::max( 0, 1 ) ==  1);
	ALWAYS_CHECK(core::max(-1, 1 ) ==  1);
	ALWAYS_CHECK(core::max( 1, 1 ) ==  1);

	ALWAYS_CHECK(core::clamp( 1, 0, 1 ) == 1);
	ALWAYS_CHECK(core::clamp( 0, 0, 1 ) == 0);

	ALWAYS_CHECK(core::clamp( 2, 0, 1 ) == 1);
	ALWAYS_CHECK(core::clamp(-2, 0, 1 ) == 0);

	int a = 1;
	int b = 2;
	core::swap(a, b);
	ALWAYS_CHECK(a == 2);
	ALWAYS_CHECK(b == 1);
}

// ========================================================
// Test_binarySearch():
// ========================================================

void Test_binarySearch()
{
	DefaultBinarySearchPredicate<int> pred;

	// Empty array (elementCount = 0):
	{
		int haystack[1] = { 0 }; // Pretend the array is empty
		int needle = 0;
		ALWAYS_CHECK(binarySearch(haystack, /* elementCount = */ 0, needle, pred) == -1);
	}

	// Only positive numbers:
	{
		int haystack[] = { 1, 2, 3, 4, 5, 6 };
		int needle;

		ALWAYS_CHECK(binarySearch(haystack, needle = 4, pred) ==  3);
		ALWAYS_CHECK(binarySearch(haystack, needle = 6, pred) ==  5);
		ALWAYS_CHECK(binarySearch(haystack, needle = 0, pred) == -1);
	}

	// Positive and negative numbers:
	{
		int haystack[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5 };
		int needle;

		ALWAYS_CHECK(binarySearch(haystack, needle = -1, pred) ==  4);
		ALWAYS_CHECK(binarySearch(haystack, needle =  0, pred) ==  5);
		ALWAYS_CHECK(binarySearch(haystack, needle = 10, pred) == -1);
	}
}

// ========================================================
// Test_quickSort():
// ========================================================

void Test_quickSort()
{
	DefaultQuickSortPredicate<int> pred;

	// Empty array (elementCount = 0):
	{
		int array[] = { 2, 1, 4 };
		quickSort(array, /* elementCount = */ 0, pred);

		const int expected[] = { 2, 1, 4 };
		ALWAYS_CHECK(std::memcmp(array, expected, sizeof(expected)) == 0);
	}

	// Sorted array:
	{
		int array[] = { -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6 };
		quickSort(array, pred);

		const int expected[] = { -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6 };
		ALWAYS_CHECK(std::memcmp(array, expected, sizeof(expected)) == 0);
	}

	// Unsorted array:
	{
		int array[] = { -1, 5, -2, -4, 6, -3, 1, 4, 2, 0, -5, 3, -6 };
		quickSort(array, pred);

		const int expected[] = { -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6 };
		ALWAYS_CHECK(std::memcmp(array, expected, sizeof(expected)) == 0);
	}

	// Lots of repeated elements:
	{
		int array[] = { 10, 10, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 50, 50 };
		quickSort(array, pred);

		const int expected[] = { 10, 10, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 50, 50 };
		ALWAYS_CHECK(std::memcmp(array, expected, sizeof(expected)) == 0);
	}
}

// ========================================================
// Test_UtilsAlgorithms():
// ========================================================

void Test_UtilsAlgorithms()
{
	Test_min_max_clamp_swap();
	Test_binarySearch();
	Test_quickSort();
}

} // namespace unittest {}
} // namespace core {}
} // namespace atlas {}
