
// ================================================================================================
// -*- C++ -*-
// File: test_main.cpp
// Author: Guilherme R. Lampert
// Created on: 07/09/15
// Brief: Driver for the Core Library unit tests.
// ================================================================================================

#include <iostream>

namespace atlas
{
namespace core
{
namespace unittest
{
void Test_CStrings();
void Test_CppStrings();
void Test_PathAndFilenameFunctions();
void Test_UtilsAlgorithms();
void Test_Arrays();
void Test_IntrusiveHashTable();
void Test_IntrusiveLists();
void Test_IntrusiveStack();
void Test_IntrusiveQueue();
void Test_FileSystem();
} // namespace unittest {}
} // namespace core {}
} // namespace atlas {}

// ========================================================

#define TEST(func) \
do { \
	std::cout << "- " << #func << "\n"; \
	func(); \
	std::cout << "- Ok\n"; \
} while (0)

int main()
{
	std::cout << "Unit Test suite for Atlas Core Library.\n";

	using namespace atlas;
	using namespace core;
	using namespace unittest;

	TEST( Test_CStrings                 );
	TEST( Test_CppStrings               );
	TEST( Test_PathAndFilenameFunctions );
	TEST( Test_UtilsAlgorithms          );
	TEST( Test_Arrays                   );
	TEST( Test_IntrusiveHashTable       );
	TEST( Test_IntrusiveLists           );
	TEST( Test_IntrusiveStack           );
	TEST( Test_IntrusiveQueue           );
	TEST( Test_FileSystem               );
}

#undef TEST

