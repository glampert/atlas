
// ================================================================================================
// -*- C++ -*-
// File: containers.hpp
// Author: Guilherme R. Lampert
// Created on: 05/03/15
// Brief: Master header of the containers submodule.
//        Including this header makes all containers and generic data structures available.
// ================================================================================================

#ifndef ATLAS_CORE_CONTAINERS_HPP
#define ATLAS_CORE_CONTAINERS_HPP

#include "atlas/core/containers/arrays.hpp"
#include "atlas/core/containers/intrusive_slist.hpp"
#include "atlas/core/containers/intrusive_dlist.hpp"
#include "atlas/core/containers/intrusive_stack.hpp"
#include "atlas/core/containers/intrusive_queue.hpp"
#include "atlas/core/containers/intrusive_hash_table.hpp"

#endif // ATLAS_CORE_CONTAINERS_HPP
