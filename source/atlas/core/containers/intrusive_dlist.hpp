
// ================================================================================================
// -*- C++ -*-
// File: intrusive_dlist.hpp
// Author: Guilherme R. Lampert
// Created on: 05/03/15
// Brief: Declares the IntrusiveDList generic data structure.
// ================================================================================================

#ifndef ATLAS_CORE_CONTAINERS_INTRUSIVE_DLIST_HPP
#define ATLAS_CORE_CONTAINERS_INTRUSIVE_DLIST_HPP

#include "atlas/core/utils/utility.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// template class DListNode<T>:
// ========================================================

template<class T>
class DListNode
{
public:

	// List needs access to internal data of its nodes.
	template<class ITEM_TYPE>
	friend class IntrusiveDList;

	// IntrusiveDList links:
	T * getDListNext() const { return dlNext; }
	T * getDListPrev() const { return dlPrev; }

	// Linked when both links are not null.
	bool isLinkedToDList() const
	{
		return (dlPrev != nullptr) &&
		       (dlNext != nullptr);
	}

protected:

	 DListNode() : dlPrev(nullptr), dlNext(nullptr) { }
	~DListNode() DEFAULTED_METHOD;

private:

	T * dlPrev; // Previous node in a doubly-linked list.
	T * dlNext; // Next node in a doubly-linked list.
};

// ========================================================
// template class IntrusiveDList<T>:
// ========================================================

// Intrusive doubly-liked list template class.
//
// An intrusive data structure is a structure that stores house
// keeping information necessary to its functioning directly inside
// the object being stored.
// This type of storage scheme forces types that wish to be inserted
// into this list to inherit from `DListNode<T>`.
//
// Only pointers to objects can actually be stored inside the structure.
// Thus the objective of the intrusive list is to eliminate extra memory allocations
// for new insertions. Instead of allocating a node structure per object, the object
// itself is the node. Consequently, memory allocations are all external to the structure.
//
// This list does not manage memory or lifetime of the objects inserted.
// Removing an item from the list WILL NOT destroy the object, just unlink it.
template<class T>
class IntrusiveDList
	: private NonCopyable
{
public:

	// Nested typedefs:
	typedef T ValueType;

	// Bidirectional iterator for the doubly-linked list.
	// Allows bidirectional list traversal (increment and decrement).
	//
	// Can have `const` applied to it;
	// Can be used with "foreach"-style iteration.
	class Iterator final
	{
	public:

		Iterator();
		Iterator(const IntrusiveDList * list, uint pos, ValueType * nd);

		const ValueType & operator*  () const;
		      ValueType & operator*  ();
		const ValueType * operator-> () const;
		      ValueType * operator-> ();

		const Iterator & operator-- ()    const; // pre-decrement
		      Iterator   operator-- (int) const; // post-decrement
		const Iterator & operator++ ()    const; // pre-increment
		      Iterator   operator++ (int) const; // post-increment

		bool operator == (const Iterator & other) const;
		bool operator != (const Iterator & other) const;

		// Internal use helpers:
		uint getCurrentPosition() const;
		ValueType * getCurrentNode() const;

		// Get a reference to the linked-list that owns this iterator.
		const IntrusiveDList * getList() const;

	private:

		void validateDereference() const;
		void validateIncrement()   const;
		void validateDecrement()   const;

		// List this iterator belongs to. Used for error checking.
		const IntrusiveDList * myList;

		// Actual node pointing to a list item. Null for list end.
		mutable ValueType * node;

		// Traversal position. Needed due to the circular nature of the list.
		mutable uint position;
	};

	// Iterators to the beginning of the table and one-past-the-end:
	Iterator begin();
	Iterator end();
	const Iterator begin() const;
	const Iterator end()   const;

public:

	// Construct an empty list.
	IntrusiveDList();

	// Destructor clears the list and unlinks all items.
	~IntrusiveDList();

	// Append at the head or tail of the list.
	// Both are constant time. `item` must not be null!
	void pushFront(ValueType * item);
	void pushBack(ValueType * item);

	// Removes one item from head or tail of the list, without destroying the object.
	// Returns the removed item or null if the list is already empty. Both are constant time.
	ValueType * popFront();
	ValueType * popBack();

	// Returns the first and last item in the list container,
	// or null if the list is empty.
	ValueType * front() const;
	ValueType * back()  const;

	// Test if the list is empty (getSize() == 0). Constant time.
	bool isEmpty() const;

	// Get size in items. Constant time.
	uint getSize() const;

	// Unlinks all objects from the list, without destroying them.
	void clear();

	// Unlinks all objects AND deletes each pointer. This method may be
	// called by the user, but it is never called by IntrusiveDList.
	void clearAndDelete();

	// Performs linear search in the list and returns an iterator to the first
	// occurrence of `wanted`, or `list.end()` if `wanted` in not present.
	// Search is performed by applying operator == in the items themselves, not the stored pointers.
	Iterator find(const ValueType & wanted) const;

	// Performs linear search in the list and returns an iterator to the first
	// item matching `pred`, or `list.end()` if there is no match.
	template<class PRED> Iterator find(const PRED & pred) const;
	template<class PRED, class PARAM> Iterator find(const PRED & pred, const PARAM & param) const;

	// Insert item into list. The container is extended by inserting
	// a new node before the item at the specified position. `item` must not be null!
	Iterator insert(Iterator position, ValueType * item);

	// Removes one item from the list.
	// Removing doesn't destroy the item, only unlinks it from the data structure.
	// The user can save the item before removing it and then destroy it after removing, if required.
	// Returns an iterator pointing to the new location of the node that followed the item unlinked,
	// which is the list end if the operation removed the last item in it.
	Iterator remove(Iterator position);

	// Removes from the list all items for which predicate `pred` returns true.
	// Returns the number of items removed.
	template<class PRED> uint removeIf(const PRED & pred);
	template<class PRED, class PARAM> uint removeIf(const PRED & pred, const PARAM & param);

	// Transfers all items from the other list to the end of this list, clearing the other.
	void splice(IntrusiveDList & other);

	// Transfers all items of this list into `dest`, making this list empty.
	// Destination must be an empty list!
	void transferTo(IntrusiveDList & dest);

private:

	ValueType * head; // Head of list. Circularly referenced.
	uint itemCount;   // Number of items in the list.
};

// ================================================================================================
// IntrusiveDList inline methods and related helpers:
// ================================================================================================

// ========================================================
// begin():
// ========================================================

template<class T>
typename IntrusiveDList<T>::Iterator
begin(IntrusiveDList<T> & dl)
{
	return dl.begin();
}

// ========================================================
// end():
// ========================================================

template<class T>
typename IntrusiveDList<T>::Iterator
end(IntrusiveDList<T> & dl)
{
	return dl.end();
}

// ========================================================
// begin() [const]:
// ========================================================

template<class T>
const typename IntrusiveDList<T>::Iterator
begin(const IntrusiveDList<T> & dl)
{
	return dl.begin();
}

// ========================================================
// end() [const]:
// ========================================================

template<class T>
const typename IntrusiveDList<T>::Iterator
end(const IntrusiveDList<T> & dl)
{
	return dl.end();
}

// ========================================================
// IntrusiveDList::IntrusiveDList():
// ========================================================

template<class T>
IntrusiveDList<T>::IntrusiveDList()
	: head(nullptr)
	, itemCount(0)
{
}

// ========================================================
// IntrusiveDList::~IntrusiveDList():
// ========================================================

template<class T>
IntrusiveDList<T>::~IntrusiveDList()
{
	clear();
}

// ========================================================
// IntrusiveDList::pushFront():
// ========================================================

template<class T>
void IntrusiveDList<T>::pushFront(ValueType * item)
{
	DEBUG_CHECK(item != nullptr);
	DEBUG_CHECK(!item->isLinkedToDList()); // An object can only be a member of one list at a time!

	if (!isEmpty())
	{
		// head->prev points the tail, and vice-versa:
		ValueType * tail = head->dlPrev;
		item->dlNext = head;
		head->dlPrev = item;
		item->dlPrev = tail;
		head = item;
	}
	else // Empty list, first insertion:
	{
		head = item;
		head->dlPrev = head;
		head->dlNext = head;
	}
	++itemCount;
}

// ========================================================
// IntrusiveDList::pushBack():
// ========================================================

template<class T>
void IntrusiveDList<T>::pushBack(ValueType * item)
{
	DEBUG_CHECK(item != nullptr);
	DEBUG_CHECK(!item->isLinkedToDList()); // An object can only be a member of one list at a time!

	if (!isEmpty())
	{
		// head->prev points the tail, and vice-versa:
		ValueType * tail = head->dlPrev;
		item->dlPrev = tail;
		tail->dlNext = item;
		item->dlNext = head;
		head->dlPrev = item;
	}
	else // Empty list, first insertion:
	{
		head = item;
		head->dlPrev = head;
		head->dlNext = head;
	}
	++itemCount;
}

// ========================================================
// IntrusiveDList::popFront():
// ========================================================

template<class T>
typename IntrusiveDList<T>::ValueType * IntrusiveDList<T>::popFront()
{
	if (isEmpty())
	{
		return nullptr;
	}

	ValueType * removedItem = head;

	ValueType * tail = head->dlPrev;
	head = head->dlNext;
	head->dlPrev = tail;
	--itemCount;

	removedItem->dlPrev = nullptr;
	removedItem->dlNext = nullptr;
	return removedItem;
}

// ========================================================
// IntrusiveDList::popBack():
// ========================================================

template<class T>
typename IntrusiveDList<T>::ValueType * IntrusiveDList<T>::popBack()
{
	if (isEmpty())
	{
		return nullptr;
	}

	ValueType * removedItem = head->dlPrev;

	ValueType * tail = head->dlPrev;
	head->dlPrev = tail->dlPrev;
	tail->dlPrev->dlNext = head;
	--itemCount;

	removedItem->dlPrev = nullptr;
	removedItem->dlNext = nullptr;
	return removedItem;
}

// ========================================================
// IntrusiveDList::front():
// ========================================================

template<class T>
typename IntrusiveDList<T>::ValueType * IntrusiveDList<T>::front() const
{
	return isEmpty() ? nullptr : head;
}

// ========================================================
// IntrusiveDList::back():
// ========================================================

template<class T>
typename IntrusiveDList<T>::ValueType * IntrusiveDList<T>::back() const
{
	return isEmpty() ? nullptr : head->dlPrev;
}

// ========================================================
// IntrusiveDList::isEmpty():
// ========================================================

template<class T>
bool IntrusiveDList<T>::isEmpty() const
{
	return itemCount == 0;
}

// ========================================================
// IntrusiveDList::getSize():
// ========================================================

template<class T>
uint IntrusiveDList<T>::getSize() const
{
	return itemCount;
}

// ========================================================
// IntrusiveDList::clear():
// ========================================================

template<class T>
void IntrusiveDList<T>::clear()
{
	// Unlink all items from the list:
	// (need the counter due to the list's circular layout)
	ValueType * item = head;
	for (uint i = 0; i < itemCount; ++i)
	{
		ValueType * tmp = item;
		item = item->dlNext;

		DEBUG_CHECK(tmp != nullptr);
		tmp->dlPrev = nullptr;
		tmp->dlNext = nullptr;
	}

	head = nullptr;
	itemCount = 0;
}

// ========================================================
// IntrusiveDList::clearAndDelete():
// ========================================================

template<class T>
void IntrusiveDList<T>::clearAndDelete()
{
	// Unlink all items from the list AND delete.
	// (need the counter due to the list's circular layout)
	ValueType * item = head;
	for (uint i = 0; i < itemCount; ++i)
	{
		ValueType * tmp = item;
		item = item->dlNext;

		delete tmp;
	}

	head = nullptr;
	itemCount = 0;
}

// ========================================================
// IntrusiveDList::find():
// ========================================================

template<class T>
typename IntrusiveDList<T>::Iterator IntrusiveDList<T>::find(const ValueType & wanted) const
{
	for (const Iterator it = begin(); it != end(); ++it)
	{
		if ((*it) == wanted)
		{
			return it;
		}
	}
	return end(); // Item not found
}

// ========================================================
// IntrusiveDList::find():
// ========================================================

template<class T>
template<class PRED>
typename IntrusiveDList<T>::Iterator IntrusiveDList<T>::find(const PRED & pred) const
{
	for (const Iterator it = begin(); it != end(); ++it)
	{
		if (pred(*it))
		{
			return it;
		}
	}
	return end(); // Item not found
}

// ========================================================
// IntrusiveDList::find():
// ========================================================

template<class T>
template<class PRED, class PARAM>
typename IntrusiveDList<T>::Iterator IntrusiveDList<T>::find(const PRED & pred, const PARAM & param) const
{
	for (const Iterator it = begin(); it != end(); ++it)
	{
		if (pred(*it, param))
		{
			return it;
		}
	}
	return end(); // Item not found
}

// ========================================================
// IntrusiveDList::insert():
// ========================================================

template<class T>
typename IntrusiveDList<T>::Iterator IntrusiveDList<T>::insert(Iterator position, ValueType * item)
{
	DEBUG_CHECK(item != nullptr);
	DEBUG_CHECK(!item->isLinkedToDList()); // An object can only be a member of one list at a time!

	// Iterators of a d-list hold a reference to the owning list,
	// we can assert here to improve safety and catch errors as early as possible:
	DEBUG_CHECK(position.getList() == this && "Iterator does not belong to this linked-list!");

	// Optimize for these special cases:
	if (position == begin())
	{
		pushFront(item);
		return Iterator(this, 0, head);
	}

	if (position == end())
	{
		pushBack(item);
		return Iterator(this, itemCount, head->dlPrev);
	}

	// Insert somewhere between head and tail (before `position`):
	ValueType * posNode = position.getCurrentNode();
	ValueType * posPrev = posNode->dlPrev;
	posPrev->dlNext = item;
	posNode->dlPrev = item;
	item->dlPrev = posPrev;
	item->dlNext = posNode;
	++itemCount;

	return Iterator(this, position.getCurrentPosition(), item);
}

// ========================================================
// IntrusiveDList::remove():
// ========================================================

template<class T>
typename IntrusiveDList<T>::Iterator IntrusiveDList<T>::remove(Iterator position)
{
	// Can't remove end(), that's just a marker to the item after the last.
	DEBUG_CHECK(position != end());

	// Can't remove from empty list either.
	DEBUG_CHECK(!isEmpty() && "List already empty!");

	// Iterators of a d-list hold a reference to the owning list,
	// we can assert here to improve safety and catch errors as early as possible:
	DEBUG_CHECK(position.getList() == this && "Iterator does not belong to this linked-list!");

	// Special case 1, remove head:
	if (position == begin())
	{
		popFront();
		return begin();
	}

	// Special case 2, remove tail:
	if (position.getCurrentNode() == head->dlPrev)
	{
		popBack();
		return Iterator(this, (itemCount != 0) ? itemCount - 1 : 0, head->dlPrev);
	}

	// Remove node:
	ValueType * posNode = position.getCurrentNode();
	ValueType * posPrev = posNode->dlPrev;
	ValueType * posNext = posNode->dlNext;
	posPrev->dlNext = posNext;
	posNext->dlPrev = posPrev;
	--itemCount;

	// Clear the node's links and return and iterator to what followed `position`:
	Iterator result(this, position.getCurrentPosition(), posNode->dlNext);
	posNode->dlPrev = nullptr;
	posNode->dlNext = nullptr;
	return result;
}

// ========================================================
// IntrusiveDList::removeIf():
// ========================================================

template<class T>
template<class PRED>
uint IntrusiveDList<T>::removeIf(const PRED & pred)
{
	uint removedCount = 0;
	Iterator it = begin();
	while (it != end())
	{
		if (pred(*it))
		{
			it = remove(it);
			++removedCount;
			continue;
		}
		++it;
	}
	return removedCount;
}

// ========================================================
// IntrusiveDList::removeIf():
// ========================================================

template<class T>
template<class PRED, class PARAM>
uint IntrusiveDList<T>::removeIf(const PRED & pred, const PARAM & param)
{
	uint removedCount = 0;
	Iterator it = begin();
	while (it != end())
	{
		if (pred(*it, param))
		{
			it = remove(it);
			++removedCount;
			continue;
		}
		++it;
	}
	return removedCount;
}

// ========================================================
// IntrusiveDList::splice():
// ========================================================

template<class T>
void IntrusiveDList<T>::splice(IntrusiveDList<T> & other)
{
	// Do nothing if the other is empty.
	if (other.isEmpty())
	{
		return;
	}

	// Simpler case when this list is empty:
	if (isEmpty())
	{
		other.transferTo(*this);
		return;
	}

	ValueType * thisTail  = head->dlPrev;
	ValueType * otherTail = other.head->dlPrev;

	// Attack other to this list's tail:
	thisTail->dlNext = other.head;
	other.head->dlPrev = thisTail;
	head->dlPrev = otherTail;

	itemCount += other.itemCount;

	// Clear the other list's state:
	other.head = nullptr;
	other.itemCount = 0;
}

// ========================================================
// IntrusiveDList::transferTo():
// ========================================================

template<class T>
void IntrusiveDList<T>::transferTo(IntrusiveDList<T> & dest)
{
	// `dest` has to be an empty list!
	DEBUG_CHECK(dest.isEmpty() && "Destination list contents would be lost!");

	dest.head = head;
	dest.itemCount = itemCount;

	head = nullptr;
	itemCount = 0;
}

// ========================================================
// IntrusiveDList::begin():
// ========================================================

template<class T>
typename IntrusiveDList<T>::Iterator IntrusiveDList<T>::begin()
{
	return Iterator(this, 0, head);
}

// ========================================================
// IntrusiveDList::end():
// ========================================================

template<class T>
typename IntrusiveDList<T>::Iterator IntrusiveDList<T>::end()
{
	return Iterator(this, itemCount, head);
}

// ========================================================
// IntrusiveDList::begin() [const]:
// ========================================================

template<class T>
const typename IntrusiveDList<T>::Iterator IntrusiveDList<T>::begin() const
{
	return Iterator(this, 0, head);
}

// ========================================================
// IntrusiveDList::end() [const]:
// ========================================================

template<class T>
const typename IntrusiveDList<T>::Iterator IntrusiveDList<T>::end() const
{
	return Iterator(this, itemCount, head);
}

// ================================================================================================
// IntrusiveDList::Iterator class definition:
// ================================================================================================

// ========================================================
// IntrusiveDList::Iterator::Iterator():
// ========================================================

template<class T>
IntrusiveDList<T>::Iterator::Iterator()
	: myList(nullptr)
	, node(nullptr)
	, position(0)
{
	// Invalid iterator.
}

// ========================================================
// IntrusiveDList::Iterator::Iterator():
// ========================================================

template<class T>
IntrusiveDList<T>::Iterator::Iterator(const IntrusiveDList<T> * list, const uint pos, ValueType * nd)
	: myList(list)
	, node(nd)
	, position(pos)
{
	DEBUG_CHECK(myList != nullptr);
}

// ========================================================
// IntrusiveDList::Iterator::operator* :
// ========================================================

template<class T>
typename IntrusiveDList<T>::ValueType & IntrusiveDList<T>::Iterator::operator* ()
{
	validateDereference();
	return *node;
}

// ========================================================
// IntrusiveDList::Iterator::operator* :
// ========================================================

template<class T>
const typename IntrusiveDList<T>::ValueType & IntrusiveDList<T>::Iterator::operator* () const
{
	validateDereference();
	return *node;
}

// ========================================================
// IntrusiveDList::Iterator::operator-> :
// ========================================================

template<class T>
typename IntrusiveDList<T>::ValueType * IntrusiveDList<T>::Iterator::operator-> ()
{
	validateDereference();
	return node;
}

// ========================================================
// IntrusiveDList::Iterator::operator-> :
// ========================================================

template<class T>
const typename IntrusiveDList<T>::ValueType * IntrusiveDList<T>::Iterator::operator-> () const
{
	validateDereference();
	return node;
}

// ========================================================
// IntrusiveDList::Iterator::operator-- (pre-decrement):
// ========================================================

template<class T>
const typename IntrusiveDList<T>::Iterator & IntrusiveDList<T>::Iterator::operator-- () const
{
	validateDecrement();
	node = node->getDListPrev();
	--position;
	return *this;
}

// ========================================================
// IntrusiveDList::Iterator::operator-- (post-decrement):
// ========================================================

template<class T>
typename IntrusiveDList<T>::Iterator IntrusiveDList<T>::Iterator::operator-- (int) const
{
	validateDecrement();
	Iterator tmp(*this);
	++(*this);
	return tmp; // Return old, pre-decrement value.
}

// ========================================================
// IntrusiveDList::Iterator::operator++ (pre-increment):
// ========================================================

template<class T>
const typename IntrusiveDList<T>::Iterator & IntrusiveDList<T>::Iterator::operator++ () const
{
	validateIncrement();
	node = node->getDListNext();
	++position;
	return *this;
}

// ========================================================
// IntrusiveDList::Iterator::operator++ (post-increment):
// ========================================================

template<class T>
typename IntrusiveDList<T>::Iterator IntrusiveDList<T>::Iterator::operator++ (int) const
{
	validateIncrement();
	Iterator tmp(*this);
	++(*this);
	return tmp; // Return old, pre-increment value.
}

// ========================================================
// IntrusiveDList::Iterator::operator == :
// ========================================================

template<class T>
bool IntrusiveDList<T>::Iterator::operator == (const Iterator & other) const
{
	DEBUG_CHECK(myList == other.myList && "Iterators belong to different linked-lists!");
	return position == other.position;
}

// ========================================================
// IntrusiveDList::Iterator::operator != :
// ========================================================

template<class T>
bool IntrusiveDList<T>::Iterator::operator != (const Iterator & other) const
{
	DEBUG_CHECK(myList == other.myList && "Iterators belong to different linked-lists!");
	return position != other.position;
}

// ========================================================
// IntrusiveDList::Iterator::getCurrentPosition():
// ========================================================

template<class T>
uint IntrusiveDList<T>::Iterator::getCurrentPosition() const
{
	return position;
}

// ========================================================
// IntrusiveDList::Iterator::getCurrentNode():
// ========================================================

template<class T>
typename IntrusiveDList<T>::ValueType * IntrusiveDList<T>::Iterator::getCurrentNode() const
{
	return node;
}

// ========================================================
// IntrusiveDList::Iterator::getList():
// ========================================================

template<class T>
const IntrusiveDList<T> * IntrusiveDList<T>::Iterator::getList() const
{
	return myList;
}

// ========================================================
// IntrusiveDList::Iterator::validateDereference():
// ========================================================

template<class T>
void IntrusiveDList<T>::Iterator::validateDereference() const
{
	DEBUG_CHECK(myList != nullptr && "Invalid linked-list iterator!");
	DEBUG_CHECK(node   != nullptr && "Invalid linked-list iterator dereference!");
	DEBUG_CHECK(position < myList->getSize() && "Invalid linked-list iterator dereference!");
}

// ========================================================
// IntrusiveDList::Iterator::validateIncrement():
// ========================================================

template<class T>
void IntrusiveDList<T>::Iterator::validateIncrement() const
{
	DEBUG_CHECK(node != nullptr && "Invalid linked-list iterator!");
	DEBUG_CHECK(position < myList->getSize() && "Out-of-bounds linked-list iterator increment!");
}

// ========================================================
// IntrusiveDList::Iterator::validateDecrement():
// ========================================================

template<class T>
void IntrusiveDList<T>::Iterator::validateDecrement() const
{
	DEBUG_CHECK(node != nullptr && "Invalid linked-list iterator!");
	DEBUG_CHECK(position > 0 && "Out-of-bounds linked-list iterator decrement!");
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_CONTAINERS_INTRUSIVE_DLIST_HPP
