
// ================================================================================================
// -*- C++ -*-
// File: arrays.hpp
// Author: Guilherme R. Lampert
// Created on: 19/04/15
// Brief: Templated array classes to replace unsafe C-style 'raw' arrays.
// ================================================================================================

#ifndef ATLAS_CORE_CONTAINERS_ARRAY_TYPES_HPP
#define ATLAS_CORE_CONTAINERS_ARRAY_TYPES_HPP

#include "atlas/core/utils/utility.hpp"
#include "atlas/core/utils/bin_search.hpp"
#include "atlas/core/utils/quick_sort.hpp"
#include "atlas/core/memory/mem_utils.hpp"

namespace atlas
{
namespace core
{
namespace internal
{

// ========================================================
// template class GenericArrayIterator<T, ARRAY_TYPE>:
// ========================================================

// Generic array iterator used by Array and SizedArray.
// Supports increment and decrement as well as random access/indexing
// via operators +, -, += and -=. Can be used in "foreach"-style iteration.
template<class T, class ARRAY_TYPE>
class GenericArrayIterator final
{
public:

	typedef T ValueType;
	typedef ARRAY_TYPE ArrayType;

	GenericArrayIterator();
	GenericArrayIterator(const ArrayType * owner, int dir, int startIndex);

	const ValueType & operator*  () const;
	      ValueType & operator*  ();
	const ValueType * operator-> () const;
	      ValueType * operator-> ();

	const GenericArrayIterator & operator-- ()    const; // pre-decrement
	      GenericArrayIterator   operator-- (int) const; // post-decrement
	const GenericArrayIterator & operator++ ()    const; // pre-increment
	      GenericArrayIterator   operator++ (int) const; // post-increment

	GenericArrayIterator & operator += (int displacement);
	GenericArrayIterator & operator -= (int displacement);

	GenericArrayIterator operator + (int displacement) const;
	GenericArrayIterator operator - (int displacement) const;

	bool operator == (const GenericArrayIterator & other) const;
	bool operator != (const GenericArrayIterator & other) const;

	// Get the current iteration index,
	// which can be used with operator [] on array types.
	int getCurrentIndex() const;

	// Get a reference to the array that owns this iterator.
	const ArrayType * getArray() const;

private:

	void validateDereference() const;
	void validateIncrement(int displacement) const;
	void validateDecrement(int displacement) const;

	ArrayType * myArray;
	mutable int index;
	int direction;
};

// ========================================================
// template class CommonArrayOps<T, DERIVED>:
// ========================================================

// "Mixin" base class that implements common array algorithms
// and methods. This class is inherited by Array and SizedArray.
// This is not meant to be used as a standalone class, hence it lives hidden
// inside the `internal` namespace and should not be used by client code.
template<class T, class DERIVED>
class CommonArrayOps
{
public:

	// Nested typedefs:
	typedef typename TypeTraits<T>::NonConstType     ValueType;
	typedef GenericArrayIterator<ValueType, DERIVED> Iterator;

	// Iterators to the beginning of the array and one-past-the-end:
	Iterator begin();
	Iterator end();
	const Iterator begin() const;
	const Iterator end()   const;

	// Reverse iteration: Increasing the iterator moves it towards the beginning of the sequence.
	Iterator revBegin();
	Iterator revEnd();
	const Iterator revBegin() const;
	const Iterator revEnd()   const;

	// Mutable and immutable array access.
	// Bounds checked on a debug build. No checking on release setting.
	const ValueType & operator [] (uint index) const;
	      ValueType & operator [] (uint index);

	// First and last items of array.
	// Both fail with an assertion if the array is empty.
	ValueType & front();
	ValueType & back();
	const ValueType & front() const;
	const ValueType & back()  const;

	// Sort array from least to greatest. See `DefaultQuickSortPredicate`.
	void sort();

	// Sorts with a custom sort predicate.
	template<class PRED> void sort(const PRED & pred);

	// Returns true if the array is sorted in ascending order. Applies operator < on the items.
	bool isSorted() const;

	// Returns true if the array is sorted according to custom predicate.
	template<class PRED> bool isSorted(const PRED & pred) const;

	// Find first match in a sorted array. Returns iterator to item or `Array::end()` if not found.
	// Applies default binary search predicate, which relies on the < > operators. See `DefaultBinarySearchPredicate`.
	Iterator findSortedMatch(const ValueType & needle) const;

	// Find first match in a sorted array using a custom comparison predicate.
	// Returns iterator to item or `Array::end()` if not found.
	template<class U, class PRED> Iterator findSorted(const U & needle, const PRED & pred) const;

	// Find first match using a linear search (doesn't assume sorted).
	// Return iterator or `Array::end()` if item not found. Compares items with `==` operator.
	Iterator findUnsortedMatch(const ValueType & needle) const;

	// Find first item for which `pred` returns true (linear search).
	// Return iterator to item or `Array::end()` if not found.
	template<class PRED> Iterator findUnsorted(const PRED & pred) const;

	// Find first item for which `pred` returns true (linear search). Return iterator to item
	// or `Array::end()` if not found. This one allows passing an extra parameter to the predicate.
	template<class PRED, class PARAM> Iterator findUnsorted(const PRED & pred, const PARAM & param) const;

	// Count number of items matching the given key item. Compares items with `==` operator.
	uint countAllMatching(const ValueType & key) const;

	// Returns the index for the pointer to an item in the array. -1 if pointer is outside the array's range.
	int indexOfPointer(const ValueType * ptr) const;

	// Fills the array with `fillWith` (only affects current size, not capacity).
	void fill(const ValueType & fillWith);

	// Fills the array with zeros (AKA "memset" it). `count` can range from 0 to capacity.
	// Care should be taken to avoid calling this method on a non-POD array.
	void zeroFill(uint count);

protected:

	// External instantiation wouldn't make sense.
	CommonArrayOps();

	// Shorthand for a static_cast.
	DERIVED & impl();
	const DERIVED & impl() const;

	// Iterator from index.
	Iterator makeIterator(uint index) const;
};

} // namespace internal {}

// ========================================================
// template class Array<T, ALIGNMENT>:
// ========================================================

// Dynamically growable sequential array similar to `std::vector`.
// Supports a user defined alignment for the base of the array
// via the `ALIGNMENT` template argument. By default this is the
// natural alignment of type T.
//
// Array is not copyable, so no returning it by value. This was
// intentional to prevent expensive accidental copies.
//
// Each reallocation will add some extra slots to the array.
// `pushBack()` reallocations will always double the current
// capacity plus add a few extra slots. Check `AllocExtra` in
// the implementation for details.
//
// This class supports both Plain Old Data (POD) and complex
// User Defined Types (UDT). Constructors and destructors
// are properly called for UDTs.
template
<
	class T,
	uint ALIGNMENT = ALIGNMENT_OF(T)
>
class Array
	: private NonCopyable
	, public internal::CommonArrayOps< T, Array<T, ALIGNMENT> >
{
public:

	// Nested typedefs:
	typedef internal::CommonArrayOps< T, Array<T, ALIGNMENT> > MixinBase;
	typedef typename MixinBase::ValueType ValueType;
	typedef typename MixinBase::Iterator  Iterator;

	// Alignment used to allocate the underlaying array of T objects.
	// By default, use the natural alignment of type T.
	static constexpr uint MemoryAlignment = ALIGNMENT;

	// Default constructor creates an empty array.
	// First insertion will init with default capacity.
	Array();

	// Allocate array and set initial size to `sizeInItems`.
	// Optionally fills the newly allocated items with `fillWith`.
	explicit Array(uint sizeInItems);
	Array(uint sizeInItems, const ValueType & fillWith);

	// Construct by copying an external C-style array.
	Array(const ValueType * items, uint count);
	template<uint N> explicit Array(const ValueType (&items)[N]);

	// Destroys all the items and frees all memory.
	~Array();

	// Explicitly allocate storage or expand current. Size not changed.
	// No-op if new capacity is less than or equal the current.
	void allocate();
	void allocate(uint capacityHint);
	void allocateExact(uint capacityWanted);

	// Manually frees all memory and sets size to zero, destroying all items.
	void deallocate();

	// Sets size to zero but doesn't deallocate. All items are destroyed.
	void clear();

	// Ensure space is allocated and sets size to `newSizeInItems`.
	// Newly allocated items are default initialized. No-op if new size <= current size.
	void resize(uint newSizeInItems);

	// Ensure space is allocated and sets size to `newSizeInItems`, initializing
	// any newly allocated items to `fillWith`. No-op if new size <= current size.
	void resize(uint newSizeInItems, const ValueType & fillWith);

	// Insert at the right-side end of the array, possibly growing it to make room.
	void pushBack(const ValueType & item);
	void pushBack(const ValueType * items, uint count);
	template<uint N> void pushBack(const ValueType (&items)[N]);

	// Remove one or more items from the right-side end of the array.
	// No array shifting or reallocations are performed. Removed items are destroyed.
	// Both methods are no-ops if the array is already empty.
	void popBack();
	void popBack(uint count);

	// Insert at index, possibly growing the array and shifting to the right to make room.
	void insert(const ValueType & item, uint index);
	void insert(const ValueType * items, uint startIndex, uint count);
	template<uint N> void insert(const ValueType (&items)[N], uint startIndex);

	// Remove item(s) at index (inclusive) and shift the array to the
	// left by 1 to `count` items. Removed items are destroyed.
	// Both methods return the new size in items of the array after removal.
	uint remove(uint index);
	uint remove(uint startIndex, uint count);

	// Remove item pointed by the iterator and shift the array to the left by 1.
	// Returns an iterator pointing to the new location of the item that followed the item removed,
	// which is the array's end if the operation removed the last item in it. Removed item is destroyed.
	Iterator remove(Iterator position);

	// Removes the item at the given index and places the last item of the array into its spot.
	// Does not preserve ordering. Returns the size of the array after removal.
	uint removeAndSwap(uint index);

	// Remove duplicate items in a sorted array, according to operator `==`.
	// Fails with a debug assertion if the array is not sorted. Returns the number of items removed.
	uint removeSortedDuplicates();

	// Remove duplicate items in a sorted array, according to provided predicate.
	// Fails with a debug assertion if the array is not sorted. Returns the number of items removed.
	template<class PRED> uint removeSortedDuplicates(const PRED & pred);

	// Removes from the array all items for which predicate
	// `pred` returns true. Returns the number of items removed.
	template<class PRED> uint removeIf(const PRED & pred);

	// Removes from the array all items for which predicate `pred` returns true.
	// Returns the number of items removed. This one allows passing an extra parameter to the predicate.
	template<class PRED, class PARAM> uint removeIf(const PRED & pred, const PARAM & param);

	// Removes all items matching the key. Items are compared
	// with `==` operator. Returns the number of items removed.
	uint removeAllMatching(const ValueType & key);

	// `pushBack()` the item if `findSortedMatch()` fails to find it.
	bool pushIfUniqueSorted(const ValueType & item);

	// `pushBack()` the item if `findUnsortedMatch()` fails to find it.
	bool pushIfUniqueUnsorted(const ValueType & item);

	// Transfers all items of this array into `dest`, making this array empty.
	// Will generate a runtime assertion if `dest` is not an empty array.
	void transferTo(Array & dest);

	// Copies all items of this array into `dest`. Destination is not cleared before
	// copying, so if it is not empty, previous items will remain at the start of `dest`.
	void copyTo(Array & dest) const;

	// Raw data pointer access (asserts if the array is not allocated):
	ValueType * getData();
	const ValueType * getData() const;

	// Misc state queries:
	bool isEmpty() const;
	bool isValid() const;
	bool isAllocated() const;

	// Size in elements/items, slots currently allocated
	// and size in bytes of underlaying array:
	uint getSize() const;
	uint getCapacity() const;
	uint getMemoryBytes() const;

private:

	// Init member variables to defaults.
	void init();

	// This is used by insert() to shift several elements to the right.
	static void shiftRight(ValueType * array, uint destIndex, uint sourceIndex, uint count);

	// Array data: Pointer to array base, slots allocated and used entries.
	ValueType * arrayPtr;
	uint size, capacity;
};

//
// Array<T> aliases for common types:
//
typedef Array<bool>   BoolArray;
typedef Array<ubyte>  ByteArray;
typedef Array<int16>  I16Array;
typedef Array<uint16> U16Array;
typedef Array<int32>  I32Array;
typedef Array<uint32> U32Array;

// ========================================================
// template class PtrArray<T>:
// ========================================================

// Growable array that specializes Array<T> for pointers.
// The PtrArray owns every pointer inserted into it, thus
// deleting all of its non-null entries in the destructor.
//
// Note: This class doesn't implement the whole interface of Array<T>
// as some methods are not fully compatible with the pointer ownership semantics.
//
template<class T>
class PtrArray
	: private NonCopyable
	, public internal::CommonArrayOps< T *, PtrArray<T> >
{
public:

	// Nested typedefs:
	typedef internal::CommonArrayOps< T *, PtrArray<T> > MixinBase;
	typedef typename MixinBase::ValueType ValueType;
	typedef typename MixinBase::Iterator  Iterator;
	typedef Array<T *> BackingArrayType;

	// Default constructor creates an empty array.
	// First insertion will init with default capacity.
	PtrArray();

	// Allocate array and set initial size to `sizeInItems`.
	explicit PtrArray(uint sizeInItems);

	// Deletes each individual pointer inside the array and frees the array of pointers.
	~PtrArray();

	// Explicitly allocate storage or expand current. Size not changed.
	// No-op if new capacity is less than or equal the current.
	void allocate();
	void allocate(uint capacityHint);
	void allocateExact(uint capacityWanted);

	// Manually frees all memory and sets size to zero, deleting all non-null items.
	void deallocate();

	// Sets size to zero but doesn't deallocate. All non-null items are deleted.
	void clear();

	// Ensure space is allocated and sets size to `newSizeInItems`.
	// Newly allocated items are initialized to null. No-op if new size <= current size.
	void resize(uint newSizeInItems);

	// Insert at the right-side end of the array, possibly growing it to make room.
	// It is assumed that the new item is unique inside the array and can be deallocated by operator `delete`.
	void pushBack(ValueType item);

	// Remove one or more items from the right-side end of the array.
	// No array shifting or reallocations are performed. Removed items are deleted.
	void popBack();
	void popBack(uint count);

	// Insert at index, possibly growing the array and shifting to the right to make room.
	// It is assumed that the new item is unique inside the array and can be deallocated by operator `delete`.
	void insert(ValueType item, uint index);

	// Remove item(s) at index (inclusive) and shift the array to the
	// left by 1 to `count` items. Removed items are deleted.
	// Both methods return the new size in items of the array after removal.
	uint remove(uint index);
	uint remove(uint startIndex, uint count);

	// Remove item pointed by the iterator and shift the array to the left by 1.
	// Returns an iterator pointing to the new location of the item that followed the item removed,
	// which is the array's end if the operation removed the last item in it. Removed item is deleted.
	Iterator remove(Iterator position);

	// Removes the item at the given index and places the last item of the array into its spot.
	// Does not preserve ordering. Returns the size of the array after removal. Removed item is deleted.
	uint removeAndSwap(uint index);

	// Deletes the item at the given item and sets the index to null.
	// The array is not resized nor shifted. Size remains unchanged.
	void deleteIndex(uint index);

	// Swap the pointer at indexA with the pointer at indexB.
	// This method will assert if the indexes are out-of-bounds.
	void swapItems(uint indexA, uint indexB);

	// Transfers all items of this array into `dest`, making this array empty.
	// Will generate a runtime assertion if `dest` is not an empty array.
	void transferTo(PtrArray & dest);

	// Raw data pointer access (asserts if the array is not allocated):
	ValueType * getData();
	const ValueType * getData() const;

	// Misc state queries:
	bool isEmpty() const;
	bool isValid() const;
	bool isAllocated() const;

	// Size in elements/items, slots currently allocated
	// and size in bytes of underlaying array:
	uint getSize() const;
	uint getCapacity() const;
	uint getMemoryBytes() const;

private:

	BackingArrayType pointers;
};

// ========================================================
// template class SizedArray<T, SIZE, ALIGNMENT>:
// ========================================================

// SizedArray is a replacement for a normal C-style static array.
//
//    int myArray[ARRAY_SIZE];
//
// Becomes:
//
//    SizedArray<int, ARRAY_SIZE [, ALIGNMENT]> myArray;
//
// Has no performance overhead in release builds, but
// does index range checking in debug builds.
//
// Memory is allocated inline and statically with the
// object, rather than on the heap.
//
// Unlike Array<>, there are no fields other than the
// actual raw data, and the size is fixed. Optional custom
// alignment for the base of the array may be provided.
//
// This class supports both Plain Old Data (POD) and
// complex User Defined Types (UDT).
template
<
	class T,
	uint SIZE,
	uint ALIGNMENT = ALIGNMENT_OF(T)
>
class SizedArray
	: private NonCopyable
	, public internal::CommonArrayOps< T, SizedArray<T, SIZE, ALIGNMENT> >
{
public:

	// Nested typedefs:
	typedef internal::CommonArrayOps< T, SizedArray<T, SIZE, ALIGNMENT> > MixinBase;
	typedef typename MixinBase::ValueType ValueType;
	typedef typename MixinBase::Iterator  Iterator;

	// Alignment used to allocate the underlaying array of T objects.
	// By default, use the natural alignment of type T.
	static constexpr uint MemoryAlignment = ALIGNMENT;

	// Default initialize all items.
	SizedArray();

	// Fill all with given item.
	explicit SizedArray(const ValueType & fillWith);

	// Fill with external C-style array:
	SizedArray(const ValueType * items, uint count);

	// Fill with fixed-size C-style array (templated size inference):
	template<uint N>
	explicit SizedArray(const ValueType (&items)[N]);

	// Copies the contents of this SizedArray to a dynamic Array<T> object.
	// `dest` is not cleared first, so any elements present on it will remain.
	template<class ARRAY_T, uint ARRAY_ALIGN>
	void toArray(Array<ARRAY_T, ARRAY_ALIGN> & dest) const;

	// Fills SIZE elements with zeros (memset).
	void zeroFill();

	// Raw data pointer access (never null):
	ValueType * getData();
	const ValueType * getData() const;

	// Misc state queries for compatibility with Array<> (never changes):
	static constexpr bool isEmpty();
	static constexpr bool isValid();
	static constexpr bool isAllocated();

	// Size in elements/items, slots currently allocated
	// and size in bytes of underlaying array (never changes):
	static constexpr uint getSize();
	static constexpr uint getCapacity();
	static constexpr uint getMemoryBytes();

private:

	ATTRIBUTE_ALIGNED(MemoryAlignment, ValueType array[SIZE]);
};

// ========================================================
// template class SizedArray2d<T, WIDTH, HEIGHT>:
// ========================================================

// Bounds checked replacement for a 2D array[x][y].
template
<
	class T,
	uint WIDTH,
	uint HEIGHT,
	uint ALIGNMENT = ALIGNMENT_OF(T)
>
class SizedArray2d
{
public:

	typedef SizedArray<SizedArray<T, HEIGHT, ALIGNMENT>, WIDTH, ALIGNMENT> Type;
	Type matrix;

	const T & operator() (const uint x, const uint y) const { return matrix[x][y]; }
	      T & operator() (const uint x, const uint y)       { return matrix[x][y]; }

	static constexpr uint getWidth()  { return WIDTH;  }
	static constexpr uint getHeight() { return HEIGHT; }
};

// ========================================================
// template class SizedArray3d<T, WIDTH, HEIGHT, DEPTH>:
// ========================================================

// Bounds checked replacement for a 3D array[x][y][z].
template
<
	class T,
	uint WIDTH,
	uint HEIGHT,
	uint DEPTH,
	uint ALIGNMENT = ALIGNMENT_OF(T)
>
class SizedArray3d
{
public:

	typedef SizedArray<SizedArray<SizedArray<T, DEPTH, ALIGNMENT>, HEIGHT, ALIGNMENT>, WIDTH, ALIGNMENT> Type;
	Type matrix;

	const T & operator() (const uint x, const uint y, const uint z) const { return matrix[x][y][z]; }
	      T & operator() (const uint x, const uint y, const uint z)       { return matrix[x][y][z]; }

	static constexpr uint getWidth()  { return WIDTH;  }
	static constexpr uint getHeight() { return HEIGHT; }
	static constexpr uint getDepth()  { return DEPTH;  }
};

/*
 * ---- Inline template implementations begin here: ----
 */

namespace internal
{

// ================================================================================================
// GenericArrayIterator<T, ARRAY_TYPE> inline methods:
// ================================================================================================

// ========================================================
// GenericArrayIterator::GenericArrayIterator():
// ========================================================

template<class T, class ARRAY_TYPE>
GenericArrayIterator<T, ARRAY_TYPE>::GenericArrayIterator()
	: myArray(nullptr)
	, index(-1)
	, direction(0)
{
	// An invalid iterator.
}

// ========================================================
// GenericArrayIterator::GenericArrayIterator():
// ========================================================

template<class T, class ARRAY_TYPE>
GenericArrayIterator<T, ARRAY_TYPE>::GenericArrayIterator(const ArrayType * owner, const int dir, const int startIndex)
	: myArray(const_cast<ArrayType *>(owner))
	, index(startIndex)
	, direction(dir)
{
	DEBUG_CHECK(direction == -1 || direction == 1);
}

// ========================================================
// GenericArrayIterator::operator* :
// ========================================================

template<class T, class ARRAY_TYPE>
const typename GenericArrayIterator<T, ARRAY_TYPE>::ValueType & GenericArrayIterator<T, ARRAY_TYPE>::operator* () const
{
	validateDereference();
	return (*myArray)[index];
}

// ========================================================
// GenericArrayIterator::operator* :
// ========================================================

template<class T, class ARRAY_TYPE>
typename GenericArrayIterator<T, ARRAY_TYPE>::ValueType & GenericArrayIterator<T, ARRAY_TYPE>::operator* ()
{
	validateDereference();
	return (*myArray)[index];
}

// ========================================================
// GenericArrayIterator::operator-> :
// ========================================================

template<class T, class ARRAY_TYPE>
const typename GenericArrayIterator<T, ARRAY_TYPE>::ValueType * GenericArrayIterator<T, ARRAY_TYPE>::operator-> () const
{
	validateDereference();
	return &(*myArray)[index];
}

// ========================================================
// GenericArrayIterator::operator-> :
// ========================================================

template<class T, class ARRAY_TYPE>
typename GenericArrayIterator<T, ARRAY_TYPE>::ValueType * GenericArrayIterator<T, ARRAY_TYPE>::operator-> ()
{
	validateDereference();
	return &(*myArray)[index];
}

// ========================================================
// GenericArrayIterator::operator-- (pre-decrement):
// ========================================================

template<class T, class ARRAY_TYPE>
const GenericArrayIterator<T, ARRAY_TYPE> & GenericArrayIterator<T, ARRAY_TYPE>::operator-- () const
{
	validateDecrement(1);
	index -= direction;
	return *this;
}

// ========================================================
// GenericArrayIterator::operator-- (post-decrement):
// ========================================================

template<class T, class ARRAY_TYPE>
GenericArrayIterator<T, ARRAY_TYPE> GenericArrayIterator<T, ARRAY_TYPE>::operator-- (int) const
{
	validateDecrement(1);
	GenericArrayIterator<T, ARRAY_TYPE> tmp(*this);
	index -= direction;
	return tmp; // Return old, pre-decrement value.
}

// ========================================================
// GenericArrayIterator::operator++ (pre-increment):
// ========================================================

template<class T, class ARRAY_TYPE>
const GenericArrayIterator<T, ARRAY_TYPE> & GenericArrayIterator<T, ARRAY_TYPE>::operator++ () const
{
	validateIncrement(1);
	index += direction;
	return *this;
}

// ========================================================
// GenericArrayIterator::operator++ (post-increment):
// ========================================================

template<class T, class ARRAY_TYPE>
GenericArrayIterator<T, ARRAY_TYPE> GenericArrayIterator<T, ARRAY_TYPE>::operator++ (int) const
{
	validateIncrement(1);
	GenericArrayIterator<T, ARRAY_TYPE> tmp(*this);
	index += direction;
	return tmp; // Return old, pre-increment value.
}

// ========================================================
// GenericArrayIterator::operator += :
// ========================================================

template<class T, class ARRAY_TYPE>
GenericArrayIterator<T, ARRAY_TYPE> & GenericArrayIterator<T, ARRAY_TYPE>::operator += (const int displacement)
{
	validateIncrement(displacement);
	index += displacement * direction;
    return *this;
}

// ========================================================
// GenericArrayIterator::operator -= :
// ========================================================

template<class T, class ARRAY_TYPE>
GenericArrayIterator<T, ARRAY_TYPE> & GenericArrayIterator<T, ARRAY_TYPE>::operator -= (const int displacement)
{
	validateDecrement(displacement);
	index -= displacement * direction;
    return *this;
}

// ========================================================
// GenericArrayIterator::operator + :
// ========================================================

template<class T, class ARRAY_TYPE>
GenericArrayIterator<T, ARRAY_TYPE> GenericArrayIterator<T, ARRAY_TYPE>::operator + (const int displacement) const
{
	validateIncrement(displacement);
	return GenericArrayIterator<T, ARRAY_TYPE>(myArray, direction, index + (displacement * direction));
}

// ========================================================
// GenericArrayIterator::operator - :
// ========================================================

template<class T, class ARRAY_TYPE>
GenericArrayIterator<T, ARRAY_TYPE> GenericArrayIterator<T, ARRAY_TYPE>::operator - (const int displacement) const
{
	validateDecrement(displacement);
	return GenericArrayIterator<T, ARRAY_TYPE>(myArray, direction, index - (displacement * direction));
}

// ========================================================
// GenericArrayIterator::operator == :
// ========================================================

template<class T, class ARRAY_TYPE>
bool GenericArrayIterator<T, ARRAY_TYPE>::operator == (const GenericArrayIterator<T, ARRAY_TYPE> & other) const
{
	DEBUG_CHECK(myArray   == other.myArray   && "Iterators belong to different arrays!");
	DEBUG_CHECK(direction == other.direction && "Different kind of iterators!");
	return index == other.index;
}

// ========================================================
// GenericArrayIterator::operator != :
// ========================================================

template<class T, class ARRAY_TYPE>
bool GenericArrayIterator<T, ARRAY_TYPE>::operator != (const GenericArrayIterator<T, ARRAY_TYPE> & other) const
{
	DEBUG_CHECK(myArray   == other.myArray   && "Iterators belong to different arrays!");
	DEBUG_CHECK(direction == other.direction && "Different kind of iterators!");
	return index != other.index;
}

// ========================================================
// GenericArrayIterator::getCurrentIndex():
// ========================================================

template<class T, class ARRAY_TYPE>
int GenericArrayIterator<T, ARRAY_TYPE>::getCurrentIndex() const
{
	return index;
}

// ========================================================
// GenericArrayIterator::getArray():
// ========================================================

template<class T, class ARRAY_TYPE>
const typename GenericArrayIterator<T, ARRAY_TYPE>::ArrayType * GenericArrayIterator<T, ARRAY_TYPE>::getArray() const
{
	return myArray;
}

// ========================================================
// GenericArrayIterator::validateDereference():
// ========================================================

template<class T, class ARRAY_TYPE>
void GenericArrayIterator<T, ARRAY_TYPE>::validateDereference() const
{
	DEBUG_CHECK(myArray != nullptr && myArray->isValid() && "Invalid array iterator!");
	DEBUG_CHECK(index >= 0 && index < static_cast<int>(myArray->getSize()) && "Invalid array iterator dereference!");
}

// ========================================================
// GenericArrayIterator::validateIncrement():
// ========================================================

template<class T, class ARRAY_TYPE>
void GenericArrayIterator<T, ARRAY_TYPE>::validateIncrement(const int displacement) const
{
	DEBUG_CHECK(myArray != nullptr && myArray->isValid() && "Invalid array iterator!");
	DEBUG_CHECK((index + (displacement * direction)) <= static_cast<int>(myArray->getSize()) && "Out-of-bounds iterator increment!");

	#ifdef ATLAS_RELEASE
	(void)displacement;
	#endif // ATLAS_RELEASE
}

// ========================================================
// GenericArrayIterator::validateDecrement():
// ========================================================

template<class T, class ARRAY_TYPE>
void GenericArrayIterator<T, ARRAY_TYPE>::validateDecrement(const int displacement) const
{
	DEBUG_CHECK(myArray != nullptr && myArray->isValid() && "Invalid array iterator!");
	DEBUG_CHECK((index - (displacement * direction)) >= 0 && "Out-of-bounds iterator decrement!");

	#ifdef ATLAS_RELEASE
	(void)displacement;
	#endif // ATLAS_RELEASE
}

// ========================================================
// Additional global GenericArrayIterator operator + :
// ========================================================

template<class T, class ARRAY_TYPE>
GenericArrayIterator<T, ARRAY_TYPE> operator + (const int displacement, GenericArrayIterator<T, ARRAY_TYPE> it)
{
	it += displacement;
	return it;
}

// ========================================================
// Additional global GenericArrayIterator operator - :
// ========================================================

template<class T, class ARRAY_TYPE>
GenericArrayIterator<T, ARRAY_TYPE> operator - (const int displacement, GenericArrayIterator<T, ARRAY_TYPE> it)
{
	it -= displacement;
	return it;
}

// ================================================================================================
// CommonArrayOps<T, DERIVED> inline methods:
// ================================================================================================

// ========================================================
// CommonArrayOps::CommonArrayOps():
// ========================================================

template<class T, class DERIVED>
CommonArrayOps<T, DERIVED>::CommonArrayOps()
{
	// No-op.
}

// ========================================================
// CommonArrayOps::impl():
// ========================================================

template<class T, class DERIVED>
DERIVED & CommonArrayOps<T, DERIVED>::impl()
{
	return *static_cast<DERIVED *>(this);
}

// ========================================================
// CommonArrayOps::impl():
// ========================================================

template<class T, class DERIVED>
const DERIVED & CommonArrayOps<T, DERIVED>::impl() const
{
	return *static_cast<const DERIVED *>(this);
}

// ========================================================
// CommonArrayOps::begin():
// ========================================================

template<class T, class DERIVED>
typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::begin()
{
	return Iterator(&impl(), 1, 0);
}

// ========================================================
// CommonArrayOps::end():
// ========================================================

template<class T, class DERIVED>
typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::end()
{
	return Iterator(&impl(), 1, impl().getSize());
}

// ========================================================
// CommonArrayOps::begin():
// ========================================================

template<class T, class DERIVED>
const typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::begin() const
{
	return Iterator(&impl(), 1, 0);
}

// ========================================================
// CommonArrayOps::end():
// ========================================================

template<class T, class DERIVED>
const typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::end() const
{
	return Iterator(&impl(), 1, impl().getSize());
}

// ========================================================
// CommonArrayOps::revBegin():
// ========================================================

template<class T, class DERIVED>
typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::revBegin()
{
	return Iterator(&impl(), -1, impl().getSize() - 1);
}

// ========================================================
// CommonArrayOps::revEnd():
// ========================================================

template<class T, class DERIVED>
typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::revEnd()
{
	return Iterator(&impl(), -1, -1);
}

// ========================================================
// CommonArrayOps::revBegin():
// ========================================================

template<class T, class DERIVED>
const typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::revBegin() const
{
	return Iterator(&impl(), -1, impl().getSize() - 1);
}

// ========================================================
// CommonArrayOps::revEnd():
// ========================================================

template<class T, class DERIVED>
const typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::revEnd() const
{
	return Iterator(&impl(), -1, -1);
}

// ========================================================
// CommonArrayOps::makeIterator():
// ========================================================

template<class T, class DERIVED>
typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::makeIterator(const uint index) const
{
	// Make a forward iterator.
	return Iterator(&impl(), 1, index);
}

// ========================================================
// CommonArrayOps::operator[]:
// ========================================================

template<class T, class DERIVED>
const typename CommonArrayOps<T, DERIVED>::ValueType & CommonArrayOps<T, DERIVED>::operator [] (const uint index) const
{
	DEBUG_CHECK(index < impl().getSize() && "Array index out-of-range!");
	return impl().getData()[index];
}

// ========================================================
// CommonArrayOps::operator[]:
// ========================================================

template<class T, class DERIVED>
typename CommonArrayOps<T, DERIVED>::ValueType & CommonArrayOps<T, DERIVED>::operator [] (const uint index)
{
	DEBUG_CHECK(index < impl().getSize() && "Array index out-of-range!");
	return impl().getData()[index];
}

// ========================================================
// CommonArrayOps::front():
// ========================================================

template<class T, class DERIVED>
typename CommonArrayOps<T, DERIVED>::ValueType & CommonArrayOps<T, DERIVED>::front()
{
	DEBUG_CHECK(impl().getSize() != 0 && "Empty array!");
	return impl().getData()[0];
}

// ========================================================
// CommonArrayOps::back():
// ========================================================

template<class T, class DERIVED>
typename CommonArrayOps<T, DERIVED>::ValueType & CommonArrayOps<T, DERIVED>::back()
{
	DEBUG_CHECK(impl().getSize() != 0 && "Empty array!");
	return impl().getData()[impl().getSize() - 1];
}

// ========================================================
// CommonArrayOps::front():
// ========================================================

template<class T, class DERIVED>
const typename CommonArrayOps<T, DERIVED>::ValueType & CommonArrayOps<T, DERIVED>::front() const
{
	DEBUG_CHECK(impl().getSize() != 0 && "Empty array!");
	return impl().getData()[0];
}

// ========================================================
// CommonArrayOps::back():
// ========================================================

template<class T, class DERIVED>
const typename CommonArrayOps<T, DERIVED>::ValueType & CommonArrayOps<T, DERIVED>::back() const
{
	DEBUG_CHECK(impl().getSize() != 0 && "Empty array!");
	return impl().getData()[impl().getSize() - 1];
}

// ========================================================
// CommonArrayOps::sort():
// ========================================================

template<class T, class DERIVED>
void CommonArrayOps<T, DERIVED>::sort()
{
	if (impl().getSize() > 1)
	{
		quickSort(impl().getData(), impl().getSize(), DefaultQuickSortPredicate<ValueType>());
	}
}

// ========================================================
// CommonArrayOps::sort():
// ========================================================

template<class T, class DERIVED>
template<class PRED>
void CommonArrayOps<T, DERIVED>::sort(const PRED & pred)
{
	if (impl().getSize() > 1)
	{
		quickSort(impl().getData(), impl().getSize(), pred);
	}
}

// ========================================================
// CommonArrayOps::isSorted():
// ========================================================

template<class T, class DERIVED>
bool CommonArrayOps<T, DERIVED>::isSorted() const
{
	const uint size = impl().getSize();
	if (size == 0)
	{
		return true; // Considered sorted if empty.
	}

	const ValueType * const array = impl().getData();
	for (uint i = 1; i < size; ++i)
	{
		if (array[i] < array[i - 1])
		{
			return false;
		}
	}

	return true;
}

// ========================================================
// CommonArrayOps::isSorted():
// ========================================================

template<class T, class DERIVED>
template<class PRED>
bool CommonArrayOps<T, DERIVED>::isSorted(const PRED & pred) const
{
	const uint size = impl().getSize();
	if (size == 0)
	{
		return true; // Considered sorted if empty.
	}

	const ValueType * const array = impl().getData();
	for (uint i = 1; i < size; ++i)
	{
		// Predicate should return if the element passed as first argument
		// is considered to go before the second argument. AKA, if arg0 < arg1.
		if (pred(array[i], array[i - 1]))
		{
			return false;
		}
	}

	return true;
}

// ========================================================
// CommonArrayOps::findSortedMatch():
// ========================================================

template<class T, class DERIVED>
typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::findSortedMatch(const ValueType & needle) const
{
	return findSorted(needle, DefaultBinarySearchPredicate<ValueType>());
}

// ========================================================
// CommonArrayOps::findSorted():
// ========================================================

template<class T, class DERIVED>
template<class U, class PRED>
typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::findSorted(const U & needle, const PRED & pred) const
{
	DEBUG_CHECK(isSorted());

	const uint size = impl().getSize();
	if (size == 0)
	{
		return end();
	}

	const int index = binarySearch(impl().getData(), size, needle, pred);
	if (index < 0)
	{
		return end();
	}

	return makeIterator(index);
}

// ========================================================
// CommonArrayOps::findUnsortedMatch():
// ========================================================

template<class T, class DERIVED>
typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::findUnsortedMatch(const ValueType & needle) const
{
	const uint size = impl().getSize();
	if (size == 0)
	{
		return end();
	}

	const ValueType * const array = impl().getData();
	for (uint i = 0; i < size; ++i)
	{
		if (array[i] == needle)
		{
			return makeIterator(i);
		}
	}
	return end();
}

// ========================================================
// CommonArrayOps::findUnsorted():
// ========================================================

template<class T, class DERIVED>
template<class PRED>
typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::findUnsorted(const PRED & pred) const
{
	const uint size = impl().getSize();
	if (size == 0)
	{
		return end();
	}

	const ValueType * const array = impl().getData();
	for (uint i = 0; i < size; ++i)
	{
		if (pred(array[i]))
		{
			return makeIterator(i);
		}
	}
	return end();
}

// ========================================================
// CommonArrayOps::findUnsorted():
// ========================================================

template<class T, class DERIVED>
template<class PRED, class PARAM>
typename CommonArrayOps<T, DERIVED>::Iterator CommonArrayOps<T, DERIVED>::findUnsorted(const PRED & pred, const PARAM & param) const
{
	const uint size = impl().getSize();
	if (size == 0)
	{
		return end();
	}

	const ValueType * const array = impl().getData();
	for (uint i = 0; i < size; ++i)
	{
		if (pred(array[i], param))
		{
			return makeIterator(i);
		}
	}
	return end();
}

// ========================================================
// CommonArrayOps::countAllMatching():
// ========================================================

template<class T, class DERIVED>
uint CommonArrayOps<T, DERIVED>::countAllMatching(const ValueType & key) const
{
	const uint size = impl().getSize();
	if (size == 0)
	{
		return 0;
	}

	uint foundCount = 0;
	const ValueType * const array = impl().getData();
	for (uint i = 0; i < size; ++i)
	{
		if (array[i] == key)
		{
			++foundCount;
		}
	}
	return foundCount;
}

// ========================================================
// CommonArrayOps::indexOfPointer():
// ========================================================

template<class T, class DERIVED>
int CommonArrayOps<T, DERIVED>::indexOfPointer(const ValueType * ptr) const
{
	const int index = static_cast<int>(ptr - impl().getData());
	if (index < 0 || static_cast<uint>(index) >= impl().getSize())
	{
		return -1;
	}
	return index;
}

// ========================================================
// CommonArrayOps::fill():
// ========================================================

template<class T, class DERIVED>
void CommonArrayOps<T, DERIVED>::fill(const ValueType & fillWith)
{
	const uint size = impl().getSize();
	if (size != 0)
	{
		fillInitialized(impl().getData(), fillWith, size);
	}
}

// ========================================================
// CommonArrayOps::zeroFill():
// ========================================================

template<class T, class DERIVED>
void CommonArrayOps<T, DERIVED>::zeroFill(const uint count)
{
	// Free to zero-fill beyond size but up to capacity.
	DEBUG_CHECK(count <= impl().getCapacity());
	clearPodArray(impl().getData(), count);
}

} // namespace internal {}

// ================================================================================================
// Array<T, ALIGNMENT> inline methods and related helpers:
// ================================================================================================

//
// Global begin/end ranges compatible with "foreach"-style iteration:
//
template<class T, uint ALIGNMENT>
typename Array<T, ALIGNMENT>::Iterator begin(Array<T, ALIGNMENT> & array)
{ return array.begin(); }

template<class T, uint ALIGNMENT>
typename Array<T, ALIGNMENT>::Iterator end(Array<T, ALIGNMENT> & array)
{ return array.end(); }

template<class T, uint ALIGNMENT>
typename Array<T, ALIGNMENT>::Iterator revBegin(Array<T, ALIGNMENT> & array)
{ return array.revBegin(); }

template<class T, uint ALIGNMENT>
typename Array<T, ALIGNMENT>::Iterator revEnd(Array<T, ALIGNMENT> & array)
{ return array.revEnd(); }

//
// Global begin/end ranges (const) compatible with "foreach"-style iteration:
//
template<class T, uint ALIGNMENT>
const typename Array<T, ALIGNMENT>::Iterator begin(const Array<T, ALIGNMENT> & array)
{ return array.begin(); }

template<class T, uint ALIGNMENT>
const typename Array<T, ALIGNMENT>::Iterator end(const Array<T, ALIGNMENT> & array)
{ return array.end(); }

template<class T, uint ALIGNMENT>
const typename Array<T, ALIGNMENT>::Iterator revBegin(const Array<T, ALIGNMENT> & array)
{ return array.revBegin(); }

template<class T, uint ALIGNMENT>
const typename Array<T, ALIGNMENT>::Iterator revEnd(const Array<T, ALIGNMENT> & array)
{ return array.revEnd(); }

//
// arrayLength() overload for Array<T, ALIGNMENT>.
//
template<class T, uint ALIGNMENT>
uint arrayLength(const Array<T, ALIGNMENT> & array) { return array.getSize(); }

// ========================================================
// Array::Array():
// ========================================================

template<class T, uint ALIGNMENT>
Array<T, ALIGNMENT>::Array()
{
	init();
	// First insertion will allocate storage.
}

// ========================================================
// Array::Array():
// ========================================================

template<class T, uint ALIGNMENT>
Array<T, ALIGNMENT>::Array(const uint sizeInItems)
{
	init();
	resize(sizeInItems);
}

// ========================================================
// Array::Array():
// ========================================================

template<class T, uint ALIGNMENT>
Array<T, ALIGNMENT>::Array(const uint sizeInItems, const ValueType & fillWith)
{
	init();
	resize(sizeInItems, fillWith);
}

// ========================================================
// Array::Array():
// ========================================================

template<class T, uint ALIGNMENT>
Array<T, ALIGNMENT>::Array(const ValueType * items, uint count)
{
	init();
	pushBack(items, count);
}

// ========================================================
// Array::Array():
// ========================================================

template<class T, uint ALIGNMENT>
template<uint N>
Array<T, ALIGNMENT>::Array(const ValueType (&items)[N])
{
	init();
	pushBack(items, N);
}

// ========================================================
// Array::~Array():
// ========================================================

template<class T, uint ALIGNMENT>
Array<T, ALIGNMENT>::~Array()
{
	deallocate();
}

// ========================================================
// Array::allocate():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::allocate()
{
	if (isAllocated())
	{
		return;
	}

	// Default to 2 initial items plus allocation
	// extra added to all reallocations.
	allocate(2);
}

// ========================================================
// Array::allocate():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::allocate(const uint capacityHint)
{
	if (capacityHint <= capacity)
	{
		return; // Doesn't shrink
	}

	// Extra elements per allocation (Powers of 2):
	constexpr uint AllocExtra = (
		(sizeof(ValueType) <= 1) ? 64 :
		(sizeof(ValueType) <= 2) ? 32 :
		(sizeof(ValueType) <= 4) ? 16 :
		(sizeof(ValueType) <= 8) ? 8  : 4
	);

	const uint newCapacity = capacityHint + AllocExtra;
	ValueType * newMemory  = core::allocate<ValueType>(newCapacity, MemoryAlignment);

	if (size != 0)
	{
		copyUninitialized(newMemory, arrayPtr, size);
		destroySequence(arrayPtr, size);
	}

	core::deallocate(arrayPtr);
	arrayPtr = newMemory;
	capacity = newCapacity;
}

// ========================================================
// Array::allocateExact():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::allocateExact(const uint capacityWanted)
{
	if (capacityWanted <= capacity)
	{
		return; // Doesn't shrink
	}

	// This one allocates the exact amount requested, without reserving
	// some extra to prevent new allocations on future array growths.
	// Use this method when you are sure the array will only be allocated once.
	//
	ValueType * newMemory = core::allocate<ValueType>(capacityWanted, MemoryAlignment);
	if (size != 0)
	{
		copyUninitialized(newMemory, arrayPtr, size);
		destroySequence(arrayPtr, size);
	}

	core::deallocate(arrayPtr);
	arrayPtr = newMemory;
	capacity = capacityWanted;
}

// ========================================================
// Array::deallocate():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::deallocate()
{
	if (!isAllocated())
	{
		return;
	}

	clear();
	core::deallocate(arrayPtr);
	arrayPtr = nullptr;
	capacity = 0;
}

// ========================================================
// Array::clear():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::clear()
{
	if (isAllocated() && size != 0)
	{
		destroySequence(arrayPtr, size);
		size = 0;
	}
}

// ========================================================
// Array::resize():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::resize(const uint newSizeInItems)
{
	if (newSizeInItems <= size)
	{
		return;
	}

	allocate(newSizeInItems);
	DEBUG_CHECK(capacity >= newSizeInItems);

	constructSequence(&getData()[size], newSizeInItems - size);
	size = newSizeInItems;
}

// ========================================================
// Array::resize():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::resize(const uint newSizeInItems, const ValueType & fillWith)
{
	if (newSizeInItems <= size)
	{
		return;
	}

	allocate(newSizeInItems);
	DEBUG_CHECK(capacity >= newSizeInItems);

	fillUninitialized(&getData()[size], fillWith, newSizeInItems - size);
	size = newSizeInItems;
}

// ========================================================
// Array::pushBack():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::pushBack(const ValueType & item)
{
	if (size == capacity)
	{
		// Double size on push back when depleted.
		// Fresh allocations start with 2 items.
		allocate((size != 0) ? (size * 2) : 2);
	}
	construct(&getData()[size++], item);
}

// ========================================================
// Array::pushBack():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::pushBack(const ValueType * items, const uint count)
{
	DEBUG_CHECK(items != nullptr);
	DEBUG_CHECK(count != 0);

	allocate(size + count);
	ValueType * array = getData();
	copyUninitialized(&array[size], items, count);
	size += count;
}

// ========================================================
// Array::pushBack():
// ========================================================

template<class T, uint ALIGNMENT>
template<uint N>
void Array<T, ALIGNMENT>::pushBack(const ValueType (&items)[N])
{
	pushBack(items, N);
}

// ========================================================
// Array::popBack():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::popBack()
{
	if (!isEmpty())
	{
		destroy(&getData()[--size]);
	}
}

// ========================================================
// Array::popBack():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::popBack(uint count)
{
	DEBUG_CHECK(count != 0);

	if (isEmpty())
	{
		return;
	}

	if (count > size)
	{
		count = size;
	}

	size -= count;
	destroySequence(&getData()[size], count);
}

// ========================================================
// Array::insert():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::insert(const ValueType & item, const uint index)
{
	// Inserting at the start of an empty array is permitted.
	if (isEmpty() && index == 0)
	{
		pushBack(item);
		return;
	}

	// Otherwise, index must be valid.
	DEBUG_CHECK(index < size && "Array index out-of-bounds!");

	allocate(size + 1);
	ValueType * array = getData();

	// Init the newly allocated element with the former last entry:
	construct(&array[size], array[size - 1]);

	// Shift the rest of array to the right and insert new item
	// (start with the one before last, since the last was already handled above).
	for (uint i = size - 1; i > index; --i)
	{
		array[i] = array[i - 1];
	}
	array[index] = item;
	++size;
}

// ========================================================
// Array::insert():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::insert(const ValueType * items, const uint startIndex, const uint count)
{
	DEBUG_CHECK(items != nullptr);
	DEBUG_CHECK(count != 0);

	// Inserting at the start of an empty array is permitted.
	if (isEmpty() && startIndex == 0)
	{
		pushBack(items, count);
		return;
	}

	// Otherwise, index must be valid.
	DEBUG_CHECK(startIndex < size && "Array index out-of-bounds!");

	// Combining copies and shifts to initialized/uninitialized memory
	// overcomplicates stuff. Better to just resize and default initialize
	// the new slots and then do a single shift and copyInitialized().
	const uint oldSize = size;
	resize(size + count);

	const uint sourceIndex = oldSize - 1;
	const uint shiftCount  = oldSize - startIndex;
	const uint destIndex   = oldSize + shiftCount - 2;

	ValueType * array = getData();
	shiftRight(array, destIndex + (count - shiftCount) + 1, sourceIndex, shiftCount);
	copyInitialized(&array[startIndex], items, count);
}

// ========================================================
// Array::insert():
// ========================================================

template<class T, uint ALIGNMENT>
template<uint N>
void Array<T, ALIGNMENT>::insert(const ValueType (&items)[N], const uint startIndex)
{
	insert(items, startIndex, N);
}

// ========================================================
// Array::remove():
// ========================================================

template<class T, uint ALIGNMENT>
uint Array<T, ALIGNMENT>::remove(const uint index)
{
	DEBUG_CHECK(index < size && "Array index out-of-bounds!");

	ValueType * array = getData();

	--size;
	for (uint i = index; i < size; ++i)
	{
		array[i] = array[i + 1];
	}

	destroy(&array[size]);
	return size;
}

// ========================================================
// Array::remove():
// ========================================================

template<class T, uint ALIGNMENT>
uint Array<T, ALIGNMENT>::remove(const uint startIndex, const uint count)
{
	DEBUG_CHECK(count != 0);
	DEBUG_CHECK(startIndex < size && "Array index out-of-bounds!");

	const uint remCount = min(count, size - startIndex);
	ValueType * array = getData();

	// Shift to the left:
	size -= remCount;
	for (uint i = startIndex; i < size; ++i)
	{
		array[i] = array[i + remCount];
	}

	// Elements at the tail have been shifted, destroy the old ones:
	destroySequence(&array[size], remCount);
	return size;
}

// ========================================================
// Array::remove():
// ========================================================

template<class T, uint ALIGNMENT>
typename Array<T, ALIGNMENT>::Iterator Array<T, ALIGNMENT>::remove(Iterator position)
{
	// Can't remove end(), that's just a marker to the item after the last.
	// Can't remove from empty array either.
	DEBUG_CHECK(position != MixinBase::end());
	DEBUG_CHECK(!isEmpty() && "Array already empty!");

	// Iterators of an array hold a reference to the owning object.
	// We can assert here to improve safety and catch errors as early as possible:
	DEBUG_CHECK(position.getArray() == this && "Iterator does not belong to this array object!");

	// Return end() if the array was cleared.
	if (remove(position.getCurrentIndex()) == 0)
	{
		return MixinBase::end();
	}

	// Return iterator to the following item, which will be
	// the same as `position` since the iterators are index based.
	return position;
}

// ========================================================
// Array::removeAndSwap():
// ========================================================

template<class T, uint ALIGNMENT>
uint Array<T, ALIGNMENT>::removeAndSwap(const uint index)
{
	DEBUG_CHECK(index < size && "Array index out-of-bounds!");

	ValueType * array = getData();

	--size;
	if (index != size)
	{
		array[index] = array[size];
	}

	destroy(&array[size]);
	return size;
}

// ========================================================
// Array::removeSortedDuplicates():
// ========================================================

template<class T, uint ALIGNMENT>
uint Array<T, ALIGNMENT>::removeSortedDuplicates()
{
	DEBUG_CHECK(MixinBase::isSorted() && "Sort the array first!");
	if (isEmpty())
	{
		return 0;
	}

	ValueType * first = getData();
	ValueType * last  = first + size;
	ValueType * iter  = first;
	while (++first != last)
	{
		if (!(*iter == *first))
		{
			*(++iter) = *first;
		}
	}
	++iter;

	first = getData();
	const uint itemsLeft = static_cast<uint>(iter - first);
	const uint removedCount = size - itemsLeft;

	// Shifted/removed items are the end. They must be
	// destroyed in case T is a complex user defined type.
	if (removedCount != 0)
	{
		size = itemsLeft;
		destroySequence(&first[size], removedCount);
	}

	return removedCount;
}

// ========================================================
// Array::removeSortedDuplicates():
// ========================================================

template<class T, uint ALIGNMENT>
template<class PRED>
uint Array<T, ALIGNMENT>::removeSortedDuplicates(const PRED & pred)
{
	DEBUG_CHECK(MixinBase::isSorted() && "Sort the array first!");
	if (isEmpty())
	{
		return 0;
	}

	ValueType * first = getData();
	ValueType * last  = first + size;
	ValueType * iter  = first;
	while (++first != last)
	{
		if (!pred(*iter, *first))
		{
			*(++iter) = *first;
		}
	}
	++iter;

	first = getData();
	const uint itemsLeft = static_cast<uint>(iter - first);
	const uint removedCount = size - itemsLeft;

	// Shifted/removed items are the end. They must be
	// destroyed in case T is a complex user defined type.
	if (removedCount != 0)
	{
		size = itemsLeft;
		destroySequence(&first[size], removedCount);
	}

	return removedCount;
}

// ========================================================
// Array::removeIf():
// ========================================================

template<class T, uint ALIGNMENT>
template<class PRED>
uint Array<T, ALIGNMENT>::removeIf(const PRED & pred)
{
	uint removedCount = 0;

	ValueType * array = getData();
	for (uint i = 0; i < size;)
	{
		if (pred(array[i]))
		{
			++removedCount;
			remove(i);
			continue;
		}
		++i;
	}

	return removedCount;
}

// ========================================================
// Array::removeIf():
// ========================================================

template<class T, uint ALIGNMENT>
template<class PRED, class PARAM>
uint Array<T, ALIGNMENT>::removeIf(const PRED & pred, const PARAM & param)
{
	uint removedCount = 0;

	ValueType * array = getData();
	for (uint i = 0; i < size;)
	{
		if (pred(array[i], param))
		{
			++removedCount;
			remove(i);
			continue;
		}
		++i;
	}

	return removedCount;
}

// ========================================================
// Array::removeAllMatching():
// ========================================================

template<class T, uint ALIGNMENT>
uint Array<T, ALIGNMENT>::removeAllMatching(const ValueType & key)
{
	uint removedCount = 0;

	ValueType * array = getData();
	for (uint i = 0; i < size;)
	{
		if (array[i] == key)
		{
			++removedCount;
			remove(i);
			continue;
		}
		++i;
	}

	return removedCount;
}

// ========================================================
// Array::pushIfUniqueSorted():
// ========================================================

template<class T, uint ALIGNMENT>
bool Array<T, ALIGNMENT>::pushIfUniqueSorted(const ValueType & item)
{
	if (MixinBase::findSortedMatch(item) != MixinBase::end())
	{
		return false;
	}

	if (!isEmpty() && item < MixinBase::back())
	{
		DEBUG_CHECK(false && "New item would break the ordering!");
		return false;
	}

	pushBack(item);
	return true;
}

// ========================================================
// Array::pushIfUniqueUnsorted():
// ========================================================

template<class T, uint ALIGNMENT>
bool Array<T, ALIGNMENT>::pushIfUniqueUnsorted(const ValueType & item)
{
	if (MixinBase::findUnsortedMatch(item) != MixinBase::end())
	{
		return false;
	}

	pushBack(item);
	return true;
}

// ========================================================
// Array::transferTo():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::transferTo(Array<T, ALIGNMENT> & dest)
{
	DEBUG_CHECK(dest.isEmpty() && "Destination array contents would be lost!");

	// Dest should be empty, but it might have memory
	// assigned to it, so free first.
	if (dest.isAllocated())
	{
		dest.deallocate();
	}

	dest.arrayPtr = arrayPtr;
	dest.size     = size;
	dest.capacity = capacity;

	// Reset this to defaults.
	init();
}

// ========================================================
// Array::copyTo():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::copyTo(Array<T, ALIGNMENT> & dest) const
{
	if (!isEmpty())
	{
		dest.pushBack(getData(), size);
	}
}

// ========================================================
// Array::getData():
// ========================================================

template<class T, uint ALIGNMENT>
typename Array<T, ALIGNMENT>::ValueType * Array<T, ALIGNMENT>::getData()
{
	DEBUG_CHECK(arrayPtr != nullptr && "Array not allocated!");
	return arrayPtr;
}

// ========================================================
// Array::getData():
// ========================================================

template<class T, uint ALIGNMENT>
const typename Array<T, ALIGNMENT>::ValueType * Array<T, ALIGNMENT>::getData() const
{
	DEBUG_CHECK(arrayPtr != nullptr && "Array not allocated!");
	return arrayPtr;
}

// ========================================================
// Array::isEmpty():
// ========================================================

template<class T, uint ALIGNMENT>
bool Array<T, ALIGNMENT>::isEmpty() const
{
	return size == 0;
}

// ========================================================
// Array::isValid():
// ========================================================

template<class T, uint ALIGNMENT>
bool Array<T, ALIGNMENT>::isValid() const
{
	return isAllocated() && size <= capacity;
}

// ========================================================
// Array::isAllocated():
// ========================================================

template<class T, uint ALIGNMENT>
bool Array<T, ALIGNMENT>::isAllocated() const
{
	return arrayPtr != nullptr && capacity != 0;
}

// ========================================================
// Array::getSize():
// ========================================================

template<class T, uint ALIGNMENT>
uint Array<T, ALIGNMENT>::getSize() const
{
	return size;
}

// ========================================================
// Array::getCapacity():
// ========================================================

template<class T, uint ALIGNMENT>
uint Array<T, ALIGNMENT>::getCapacity() const
{
	return capacity;
}

// ========================================================
// Array::getMemoryBytes():
// ========================================================

template<class T, uint ALIGNMENT>
uint Array<T, ALIGNMENT>::getMemoryBytes() const
{
	return (capacity * sizeof(ValueType)) + MemoryAlignment;
}

// ========================================================
// Array::init():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::init()
{
	arrayPtr = nullptr;
	size     = 0;
	capacity = 0;
}

// ========================================================
// Array::shiftRight():
// ========================================================

template<class T, uint ALIGNMENT>
void Array<T, ALIGNMENT>::shiftRight(ValueType * array, const uint destIndex, const uint sourceIndex, uint count)
{
	ValueType * src = &array[sourceIndex];
	ValueType * dst = &array[destIndex];
	while (count--)
	{
		*dst-- = *src--;
	}
}

// ================================================================================================
// PtrArray<T> inline methods and related helpers:
// ================================================================================================

//
// Global begin/end ranges compatible with "foreach"-style iteration:
//
template<class T>
typename PtrArray<T>::Iterator begin(PtrArray<T> & array)
{ return array.begin(); }

template<class T>
typename PtrArray<T>::Iterator end(PtrArray<T> & array)
{ return array.end(); }

template<class T>
typename PtrArray<T>::Iterator revBegin(PtrArray<T> & array)
{ return array.revBegin(); }

template<class T>
typename PtrArray<T>::Iterator revEnd(PtrArray<T> & array)
{ return array.revEnd(); }

//
// Global begin/end ranges (const) compatible with "foreach"-style iteration:
//
template<class T>
const typename PtrArray<T>::Iterator begin(const PtrArray<T> & array)
{ return array.begin(); }

template<class T>
const typename PtrArray<T>::Iterator end(const PtrArray<T> & array)
{ return array.end(); }

template<class T>
const typename PtrArray<T>::Iterator revBegin(const PtrArray<T> & array)
{ return array.revBegin(); }

template<class T>
const typename PtrArray<T>::Iterator revEnd(const PtrArray<T> & array)
{ return array.revEnd(); }

//
// arrayLength() overload for PtrArray<T>.
//
template<class T>
uint arrayLength(const PtrArray<T> & array) { return array.getSize(); }

// ========================================================
// PtrArray::PtrArray():
// ========================================================

template<class T>
PtrArray<T>::PtrArray()
{
}

// ========================================================
// PtrArray::PtrArray():
// ========================================================

template<class T>
PtrArray<T>::PtrArray(const uint sizeInItems)
	: pointers(sizeInItems, nullptr)
{
}

// ========================================================
// PtrArray::~PtrArray():
// ========================================================

template<class T>
PtrArray<T>::~PtrArray()
{
	clear(); // Deletes every item.
	// `pointers` destructor deallocates the underlaying array.
}

// ========================================================
// PtrArray::allocated():
// ========================================================

template<class T>
void PtrArray<T>::allocate()
{
	pointers.allocate();
}

// ========================================================
// PtrArray::allocate():
// ========================================================

template<class T>
void PtrArray<T>::allocate(const uint capacityHint)
{
	pointers.allocate(capacityHint);
}

// ========================================================
// PtrArray::allocateExact():
// ========================================================

template<class T>
void PtrArray<T>::allocateExact(const uint capacityWanted)
{
	pointers.allocateExact(capacityWanted);
}

// ========================================================
// PtrArray::deallocate():
// ========================================================

template<class T>
void PtrArray<T>::deallocate()
{
	if (!pointers.isAllocated())
	{
		return;
	}

	clear(); // Deletes every item.
	pointers.deallocate();
}

// ========================================================
// PtrArray::clear();
// ========================================================

template<class T>
void PtrArray<T>::clear()
{
	if (pointers.isEmpty())
	{
		return;
	}

	const uint count = pointers.getSize();
	for (uint i = 0; i < count; ++i)
	{
		deleteIndex(i);
	}
	pointers.clear();
}

// ========================================================
// PtrArray::pushBack():
// ========================================================

template<class T>
void PtrArray<T>::resize(const uint newSizeInItems)
{
	// Resize and fill with nulls.
	pointers.resize(newSizeInItems, nullptr);
}

// ========================================================
// PtrArray::pushBack():
// ========================================================

template<class T>
void PtrArray<T>::pushBack(ValueType item)
{
	// Item may be null.
	pointers.pushBack(item);
}

// ========================================================
// PtrArray::popBack():
// ========================================================

template<class T>
void PtrArray<T>::popBack()
{
	deleteIndex(pointers.getSize() - 1);
	pointers.popBack();
}

// ========================================================
// PtrArray::popBack():
// ========================================================

template<class T>
void PtrArray<T>::popBack(const uint count)
{
	for (uint i = 1; i <= count; ++i)
	{
		deleteIndex(pointers.getSize() - i);
	}
	pointers.popBack(count);
}

// ========================================================
// PtrArray::insert():
// ========================================================

template<class T>
void PtrArray<T>::insert(ValueType item, const uint index)
{
	// Item may be null.
	pointers.insert(item, index);
}

// ========================================================
// PtrArray::remove():
// ========================================================

template<class T>
uint PtrArray<T>::remove(const uint index)
{
	deleteIndex(index);
	return pointers.remove(index);
}

// ========================================================
// PtrArray::remove():
// ========================================================

template<class T>
uint PtrArray<T>::remove(const uint startIndex, const uint count)
{
	for (uint i = startIndex; i < count; ++i)
	{
		deleteIndex(i);
	}
	return pointers.remove(startIndex, count);
}

// ========================================================
// PtrArray::remove():
// ========================================================

template<class T>
typename PtrArray<T>::Iterator PtrArray<T>::remove(Iterator position)
{
	// Can't remove end(), that's just a marker to the item after the last.
	// Can't remove from empty array either.
	DEBUG_CHECK(position != MixinBase::end());
	DEBUG_CHECK(!isEmpty() && "Array already empty!");

	// Iterators of an array hold a reference to the owning object.
	// We can assert here to improve safety and catch errors as early as possible:
	DEBUG_CHECK(position.getArray() == this && "Iterator does not belong to this array object!");

	// Return end() if the array was cleared.
	if (remove(position.getCurrentIndex()) == 0)
	{
		return MixinBase::end();
	}

	// Return iterator to the following item, which will be
	// the same as `position` since the iterators are index based.
	return position;
}

// ========================================================
// PtrArray::removeAndSwap():
// ========================================================

template<class T>
uint PtrArray<T>::removeAndSwap(const uint index)
{
	deleteIndex(index);
	return pointers.removeAndSwap(index);
}

// ========================================================
// PtrArray::deleteIndex():
// ========================================================

template<class T>
void PtrArray<T>::deleteIndex(const uint index)
{
	delete pointers[index];
	pointers[index] = nullptr;
}

// ========================================================
// PtrArray::swapItems():
// ========================================================

template<class T>
void PtrArray<T>::swapItems(const uint indexA, const uint indexB)
{
	if (indexA != indexB)
	{
		core::swap(pointers[indexA], pointers[indexB]);
	}
}

// ========================================================
// PtrArray::transferTo():
// ========================================================

template<class T>
void PtrArray<T>::transferTo(PtrArray & dest)
{
	pointers.transferTo(dest.pointers);
}

// ========================================================
// PtrArray::getData():
// ========================================================

template<class T>
typename PtrArray<T>::ValueType * PtrArray<T>::getData()
{
	return pointers.getData();
}

// ========================================================
// PtrArray::getData():
// ========================================================

template<class T>
const typename PtrArray<T>::ValueType * PtrArray<T>::getData() const
{
	return pointers.getData();
}

// ========================================================
// PtrArray::isEmpty():
// ========================================================

template<class T>
bool PtrArray<T>::isEmpty() const
{
	return pointers.isEmpty();
}

// ========================================================
// PtrArray::isValid():
// ========================================================

template<class T>
bool PtrArray<T>::isValid() const
{
	return pointers.isValid();
}

// ========================================================
// PtrArray::isAllocated():
// ========================================================

template<class T>
bool PtrArray<T>::isAllocated() const
{
	return pointers.isAllocated();
}

// ========================================================
// PtrArray::getSize():
// ========================================================

template<class T>
uint PtrArray<T>::getSize() const
{
	return pointers.getSize();
}

// ========================================================
// PtrArray::getCapacity():
// ========================================================

template<class T>
uint PtrArray<T>::getCapacity() const
{
	return pointers.getCapacity();
}

// ========================================================
// PtrArray::getMemoryBytes():
// ========================================================

template<class T>
uint PtrArray<T>::getMemoryBytes() const
{
	return pointers.getMemoryBytes();
}

// ================================================================================================
// SizedArray<T, SIZE, ALIGNMENT> inline methods and related helpers:
// ================================================================================================

//
// Global begin/end ranges compatible with "foreach"-style iteration:
//
template<class T, uint SIZE, uint ALIGNMENT>
typename SizedArray<T, SIZE, ALIGNMENT>::Iterator begin(SizedArray<T, SIZE, ALIGNMENT> & array)
{ return array.begin(); }

template<class T, uint SIZE, uint ALIGNMENT>
typename SizedArray<T, SIZE, ALIGNMENT>::Iterator end(SizedArray<T, SIZE, ALIGNMENT> & array)
{ return array.end(); }

template<class T, uint SIZE, uint ALIGNMENT>
typename SizedArray<T, SIZE, ALIGNMENT>::Iterator revBegin(SizedArray<T, SIZE, ALIGNMENT> & array)
{ return array.revBegin(); }

template<class T, uint SIZE, uint ALIGNMENT>
typename SizedArray<T, SIZE, ALIGNMENT>::Iterator revEnd(SizedArray<T, SIZE, ALIGNMENT> & array)
{ return array.revEnd(); }

//
// Global begin/end ranges (const) compatible with "foreach"-style iteration:
//
template<class T, uint SIZE, uint ALIGNMENT>
const typename SizedArray<T, SIZE, ALIGNMENT>::Iterator begin(const SizedArray<T, SIZE, ALIGNMENT> & array)
{ return array.begin(); }

template<class T, uint SIZE, uint ALIGNMENT>
const typename SizedArray<T, SIZE, ALIGNMENT>::Iterator end(const SizedArray<T, SIZE, ALIGNMENT> & array)
{ return array.end(); }

template<class T, uint SIZE, uint ALIGNMENT>
const typename SizedArray<T, SIZE, ALIGNMENT>::Iterator revBegin(const SizedArray<T, SIZE, ALIGNMENT> & array)
{ return array.revBegin(); }

template<class T, uint SIZE, uint ALIGNMENT>
const typename SizedArray<T, SIZE, ALIGNMENT>::Iterator revEnd(const SizedArray<T, SIZE, ALIGNMENT> & array)
{ return array.revEnd(); }

//
// arrayLength() overload for SizedArray<T, SIZE, ALIGNMENT>.
//
template<class T, uint SIZE, uint ALIGNMENT>
constexpr uint arrayLength(const SizedArray<T, SIZE, ALIGNMENT> & array) { return array.getSize(); }

// ========================================================
// SizedArray::SizedArray():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
SizedArray<T, SIZE, ALIGNMENT>::SizedArray()
{
	// No-op.
}

// ========================================================
// SizedArray::SizedArray():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
SizedArray<T, SIZE, ALIGNMENT>::SizedArray(const ValueType & fillWith)
{
	fillInitialized(array, fillWith, SIZE);
}

// ========================================================
// SizedArray::SizedArray():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
SizedArray<T, SIZE, ALIGNMENT>::SizedArray(const ValueType * items, const uint count)
{
	copyInitialized(array, items, count);
}

// ========================================================
// SizedArray::SizedArray():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
template<uint N>
SizedArray<T, SIZE, ALIGNMENT>::SizedArray(const ValueType (&items)[N])
{
	copyInitialized(array, items, N);
}

// ========================================================
// SizedArray::toArray():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
template<class ARRAY_T, uint ARRAY_ALIGN>
void SizedArray<T, SIZE, ALIGNMENT>::toArray(Array<ARRAY_T, ARRAY_ALIGN> & dest) const
{
	dest.pushBack(array, SIZE);
}

// ========================================================
// SizedArray::zeroFill():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
void SizedArray<T, SIZE, ALIGNMENT>::zeroFill()
{
	MixinBase::zeroFill(SIZE);
}

// ========================================================
// SizedArray::getData():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
typename SizedArray<T, SIZE, ALIGNMENT>::ValueType * SizedArray<T, SIZE, ALIGNMENT>::getData()
{
	return array;
}

// ========================================================
// SizedArray::getData():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
const typename SizedArray<T, SIZE, ALIGNMENT>::ValueType * SizedArray<T, SIZE, ALIGNMENT>::getData() const
{
	return array;
}

// ========================================================
// SizedArray::isEmpty():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
constexpr bool SizedArray<T, SIZE, ALIGNMENT>::isEmpty()
{
	return false;
}

// ========================================================
// SizedArray::isValid():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
constexpr bool SizedArray<T, SIZE, ALIGNMENT>::isValid()
{
	return true;
}

// ========================================================
// SizedArray::isAllocated():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
constexpr bool SizedArray<T, SIZE, ALIGNMENT>::isAllocated()
{
	return true;
}

// ========================================================
// SizedArray::getSize():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
constexpr uint SizedArray<T, SIZE, ALIGNMENT>::getSize()
{
	return SIZE;
}

// ========================================================
// SizedArray::getCapacity():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
constexpr uint SizedArray<T, SIZE, ALIGNMENT>::getCapacity()
{
	return SIZE;
}

// ========================================================
// SizedArray::getMemoryBytes():
// ========================================================

template<class T, uint SIZE, uint ALIGNMENT>
constexpr uint SizedArray<T, SIZE, ALIGNMENT>::getMemoryBytes()
{
	return (SIZE * sizeof(ValueType)) + MemoryAlignment;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_CONTAINERS_ARRAY_TYPES_HPP
