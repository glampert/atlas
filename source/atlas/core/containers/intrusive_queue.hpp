
// ================================================================================================
// -*- C++ -*-
// File: intrusive_queue.hpp
// Author: Guilherme R. Lampert
// Created on: 06/03/15
// Brief: Declares the IntrusiveQueue generic data structure.
// ================================================================================================

#ifndef ATLAS_CORE_CONTAINERS_INTRUSIVE_QUEUE_HPP
#define ATLAS_CORE_CONTAINERS_INTRUSIVE_QUEUE_HPP

// Queue back-end is a doubly-linked list.
#include "atlas/core/containers/intrusive_dlist.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// QueueNode<T> template class alias:
// ========================================================

#if ATLAS_COMPILER_HAS_CPP11
	template<class T>
	using QueueNode = DListNode<T>;
#else // !ATLAS_COMPILER_HAS_CPP11
	template<class T>
	struct QueueNode : public DListNode<T> { };
#endif // ATLAS_COMPILER_HAS_CPP11

// ========================================================
// template class IntrusiveQueue<T>:
// ========================================================

template<class T>
class IntrusiveQueue
	: private NonCopyable
{
public:

	// Nested typedefs:
	typedef T ValueType;

	// Check if empty / query size (constant time):
	bool isEmpty() const;
	uint getSize() const;

	// Access both ends of the queue (FIFO order). Null if empty:
	ValueType * front() const;
	ValueType * back()  const;

	// Push to the back of the queue and pops from the front:
	void push(ValueType * item);
	ValueType * pop();

	// Clears the whole queue (unlinks all nodes; objects NOT destroyed).
	void clear();

	// Transfers all items of this queue into `dest`, making this queue empty.
	// Destination must be an empty queue!
	void transferTo(IntrusiveQueue & dest);

	// Access the underlaying doubly-linked list container:
	const IntrusiveDList<T> & getBackEndContainer() const;
	      IntrusiveDList<T> & getBackEndContainer();

private:

	IntrusiveDList<T> backEnd;
};

// ================================================================================================
// IntrusiveQueue inline implementation:
// ================================================================================================

// ========================================================
// IntrusiveQueue::isEmpty():
// ========================================================

template<class T>
bool IntrusiveQueue<T>::isEmpty() const
{
	return backEnd.isEmpty();
}

// ========================================================
// IntrusiveQueue::getSize():
// ========================================================

template<class T>
uint IntrusiveQueue<T>::getSize() const
{
	return backEnd.getSize();
}

// ========================================================
// IntrusiveQueue::front():
// ========================================================

template<class T>
typename IntrusiveQueue<T>::ValueType * IntrusiveQueue<T>::front() const
{
	return backEnd.front();
}

// ========================================================
// IntrusiveQueue::back():
// ========================================================

template<class T>
typename IntrusiveQueue<T>::ValueType * IntrusiveQueue<T>::back() const
{
	return backEnd.back();
}

// ========================================================
// IntrusiveQueue::push():
// ========================================================

template<class T>
void IntrusiveQueue<T>::push(ValueType * item)
{
	backEnd.pushBack(item);
}

// ========================================================
// IntrusiveQueue::pop():
// ========================================================

template<class T>
typename IntrusiveQueue<T>::ValueType * IntrusiveQueue<T>::pop()
{
	return backEnd.popFront();
}

// ========================================================
// IntrusiveQueue::clear():
// ========================================================

template<class T>
void IntrusiveQueue<T>::clear()
{
	backEnd.clear();
}

// ========================================================
// IntrusiveQueue::transferTo():
// ========================================================

template<class T>
void IntrusiveQueue<T>::transferTo(IntrusiveQueue<T> & dest)
{
	// `dest` has to be an empty queue!
	DEBUG_CHECK(dest.isEmpty() && "Destination queue contents would be lost!");
	backEnd.transferTo(dest.backEnd);
}

// ========================================================
// IntrusiveQueue::getBackEndContainer():
// ========================================================

template<class T>
const IntrusiveDList<T> & IntrusiveQueue<T>::getBackEndContainer() const
{
	return backEnd;
}

// ========================================================
// IntrusiveQueue::getBackEndContainer():
// ========================================================

template<class T>
IntrusiveDList<T> & IntrusiveQueue<T>::getBackEndContainer()
{
	return backEnd;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_CONTAINERS_INTRUSIVE_QUEUE_HPP
