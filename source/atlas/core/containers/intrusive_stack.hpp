
// ================================================================================================
// -*- C++ -*-
// File: intrusive_stack.hpp
// Author: Guilherme R. Lampert
// Created on: 06/03/15
// Brief: Declares the IntrusiveStack generic data structure.
// ================================================================================================

#ifndef ATLAS_CORE_CONTAINERS_INTRUSIVE_STACK_HPP
#define ATLAS_CORE_CONTAINERS_INTRUSIVE_STACK_HPP

// Stack back-end is a singly-linked list.
#include "atlas/core/containers/intrusive_slist.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// StackNode<T> template class alias:
// ========================================================

#if ATLAS_COMPILER_HAS_CPP11
	template<class T>
	using StackNode = SListNode<T>;
#else // !ATLAS_COMPILER_HAS_CPP11
	template<class T>
	struct StackNode : public SListNode<T> { };
#endif // ATLAS_COMPILER_HAS_CPP11

// ========================================================
// template class IntrusiveStack<T>:
// ========================================================

template<class T>
class IntrusiveStack
	: private NonCopyable
{
public:

	// Nested typedefs:
	typedef T ValueType;

	// Check if empty / query size (constant time):
	bool isEmpty() const;
	uint getSize() const;

	// Access top item or null if the stack is empty:
	ValueType * top() const;

	// Push to the top / remove top element (LIFO order):
	void push(ValueType * item);
	ValueType * pop();

	// Clears the whole stack (unlinks all nodes; objects NOT destroyed).
	void clear();

	// Transfers all items of this stack into `dest`, making this stack empty.
	// Destination must be an empty stack!
	void transferTo(IntrusiveStack & dest);

	// Access the underlaying singly-linked list container:
	const IntrusiveSList<T> & getBackEndContainer() const;
	      IntrusiveSList<T> & getBackEndContainer();

private:

	IntrusiveSList<T> backEnd;
};

// ================================================================================================
// IntrusiveStack inline implementation:
// ================================================================================================

// ========================================================
// IntrusiveStack::isEmpty():
// ========================================================

template<class T>
bool IntrusiveStack<T>::isEmpty() const
{
	return backEnd.isEmpty();
}

// ========================================================
// IntrusiveStack::getSize():
// ========================================================

template<class T>
uint IntrusiveStack<T>::getSize() const
{
	return backEnd.getSize();
}

// ========================================================
// IntrusiveStack::top():
// ========================================================

template<class T>
typename IntrusiveStack<T>::ValueType * IntrusiveStack<T>::top() const
{
	return backEnd.front();
}

// ========================================================
// IntrusiveStack::push():
// ========================================================

template<class T>
void IntrusiveStack<T>::push(ValueType * item)
{
	backEnd.pushFront(item);
}

// ========================================================
// IntrusiveStack::pop():
// ========================================================

template<class T>
typename IntrusiveStack<T>::ValueType * IntrusiveStack<T>::pop()
{
	return backEnd.popFront();
}

// ========================================================
// IntrusiveStack::clear():
// ========================================================

template<class T>
void IntrusiveStack<T>::clear()
{
	backEnd.clear();
}

// ========================================================
// IntrusiveStack::transferTo():
// ========================================================

template<class T>
void IntrusiveStack<T>::transferTo(IntrusiveStack<T> & dest)
{
	// `dest` has to be an empty stack!
	DEBUG_CHECK(dest.isEmpty() && "Destination stack contents would be lost!");
	backEnd.transferTo(dest.backEnd);
}

// ========================================================
// IntrusiveStack::getBackEndContainer():
// ========================================================

template<class T>
const IntrusiveSList<T> & IntrusiveStack<T>::getBackEndContainer() const
{
	return backEnd;
}

// ========================================================
// IntrusiveStack::getBackEndContainer():
// ========================================================

template<class T>
IntrusiveSList<T> & IntrusiveStack<T>::getBackEndContainer()
{
	return backEnd;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_CONTAINERS_INTRUSIVE_STACK_HPP
