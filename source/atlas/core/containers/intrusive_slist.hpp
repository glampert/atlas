
// ================================================================================================
// -*- C++ -*-
// File: intrusive_slist.hpp
// Author: Guilherme R. Lampert
// Created on: 06/03/15
// Brief: Declares the IntrusiveSList generic data structure.
// ================================================================================================

#ifndef ATLAS_CORE_CONTAINERS_INTRUSIVE_SLIST_HPP
#define ATLAS_CORE_CONTAINERS_INTRUSIVE_SLIST_HPP

#include "atlas/core/utils/utility.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// template class SListNode<T>:
// ========================================================

template<class T>
class SListNode
{
public:

	// List needs access to internal data of its nodes.
	template<class ITEM_TYPE>
	friend class IntrusiveSList;

	// IntrusiveSList link:
	T *  getSListNext()    const { return slNext;   }
	bool isLinkedToSList() const { return slLinked; }

protected:

	 SListNode() : slNext(nullptr), slLinked(false) { }
	~SListNode() DEFAULTED_METHOD;

private:

	// Next node in a singly-linked list.
	T * slNext;

	// Keep track if linked or not.
	// We can't use `slNext` because it will be null for the last item.
	bool slLinked;
};

// ========================================================
// template class IntrusiveSList<T>:
// ========================================================

// Intrusive singly-liked list template class.
//
// An intrusive data structure is a structure that stores house
// keeping information necessary to its functioning directly inside
// the object being stored.
// This type of storage scheme forces types that wish to be inserted
// into this list to inherit from `SListNode<T>`.
//
// Only pointers to objects can actually be stored inside the structure.
// Thus the objective of the intrusive list is to eliminate extra memory allocations
// for new insertions. Instead of allocating a node structure per object, the object
// itself is the node. Consequently, memory allocations are all external to the structure.
//
// This list does not manage memory or lifetime of the objects inserted.
// Removing an item from the list WILL NOT destroy the object, just unlink it.
template<class T>
class IntrusiveSList
	: private NonCopyable
{
public:

	// Nested typedefs:
	typedef T ValueType;

	// Forward iterator for a singly-linked list.
	// Allows unidirectional list traversal (increment only).
	//
	// Can have `const` applied to it;
	// Can be used with "foreach"-style iteration.
	class Iterator final
	{
	public:

		Iterator();
		Iterator(const IntrusiveSList * list, ValueType * nd);

		const ValueType & operator*  () const;
		      ValueType & operator*  ();
		const ValueType * operator-> () const;
		      ValueType * operator-> ();

		const Iterator & operator++ ()    const; // pre-increment
		      Iterator   operator++ (int) const; // post-increment

		bool operator == (const Iterator & other) const;
		bool operator != (const Iterator & other) const;

		// Current list node the iterator is pointing to.
		ValueType * getCurrentNode() const;

		// Get a reference to the linked-list that owns this iterator.
		const IntrusiveSList * getList() const;

	private:

		void validateDereference() const;

		// List this iterator belongs to. Used for error checking.
		const IntrusiveSList * myList;

		// Actual node pointing to a list item. Null for list end.
		mutable ValueType * node;
	};

	// Iterators to the beginning of the table and one-past-the-end:
	Iterator begin();
	Iterator end();
	const Iterator begin() const;
	const Iterator end()   const;

public:

	// Construct an empty list.
	IntrusiveSList();

	// Destructor clears the list and unlinks all items.
	~IntrusiveSList();

	// Append at the head or tail of the list.
	// `item` must not be null! Both are constant time.
	void pushFront(ValueType * item);
	void pushBack(ValueType * item);

	// Removes one item from head or tail of the list, without destroying the object.
	// Returns the removed item or null if the list is already empty.
	// NOTE: In the current implementation removing from the end (`popBack()`) has O(N)
	// time complexity. Unlike `popBack()`, `popFront()` has constant execution time.
	ValueType * popFront();
	ValueType * popBack();

	// Returns the first and last item in the list container,
	// or null if the list is empty.
	ValueType * front() const;
	ValueType * back()  const;

	// Test if the list is empty (getSize() == 0). Constant time.
	bool isEmpty() const;

	// Get size in items. Constant time.
	uint getSize() const;

	// Unlinks all objects from the list, without destroying them.
	void clear();

	// Unlinks all objects AND deletes each pointer. This method may be
	// called by the user, but it is never called by IntrusiveSList.
	void clearAndDelete();

	// Performs linear search in the list and returns an iterator to the first
	// occurrence of `wanted`, or `list.end()` if `wanted` in not present.
	// Search is performed by applying operator == in the items themselves, not the stored pointers.
	Iterator find(const ValueType & wanted) const;

	// Performs linear search in the list and returns an iterator to the first
	// item matching `pred`, or `list.end()` if there is no match.
	template<class PRED> Iterator find(const PRED & pred) const;
	template<class PRED, class PARAM> Iterator find(const PRED & pred, const PARAM & param) const;

	// Insert item into list. The container is extended by inserting
	// a new node before the item at the specified position. `item` must not be null!
	Iterator insert(Iterator position, ValueType * item);

	// Removes one item from the list.
	// Removing doesn't destroy the item, only unlinks it from the data structure.
	// The user can save the item before removing it and then destroy it after removing, if required.
	// Returns an iterator pointing to the new location of the node that followed the item unlinked,
	// which is the list end if the operation removed the last item in it.
	Iterator remove(Iterator position);

	// Removes from the list all items for which predicate `pred` returns true.
	// Returns the number of items removed.
	template<class PRED> uint removeIf(const PRED & pred);
	template<class PRED, class PARAM> uint removeIf(const PRED & pred, const PARAM & param);

	// Transfers all items from the other list to the end of this list, clearing the other.
	void splice(IntrusiveSList & other);

	// Transfers all items of this list into `dest`, making this list empty.
	// Destination must be an empty list!
	void transferTo(IntrusiveSList & dest);

private:

	// Make a valid iterator from a node ALREADY linked to this list.
	Iterator makeIterator(ValueType * node) const;

	ValueType * head; // Head of list, null if list empty.
	ValueType * tail; // Last element before the end of the list. Equals to head if itemCount == 1, null if list empty.
	uint itemCount;   // Number of items in the list. 0 is empty.
};

// ================================================================================================
// IntrusiveSList inline methods and related helpers:
// ================================================================================================

// ========================================================
// begin():
// ========================================================

template<class T>
typename IntrusiveSList<T>::Iterator
begin(IntrusiveSList<T> & sl)
{
	return sl.begin();
}

// ========================================================
// end():
// ========================================================

template<class T>
typename IntrusiveSList<T>::Iterator
end(IntrusiveSList<T> & sl)
{
	return sl.end();
}

// ========================================================
// begin() [const]:
// ========================================================

template<class T>
const typename IntrusiveSList<T>::Iterator
begin(const IntrusiveSList<T> & sl)
{
	return sl.begin();
}

// ========================================================
// end() [const]:
// ========================================================

template<class T>
const typename IntrusiveSList<T>::Iterator
end(const IntrusiveSList<T> & sl)
{
	return sl.end();
}

// ========================================================
// IntrusiveSList::IntrusiveSList():
// ========================================================

template<class T>
IntrusiveSList<T>::IntrusiveSList()
	: head(nullptr)
	, tail(nullptr)
	, itemCount(0)
{
}

// ========================================================
// IntrusiveSList::~IntrusiveSList():
// ========================================================

template<class T>
IntrusiveSList<T>::~IntrusiveSList()
{
	clear();
}

// ========================================================
// IntrusiveSList::pushFront():
// ========================================================

template<class T>
void IntrusiveSList<T>::pushFront(ValueType * item)
{
	DEBUG_CHECK(item != nullptr);
	DEBUG_CHECK(!item->isLinkedToSList()); // An object can only be a member of one list at a time!

	if (!isEmpty())
	{
		item->slNext = head;
		head = item;
	}
	else // Empty list, first insertion:
	{
		item->slNext = head;
		head = item;
		tail = head;
	}

	item->slLinked = true;
	++itemCount;
}

// ========================================================
// IntrusiveSList::pushBack():
// ========================================================

template<class T>
void IntrusiveSList<T>::pushBack(ValueType * item)
{
	DEBUG_CHECK(item != nullptr);
	DEBUG_CHECK(!item->isLinkedToSList()); // An object can only be a member of one list at a time!

	if (!isEmpty())
	{
		tail->slNext = item;
		tail = tail->slNext;
		item->slNext = nullptr; // Make sure item's next is null.
	}
	else // Empty list, first insertion:
	{
		head = item;
		tail = head;
		head->slNext = nullptr;
	}

	item->slLinked = true;
	++itemCount;
}

// ========================================================
// IntrusiveSList::popFront():
// ========================================================

template<class T>
typename IntrusiveSList<T>::ValueType * IntrusiveSList<T>::popFront()
{
	if (isEmpty())
	{
		return nullptr;
	}

	ValueType * removedItem = head;

	// 1 item left, clear the list:
	if (head == tail)
	{
		DEBUG_CHECK(itemCount == 1);
		head = tail = nullptr;
	}
	else
	{
		// Shift head by 1:
		head = head->slNext;
	}

	--itemCount;

	removedItem->slNext   = nullptr;
	removedItem->slLinked = false;

	return removedItem;
}

// ========================================================
// IntrusiveSList::popBack():
// ========================================================

template<class T>
typename IntrusiveSList<T>::ValueType * IntrusiveSList<T>::popBack()
{
	if (isEmpty())
	{
		return nullptr;
	}

	ValueType * removedItem = tail;

	// 1 item left, clear the list:
	if (head == tail)
	{
		DEBUG_CHECK(itemCount == 1);
		head = tail = nullptr;
	}
	else
	{
		ValueType * it = head;
		ValueType * tailPrev = nullptr; // Element before tail, which will become the new tail.

		// Find the node before the tail:
		while (it != tail)
		{
			tailPrev = it;
			it = it->slNext;
		}

		// Unlink tail node:
		DEBUG_CHECK(it != nullptr);
		tailPrev->slNext = nullptr;
		tail = tailPrev;
	}

	--itemCount;

	removedItem->slNext   = nullptr;
	removedItem->slLinked = false;

	return removedItem;
}

// ========================================================
// IntrusiveSList::front():
// ========================================================

template<class T>
typename IntrusiveSList<T>::ValueType * IntrusiveSList<T>::front() const
{
	return isEmpty() ? nullptr : head;
}

// ========================================================
// IntrusiveSList::back():
// ========================================================

template<class T>
typename IntrusiveSList<T>::ValueType * IntrusiveSList<T>::back() const
{
	return isEmpty() ? nullptr : tail;
}

// ========================================================
// IntrusiveSList::isEmpty():
// ========================================================

template<class T>
bool IntrusiveSList<T>::isEmpty() const
{
	return itemCount == 0;
}

// ========================================================
// IntrusiveSList::getSize():
// ========================================================

template<class T>
uint IntrusiveSList<T>::getSize() const
{
	return itemCount;
}

// ========================================================
// IntrusiveSList::clear():
// ========================================================

template<class T>
void IntrusiveSList<T>::clear()
{
	for (ValueType * item = head; item != nullptr;)
	{
		ValueType * tmp = item;
		item = item->slNext;

		tmp->slNext   = nullptr;
		tmp->slLinked = false;
	}

	head = tail = nullptr;
	itemCount = 0;
}

// ========================================================
// IntrusiveSList::clearAndDelete():
// ========================================================

template<class T>
void IntrusiveSList<T>::clearAndDelete()
{
	for (ValueType * item = head; item != nullptr;)
	{
		ValueType * tmp = item;
		item = item->slNext;

		delete tmp;
	}

	head = tail = nullptr;
	itemCount = 0;
}

// ========================================================
// IntrusiveSList::find():
// ========================================================

template<class T>
typename IntrusiveSList<T>::Iterator IntrusiveSList<T>::find(const ValueType & wanted) const
{
	for (const Iterator it = begin(); it != end(); ++it)
	{
		if ((*it) == wanted)
		{
			return it;
		}
	}
	return end(); // Item not found
}

// ========================================================
// IntrusiveSList::find():
// ========================================================

template<class T>
template<class PRED>
typename IntrusiveSList<T>::Iterator IntrusiveSList<T>::find(const PRED & pred) const
{
	for (const Iterator it = begin(); it != end(); ++it)
	{
		if (pred(*it))
		{
			return it;
		}
	}
	return end(); // Item not found
}

// ========================================================
// IntrusiveSList::find():
// ========================================================

template<class T>
template<class PRED, class PARAM>
typename IntrusiveSList<T>::Iterator IntrusiveSList<T>::find(const PRED & pred, const PARAM & param) const
{
	for (const Iterator it = begin(); it != end(); ++it)
	{
		if (pred(*it, param))
		{
			return it;
		}
	}
	return end(); // Item not found
}

// ========================================================
// IntrusiveSList::insert():
// ========================================================

template<class T>
typename IntrusiveSList<T>::Iterator IntrusiveSList<T>::insert(Iterator position, ValueType * item)
{
	DEBUG_CHECK(item != nullptr);
	DEBUG_CHECK(!item->isLinkedToSList()); // An object can only be a member of one list at a time!

	// Iterators of a s-list hold a reference to the owning list,
	// we can assert here to improve safety and catch errors as early as possible:
	DEBUG_CHECK(position.getList() == this && "Iterator does not belong to this linked-list!");

	// Optimize for head/tail insertion:
	if (position == begin())
	{
		pushFront(item);
		return makeIterator(head);
	}

	if (position == end())
	{
		pushBack(item);
		return makeIterator(tail);
	}

	// Insert somewhere in between:
	ValueType * it = head;
	ValueType * posNode = position.getCurrentNode();
	ValueType * posPrev = nullptr; // Element before `position`.

	while (it != posNode)
	{
		posPrev = it;
		it = it->slNext;
	}

	DEBUG_CHECK(it != nullptr);
	posPrev->slNext = item;
	item->slNext = posNode;

	++itemCount;

	item->slLinked = true;
	return makeIterator(item);
}

// ========================================================
// IntrusiveSList::remove():
// ========================================================

template<class T>
typename IntrusiveSList<T>::Iterator IntrusiveSList<T>::remove(Iterator position)
{
	// Can't remove end(), that's just a marker to the item after the last.
	DEBUG_CHECK(position != end());

	// Can't remove from empty list either.
	DEBUG_CHECK(!isEmpty() && "List already empty!");

	// Iterators of a s-list hold a reference to the owning list,
	// we can assert here to improve safety and catch errors as early as possible:
	DEBUG_CHECK(position.getList() == this && "Iterator does not belong to this linked-list!");

	// Fetch node pointer:
	ValueType * posNode = position.getCurrentNode();

	// Last element, clear the list:
	if (head == tail)
	{
		DEBUG_CHECK(itemCount == 1 && posNode == head);
		head = tail = nullptr;
		--itemCount;

		posNode->slNext   = nullptr;
		posNode->slLinked = false;
		return end();
	}

	// Remove head:
	if (posNode == head)
	{
		head = head->slNext;
		--itemCount;

		posNode->slNext   = nullptr;
		posNode->slLinked = false;
		return makeIterator(head);
	}

	ValueType * it = head;
	ValueType * posPrev = nullptr;

	// Find the element before `position`:
	while (it != posNode)
	{
		posPrev = it;
		it = it->slNext;
	}
	DEBUG_CHECK(it != nullptr);

	// Unlink `position` by pointing its previous element to what was after it:
	posPrev->slNext = posNode->slNext;

	// Make sure tail pointer is up-to-date:
	if (posNode == tail)
	{
		tail = posPrev;
	}

	--itemCount;

	Iterator result(makeIterator(posNode->slNext));
	posNode->slNext   = nullptr;
	posNode->slLinked = false;

	// Return the element that followed `position`:
	return result;
}

// ========================================================
// IntrusiveSList::removeIf():
// ========================================================

template<class T>
template<class PRED>
uint IntrusiveSList<T>::removeIf(const PRED & pred)
{
	uint removedCount = 0;
	Iterator it = begin();
	while (it != end())
	{
		if (pred(*it))
		{
			it = remove(it);
			++removedCount;
			continue;
		}
		++it;
	}
	return removedCount;
}

// ========================================================
// IntrusiveSList::removeIf():
// ========================================================

template<class T>
template<class PRED, class PARAM>
uint IntrusiveSList<T>::removeIf(const PRED & pred, const PARAM & param)
{
	uint removedCount = 0;
	Iterator it = begin();
	while (it != end())
	{
		if (pred(*it, param))
		{
			it = remove(it);
			++removedCount;
			continue;
		}
		++it;
	}
	return removedCount;
}

// ========================================================
// IntrusiveSList::splice():
// ========================================================

template<class T>
void IntrusiveSList<T>::splice(IntrusiveSList<T> & other)
{
	// Do nothing if the other is empty.
	if (other.isEmpty())
	{
		return;
	}

	// Simpler case when this list is empty:
	if (isEmpty())
	{
		other.transferTo(*this);
		return;
	}

	// Attack the other list to this list's tail:
	tail->slNext = other.head;
	tail = other.tail;

	itemCount += other.itemCount;

	// Clear the other list's state:
	other.head = nullptr;
	other.tail = nullptr;
	other.itemCount = 0;
}

// ========================================================
// IntrusiveSList::transferTo():
// ========================================================

template<class T>
void IntrusiveSList<T>::transferTo(IntrusiveSList<T> & dest)
{
	// `dest` has to be an empty list!
	DEBUG_CHECK(dest.isEmpty() && "Destination list contents would be lost!");

	dest.head = head;
	dest.tail = tail;
	dest.itemCount = itemCount;

	head = nullptr;
	tail = nullptr;
	itemCount = 0;
}

// ========================================================
// IntrusiveSList::makeIterator():
// ========================================================

template<class T>
typename IntrusiveSList<T>::Iterator IntrusiveSList<T>::makeIterator(ValueType * node) const
{
	if (node != nullptr)
	{
		// We can at least check that this is not a loose node.
		// A future improvement would be also asserting that it is linked to *this* list...
		DEBUG_CHECK(node->isLinkedToSList());
	}
	return Iterator(this, node);
}

// ========================================================
// IntrusiveSList::begin():
// ========================================================

template<class T>
typename IntrusiveSList<T>::Iterator IntrusiveSList<T>::begin()
{
	return makeIterator(head);
}

// ========================================================
// IntrusiveSList::end():
// ========================================================

template<class T>
typename IntrusiveSList<T>::Iterator IntrusiveSList<T>::end()
{
	return makeIterator(nullptr); // tail == nullptr when the list it empty, tail->next is always null.
}

// ========================================================
// IntrusiveSList::begin() [const]:
// ========================================================

template<class T>
const typename IntrusiveSList<T>::Iterator IntrusiveSList<T>::begin() const
{
	return makeIterator(head);
}

// ========================================================
// IntrusiveSList::end() [const]:
// ========================================================

template<class T>
const typename IntrusiveSList<T>::Iterator IntrusiveSList<T>::end() const
{
	return makeIterator(nullptr); // tail == nullptr when the list it empty, tail->next is always null.
}

// ================================================================================================
// IntrusiveSList::Iterator class definition:
// ================================================================================================

// ========================================================
// IntrusiveSList::Iterator::Iterator():
// ========================================================

template<class T>
IntrusiveSList<T>::Iterator::Iterator()
	: myList(nullptr)
	, node(nullptr)
{
	// Invalid iterator.
}

// ========================================================
// IntrusiveSList::Iterator::Iterator():
// ========================================================

template<class T>
IntrusiveSList<T>::Iterator::Iterator(const IntrusiveSList<T> * list, ValueType * nd)
	: myList(list)
	, node(nd)
{
	DEBUG_CHECK(myList != nullptr);
}

// ========================================================
// IntrusiveSList::Iterator::operator* :
// ========================================================

template<class T>
typename IntrusiveSList<T>::ValueType & IntrusiveSList<T>::Iterator::operator* ()
{
	validateDereference();
	return *node;
}

// ========================================================
// IntrusiveSList::Iterator::operator* :
// ========================================================

template<class T>
const typename IntrusiveSList<T>::ValueType & IntrusiveSList<T>::Iterator::operator* () const
{
	validateDereference();
	return *node;
}

// ========================================================
// IntrusiveSList::Iterator::operator-> :
// ========================================================

template<class T>
typename IntrusiveSList<T>::ValueType * IntrusiveSList<T>::Iterator::operator-> ()
{
	validateDereference();
	return node;
}

// ========================================================
// IntrusiveSList::Iterator::operator-> :
// ========================================================

template<class T>
const typename IntrusiveSList<T>::ValueType * IntrusiveSList<T>::Iterator::operator-> () const
{
	validateDereference();
	return node;
}

// ========================================================
// IntrusiveSList::Iterator::operator++ (pre-increment):
// ========================================================

template<class T>
const typename IntrusiveSList<T>::Iterator & IntrusiveSList<T>::Iterator::operator++ () const
{
	DEBUG_CHECK(node != nullptr && "Out-of-bounds linked-list iterator increment!");
	node = node->getSListNext();
	return *this;
}

// ========================================================
// IntrusiveSList::Iterator::operator++ (post-increment):
// ========================================================

template<class T>
typename IntrusiveSList<T>::Iterator IntrusiveSList<T>::Iterator::operator++ (int) const
{
	DEBUG_CHECK(node != nullptr && "Out-of-bounds linked-list iterator increment!");
	Iterator tmp(*this);
	++(*this);
	return tmp; // Return old, pre-increment value.
}

// ========================================================
// IntrusiveSList::Iterator::operator == :
// ========================================================

template<class T>
bool IntrusiveSList<T>::Iterator::operator == (const Iterator & other) const
{
	DEBUG_CHECK(myList == other.myList && "Iterators belong to different linked-lists!");
	return node == other.node;
}

// ========================================================
// IntrusiveSList::Iterator::operator != :
// ========================================================

template<class T>
bool IntrusiveSList<T>::Iterator::operator != (const Iterator & other) const
{
	DEBUG_CHECK(myList == other.myList && "Iterators belong to different linked-lists!");
	return node != other.node;
}

// ========================================================
// IntrusiveSList::Iterator::getCurrentNode():
// ========================================================

template<class T>
typename IntrusiveSList<T>::ValueType * IntrusiveSList<T>::Iterator::getCurrentNode() const
{
	return node;
}

// ========================================================
// IntrusiveSList::Iterator::getList():
// ========================================================

template<class T>
const IntrusiveSList<T> * IntrusiveSList<T>::Iterator::getList() const
{
	return myList;
}

// ========================================================
// IntrusiveSList::Iterator::validateDereference():
// ========================================================

template<class T>
void IntrusiveSList<T>::Iterator::validateDereference() const
{
	DEBUG_CHECK(myList != nullptr && "Invalid linked-list iterator!");
	DEBUG_CHECK(node   != nullptr && "Invalid linked-list iterator dereference!");
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_CONTAINERS_INTRUSIVE_SLIST_HPP
