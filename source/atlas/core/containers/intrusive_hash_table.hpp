
// ================================================================================================
// -*- C++ -*-
// File: intrusive_hash_table.hpp
// Author: Guilherme R. Lampert
// Created on: 04/03/15
// Brief: Declares the IntrusiveHashTable generic data structure.
// ================================================================================================

#ifndef ATLAS_CORE_CONTAINERS_INTRUSIVE_HASH_TABLE_HPP
#define ATLAS_CORE_CONTAINERS_INTRUSIVE_HASH_TABLE_HPP

#include "atlas/core/utils/utility.hpp"
#include "atlas/core/utils/hash_funcs.hpp"
#include "atlas/core/maths/math_utils.hpp"
#include "atlas/core/memory/mem_utils.hpp"
#include "atlas/core/strings/dynamic_string.hpp"
#include "atlas/core/strings/hashed_str.hpp"

#if ATLAS_COMPILER_HAS_CPP11
	#include <type_traits>
#endif // ATLAS_COMPILER_HAS_CPP11

namespace atlas
{
namespace core
{

// ========================================================
// template struct Hash<T>:
// ========================================================

// Default hash functor. Assumes the input type is POD
// and forwards the hashing to a default bitwise hash function.
template<class T>
struct Hash
{
	uint32 operator() (const T & value) const
	{
		#if ATLAS_COMPILER_HAS_CPP11
		COMPILE_TIME_CHECK(std::is_pod<T>::value, "Type must be Plain Old Data (POD)!");
		#endif // ATLAS_COMPILER_HAS_CPP11

		return hashOat(&value, sizeof(T));
	}
};

// ========================================================
// template struct Hash<String>:
// ========================================================

// Specialization of the default hash functor for Atlas Strings.
// Note: Hash is case-sensitive!
template<>
struct Hash<String>
{
	uint32 operator() (const String & str) const
	{
		return str.hash();
	}
};

// ========================================================
// template struct Hash<HashedStr>:
// ========================================================

// Specialization of the default hash functor for `HashedStr`.
// (constant time since HashedStr already has the string's hash precomputed).
template<class S>
struct Hash< HashedStr<S> >
{
	uint32 operator() (const HashedStr<S> & str) const
	{
		return str.getHash();
	}
};

// ========================================================
// template class HashTableNode<T>:
// ========================================================

// Types that will be inserted into the IntrusiveHashTable
// must inherit from this template class.
template<class T>
class HashTableNode
{
public:

	// Hash-table needs access to internal data of its nodes.
	template<class K, class V, class HASH>
	friend class IntrusiveHashTable;

	// IntrusiveHashTable interface:
	uint32 getHashTableKeyHash() const { return htKeyHash;      }
	T *    getHashTableNext()    const { return htNext;         }
	bool   isLinkedToHashTable() const { return htKeyHash != 0; }

protected:

	 HashTableNode() : htNext(nullptr), htKeyHash(0) { }
	~HashTableNode() DEFAULTED_METHOD;

private:

	// Next node in the IntrusiveHashTable bucket chain.
	// Null if this is the last item or if not linked.
	T * htNext;

	// Hash of the node's key.
	// Zero only if not linked to a table.
	uint32 htKeyHash;
};

// ========================================================
// template class IntrusiveHashTable<K, V, HASH>:
// ========================================================

// A fast chained hash-table with support for generic keys.
// This hash-table is an intrusive data structure, so types wishing
// to be inserted into the structure must inherit from `HashTableNode<T>`.
// This will add the data needed for the hash-table management directly
// inside the type itself, usually sparing a memory allocation for an
// auxiliary structure.
//
// Hash-tables can have much faster search times than any other data structure,
// as long as the load is balanced. If the hash-table's capacity is depleted,
// its search time will degrade to something close to a linked list.
// That is why extra care should be taken to set the table size (bucket count)
// close to the largest occupancy expected.
//
// This hash-table does not manage memory or lifetime of the objects inserted.
// Removing an item from the table WILL NOT destroy the object, just unlink it.
template
<
	class K, // The key type
	class V, // The mapped type (value)
	class HASH = Hash<typename TypeTraits<K>::UnqualifiedType> // Hashes a key instance
>
class IntrusiveHashTable
	: private NonCopyable
{
public:

	// Nested typedefs:
	typedef V ValueType;
	typedef HASH KeyHasher;
	typedef typename TypeTraits<K>::UnqualifiedType KeyType;

	// Construct empty (no allocation). Allocates on first insertion.
	explicit IntrusiveHashTable(bool allowDuplicateKeys);

	// Construct and allocate storage with num buckets hint.
	IntrusiveHashTable(bool allowDuplicateKeys, uint sizeHint);

	// Destructor clears the table and unlinks all items.
	~IntrusiveHashTable();

	// Explicitly allocate storage. No-op if already allocated.
	void allocate();
	void allocate(uint sizeHint);

	// Clears and frees all memory.
	void deallocate();

	// Sets to empty without deallocating.
	void clear();

	// Sets to empty without deallocating.
	// Calls the provided function for each item after it is unlinked.
	template<class FUNC>
	void clearWithFunction(FUNC func);

	// Test if table buckets are already allocated.
	bool isAllocated() const;

	// Test if the table's internal state is valid.
	bool isValid() const;

	// Test if empty.
	bool isEmpty() const;

	// Get size in items.
	uint getSize() const;

	// Number of buckets allocated.
	uint getBucketCount() const;

	// Estimate memory usage of internal control structures.
	uint getMemoryBytes() const;

	// Set/get the "allow duplicate keys" flag.
	void setAllowDuplicateKeys(bool allow);
	bool isAllowingDuplicateKeys() const;

	// Access item by key. Returns null if key is not present.
	ValueType * find(const KeyType & key) const;

	// Find all entries matching `key` in the table. This is useful when the table is allowing duplicated keys.
	// `items[]` is an array of pointers to items. `maxItems` is the maximum number of items to return
	// (size of `items[]` array). Returns the number of items found and added to the `items[]` output array.
	uint findAllMatching(const KeyType & key, ValueType ** items, uint maxItems) const;

	// Count number of items with the given key.
	// Will never be greater than one if duplicate keys are not allowed.
	uint countAllMatching(const KeyType & key) const;

	// Operator[] to access items by key (same as `find()`).
	ValueType * operator [] (const KeyType & key) const;

	// Insertion. Fails in case of duplicate keys only when duplicate keys are being disallowed.
	bool insert(const KeyType & key, ValueType * value);

	// Remove (unlink) single key/value pair. Returns a reference to the removed item. Null if no key found.
	ValueType * remove(const KeyType & key);

	// Remove (unlink) all items matching the key. Returns number of items removed.
	uint removeAllMatching(const KeyType & key);

	// Transfers all items of this table into `dest`, making this table empty.
	// Will generate a runtime assertion if `dest` is not an empty hash-table.
	void transferTo(IntrusiveHashTable & dest);

public:

	// Hash-table iterator type.
	// Can have `const` applied to it;
	// Can be used with "foreach"-style iteration.
	//
	// Adding or removing items to/from the hash-table
	// will invalidate existing iterators!
	//
	// Only FORWARD iteration is possible, due to the
	// hash-table chains being singly-linked lists.
	class Iterator final
	{
	public:

		Iterator();
		Iterator(const IntrusiveHashTable * ht, ValueType ** bkts, uint count);

		const ValueType & operator*  () const;
		      ValueType & operator*  ();
		const ValueType * operator-> () const;
		      ValueType * operator-> ();

		const Iterator & operator++ ()    const; // pre-increment
		Iterator         operator++ (int) const; // post-increment

		bool operator == (const Iterator & other) const;
		bool operator != (const Iterator & other) const;

		// Get a reference to the hash-table that owns this iterator.
		const IntrusiveHashTable * getHashTable() const;

	private:

		void validateDereference() const;
		void validateMovement()    const;

		// Hash-table that owns this iterator.
		// Used for error checking only.
		const IntrusiveHashTable * myTable;

		ValueType        ** buckets;       // The hash table we are iterating.
		mutable ValueType * currentLink;   // Current link in the current bucket's chain.
		mutable uint        currentBucket; // Current bucket index in the table above.
		uint                bucketCount;   // Total allocated size of the table.
	};

	// Iterate hash-table *values*.
	// Iterators to the beginning of the table and one-past-the-end:
	Iterator begin();
	Iterator end();
	const Iterator begin() const;
	const Iterator end()   const;

private:

	uint hashOf(const KeyType & key) const;
	uint bucketOf(uint keyHash) const;
	void freeDynamicMemory();

	// A prime number close to 2048.
	static constexpr uint DefaultCapacity = 2053;

	// Array of pointers to items (the buckets).
	ValueType ** table;

	// Total size of `table` and buckets used so far.
	uint bucketCount;
	uint usedBuckets;

	// If allowing duplicate keys or not.
	bool allowDupKeys;
};

// ================================================================================================
// IntrusiveHashTable inline methods and related helpers:
// ================================================================================================

// ========================================================
// begin():
// ========================================================

template<class K, class V, class HASH>
typename IntrusiveHashTable<K, V, HASH>::Iterator
begin(IntrusiveHashTable<K, V, HASH> & ht)
{
	return ht.begin();
}

// ========================================================
// end():
// ========================================================

template<class K, class V, class HASH>
typename IntrusiveHashTable<K, V, HASH>::Iterator
end(IntrusiveHashTable<K, V, HASH> & ht)
{
	return ht.end();
}

// ========================================================
// begin() [const]:
// ========================================================

template<class K, class V, class HASH>
const typename IntrusiveHashTable<K, V, HASH>::Iterator
begin(const IntrusiveHashTable<K, V, HASH> & ht)
{
	return ht.begin();
}

// ========================================================
// end() [const]:
// ========================================================

template<class K, class V, class HASH>
const typename IntrusiveHashTable<K, V, HASH>::Iterator
end(const IntrusiveHashTable<K, V, HASH> & ht)
{
	return ht.end();
}

// ========================================================
// IntrusiveHashTable::IntrusiveHashTable():
// ========================================================

template<class K, class V, class HASH>
IntrusiveHashTable<K, V, HASH>::IntrusiveHashTable(const bool allowDuplicateKeys)
	: table(nullptr)
	, bucketCount(0)
	, usedBuckets(0)
	, allowDupKeys(allowDuplicateKeys)
{
	// Empty table. Allocates the buckets on first insertion.
}

// ========================================================
// IntrusiveHashTable::IntrusiveHashTable():
// ========================================================

template<class K, class V, class HASH>
IntrusiveHashTable<K, V, HASH>::IntrusiveHashTable(const bool allowDuplicateKeys, const uint sizeHint)
	: table(nullptr)
	, bucketCount(0)
	, usedBuckets(0)
	, allowDupKeys(allowDuplicateKeys)
{
	allocate(sizeHint);
}

// ========================================================
// IntrusiveHashTable::~IntrusiveHashTable():
// ========================================================

template<class K, class V, class HASH>
IntrusiveHashTable<K, V, HASH>::~IntrusiveHashTable()
{
	deallocate();
}

// ========================================================
// IntrusiveHashTable::allocate():
// ========================================================

template<class K, class V, class HASH>
void IntrusiveHashTable<K, V, HASH>::allocate()
{
	allocate(DefaultCapacity);
}

// ========================================================
// IntrusiveHashTable::allocate():
// ========================================================

template<class K, class V, class HASH>
void IntrusiveHashTable<K, V, HASH>::allocate(uint sizeHint)
{
	if (isAllocated())
	{
		return;
	}

	while (!math::isPrime(sizeHint))
	{
		++sizeHint;
	}

	table = new ValueType *[sizeHint];
	clearPodArray(table, sizeHint);

	bucketCount = sizeHint;
}

// ========================================================
// IntrusiveHashTable::deallocate():
// ========================================================

template<class K, class V, class HASH>
void IntrusiveHashTable<K, V, HASH>::deallocate()
{
	if (!isAllocated())
	{
		return;
	}

	// Unlink all items first.
	clear();
	freeDynamicMemory();
	bucketCount = 0;
}

// ========================================================
// IntrusiveHashTable::clear():
// ========================================================

template<class K, class V, class HASH>
void IntrusiveHashTable<K, V, HASH>::clear()
{
	if (isEmpty())
	{
		return;
	}

	// Check each bucket. Non-null ones are occupied:
	for (uint bucket = 0; bucket < bucketCount; ++bucket)
	{
		// Reset each item in this bucket's chain:
		for (ValueType * item = table[bucket]; item != nullptr;)
		{
			ValueType * nextItem = item->htNext;
			item->htNext    = nullptr;
			item->htKeyHash = 0;
			item = nextItem;
		}
		table[bucket] = nullptr;
	}

	usedBuckets = 0;
}

// ========================================================
// IntrusiveHashTable::clearWithFunction():
// ========================================================

template<class K, class V, class HASH>
template<class FUNC>
void IntrusiveHashTable<K, V, HASH>::clearWithFunction(FUNC func)
{
	if (isEmpty())
	{
		return;
	}

	for (uint bucket = 0; bucket < bucketCount; ++bucket)
	{
		for (ValueType * item = table[bucket]; item != nullptr;)
		{
			ValueType * nextItem = item->htNext;
			func(item); // Delete/unlink. Up to the user to decide.
			item = nextItem;
		}
		table[bucket] = nullptr;
	}

	usedBuckets = 0;
}

// ========================================================
// IntrusiveHashTable::isAllocated():
// ========================================================

template<class K, class V, class HASH>
bool IntrusiveHashTable<K, V, HASH>::isAllocated() const
{
	return table != nullptr && bucketCount != 0;
}

// ========================================================
// IntrusiveHashTable::isValid():
// ========================================================

template<class K, class V, class HASH>
bool IntrusiveHashTable<K, V, HASH>::isValid() const
{
	return isAllocated() && usedBuckets <= bucketCount;
}

// ========================================================
// IntrusiveHashTable::isEmpty():
// ========================================================

template<class K, class V, class HASH>
bool IntrusiveHashTable<K, V, HASH>::isEmpty() const
{
	return usedBuckets == 0;
}

// ========================================================
// IntrusiveHashTable::getSize():
// ========================================================

template<class K, class V, class HASH>
uint IntrusiveHashTable<K, V, HASH>::getSize() const
{
	return usedBuckets;
}

// ========================================================
// IntrusiveHashTable::getBucketCount():
// ========================================================

template<class K, class V, class HASH>
uint IntrusiveHashTable<K, V, HASH>::getBucketCount() const
{
	return bucketCount;
}

// ========================================================
// IntrusiveHashTable::getMemoryBytes():
// ========================================================

template<class K, class V, class HASH>
uint IntrusiveHashTable<K, V, HASH>::getMemoryBytes() const
{
	return bucketCount * sizeof(ValueType *);
}

// ========================================================
// IntrusiveHashTable::setAllowDuplicateKeys():
// ========================================================

template<class K, class V, class HASH>
void IntrusiveHashTable<K, V, HASH>::setAllowDuplicateKeys(const bool allow)
{
	allowDupKeys = allow;
}

// ========================================================
// IntrusiveHashTable::isAllowingDuplicateKeys():
// ========================================================

template<class K, class V, class HASH>
bool IntrusiveHashTable<K, V, HASH>::isAllowingDuplicateKeys() const
{
	return allowDupKeys;
}

// ========================================================
// IntrusiveHashTable::find():
// ========================================================

template<class K, class V, class HASH>
typename IntrusiveHashTable<K, V, HASH>::ValueType *
IntrusiveHashTable<K, V, HASH>::find(const KeyType & key) const
{
	if (isEmpty())
	{
		return nullptr;
	}

	const uint keyHash = hashOf(key);
	for (ValueType * item = table[bucketOf(keyHash)]; item != nullptr; item = item->htNext)
	{
		if (keyHash == item->htKeyHash)
		{
			return item;
		}
	}

	return nullptr;
}

// ========================================================
// IntrusiveHashTable::findAllMatching():
// ========================================================

template<class K, class V, class HASH>
uint IntrusiveHashTable<K, V, HASH>::findAllMatching(const KeyType & key, ValueType ** items, const uint maxItems) const
{
	DEBUG_CHECK(items != nullptr);
	DEBUG_CHECK(maxItems != 0);

	if (isEmpty())
	{
		return 0;
	}

	const uint keyHash = hashOf(key);
	uint foundCount = 0;

	// Duplicate keys will share the same bucket/chain.
	for (ValueType * item = table[bucketOf(keyHash)]; item != nullptr; item = item->htNext)
	{
		if (keyHash == item->htKeyHash)
		{
			items[foundCount++] = item;
		}
		if (foundCount == maxItems)
		{
			break;
		}
	}

	return foundCount;
}

// ========================================================
// IntrusiveHashTable::countAllMatching():
// ========================================================

template<class K, class V, class HASH>
uint IntrusiveHashTable<K, V, HASH>::countAllMatching(const KeyType & key) const
{
	if (isEmpty())
	{
		return 0;
	}

	const uint keyHash = hashOf(key);
	uint foundCount = 0;

	// Duplicate keys will share the same bucket/chain.
	for (ValueType * item = table[bucketOf(keyHash)]; item != nullptr; item = item->htNext)
	{
		if (keyHash == item->htKeyHash)
		{
			++foundCount;
		}
	}

	return foundCount;
}

// ========================================================
// IntrusiveHashTable::operator[]:
// ========================================================

template<class K, class V, class HASH>
typename IntrusiveHashTable<K, V, HASH>::ValueType *
IntrusiveHashTable<K, V, HASH>::operator [] (const KeyType & key) const
{
	return find(key);
}

// ========================================================
// IntrusiveHashTable::insert():
// ========================================================

template<class K, class V, class HASH>
bool IntrusiveHashTable<K, V, HASH>::insert(const KeyType & key, ValueType * value)
{
	DEBUG_CHECK(value != nullptr);
	DEBUG_CHECK(!value->isLinkedToHashTable());

	// Ensure allocated:
	allocate();

	const uint keyHash = hashOf(key);
	const uint bucket  = bucketOf(keyHash);

	// This bucket's chain is already in use. Append to it:
	if (table[bucket] != nullptr)
	{
		// If disallowing duplicate keys we must scan this chain
		// and make sure no key with the same name already exists.
		if (!isAllowingDuplicateKeys())
		{
			for (ValueType * item = table[bucket]; item != nullptr; item = item->htNext)
			{
				if (keyHash == item->htKeyHash)
				{
					return false; // This specific key is already in use, fail.
				}
			}
		}

		// Make the new value head of the chain:
		value->htKeyHash = keyHash;
		value->htNext    = table[bucket];
		table[bucket]    = value;
	}
	else // Empty chain:
	{
		value->htKeyHash = keyHash;
		value->htNext    = nullptr;
		table[bucket]    = value;
	}

	++usedBuckets;
	return true;
}

// ========================================================
// IntrusiveHashTable::remove():
// ========================================================

template<class K, class V, class HASH>
typename IntrusiveHashTable<K, V, HASH>::ValueType *
IntrusiveHashTable<K, V, HASH>::remove(const KeyType & key)
{
	if (isEmpty())
	{
		return nullptr;
	}

	const uint keyHash = hashOf(key);
	const uint bucket  = bucketOf(keyHash);

	ValueType * previous = nullptr;
	for (ValueType * item = table[bucket]; item != nullptr;)
	{
		if (keyHash == item->htKeyHash)
		{
			--usedBuckets;

			if (previous != nullptr)
			{
				// Not the head of the chain, remove from middle:
				previous->htNext = item->htNext;
			}
			else if (item == table[bucket] && item->htNext == nullptr)
			{
				// Single item bucket, clear the entry:
				table[bucket] = nullptr;
			}
			else if (item == table[bucket] && item->htNext != nullptr)
			{
				// Head of chain with other item(s) following:
				table[bucket] = item->htNext;
			}
			else
			{
				ALWAYS_CHECK(false && "IntrusiveHashTable bucket chain is corrupted!");
			}

			item->htNext = nullptr;
			item->htKeyHash = 0;
			return item;
		}

		previous = item;
		item = item->htNext;
	}

	return nullptr;
}

// ========================================================
// IntrusiveHashTable::removeAllMatching():
// ========================================================

template<class K, class V, class HASH>
uint IntrusiveHashTable<K, V, HASH>::removeAllMatching(const KeyType & key)
{
	if (isEmpty())
	{
		return 0;
	}

	const uint keyHash = hashOf(key);
	const uint bucket  = bucketOf(keyHash);

	ValueType * previous = nullptr;
	uint removedCount = 0;

	for (ValueType * item = table[bucket]; item != nullptr;)
	{
		if (keyHash == item->htKeyHash)
		{
			--usedBuckets;

			if (previous != nullptr)
			{
				// Not the head of the chain, remove from middle:
				previous->htNext = item->htNext;
			}
			else if (item == table[bucket] && item->htNext == nullptr)
			{
				// Stop when the bucket's chain has been cleared:
				table[bucket] = nullptr;
				item->htNext  = nullptr;
				item->htKeyHash = 0;

				++removedCount;
				break;
			}
			else if (item == table[bucket] && item->htNext != nullptr)
			{
				// Head of chain with other item(s) following:
				table[bucket] = item->htNext;
			}
			else
			{
				ALWAYS_CHECK(false && "IntrusiveHashTable bucket chain is corrupted!");
			}

			ValueType * nextItem = item->htNext;
			item->htNext = nullptr;
			item->htKeyHash = 0;
			item = nextItem;

			++removedCount;
		}
		else
		{
			previous = item;
			item = item->htNext;
		}
	}

	return removedCount;
}

// ========================================================
// IntrusiveHashTable::transferTo():
// ========================================================

template<class K, class V, class HASH>
void IntrusiveHashTable<K, V, HASH>::transferTo(IntrusiveHashTable<K, V, HASH> & dest)
{
	DEBUG_CHECK(dest.isEmpty() && "Destination table contents would be lost!");

	// Dest must be empty, but it might have
	// dynamic memory assigned to it, so free first.
	if (dest.isAllocated())
	{
		dest.freeDynamicMemory();
	}

	dest.table        = table;
	dest.bucketCount  = bucketCount;
	dest.usedBuckets  = usedBuckets;
	dest.allowDupKeys = allowDupKeys;

	table        = nullptr;
	bucketCount  = 0;
	usedBuckets  = 0;
	allowDupKeys = false;
}

// ========================================================
// IntrusiveHashTable::hashOf():
// ========================================================

template<class K, class V, class HASH>
uint IntrusiveHashTable<K, V, HASH>::hashOf(const KeyType & key) const
{
	const uint keyHash = KeyHasher()(key);
	DEBUG_CHECK(keyHash != 0 && "Null hash indexes not allowed!");
	return keyHash;
}

// ========================================================
// IntrusiveHashTable::bucketOf():
// ========================================================

template<class K, class V, class HASH>
uint IntrusiveHashTable<K, V, HASH>::bucketOf(const uint keyHash) const
{
	const uint bucket = keyHash % bucketCount;
	DEBUG_CHECK(bucket < bucketCount && "Bucket index out-of-bounds!");
	return bucket;
}

// ========================================================
// IntrusiveHashTable::freeDynamicMemory():
// ========================================================

template<class K, class V, class HASH>
void IntrusiveHashTable<K, V, HASH>::freeDynamicMemory()
{
	delete[] table;
	table = nullptr;
}

// ========================================================
// IntrusiveHashTable::begin():
// ========================================================

template<class K, class V, class HASH>
typename IntrusiveHashTable<K, V, HASH>::Iterator
IntrusiveHashTable<K, V, HASH>::begin()
{
	return Iterator(this, table, bucketCount);
}

// ========================================================
// IntrusiveHashTable::end():
// ========================================================

template<class K, class V, class HASH>
typename IntrusiveHashTable<K, V, HASH>::Iterator
IntrusiveHashTable<K, V, HASH>::end()
{
	return Iterator(this, nullptr, 0);
}

// ========================================================
// IntrusiveHashTable::begin() [const]:
// ========================================================

template<class K, class V, class HASH>
const typename IntrusiveHashTable<K, V, HASH>::Iterator
IntrusiveHashTable<K, V, HASH>::begin() const
{
	return Iterator(this, table, bucketCount);
}

// ========================================================
// IntrusiveHashTable::end() [const]:
// ========================================================

template<class K, class V, class HASH>
const typename IntrusiveHashTable<K, V, HASH>::Iterator
IntrusiveHashTable<K, V, HASH>::end() const
{
	return Iterator(this, nullptr, 0);
}

// ================================================================================================
// IntrusiveHashTable::Iterator class definition:
// ================================================================================================

// ========================================================
// IntrusiveHashTable::Iterator::Iterator():
// ========================================================

template<class K, class V, class HASH>
IntrusiveHashTable<K, V, HASH>::Iterator::Iterator()
	: myTable(nullptr)
	, buckets(nullptr)
	, currentLink(nullptr)
	, currentBucket(0)
	, bucketCount(0)
{
}

// ========================================================
// IntrusiveHashTable::Iterator::Iterator():
// ========================================================

template<class K, class V, class HASH>
IntrusiveHashTable<K, V, HASH>::Iterator::Iterator(const IntrusiveHashTable<K, V, HASH> * ht,
                                                   IntrusiveHashTable<K, V, HASH>::ValueType ** bkts,
                                                   const uint count)
	: myTable(ht)
	, buckets(bkts)
	, currentLink(nullptr)
	, currentBucket(0)
	, bucketCount(count)
{
	DEBUG_CHECK(myTable != nullptr);
	if (buckets != nullptr)
	{
		for (;;)
		{
			if ((currentLink = buckets[currentBucket]) != nullptr)
			{
				break;
			}
			if (++currentBucket == bucketCount)
			{
				break;
			}
		}
	}
}

// ========================================================
// IntrusiveHashTable::Iterator::operator* :
// ========================================================

template<class K, class V, class HASH>
typename IntrusiveHashTable<K, V, HASH>::ValueType & IntrusiveHashTable<K, V, HASH>::Iterator::operator* ()
{
	validateDereference();
	return *currentLink;
}

// ========================================================
// IntrusiveHashTable::Iterator::operator* :
// ========================================================

template<class K, class V, class HASH>
const typename IntrusiveHashTable<K, V, HASH>::ValueType & IntrusiveHashTable<K, V, HASH>::Iterator::operator* () const
{
	validateDereference();
	return *currentLink;
}

// ========================================================
// IntrusiveHashTable::Iterator::operator-> :
// ========================================================

template<class K, class V, class HASH>
typename IntrusiveHashTable<K, V, HASH>::ValueType * IntrusiveHashTable<K, V, HASH>::Iterator::operator-> ()
{
	validateDereference();
	return currentLink;
}

// ========================================================
// IntrusiveHashTable::Iterator::operator-> :
// ========================================================

template<class K, class V, class HASH>
const typename IntrusiveHashTable<K, V, HASH>::ValueType * IntrusiveHashTable<K, V, HASH>::Iterator::operator-> () const
{
	validateDereference();
	return currentLink;
}

// ========================================================
// IntrusiveHashTable::Iterator::operator++ (pre-incr):
// ========================================================

template<class K, class V, class HASH>
const typename IntrusiveHashTable<K, V, HASH>::Iterator & IntrusiveHashTable<K, V, HASH>::Iterator::operator++ () const
{
	validateMovement();
	if ((currentLink = currentLink->htNext) == nullptr)
	{
		do
		{
			if (++currentBucket == bucketCount)
			{
				break; // Reached end.
			}
			currentLink = buckets[currentBucket];
		}
		while (currentLink == nullptr);
	}
	return *this;
}

// ========================================================
// IntrusiveHashTable::Iterator::operator++ (post-incr):
// ========================================================

template<class K, class V, class HASH>
typename IntrusiveHashTable<K, V, HASH>::Iterator IntrusiveHashTable<K, V, HASH>::Iterator::operator++ (int) const
{
	validateMovement();
	Iterator tmp(*this);
	++(*this);
	return tmp; // Return old, pre-increment value.
}

// ========================================================
// IntrusiveHashTable::Iterator::operator == :
// ========================================================

template<class K, class V, class HASH>
bool IntrusiveHashTable<K, V, HASH>::Iterator::operator == (const IntrusiveHashTable<K, V, HASH>::Iterator & other) const
{
	DEBUG_CHECK(myTable == other.myTable && "Iterators belong to different hash-tables!");
	return currentLink == other.currentLink;
}

// ========================================================
// IntrusiveHashTable::Iterator::operator != :
// ========================================================

template<class K, class V, class HASH>
bool IntrusiveHashTable<K, V, HASH>::Iterator::operator != (const IntrusiveHashTable<K, V, HASH>::Iterator & other) const
{
	DEBUG_CHECK(myTable == other.myTable && "Iterators belong to different hash-tables!");
	return currentLink != other.currentLink;
}

// ========================================================
// IntrusiveHashTable::Iterator::getHashTable():
// ========================================================

template<class K, class V, class HASH>
const IntrusiveHashTable<K, V, HASH> * IntrusiveHashTable<K, V, HASH>::Iterator::getHashTable() const
{
	return myTable;
}

// ========================================================
// IntrusiveHashTable::Iterator::validateDereference():
// ========================================================

template<class K, class V, class HASH>
void IntrusiveHashTable<K, V, HASH>::Iterator::validateDereference() const
{
	DEBUG_CHECK(currentLink != nullptr && "Invalid hash-table iterator dereference!");
	DEBUG_CHECK(myTable     != nullptr && "Invalid hash-table iterator!");
	DEBUG_CHECK(buckets     != nullptr && "Invalid hash-table iterator!");
}

// ========================================================
// IntrusiveHashTable::Iterator::validateMovement():
// ========================================================

template<class K, class V, class HASH>
void IntrusiveHashTable<K, V, HASH>::Iterator::validateMovement() const
{
	DEBUG_CHECK(currentLink != nullptr && "Moving pass the end of hash-table sequence!");
	DEBUG_CHECK(myTable     != nullptr && "Invalid hash-table iterator!");
	DEBUG_CHECK(buckets     != nullptr && "Invalid hash-table iterator!");
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_CONTAINERS_INTRUSIVE_HASH_TABLE_HPP
