
// ================================================================================================
// -*- C++ -*-
// File: c_string.hpp
// Author: Guilherme R. Lampert
// Created on: 17/12/14
// Brief: Functions to manipulate C-style char* strings.
//        The string classes are built on top of these functions.
// ================================================================================================

#ifndef ATLAS_CORE_STRINGS_C_STRING_HPP
#define ATLAS_CORE_STRINGS_C_STRING_HPP

#include "atlas/core/utils/basic_types.hpp"
#include <cctype>
#include <cstdarg>

namespace atlas
{
namespace core
{

// ========================================================
// More idiomatic wrappers to the CTYPE library:
// ========================================================

namespace ctype
{
ATTRIBUTE_PURE inline bool isAlpha    (char c) { return !!std::isalpha(c);  }
ATTRIBUTE_PURE inline bool isAlphaNum (char c) { return !!std::isalnum(c);  }
ATTRIBUTE_PURE inline bool isControl  (char c) { return !!std::iscntrl(c);  }
ATTRIBUTE_PURE inline bool isGraph    (char c) { return !!std::isgraph(c);  }
ATTRIBUTE_PURE inline bool isPrint    (char c) { return !!std::isprint(c);  }
ATTRIBUTE_PURE inline bool isPunct    (char c) { return !!std::ispunct(c);  }
ATTRIBUTE_PURE inline bool isSpace    (char c) { return !!std::isspace(c);  }
ATTRIBUTE_PURE inline bool isDigit    (char c) { return !!std::isdigit(c);  }
ATTRIBUTE_PURE inline bool isHexDigit (char c) { return !!std::isxdigit(c); }
ATTRIBUTE_PURE inline bool isLower    (char c) { return !!std::islower(c);  }
ATTRIBUTE_PURE inline bool isUpper    (char c) { return !!std::isupper(c);  }
ATTRIBUTE_PURE inline char toLower    (char c) { return static_cast<char>(std::tolower(c)); }
ATTRIBUTE_PURE inline char toUpper    (char c) { return static_cast<char>(std::toupper(c)); }
} // namespace ctype {}

// ========================================================
// Low-level C-string manipulation:
// ========================================================

// Length in characters of the C string (not including the null terminator).
uint strLength(const char * str) ATTRIBUTE_PURE;

// Case-sensitive equal-compare for two strings.
bool strEquals(const char * str1, const char * str2);

// Compare characters between two strings. Same as strcmp()/strcasecmp().
int strCmp(const char * str1, const char * str2);
int strCmpIgnoreCase(const char * str1, const char * str2);

// Compare N characters between two strings. Same as strncmp()/strncasecmp().
int strCmpN(const char * str1, const char * str2, uint charsToCompare);
int strCmpNIgnoreCase(const char * str1, const char * str2, uint charsToCompare);

// Test if a given string begins with another substring (prefix).
bool strStartsWith(const char * str, uint strLen, const char * prefix, uint prefixLen);
bool strStartsWithIgnoreCase(const char * str, uint strLen, const char * prefix, uint prefixLen);

// Test if a given string ends with another substring (suffix).
bool strEndsWith(const char * str, uint strLen, const char * suffix, uint suffixLen);
bool strEndsWithIgnoreCase(const char * str, uint strLen, const char * suffix, uint suffixLen);

// Finds first occurrence of a char or substring, return its index inside the source string.
// If nothing is found, these functions return -1.
int strFindFirst(const char * str, char c);
int strFindFirst(const char * str, const char * substring, uint substringLen);

// Finds last occurrence of a char or substring, return its index inside the source string.
// If nothing is found, these functions return -1.
int strFindLast(const char * str, char c);
int strFindLast(const char * str, const char * substring, uint substringLen);

// Finds first occurrence of any char of `charSet` in `str`. Similar to `std::strcspn()`.
// Returns the length of `str` if none of the characters in `charSet` are found in `str`.
uint strFindFirstMatching(const char * str, const char * charSet);

// Copy with overflow checking. Ensures a null at the end even if the dest buffer overflows, always
// producing a valid (null terminated) string at `dest`. Destination and source must not overlap!
uint strCopy(char  * RESTRICT dest, uint destSizeInChars, const char * RESTRICT source);
uint strCopyN(char * RESTRICT dest, uint destSizeInChars, const char * RESTRICT source, uint charsToCopy);

// Erases a part of the string's content, filling the end with null chars.
// `startIndex` must be in a valid range inside the input string. The char pointed by `startIndex` is included.
// If `charsToErase` is less than 0 or greater than the string's length, erases until the end,
// starting at `startIndex`. Does nothing if the string is empty or if `charsToErase` is zero.
uint strErase(char * str, uint strLen, uint startIndex, int charsToErase);

// Replaces a sequence of characters in a string by a single character replicated `charsToReplace` times.
// `startIndex` must be in a valid range inside this string. The char pointed by `startIndex` is included.
// If `charsToErase` is less than 0 or greater than the string's length, fills with `replaceWith` chars
// starting at `startIndex`. Does nothing if the string is empty or if `charsToErase` is zero.
void strReplace(char * str, uint strLen, uint startIndex, int charsToReplace, char replaceWith);

// Searches for any occurrences of `strToReplace` inside `str` and replaces those with `strReplaceWith`.
// `charsToReplace` is the number of characters that will be taken into account in the search->replace process.
// If any of the inputs is longer than that, the extra length is ignored. The search is always case-sensitive.
void strReplace(char * str, uint strLen, const char * strToReplace, const char * strReplaceWith, uint charsToReplace);

// Split string into tokens, modifying the input string.
// Similar to C's `strtok()`, but keeps no global data behind the scenes, making this function thread-safe.
char * strTokenize(char * RESTRICT str, const char * RESTRICT delimiters, char ** remaining);

// Trim a string left or right (remove white spaces).
// Whitespace chars are determined by `ctype::isSpace()`.
uint strRTrim(char * str, uint strLen);
uint strLTrim(char * str, uint strLen);

// Cast a string to uppercase or lowercase in-place.
char * strToUpper(char * str);
char * strToLower(char * str);

// Compute the hash value of a string.
// The hash function of choice is implementation defined.
uint32 strHash(const char * str) ATTRIBUTE_PURE;
uint32 strHashIgnoreCase(const char * str) ATTRIBUTE_PURE;

// Increment the string pointer to skip leading white spaces.
// Whitespace chars are determined by `ctype::isSpace()`.
char * strSkipWhiteSpaces(char * str);
const char * strSkipWhiteSpaces(const char * str);

// Allocate and free strings from the program heap.
// `strAllocate()` receives the length in chars of the string to be allocate,
// adding +1 to that length by default for the null terminator.
char * strAllocate(uint lengthNotIncludingTerminator);
char * strClone(const char * source, const uint * optionalSrcLength = nullptr);
void strDeallocate(char * str); // -- Accepts null.

// Replacement for `snprintf()`. Truncates the output on error or overflow.
int strPrintF(char * buf, uint bufSize, const char * format, ...) ATTRIBUTE_FORMAT_FUNC(printf, 3, 4);

// Replacement for `vsnprintf()`. Truncates the output on error or overflow.
int strVPrintF(char * buf, uint bufSize, const char * format, va_list vaList) ATTRIBUTE_FORMAT_FUNC(printf, 3, 0);

// Convert a 32-bit integer to a C-string. Numerical base is a value >= 2 && <= 36.
bool strFromInt(int32  val, char * buf, uint bufSize, int base);
bool strFromInt(uint32 val, char * buf, uint bufSize, int base);

// Convert a 64-bit integer to a C-string. Numerical base is a value >= 2 && <= 36.
bool strFromInt(int64  val, char * buf, uint bufSize, int base);
bool strFromInt(uint64 val, char * buf, uint bufSize, int base);

// Convert a floating point value to a C-string.
// `decimalDigits` is the number of decimal digits to output. Pass -1 to use the default precision.
bool strFromFloat(float  val, char * buf, uint bufSize, int decimalDigits);
bool strFromFloat(double val, char * buf, uint bufSize, int decimalDigits);

// Convert a C-string to a 32-bit integer. Numerical base is a value == 0 || >= 2 && <= 36.
// If `endPtr` is not null, it returns a pointer to the first character that could not be converted.
// The string containing the digits may be preceded by white spaces.
bool strToInt(const char * numPtr, char ** endPtr, int base, int32  & result);
bool strToInt(const char * numPtr, char ** endPtr, int base, uint32 & result);

// Convert a C-string to a 64-bit integer. Numerical base is a value == 0 || >= 2 && <= 36.
// If `endPtr` is not null, it returns a pointer to the first character that could not be converted.
// The string containing the digits may be preceded by white spaces.
bool strToInt(const char * numPtr, char ** endPtr, int base, int64  & result);
bool strToInt(const char * numPtr, char ** endPtr, int base, uint64 & result);

// Convert a C-string to a floating point value.
// If `endPtr` is not null, it returns a pointer to the first character that could not be converted.
// The string containing the digits may be preceded by white spaces.
bool strToFloat(const char * numPtr, char ** endPtr, float  & result);
bool strToFloat(const char * numPtr, char ** endPtr, double & result);

// Fast C-string to 32-bit floating point value.
// This function does not skip leading white spaces before the first
// numerical character, nor does any validation of the input parameters.
// Floating point numbers must be in the form [+|-]int[.frac]. Example: `3.141592`;
// No support for scientific notation either.
float strToFloatFast(const char * numPtr, char ** endPtr);

// Removes trailing zeros from a floating point numeric string by
// replacing them with null characters. Example: 1.4200 => 1.42
// `newLen` outputs the new length in characters of the trimmed string,
// not including the final null. This parameter is optional.
char * strTrimTrailingFloatZeros(char * str, uint * newLen = nullptr);

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_STRINGS_C_STRING_HPP
