
// ================================================================================================
// -*- C++ -*-
// File: sized_string.hpp
// Author: Guilherme R. Lampert
// Created on: 17/12/14
// Brief: Fixed-size string template class.
// ================================================================================================

#ifndef ATLAS_CORE_STRINGS_SIZED_STRING_HPP
#define ATLAS_CORE_STRINGS_SIZED_STRING_HPP

#include "atlas/core/strings/string_base.hpp"
#include <cstring>

namespace atlas
{
namespace core
{

// ========================================================
// template class SizedString<SIZE>:
// ========================================================

template<uint SIZE>
class SizedString final
	: public StringBase
{
public:

	// Construct an empty but valid string.
	SizedString();

	// Construct a string filled with a given character (fills `SIZE` chars).
	SizedString(char fillWith);

	// Construct from a null terminated C string:
	SizedString(const char * str);
	SizedString(const char * str, uint count);
	SizedString(const char * str, uint startIndex, uint count);

	// Construct from a compatible string type:
	SizedString(const SizedString & other);
	SizedString(const StringBase & other);
	SizedString(const StringBase & other, uint count);
	SizedString(const StringBase & other, uint startIndex, uint count);

	// Assignment operators:
	SizedString & operator  = (const SizedString & other);
	SizedString & operator  = (const StringBase & other);
	SizedString & operator  = (const char * str);
	SizedString & operator += (const StringBase & other);
	SizedString & operator += (const char * str);
	SizedString & operator += (char c);
	SizedString   operator  + (const StringBase & other) const;
	SizedString   operator  + (const char * str) const;
	SizedString   operator  + (char c) const;

	// Assign string of compatible type:
	SizedString & assign(const StringBase & other);
	SizedString & assign(const StringBase & other, uint count);
	SizedString & assign(const StringBase & other, uint startIndex, uint count);

	// Assign C-string:
	SizedString & assign(const char * str);
	SizedString & assign(const char * str, uint count);
	SizedString & assign(const char * str, uint startIndex, uint count);

	// Append string of compatible type:
	SizedString & append(const StringBase & other);
	SizedString & append(const StringBase & other, uint count);
	SizedString & append(const StringBase & other, uint startIndex, uint count);

	// Append C-string:
	SizedString & append(char c);
	SizedString & append(const char * str);
	SizedString & append(const char * str, uint count);
	SizedString & append(const char * str, uint startIndex, uint count);

	// Finds the first occurrence of a substring in this string, returning it as a new string
	// or an empty string if the substring is not part of it. Search is always case-sensitive.
	SizedString extractSubstring(const StringBase & substring) const;
	SizedString extractSubstring(const char * substring) const;

	// Extract a part of this string to a new one.
	// If `count` is negative or greater than this string's length, copies from `startIndex`
	// until the end of this string. Produces an empty string if this is empty or if `count` is zero.
	SizedString extractSubstring(uint startIndex, int count) const;

	// Add padding characters to the start or end of the string. If the string is already
	// longer than the requested padding, the input string is returned unchanged.
	SizedString padRight(const uint padWanted, char padChar = ' ') const;
	SizedString padLeft(const uint padWanted, char padChar = ' ') const;

	// Append or remove a single character at the end of the string.
	// `pushBack()` will fail with a debug assertion if the string is full.
	// `popBack()`  will fail with a debug assertion if the string is empty.
	void pushBack(char c);
	void popBack();

private:

	// Since this string never grows, if you try to write more
	// than SIZE chars to it, you'll get a debug assertion.
	char buffer[SIZE];
};

// ================================================================================================
// SizedString inline methods:
// ================================================================================================

// ========================================================
// SizedString::SizedString():
// ========================================================

template<uint SIZE>
SizedString<SIZE>::SizedString()
	: StringBase(buffer, SIZE, 0)
{
	// Empty string.
}

// ========================================================
// SizedString::SizedString():
// ========================================================

template<uint SIZE>
SizedString<SIZE>::SizedString(const char fillWith)
	: StringBase(buffer, SIZE, 0)
{
	if (fillWith != '\0')
	{
		length = SIZE - 1;
		fill(fillWith);
	}
}

// ========================================================
// SizedString::SizedString():
// ========================================================

template<uint SIZE>
SizedString<SIZE>::SizedString(const char * str)
	: StringBase(buffer, SIZE, 0)
{
	assign(str);
}

// ========================================================
// SizedString::SizedString():
// ========================================================

template<uint SIZE>
SizedString<SIZE>::SizedString(const char * str, const uint count)
	: StringBase(buffer, SIZE, 0)
{
	assign(str, count);
}

// ========================================================
// SizedString::SizedString():
// ========================================================

template<uint SIZE>
SizedString<SIZE>::SizedString(const char * str, const uint startIndex, const uint count)
	: StringBase(buffer, SIZE, 0)
{
	assign(str, startIndex, count);
}

// ========================================================
// SizedString::SizedString():
// ========================================================

template<uint SIZE>
SizedString<SIZE>::SizedString(const StringBase & other)
	: StringBase(buffer, SIZE, 0)
{
	assign(other);
}

// ========================================================
// SizedString::SizedString():
// ========================================================

template<uint SIZE>
SizedString<SIZE>::SizedString(const SizedString<SIZE> & other)
	: StringBase(buffer, SIZE, 0)
{
	assign(other);
}

// ========================================================
// SizedString::SizedString():
// ========================================================

template<uint SIZE>
SizedString<SIZE>::SizedString(const StringBase & other, const uint count)
	: StringBase(buffer, SIZE, 0)
{
	assign(other, count);
}

// ========================================================
// SizedString::SizedString():
// ========================================================

template<uint SIZE>
SizedString<SIZE>::SizedString(const StringBase & other, const uint startIndex, const uint count)
	: StringBase(buffer, SIZE, 0)
{
	assign(other, startIndex, count);
}

// ========================================================
// SizedString::operator = :
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::operator = (const SizedString<SIZE> & other)
{
	return assign(other);
}

// ========================================================
// SizedString::operator = :
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::operator = (const StringBase & other)
{
	return assign(other);
}

// ========================================================
// SizedString::operator = :
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::operator = (const char * str)
{
	return assign(str);
}

// ========================================================
// SizedString::operator += :
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::operator += (const StringBase & other)
{
	return append(other);
}

// ========================================================
// SizedString::operator += :
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::operator += (const char * str)
{
	return append(str);
}

// ========================================================
// SizedString::operator += :
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::operator += (const char c)
{
	pushBack(c);
	return *this;
}

// ========================================================
// SizedString::operator + :
// ========================================================

template<uint SIZE>
SizedString<SIZE> SizedString<SIZE>::operator + (const StringBase & other) const
{
	SizedString<SIZE> tmp(*this);
	tmp.append(other);
	return tmp;
}

// ========================================================
// SizedString::operator + :
// ========================================================

template<uint SIZE>
SizedString<SIZE> SizedString<SIZE>::operator + (const char * str) const
{
	SizedString<SIZE> tmp(*this);
	tmp.append(str);
	return tmp;
}

// ========================================================
// SizedString::operator + :
// ========================================================

template<uint SIZE>
SizedString<SIZE> SizedString<SIZE>::operator + (const char c) const
{
	SizedString<SIZE> tmp(*this);
	tmp.pushBack(c);
	return tmp;
}

// ========================================================
// SizedString::assign():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::assign(const StringBase & other)
{
	return assign(other.getCStr(), other.getLength());
}

// ========================================================
// SizedString::assign():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::assign(const StringBase & other, const uint count)
{
	return assign(other.getCStr(), count);
}

// ========================================================
// SizedString::assign():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::assign(const StringBase & other, const uint startIndex, const uint count)
{
	return assign(other.getCStr(), startIndex, count);
}

// ========================================================
// SizedString::assign():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::assign(const char * str)
{
	DEBUG_CHECK(str != nullptr);
	DEBUG_CHECK(strLength(str) < SIZE);
	if (*str != '\0')
	{
		length = strCopy(cStr, SIZE, str);
	}
	return *this;
}

// ========================================================
// SizedString::assign():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::assign(const char * str, const uint count)
{
	DEBUG_CHECK(str != nullptr);
	DEBUG_CHECK(count < SIZE);
	if (*str != '\0' && count != 0)
	{
		length = strCopyN(cStr, SIZE, str, count);
	}
	return *this;
}

// ========================================================
// SizedString::assign():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::assign(const char * str, const uint startIndex, const uint count)
{
	DEBUG_CHECK(str != nullptr);
	DEBUG_CHECK(count < SIZE);
	if (*str != '\0' && count != 0)
	{
		length = strCopyN(cStr, SIZE, &str[startIndex], count);
	}
	return *this;
}

// ========================================================
// SizedString::append():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::append(const StringBase & other)
{
	return append(other.getCStr(), other.getLength());
}

// ========================================================
// SizedString::append():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::append(const StringBase & other, const uint count)
{
	return append(other.getCStr(), count);
}

// ========================================================
// SizedString::append():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::append(const StringBase & other, const uint startIndex, const uint count)
{
	return append(other.getCStr(), startIndex, count);
}

// ========================================================
// SizedString::append():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::append(const char c)
{
	pushBack(c);
	return *this;
}

// ========================================================
// SizedString::append():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::append(const char * str)
{
	return append(str, 0, strLength(str));
}

// ========================================================
// SizedString::append():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::append(const char * str, const uint count)
{
	return append(str, 0, count);
}

// ========================================================
// SizedString::append():
// ========================================================

template<uint SIZE>
SizedString<SIZE> & SizedString<SIZE>::append(const char * str, const uint startIndex, const uint count)
{
	DEBUG_CHECK(str != nullptr);
	DEBUG_CHECK(count < SIZE - length);
	if (*str != '\0' && count != 0)
	{
		std::memmove(&cStr[length], &str[startIndex], count);
		length += count;
		cStr[length] = '\0';
	}
	return *this;
}

// ========================================================
// SizedString::extractSubstring():
// ========================================================

template<uint SIZE>
SizedString<SIZE> SizedString<SIZE>::extractSubstring(const StringBase & substring) const
{
	SizedString<SIZE> result;
	const int subIndex = firstIndexOf(substring);
	if (subIndex >= 0)
	{
		result.assign(cStr, subIndex, substring.getLength());
	}
	return result;
}

// ========================================================
// SizedString::extractSubstring():
// ========================================================

template<uint SIZE>
SizedString<SIZE> SizedString<SIZE>::extractSubstring(const char * substring) const
{
	SizedString<SIZE> result;
	const int subIndex = firstIndexOf(substring);
	if (subIndex >= 0)
	{
		result.assign(cStr, subIndex, strLength(substring));
	}
	return result;
}

// ========================================================
// SizedString::extractSubstring():
// ========================================================

template<uint SIZE>
SizedString<SIZE> SizedString<SIZE>::extractSubstring(const uint startIndex, const int count) const
{
	SizedString<SIZE> result;
	if (!isEmpty() && count != 0)
	{
		const uint charsAvailable = length - startIndex;
		const uint charsToCopy = (count > 0 && static_cast<uint>(count) <= charsAvailable) ? count : charsAvailable;
		result.assign(*this, startIndex, charsToCopy);
	}
	return result;
}

// ========================================================
// SizedString::padRight():
// ========================================================

template<uint SIZE>
SizedString<SIZE> SizedString<SIZE>::padRight(const uint padWanted, const char padChar) const
{
	if (length >= padWanted)
	{
		return *this;
	}

	int padCount = padWanted - length;
	SizedString<SIZE> paddingStr;
	while (padCount--)
	{
		paddingStr.pushBack(padChar);
	}

	return (*this) + paddingStr;
}

// ========================================================
// SizedString::padLeft():
// ========================================================

template<uint SIZE>
SizedString<SIZE> SizedString<SIZE>::padLeft(const uint padWanted, const char padChar) const
{
	if (length >= padWanted)
	{
		return *this;
	}

	int padCount = padWanted - length;
	SizedString<SIZE> paddingStr;
	while (padCount--)
	{
		paddingStr.pushBack(padChar);
	}

	return paddingStr + (*this);
}

// ========================================================
// SizedString::pushBack():
// ========================================================

template<uint SIZE>
void SizedString<SIZE>::pushBack(const char c)
{
	DEBUG_CHECK(length < SIZE - 1);

	if (c == '\0')
	{
		return;
	}

	cStr[length++] = c;
	cStr[length] = '\0';
}

// ========================================================
// SizedString::popBack():
// ========================================================

template<uint SIZE>
void SizedString<SIZE>::popBack()
{
	DEBUG_CHECK(!isEmpty());
	cStr[--length] = '\0';
}

// ========================================================
// Additional string operators that have to be global:
// ========================================================

template<uint SIZE>
bool operator == (const char * a, const SizedString<SIZE> & b)
{
	return b == a;
}

template<uint SIZE>
bool operator != (const char * a, const SizedString<SIZE> & b)
{
	return b != a;
}

template<uint SIZE>
SizedString<SIZE> operator + (const char * a, const SizedString<SIZE> & b)
{
	SizedString<SIZE> tmp(a);
	tmp.append(b);
	return tmp;
}

template<uint SIZE>
SizedString<SIZE> operator + (const char a, const SizedString<SIZE> & b)
{
	SizedString<SIZE> tmp;
	tmp.pushBack(a);
	tmp.append(b);
	return tmp;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_STRINGS_SIZED_STRING_HPP
