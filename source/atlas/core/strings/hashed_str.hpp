
// ================================================================================================
// -*- C++ -*-
// File: hashed_str.hpp
// Author: Guilherme R. Lampert
// Created on: 22/12/14
// Brief: String paired with its hash value.
// ================================================================================================

#ifndef ATLAS_CORE_STRINGS_HASHED_STR_HPP
#define ATLAS_CORE_STRINGS_HASHED_STR_HPP

#include "atlas/core/strings/string_base.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// template class HashedStr<STR_TYPE>:
// ========================================================

template<class STR_TYPE>
class HashedStr final
{
public:

	// From string C++ object:
	HashedStr(const STR_TYPE & str);           // Construct from string. Computes the (case-sensitive) hash.
	HashedStr(const STR_TYPE & str, uint32 h); // Construct from string and precomputed hash value.

	// Same as the above, but for raw C-strings:
	HashedStr(const char * str);
	HashedStr(const char * str, uint32 h);

	bool operator == (const HashedStr & other) const;
	bool operator == (const STR_TYPE & str) const;
	bool operator == (const char * str) const;
	bool operator == (uint32 h) const;

	bool operator != (const HashedStr & other) const;
	bool operator != (const STR_TYPE & str) const;
	bool operator != (const char * str) const;
	bool operator != (uint32 h) const;

	uint32 getHash() const;
	const STR_TYPE & getString() const;

private:

	// Must construct with a valid string.
	HashedStr() DELETED_METHOD;

	// Once constructed, can't be changed.
	HashedStr & operator = (const HashedStr &) DELETED_METHOD;

	const uint32   hash;   // Hash value of the following string.
	const STR_TYPE string; // `FixedString<T>`, `String` or any compatible type.
};

// ================================================================================================
// HashedStr inline methods:
// ================================================================================================

// ========================================================
// HashedStr::HashedStr():
// ========================================================

template<class STR_TYPE>
HashedStr<STR_TYPE>::HashedStr(const STR_TYPE & str)
	: hash(str.hash()), string(str)
{
}

// ========================================================
// HashedStr::HashedStr():
// ========================================================

template<class STR_TYPE>
HashedStr<STR_TYPE>::HashedStr(const STR_TYPE & str, const uint32 h)
	: hash(h), string(str)
{
}

// ========================================================
// HashedStr::HashedStr():
// ========================================================

template<class STR_TYPE>
HashedStr<STR_TYPE>::HashedStr(const char * str)
	: hash(strHash(str)), string(str)
{
}

// ========================================================
// HashedStr::HashedStr():
// ========================================================

template<class STR_TYPE>
HashedStr<STR_TYPE>::HashedStr(const char * str, const uint32 h)
	: hash(h), string(str)
{
}

// ========================================================
// HashedStr::operator == :
// ========================================================

template<class STR_TYPE>
bool HashedStr<STR_TYPE>::operator == (const HashedStr & other) const
{
	return hash == other.hash;
}

// ========================================================
// HashedStr::operator == :
// ========================================================

template<class STR_TYPE>
bool HashedStr<STR_TYPE>::operator == (const STR_TYPE & str) const
{
	return string == str;
}

// ========================================================
// HashedStr::operator == :
// ========================================================

template<class STR_TYPE>
bool HashedStr<STR_TYPE>::operator == (const char * str) const
{
	return string == str;
}

// ========================================================
// HashedStr::operator == :
// ========================================================

template<class STR_TYPE>
bool HashedStr<STR_TYPE>::operator == (const uint32 h) const
{
	return hash == h;
}

// ========================================================
// HashedStr::operator != :
// ========================================================

template<class STR_TYPE>
bool HashedStr<STR_TYPE>::operator != (const HashedStr & other) const
{
	return hash != other.hash;
}

// ========================================================
// HashedStr::operator != :
// ========================================================

template<class STR_TYPE>
bool HashedStr<STR_TYPE>::operator != (const STR_TYPE & str) const
{
	return string != str;
}

// ========================================================
// HashedStr::operator != :
// ========================================================

template<class STR_TYPE>
bool HashedStr<STR_TYPE>::operator != (const char * str) const
{
	return string != str;
}

// ========================================================
// HashedStr::operator != :
// ========================================================

template<class STR_TYPE>
bool HashedStr<STR_TYPE>::operator != (const uint32 h) const
{
	return hash != h;
}

// ========================================================
// HashedStr::getHash():
// ========================================================

template<class STR_TYPE>
uint32 HashedStr<STR_TYPE>::getHash() const
{
	return hash;
}

// ========================================================
// HashedStr::getString():
// ========================================================

template<class STR_TYPE>
const STR_TYPE & HashedStr<STR_TYPE>::getString() const
{
	return string;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_STRINGS_HASHED_STR_HPP
