
// ================================================================================================
// -*- C++ -*-
// File: c_string.cpp
// Author: Guilherme R. Lampert
// Created on: 17/12/14
// Brief: Functions to manipulate C-style char* strings.
// ================================================================================================

#include "atlas/core/strings/c_string.hpp"
#include "atlas/core/memory/mem_utils.hpp"
#include "atlas/core/utils/utility.hpp"
#include <cstdio>
#include <cstdlib>
#include <cstring>

// ========================================================
// NOTES:
//
// These C-string functions are the back-end of the String
// classes. We never run `strlen()` behind the caller's back,
// so if a function here requires the length of the string,
// is must be passed as a parameter.
//
// That's one trade off to maximize efficiency. But at any
// rate, these functions should only be needed by the
// String classes, so no higher level code should have
// to bother calling the low-level `char *` routines.
//
// ========================================================

namespace atlas
{
namespace core
{

// ========================================================
// strLength():
// ========================================================

uint strLength(const char * str)
{
	DEBUG_CHECK(str != nullptr);
	// strlen() maps directly to a hardware instruction on some
	// compilers|platforms, so we should take advantage of it.
	return static_cast<uint>(std::strlen(str));
}

// ========================================================
// strEquals():
// ========================================================

bool strEquals(const char * str1, const char * str2)
{
	return strCmp(str1, str2) == 0;
}

// ========================================================
// strCmp():
// ========================================================

int strCmp(const char * str1, const char * str2)
{
	DEBUG_CHECK(str1 != nullptr);
	DEBUG_CHECK(str2 != nullptr);

	if (str1 == str2)
	{
		// Same memory, strings are equal.
		return 0;
	}

	// Case-sensitive comparison:
	while (*str1 != '\0' && *str1 == *str2)
	{
		++str1, ++str2;
	}

	int result = *str1 - *str2;

	// Clamp to -1, 0, +1:
	if (result < 0)
	{
		result = -1;
	}
	else if (result > 0)
	{
		result = 1;
	}
	return result;
}

// ========================================================
// strCmpIgnoreCase():
// ========================================================

int strCmpIgnoreCase(const char * str1, const char * str2)
{
	DEBUG_CHECK(str1 != nullptr);
	DEBUG_CHECK(str2 != nullptr);

	if (str1 == str2)
	{
		// Same memory, strings are equal.
		return 0;
	}

	// Case-insensitive comparison:
	char c1, c2;
	do
	{
		c1 = *str1++;
		c2 = *str2++;
		if (c1 != c2)
		{
			// Cast them to lower-case and test again:
			c1 = ctype::toLower(c1);
			c2 = ctype::toLower(c2);
			if (c1 != c2)
			{
				break; // Strings not equal.
			}
		}
	}
	while (c1);

	int result = c1 - c2;

	// Clamp to -1, 0, +1:
	if (result < 0)
	{
		result = -1;
	}
	else if (result > 0)
	{
		result = 1;
	}
	return result;
}

// ========================================================
// strCmpN():
// ========================================================

int strCmpN(const char * str1, const char * str2, uint charsToCompare)
{
	DEBUG_CHECK(str1 != nullptr);
	DEBUG_CHECK(str2 != nullptr);
	DEBUG_CHECK(charsToCompare != 0);

	if (str1 == str2)
	{
		// Same memory, strings are equal.
		return 0;
	}

	// Case-sensitive comparison:
	while (--charsToCompare && *str1 != '\0' && *str1 == *str2)
	{
		++str1, ++str2;
	}

	int result = *str1 - *str2;

	// Clamp to -1, 0, +1:
	if (result < 0)
	{
		result = -1;
	}
	else if (result > 0)
	{
		result = 1;
	}
	return result;
}

// ========================================================
// strCmpNIgnoreCase():
// ========================================================

int strCmpNIgnoreCase(const char * str1, const char * str2, uint charsToCompare)
{
	DEBUG_CHECK(str1 != nullptr);
	DEBUG_CHECK(str2 != nullptr);
	DEBUG_CHECK(charsToCompare != 0);

	if (str1 == str2)
	{
		// Same memory, strings are equal.
		return 0;
	}

	// Case-insensitive comparison:
	char c1, c2;
	do
	{
		c1 = *str1++;
		c2 = *str2++;
		if (c1 != c2)
		{
			// Cast them to lower-case and test again:
			c1 = ctype::toLower(c1);
			c2 = ctype::toLower(c2);
			if (c1 != c2)
			{
				break; // Strings not equal.
			}
		}
	}
	while (--charsToCompare && c1);

	int result = c1 - c2;

	// Clamp to -1, 0, +1:
	if (result < 0)
	{
		result = -1;
	}
	else if (result > 0)
	{
		result = 1;
	}
	return result;
}

// ========================================================
// strStartsWith():
// ========================================================

bool strStartsWith(const char * str, const uint strLen,
                   const char * prefix, const uint prefixLen)
{
	DEBUG_CHECK(str    != nullptr);
	DEBUG_CHECK(prefix != nullptr);

	if (strLen < prefixLen)
	{
		return false;
	}
	if (strLen == 0 || prefixLen == 0)
	{
		return false;
	}
	return strCmpN(str, prefix, prefixLen) == 0;
}

// ========================================================
// strStartsWithIgnoreCase():
// ========================================================

bool strStartsWithIgnoreCase(const char * str, const uint strLen,
                             const char * prefix, const uint prefixLen)
{
	DEBUG_CHECK(str    != nullptr);
	DEBUG_CHECK(prefix != nullptr);

	if (strLen < prefixLen)
	{
		return false;
	}
	if (strLen == 0 || prefixLen == 0)
	{
		return false;
	}
	return strCmpNIgnoreCase(str, prefix, prefixLen) == 0;
}

// ========================================================
// strEndsWith():
// ========================================================

bool strEndsWith(const char * str, const uint strLen,
                 const char * suffix, const uint suffixLen)
{
	DEBUG_CHECK(str    != nullptr);
	DEBUG_CHECK(suffix != nullptr);

	if (strLen < suffixLen)
	{
		return false;
	}
	if (strLen == 0 || suffixLen == 0)
	{
		return false;
	}
	if (strLen == suffixLen)
	{
		return strCmp(str, suffix) == 0;
	}
	return strCmpN(&str[strLen - suffixLen], suffix, suffixLen) == 0;
}

// ========================================================
// strEndsWithIgnoreCase():
// ========================================================

bool strEndsWithIgnoreCase(const char * str, const uint strLen,
                           const char * suffix, const uint suffixLen)
{
	DEBUG_CHECK(str    != nullptr);
	DEBUG_CHECK(suffix != nullptr);

	if (strLen < suffixLen)
	{
		return false;
	}
	if (strLen == 0 || suffixLen == 0)
	{
		return false;
	}
	if (strLen == suffixLen)
	{
		return strCmpIgnoreCase(str, suffix) == 0;
	}
	return strCmpNIgnoreCase(&str[strLen - suffixLen], suffix, suffixLen) == 0;
}

// ========================================================
// strFindFirst():
// ========================================================

int strFindFirst(const char * str, const char c)
{
	DEBUG_CHECK(str != nullptr);

	int i = 0;
	for (; str[i] != '\0'; ++i)
	{
		if (str[i] == c)
		{
			return i;
		}
	}

	// In case c == 0 (find the length of the string).
	if (c == '\0')
	{
		return i;
	}
	return -1; // Not found.
}

// ========================================================
// strFindFirst():
// ========================================================

int strFindFirst(const char * str, const char * substring, const uint substringLen)
{
	DEBUG_CHECK(str       != nullptr);
	DEBUG_CHECK(substring != nullptr);

	if (*str == '\0' || *substring == '\0')
	{
		return -1;
	}

	// If the substring doesn't start with a null char, then its length can't be zero.
	DEBUG_CHECK(substringLen != 0);

	for (int i = 0; str[i] != '\0'; ++i)
	{
		if (str[i] == *substring && strCmpN(&str[i], substring, substringLen) == 0)
		{
			return i;
		}
	}
	return -1; // Not found.
}

// ========================================================
// strFindLast():
// ========================================================

int strFindLast(const char * str, const char c)
{
	DEBUG_CHECK(str != nullptr);

	int lastIndex = -1, i = 0;
	for (; str[i] != '\0'; ++i)
	{
		if (str[i] == c)
		{
			lastIndex = i;
			// Continue till the last occurrence...
		}
	}

	// In case c == 0 (find the length of the string).
	if (c == '\0')
	{
		lastIndex = i;
	}
	return lastIndex;
}

// ========================================================
// strFindLast():
// ========================================================

int strFindLast(const char * str, const char * substring, const uint substringLen)
{
	DEBUG_CHECK(str       != nullptr);
	DEBUG_CHECK(substring != nullptr);

	if (*str == '\0' || *substring == '\0')
	{
		return -1;
	}

	// If the substring doesn't start with a null char, then its length can't be zero.
	DEBUG_CHECK(substringLen != 0);

	int lastIndex = -1;
	for (int i = 0; str[i] != '\0'; ++i)
	{
		if (str[i] == *substring && strCmpN(&str[i], substring, substringLen) == 0)
		{
			lastIndex = i;
			// Continue till the last occurrence...
		}
	}
	return lastIndex;
}

// ========================================================
// strFindFirstMatching():
// ========================================================

uint strFindFirstMatching(const char * str, const char * charSet)
{
	DEBUG_CHECK(str     != nullptr);
	DEBUG_CHECK(charSet != nullptr);

	const char * startPtr = str;
	while (*str != '\0')
	{
		const char * setPtr;
		for (setPtr = charSet; *setPtr != '\0'; ++setPtr)
		{
			if (*str == *setPtr)
			{
				break;
			}
		}
		if (*setPtr != '\0')
		{
			break;
		}
		++str;
	}

	return static_cast<uint>(str - startPtr);
}

// ========================================================
// strCopy():
// ========================================================

uint strCopy(char * RESTRICT dest, const uint destSizeInChars, const char * RESTRICT source)
{
	DEBUG_CHECK(dest   != nullptr);
	DEBUG_CHECK(source != nullptr);
	DEBUG_CHECK(destSizeInChars != 0);

	// Copy until the end of source or until we run out of space in dest:
	char * RESTRICT ptr = dest;
	uint charsAvailable = destSizeInChars;
	while ((*ptr++ = *source++) && --charsAvailable != 0)
	{
	}

	// Truncate on overflow:
	if (charsAvailable == 0)
	{
		*(--ptr) = '\0';
		return static_cast<uint>(ptr - dest);
	}

	// Return the number of chars written to dest (not counting the null terminator).
	return static_cast<uint>(ptr - 1 - dest);
}

// ========================================================
// strCopyN():
// ========================================================

uint strCopyN(char * RESTRICT dest, const uint destSizeInChars,
              const char * RESTRICT source, const uint charsToCopy)
{
	DEBUG_CHECK(dest   != nullptr);
	DEBUG_CHECK(source != nullptr);
	DEBUG_CHECK(destSizeInChars != 0);

	if (charsToCopy == 0)
	{
		return 0;
	}

	// Copy until the end of source or until `charsToCopy` have been
	// copied, whichever comes first. Also halt and truncate the string
	// if `destSizeInChars` is exceeded.
	//
	char * RESTRICT ptr = dest;
	uint charsAvailable = destSizeInChars, charsCopied = 0;
	for (;;)
	{
		if (!(*ptr++ = *source++) || --charsAvailable == 0)
		{
			break;
		}
		if (++charsCopied == charsToCopy)
		{
			*ptr++ = '\0';
			break;
		}
	}

	// Truncate on overflow:
	if (charsAvailable == 0)
	{
		*(--ptr) = '\0';
		return static_cast<uint>(ptr - dest);
	}

	// Return the number of chars written to dest (not counting the null terminator).
	return static_cast<uint>(ptr - 1 - dest);
}

// ========================================================
// strErase():
// ========================================================

uint strErase(char * str, const uint strLen, const uint startIndex, const int charsToErase)
{
	DEBUG_CHECK(str != nullptr);
	if (*str == '\0')
	{
		DEBUG_CHECK(strLen == 0); // This would indicate a parameter mismatch if not true.
		return strLen;
	}

	// If the string is not empty, a length must be provided.
	DEBUG_CHECK(strLen != 0);
	DEBUG_CHECK(startIndex < strLen && "Index out of range!");

	// Do nothing in this case.
	if (charsToErase == 0)
	{
		return strLen;
	}

	// Erase ALL till the end of the string, starting from `startIndex`,
	// if `charsToErase` is not in range.
	if (charsToErase < 0 || static_cast<uint>(charsToErase) >= strLen)
	{
		std::memset(&str[startIndex], 0, strLen - startIndex);
		return strLen - startIndex; // New length.
	}

	// Erase `charsToErase` chars from an arbitrary position by shifting to the left:
	const uint charsToMove = strLen - (startIndex + charsToErase) + 1; // Including the null terminator (+1)
	std::memmove(&str[startIndex], &str[startIndex + charsToErase], charsToMove);
	return strLen - charsToErase; // New length.
}

// ========================================================
// strReplace():
// ========================================================

void strReplace(char * str, const uint strLen, const uint startIndex,
                const int charsToReplace, const char replaceWith)
{
	DEBUG_CHECK(str != nullptr);
	if (*str == '\0')
	{
		DEBUG_CHECK(strLen == 0); // This would indicate a parameter mismatch if not true.
		return;
	}

	// If the string is not empty, a length must be provided.
	DEBUG_CHECK(strLen != 0);
	DEBUG_CHECK(startIndex < strLen && "Index out of range!");

	// Do nothing in this case.
	if (charsToReplace == 0)
	{
		return;
	}

	// Fill until end with the given char, starting at pos:
	if (charsToReplace < 0 || static_cast<uint>(charsToReplace) >= strLen)
	{
		std::memset(&str[startIndex], replaceWith, strLen - startIndex);
		return;
	}

	const int charsLeft = static_cast<int>(strLen - startIndex);
	std::memset(&str[startIndex], replaceWith, (charsToReplace <= charsLeft) ? charsToReplace : charsLeft);
}

// ========================================================
// strReplace():
// ========================================================

void strReplace(char * str, const uint strLen, const char * strToReplace,
                const char * strReplaceWith, const uint charsToReplace)
{
	DEBUG_CHECK(charsToReplace != 0);

	if (*str == '\0' || *strToReplace == '\0' || *strReplaceWith == '\0')
	{
		DEBUG_CHECK(strLen == 0); // This would indicate a parameter mismatch if not true.
		return;
	}

	// Find every occurrence of the substring to be replaced:
	for (uint i = 0; i < strLen;)
	{
		if (str[i] != strToReplace[0])
		{
			++i; continue;
		}
		if (strCmpN(&str[i], strToReplace, charsToReplace) != 0)
		{
			++i; continue;
		}
		// Found a match, replace it:
		std::memmove(&str[i], strReplaceWith, charsToReplace);
		// We can skip over the replaced substring now:
		i += charsToReplace;
	}
}

// ========================================================
// strTokenize():
// ========================================================

char * strTokenize(char * RESTRICT str, const char * RESTRICT delimiters, char ** remaining)
{
	DEBUG_CHECK(remaining != nullptr);
	DEBUG_CHECK(str != nullptr || *remaining != nullptr);
	DEBUG_CHECK(delimiters != nullptr);

	// If `str` is null, continue with previous string:
	if (str == nullptr)
	{
		str = *remaining;
	}

	while (*str != '\0')
	{
		const char * RESTRICT ctl = delimiters;
		for (; *ctl != '\0' && *ctl != *str; ++ctl)
		{
		}
		if (*ctl == '\0')
		{
			break;
		}
		++str;
	}

	char * RESTRICT token = str;

	// Find the end of the token.
	// If it is not the end of the string, put a null char there.
	for (; *str != '\0'; ++str)
	{
		const char * RESTRICT ctl = delimiters;
		for (; *ctl != '\0' && *ctl != *str; ++ctl)
		{
		}
		if (*ctl != '\0')
		{
			*str++ = '\0';
			break;
		}
	}

	// Update remaining string and determine if a token has been found:
	*remaining = str;
	return (token == str) ? nullptr : token;
}

// ========================================================
// strRTrim():
// ========================================================

uint strRTrim(char * str, uint strLen)
{
	DEBUG_CHECK(str != nullptr);
	if (*str == '\0')
	{
		DEBUG_CHECK(strLen == 0); // This would indicate a parameter mismatch if not true.
		return strLen;
	}

	char * ptr = str + (strLen - 1);
	while (strLen != 0 && ctype::isSpace(*ptr))
	{
		*ptr-- = '\0', strLen--;
	}

	return strLen;
}

// ========================================================
// strLTrim():
// ========================================================

uint strLTrim(char * str, uint strLen)
{
	DEBUG_CHECK(str != nullptr);
	if (*str == '\0')
	{
		DEBUG_CHECK(strLen == 0); // This would indicate a parameter mismatch if not true.
		return strLen;
	}

	char * ptr = strSkipWhiteSpaces(str);
	const std::ptrdiff_t displacement = ptr - str;

	if (displacement > 0) // Any white space to the left?
	{
		strLen -= displacement;
		if (strLen != 0)
		{
			std::memmove(str, ptr, strLen);
		}
		str[strLen] = '\0';
	}

	return strLen;
}

// ========================================================
// strToUpper():
// ========================================================

char * strToUpper(char * str)
{
	DEBUG_CHECK(str != nullptr);
	for (char * ptr = str; *ptr != '\0'; ++ptr)
	{
		*ptr = ctype::toUpper(*ptr);
	}
	return str;
}

// ========================================================
// strToLower():
// ========================================================

char * strToLower(char * str)
{
	DEBUG_CHECK(str != nullptr);
	for (char * ptr = str; *ptr != '\0'; ++ptr)
	{
		*ptr = ctype::toLower(*ptr);
	}
	return str;
}

// ========================================================
// strHash():
// ========================================================

uint32 strHash(const char * str)
{
	DEBUG_CHECK(str != nullptr);

	// Simple and fast One-at-a-Time (OAT) hash algorithm:
	// http://en.wikipedia.org/wiki/Jenkins_hash_function
	//
	uint32 h = 0;
	while (*str != '\0')
	{
		h += *str++;
		h += (h << 10);
		h ^= (h >>  6);
	}
	h += (h <<  3);
	h ^= (h >> 11);
	h += (h << 15);

	return h;
}

// ========================================================
// strHashIgnoreCase():
// ========================================================

uint32 strHashIgnoreCase(const char * str)
{
	DEBUG_CHECK(str != nullptr);

	// Simple and fast One-at-a-Time (OAT) hash algorithm:
	// http://en.wikipedia.org/wiki/Jenkins_hash_function
	//
	uint32 h = 0;
	while (*str != '\0')
	{
		h += ctype::toLower(*str++);
		h += (h << 10);
		h ^= (h >>  6);
	}
	h += (h <<  3);
	h ^= (h >> 11);
	h += (h << 15);

	return h;
}

// ========================================================
// strSkipWhiteSpaces():
// ========================================================

char * strSkipWhiteSpaces(char * str)
{
	DEBUG_CHECK(str != nullptr);
	while (*str != '\0')
	{
		if (!ctype::isSpace(*str))
		{
			break;
		}
		++str;
	}
	return str;
}

// ========================================================
// strSkipWhiteSpaces():
// ========================================================

const char * strSkipWhiteSpaces(const char * str)
{
	DEBUG_CHECK(str != nullptr);
	while (*str != '\0')
	{
		if (!ctype::isSpace(*str))
		{
			break;
		}
		++str;
	}
	return str;
}

// ========================================================
// strAllocate():
// ========================================================

char * strAllocate(const uint lengthNotIncludingTerminator)
{
	DEBUG_CHECK(lengthNotIncludingTerminator != 0);
	return new char[lengthNotIncludingTerminator + 1];
}

// ========================================================
// strClone():
// ========================================================

char * strClone(const char * source, const uint * optionalSrcLength)
{
	DEBUG_CHECK(source != nullptr);

	const uint length = (optionalSrcLength != nullptr) ? *optionalSrcLength : strLength(source);
	char * clone = strAllocate(length);

	std::memcpy(clone, source, length);
	clone[length] = '\0';

	return clone;
}

// ========================================================
// strDeallocate():
// ========================================================

void strDeallocate(char * str)
{
	delete[] str;
}

// ========================================================
// strPrintF():
// ========================================================

#if (ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE && ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC)
	// Suppress "format string is not a string literal" on GCC and Clang.
	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wformat-nonliteral"
#endif // ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE && ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC

int strPrintF(char * buf, const uint bufSize, const char * format, ...)
{
	DEBUG_CHECK(format  != nullptr);
	DEBUG_CHECK(buf     != nullptr);
	DEBUG_CHECK(bufSize != 0);

	va_list vaList;
	va_start(vaList, format);
	const int result = strVPrintF(buf, bufSize, format, vaList);
	va_end(vaList);

	return result;
}

// ========================================================
// strVPrintF():
// ========================================================

int strVPrintF(char * buf, uint bufSize, const char * format, va_list vaList)
{
	DEBUG_CHECK(format  != nullptr);
	DEBUG_CHECK(buf     != nullptr);
	DEBUG_CHECK(bufSize != 0);

	// `bufSize` is:
	// Maximum number of chars to be used in the input buffer.
	// The generated string has a length of at most bufSize - 1 chars,
	// leaving space for the additional null terminator.

	const int result = std::vsnprintf(buf, bufSize, format, vaList);
	if (result < 0)
	{
		DEBUG_CHECK(false && "std::vsnprintf() failed with negative result!");
		clearPodArray(buf, bufSize); // Clear the string.
		return result;
	}
	if (static_cast<uint>(result) >= bufSize)
	{
		DEBUG_CHECK(false && "std::vsnprintf() overflowed the input buffer!");
		buf[bufSize - 1] = '\0'; // Truncate the string.
		return result;
	}

	buf[result] = '\0'; // Ensure null terminated.
	return result;
}

#if (ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE && ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC)
	#pragma GCC diagnostic pop
#endif // ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE && ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC

// Local helpers:
namespace
{

// ========================================================
// strFromIntHelper() [LOCAL]:
// ========================================================

bool strFromIntHelper(uint64 val, char * buf, const uint bufSize, uint base, const int isNeg)
{
	DEBUG_CHECK(buf != nullptr);

	if (bufSize < (isNeg ? 3 : 2))
	{
		buf[0] = '\0';
		return false;
	}
	if (base < 2 || base > 36)
	{
		buf[0] = '\0';
		return false;
	}

	char * ptr = buf;
	char * firstDigit;
	uint digitVal = 0;
	uint length   = 0;

	if (base == 16)
	{
		// Add a '0x' in front of the hexadecimal values:
		*ptr++ = '0';
		*ptr++ = 'x';
		length += 2;
	}
    else if (isNeg && base == 10)
	{
		// Negative decimal, so output '-' and negate:
		*ptr++ = '-';
		length++;
		val = static_cast<uint64>(-static_cast<int64>(val));
	}

	firstDigit = ptr; // Save pointer to first digit to reverse the string later.
	do
	{
		digitVal = static_cast<uint>(val % base);
		val /= base;

		// Convert to ASCII and store:
		if (digitVal > 9)
		{
			*ptr++ = static_cast<char>((digitVal - 10) + 'A'); // A letter
		}
		else
		{
			*ptr++ = static_cast<char>(digitVal + '0'); // A digit
		}
		++length;
	}
	while (val > 0 && length < bufSize);

	// Check for buffer overflows:
	if (length >= bufSize)
	{
		DEBUG_CHECK(false && "Buffer overflow!");
		buf[0] = '\0';
		return false;
	}

	// Terminate string. `ptr` points to last digit.
	*ptr-- = '\0';

	// We now have the digits of the number in the buffer,
	// but in reverse order. So reverse the string now.
	do
	{
		char tmp = *ptr;
		*ptr = *firstDigit;
		*firstDigit = tmp;
		--ptr, ++firstDigit;
	}
	while (firstDigit < ptr);

	return true;
}

} // namespace {}

// ========================================================
// strFromInt():
// ========================================================

bool strFromInt(const int32 val, char * buf, const uint bufSize, const int base)
{
	uint64 tmp;
	int isNeg;

	if (base == 10)
	{
		isNeg = (val < 0) ? 1 : 0;
		tmp   = static_cast<uint64>(val);
	}
	else
	{
		isNeg = 0;
		tmp   = static_cast<uint64>(val & uint32Max);
	}

	// strFromIntHelper() does further error checking.
	return strFromIntHelper(tmp, buf, bufSize, base, isNeg);
}

// ========================================================
// strFromInt():
// ========================================================

bool strFromInt(const uint32 val, char * buf, const uint bufSize, const int base)
{
	const uint64 tmp = (base == 10) ? static_cast<uint64>(val) : static_cast<uint64>(val & uint32Max);
	return strFromIntHelper(tmp, buf, bufSize, base, 0);
}

// ========================================================
// strFromInt():
// ========================================================

bool strFromInt(const int64 val, char * buf, const uint bufSize, const int base)
{
	return strFromIntHelper(static_cast<uint64>(val), buf, bufSize,
			base, (base == 10 && val < 0) ? 1 : 0);
}

// ========================================================
// strFromInt():
// ========================================================

bool strFromInt(const uint64 val, char * buf, const uint bufSize, const int base)
{
	return strFromIntHelper(val, buf, bufSize, base, 0);
}

// ========================================================
// strFromFloat():
// ========================================================

#if (ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE && ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC)
	// Suppress "format string is not a string literal" on GCC and Clang.
	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wformat-nonliteral"
#endif // ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE && ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC

bool strFromFloat(const float val, char * buf, const uint bufSize, const int decimalDigits)
{
	const char * fmt;
	char fmtBuf[32] = {0};

	if (decimalDigits >= 0 && decimalDigits <= 6 /* FLT_DIG */)
	{
		strPrintF(fmtBuf, arrayLength(fmtBuf), "%%.%df", decimalDigits);
		fmt = fmtBuf;
	}
	else
	{
		fmt = "%f"; // Default precision.
	}

	return strPrintF(buf, bufSize, fmt, val) > 0;
}

// ========================================================
// strFromFloat():
// ========================================================

bool strFromFloat(const double val, char * buf, const uint bufSize, const int decimalDigits)
{
	const char * fmt;
	char fmtBuf[32] = {0};

	if (decimalDigits >= 0 && decimalDigits <= 10 /* DBL_DIG */)
	{
		strPrintF(fmtBuf, arrayLength(fmtBuf), "%%.%dlf", decimalDigits);
		fmt = fmtBuf;
	}
	else
	{
		fmt = "%lf"; // Default precision.
	}

	return strPrintF(buf, bufSize, fmt, val) > 0;
}

#if (ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE && ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC)
	#pragma GCC diagnostic pop
#endif // ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE && ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC

// ========================================================
// strToInt():
// ========================================================

bool strToInt(const char * numPtr, char ** endPtr, const int base, int32 & result)
{
	DEBUG_CHECK(numPtr != nullptr);

	if (endPtr != nullptr)
	{
		*endPtr = const_cast<char *>(numPtr);
	}

	if (base < 0 || base == 1 || 36 < base)
	{
		return false; // Wrong base!
	}

	char * ep;
	result = static_cast<int32>(std::strtol(numPtr, &ep, base));

	if (endPtr != nullptr)
	{
		*endPtr = ep;
	}

	return (ep != numPtr) ? true : false;
}

// ========================================================
// strToInt():
// ========================================================

bool strToInt(const char * numPtr, char ** endPtr, const int base, uint32 & result)
{
	DEBUG_CHECK(numPtr != nullptr);

	if (endPtr != nullptr)
	{
		*endPtr = const_cast<char *>(numPtr);
	}

	if (base < 0 || base == 1 || 36 < base)
	{
		return false; // Wrong base!
	}

	char * ep;
	result = static_cast<uint32>(std::strtoul(numPtr, &ep, base));

	if (endPtr != nullptr)
	{
		*endPtr = ep;
	}

	return (ep != numPtr) ? true : false;
}

// ========================================================
// strToInt():
// ========================================================

bool strToInt(const char * numPtr, char ** endPtr, const int base, int64 & result)
{
	// We can use the unsigned function:
	uint64 tmp;
	const bool r = strToInt(numPtr, endPtr, base, tmp);

	// Also check for integer overflow:
	result = (tmp == uint64Max) ? int64Max : static_cast<int64>(tmp);
	return r;
}

// ========================================================
// strToInt():
// ========================================================

bool strToInt(const char * numPtr, char ** endPtr, int base, uint64 & result)
{
	static const char nDigits[37] =
	{
		0 , 0 , 65, 41, 33, 28, 25, 23, 22, 21,
		20, 19, 18, 18, 17, 17, 17, 16, 16, 16,
		15, 15, 15, 15, 14, 14, 14, 14, 14, 14,
		14, 13, 13, 13, 13, 13, 13
	};
	static const char sDigits[] = "0123456789abcdefghijklmnopqrstuvwxyz";

	DEBUG_CHECK(numPtr != nullptr);

	if (endPtr != nullptr)
	{
		*endPtr = const_cast<char *>(numPtr);
	}

	if (base < 0 || base == 1 || 36 < base)
	{
		return false; // Wrong base!
	}

	const char * sc, * sd;
	const char * s1, * s2;

	for (sc = numPtr; ctype::isSpace(*sc); ++sc)
	{
		// Skip leading white spaces.
	}

	char sign = static_cast<char>((*sc == '-' || *sc == '+') ? *sc++ : '+');

	// Guess numerical type:
	if (0 < base)
	{
		// Hexadecimal string, strip 0x or 0X:
		if (base == 16 && *sc == '0' && (sc[1] == 'x' || sc[1] == 'X'))
		{
			sc += 2;
		}
	}
	else if (*sc != '0')
	{
		base = 10;
	}
	else if (sc[1] == 'x' || sc[1] == 'X')
	{
		base = 16, sc += 2;
	}
	else
	{
		base = 8;
	}

	for (s1 = sc; *sc == '0'; ++sc)
	{
		// Skip leading zeros.
	}

	char dig = 0;
	uint64 x = 0, y = 0;

	// Accumulate the digits:
	for (s2 = sc; (sd = reinterpret_cast<char *>(std::memchr(sDigits, ctype::toLower(*sc), base))) != nullptr; ++sc, y = x)
	{
		dig = static_cast<char>(sd - sDigits); // Save it for overflow checking.
		x = x * base + dig;
	}

	// Check string validity:
	if (s1 == sc)
	{
		DEBUG_CHECK(false && "Internal error in strToInt()!");
		return false;
	}

	const std::ptrdiff_t n = sc - s2 - nDigits[base];
	if (n < 0)
	{
		// Dummy statement.
	}
	else if (0 < n || x < x - dig || (x - dig) / base != y)
	{
		// Overflow:
		x = uint64Max, sign = '+';
	}

	// Get final value:
	if (sign == '-')
	{
		x = 0 - x;
	}
	if (endPtr != nullptr)
	{
		*endPtr = const_cast<char *>(sc);
	}

	result = x;
	return true;
}

// ========================================================
// strToFloat():
// ========================================================

bool strToFloat(const char * numPtr, char ** endPtr, float & result)
{
	DEBUG_CHECK(numPtr != nullptr);

	char * ep;
	result = static_cast<float>(std::strtod(numPtr, &ep));

	if (endPtr != nullptr)
	{
		*endPtr = ep;
	}

	return (ep != numPtr) ? true : false;
}

// ========================================================
// strToFloat():
// ========================================================

bool strToFloat(const char * numPtr, char ** endPtr, double & result)
{
	DEBUG_CHECK(numPtr != nullptr);

	char * ep;
	result = static_cast<double>(std::strtod(numPtr, &ep));

	if (endPtr != nullptr)
	{
		*endPtr = ep;
	}

	return (ep != numPtr) ? true : false;
}

// ========================================================
// strToFloatFast():
// ========================================================

float strToFloatFast(const char * numPtr, char ** endPtr)
{
	DEBUG_CHECK(numPtr != nullptr);

	int sign;
	int f = 0, m = 0, d = 1;
	const char * s = numPtr;

	if (*s == '-')
	{
		sign = -1;
		++s;
	}
	else
	{
		sign = 1;
		if (*s == '+')
		{
			++s;
		}
	}

	// Get the integer part:
	while (*s >= '0' && *s <= '9')
	{
		f = (*s - '0') + f * 10;
		++s;
	}

	// Get the digits after the dot (fractional part), if any:
	if (*s == '.')
	{
		for (++s; (*s >= '0' && *s <= '9'); ++s)
		{
			m = (*s - '0') + m * 10;
			d *= 10;
		}
	}

	if (endPtr != nullptr)
	{
		*endPtr = const_cast<char *>(s);
	}

	return sign * (f + static_cast<float>(m) / d);
}

// ========================================================
// strTrimTrailingFloatZeros():
// ========================================================

char * strTrimTrailingFloatZeros(char * str, uint * newLen)
{
	DEBUG_CHECK(str != nullptr);

	uint n = 0;
	for (char * ptr = str; *ptr != '\0'; ++ptr, ++n)
	{
		if (*ptr == '.')
		{
			while (*++ptr) // Find the end of the string.
			{
				++n;
			}
			++n;

			while (*--ptr == '0') // Remove trailing zeros.
			{
				*ptr = '\0', --n;
			}

			if (*ptr == '.') // If the dot was left alone at the end, remove it too.
			{
				*ptr = '\0', --n;
			}

			break;
		}
	}

	if (newLen != nullptr)
	{
		*newLen = n;
	}

	return str;
}

} // namespace core {}
} // namespace atlas {}
