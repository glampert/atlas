
// ================================================================================================
// -*- C++ -*-
// File: dynamic_string.cpp
// Author: Guilherme R. Lampert
// Created on: 18/12/14
// Brief: Dynamically resizeable string type. `String` is the go-to string type for most uses.
// ================================================================================================

#include "atlas/core/strings/dynamic_string.hpp"
#include <cstring>

namespace atlas
{
namespace core
{

// This is not mandatory for proper functioning of `String`,
// but can increase its runtime performance on most modern CPUs.
// "memcopies" are faster if properly aligned.
COMPILE_TIME_CHECK(sizeof(String) % 16 == 0, "Ideally, String size should be a multiple of 16!");

// ========================================================
// String::String():
// ========================================================

String::String()
	: StringBase(smallStr, SmallStrSize, 0)
{
	// Empty string.
}

// ========================================================
// String::String():
// ========================================================

String::String(const uint sizeToReserve)
	: StringBase(smallStr, SmallStrSize, 0)
{
	allocate(sizeToReserve, /* preserveString = */ false);
}

// ========================================================
// String::String():
// ========================================================

String::String(uint count, char fillWith)
	: StringBase(smallStr, SmallStrSize, 0)
{
	DEBUG_CHECK(count != 0);
	allocate(count, /* preserveString = */ false);
	if (fillWith != '\0')
	{
		length = count;
		fill(fillWith);
	}
}

// ========================================================
// String::~String():
// ========================================================

String::~String()
{
	freeDynamicMemory();
}

// ========================================================
// String::assign():
// ========================================================

String & String::assign(const char * str)
{
	DEBUG_CHECK(str != nullptr);
	const uint count = strLength(str);
	if (count != 0)
	{
		allocate(count, /* preserveString = */ false);
		length = strCopy(cStr, capacity, str);
	}
	return *this;
}

// ========================================================
// String::assign():
// ========================================================

String & String::assign(const char * str, const uint count)
{
	DEBUG_CHECK(str != nullptr);
	if (*str != '\0' && count != 0)
	{
		allocate(count, /* preserveString = */ false);
		length = strCopyN(cStr, capacity, str, count);
	}
	return *this;
}

// ========================================================
// String::assign():
// ========================================================

String & String::assign(const char * str, const uint startIndex, const uint count)
{
	DEBUG_CHECK(str != nullptr);
	if (*str != '\0' && count != 0)
	{
		allocate(count, /* preserveString = */ false);
		length = strCopyN(cStr, capacity, &str[startIndex], count);
	}
	return *this;
}

// ========================================================
// String::append():
// ========================================================

String & String::append(const char * str, const uint startIndex, const uint count)
{
	DEBUG_CHECK(str != nullptr);
	if (*str != '\0' && count != 0)
	{
		allocate(length + count, /* preserveString = */ true);
		std::memmove(&cStr[length], &str[startIndex], count);
		length += count;
		cStr[length] = '\0';
	}
	return *this;
}

// ========================================================
// String::extractSubstring():
// ========================================================

String String::extractSubstring(const uint startIndex, const int count) const
{
	String result;
	if (!isEmpty() && count != 0)
	{
		const uint charsAvailable = length - startIndex;
		const uint charsToCopy = (count > 0 && static_cast<uint>(count) <= charsAvailable) ? count : charsAvailable;
		result.assign(*this, startIndex, charsToCopy);
	}
	return result;
}

// ========================================================
// String::extractSubstring():
// ========================================================

String String::extractSubstring(const StringBase & substring) const
{
	String result;
	const int subIndex = firstIndexOf(substring);
	if (subIndex >= 0)
	{
		result.assign(cStr, subIndex, substring.getLength());
	}
	return result;
}

// ========================================================
// String::extractSubstring():
// ========================================================

String String::extractSubstring(const char * substring) const
{
	String result;
	const int subIndex = firstIndexOf(substring);
	if (subIndex >= 0)
	{
		result.assign(cStr, subIndex, strLength(substring));
	}
	return result;
}

// ========================================================
// String::padRight():
// ========================================================

String String::padRight(const uint padWanted, const char padChar) const
{
	if (length >= padWanted)
	{
		return *this;
	}

	int padCount = padWanted - length;
	String paddingStr(padCount);
	while (padCount--)
	{
		paddingStr.pushBack(padChar);
	}

	return (*this) + paddingStr;
}

// ========================================================
// String::padLeft():
// ========================================================

String String::padLeft(const uint padWanted, const char padChar) const
{
	if (length >= padWanted)
	{
		return *this;
	}

	int padCount = padWanted - length;
	String paddingStr(padCount);
	while (padCount--)
	{
		paddingStr.pushBack(padChar);
	}

	return paddingStr + (*this);
}

// ========================================================
// String::pushBack():
// ========================================================

void String::pushBack(const char c)
{
	if (c == '\0')
	{
		return;
	}

	allocate(length + 1, /* preserveString = */ true);

	cStr[length++] = c;
	cStr[length] = '\0';
}

// ========================================================
// String::popBack():
// ========================================================

void String::popBack()
{
	DEBUG_CHECK(!isEmpty());
	cStr[--length] = '\0';
}

// ========================================================
// String::allocate():
// ========================================================

void String::allocate(const uint newLen, const bool preserveString)
{
	DEBUG_CHECK(isValid());
	if (newLen < capacity)
	{
		return; // Doesn't shrink
	}

	const uint totalChars = newLen + ReallocExtra;
	char * newMemory = strAllocate(totalChars); // An implicit +1 here

	if (length != 0 && preserveString)
	{
		// Copy current to new storage if required:
		std::memcpy(newMemory, cStr, length + 1);
	}
	else
	{
		// Discard current string:
		newMemory[0] = '\0', length = 0;
	}

	freeDynamicMemory();
	cStr     = newMemory;
	capacity = totalChars + 1;
}

// ========================================================
// String::deallocate():
// ========================================================

void String::deallocate()
{
	freeDynamicMemory();
	cStr     = smallStr;
	cStr[0]  = '\0';
	capacity = SmallStrSize;
	length   = 0;
}

// ========================================================
// String::transferTo():
// ========================================================

void String::transferTo(String & dest)
{
	DEBUG_CHECK(dest.isEmpty() && "Destination string contents would be lost!");

	// The small string buffer cannot be transfered since
	// it is part of the string object. In this case, this
	// is the same as a normal assignment / string copy.
	if (!hasDynamicMemory())
	{
		dest = *this;
		clear();
	}
	else // Some dynamic memory is allocated for `this`:
	{
		if (dest.hasDynamicMemory())
		{
			// Dest should be empty, but it might have dynamic
			// memory assigned to it, so free first.
			dest.freeDynamicMemory();
		}

		dest.cStr     = cStr;
		dest.capacity = capacity;
		dest.length   = length;

		// Rest this to point to its small string buffer.
		cStr     = smallStr;
		cStr[0]  = '\0';
		capacity = SmallStrSize;
		length   = 0;
	}
}

// ========================================================
// String::hasDynamicMemory():
// ========================================================

bool String::hasDynamicMemory() const
{
	return cStr != smallStr;
}

// ========================================================
// String::freeDynamicMemory():
// ========================================================

void String::freeDynamicMemory()
{
	// Free if not pointing to the small static buffer.
	if (cStr != smallStr && cStr != nullptr)
	{
		strDeallocate(cStr);
	}
}

// ================================================================================================
// String splitting:
// ================================================================================================

namespace
{

// ========================================================
// splitStringHelper() [LOCAL]:
// ========================================================

template<class TOKEN_HANDLER>
uint splitStringHelper(const char * RESTRICT sourcePtr, const char * RESTRICT delimiterPtr, TOKEN_HANDLER & tokenHandler)
{
	const char * RESTRICT sourceStartPtr = sourcePtr;
	uint tokensFound = 0;

	// Loop and find all matches:
	for (;;)
	{
		// Find the beginning of the next token.
		while (*sourcePtr != '\0')
		{
			const char * RESTRICT ctl = delimiterPtr;
			for (; *ctl != '\0' && *ctl != *sourcePtr; ++ctl)
			{
			}
			if (*ctl == '\0')
			{
				break;
			}
			++sourcePtr;
		}

		// Remember token start position. End will be searched in the sequence.
		const char * RESTRICT tokenStartPtr = sourcePtr;
		const char * RESTRICT tokenEndPtr   = nullptr;

		// Find the end of the token. If it is not the end of the string, save the pointer.
		for (; *sourcePtr != '\0'; ++sourcePtr)
		{
			const char * RESTRICT ctl = delimiterPtr;
			for (; *ctl != '\0' && *ctl != *sourcePtr; ++ctl)
			{
			}
			if (*ctl != '\0')
			{
				tokenEndPtr = sourcePtr;
				++sourcePtr;
				break;
			}
		}

		if (tokenEndPtr != nullptr)
		{
			++tokensFound;
			tokenHandler(static_cast<uint>(tokenStartPtr - sourceStartPtr),
			             static_cast<uint>(tokenEndPtr   - sourceStartPtr));
		}
		else
		{
			// Residual substring at the end with no delimiters following?
			if (*tokenStartPtr != '\0')
			{
				tokenEndPtr = tokenStartPtr;
				while (*tokenEndPtr != '\0') { ++tokenEndPtr; }

				++tokensFound;
				tokenHandler(static_cast<uint>(tokenStartPtr - sourceStartPtr),
				             static_cast<uint>(tokenEndPtr   - sourceStartPtr));
			}

			break; // Reached end without finding another token; stop.
		}
	}

	return tokensFound;
}

} // namespace {}

// ========================================================
// split():
// ========================================================

uint split(const String & source, const String & delimiters, Array<String::SubIndexes> & subIndexes)
{
	// splitStringHelper() expects non-aliasing strings!
	DEBUG_CHECK(source.getCStr() != delimiters.getCStr() && "This function can't take the same string as source and delimiters!");
	DEBUG_CHECK(source.isValid() && delimiters.isValid());

	if (source.isEmpty() || delimiters.isEmpty())
	{
		subIndexes.clear();
		return 0;
	}

	struct TokenHandler
	{
		Array<String::SubIndexes> & subTokens;

		TokenHandler(Array<String::SubIndexes> & dest)
			: subTokens(dest) { }

		void operator() (const uint startIndex, const uint endIndex)
		{
			const String::SubIndexes substrIndex = { startIndex, endIndex };
			subTokens.pushBack(substrIndex);
		}
	};

	TokenHandler tkHander(subIndexes);
	return splitStringHelper(source.getCStr(), delimiters.getCStr(), tkHander);
}

// ========================================================
// split():
// ========================================================

uint split(const String & source, const String & delimiters, Array<String> & substrings)
{
	// splitStringHelper() expects non-aliasing strings!
	DEBUG_CHECK(source.getCStr() != delimiters.getCStr() && "This function can't take the same string as source and delimiters!");
	DEBUG_CHECK(source.isValid() && delimiters.isValid());

	if (source.isEmpty() || delimiters.isEmpty())
	{
		substrings.clear();
		return 0;
	}

	struct TokenHandler
	{
		Array<String> & subTokens;
		const String  & srcStr;

		TokenHandler(Array<String> & dest, const String & src)
			: subTokens(dest), srcStr(src) { }

		void operator() (const uint startIndex, const uint endIndex)
		{
			const uint charCount = endIndex - startIndex;
			subTokens.pushBack(srcStr.extractSubstring(startIndex, (charCount != 0) ? charCount : 1));
		}
	};

	TokenHandler tkHander(substrings, source);
	return splitStringHelper(source.getCStr(), delimiters.getCStr(), tkHander);
}

} // namespace core {}
} // namespace atlas {}
