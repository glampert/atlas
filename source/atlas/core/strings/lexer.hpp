
// ================================================================================================
// -*- C++ -*-
// File: lexer.hpp
// Author: Guilherme R. Lampert
// Created on: 08/08/15
// Brief: Lexicographical scanner (Lexer) compatible with C-like languages. Adapted from idLexer.
// ================================================================================================

#ifndef ATLAS_CORE_STRINGS_LEXER_HPP
#define ATLAS_CORE_STRINGS_LEXER_HPP

#include "atlas/core/strings/dynamic_string.hpp"
#include "atlas/core/utils/default_log.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// class Token:
// ========================================================

// Tokens are the basic output of the Lexer. Each token consist
// of a string and possibly a numerical value scanned from the
// string. Tokens will not allocate memory for small strings, due
// to the Small String Optimization done in the String class.
//
// Notes: It is fine to copy/assign Tokens, but they should not
// be kept around after the Lexer that emitted the Token is gone,
// otherwise, you risk getting invalid references to to internal
// whitespace markers. If you plan on reusing tokens, remember
// to `clear()` them before storing for future reuse.
//
// This class was based on idToken, from Doom 3. Check the end
// of this file for a copyright notice from id Software.
//
class Token final
{
public:

	// Main token type:
	struct Type
	{
		enum Enum
		{
			Undefined   = 0, // Initial value.
			String      = 1,
			Literal     = 2,
			Number      = 3,
			NameId      = 4,
			Punctuation = 5
		};
	};

	// Number/value sub types and flags:
	struct Flag
	{
		enum Enum
		{
			Integer           = 0x00001, // Integer number.
			Decimal           = 0x00002, // Decimal number.
			Hexadecimal       = 0x00004, // Hexadecimal number.
			Octal             = 0x00008, // Octal number.
			Binary            = 0x00010, // Binary number.
			Long              = 0x00020, // Long int.
			Unsigned          = 0x00040, // Unsigned int.
			Float             = 0x00080, // Floating point number.
			SinglePrecision   = 0x00100, // Float.
			DoublePrecision   = 0x00200, // Double.
			ExtendedPrecision = 0x00400, // Long double.
			Infinite          = 0x00800, // Infinite 1.#INF.
			Indefinite        = 0x01000, // Indefinite 1.#IND.
			NaN               = 0x02000, // Not-a-number.
			IpAddress         = 0x04000, // IP address.
			IpPort            = 0x08000, // IP port.
			ValuesValid       = 0x10000  // Set if `ulongValue` and `doubleValue` are up-to-date.
		};
	};

	Token();

	// Operator overloads:
	bool operator == (const char * cstr)  const;
	bool operator == (const String & str) const;
	bool operator != (const char * cstr)  const;
	bool operator != (const String & str) const;
	char operator [] (uint index) const; // Access a char in the underlaying text string.

	// Flag queries:
	int getType()    const;
	int getSubType() const;

	// Value access:
	int    getIntValue()     const;    // Signed integer value of `Token::Type::Number`.
	float  getFloatValue()   const;    // Float value of `Token::Type::Number`.
	double getDoubleValue()  const;    // Double value of `Token::Type::Number`.
	ulong  getULongValue()   const;    // Unsigned integer value of `Token::Type::Number`.
	uint   getLength()       const;    // Length in chars of the raw token text.
	const String & getText() const;    // Get raw textual value of the token.

	// Misc helpers:
	void transferTo(String & dest);    // Transfers this token's text to the output string, clearing it.
	int whiteSpaceBeforeToken() const; // Returns length of whitespace before token.
	void clearTokenWhiteSpace();       // Forget whitespace before token.
	void clear();                      // Reset the token to its initial states.

private:

	// Let the Lexer fiddle with the token data freely.
	friend class Lexer;

	void append(char c);
	void computeNumberValue() const;

	// Lexing states (pointers to Lexer text):
	const char * whiteSpaceStartPtr; // Start of whitespace before token, only used by the Lexer.
	const char * whiteSpaceEndPtr;   // End of whitespace before token, only used by the Lexer.

	// State flags:
	mutable int type;                // Token type (Type enum).
	mutable int subtype;             // Token sub type (Flag enum).
	int lineNumber;                  // Line in script the token was on.
	int linesCrossed;                // Number of lines crossed in whitespace before token.

	// Cached values:
	mutable ulong  ulongValue;       // Integer value; initially zero.
	mutable double doubleValue;      // Floating point value; initially zero.

	// Raw token text:
	String text;
};

// ========================================================
// class Lexer:
// ========================================================

// Lexicographical scanner, based on the idLexer class from Doom 3.
// Supports punctuation for lexing of C-like languages.
//
// Does not use memory allocations during scan. The Lexer uses no memory
// allocations at all if a source is loaded with `initFromFileInMemory()`.
// However, tokens may still allocate memory for large strings.
//
// A number directly following the escape character '\' in a string is
// assumed to be in decimal format instead of octal. Binary numbers of
// the form 0b... or 0B... can also be used.
//
// Check the end of this file for Doom 3/id Software copyright notice.
//
class Lexer final
	: private NonCopyable
{
public:

	// Miscellaneous lexing flags. Can be ORed together.
	struct Flag
	{
		enum Enum
		{
			NoErrors                   = BIT(0),  // Don't print any errors.
			NoWarnings                 = BIT(1),  // Don't print any warnings.
			NoFatalErrors              = BIT(2),  // Errors aren't fatal. By default all errors are fatal.
			NoStringConcat             = BIT(3),  // Multiple strings separated by whitespace are not concatenated.
			NoStringEscapeChars        = BIT(4),  // No escape characters allowed inside strings.
			AllowPathNames             = BIT(5),  // Allow path separators in names, e.g.: `my/path/based/name`.
			AllowNumberNames           = BIT(6),  // Allow names to start with a number, e.g.: `3lite`.
			AllowIpAddresses           = BIT(7),  // Allow IP addresses to be parsed as numbers.
			AllowFloatExceptions       = BIT(8),  // Allow float exceptions like 1.#INF or 1.#IND to be parsed.
			AllowMultiCharLiterals     = BIT(9),  // Allow multi-character literals.
			AllowBackslashStringConcat = BIT(10), // Allow multiple strings separated by '\' to be concatenated.
			OnlyStrings                = BIT(11)  // Scan as whitespace delimited strings (quoted strings keep quotes).
		};
	};

	// For all the following constructors the log stream is optional.
	// If null, the Lexer will use the default library log. Flags are
	// also optional and may be zero.
	explicit Lexer(LogStream * log = nullptr, int logLvl = LogLevel::Info);

	// Init by loading a file from the file system.
	Lexer(const String & filename, uint flags = 0,
	      LogStream * log = nullptr, int logLvl = LogLevel::Info);

	// Init with a memory buffer. Same as `initFromFileInMemory()`. Does not take ownership of `ptr`.
	Lexer(const char * ptr, uint length, const String & name, uint flags = 0,
	      uint startLine = 1, LogStream * log = nullptr, int logLvl = LogLevel::Info);

	// Might allocate memory if loading from file.
	// The destructor cleans it up.
	~Lexer();

	// Load a script from the given file and set flags.
	bool initFromFile(const String & filename, uint flags = 0);

	// Load a script from the given memory with the given length and a specified line offset,
	// so source strings extracted from a file can still refer to proper line numbers in the file.
	// Note: `ptr` is expected to point at a valid C string: ptr[length] == '\0'!
	// The Lexer WILL NOT take ownership of the passed pointer. Caller is responsible for freeing it!
	bool initFromFileInMemory(const char * ptr, uint length, const String & name, uint flags = 0, uint startLine = 1);

	// Returns true if a script/text-source is already loaded.
	bool isInitialized() const;

	// Free the script and any associated data.
	void deallocate();

	// Reset the Lexer to the beginning of its text input.
	// This will also clear the error and warning flags. Lexing flags stay the same.
	void reset();

	// Read the next token (or return a cached token).
	bool readNextToken(Token & tokenOut);

	// Read a token only if on the same line.
	bool readNextTokenOnLine(Token & tokenOut);

	// Expect a certain token, reads the token when available.
	bool expectTokenString(const char * string);
	bool expectTokenString(const char * string, Token & tokenOut);

	// Expect a certain token type, e.g.: number, name/id, string, etc.
	bool expectTokenType(int type, int subtype, Token & tokenOut);
	bool expectAnyToken(Token & tokenOut);

	// Returns true when the token is available.
	bool checkTokenString(const char * string);
	bool checkTokenString(const char * string, Token & tokenOut);

	// Returns true an reads the token when a token with the given type is available.
	bool checkTokenType(int type, int subtype, Token & tokenOut);

	// Returns true if the next token equals the given string
	// but does not remove the token from the source.
	bool peekTokenString(const char * string);

	// Returns true if the next token equals the given type but
	// does not remove the token from the source.
	bool peekTokenType(int type, int subtype, Token & tokenOut);

	// Skip tokens until the given token string is read.
	bool skipUntilString(const char * string);

	// Skip the rest of the current line.
	bool skipRestOfLine();

	// Skip a braced section.
	bool skipBracedSection(bool parseFirstBrace = true);

	// Skips spaces, tabs, C-style multi-line comments, C++ comments, etc.
	// Returns false if there is no token left to read.
	bool skipWhiteSpace(bool currentLine);

	// Unread the given token / put it back.
	void unreadToken(const Token & tokenIn);

	// Returns the rest of the current line. Returns a pointer to the C-string of `out`.
	const char * readRestOfLine(String & out);

	// Read a signed integer token. Returns zero on error and logs a message.
	int scanInt();

	// Read a boolean token. Returns false on error and logs a message.
	bool scanBool();

	// Read a floating point number. If `errorFlag` is null, a non-numeric token will issue
	// a Lexer.error(). If it isn't null, it will issue a Lexer.warning() and set `errorFlag` to true.
	float  scanFloat(bool * errorFlag = nullptr);
	double scanDouble(bool * errorFlag = nullptr);

	// Parse matrices/nested-tuples of floats:
	bool scanFloatMatrix1(int x, float * m);
	bool scanFloatMatrix2(int y, int x, float * m);
	bool scanFloatMatrix3(int z, int y, int x, float * m);

	// Read a braced section into a string.
	const char * scanBracedSection(String & out);

	// Read a braced section into a string, maintaining indents and newlines.
	const char * scanBracedSectionExact(String & out, int tabs = -1);

	// Read the rest of the line.
	const char * scanRestOfLine(String & out);

	// Pulls the entire line, including the '\n' at the end.
	const char * scanCompleteLine(String & out);

	// Retrieves the whitespace characters before the last
	// read token. Return the length of the output string.
	uint getLastWhiteSpace(String & whiteSpace) const;

	// Returns start index into text buffer of last whitespace.
	uint getLastWhiteSpaceStart() const;

	// Returns end index into text buffer of last whitespace.
	uint getLastWhiteSpaceEnd() const;

	// Set/Get Lexer flags.
	void setFlags(uint newFlags);
	uint getFlags() const;

	// Set/Get the log stream used to print errors/warnings.
	void setLog(LogStream & log);
	LogStream & getLog() const;

	// Set/Get the verbosity level of log used to print errors/warnings. `LogLevel::Info` by default.
	void setLogVerbosityLevel(int newLevel);
	int getLogVerbosityLevel() const;

	// Returns true if at the end of the file/input string.
	bool isAtEndOfFile() const;

	// Returns the current filename.
	const String & getFileName() const;

	// Get offset in script/source file.
	uint getFileOffset() const;

	// Returns the current line number in the input source.
	uint getLineNum() const;

	// Print an error message to the log stream. This function always
	// returns false, to allow for `return error("foo bar");` usage.
	bool error(const String & errorMessage);

	// Print a warning message to the log stream.
	void warning(const String & warningMessage);

	// Returns true if `error()` was called, even with `Flag::NoFatalErrors` or `Flag::NoErrors` set.
	bool hadErrors() const;

	// Returns true if `warning()` was called at least one.
	bool hadWarnings() const;

	// Returns a pointer to the punctuation string with the given id.
	static String getPunctuationFromId(int id);

	// Get the id for the given punctuation string.
	static int getPunctuationId(const char * punctuationString);

private:

	struct Punctuation
	{
		const char * chars; // Punctuation character(s).
		int id;             // Punctuation id/code.
	};

	// Shared punctuation tables for C/C++ and compatible script formats:
	static const Punctuation punctuationInfo[];
	static const int punctuationTable[];
	static const int nextPunctuation[];

	// Per-instance data:
	const char * bufferHeadPtr;      // Buffer containing the script; owned by Lexer if allocated == true.
	const char * scriptPtr;          // Current pointer in the script.
	const char * endPtr;             // Pointer to the end of the script.
	const char * lastScriptPtr;      // Script pointer before reading last token.
	const char * whiteSpaceStartPtr; // Start of last white space.
	const char * whiteSpaceEndPtr;   // End of last white space.
	LogStream  * logStr;             // Error/warning log. If null, uses the default library log.
	int          logLevel;           // Log verbosity level. Initially `LogLevel::Info`.
	uint         scriptLength;       // Length of the script in characters, not counting a null terminator.
	uint         lineNumber;         // Current line in script.
	uint         lastLine;           // Line before reading token.
	uint         lexFlags;           // Several script/lexing flags.
	bool         tokenAvailable;     // Set by unreadToken().
	bool         initialized;        // Set when a script file is loaded from file or memory
	bool         allocated;          // True if bufferHeadPtr memory was allocated by Lexer. False if external.
	bool         hadError;           // set by Lexer.error(), even if the errors are suppressed.
	bool         hadWarning;         // Set by Lexer.warning(), even if warnings are suppressed.
	Token        leftoverToken;      // Available token from unreadToken(). May be empty.
	String       scriptFilename;     // File name of the script being scanned. Used for error reporting.

private:

	// Internal helpers:
	void commonInit();
	bool readWhiteSpace();
	bool readEscapeCharacter(char & ch);
	bool readString(Token & tokenOut, int quote);
	bool readName(Token & tokenOut);
	bool readNumber(Token & tokenOut);
	bool readPunctuation(Token & tokenOut);
	bool checkString(const char * str) const;
};

// ================================================================================================
// Token inline methods:
// ================================================================================================

// ========================================================
// Token::Token():
// ========================================================

inline Token::Token()
{
	clear();
}

// ========================================================
// Token::operator == :
// ========================================================

inline bool Token::operator == (const char * cstr) const
{
	return text == cstr;
}

// ========================================================
// Token::operator == :
// ========================================================

inline bool Token::operator == (const String & str) const
{
	return text == str;
}

// ========================================================
// Token::operator != :
// ========================================================

inline bool Token::operator != (const char * cstr) const
{
	return text != cstr;
}

// ========================================================
// Token::operator != :
// ========================================================

inline bool Token::operator != (const String & str) const
{
	return text != str;
}

// ========================================================
// Token::operator []:
// ========================================================

inline char Token::operator [] (const uint index) const
{
	return text[index];
}

// ========================================================
// Token::getType()
// ========================================================

inline int Token::getType() const
{
	return type;
}

// ========================================================
// Token::getSubType()
// ========================================================

inline int Token::getSubType() const
{
	return subtype;
}

// ========================================================
// Token::getIntValue():
// ========================================================

inline int Token::getIntValue() const
{
	return static_cast<int>(getULongValue());
}

// ========================================================
// Token::getFloatValue():
// ========================================================

inline float Token::getFloatValue() const
{
	return static_cast<float>(getDoubleValue());
}

// ========================================================
// Token::getDoubleValue():
// ========================================================

inline double Token::getDoubleValue() const
{
	if (type != Type::Number)
	{
		return 0.0;
	}
	if (!(subtype & Flag::ValuesValid))
	{
		computeNumberValue();
	}
	return doubleValue;
}

// ========================================================
// Token::getULongValue():
// ========================================================

inline ulong Token::getULongValue() const
{
	if (type != Type::Number)
	{
		return 0;
	}
	if (!(subtype & Flag::ValuesValid))
	{
		computeNumberValue();
	}
	return ulongValue;
}

// ========================================================
// Token::getLength():
// ========================================================

inline uint Token::getLength() const
{
	return text.getLength();
}

// ========================================================
// Token::getText():
// ========================================================

inline const String & Token::getText() const
{
	return text;
}

// ========================================================
// Token::whiteSpaceBeforeToken():
// ========================================================

inline int Token::whiteSpaceBeforeToken() const
{
	return static_cast<int>(whiteSpaceEndPtr - whiteSpaceStartPtr);
}

// ========================================================
// Token::transferTo():
// ========================================================

inline void Token::transferTo(String & dest)
{
	dest.clear();
	text.transferTo(dest);
}

// ========================================================
// Token::clearTokenWhiteSpace():
// ========================================================

inline void Token::clearTokenWhiteSpace()
{
	whiteSpaceStartPtr = nullptr;
	whiteSpaceEndPtr   = nullptr;
	linesCrossed       = 0;
}

// ========================================================
// Token::append():
// ========================================================

inline void Token::append(const char c)
{
	text.pushBack(c);
}

// ================================================================================================
// Lexer inline methods:
// ================================================================================================

// ========================================================
// Lexer::isInitialized():
// ========================================================

inline bool Lexer::isInitialized() const
{
	return initialized;
}

// ========================================================
// Lexer::getFileName():
// ========================================================

inline const String & Lexer::getFileName() const
{
	return scriptFilename;
}

// ========================================================
// Lexer::getFileOffset():
// ========================================================

inline uint Lexer::getFileOffset() const
{
	return static_cast<uint>(scriptPtr - bufferHeadPtr);
}

// ========================================================
// Lexer::getLineNum():
// ========================================================

inline uint Lexer::getLineNum() const
{
	return lineNumber;
}

// ========================================================
// Lexer::setFlags():
// ========================================================

inline void Lexer::setFlags(const uint newFlags)
{
	lexFlags = newFlags;
}

// ========================================================
// Lexer::getFlags():
// ========================================================

inline uint Lexer::getFlags() const
{
	return lexFlags;
}

// ========================================================
// Lexer::setLog():
// ========================================================

inline void Lexer::setLog(LogStream & log)
{
	logStr = &log;
}

// ========================================================
// Lexer::getLog():
// ========================================================

inline LogStream & Lexer::getLog() const
{
	return (logStr != nullptr) ? *logStr : core::getLog();
}

// ========================================================
// Lexer::setLogVerbosityLevel():
// ========================================================

inline void Lexer::setLogVerbosityLevel(const int newLevel)
{
	logLevel = newLevel;
}

// ========================================================
// Lexer::getLogVerbosityLevel():
// ========================================================

inline int Lexer::getLogVerbosityLevel() const
{
	return logLevel;
}

// ========================================================
// Lexer::isAtEndOfFile():
// ========================================================

inline bool Lexer::isAtEndOfFile() const
{
	return scriptPtr >= endPtr;
}

// ========================================================
// Lexer::hadErrors():
// ========================================================

inline bool Lexer::hadErrors() const
{
	return hadError;
}

// ========================================================
// Lexer::hadWarnings():
// ========================================================

inline bool Lexer::hadWarnings() const
{
	return hadWarning;
}

// ========================================================
// Lexer::getLastWhiteSpaceStart():
// ========================================================

inline uint Lexer::getLastWhiteSpaceStart() const
{
	return static_cast<uint>(whiteSpaceStartPtr - bufferHeadPtr);
}

// ========================================================
// Lexer::getLastWhiteSpaceEnd():
// ========================================================

inline uint Lexer::getLastWhiteSpaceEnd() const
{
	return static_cast<uint>(whiteSpaceEndPtr - bufferHeadPtr);
}

} // namespace core {}
} // namespace atlas {}

/*
================================================================================================
            -- ORIGINAL COPYRIGHT NOTICE FROM DOOM 3 BFG (Lexer.h and Token.h) --

Doom 3 BFG Edition GPL Source Code.
Copyright (C) 1993-2012 id Software LLC, a ZeniMax Media company.

This file is part of the Doom 3 BFG Edition GPL Source Code ("Doom 3 BFG Edition Source Code").

Doom 3 BFG Edition Source Code is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later version.

Doom 3 BFG Edition Source Code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Doom 3 BFG Edition Source Code. If not, see <http://www.gnu.org/licenses/>.

In addition, the Doom 3 BFG Edition Source Code is also subject to certain additional terms.
You should have received a copy of these additional terms immediately following the terms and
conditions of the GNU General Public License which accompanied the Doom 3 BFG Edition Source
Code. If not, please request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional terms, you may
contact in writing id Software LLC, c/o ZeniMax Media Inc., Suite 120, Rockville,
Maryland 20850 USA.

================================================================================================
*/

#endif // ATLAS_CORE_STRINGS_LEXER_HPP
