
// ================================================================================================
// -*- C++ -*-
// File: string_base.hpp
// Author: Guilherme R. Lampert
// Created on: 17/12/14
// Brief: Basic string type with common string manipulation methods and algorithms.
// ================================================================================================

#ifndef ATLAS_CORE_STRINGS_STRING_BASE_HPP
#define ATLAS_CORE_STRINGS_STRING_BASE_HPP

#include "atlas/core/strings/c_string.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// class StringBase:
// ========================================================

class StringBase
{
public:

	// Start and end indexes for a substring.
	struct SubIndexes
	{
		uint start;
		uint end;
	};

	// Comparison operators: Equal/Not equal (case-sensitive!)
	bool operator == (const StringBase & other) const;
	bool operator != (const StringBase & other) const;
	bool operator == (const char * str) const;
	bool operator != (const char * str) const;

	// Comparison operators: Less-equal/Greater-equal
	bool operator <= (const StringBase & other) const;
	bool operator >= (const StringBase & other) const;
	bool operator <= (const char * str) const;
	bool operator >= (const char * str) const;

	// Comparison operators: Less/Greater
	bool operator < (const StringBase & other) const;
	bool operator > (const StringBase & other) const;
	bool operator < (const char * str) const;
	bool operator > (const char * str) const;

	// Mutable and immutable array access.
	// Bounds checked on a debug build. No checking on release setting.
	char & operator [] (uint index);
	char   operator [] (uint index) const;

	// First and last chars of string.
	// Both returning '\0' if string is empty.
	char & front();
	char & back();
	char front() const;
	char back()  const;

	// strcmp()-like case-sensitive comparison. Returns:
	// < 0 if this <  other;
	// = 0 if this == other;
	// > 0 if this >  other;
	int compare(const StringBase & other) const;
	int compare(const char * str) const;
	int compare(const StringBase & other, uint count) const;
	int compare(const char * str, uint count) const;

	// Compare strings like `compare()`, but ignores character case. E.g.: "A" equals "a".
	int compareIgnoreCase(const StringBase & other) const;
	int compareIgnoreCase(const char * str) const;
	int compareIgnoreCase(const StringBase & other, uint count) const;
	int compareIgnoreCase(const char * str, uint count) const;

	// Same as `operator ==` or `compare(other) == 0`.
	bool equals(const StringBase & other) const;
	bool equals(const char * str) const;

	// Compares two strings for equality ignoring character case. E.g.: "AAA" equals "aaa".
	bool equalsIgnoreCase(const StringBase & other) const;
	bool equalsIgnoreCase(const char * str) const;

	// Test if this string starts with a certain character sequence (prefix).
	bool startsWith(const StringBase & prefix) const;
	bool startsWith(const char * prefix) const;

	// Same as `startsWith()` but ignores character case.
	bool startsWithIgnoreCase(const StringBase & prefix) const;
	bool startsWithIgnoreCase(const char * prefix) const;

	// Test if this string ends with a certain character sequence (suffix).
	bool endsWith(const StringBase & suffix) const;
	bool endsWith(const char * suffix) const;

	// Same as `endsWith()` but ignores character case.
	bool endsWithIgnoreCase(const StringBase & suffix) const;
	bool endsWithIgnoreCase(const char * suffix) const;

	// Find index inside this string of the FIRST occurrence of a char or substring.
	// Returns the index to the start of the entity if found, -1 otherwise.
	int firstIndexOf(char c) const;
	int firstIndexOf(const StringBase & substring) const;
	int firstIndexOf(const char * substring) const;

	// Same as `firstIndexOf()` but ignores character case.
	int firstIndexOfIgnoreCase(const StringBase & substring) const;
	int firstIndexOfIgnoreCase(const char * substring) const;

	// Find index inside this string of the LAST occurrence of a char or substring.
	// Returns the index to the start of the entity if found, -1 otherwise.
	int lastIndexOf(char c) const;
	int lastIndexOf(const StringBase & substring) const;
	int lastIndexOf(const char * substring) const;

	// Same as `lastIndexOf()` but ignores character case.
	int lastIndexOfIgnoreCase(const StringBase & substring) const;
	int lastIndexOfIgnoreCase(const char * substring) const;

	// Finds in this string the first occurrence of any character
	// in the `charSet` string. Returns -1 if none of characters in
	// `charSet` are found inside this string.
	int findAnyMatch(const StringBase & charSet) const;
	int findAnyMatch(const char * charSet) const;

	// Searches for any occurrences of `strToReplace` inside this string and replaces those with `strReplaceWith`.
	// `charsToReplace` is the number of characters that will be taken into account in the search->replace process.
	// If any of the inputs is greater than that, the extra length is ignored. The search is always case-sensitive.
	StringBase & replace(const StringBase & strToReplace, const StringBase & strReplaceWith, uint charsToReplace);
	StringBase & replace(const char * strToReplace, const char * strReplaceWith, uint charsToReplace);

	// Replaces a sequence of characters in this string by a single character repeated `count` times.
	// `startIndex` must be in a valid range inside this string. The char pointed by `startIndex` is included.
	// If `count` is less than 0 or greater than the string's length, fills the string with `replaceWith` chars
	// starting at `startIndex` until the end. Does nothing if the string is empty or if `count` is zero.
	StringBase & replace(uint startIndex, int count, char replaceWith);

	// Erases a part of the string's content, shortening its length and filling the end with null chars.
	// `startIndex` must be in a valid range inside this string. The char pointed by `startIndex` is included.
	// If `count` is less than 0 or greater than the string's length, erases until the end, starting at `startIndex`.
	// Does nothing if the string is empty or if `count` is zero.
	StringBase & erase(uint startIndex, int count);

	// Fills the string with a given char. If the char is '\0', the string's length
	// is also cleared. Otherwise, its length remains unchanged. Does nothing is the string is empty.
	StringBase & fill(char fillWith);

	// Truncates the string's length, without deallocating memory.
	// Does nothing if `newLen` is greater or equal than the current length.
	StringBase & truncate(uint newLen);

	// Right trims (right-side white spaces removed).
	StringBase & trimRight();

	// Left trims (left-side white spaces removed).
	StringBase & trimLeft();

	// Trims the string (left and right white spaces removed).
	StringBase & trim();

	// Make the whole string uppercase.
	StringBase & toUpper();

	// Make the whole string lowercase.
	StringBase & toLower();

	// Sets the string to an empty (but valid) string, without deallocating memory.
	void clear();

	// Checks if string length is zero.
	bool isEmpty() const;

	// Test if the string's internal state is valid.
	bool isValid() const;

	// Get a pointer to the underlaying null-terminated char array.
	const char * getCStr() const;

	// Get the number of characters currently allocated.
	uint getCapacity() const;

	// Get the length of the string in characters.
	uint getLength() const;

	// Computes the (case-sensitive) hash value of this string using a default hash function.
	uint32 hash() const;

	// Computes the hash value of this string ignoring case, as if the whole string was lowercase.
	uint32 hashIgnoreCase() const;

	// Parse/scan numbers from the string.
	// For `scanInt()`, `base` is optional. It will be inferred from the string if you pass 0.
	// If `firstUnread` is not null, it will return the index of the fist character that was
	// not converted, which will be the length of the string if the whole string consisted of a valid number.
	// Output is cleared to zero if the conversion fails for whatever reasons.
	bool scanInt(int32  & intVal, int base = 0, uint * firstUnread = nullptr) const;
	bool scanInt(uint32 & intVal, int base = 0, uint * firstUnread = nullptr) const;
	bool scanInt(int64  & intVal, int base = 0, uint * firstUnread = nullptr) const;
	bool scanInt(uint64 & intVal, int base = 0, uint * firstUnread = nullptr) const;
	bool scanFloat(float  & floatVal, uint * firstUnread = nullptr) const;
	bool scanFloat(double & floatVal, uint * firstUnread = nullptr) const;

	// Tries to scan a tuple of floating-point or integer values from a string.
	// The expected format of the string is, for example:
	//   "(1,2,3)" or "( 1.1, 2.2, 3.3 )"
	// With any number of spaces accepted between the values.
	// The string must begin with a parenthesis '(' and end with the corresponding closing one.
	// `elementCount` must be either 2, 3 or 4. Larger sizes are currently not supported.
	// Output is cleared to zeros if the conversion fails for whatever reasons.
	bool scanIntTuple(int * tuple, uint elementCount) const;
	bool scanFloatTuple(float * tuple, uint elementCount) const;

	// Tries to scan a boolean value from the string.
	// Strings that evaluate to true  : "true",  "yes", "1", "on"  (case independent).
	// Strings that evaluate to false : "false", "no",  "0", "off" (case independent).
	// Any other string evaluates to false and returns false.
	// The boolean string may be followed by other non-boolean characters
	// (we test the above strings as prefixes). May also be preceded by white spaces.
	bool scanBool(bool & boolVal) const;

public:

	// String iterator type.
	// Can have `const` applied to it;
	// Can be used with "foreach"-style iteration.
	class Iterator final
	{
	public:

		Iterator();
		Iterator(const StringBase * owner, int dir, int startIndex);

		char & operator* ();
		char   operator* () const;

		const Iterator & operator-- ()    const; // pre-decrement
		      Iterator   operator-- (int) const; // post-decrement
		const Iterator & operator++ ()    const; // pre-increment
		      Iterator   operator++ (int) const; // post-increment

		bool operator == (const Iterator & other) const;
		bool operator != (const Iterator & other) const;

		// Get the current iteration index, which can be used with string's [] operator.
		int getCurrentIndex() const;

		// Get a reference to the string that owns this iterator.
		const StringBase * getString() const;

	private:

		StringBase * myString;
		mutable int index;
		int direction;
	};

	// Iterators to the beginning of the string and one-past-the-end:
	Iterator begin();
	Iterator end();
	const Iterator begin() const;
	const Iterator end()   const;

	// Reverse iteration: Increasing the iterator moves it towards the beginning of the string.
	Iterator revBegin();
	Iterator revEnd();
	const Iterator revBegin() const;
	const Iterator revEnd()   const;

protected:

	// Default constructor is a no-op.
	// It sets all fields to zero/null.
	StringBase();

	// Construct with all parameters. For use by child classes only.
	StringBase(char * newCStr, uint newCap, uint newLen);

	// Protected and non-virtual.
	// StringBase instances are not to be directly deleted.
	~StringBase() DEFAULTED_METHOD;

	// Data:
	char * cStr;     // Pointer to null-terminated char buffer. Set to null by the default constructor.
	uint   capacity; // Total size in chars of `cStr`, including a null terminator.
	uint   length;   // Chars used so far, not counting the null terminator. Always < `capacity`.

private:

	// Value copy not permitted at this level.
	StringBase(const StringBase &) DELETED_METHOD;
	StringBase & operator = (const StringBase &) DELETED_METHOD;
};

// ================================================================================================
// StringBase inline methods and related helpers:
// ================================================================================================

//
// Global begin/end ranges compatible with "foreach"-style iteration:
//
inline StringBase::Iterator begin(StringBase & str)
{ return str.begin(); }

inline StringBase::Iterator end(StringBase & str)
{ return str.end(); }

inline StringBase::Iterator revBegin(StringBase & str)
{ return str.revBegin(); }

inline StringBase::Iterator revEnd(StringBase & str)
{ return str.revEnd(); }

//
// Global begin/end ranges (const) compatible with "foreach"-style iteration:
//
inline const StringBase::Iterator begin(const StringBase & str)
{ return str.begin(); }

inline const StringBase::Iterator end(const StringBase & str)
{ return str.end(); }

inline const StringBase::Iterator revBegin(const StringBase & str)
{ return str.revBegin(); }

inline const StringBase::Iterator revEnd(const StringBase & str)
{ return str.revEnd(); }

// ========================================================
// StringBase::StringBase():
// ========================================================

inline StringBase::StringBase()
	: cStr(nullptr)
	, capacity(0)
	, length(0)
{
	// Not a valid string!
}

// ========================================================
// StringBase::StringBase():
// ========================================================

inline StringBase::StringBase(char * newCStr, uint newCap, uint newLen)
	: cStr(newCStr)
	, capacity(newCap)
	, length(newLen)
{
	if (cStr != nullptr)
	{
		*cStr = '\0';
	}
}

// ========================================================
// StringBase::operator == :
// ========================================================

inline bool StringBase::operator == (const StringBase & other) const
{
	return compare(other) == 0;
}

// ========================================================
// StringBase::operator != :
// ========================================================

inline bool StringBase::operator != (const StringBase & other) const
{
	return compare(other) != 0;
}

// ========================================================
// StringBase::operator == :
// ========================================================

inline bool StringBase::operator == (const char * str) const
{
	return compare(str) == 0;
}

// ========================================================
// StringBase::operator != :
// ========================================================

inline bool StringBase::operator != (const char * str) const
{
	return compare(str) != 0;
}

// ========================================================
// StringBase::operator <= :
// ========================================================

inline bool StringBase::operator <= (const StringBase & other) const
{
	return compare(other) <= 0;
}

// ========================================================
// StringBase::operator >= :
// ========================================================

inline bool StringBase::operator >= (const StringBase & other) const
{
	return compare(other) >= 0;
}

// ========================================================
// StringBase::operator <= :
// ========================================================

inline bool StringBase::operator <= (const char * str) const
{
	return compare(str) <= 0;
}

// ========================================================
// StringBase::operator >= :
// ========================================================

inline bool StringBase::operator >= (const char * str) const
{
	return compare(str) >= 0;
}

// ========================================================
// StringBase::operator < :
// ========================================================

inline bool StringBase::operator < (const StringBase & other) const
{
	return compare(other) < 0;
}

// ========================================================
// StringBase::operator > :
// ========================================================

inline bool StringBase::operator > (const StringBase & other) const
{
	return compare(other) > 0;
}

// ========================================================
// StringBase::operator < :
// ========================================================

inline bool StringBase::operator < (const char * str) const
{
	return compare(str) < 0;
}

// ========================================================
// StringBase::operator > :
// ========================================================

inline bool StringBase::operator > (const char * str) const
{
	return compare(str) > 0;
}

// ========================================================
// StringBase::operator[]:
// ========================================================

inline char & StringBase::operator [] (const uint index)
{
	DEBUG_CHECK(isValid() && "Invalid string!");
	DEBUG_CHECK(index < length && "String index out-of-range!");
	return cStr[index];
}

// ========================================================
// StringBase::operator[]:
// ========================================================

inline char StringBase::operator [] (const uint index) const
{
	DEBUG_CHECK(isValid() && "Invalid string!");
	DEBUG_CHECK(index < length && "String index out-of-range!");
	return cStr[index];
}

// ========================================================
// StringBase::front():
// ========================================================

inline char & StringBase::front()
{
	DEBUG_CHECK(isValid());
	return cStr[0];
}

// ========================================================
// StringBase::back():
// ========================================================

inline char & StringBase::back()
{
	DEBUG_CHECK(isValid());
	return cStr[(length != 0) ? (length - 1) : 0];
}

// ========================================================
// StringBase::front() [const]:
// ========================================================

inline char StringBase::front() const
{
	DEBUG_CHECK(isValid());
	return cStr[0];
}

// ========================================================
// StringBase::back() [const]:
// ========================================================

inline char StringBase::back() const
{
	DEBUG_CHECK(isValid());
	return cStr[(length != 0) ? (length - 1) : 0];
}

// ========================================================
// StringBase::compare():
// ========================================================

inline int StringBase::compare(const StringBase & other) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(other.isValid());
	return strCmp(cStr, other.cStr);
}

// ========================================================
// StringBase::compare():
// ========================================================

inline int StringBase::compare(const char * str) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(str != nullptr);
	return strCmp(cStr, str);
}

// ========================================================
// StringBase::compare():
// ========================================================

inline int StringBase::compare(const StringBase & other, const uint count) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(other.isValid());
	DEBUG_CHECK(count != 0 && count <= other.length);
	return strCmpN(cStr, other.cStr, count);
}

// ========================================================
// StringBase::compare():
// ========================================================

inline int StringBase::compare(const char * str, const uint count) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(str != nullptr && count != 0);
	return strCmpN(cStr, str, count);
}

// ========================================================
// StringBase::compareIgnoreCase():
// ========================================================

inline int StringBase::compareIgnoreCase(const StringBase & other) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(other.isValid());
	return strCmpIgnoreCase(cStr, other.cStr);
}

// ========================================================
// StringBase::compareIgnoreCase():
// ========================================================

inline int StringBase::compareIgnoreCase(const char * str) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(str != nullptr);
	return strCmpIgnoreCase(cStr, str);
}

// ========================================================
// StringBase::compareIgnoreCase():
// ========================================================

inline int StringBase::compareIgnoreCase(const StringBase & other, uint count) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(other.isValid());
	DEBUG_CHECK(count != 0 && count <= other.length);
	return strCmpNIgnoreCase(cStr, other.cStr, count);
}

// ========================================================
// StringBase::compareIgnoreCase():
// ========================================================

inline int StringBase::compareIgnoreCase(const char * str, uint count) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(str != nullptr && count != 0);
	return strCmpNIgnoreCase(cStr, str, count);
}

// ========================================================
// StringBase::equals():
// ========================================================

inline bool StringBase::equals(const StringBase & other) const
{
	return compare(other) == 0;
}

// ========================================================
// StringBase::equals():
// ========================================================

inline bool StringBase::equals(const char * str) const
{
	return compare(str) == 0;
}

// ========================================================
// StringBase::equalsIgnoreCase():
// ========================================================

inline bool StringBase::equalsIgnoreCase(const StringBase & other) const
{
	return compareIgnoreCase(other) == 0;
}

// ========================================================
// StringBase::equalsIgnoreCase():
// ========================================================

inline bool StringBase::equalsIgnoreCase(const char * str) const
{
	return compareIgnoreCase(str) == 0;
}

// ========================================================
// StringBase::startsWith():
// ========================================================

inline bool StringBase::startsWith(const StringBase & prefix) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(prefix.isValid());
	return strStartsWith(cStr, length, prefix.cStr, prefix.length);
}

// ========================================================
// StringBase::startsWith():
// ========================================================

inline bool StringBase::startsWith(const char * prefix) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(prefix != nullptr);
	return strStartsWith(cStr, length, prefix, strLength(prefix));
}

// ========================================================
// StringBase::startsWithIgnoreCase():
// ========================================================

inline bool StringBase::startsWithIgnoreCase(const StringBase & prefix) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(prefix.isValid());
	return strStartsWithIgnoreCase(cStr, length, prefix.cStr, prefix.length);
}

// ========================================================
// StringBase::startsWithIgnoreCase():
// ========================================================

inline bool StringBase::startsWithIgnoreCase(const char * prefix) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(prefix != nullptr);
	return strStartsWithIgnoreCase(cStr, length, prefix, strLength(prefix));
}

// ========================================================
// StringBase::endsWith():
// ========================================================

inline bool StringBase::endsWith(const StringBase & suffix) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(suffix.isValid());
	return strEndsWith(cStr, length, suffix.cStr, suffix.length);
}

// ========================================================
// StringBase::endsWith():
// ========================================================

inline bool StringBase::endsWith(const char * suffix) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(suffix != nullptr);
	return strEndsWith(cStr, length, suffix, strLength(suffix));
}

// ========================================================
// StringBase::endsWithIgnoreCase():
// ========================================================

inline bool StringBase::endsWithIgnoreCase(const StringBase & suffix) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(suffix.isValid());
	return strEndsWithIgnoreCase(cStr, length, suffix.cStr, suffix.length);
}

// ========================================================
// StringBase::endsWithIgnoreCase():
// ========================================================

inline bool StringBase::endsWithIgnoreCase(const char * suffix) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(suffix != nullptr);
	return strEndsWithIgnoreCase(cStr, length, suffix, strLength(suffix));
}

// ========================================================
// StringBase::firstIndexOf():
// ========================================================

inline int StringBase::firstIndexOf(const char c) const
{
	DEBUG_CHECK(isValid());
	return strFindFirst(cStr, c);
}

// ========================================================
// StringBase::firstIndexOf():
// ========================================================

inline int StringBase::firstIndexOf(const StringBase & substring) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(substring.isValid());
	return strFindFirst(cStr, substring.cStr, substring.length);
}

// ========================================================
// StringBase::firstIndexOf():
// ========================================================

inline int StringBase::firstIndexOf(const char * substring) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(substring != nullptr);
	return strFindFirst(cStr, substring, strLength(substring));
}

// ========================================================
// StringBase::lastIndexOf():
// ========================================================

inline int StringBase::lastIndexOf(const char c) const
{
	DEBUG_CHECK(isValid());
	return strFindLast(cStr, c);
}

// ========================================================
// StringBase::lastIndexOf():
// ========================================================

inline int StringBase::lastIndexOf(const StringBase & substring) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(substring.isValid());
	return strFindLast(cStr, substring.cStr, substring.length);
}

// ========================================================
// StringBase::lastIndexOf():
// ========================================================

inline int StringBase::lastIndexOf(const char * substring) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(substring != nullptr);
	return strFindLast(cStr, substring, strLength(substring));
}

// ========================================================
// StringBase::findAnyMatch():
// ========================================================

inline int StringBase::findAnyMatch(const StringBase & charSet) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(charSet.isValid());

	const uint matchIndex = strFindFirstMatching(cStr, charSet.cStr);
	if (matchIndex >= length)
	{
		return -1;
	}
	return static_cast<int>(matchIndex);
}

// ========================================================
// StringBase::findAnyMatch():
// ========================================================

inline int StringBase::findAnyMatch(const char * charSet) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(charSet != nullptr);

	const uint matchIndex = strFindFirstMatching(cStr, charSet);
	if (matchIndex >= length)
	{
		return -1;
	}
	return static_cast<int>(matchIndex);
}

// ========================================================
// StringBase::replace():
// ========================================================

inline StringBase & StringBase::replace(const uint startIndex, const int count, const char replaceWith)
{
	DEBUG_CHECK(isValid());
	strReplace(cStr, length, startIndex, count, replaceWith);
	return *this;
}

// ========================================================
// StringBase::erase():
// ========================================================

inline StringBase & StringBase::erase(const uint startIndex, const int count)
{
	DEBUG_CHECK(isValid());
	length = strErase(cStr, length, startIndex, count);
	return *this;
}

// ========================================================
// StringBase::trimRight():
// ========================================================

inline StringBase & StringBase::trimRight()
{
	DEBUG_CHECK(isValid());
	length = strRTrim(cStr, length);
	return *this;
}

// ========================================================
// StringBase::trimLeft():
// ========================================================

inline StringBase & StringBase::trimLeft()
{
	DEBUG_CHECK(isValid());
	length = strLTrim(cStr, length);
	return *this;
}

// ========================================================
// StringBase::trim():
// ========================================================

inline StringBase & StringBase::trim()
{
	DEBUG_CHECK(isValid());
	length = strRTrim(cStr, length);
	length = strLTrim(cStr, length);
	return *this;
}

// ========================================================
// StringBase::toUpper():
// ========================================================

inline StringBase & StringBase::toUpper()
{
	DEBUG_CHECK(isValid());
	strToUpper(cStr);
	return *this;
}

// ========================================================
// StringBase::toLower():
// ========================================================

inline StringBase & StringBase::toLower()
{
	DEBUG_CHECK(isValid());
	strToLower(cStr);
	return *this;
}

// ========================================================
// StringBase::clear():
// ========================================================

inline void StringBase::clear()
{
	DEBUG_CHECK(cStr != nullptr);
	length = 0;
	*cStr  = '\0';
}

// ========================================================
// StringBase::isEmpty():
// ========================================================

inline bool StringBase::isEmpty() const
{
	return length == 0;
}

// ========================================================
// StringBase::isValid():
// ========================================================

inline bool StringBase::isValid() const
{
	return cStr != nullptr && capacity != 0 && length < capacity;
}

// ========================================================
// StringBase::getCStr():
// ========================================================

inline const char * StringBase::getCStr() const
{
	return cStr;
}

// ========================================================
// StringBase::getCapacity():
// ========================================================

inline uint StringBase::getCapacity() const
{
	return capacity;
}

// ========================================================
// StringBase::getLength():
// ========================================================

inline uint StringBase::getLength() const
{
	return length;
}

// ========================================================
// StringBase::hash():
// ========================================================

inline uint32 StringBase::hash() const
{
	DEBUG_CHECK(isValid());
	return strHash(cStr);
}

// ========================================================
// StringBase::hashIgnoreCase():
// ========================================================

inline uint32 StringBase::hashIgnoreCase() const
{
	DEBUG_CHECK(isValid());
	return strHashIgnoreCase(cStr);
}

// ========================================================
// StringBase::begin():
// ========================================================

inline StringBase::Iterator StringBase::begin()
{
	return Iterator(this, 1, 0);
}

// ========================================================
// StringBase::end():
// ========================================================

inline StringBase::Iterator StringBase::end()
{
	return Iterator(this, 1, length);
}

// ========================================================
// StringBase::begin():
// ========================================================

inline const StringBase::Iterator StringBase::begin() const
{
	return Iterator(this, 1, 0);
}

// ========================================================
// StringBase::end():
// ========================================================

inline const StringBase::Iterator StringBase::end() const
{
	return Iterator(this, 1, length);
}

// ========================================================
// StringBase::revBegin():
// ========================================================

inline StringBase::Iterator StringBase::revBegin()
{
	return Iterator(this, -1, length - 1);
}

// ========================================================
// StringBase::revEnd():
// ========================================================

inline StringBase::Iterator StringBase::revEnd()
{
	return Iterator(this, -1, -1);
}

// ========================================================
// StringBase::revBegin():
// ========================================================

inline const StringBase::Iterator StringBase::revBegin() const
{
	return Iterator(this, -1, length - 1);
}

// ========================================================
// StringBase::revEnd():
// ========================================================

inline const StringBase::Iterator StringBase::revEnd() const
{
	return Iterator(this, -1, -1);
}

// ================================================================================================
// StringBase::Iterator class definition:
// ================================================================================================

// ========================================================
// StringBase::Iterator::Iterator():
// ========================================================

inline StringBase::Iterator::Iterator()
	: myString(nullptr)
	, index(-1)
	, direction(0)
{
	// An invalid iterator.
}

// ========================================================
// StringBase::Iterator::Iterator():
// ========================================================

inline StringBase::Iterator::Iterator(const StringBase * owner, const int dir, const int startIndex)
	: myString(const_cast<StringBase *>(owner))
	, index(startIndex)
	, direction(dir)
{
	DEBUG_CHECK(direction == -1 || direction == 1);
}

// ========================================================
// StringBase::Iterator::operator* :
// ========================================================

inline char & StringBase::Iterator::operator* ()
{
	DEBUG_CHECK(myString != nullptr && myString->isValid() && "Invalid string iterator!");
	DEBUG_CHECK(index >= 0 && index < static_cast<int>(myString->getLength()) && "Invalid string iterator dereference!");
	return (*myString)[index];
}

// ========================================================
// StringBase::Iterator::operator* :
// ========================================================

inline char StringBase::Iterator::operator* () const
{
	DEBUG_CHECK(myString != nullptr && myString->isValid() && "Invalid string iterator!");
	DEBUG_CHECK(index >= 0 && index < static_cast<int>(myString->getLength()) && "Invalid string iterator dereference!");
	return (*myString)[index];
}

// ========================================================
// StringBase::Iterator::operator-- (pre-decrement):
// ========================================================

inline const StringBase::Iterator & StringBase::Iterator::operator-- () const
{
	DEBUG_CHECK(myString != nullptr && myString->isValid() && "Invalid string iterator!");
	DEBUG_CHECK(index > 0 && "Out-of-bounds iterator decrement!");

	index -= direction;
	return *this;
}

// ========================================================
// StringBase::Iterator::operator-- (post-decrement):
// ========================================================

inline StringBase::Iterator StringBase::Iterator::operator-- (int) const
{
	DEBUG_CHECK(myString != nullptr && myString->isValid() && "Invalid string iterator!");
	DEBUG_CHECK(index > 0 && "Out-of-bounds iterator decrement!");

	Iterator tmp(*this);
	index -= direction;
	return tmp; // Return old, pre-decrement value.
}

// ========================================================
// StringBase::Iterator::operator++ (pre-increment):
// ========================================================

inline const StringBase::Iterator & StringBase::Iterator::operator++ () const
{
	DEBUG_CHECK(myString != nullptr && myString->isValid()  && "Invalid string iterator!");
	DEBUG_CHECK(index < static_cast<int>(myString->getLength()) && "Out-of-bounds iterator increment!");

	index += direction;
	return *this;
}

// ========================================================
// StringBase::Iterator::operator++ (post-increment):
// ========================================================

inline StringBase::Iterator StringBase::Iterator::operator++ (int) const
{
	DEBUG_CHECK(myString != nullptr && myString->isValid()  && "Invalid string iterator!");
	DEBUG_CHECK(index < static_cast<int>(myString->getLength()) && "Out-of-bounds iterator increment!");

	Iterator tmp(*this);
	index += direction;
	return tmp; // Return old, pre-increment value.
}

// ========================================================
// StringBase::Iterator::operator == :
// ========================================================

inline bool StringBase::Iterator::operator == (const StringBase::Iterator & other) const
{
	DEBUG_CHECK(myString  == other.myString  && "Iterators belong to different strings!");
	DEBUG_CHECK(direction == other.direction && "Different kind of iterators!");
	return index == other.index;
}

// ========================================================
// StringBase::Iterator::operator != :
// ========================================================

inline bool StringBase::Iterator::operator != (const StringBase::Iterator & other) const
{
	DEBUG_CHECK(myString  == other.myString  && "Iterators belong to different strings!");
	DEBUG_CHECK(direction == other.direction && "Different kind of iterators!");
	return index != other.index;
}

// ========================================================
// StringBase::Iterator::getCurrentIndex():
// ========================================================

inline int StringBase::Iterator::getCurrentIndex() const
{
	return index;
}

// ========================================================
// StringBase::Iterator::getString():
// ========================================================

inline const StringBase * StringBase::Iterator::getString() const
{
	return myString;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_STRINGS_STRING_BASE_HPP
