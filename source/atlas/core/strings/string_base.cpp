
// ================================================================================================
// -*- C++ -*-
// File: string_base.cpp
// Author: Guilherme R. Lampert
// Created on: 17/12/14
// Brief: Basic string type with common string manipulation methods and algorithms.
// ================================================================================================

#include "atlas/core/strings/string_base.hpp"
#include <cstdio>
#include <cstring>

namespace atlas
{
namespace core
{

// ========================================================
// StringBase::truncate():
// ========================================================

StringBase & StringBase::truncate(const uint newLen)
{
	if (newLen >= length)
	{
		return *this;
	}

	length = newLen;
	cStr[length] = '\0';
	return *this;
}

// ========================================================
// StringBase::replace():
// ========================================================

StringBase & StringBase::replace(const StringBase & strToReplace, const StringBase & strReplaceWith, const uint charsToReplace)
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(strToReplace.isValid());
	DEBUG_CHECK(strReplaceWith.isValid());

	DEBUG_CHECK(length >= charsToReplace);
	DEBUG_CHECK(strToReplace.length   >= charsToReplace);
	DEBUG_CHECK(strReplaceWith.length >= charsToReplace);

	strReplace(cStr, length, strToReplace.cStr, strReplaceWith.cStr, charsToReplace);
	return *this;
}

// ========================================================
// StringBase::replace():
// ========================================================

StringBase & StringBase::replace(const char * strToReplace, const char * strReplaceWith, const uint charsToReplace)
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(strToReplace   != nullptr);
	DEBUG_CHECK(strReplaceWith != nullptr);

	DEBUG_CHECK(length >= charsToReplace);
	DEBUG_CHECK(strLength(strToReplace)   >= charsToReplace);
	DEBUG_CHECK(strLength(strReplaceWith) >= charsToReplace);

	strReplace(cStr, length, strToReplace, strReplaceWith, charsToReplace);
	return *this;
}

// ========================================================
// StringBase::firstIndexOfIgnoreCase():
// ========================================================

int StringBase::firstIndexOfIgnoreCase(const StringBase & substring) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(substring.isValid());

	if (isEmpty() || substring.isEmpty())
	{
		return -1;
	}

	for (int i = 0; cStr[i] != '\0'; ++i)
	{
		if (ctype::toLower(cStr[i]) == ctype::toLower(substring[0]))
		{
			if (strCmpNIgnoreCase(&cStr[i], substring.cStr, substring.length) == 0)
			{
				return i;
			}
		}
	}
	return -1; // Not found.
}

// ========================================================
// StringBase::firstIndexOfIgnoreCase():
// ========================================================

int StringBase::firstIndexOfIgnoreCase(const char * substring) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(substring != nullptr);

	if (isEmpty() || *substring == '\0')
	{
		return -1;
	}

	const uint substringLen = strLength(substring);
	for (int i = 0; cStr[i] != '\0'; ++i)
	{
		if (ctype::toLower(cStr[i]) == ctype::toLower(substring[0]))
		{
			if (strCmpNIgnoreCase(&cStr[i], substring, substringLen) == 0)
			{
				return i;
			}
		}
	}
	return -1; // Not found.
}

// ========================================================
// StringBase::lastIndexOfIgnoreCase():
// ========================================================

int StringBase::lastIndexOfIgnoreCase(const StringBase & substring) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(substring.isValid());

	if (isEmpty() || substring.isEmpty())
	{
		return -1;
	}

	int lastIndex = -1;
	for (int i = 0; cStr[i] != '\0'; ++i)
	{
		if (ctype::toLower(cStr[i]) == ctype::toLower(substring[0]))
		{
			if (strCmpNIgnoreCase(&cStr[i], substring.cStr, substring.length) == 0)
			{
				lastIndex = i;
				// Continue till the last occurrence...
			}
		}
	}
	return lastIndex;
}

// ========================================================
// StringBase::lastIndexOfIgnoreCase():
// ========================================================

int StringBase::lastIndexOfIgnoreCase(const char * substring) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(substring != nullptr);

	if (isEmpty() || *substring == '\0')
	{
		return -1;
	}

	int lastIndex = -1;
	const uint substringLen = strLength(substring);
	for (int i = 0; cStr[i] != '\0'; ++i)
	{
		if (ctype::toLower(cStr[i]) == ctype::toLower(substring[0]))
		{
			if (strCmpNIgnoreCase(&cStr[i], substring, substringLen) == 0)
			{
				lastIndex = i;
				// Continue till the last occurrence...
			}
		}
	}
	return lastIndex;
}

// ========================================================
// StringBase::fill():
// ========================================================

StringBase & StringBase::fill(const char fillWith)
{
	DEBUG_CHECK(isValid());

	if (isEmpty())
	{
		return *this;
	}
	if (fillWith == '\0')
	{
		clear();
		return *this;
	}

	std::memset(cStr, fillWith, length);
	cStr[length] = '\0';
	return *this;
}

// ========================================================
// SCAN_INT_IMP() local helper macro:
// ========================================================

#define SCAN_INT_IMP(result, base, firstUnread) \
{ \
	DEBUG_CHECK(isValid()); \
	char * endPtr; \
	if (!strToInt(cStr, &endPtr, (base), (result))) \
	{ \
		(result) = 0; \
		if ((firstUnread) != nullptr) \
		{ \
			(*(firstUnread)) = 0; \
		} \
		return false; \
	} \
	if ((firstUnread) != nullptr) \
	{ \
		(*(firstUnread)) = static_cast<uint>(endPtr - cStr); \
	} \
	return true; \
}

// ========================================================
// SCAN_FLT_IMP() local helper macro:
// ========================================================

#define SCAN_FLT_IMP(result, firstUnread) \
{ \
	DEBUG_CHECK(isValid()); \
	char * endPtr; \
	if (!strToFloat(cStr, &endPtr, (result))) \
	{ \
		(result) = 0; \
		if ((firstUnread) != nullptr) \
		{ \
			(*(firstUnread)) = 0; \
		} \
		return false; \
	} \
	if ((firstUnread) != nullptr) \
	{ \
		(*(firstUnread)) = static_cast<uint>(endPtr - cStr); \
	} \
	return true; \
}

// ========================================================
// StringBase::scanInt():
// ========================================================

bool StringBase::scanInt(int32 & intVal, const int base, uint * firstUnread) const
{
	SCAN_INT_IMP(intVal, base, firstUnread);
}

// ========================================================
// StringBase::scanInt():
// ========================================================

bool StringBase::scanInt(uint32 & intVal, const int base, uint * firstUnread) const
{
	SCAN_INT_IMP(intVal, base, firstUnread);
}

// ========================================================
// StringBase::scanInt():
// ========================================================

bool StringBase::scanInt(int64 & intVal, const int base, uint * firstUnread) const
{
	SCAN_INT_IMP(intVal, base, firstUnread);
}

// ========================================================
// StringBase::scanInt():
// ========================================================

bool StringBase::scanInt(uint64 & intVal, const int base, uint * firstUnread) const
{
	SCAN_INT_IMP(intVal, base, firstUnread);
}

// ========================================================
// StringBase::scanFloat():
// ========================================================

bool StringBase::scanFloat(float & floatVal, uint * firstUnread) const
{
	SCAN_FLT_IMP(floatVal, firstUnread);
}

// ========================================================
// StringBase::scanFloat():
// ========================================================

bool StringBase::scanFloat(double & floatVal, uint * firstUnread) const
{
	SCAN_FLT_IMP(floatVal, firstUnread);
}

// ========================================================
// StringBase::scanIntTuple():
// ========================================================

bool StringBase::scanIntTuple(int * tuple, const uint elementCount) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(tuple != nullptr);
	DEBUG_CHECK(elementCount >= 2 && elementCount <= 4);

	const char * firstNonBlank = strSkipWhiteSpaces(cStr);
	switch (elementCount)
	{
	case 2 :
		if (std::sscanf(firstNonBlank, "(%i,%i)", &tuple[0], &tuple[1]) == 2)
		{
			return true;
		}
		break;
	case 3 :
		if (std::sscanf(firstNonBlank, "(%i,%i,%i)", &tuple[0], &tuple[1], &tuple[2]) == 3)
		{
			return true;
		}
		break;
	case 4 :
		if (std::sscanf(firstNonBlank, "(%i,%i,%i,%i)", &tuple[0], &tuple[1], &tuple[2], &tuple[3]) == 4)
		{
			return true;
		}
		break;
	default :
		UNREACHABLE();
	} // switch (elementCount)

	// Clear the output on error:
	for (uint i = 0; i < elementCount; ++i) { tuple[i] = 0; }
	return false;
}

// ========================================================
// StringBase::scanFloatTuple():
// ========================================================

bool StringBase::scanFloatTuple(float * tuple, const uint elementCount) const
{
	DEBUG_CHECK(isValid());
	DEBUG_CHECK(tuple != nullptr);
	DEBUG_CHECK(elementCount >= 2 && elementCount <= 4);

	const char * firstNonBlank = strSkipWhiteSpaces(cStr);
	switch (elementCount)
	{
	case 2 :
		if (std::sscanf(firstNonBlank, "(%f,%f)", &tuple[0], &tuple[1]) == 2)
		{
			return true;
		}
		break;
	case 3 :
		if (std::sscanf(firstNonBlank, "(%f,%f,%f)", &tuple[0], &tuple[1], &tuple[2]) == 3)
		{
			return true;
		}
		break;
	case 4 :
		if (std::sscanf(firstNonBlank, "(%f,%f,%f,%f)", &tuple[0], &tuple[1], &tuple[2], &tuple[3]) == 4)
		{
			return true;
		}
		break;
	default :
		UNREACHABLE();
	} // switch (elementCount)

	// Clear the output on error:
	for (uint i = 0; i < elementCount; ++i) { tuple[i] = 0.0f; }
	return false;
}

// ========================================================
// StringBase::scanBool():
// ========================================================

bool StringBase::scanBool(bool & boolVal) const
{
	DEBUG_CHECK(isValid());

	static const char * trueStrings[]  = { "true",  "yes", "1", "on",  nullptr };
	static const char * falseStrings[] = { "false", "no",  "0", "off", nullptr };

	const char * firstNonBlank = strSkipWhiteSpaces(cStr);

	for (int i = 0; trueStrings[i] != nullptr; ++i)
	{
		if (strStartsWithIgnoreCase(firstNonBlank, strLength(firstNonBlank),
				trueStrings[i], strLength(trueStrings[i])))
		{
			boolVal = true;
			return true; // Valid boolean string.
		}
	}

	for (int i = 0; falseStrings[i] != nullptr; ++i)
	{
		if (strStartsWithIgnoreCase(firstNonBlank, strLength(firstNonBlank),
				falseStrings[i], strLength(falseStrings[i])))
		{
			boolVal = false;
			return true; // Valid boolean string.
		}
	}

	boolVal = false;
	return false; // Not a boolean string.
}

#undef SCAN_INT_IMP
#undef SCAN_FLT_IMP

} // namespace core {}
} // namespace atlas {}
