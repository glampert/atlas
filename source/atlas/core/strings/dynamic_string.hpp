
// ================================================================================================
// -*- C++ -*-
// File: dynamic_string.hpp
// Author: Guilherme R. Lampert
// Created on: 18/12/14
// Brief: Dynamically resizeable string type. `String` is the go-to string type for most uses.
// ================================================================================================

#ifndef ATLAS_CORE_STRINGS_DYNAMIC_STRING_HPP
#define ATLAS_CORE_STRINGS_DYNAMIC_STRING_HPP

#include "atlas/core/strings/string_base.hpp"
#include "atlas/core/containers/arrays.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// class String:
// ========================================================

class String final
	: public StringBase
{
public:

	// Construct an empty but valid string.
	String();

	// Construct an empty string, but preallocate a given capacity.
	explicit String(uint sizeToReserve);

	// Construct a string filled with a given character (fills `count` chars).
	String(uint count, char fillWith);

	// Construct from a null terminated C string:
	String(const char * str);
	String(const char * str, uint count);
	String(const char * str, uint startIndex, uint count);

	// Construct from a compatible string type:
	String(const String & other);
	String(const StringBase & other);
	String(const StringBase & other, uint count);
	String(const StringBase & other, uint startIndex, uint count);

	// Assignment operators:
	String & operator  = (const String & other);
	String & operator  = (const StringBase & other);
	String & operator  = (const char * str);
	String & operator += (const StringBase & other);
	String & operator += (const char * str);
	String & operator += (char c);
	String   operator  + (const StringBase & other) const;
	String   operator  + (const char * str) const;
	String   operator  + (char c) const;

	// Assign string of compatible type:
	String & assign(const StringBase & other);
	String & assign(const StringBase & other, uint count);
	String & assign(const StringBase & other, uint startIndex, uint count);

	// Assign C-string:
	String & assign(const char * str);
	String & assign(const char * str, uint count);
	String & assign(const char * str, uint startIndex, uint count);

	// Append string of compatible type:
	String & append(const StringBase & other);
	String & append(const StringBase & other, uint count);
	String & append(const StringBase & other, uint startIndex, uint count);

	// Append C-string:
	String & append(char c);
	String & append(const char * str);
	String & append(const char * str, uint count);
	String & append(const char * str, uint startIndex, uint count);

	// Finds the first occurrence of a substring in this string, returning it as a new string
	// or an empty string if the substring is not part of it. Search is always case-sensitive.
	String extractSubstring(const StringBase & substring) const;
	String extractSubstring(const char * substring) const;

	// Extract a part of this string to a new one.
	// If `count` is negative or greater than this string's length, copies from `startIndex`
	// until the end of this string. Produces an empty string if this is empty or if `count` is zero.
	String extractSubstring(uint startIndex, int count) const;

	// Add padding characters to the start or end of the string. If the string is already
	// longer than the requested padding, the input string is returned unchanged.
	String padRight(const uint padWanted, char padChar = ' ') const;
	String padLeft(const uint padWanted, char padChar = ' ') const;

	// Append or remove a single character at the end of the string.
	// `pushBack()` will fail with a debug assertion if the string is full.
	// `popBack()`  will fail with a debug assertion if the string is empty.
	void pushBack(char c);
	void popBack();

	// Grows the string storage if `newLength` is greater than the current allocated size.
	// Does nothing otherwise. `newLen` is the number of chars to reserve memory for.
	// +1 is automatically added to this value for the null terminator.
	// If `preserveString` is true, the current string is preserved in case of a
	// reallocation. Otherwise, it is discarded.
	void allocate(uint newLen, bool preserveString = true);

	// Clear the current string and frees any dynamically allocated memory.
	// The string object is still valid after that, but with backing-store pointing to `smallStr`.
	void deallocate();

	// Transfers the contents of this string to `dest`, making this string empty.
	// Will generate a runtime assertion if `dest` is not an empty string.
	void transferTo(String & dest);

	// Does any necessary cleanup if dynamic memory was allocated.
	~String();

private:

	bool hasDynamicMemory() const;
	void freeDynamicMemory();

	// Keep a small static buffer for short strings.
	// When this space is depleted, allocate a new one
	// on the heap with `ReallocExtra` extra chars added.
	// This is sometimes called the "Small String Optimization".
	static constexpr uint SmallStrSize = 32;
	static constexpr uint ReallocExtra = SmallStrSize * 2;
	char smallStr[SmallStrSize];
};

// ========================================================
// String splitting:
// ========================================================

// Split a string into tokens/substrings according to a set of delimiter characters,
// producing an array with one or more substrings (or indexes to the substrings).
// Both functions return the number of substrings produced, which will be one
// (the whole `source` string) if none of the delimiting characters are found inside the input.
uint split(const String & source, const String & delimiters, Array<String::SubIndexes> & subIndexes);
uint split(const String & source, const String & delimiters, Array<String> & substrings);

// ================================================================================================
// String inline methods:
// ================================================================================================

// ========================================================
// String::String():
// ========================================================

inline String::String(const char * str)
	: StringBase(smallStr, SmallStrSize, 0)
{
	assign(str);
}

// ========================================================
// String::String():
// ========================================================

inline String::String(const char * str, const uint count)
	: StringBase(smallStr, SmallStrSize, 0)
{
	assign(str, count);
}

// ========================================================
// String::String():
// ========================================================

inline String::String(const char * str, const uint startIndex, const uint count)
	: StringBase(smallStr, SmallStrSize, 0)
{
	assign(str, startIndex, count);
}

// ========================================================
// String::String():
// ========================================================

inline String::String(const StringBase & other)
	: StringBase(smallStr, SmallStrSize, 0)
{
	assign(other);
}

// ========================================================
// String::String():
// ========================================================

inline String::String(const String & other)
	: StringBase(smallStr, SmallStrSize, 0)
{
	assign(other);
}

// ========================================================
// String::String():
// ========================================================

inline String::String(const StringBase & other, const uint count)
	: StringBase(smallStr, SmallStrSize, 0)
{
	assign(other, count);
}

// ========================================================
// String::String():
// ========================================================

inline String::String(const StringBase & other, const uint startIndex, const uint count)
	: StringBase(smallStr, SmallStrSize, 0)
{
	assign(other, startIndex, count);
}

// ========================================================
// String::operator = :
// ========================================================

inline String & String::operator = (const String & other)
{
	return assign(other);
}

// ========================================================
// String::operator = :
// ========================================================

inline String & String::operator = (const StringBase & other)
{
	return assign(other);
}

// ========================================================
// String::operator = :
// ========================================================

inline String & String::operator = (const char * str)
{
	return assign(str);
}

// ========================================================
// String::operator += :
// ========================================================

inline String & String::operator += (const StringBase & other)
{
	return append(other);
}

// ========================================================
// String::operator += :
// ========================================================

inline String & String::operator += (const char * str)
{
	return append(str);
}

// ========================================================
// String::operator += :
// ========================================================

inline String & String::operator += (const char c)
{
	pushBack(c);
	return *this;
}

// ========================================================
// String::operator + :
// ========================================================

inline String String::operator + (const StringBase & other) const
{
	String tmp(*this);
	tmp.append(other);
	return tmp;
}

// ========================================================
// String::operator + :
// ========================================================

inline String String::operator + (const char * str) const
{
	String tmp(*this);
	tmp.append(str);
	return tmp;
}

// ========================================================
// String::operator + :
// ========================================================

inline String String::operator + (const char c) const
{
	String tmp(*this);
	tmp.pushBack(c);
	return tmp;
}

// ========================================================
// String::assign():
// ========================================================

inline String & String::assign(const StringBase & other)
{
	return assign(other.getCStr(), other.getLength());
}

// ========================================================
// String::assign():
// ========================================================

inline String & String::assign(const StringBase & other, const uint count)
{
	return assign(other.getCStr(), count);
}

// ========================================================
// String::assign():
// ========================================================

inline String & String::assign(const StringBase & other, const uint startIndex, const uint count)
{
	return assign(other.getCStr(), startIndex, count);
}

// ========================================================
// String::append():
// ========================================================

inline String & String::append(const StringBase & other)
{
	return append(other.getCStr(), other.getLength());
}

// ========================================================
// String::append():
// ========================================================

inline String & String::append(const StringBase & other, const uint count)
{
	return append(other.getCStr(), count);
}

// ========================================================
// String::append():
// ========================================================

inline String & String::append(const StringBase & other, const uint startIndex, const uint count)
{
	return append(other.getCStr(), startIndex, count);
}

// ========================================================
// String::append():
// ========================================================

inline String & String::append(const char c)
{
	pushBack(c);
	return *this;
}

// ========================================================
// String::append():
// ========================================================

inline String & String::append(const char * str)
{
	return append(str, 0, strLength(str));
}

// ========================================================
// String::append():
// ========================================================

inline String & String::append(const char * str, const uint count)
{
	return append(str, 0, count);
}

// ========================================================
// Additional string operators that have to be global:
// ========================================================

inline bool operator == (const char * a, const String & b)
{
	return b == a;
}

inline bool operator != (const char * a, const String & b)
{
	return b != a;
}

inline String operator + (const char * a, const String & b)
{
	String tmp(a);
	tmp.append(b);
	return tmp;
}

inline String operator + (const char a, const String & b)
{
	String tmp;
	tmp.pushBack(a);
	tmp.append(b);
	return tmp;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_STRINGS_DYNAMIC_STRING_HPP
