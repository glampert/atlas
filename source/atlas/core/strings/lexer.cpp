
// ================================================================================================
// -*- C++ -*-
// File: lexer.cpp
// Author: Guilherme R. Lampert
// Created on: 08/08/15
// Brief: Lexicographical scanner (Lexer) compatible with C-like languages. Adapted from idLexer.
// ================================================================================================

#include "atlas/core/strings/lexer.hpp"
#include "atlas/core/fs/file_handling.hpp"

// ========================================================
// NOTES:
//
// Code in this file and its accompanying header file are
// derived from source code of Doom 3 BFG edition (idLexer
// and idToken), and thus are subject to a different
// license (GPL). Please read the original copyright notice
// extracted from the original source files that can be
// found at the end of this file or the end of lexer.hpp
// for more information and legal details.
//
// ========================================================

namespace atlas
{
namespace core
{

// ================================================================================================
// Token class implementation:
// ================================================================================================

// ========================================================
// Token::clear():
// ========================================================

void Token::clear()
{
	whiteSpaceStartPtr = nullptr;
	whiteSpaceEndPtr   = nullptr;
	type               = 0;
	subtype            = 0;
	lineNumber         = 0;
	linesCrossed       = 0;
	ulongValue         = 0;
	doubleValue        = 0.0;
	text.clear();
}

// ========================================================
// Token::computeNumberValue():
// ========================================================

void Token::computeNumberValue() const
{
	DEBUG_CHECK(type == Type::Number);

	const char * p = text.getCStr();
	double newDoubleVal = 0.0;
	ulong  newULongVal  = 0;

	if (subtype & Flag::Float)
	{
		// Floating point exceptions:
		if (subtype & (Flag::Infinite | Flag::Indefinite | Flag::NaN))
		{
			if (subtype & Flag::Infinite) // 1.#INF
			{
				const uint32 inf = 0x7F800000;
				newDoubleVal = static_cast<double>(*reinterpret_cast<const float32 *>(&inf));
			}
			else if (subtype & Flag::Indefinite) // 1.#IND
			{
				const uint32 ind = 0xFFC00000;
				newDoubleVal = static_cast<double>(*reinterpret_cast<const float32 *>(&ind));
			}
			else if (subtype & Flag::NaN) // 1.#QNAN
			{
				const uint32 nan = 0x7FC00000;
				newDoubleVal = static_cast<double>(*reinterpret_cast<const float32 *>(&nan));
			}
		}
		else
		{
			double m;

			while (*p && *p != '.' && *p != 'e')
			{
				newDoubleVal = newDoubleVal * 10.0 + (static_cast<double>(*p - '0'));
				++p;
			}

			if (*p == '.')
			{
				++p;
				for (m = 0.1; *p && *p != 'e'; ++p)
				{
					newDoubleVal = newDoubleVal + (static_cast<double>(*p - '0')) * m;
					m *= 0.1;
				}
			}

			if (*p == 'e')
			{
				bool div;

				++p;
				if (*p == '-')
				{
					div = true;
					++p;
				}
				else if (*p == '+')
				{
					div = false;
					++p;
				}
				else
				{
					div = false;
				}

				int i, pow;
				for (pow = 0; *p; ++p)
				{
					pow = pow * 10 + (static_cast<int>(*p - '0'));
				}
				for (m = 1.0, i = 0; i < pow; ++i)
				{
					m *= 10.0;
				}

				if (div)
				{
					newDoubleVal /= m;
				}
				else
				{
					newDoubleVal *= m;
				}
			}
		}
		newULongVal = static_cast<ulong>(newDoubleVal);
	}
	else if (subtype & Flag::Decimal)
	{
		while (*p)
		{
			newULongVal = newULongVal * 10 + (*p - '0');
			++p;
		}
		newDoubleVal = newULongVal;
	}
	else if (subtype & Flag::IpAddress)
	{
		int c = 0;
		while (*p && *p != ':')
		{
			if (*p == '.')
			{
				while (c != 3)
				{
					newULongVal = newULongVal * 10;
					++c;
				}
				c = 0;
			}
			else
			{
				newULongVal = newULongVal * 10 + (*p - '0');
				++c;
			}
			++p;
		}
		while (c != 3)
		{
			newULongVal = newULongVal * 10;
			++c;
		}
		newDoubleVal = newULongVal;
	}
	else if (subtype & Flag::Octal)
	{
		p += 1; // Step over the first zero
		while (*p)
		{
			newULongVal = (newULongVal << 3) + (*p - '0');
			++p;
		}
		newDoubleVal = newULongVal;
	}
	else if (subtype & Flag::Hexadecimal)
	{
		p += 2; // Step over the leading 0x or 0X
		while (*p)
		{
			newULongVal <<= 4;
			if (*p >= 'a' && *p <= 'f')
			{
				newULongVal += *p - 'a' + 10;
			}
			else if (*p >= 'A' && *p <= 'F')
			{
				newULongVal += *p - 'A' + 10;
			}
			else
			{
				newULongVal += *p - '0';
			}
			++p;
		}
		newDoubleVal = newULongVal;
	}
	else if (subtype & Flag::Binary)
	{
		p += 2; // Step over the leading 0b or 0B
		while (*p)
		{
			newULongVal = (newULongVal << 1) + (*p - '0');
			++p;
		}
		newDoubleVal = newULongVal;
	}

	// Save the newly computed values and set the ready flag:
	doubleValue = newDoubleVal;
	ulongValue  = newULongVal;
	subtype    |= Flag::ValuesValid;
}

// ================================================================================================
// Lexer class implementation:
// ================================================================================================

// ========================================================
// Lexer::commonInit():
// ========================================================

void Lexer::commonInit()
{
	bufferHeadPtr      = nullptr;
	scriptPtr          = nullptr;
	endPtr             = nullptr;
	lastScriptPtr      = nullptr;
	whiteSpaceStartPtr = nullptr;
	whiteSpaceEndPtr   = nullptr;
	scriptLength       = 0;
	lineNumber         = 0;
	lastLine           = 0;
	lexFlags           = 0;
	tokenAvailable     = false;
	initialized        = false;
	allocated          = false;
	hadError           = false;
	hadWarning         = false;
}

// ========================================================
// Lexer::Lexer():
// ========================================================

Lexer::Lexer(LogStream * log, const int logLvl)
{
	logStr   = log;
	logLevel = logLvl;
	commonInit();
}

// ========================================================
// Lexer::Lexer():
// ========================================================

Lexer::Lexer(const String & filename, const uint flags, LogStream * log, const int logLvl)
{
	logStr   = log;
	logLevel = logLvl;
	commonInit();
	initFromFile(filename, flags);
}

// ========================================================
// Lexer::Lexer():
// ========================================================

Lexer::Lexer(const char * ptr, const uint length, const String & name, const uint flags,
             const uint startLine, LogStream * log, const int logLvl)
{
	logStr   = log;
	logLevel = logLvl;
	commonInit();
	initFromFileInMemory(ptr, length, name, flags, startLine);
}

// ========================================================
// Lexer::~Lexer():
// ========================================================

Lexer::~Lexer()
{
	deallocate();
}

// ========================================================
// Lexer::initFromFile():
// ========================================================

bool Lexer::initFromFile(const String & filename, const uint flags)
{
	DEBUG_CHECK(!filename.isEmpty());

	if (initialized)
	{
		return error("Lexer::initFromFile(): Another script is already loaded!");
	}

	uint fileLength;
	char * fileContents;
	ErrorInfo errorInfo;

	if (!FileSystem::loadTextFile(filename, fileContents, fileLength, &errorInfo))
	{
		return error("Lexer failed to load text file \"" + filename + "\": " + toString(errorInfo));
	}

	scriptFilename = filename;
	bufferHeadPtr  = fileContents;
	scriptLength   = fileLength;
	scriptPtr      = bufferHeadPtr;
	lastScriptPtr  = bufferHeadPtr;
	endPtr         = &bufferHeadPtr[fileLength];
	lineNumber     = 1;
	lastLine       = 1;
	lexFlags       = flags;
	tokenAvailable = false;
	allocated      = true; // We own the buffer returned by loadTextFile().
	initialized    = true;

	return true;
}

// ========================================================
// Lexer::initFromFileInMemory():
// ========================================================

bool Lexer::initFromFileInMemory(const char * ptr, const uint length, const String & name, const uint flags, const uint startLine)
{
	DEBUG_CHECK(ptr != nullptr);
	DEBUG_CHECK(length != 0);

	if (initialized)
	{
		return error("Lexer::initFromFileInMemory(): Another script is already loaded!");
	}

	//
	// Note that in this case, Lexer DOES NOT take ownership of the input buffer!
	//

	scriptFilename = (!name.isEmpty()) ? name : "(memory)";
	bufferHeadPtr  = ptr;
	scriptLength   = length;
	scriptPtr      = bufferHeadPtr;
	lastScriptPtr  = bufferHeadPtr;
	endPtr         = &bufferHeadPtr[length];
	lineNumber     = startLine;
	lastLine       = startLine;
	lexFlags       = flags;
	tokenAvailable = false;
	allocated      = false;
	initialized    = true;

	return true;
}

// ========================================================
// Lexer::deallocate():
// ========================================================

void Lexer::deallocate()
{
	if (allocated)
	{
		delete[] bufferHeadPtr;

		allocated          = false;
		bufferHeadPtr      = nullptr;
		scriptPtr          = nullptr;
		endPtr             = nullptr;
		lastScriptPtr      = nullptr;
		whiteSpaceStartPtr = nullptr;
		whiteSpaceEndPtr   = nullptr;
		scriptLength       = 0;
	}

	leftoverToken.clear();
	scriptFilename.clear();

	lineNumber     = 0;
	lastLine       = 0;
	tokenAvailable = false;
	initialized    = false;
}

// ========================================================
// Lexer::reset():
// ========================================================

void Lexer::reset()
{
	scriptPtr          = bufferHeadPtr;
	lastScriptPtr      = bufferHeadPtr;
	whiteSpaceStartPtr = nullptr;
	whiteSpaceEndPtr   = nullptr;
	tokenAvailable     = false;
	hadError           = false;
	hadWarning         = false;
	lineNumber         = 1;
	lastLine           = 1;

	leftoverToken.clear();
}

// ========================================================
// Lexer::error():
// ========================================================

bool Lexer::error(const String & errorMessage)
{
	hadError = true;
	if (lexFlags & Flag::NoErrors)
	{
		return false;
	}

	// Use the default log if none provided by the user.
	if (logStr == nullptr)
	{
		logStr   = &core::getLog();
		logLevel = core::getLogVerbosityLevel();
	}

	if (lexFlags & Flag::NoFatalErrors)
	{
		LOG_ERROR_EX(*logStr, logLevel, AnsiColorCodes::Red << "File " << scriptFilename
			<< "(" << lineNumber << "): " << AnsiColorCodes::Default << errorMessage);
	}
	else
	{
		LOG_FATAL_EX(*logStr, logLevel, AnsiColorCodes::Magenta << "File " << scriptFilename
			<< "(" << lineNumber << "): " << AnsiColorCodes::Default << errorMessage);
	}

	return false;
}

// ========================================================
// Lexer::warning():
// ========================================================

void Lexer::warning(const String & warningMessage)
{
	hadWarning = true;
	if (lexFlags & Flag::NoWarnings)
	{
		return;
	}

	// Use the default log if none provided by the user.
	if (logStr == nullptr)
	{
		logStr   = &core::getLog();
		logLevel = core::getLogVerbosityLevel();
	}

	LOG_WARN_EX(*logStr, logLevel, AnsiColorCodes::Yellow << "File " << scriptFilename
		<< "(" << lineNumber << "): " << AnsiColorCodes::Default << warningMessage);
}

// ========================================================
// Lexer::readWhiteSpace():
// ========================================================

bool Lexer::readWhiteSpace()
{
	for (;;)
	{
		// Skip whitespace:
		while (*scriptPtr <= ' ')
		{
			if (!*scriptPtr)
			{
				return false;
			}
			if (*scriptPtr == '\n')
			{
				lineNumber++;
			}
			scriptPtr++;
		}

		// Skip comments:
		if (*scriptPtr == '/')
		{
			// C++-style comments:
			if (*(scriptPtr + 1) == '/')
			{
				scriptPtr++;
				do
				{
					scriptPtr++;
					if (!*scriptPtr)
					{
						return false;
					}
				}
				while (*scriptPtr != '\n');

				lineNumber++;
				scriptPtr++;

				if (!*scriptPtr)
				{
					return false;
				}
				continue;
			}
			// C-style/multi-line comments:
			else if (*(scriptPtr + 1) == '*')
			{
				scriptPtr++;
				for (;;)
				{
					scriptPtr++;
					if (!*scriptPtr)
					{
						return false;
					}
					if (*scriptPtr == '\n')
					{
						lineNumber++;
					}
					else if (*scriptPtr == '/')
					{
						if (*(scriptPtr - 1) == '*')
						{
							break;
						}
						if (*(scriptPtr + 1) == '*')
						{
							warning("Nested C-style, multi-line comment!");
						}
					}
				}

				scriptPtr++;
				if (!*scriptPtr)
				{
					return false;
				}
				scriptPtr++;
				if (!*scriptPtr)
				{
					return false;
				}
				continue;
			}
		}
		break;
	}
	return true;
}

// ========================================================
// Lexer::skipWhiteSpace():
// ========================================================

bool Lexer::skipWhiteSpace(const bool currentLine)
{
	for (;;)
	{
		DEBUG_CHECK(scriptPtr <= endPtr);

		if (scriptPtr == endPtr)
		{
			return false;
		}

		// Skip whitespace:
		while (*scriptPtr <= ' ')
		{
			if (scriptPtr == endPtr)
			{
				return false;
			}
			if (!*scriptPtr)
			{
				return false;
			}
			if (*scriptPtr == '\n')
			{
				lineNumber++;
				if (currentLine)
				{
					scriptPtr++;
					return true;
				}
			}
			scriptPtr++;
		}

		// Skip comments:
		if (*scriptPtr == '/')
		{
			// C++-style comments:
			if (*(scriptPtr + 1) == '/')
			{
				scriptPtr++;
				do
				{
					scriptPtr++;
					if (!*scriptPtr)
					{
						return false;
					}
				}
				while (*scriptPtr != '\n');

				lineNumber++;
				scriptPtr++;

				if (currentLine)
				{
					return true;
				}
				if (!*scriptPtr)
				{
					return false;
				}
				continue;
			}
			// C-style/multi-line comments:
			else if (*(scriptPtr + 1) == '*')
			{
				scriptPtr++;
				for (;;)
				{
					scriptPtr++;
					if (!*scriptPtr)
					{
						return false;
					}
					if (*scriptPtr == '\n')
					{
						lineNumber++;
					}
					else if (*scriptPtr == '/')
					{
						if (*(scriptPtr - 1) == '*')
						{
							break;
						}
						if (*(scriptPtr + 1) == '*')
						{
							warning("Nested C-style, multi-line comment!");
						}
					}
				}

				scriptPtr++;
				if (!*scriptPtr)
				{
					return false;
				}
				continue;
			}
		}
		break;
	}
	return true;
}

// ========================================================
// Lexer::readEscapeCharacter():
// ========================================================

bool Lexer::readEscapeCharacter(char & ch)
{
	int c, val, i;
	scriptPtr++; // Step over the leading '\\'

	// Determine the escape character:
	switch (*scriptPtr)
	{
		case '\\' : c = '\\'; break;
		case 'n'  : c = '\n'; break;
		case 'r'  : c = '\r'; break;
		case 't'  : c = '\t'; break;
		case 'v'  : c = '\v'; break;
		case 'b'  : c = '\b'; break;
		case 'f'  : c = '\f'; break;
		case 'a'  : c = '\a'; break;
		case '\'' : c = '\''; break;
		case '\"' : c = '\"'; break;
		case '\?' : c = '\?'; break;
		case 'x'  : // Scan hexadecimal constant:
		{
			scriptPtr++;
			for (i = 0, val = 0; ; i++, scriptPtr++)
			{
				c = *scriptPtr;
				if (c >= '0' && c <= '9')
				{
					c = c - '0';
				}
				else if (c >= 'A' && c <= 'Z')
				{
					c = c - 'A' + 10;
				}
				else if (c >= 'a' && c <= 'z')
				{
					c = c - 'a' + 10;
				}
				else
				{
					break;
				}
				val = (val << 4) + c;
			}
			scriptPtr--;

			if (val > 0xFF)
			{
				warning("Hexadecimal value in escape character is too big! Truncating to 0xFF...");
				val = 0xFF;
			}

			c = val;
			break;
		}
		default: // NOTE: decimal ASCII code, NOT octal!
		{
			if (*scriptPtr < '0' || *scriptPtr > '9')
			{
				return error("Unknown/invalid escape char!");
			}

			for (i = 0, val = 0; ; i++, scriptPtr++)
			{
				c = *scriptPtr;
				if (c >= '0' && c <= '9')
				{
					c = c - '0';
				}
				else
				{
					break;
				}
				val = val * 10 + c;
			}
			scriptPtr--;

			if (val > 0xFF)
			{
				warning("Value in escape character is too big! Truncating to 0xFF...");
				val = 0xFF;
			}

			c = val;
			break;
		}
	} // switch (*scriptPtr)

	// Step over the escape character or the last digit of the number.
	scriptPtr++;

	ch = static_cast<char>(c);
	return true;
}

// ========================================================
// Lexer::readString():
// ========================================================

bool Lexer::readString(Token & tokenOut, const int quote)
{
	// Escape characters are interpreted.
	// Reads two strings with only a white space between them as one string.

	uint tmpLineNum;
	const char * tmpScriptPtr;
	char ch;

	if (quote == '\"')
	{
		tokenOut.type = Token::Type::String;
	}
	else
	{
		tokenOut.type = Token::Type::Literal;
	}

	scriptPtr++; // Skip leading quote

	for (;;)
	{
		// If there is an escape character and escape characters are allowed...
		if (*scriptPtr == '\\' && !(lexFlags & Flag::NoStringEscapeChars))
		{
			if (!readEscapeCharacter(ch))
			{
				return false;
			}
			tokenOut.append(ch);
		}
		// If a trailing quote...
		else if (*scriptPtr == quote)
		{
			// Step over the quote
			scriptPtr++;

			// If consecutive strings should not be concatenated...
			if ((lexFlags & Flag::NoStringConcat) &&
			  (!(lexFlags & Flag::AllowBackslashStringConcat) || quote != '\"'))
			{
				break;
			}

			tmpScriptPtr = scriptPtr;
			tmpLineNum   = lineNumber;

			// Read white space between possible two consecutive strings.
			// Restore line index on failure.
			if (!readWhiteSpace())
			{
				scriptPtr  = tmpScriptPtr;
				lineNumber = tmpLineNum;
				break;
			}

			if (lexFlags & Flag::NoStringConcat)
			{
				if (*scriptPtr != '\\')
				{
					scriptPtr  = tmpScriptPtr;
					lineNumber = tmpLineNum;
					break;
				}

				// Step over the '\\'
				scriptPtr++;
				if (!readWhiteSpace() || *scriptPtr != quote)
				{
					return error("Expecting string after '\\' terminated line!");
				}
			}

			// If there's no leading quote...
			if (*scriptPtr != quote)
			{
				scriptPtr  = tmpScriptPtr;
				lineNumber = tmpLineNum;
				break;
			}
			scriptPtr++; // Step over the new leading quote.
		}
		else
		{
			if (*scriptPtr == '\0')
			{
				return error("Missing trailing quote!");
			}
			if (*scriptPtr == '\n')
			{
				return error("Newline inside string!");
			}
			tokenOut.append(*scriptPtr++);
		}
	}

	if (tokenOut.type == Token::Type::Literal)
	{
		if (!(lexFlags & Flag::AllowMultiCharLiterals))
		{
			if (tokenOut.getLength() != 1)
			{
				warning("Literal is not one character long!");
			}
		}
		tokenOut.subtype = tokenOut[0];
	}
	else
	{
		// The sub type is the length of the string
		tokenOut.subtype = tokenOut.getLength();
	}

	return true;
}

// ========================================================
// Lexer::readName():
// ========================================================

bool Lexer::readName(Token & tokenOut)
{
	struct Local
	{
		// Names can contain aA-zZ letters, numbers or underscore.
		static bool validNameChar(const int c)
		{
			return ((c >= 'a' && c <= 'z') ||
			        (c >= 'A' && c <= 'Z') ||
			        (c >= '0' && c <= '9') || c == '_');
		}
		// If treating all tokens as strings, don't parse '-' as a separate token.
		static bool stringsOnly(const int c, const uint lexFlags)
		{
			return (lexFlags & Flag::OnlyStrings) && (c == '-');
		}
		// If special path name characters are allowed.
		static bool allowingPathNames(const int c, const uint lexFlags)
		{
			return (lexFlags & Flag::AllowPathNames) &&
			       (c == '/' || c == '\\' || c == ':' || c == '.');
		}
	};

	int c;
	tokenOut.type = Token::Type::NameId;

	do
	{
		tokenOut.append(*scriptPtr++);
		c = *scriptPtr;
	}
	while (Local::validNameChar(c) ||
	       Local::stringsOnly(c, lexFlags) ||
	       Local::allowingPathNames(c, lexFlags));

	// The sub type is the length of the name.
	tokenOut.subtype = tokenOut.getLength();
	return true;
}

// ========================================================
// Lexer::checkString():
// ========================================================

bool Lexer::checkString(const char * string) const
{
	DEBUG_CHECK(string != nullptr);

	for (int i = 0; string[i] != '\0'; ++i)
	{
		if (scriptPtr[i] != string[i])
		{
			return false;
		}
	}
	return true;
}

// ========================================================
// Lexer::readNumber():
// ========================================================

bool Lexer::readNumber(Token & tokenOut)
{
	int i;
	int dot;
	char c, c2;

	tokenOut.type        = Token::Type::Number;
	tokenOut.subtype     = 0;
	tokenOut.ulongValue  = 0;
	tokenOut.doubleValue = 0;

	c  = *scriptPtr;
	c2 = *(scriptPtr + 1);

	if (c == '0' && c2 != '.')
	{
		// Check for a hexadecimal number:
		if (c2 == 'x' || c2 == 'X')
		{
			tokenOut.append(*scriptPtr++);
			tokenOut.append(*scriptPtr++);
			c = *scriptPtr;

			while ((c >= '0' && c <= '9') ||
			       (c >= 'a' && c <= 'f') ||
			       (c >= 'A' && c <= 'F'))
			{
				tokenOut.append(c);
				c = *(++scriptPtr);
			}

			tokenOut.subtype = Token::Flag::Hexadecimal | Token::Flag::Integer;
		}
		// Check for a binary number:
		else if (c2 == 'b' || c2 == 'B')
		{
			tokenOut.append(*scriptPtr++);
			tokenOut.append(*scriptPtr++);
			c = *scriptPtr;

			while (c == '0' || c == '1')
			{
				tokenOut.append(c);
				c = *(++scriptPtr);
			}

			tokenOut.subtype = Token::Flag::Binary | Token::Flag::Integer;
		}
		// Its an octal number:
		else
		{
			tokenOut.append(*scriptPtr++);
			c = *scriptPtr;

			while (c >= '0' && c <= '7')
			{
				tokenOut.append(c);
				c = *(++scriptPtr);
			}

			tokenOut.subtype = Token::Flag::Octal | Token::Flag::Integer;
		}
	}
	else // Decimal integer or floating point number or even an IP address:
	{
		// Count the number of '.' in the number:
		dot = 0;
		for (;;)
		{
			if (c >= '0' && c <= '9')
			{
			}
			else if (c == '.')
			{
				dot++;
			}
			else
			{
				break;
			}

			tokenOut.append(c);
			c = *(++scriptPtr);
		}

		if (c == 'e' && dot == 0)
		{
			// We have scientific notation without a decimal point.
			dot++;
		}

		// If a floating point number...
		if (dot == 1)
		{
			tokenOut.subtype = Token::Flag::Decimal | Token::Flag::Float;

			// Check for floating point exponent:
			if (c == 'e')
			{
				// Append the 'e' so that Token::computeNumberValue() parses the value properly.
				tokenOut.append(c);
				c = *(++scriptPtr);

				if (c == '-')
				{
					tokenOut.append(c);
					c = *(++scriptPtr);
				}
				else if (c == '+')
				{
					tokenOut.append(c);
					c = *(++scriptPtr);
				}

				while (c >= '0' && c <= '9')
				{
					tokenOut.append(c);
					c = *(++scriptPtr);
				}
			}
			// Check for floating point exception infinite 1.#INF or indefinite 1.#IND or NaN:
			else if (c == '#')
			{
				c2 = 4;
				if (checkString("INF"))
				{
					tokenOut.subtype |= Token::Flag::Infinite;
				}
				else if (checkString("IND"))
				{
					tokenOut.subtype |= Token::Flag::Indefinite;
				}
				else if (checkString("NAN"))
				{
					tokenOut.subtype |= Token::Flag::NaN;
				}
				else if (checkString("QNAN"))
				{
					tokenOut.subtype |= Token::Flag::NaN;
					c2++;
				}
				else if (checkString("SNAN"))
				{
					tokenOut.subtype |= Token::Flag::NaN;
					c2++;
				}

				for (i = 0; i < c2; ++i)
				{
					tokenOut.append(c);
					c = *(++scriptPtr);
				}

				while (c >= '0' && c <= '9')
				{
					tokenOut.append(c);
					c = *(++scriptPtr);
				}

				if (!(lexFlags & Flag::AllowFloatExceptions))
				{
					return error("Floating-point exception scanned: " + tokenOut.text);
				}
			}
		}
		else if (dot > 1)
		{
			if (!(lexFlags & Flag::AllowIpAddresses))
			{
				return error("More than one dot in number! Set Lexer::AllowIpAddresses to parse IP addresses.");
			}
			if (dot != 3)
			{
				return error("IP address should have three dots!");
			}
			tokenOut.subtype = Token::Flag::IpAddress;
		}
		else
		{
			tokenOut.subtype = Token::Flag::Decimal | Token::Flag::Integer;
		}
	}

	if (tokenOut.subtype & Token::Flag::Float)
	{
		if (c > ' ')
		{
			// Single-precision: float
			if (c == 'f' || c == 'F')
			{
				tokenOut.subtype |= Token::Flag::SinglePrecision;
				scriptPtr++;
			}
			// Extended-precision: long double
			else if (c == 'l' || c == 'L')
			{
				tokenOut.subtype |= Token::Flag::ExtendedPrecision;
				scriptPtr++;
			}
			// Default is double-precision: double
			else
			{
				tokenOut.subtype |= Token::Flag::DoublePrecision;
			}
		}
		else
		{
			tokenOut.subtype |= Token::Flag::DoublePrecision;
		}
	}
	else if (tokenOut.subtype & Token::Flag::Integer)
	{
		if (c > ' ')
		{
			// Default: signed long
			for (i = 0; i < 2; ++i)
			{
				// long integer
				if (c == 'l' || c == 'L')
				{
					tokenOut.subtype |= Token::Flag::Long;
				}
				// unsigned integer
				else if (c == 'u' || c == 'U')
				{
					tokenOut.subtype |= Token::Flag::Unsigned;
				}
				else
				{
					break;
				}
				c = *(++scriptPtr);
			}
		}
	}
	else if (tokenOut.subtype & Token::Flag::IpAddress)
	{
		if (c == ':')
		{
			tokenOut.append(c);
			c = *(++scriptPtr);

			while (c >= '0' && c <= '9')
			{
				tokenOut.append(c);
				c = *(++scriptPtr);
			}

			tokenOut.subtype |= Token::Flag::IpPort;
		}
	}

	return true;
}

// ========================================================
// Lexer::readPunctuation():
// ========================================================

bool Lexer::readPunctuation(Token & tokenOut)
{
	int l, n, i;
	for (n = punctuationTable[static_cast<uint>(*scriptPtr)]; n >= 0; n = nextPunctuation[n])
	{
		const Punctuation * punct = &punctuationInfo[n];
		const char        * chars = punct->chars;

		// Check for this punctuation in the script:
		for (l = 0; chars[l] && scriptPtr[l]; ++l)
		{
			if (scriptPtr[l] != chars[l])
			{
				break;
			}
		}

		if (!chars[l])
		{
			tokenOut.text.clear();
			tokenOut.text.allocate(l + 1, /* preserveString = */ false);

			for (i = 0; i <= l; ++i)
			{
				tokenOut.append(chars[i]);
			}
			DEBUG_CHECK(tokenOut.getLength() == static_cast<uint>(l));

			scriptPtr       += l;
			tokenOut.type    = Token::Type::Punctuation;
			tokenOut.subtype = punct->id; // Subtype is the punctuation id.

			return true;
		}
	}

	return false;
}

// ========================================================
// Lexer::readNextToken():
// ========================================================

bool Lexer::readNextToken(Token & tokenOut)
{
	if (!initialized || scriptPtr == nullptr)
	{
		return error("Lexer not properly initialized: No script loaded!");
	}

	// If there is a token available (from unreadToken)...
	if (tokenAvailable)
	{
		tokenOut = leftoverToken;
		leftoverToken.clear();
		tokenAvailable = false;
		return true;
	}

	// Save script & line pointers:
	lastScriptPtr = scriptPtr;
	lastLine      = lineNumber;

	// Make sure output is clear:
	tokenOut.clear();

	whiteSpaceStartPtr = scriptPtr;
	tokenOut.whiteSpaceStartPtr = scriptPtr;

	if (!readWhiteSpace())
	{
		return false;
	}

	whiteSpaceEndPtr = scriptPtr;
	tokenOut.whiteSpaceEndPtr = scriptPtr;

	tokenOut.lineNumber   = lineNumber;            // Line the token is on
	tokenOut.linesCrossed = lineNumber - lastLine; // Num of lines crossed before token

	int c = *scriptPtr;

	// If we're keeping everything as whitespace delimited strings...
	if (lexFlags & Flag::OnlyStrings)
	{
		// If there is a leading quote
		if (c == '\"' || c == '\'')
		{
			if (!readString(tokenOut, c))
			{
				return false;
			}
		}
		else if (!readName(tokenOut))
		{
			return false;
		}
	}
	// If there is a number...
	else if ((c >= '0' && c <= '9') ||
	         (c == '.' && (*(scriptPtr + 1) >= '0' && *(scriptPtr + 1) <= '9')))
	{
		if (!readNumber(tokenOut))
		{
			return false;
		}

		// If names are allowed to start with a number
		if (lexFlags & Flag::AllowNumberNames)
		{
			c = *scriptPtr;
			if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_')
			{
				if (!readName(tokenOut))
				{
					return false;
				}
			}
		}
	}
	// If there is a leading quote...
	else if (c == '\"' || c == '\'')
	{
		if (!readString(tokenOut, c))
		{
			return false;
		}
	}
	// If there is a name...
	else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_')
	{
		if (!readName(tokenOut))
		{
			return false;
		}
	}
	// Names may also start with a slash when pathnames are allowed...
	else if ((lexFlags & Flag::AllowPathNames) && ((c == '/' || c == '\\') || c == '.'))
	{
		if (!readName(tokenOut))
		{
			return false;
		}
	}
	// Finally, check for punctuations:
	else if (!readPunctuation(tokenOut))
	{
		return error(String("Unknown punctuation \'") + c + String("\'"));
	}

	// Successfully read a token.
	return true;
}

// ========================================================
// Lexer::readNextTokenOnLine():
// ========================================================

bool Lexer::readNextTokenOnLine(Token & tokenOut)
{
	Token tok;

	if (!readNextToken(tok))
	{
		scriptPtr  = lastScriptPtr;
		lineNumber = lastLine;
		return false;
	}

	// If no lines were crossed before this token:
	if (!tok.linesCrossed)
	{
		tokenOut = tok;
		return true;
	}

	// Restore our position
	scriptPtr  = lastScriptPtr;
	lineNumber = lastLine;
	tokenOut.clear();
	return false;
}

// ========================================================
// Lexer::expectTokenString():
// ========================================================

bool Lexer::expectTokenString(const char * string)
{
	Token tok;

	if (!readNextToken(tok))
	{
		return error("Couldn't find expected token \'" + String(string) + "\'");
	}

	if (tok != string)
	{
		return error("Expected \'" + String(string) + "\' but found \'" + tok.text + "\'");
	}

	return true;
}

// ========================================================
// Lexer::expectTokenString():
// ========================================================

bool Lexer::expectTokenString(const char * string, Token & tokenOut)
{
	if (!readNextToken(tokenOut))
	{
		return error("Couldn't find expected token \'" + String(string) + "\'");
	}

	if (tokenOut != string)
	{
		return error("Expected \'" + String(string) + "\' but found \'" + tokenOut.text + "\'");
	}

	return true;
}

// ========================================================
// Lexer::expectTokenType():
// ========================================================

bool Lexer::expectTokenType(const int type, const int subtype, Token & tokenOut)
{
	String str;

	if (!readNextToken(tokenOut))
	{
		return error("Couldn't read expected token!");
	}

	if (tokenOut.type != type)
	{
		switch (type)
		{
			case Token::Type::String      : str = "string";       break;
			case Token::Type::Literal     : str = "literal";      break;
			case Token::Type::Number      : str = "number";       break;
			case Token::Type::NameId      : str = "name/id";      break;
			case Token::Type::Punctuation : str = "punctuation";  break;
			default                       : str = "unknown type"; break;
		} // switch (type)

		return error("Expected a " + str + " but found \'" + tokenOut.text + "\'");
	}

	if (tokenOut.type == Token::Type::Number)
	{
		if ((tokenOut.subtype & subtype) != subtype)
		{
			if (subtype & Token::Flag::Decimal)     { str  = "decimal ";  }
			if (subtype & Token::Flag::Hexadecimal) { str  = "hex ";      }
			if (subtype & Token::Flag::Octal)       { str  = "octal ";    }
			if (subtype & Token::Flag::Binary)      { str  = "binary ";   }
			if (subtype & Token::Flag::Unsigned)    { str += "unsigned "; }
			if (subtype & Token::Flag::Long)        { str += "long ";     }
			if (subtype & Token::Flag::Float)       { str += "float ";    }
			if (subtype & Token::Flag::Integer)     { str += "integer ";  }
			str.trimRight();

			return error("Expected " + str + " but found \'" + tokenOut.text + "\'");
		}
	}
	else if (tokenOut.type == Token::Type::Punctuation)
	{
		if (subtype < 0)
		{
			return error("BUG: Wrong punctuation subtype!");
		}
		if (tokenOut.subtype != subtype)
		{
			return error("Expected \'" + getPunctuationFromId(subtype) + "\' but found \'" + tokenOut.text + "\'");
		}
	}

	return true;
}

// ========================================================
// Lexer::expectAnyToken():
// ========================================================

bool Lexer::expectAnyToken(Token & tokenOut)
{
	if (!readNextToken(tokenOut))
	{
		return error("Couldn't read expected token!");
	}
	return true;
}

// ========================================================
// Lexer::checkTokenString():
// ========================================================

bool Lexer::checkTokenString(const char * string)
{
	Token tok;

	if (!readNextToken(tok))
	{
		return false;
	}

	// If the given string is available:
	if (tok == string)
	{
		return true;
	}

	// Unread token:
	scriptPtr  = lastScriptPtr;
	lineNumber = lastLine;
	return false;
}

// ========================================================
// Lexer::checkTokenString():
// ========================================================

bool Lexer::checkTokenString(const char * string, Token & tokenOut)
{
	if (!readNextToken(tokenOut))
	{
		return false;
	}

	// If the given string is available:
	if (tokenOut == string)
	{
		return true;
	}

	// Unread token:
	scriptPtr  = lastScriptPtr;
	lineNumber = lastLine;
	return false;
}

// ========================================================
// Lexer::checkTokenType():
// ========================================================

bool Lexer::checkTokenType(const int type, const int subtype, Token & tokenOut)
{
	Token tok;

	if (!readNextToken(tok))
	{
		return false;
	}

	if (tok.type == type && (tok.subtype & subtype) == subtype)
	{
		tokenOut = tok;
		return true;
	}

	// Unread token:
	scriptPtr  = lastScriptPtr;
	lineNumber = lastLine;
	return false;
}

// ========================================================
// Lexer::peekTokenString():
// ========================================================

bool Lexer::peekTokenString(const char * string)
{
	Token tok;
	if (!readNextToken(tok))
	{
		return false;
	}

	// Unread token:
	scriptPtr  = lastScriptPtr;
	lineNumber = lastLine;

	if (tok == string)
	{
		return true;
	}

	return false;
}

// ========================================================
// Lexer::peekTokenType():
// ========================================================

bool Lexer::peekTokenType(const int type, const int subtype, Token & tokenOut)
{
	Token tok;
	if (!readNextToken(tok))
	{
		return false;
	}

	// Unread token:
	scriptPtr  = lastScriptPtr;
	lineNumber = lastLine;

	if (tok.type == type && (tok.subtype & subtype) == subtype)
	{
		tokenOut = tok;
		return true;
	}

	return false;
}

// ========================================================
// Lexer::skipUntilString():
// ========================================================

bool Lexer::skipUntilString(const char * string)
{
	Token tok;
	while (readNextToken(tok))
	{
		if (tok == string)
		{
			return true;
		}
	}
	return false;
}

// ========================================================
// Lexer::skipRestOfLine():
// ========================================================

bool Lexer::skipRestOfLine()
{
	Token tok;
	while (readNextToken(tok))
	{
		if (tok.linesCrossed)
		{
			scriptPtr  = lastScriptPtr;
			lineNumber = lastLine;
			return true;
		}
	}
	return false;
}

// ========================================================
// Lexer::skipBracedSection():
// ========================================================

bool Lexer::skipBracedSection(const bool parseFirstBrace)
{
	// Skips until a matching close brace is found.
	// Internal brace depths are properly skipped.

	Token tok;
	int depth = parseFirstBrace ? 0 : 1;

	do
	{
		if (!readNextToken(tok))
		{
			return false;
		}

		if (tok.type == Token::Type::Punctuation)
		{
			if (tok == "{")
			{
				++depth;
			}
			else if (tok == "}")
			{
				--depth;
			}
		}
	}
	while (depth);

	return true;
}

// ========================================================
// Lexer::unreadToken():
// ========================================================

void Lexer::unreadToken(const Token & tokenIn)
{
	if (tokenAvailable)
	{
		warning("Lexer::unreadToken() called twice in a row!");
	}

	leftoverToken  = tokenIn;
	tokenAvailable = true;
}

// ========================================================
// Lexer::readRestOfLine():
// ========================================================

const char * Lexer::readRestOfLine(String & out)
{
	for (;;)
	{
		if (*scriptPtr == '\n')
		{
			lineNumber++;
			break;
		}
		if (*scriptPtr == '\0')
		{
			break;
		}

		if (*scriptPtr <= ' ')
		{
			out += " ";
		}
		else
		{
			out += *scriptPtr;
		}

		scriptPtr++;
	}

	out.trim();
	return out.getCStr();
}

// ========================================================
// Lexer::scanInt():
// ========================================================

int Lexer::scanInt()
{
	Token tok;
	if (!readNextToken(tok))
	{
		return error("Couldn't read expected integer!");
	}

	if (tok.type == Token::Type::Punctuation && tok == "-")
	{
		expectTokenType(Token::Type::Number, Token::Flag::Integer, tok);
		return -(static_cast<signed int>(tok.getIntValue()));
	}
	else if (tok.type != Token::Type::Number || tok.subtype == Token::Flag::Float)
	{
		warning("Expected integer value, found \'" + tok.text + "\'.");
	}

	return tok.getIntValue();
}

// ========================================================
// Lexer::scanBool():
// ========================================================

bool Lexer::scanBool()
{
	Token tok;
	if (!expectTokenType(Token::Type::Number, 0, tok))
	{
		return error("Couldn't read expected boolean!");
	}
	return tok.getIntValue() > 0;
}

// ========================================================
// Lexer::scanFloat():
// ========================================================

float Lexer::scanFloat(bool * errorFlag)
{
	return static_cast<float>(scanDouble(errorFlag));
}

// ========================================================
// Lexer::scanDouble():
// ========================================================

double Lexer::scanDouble(bool * errorFlag)
{
	if (errorFlag)
	{
		*errorFlag = false;
	}

	Token tok;
	if (!readNextToken(tok))
	{
		if (errorFlag)
		{
			warning("Couldn't read expected floating point number!");
			*errorFlag = true;
		}
		else
		{
			error("Couldn't read expected floating point number!");
		}
		return 0.0;
	}

	if (tok.type == Token::Type::Punctuation && tok == "-")
	{
		expectTokenType(Token::Type::Number, 0, tok);
		return -tok.getDoubleValue();
	}
	else if (tok.type != Token::Type::Number)
	{
		if (errorFlag)
		{
			warning("Expected float value, found \'" + tok.text + "\'.");
			*errorFlag = true;
		}
		else
		{
			error("Expected float value, found \'" + tok.text + "\'.");
		}
	}

	return tok.getDoubleValue();
}

// ========================================================
// Lexer::scanFloatMatrix1():
// ========================================================

bool Lexer::scanFloatMatrix1(const int x, float * m)
{
	if (!expectTokenString("("))
	{
		return false;
	}

	for (int i = 0; i < x; ++i)
	{
		m[i] = scanFloat();
	}

	if (!expectTokenString(")"))
	{
		return false;
	}

	return true;
}

// ========================================================
// Lexer::scanFloatMatrix2():
// ========================================================

bool Lexer::scanFloatMatrix2(const int y, const int x, float * m)
{
	if (!expectTokenString("("))
	{
		return false;
	}

	for (int i = 0; i < y; ++i)
	{
		if (!scanFloatMatrix1(x, m + i * x))
		{
			return false;
		}
	}

	if (!expectTokenString(")"))
	{
		return false;
	}

	return true;
}

// ========================================================
// Lexer::scanFloatMatrix3():
// ========================================================

bool Lexer::scanFloatMatrix3(const int z, const int y, const int x, float * m)
{
	if (!expectTokenString("("))
	{
		return false;
	}

	for (int i = 0; i < z; ++i)
	{
		if (!scanFloatMatrix2(y, x, m + i * x * y))
		{
			return false;
		}
	}

	if (!expectTokenString(")"))
	{
		return false;
	}

	return true;
}

// ========================================================
// Lexer::scanBracedSectionExact():
// ========================================================

const char * Lexer::scanBracedSectionExact(String & out, int tabs)
{
	out.clear();
	if (!expectTokenString("{"))
	{
		return out.getCStr();
	}

	out = "{";
	int  depth     = 1;
	bool skipWhite = false;
	bool doTabs    = (tabs >= 0);

	while (depth && *scriptPtr)
	{
		const char c = *(scriptPtr++);
		switch (c)
		{
		case '\t' :
		case ' '  :
			if (skipWhite)
			{
				continue;
			}
			break;

		case '\n' :
			if (doTabs)
			{
				out += c;
				skipWhite = true;
				continue;
			}
			break;

		case '{' :
			depth++;
			tabs++;
			break;

		case '}' :
			depth--;
			tabs--;
			break;

		default :
			break;
		} // switch (c)

		if (skipWhite)
		{
			int i = tabs;
			if (c == '{')
			{
				i--;
			}

			skipWhite = false;
			for (; i > 0; --i)
			{
				out += '\t';
			}
		}
		out += c;
	}

	return out.getCStr();
}

// ========================================================
// Lexer::scanBracedSection():
// ========================================================

const char * Lexer::scanBracedSection(String & out)
{
	// The next token should be an open brace.
	// Scans until a matching close brace is found.
	// Internal brace depths are properly skipped.

	Token tok;
	int i, depth;

	out.clear();
	if (!expectTokenString("{"))
	{
		return out.getCStr();
	}

	out   = "{";
	depth = 1;

	do
	{
		if (!readNextToken(tok))
		{
			error("Missing closing \'{\'!");
			return out.getCStr();
		}

		// If the token is on a new line...
		for (i = 0; i < tok.linesCrossed; ++i)
		{
			out += "\r\n";
		}

		if (tok.type == Token::Type::Punctuation)
		{
			if (tok[0] == '{')
			{
				++depth;
			}
			else if (tok[0] == '}')
			{
				--depth;
			}
		}

		if (tok.type == Token::Type::String)
		{
			out += "\"" + tok.text + "\"";
		}
		else
		{
			out += tok.text;
		}

		out += " ";
	}
	while (depth);

	return out.getCStr();
}

// ========================================================
// Lexer::scanRestOfLine():
// ========================================================

const char * Lexer::scanRestOfLine(String & out)
{
	Token tok;
	out.clear();

	while (readNextToken(tok))
	{
		if (tok.linesCrossed)
		{
			scriptPtr  = lastScriptPtr;
			lineNumber = lastLine;
			break;
		}

		if (!out.isEmpty())
		{
			out += " ";
		}
		out += tok.text;
	}

	return out.getCStr();
}

// ========================================================
// Lexer::scanCompleteLine():
// ========================================================

const char * Lexer::scanCompleteLine(String & out)
{
	// Returns a string up to the '\n', but doesn't eat any
	// whitespace at the beginning of the next line.

	const char * startPtr = scriptPtr;
	for (;;)
	{
		if (*scriptPtr == '\0')
		{
			break; // End of bufferHeadPtr
		}
		if (*scriptPtr == '\n')
		{
			lineNumber++;
			scriptPtr++;
			break;
		}
		scriptPtr++;
	}

	out.clear();
	out.append(startPtr, static_cast<uint>(scriptPtr - startPtr));
	return out.getCStr();
}

// ========================================================
// Lexer::getLastWhiteSpace():
// ========================================================

uint Lexer::getLastWhiteSpace(String & whiteSpace) const
{
	whiteSpace.clear();
	for (const char * p = whiteSpaceStartPtr; p < whiteSpaceEndPtr; ++p)
	{
		whiteSpace.pushBack(*p);
	}
	return whiteSpace.getLength();
}

// ========================================================
// Lexer::getPunctuationFromId():
// ========================================================

String Lexer::getPunctuationFromId(const int id)
{
	for (int i = 0; punctuationInfo[i].chars != nullptr; ++i)
	{
		if (punctuationInfo[i].id == id)
		{
			return punctuationInfo[i].chars;
		}
	}
	return "Unknown punctuation";
}

// ========================================================
// Lexer::getPunctuationId():
// ========================================================

int Lexer::getPunctuationId(const char * punctuationString)
{
	DEBUG_CHECK(punctuationString != nullptr);

	for (int i = 0; punctuationInfo[i].chars != nullptr; ++i)
	{
		if (strCmp(punctuationInfo[i].chars, punctuationString) == 0)
		{
			return punctuationInfo[i].id;
		}
	}
	return 0; // Zero is reserved for null/undefined punctuation.
}

// ================================================================================================
// Lexer punctuation tables for C/C++-like languages:
// ================================================================================================

enum PunctuationId
{
	// 0 is reserved for null/undefined punctuation.
	P_RSHIFT_ASSIGN = 1,
	P_LSHIFT_ASSIGN,
	P_ELLIPSIS,
	P_PRECOMP_MERGE,
	P_LOGIC_AND,
	P_LOGIC_OR,
	P_LOGIC_GEQ,
	P_LOGIC_LEQ,
	P_LOGIC_EQ,
	P_LOGIC_UNEQ,
	P_MUL_ASSIGN,
	P_DIV_ASSIGN,
	P_MOD_ASSIGN,
	P_ADD_ASSIGN,
	P_SUB_ASSIGN,
	P_INCR,
	P_DECR,
	P_BIN_AND_ASSIGN,
	P_BIN_OR_ASSIGN,
	P_BIN_XOR_ASSIGN,
	P_RSHIFT,
	P_LSHIFT,
	P_POINTER_DEREF,
	P_CPP1,
	P_CPP2,
	P_MUL,
	P_DIV,
	P_MOD,
	P_ADD,
	P_SUB,
	P_ASSIGN,
	P_BIN_AND,
	P_BIN_OR,
	P_BIN_XOR,
	P_BIN_NOT,
	P_LOGIC_NOT,
	P_LOGIC_GREATER,
	P_LOGIC_LESS,
	P_DOT,
	P_COMMA,
	P_SEMICOLON,
	P_COLON,
	P_QUESTION_MARK,
	P_PARENTHESES_OPEN,
	P_PARENTHESES_CLOSE,
	P_BRACE_OPEN,
	P_BRACE_CLOSE,
	P_SQ_BRACKET_OPEN,
	P_SQ_BRACKET_CLOSE,
	P_BACKSLASH,
	P_PRECOMP,
	P_DOLLAR
};

const Lexer::Punctuation Lexer::punctuationInfo[53] =
{
	{ ">>=", P_RSHIFT_ASSIGN     },
	{ "<<=", P_LSHIFT_ASSIGN     },
	{ "...", P_ELLIPSIS          },
	{ "##",  P_PRECOMP_MERGE     },
	{ "&&",  P_LOGIC_AND         },
	{ "||",  P_LOGIC_OR          },
	{ ">=",  P_LOGIC_GEQ         },
	{ "<=",  P_LOGIC_LEQ         },
	{ "==",  P_LOGIC_EQ          },
	{ "!=",  P_LOGIC_UNEQ        },
	{ "*=",  P_MUL_ASSIGN        },
	{ "/=",  P_DIV_ASSIGN        },
	{ "%=",  P_MOD_ASSIGN        },
	{ "+=",  P_ADD_ASSIGN        },
	{ "-=",  P_SUB_ASSIGN        },
	{ "++",  P_INCR              },
	{ "--",  P_DECR              },
	{ "&=",  P_BIN_AND_ASSIGN    },
	{ "|=",  P_BIN_OR_ASSIGN     },
	{ "^=",  P_BIN_XOR_ASSIGN    },
	{ ">>",  P_RSHIFT            },
	{ "<<",  P_LSHIFT            },
	{ "->",  P_POINTER_DEREF     },
	{ "::",  P_CPP1              },
	{ ".*",  P_CPP2              },
	{ "*",   P_MUL               },
	{ "/",   P_DIV               },
	{ "%",   P_MOD               },
	{ "+",   P_ADD               },
	{ "-",   P_SUB               },
	{ "=",   P_ASSIGN            },
	{ "&",   P_BIN_AND           },
	{ "|",   P_BIN_OR            },
	{ "^",   P_BIN_XOR           },
	{ "~",   P_BIN_NOT           },
	{ "!",   P_LOGIC_NOT         },
	{ ">",   P_LOGIC_GREATER     },
	{ "<",   P_LOGIC_LESS        },
	{ ".",   P_DOT               },
	{ ",",   P_COMMA             },
	{ ";",   P_SEMICOLON         },
	{ ":",   P_COLON             },
	{ "?",   P_QUESTION_MARK     },
	{ "(",   P_PARENTHESES_OPEN  },
	{ ")",   P_PARENTHESES_CLOSE },
	{ "{",   P_BRACE_OPEN        },
	{ "}",   P_BRACE_CLOSE       },
	{ "[",   P_SQ_BRACKET_OPEN   },
	{ "]",   P_SQ_BRACKET_CLOSE  },
	{ "\\",  P_BACKSLASH         },
	{ "#",   P_PRECOMP           },
	{ "$",   P_DOLLAR            },
	{ nullptr, 0                 }
};

const int Lexer::punctuationTable[256] =
{
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,   9,  -1,   3,
	 51,  12,   4,  -1,  43,  44,
	 10,  13,  39,  14,   2,  11,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  23,  40,
	  1,   8,   0,  42,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  47,  49,  48,  19,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  45,   5,  46,
	 34,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1
};

const int Lexer::nextPunctuation[53] =
{
	  6,   7,  24,  50,  17,  18,
	 20,  21,  30,  35,  25,  26,
	 27,  15,  16,  28,  22,  31,
	 32,  33,  36,  37,  29,  41,
	 38,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1,  -1,
	 -1,  -1,  -1,  -1,  -1
};

} // namespace core {}
} // namespace atlas {}

/*
================================================================================================
           -- ORIGINAL COPYRIGHT NOTICE FROM DOOM 3 BFG (Lexer.cpp and Token.cpp) --

Doom 3 BFG Edition GPL Source Code.
Copyright (C) 1993-2012 id Software LLC, a ZeniMax Media company.

This file is part of the Doom 3 BFG Edition GPL Source Code ("Doom 3 BFG Edition Source Code").

Doom 3 BFG Edition Source Code is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later version.

Doom 3 BFG Edition Source Code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Doom 3 BFG Edition Source Code. If not, see <http://www.gnu.org/licenses/>.

In addition, the Doom 3 BFG Edition Source Code is also subject to certain additional terms.
You should have received a copy of these additional terms immediately following the terms and
conditions of the GNU General Public License which accompanied the Doom 3 BFG Edition Source
Code. If not, please request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional terms, you may
contact in writing id Software LLC, c/o ZeniMax Media Inc., Suite 120, Rockville,
Maryland 20850 USA.

================================================================================================
*/
