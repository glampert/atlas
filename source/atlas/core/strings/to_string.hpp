
// ================================================================================================
// -*- C++ -*-
// File: to_string.hpp
// Author: Guilherme R. Lampert
// Created on: 30/12/14
// Brief: `toString()` functions for common and native types.
// ================================================================================================

#ifndef ATLAS_CORE_STRINGS_TO_STRING_HPP
#define ATLAS_CORE_STRINGS_TO_STRING_HPP

#include "atlas/core/utils/utility.hpp"
#include "atlas/core/strings/dynamic_string.hpp"
#include "atlas/core/strings/sized_string.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// Strings to string - Pass through:
// ========================================================

inline String toString(const char * str)
{
	return (str != nullptr) ? str : "(null)";
}

inline const String & toString(const String & str)
{
	return str;
}

template<uint SIZE>
inline const SizedString<SIZE> & toString(const SizedString<SIZE> & str)
{
	return str;
}

// ========================================================
// Native numerical types to string:
// ========================================================

inline String toString(const bool val)
{
	return val ? "true" : "false";
}

inline String toString(const int32 val, const int base = 10)
{
	char buffer[128] = {0};
	strFromInt(val, buffer, arrayLength(buffer), base);
	return buffer;
}

inline String toString(const uint32 val, const int base = 10)
{
	char buffer[128] = {0};
	strFromInt(val, buffer, arrayLength(buffer), base);
	return buffer;
}

inline String toString(const int64 val, const int base = 10)
{
	char buffer[128] = {0};
	strFromInt(val, buffer, arrayLength(buffer), base);
	return buffer;
}

inline String toString(const uint64 val, const int base = 10)
{
	char buffer[128] = {0};
	strFromInt(val, buffer, arrayLength(buffer), base);
	return buffer;
}

inline String toString(const float val, const int decimalDigits = -1)
{
	char buffer[128] = {0};
	strFromFloat(val, buffer, arrayLength(buffer), decimalDigits);
	return strTrimTrailingFloatZeros(buffer);
}

inline String toString(const double val, const int decimalDigits = -1)
{
	char buffer[128] = {0};
	strFromFloat(val, buffer, arrayLength(buffer), decimalDigits);
	return strTrimTrailingFloatZeros(buffer);
}

inline String toString(const long val, const int base = 10)
{
	char buffer[128] = {0};
	strFromInt(static_cast<int64>(val), buffer, arrayLength(buffer), base);
	return buffer;
}

inline String toString(const ulong val, const int base = 10)
{
	char buffer[128] = {0};
	strFromInt(static_cast<uint64>(val), buffer, arrayLength(buffer), base);
	return buffer;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_STRINGS_TO_STRING_HPP
