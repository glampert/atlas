
// ================================================================================================
// -*- C++ -*-
// File: memory.hpp
// Author: Guilherme R. Lampert
// Created on: 19/04/15
// Brief: Master header of the memory management submodule.
//        Including this header makes all the memory functions and types available.
// ================================================================================================

#ifndef ATLAS_CORE_MEMORY_HPP
#define ATLAS_CORE_MEMORY_HPP

#include "atlas/core/memory/mem_debug.hpp"
#include "atlas/core/memory/mem_utils.hpp"
#include "atlas/core/memory/auto_ptr.hpp"
#include "atlas/core/memory/object_pool.hpp"

#endif // ATLAS_CORE_MEMORY_HPP
