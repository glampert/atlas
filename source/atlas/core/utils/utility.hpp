
// ================================================================================================
// -*- C++ -*-
// File: utility.hpp
// Author: Guilherme R. Lampert
// Created on: 08/01/15
// Brief: Miscellaneous utility functions and helpers.
// ================================================================================================

#ifndef ATLAS_CORE_UTILS_UTILITY_HPP
#define ATLAS_CORE_UTILS_UTILITY_HPP

#include "atlas/core/utils/basic_types.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// arrayLength<T, N>():
// ========================================================

// Length in elements of type 'T' of statically allocated C++ arrays.
template<class T, uint N>
constexpr uint arrayLength(const T (&)[N])
{
	return N;
}

// ========================================================
// clamp<T>():
// ========================================================

// Clamp any value within minimum/maximum range, inclusive.
template<class T>
constexpr T clamp(const T & x, const T & minimum, const T & maximum)
{
	return (x < minimum) ? minimum : (x > maximum) ? maximum : x;
}

// ========================================================
// min<T>():
// ========================================================

// Returns the smaller of the given values.
template<class T>
constexpr T min(const T & a, const T & b)
{
	return (b < a) ? b : a;
}

// ========================================================
// max<T>():
// ========================================================

// Returns the greater of the given values.
template<class T>
constexpr T max(const T & a, const T & b)
{
	return (a < b) ? b : a;
}

// ========================================================
// swap<T>():
// ========================================================

// Swap two objects, without testing if they are already equal.
template<class T>
void swap(T & a, T & b)
{
	T c(a);
	a = b;
	b = c;
}

// ========================================================
// begin<T, N>():
// ========================================================

// Provided for compatibility with statically declared arrays and `foreach()`.
template<class T, uint N>
constexpr T * begin(T (&array)[N])
{
	return array;
}

// ========================================================
// end<T, N>():
// ========================================================

// Provided for compatibility with statically declared arrays and `foreach()`.
template<class T, uint N>
constexpr T * end(T (&array)[N])
{
	return array + N;
}

// ========================================================
// template class TypeTraits<T>:
// ========================================================

// Class template TypeTraits.
// Figures out at compile time a few properties of any given type.
// Can also be used to remove qualifiers from a type T.
template<class T>
class TypeTraits final
	: private NonCopyable
{
private:

	// There is no reason for declaring instances of TypeTraits.
	TypeTraits() DELETED_METHOD;

	// ---- UnConst: ----

	template<class U> struct UnConst
	{
		typedef U Result;
		enum { IsConst = false };
	};

	template<class U> struct UnConst<const U>
	{
		typedef U Result;
		enum { IsConst = true };
	};

	template<class U> struct UnConst<const U &>
	{
		typedef U& Result;
		enum { IsConst = true };
	};

	// ---- UnVolatile: ----

	template<class U> struct UnVolatile
	{
		typedef U Result;
		enum { IsVolatile = false };
	};

	template<class U> struct UnVolatile<volatile U>
	{
		typedef U Result;
		enum { IsVolatile = true };
	};

	template<class U> struct UnVolatile<volatile U &>
	{
		typedef U& Result;
		enum { IsVolatile = true };
	};

	// ---- UnRef: ----

	template<class U> struct UnRef
	{
		typedef U Result;
		enum { IsReference = false };
	};

	template<class U> struct UnRef<U &>
	{
		typedef U Result;
		enum { IsReference = true };
	};

public:

	// Non constant type:
	typedef typename UnConst<T>::Result NonConstType;

	// Non volatile type:
	typedef typename UnVolatile<T>::Result NonVolatileType;

	// NonConstType and NonVolatileType type:
	typedef typename UnVolatile<typename UnConst<T>::Result>::Result UnqualifiedType;

	// Type referred by T& or T (removed ref):
	typedef typename UnRef<T>::Result RemoveReference;

	// Type referred by const T& or T (removes ref and const qualifier):
	typedef typename UnRef<typename UnConst<T>::Result>::Result RemoveConstReference;
};

// ========================================================
// template struct CmpEqual<T>:
// ========================================================

// Functor to compare 'a' and 'b' with `operator ==`:
template<class T>
struct CmpEqual
{
	bool operator() (const T & a, const T & b) const
	{
		return a == b;
	}
};

// ========================================================
// template struct CmpNotEqual<T>:
// ========================================================

// Functor to compare 'a' and 'b' with `operator !=`:
template<class T>
struct CmpNotEqual
{
	bool operator() (const T & a, const T & b) const
	{
		return a != b;
	}
};

// ========================================================
// template struct CmpLess<T>:
// ========================================================

// Functor to compare 'a' and 'b' with `operator <`:
template<class T>
struct CmpLess
{
	bool operator() (const T & a, const T & b) const
	{
		return a < b;
	}
};

// ========================================================
// template struct CmpLessEqual<T>:
// ========================================================

// Functor to compare 'a' and 'b' with `operator <=`:
template<class T>
struct CmpLessEqual
{
	bool operator() (const T & a, const T & b) const
	{
		return a <= b;
	}
};

// ========================================================
// template struct CmpGreater<T>:
// ========================================================

// Functor to compare 'a' and 'b' with `operator >`:
template<class T>
struct CmpGreater
{
	bool operator() (const T & a, const T & b) const
	{
		return a > b;
	}
};

// ========================================================
// template struct CmpGreaterEqual<T>:
// ========================================================

// Functor to compare 'a' and 'b' with `operator >=`:
template<class T>
struct CmpGreaterEqual
{
	bool operator() (const T & a, const T & b) const
	{
		return a >= b;
	}
};

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_UTILS_UTILITY_HPP
