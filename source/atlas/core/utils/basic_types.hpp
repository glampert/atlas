
// ================================================================================================
// -*- C++ -*-
// File: basic_types.hpp
// Author: Guilherme R. Lampert
// Created on: 17/12/14
// Brief: Basic native types, some common data structures and common constants.
// ================================================================================================

#ifndef ATLAS_CORE_UTILS_BASIC_TYPES_HPP
#define ATLAS_CORE_UTILS_BASIC_TYPES_HPP

#include "atlas/common.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// Sized native types:
// ========================================================

// ---- 8bit integers ----
typedef std::int8_t  int8;
typedef std::uint8_t uint8;

// ---- 16bit integers ----
typedef std::int16_t  int16;
typedef std::uint16_t uint16;

// ---- 32bit integers ----
typedef std::int32_t  int32;
typedef std::uint32_t uint32;

// ---- 64bit integers ----
typedef std::int64_t  int64;
typedef std::uint64_t uint64;

// ---- Floating-point types ----
typedef float  float32;
typedef double float64;

// ---- Misc aliases ----
typedef int8  byte;
typedef uint8 ubyte;
typedef unsigned int  uint;
typedef unsigned long ulong;

// ========================================================
// Constants/limits for the types above:
// ========================================================

// ---- 8bit integers ----
constexpr int8   int8Min   = INT8_MIN;
constexpr int8   int8Max   = INT8_MAX;
constexpr uint8  uint8Max  = UINT8_MAX;

// ---- 16bit integers ----
constexpr int16  int16Min  = INT16_MIN;
constexpr int16  int16Max  = INT16_MAX;
constexpr uint16 uint16Max = UINT16_MAX;

// ---- 32bit integers ----
constexpr int32  int32Min  = INT32_MIN;
constexpr int32  int32Max  = INT32_MAX;
constexpr uint32 uint32Max = UINT32_MAX;

// ---- 64bit integers ----
constexpr int64  int64Min  = INT64_MIN;
constexpr int64  int64Max  = INT64_MAX;
constexpr uint64 uint64Max = UINT64_MAX;

// ---- byte/ubyte ----
constexpr byte   byteMin   = INT8_MIN;
constexpr byte   byteMax   = INT8_MAX;
constexpr ubyte  ubyteMax  = UINT8_MAX;

// ---- int/uint/ulong ----
constexpr int    intMin    = INT_MIN;
constexpr int    intMax    = INT_MAX;
constexpr uint   uintMax   = UINT_MAX;
constexpr ulong  ulongMax  = ULONG_MAX;

// ========================================================
// class NonCopyable:
// ========================================================

// Classes/structures that inherit privately from this
// type will have copy construction and assignment disabled.
class NonCopyable
{
protected:

	 NonCopyable() DEFAULTED_METHOD;
	~NonCopyable() DEFAULTED_METHOD;

private:

	NonCopyable(const NonCopyable &) DELETED_METHOD;
	NonCopyable & operator = (const NonCopyable &) DELETED_METHOD;
};

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_UTILS_BASIC_TYPES_HPP
