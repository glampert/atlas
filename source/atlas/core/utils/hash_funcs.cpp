
// ================================================================================================
// -*- C++ -*-
// File: hash_funcs.cpp
// Author: Guilherme R. Lampert
// Created on: 08/01/15
// Brief: Common hash functions, such as CRC-32.
// ================================================================================================

#include "atlas/core/utils/hash_funcs.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// hashCrc32():
// ========================================================

uint32 hashCrc32(const void * data, uint sizeBytes)
{
	DEBUG_CHECK(data != nullptr);
	DEBUG_CHECK(sizeBytes != 0);

	// This compact CRC-32 algorithm was adapted from 'miniz.c', which in turn was taken from
	// "A compact CCITT crc16 and crc32 C implementation that balances processor cache usage against speed"
	// By Karl Malbrain.
	static const uint32 crcTable[16] =
	{
		0,
		0x1DB71064,
		0x3B6E20C8,
		0x26D930AC,
		0x76DC4190,
		0x6B6B51F4,
		0x4DB26158,
		0x5005713C,
		0xEDB88320,
		0xF00F9344,
		0xD6D6A3E8,
		0xCB61B38C,
		0x9B64C2B0,
		0x86D3D2D4,
		0xA00AE278,
		0xBDBDF21C
	};

	const ubyte * ptr = reinterpret_cast<const ubyte *>(data);
	uint32 h = 0;

	h = ~h;
	while (sizeBytes--)
	{
		ubyte b = *ptr++;
		h = (h >> 4) ^ crcTable[(h & 0xF) ^ (b & 0xF)];
		h = (h >> 4) ^ crcTable[(h & 0xF) ^ (b >> 4) ];
	}

	return ~h;
}

// ========================================================
// hashOat():
// ========================================================

uint32 hashOat(const void * data, uint sizeBytes)
{
	DEBUG_CHECK(data != nullptr);
	DEBUG_CHECK(sizeBytes != 0);

	const ubyte * ptr = reinterpret_cast<const ubyte *>(data);
	uint32 h = 0;

	for (uint i = 0; i < sizeBytes; ++i)
	{
		h += ptr[i];
		h += (h << 10);
		h ^= (h >>  6);
	}

	h += (h <<  3);
	h ^= (h >> 11);
	h += (h << 15);

	return h;
}

// ========================================================
// hashMurMur32():
// ========================================================

uint32 hashMurMur32(const void * data, uint sizeBytes)
{
	//
	// Based entirely on the implementation provided by Wikipedia:
	//   https://en.wikipedia.org/wiki/MurmurHash
	//

	constexpr uint32 Seed = 0x8E96A8F2;
	constexpr uint32 C1   = 0xCC9E2D51;
	constexpr uint32 C2   = 0x1B873593;
	constexpr uint32 N    = 0xE6546B64;
	constexpr uint32 M    = 5;
	constexpr uint32 R1   = 15;
	constexpr uint32 R2   = 13;

	DEBUG_CHECK(data != nullptr);
	DEBUG_CHECK(sizeBytes != 0);

	const uint32 * blocks = reinterpret_cast<const uint32 *>(data);
	const uint blockCount = sizeBytes / sizeof(uint32);

	uint32 hash = Seed;
	for (uint i = 0; i < blockCount; ++i)
	{
		uint32 k = blocks[i];
		k *= C1;
		k  = (k << R1) | (k >> (32 - R1));
		k *= C2;

		hash ^= k;
		hash  = ((hash << R2) | (hash >> (32 - R2))) * M + N;
	}

	const uint8 * key  = reinterpret_cast<const uint8 *>(data);
	const uint8 * tail = key + (blockCount * sizeof(uint32));
	uint32 k1 = 0;

	switch (sizeBytes & 3)
	{
	case 3 : k1 ^= tail[2] << 16;
	case 2 : k1 ^= tail[1] << 8;
	case 1 : k1 ^= tail[0];
		k1   *= C1;
		k1    = (k1 << R1) | (k1 >> (32 - R1));
		k1   *= C2;
		hash ^= k1;
	} // switch (sizeBytes & 3)

	hash ^= sizeBytes;
	hash ^= (hash >> 16);
	hash *= 0x85EBCA6B;
	hash ^= (hash >> 13);
	hash *= 0xC2B2AE35;
	hash ^= (hash >> 16);

	return hash;
}

} // namespace core {}
} // namespace atlas {}
