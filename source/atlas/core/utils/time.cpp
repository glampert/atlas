
// ================================================================================================
// -*- C++ -*-
// File: time.cpp
// Author: Guilherme R. Lampert
// Created on: 08/09/15
// Brief: Clocks and timers.
// ================================================================================================

#include "atlas/core/utils/time.hpp"
#include "atlas/core/utils/default_log.hpp"

// OSX time utilities:
#include <mach/mach_time.h> // mach_absolute_time()/mach_timebase_info()

namespace atlas
{
namespace core
{

// ========================================================
// Time to string:
// ========================================================

String toString(const Time t)
{
	return "{ micro:" + core::toString(t.asMicroseconds()) +
	       ", milli:" + core::toString(t.asMilliseconds()) +
	       ", sec:"   + core::toString(t.asSeconds()) +
	       ", min:"   + core::toString(t.asMinutes()) +
	       ", hours:" + core::toString(t.asHours()) + " }";
}

// ================================================================================================
// Clock implementation:
// ================================================================================================

Clock::Clock()
	: frequency(0)
	, baseTime(0)
{
	// mach_absolute_time() is in nanoseconds precision!
	baseTime = static_cast<int64>(mach_absolute_time());

	// Get the clock frequency once and store.
	// This value doesn't change while the system is running.
	mach_timebase_info_data_t timeBaseInfo;
	const kern_return_t result = mach_timebase_info(&timeBaseInfo);

	if (result == KERN_SUCCESS)
	{
		frequency = (timeBaseInfo.numer / timeBaseInfo.denom);
	}
	else
	{
		LOG_FATAL("High resolution clock is unavailable!");
	}
}

Time Clock::getTime() const
{
	// mach_absolute_time() is in nanoseconds precision!
	return microseconds(((static_cast<int64>(mach_absolute_time()) - baseTime) * frequency) / 1000);
}

Clock::Timestamp Clock::getCalendarTimestamp() const
{
	std::time_t t;
	std::time(&t);
	return t;
}

DateTime Clock::timestampToLocalDateTime(const Clock::Timestamp timestamp)
{
	const std::time_t t = timestamp;
	const struct tm * timeInfo = std::localtime(&t);

	DateTime dateTime;
	dateTime.day     = timeInfo->tm_mday;
	dateTime.month   = timeInfo->tm_mon  + 1;    // From 1 to 12 instead of 0 to 11.
	dateTime.year    = timeInfo->tm_year + 1900; // Get the actual year, not the number of years elapsed since 1900.
	dateTime.weekDay = timeInfo->tm_wday;
	dateTime.yearDay = timeInfo->tm_yday;
	dateTime.seconds = timeInfo->tm_sec;
	dateTime.minutes = timeInfo->tm_min;
	dateTime.hour    = timeInfo->tm_hour;
	return dateTime;
}

String Clock::timestampString(const Clock::Timestamp timestamp)
{
	String timeStr;

	if (timestamp != 0)
	{
		timeStr = std::ctime(&timestamp);
		timeStr.popBack(); // Remove the default new line added by `ctime()`.
	}
	else
	{
		timeStr = "(null)";
	}

	return timeStr;
}

} // namespace core {}
} // namespace atlas {}
