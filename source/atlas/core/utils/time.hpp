
// ================================================================================================
// -*- C++ -*-
// File: time.hpp
// Author: Guilherme R. Lampert
// Created on: 08/09/15
// Brief: Clocks and timers.
// ================================================================================================

#ifndef ATLAS_CORE_UTILS_TIME_HPP
#define ATLAS_CORE_UTILS_TIME_HPP

#include "atlas/core/utils/basic_types.hpp"
#include "atlas/core/strings/to_string.hpp"
#include <ctime>

namespace atlas
{
namespace core
{

// ========================================================
// struct DateTime:
// ========================================================

struct DateTime final
{
	// Date:
	int day;     // Day of the month:         1-31
	int month;   // Months since January:     1-12
	int year;    // Current year.
	int weekDay; // Days since Sunday:        0-6
	int yearDay; // Days since January 1:     0-365

	// Time of the day:
	int seconds; // Seconds after the minute: 0-59
	int minutes; // Minutes after the hour:   0-59
	int hour;    // Hours since midnight:     0-23
};

// ========================================================
// class Time:
// ========================================================

// A class the behaves much like an ordinary integer value
// but disallows implicit conversions between unrelated time
// scaled (seconds, milliseconds, etc). The finest resolution
// available is microseconds.
class Time final
{
public:

	// Constructs a zero/null time.
	Time();

	// Time scale conversions:
	double asHours()        const;
	double asMinutes()      const;
	double asSeconds()      const;
	int64  asMilliseconds() const;
	int64  asMicroseconds() const;

	// Create time instances from raw values:
	friend Time hours(double h);
	friend Time minutes(double min);
	friend Time seconds(double sec);
	friend Time milliseconds(int64 millis);
	friend Time microseconds(int64 microsec);

	// Comparison operators:
	bool operator == (Time other) const;
	bool operator != (Time other) const;
	bool operator <  (Time other) const;
	bool operator >  (Time other) const;
	bool operator <= (Time other) const;
	bool operator >= (Time other) const;

	// Arithmetical ops:
	Time   operator -  () const;
	Time   operator +  (Time other) const;
	Time   operator -  (Time other) const;
	Time   operator *  (Time other) const;
	Time   operator /  (Time other) const;
	Time & operator += (Time other);
	Time & operator -= (Time other);
	Time & operator *= (Time other);
	Time & operator /= (Time other);

private:

	// Can only create an initialized Time from the friend functions.
	explicit Time(int64 microsec);

	// Stored using the finest grained resolution available.
	int64 microseconds;
};

String toString(const Time t);

// ========================================================
// class Clock:
// ========================================================

class Clock final
{
public:

	typedef std::time_t Timestamp;

	Clock();

	// Get the time elapsed since the application started.
	Time getTime() const;

	// Returns the current calendar time encoded as a Timestamp object.
	Timestamp getCalendarTimestamp() const;

	// Uses the value of `timestamp` to fill a structure with values that
	// represent the corresponding time, expressed for the local timezone.
	static DateTime timestampToLocalDateTime(Timestamp timestamp);

	// Interprets the value of `timestamp` as a calendar time and converts it to a string containing
	// a human-readable version of the corresponding time and date, in terms of local time.
	// The returned string has the following format: "Www Mmm dd hh:mm:ss yyyy".
	static String timestampString(Timestamp timestamp);

private:

	// Implementation defined time scales.
	// Treat as opaque values until converted to a Time object.
	int64 frequency;
	int64 baseTime;
};

// ================================================================================================
// Time inline methods:
// ================================================================================================

inline Time::Time()
	: microseconds(0)
{ }

inline Time::Time(const int64 microsec)
	: microseconds(microsec)
{ }

inline bool Time::operator == (const Time other) const { return microseconds == other.microseconds; }
inline bool Time::operator != (const Time other) const { return microseconds != other.microseconds; }
inline bool Time::operator <  (const Time other) const { return microseconds <  other.microseconds; }
inline bool Time::operator >  (const Time other) const { return microseconds >  other.microseconds; }
inline bool Time::operator <= (const Time other) const { return microseconds <= other.microseconds; }
inline bool Time::operator >= (const Time other) const { return microseconds >= other.microseconds; }

inline Time Time::operator - () const { return Time(-microseconds); }
inline Time Time::operator + (const Time other) const { return Time(microseconds + other.microseconds); }
inline Time Time::operator - (const Time other) const { return Time(microseconds - other.microseconds); }
inline Time Time::operator * (const Time other) const { return Time(microseconds * other.microseconds); }
inline Time Time::operator / (const Time other) const { return Time(microseconds / other.microseconds); }

inline Time & Time::operator += (const Time other) { microseconds += other.microseconds; return *this; }
inline Time & Time::operator -= (const Time other) { microseconds -= other.microseconds; return *this; }
inline Time & Time::operator *= (const Time other) { microseconds *= other.microseconds; return *this; }
inline Time & Time::operator /= (const Time other) { microseconds /= other.microseconds; return *this; }

//
// Time conversions:
//

inline double Time::asHours() const
{
	return static_cast<double>(microseconds) / 3600000000.0;
}

inline double Time::asMinutes() const
{
	return static_cast<double>(microseconds) / 60000000.0;
}

inline double Time::asSeconds() const
{
	return static_cast<double>(microseconds) / 1000000.0;
}

inline int64 Time::asMilliseconds() const
{
	return microseconds / 1000;
}

inline int64 Time::asMicroseconds() const
{
	return microseconds;
}

//
// Create time values with explicit scales:
//

inline Time hours(const double h)
{
	return Time(static_cast<int64>(h * 3600000000.0));
}

inline Time minutes(const double min)
{
	return Time(static_cast<int64>(min * 60000000.0));
}

inline Time seconds(const double sec)
{
	return Time(static_cast<int64>(sec * 1000000.0));
}

inline Time milliseconds(const int64 millis)
{
	return Time(millis * 1000);
}

inline Time microseconds(const int64 microsec)
{
	return Time(microsec);
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_UTILS_TIME_HPP
