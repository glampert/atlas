
// ================================================================================================
// -*- C++ -*-
// File: default_log.cpp
// Author: Guilherme R. Lampert
// Created on: 09/07/15
// Brief: Defines a default log stream that can be used by clients of the Core Library.
// ================================================================================================

#include "atlas/core/utils/default_log.hpp"
#include "atlas/core/strings/to_string.hpp"
#include "atlas/core/fs/path_utils.hpp"
#include <cstdlib> // For std::abort

namespace atlas
{
namespace core
{

// ========================================================
// The default Atlas log stream:
// ========================================================

#if (ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE && ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC)
	//
	// Suppress "declaration requires an exit-time destructor" and
	// "declaration requires a global destructor" on Clang.
	//
	// Logs are the only exceptions to the no global constructors/destructors
	// rule we have adopted for the Atlas library.
	//
	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wexit-time-destructors"
	#pragma GCC diagnostic ignored "-Wglobal-constructors"
#endif // ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE && ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC

LogStream & getLog()
{
#ifndef DEFAULT_LOG_FILENAME
	#define DEFAULT_LOG_FILENAME "atlas_core.log"
#endif // DEFAULT_LOG_FILENAME

#ifndef DEFAULT_LOG_FILE_UNBUFFERED
	#define DEFAULT_LOG_FILE_UNBUFFERED false
#endif // DEFAULT_LOG_FILE_UNBUFFERED

#if ATLAS_DEFAULT_LOG_STDOUT
	static StdLogStream theLog(StdLogStream::StdOut);
#elif DEFAULT_LOG_STDERR
	static StdLogStream theLog(StdLogStream::StdErr);
#elif DEFAULT_LOG_USER_FILE
	static FileLogStream theLog(DEFAULT_LOG_FILENAME, DEFAULT_LOG_FILE_UNBUFFERED);
#else // DEFAULT_LOG_NULL_IMPL | anything else
	static NullLogStream theLog;
#endif // DEFAULT_LOG_XYZ

	return theLog;
}

#if (ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE && ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC)
	#pragma GCC diagnostic pop
#endif // ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE && ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC

// ========================================================
// Log output verbosity level:
// ========================================================

static int gLogVerbosityLvl = LogLevel::Info;

int getLogVerbosityLevel()
{
	return gLogVerbosityLvl;
}

void setLogVerbosityLevel(const int newLevel)
{
	gLogVerbosityLvl = newLevel;
}

// ========================================================
// ANSI Color Codes:
// ========================================================

#if ATLAS_ENABLE_ANSI_COLOR_CODES
	//
	// ANSI color codes help visualizing debug strings
	// when printed to a terminal or console.
	//
	const char * AnsiColorCodes::Red     = "\033[31;1m";
	const char * AnsiColorCodes::Green   = "\033[32;1m";
	const char * AnsiColorCodes::Yellow  = "\033[33;1m";
	const char * AnsiColorCodes::Blue    = "\033[34;1m";
	const char * AnsiColorCodes::Magenta = "\033[35;1m";
	const char * AnsiColorCodes::Cyan    = "\033[36;1m";
	const char * AnsiColorCodes::White   = "\033[37;1m";
	const char * AnsiColorCodes::Default = "\033[0;1m"; // Restore terminal's default.
#else // !ATLAS_ENABLE_ANSI_COLOR_CODES
	//
	// Might want to disable the color codes and use
	// these no-ops if the logs are being redirected to a file.
	//
	const char * AnsiColorCodes::Red     = "";
	const char * AnsiColorCodes::Green   = "";
	const char * AnsiColorCodes::Yellow  = "";
	const char * AnsiColorCodes::Blue    = "";
	const char * AnsiColorCodes::Magenta = "";
	const char * AnsiColorCodes::Cyan    = "";
	const char * AnsiColorCodes::White   = "";
	const char * AnsiColorCodes::Default = "";
#endif // ATLAS_ENABLE_ANSI_COLOR_CODES

// ========================================================
// makeLogMessagePrefix():
// ========================================================

SizedString<256> makeLogMessagePrefix(const String & typeToken, const char * filename, const int lineNum)
{
	const int lastSlash = strFindLast(filename, getPathSeparatorChar());
	const char * colorCode;

	switch (typeToken[1])
	{
	case 'f' : colorCode = AnsiColorCodes::Magenta; break; // Fatal errors
	case 'e' : colorCode = AnsiColorCodes::Red;     break; // Errors
	case 'w' : colorCode = AnsiColorCodes::Yellow;  break; // Warnings
	case 'i' : colorCode = AnsiColorCodes::Green;   break; // Info/tracing
	default  : colorCode = AnsiColorCodes::Default; break; // Unspecified
	} // switch (typeToken[1])

	// Produces something like:
	//   "[i] main.cpp(22): your message here"
	//
	SizedString<256> msgPrefix = colorCode + typeToken + " ";
	msgPrefix += (lastSlash >= 0) ? &filename[lastSlash + 1] : filename;
	msgPrefix += "(" + toString(lineNum) + ") ";
	msgPrefix = msgPrefix.padRight(42);
	msgPrefix += AnsiColorCodes::Default;
	msgPrefix +=  ": ";
	return msgPrefix;
}

// ========================================================
// errorAbort(): [noreturn]
// ========================================================

ATTRIBUTE_NO_RETURN void errorAbort()
{
	// TODO:
	//  Should also print a stack trace,
	//  but not sure if this is the place for that...
	//
	std::abort();
}

} // namespace core {}
} // namespace atlas {}
