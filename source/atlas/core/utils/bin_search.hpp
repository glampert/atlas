
// ================================================================================================
// -*- C++ -*-
// File: bin_search.hpp
// Author: Guilherme R. Lampert
// Created on: 08/01/15
// Brief: Defines the `binarySearch()` template function and helper types.
// ================================================================================================

#ifndef ATLAS_CORE_UTILS_BIN_SEARCH_HPP
#define ATLAS_CORE_UTILS_BIN_SEARCH_HPP

#include "atlas/core/utils/basic_types.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// template struct DefaultBinarySearchPredicate<T, U>:
// ========================================================

// Default predicate that can be used with `binarySearch()`.
// Applies operators < and > to type T and U.
template<class T, class U = T>
struct DefaultBinarySearchPredicate
{
	int operator() (const T & value, const U & needle) const
	{
		if (value < needle) { return -1; }
		if (value > needle) { return +1; }
		return 0;
	}
};

// ========================================================
// binarySearch<T, U, PRED>():
// ========================================================

// Binary search of a value is a sorted array. Returns index of key in the array or -1 if not found.
// Predicate takes the form: `int pred(const T & value, const U & needle)`, returning -1,0,+1.
template<class T, class U, class PRED>
int binarySearch(const T * const haystack, const uint elementCount, const U & needle, const PRED & pred)
{
	DEBUG_CHECK(haystack != nullptr);
	if (elementCount == 0)
	{
		return -1; // Empty array
	}

	uint lower = 0;
	uint upper = elementCount;

	while (lower < upper)
	{
		const uint index  = (lower + upper) / 2;
		const T & current = haystack[index];
		const int result  = pred(current, needle);

		if (result > 0)
		{
			upper = index;
		}
		else if (result < 0)
		{
			lower = index + 1;
		}
		else
		{
			return index;
		}
	}

	return -1; // Not found
}

// ========================================================
// binarySearch<T, N, U, PRED>():
// ========================================================

// Overload of `binarySearch()` that operates on statically allocated arrays.
template<class T, uint N, class U, class PRED>
int binarySearch(const T (&haystack)[N], const U & needle, const PRED & pred)
{
	return binarySearch(haystack, N, needle, pred);
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_UTILS_BIN_SEARCH_HPP
