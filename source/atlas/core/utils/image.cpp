
// ================================================================================================
// -*- C++ -*-
// File: image.cpp
// Author: Guilherme R. Lampert
// Created on: 26/06/15
// Brief: Image loading and manipulation.
// ================================================================================================

#include "atlas/core/utils/image.hpp"
#include "atlas/core/fs/path_utils.hpp"
#include "atlas/core/maths/math_utils.hpp"
#include <cstring> // For memcpy & friends.

// ========================================================
// The header-only STB libraries (only included here):
// ========================================================

// Common memory allocators for STB and out Image class:
#define IMG_ALLOC(count)        atlas::core::allocate<atlas::core::ubyte>(count)
#define IMG_REALLOC(ptr, count) atlas::core::reallocate<atlas::core::ubyte>((atlas::core::ubyte *)(ptr), (count))
#define IMG_FREE(ptr)           atlas::core::deallocate(ptr)

//
// ---- STB Image (stbi):
//

#define STBI_ASSERT(x)                    DEBUG_CHECK(x)
#define STBI_MALLOC(count)                IMG_ALLOC(count)
#define STBI_REALLOC(ptr, count)          IMG_REALLOC(ptr, count)
#define STBI_FREE(ptr)                    IMG_FREE(ptr)

#define STB_IMAGE_IMPLEMENTATION          1
#define STB_IMAGE_STATIC                  1

// Explicitly disabled image loaders:
#define STBI_NO_HDR                       1
#define STBI_NO_PIC                       1
#define STBI_NO_PNM                       1
#define STBI_NO_PSD                       1
#define STBI_NO_LINEAR                    1

#include "thirdparty/stb/stb_image.h"

//
// ---- STB Image Write (stbiw):
//

#define STBIW_ASSERT(x)                   DEBUG_CHECK(x)
#define STBIW_MALLOC(count)               IMG_ALLOC(count)
#define STBIW_REALLOC(ptr, count)         IMG_REALLOC(ptr, count)
#define STBIW_FREE(ptr)                   IMG_FREE(ptr)

#define STB_IMAGE_WRITE_IMPLEMENTATION    1
#define STB_IMAGE_WRITE_STATIC            1
#define STBIW_NO_HDR_WRITER               1

#include "thirdparty/stb/stb_image_write.h"

//
// ---- STB Image Resize (stbir):
//

#define STBIR_ASSERT(x)                   DEBUG_CHECK(x)
#define STBIR_MALLOC(count, allocContext) IMG_ALLOC(count)
#define STBIR_FREE(ptr, allocContext)     IMG_FREE(ptr)

#define STB_IMAGE_RESIZE_IMPLEMENTATION   1
#define STB_IMAGE_RESIZE_STATIC           1
#define STBIR_DEBUG                       1 /* Enable STBIR__DEBUG_ASSERT, which resolves to DEBUG_CHECK */

#include "thirdparty/stb/stb_image_resize.h"

// ========================================================

namespace atlas
{
namespace core
{
namespace
{

// A big value, enough to hold the largest pixel size imaginable and more.
// Used internally for image resizing/transforming to avoid allocating heap memory.
constexpr uint MaxPixelBytes = 128;

// ========================================================
// set/get pixels functions for the supported formats:
// ========================================================

void setPixel_R8(ImageSurface & surface, const uint pX, const uint pY, const ubyte * pixelIn)
{
	ubyte * pixels = surface.getPixelData();
	pixels[pX + (pY * surface.getWidth())] = *pixelIn;
}

void getPixel_R8(const ImageSurface & surface, const uint pX, const uint pY, ubyte * pixelOut)
{
	const ubyte * pixels = surface.getPixelData();
	*pixelOut = pixels[pX + (pY * surface.getWidth())];
}

// ========================================================

void setPixel_RG8(ImageSurface & surface, const uint pX, const uint pY, const ubyte * pixelIn)
{
	ubyte * pixels = surface.getPixelData();
	const uint pixelIndex = pX + (pY * surface.getWidth());

	pixels[(pixelIndex * 2) + 0] = pixelIn[0];
	pixels[(pixelIndex * 2) + 1] = pixelIn[1];
}

void getPixel_RG8(const ImageSurface & surface, const uint pX, const uint pY, ubyte * pixelOut)
{
	const ubyte * pixels = surface.getPixelData();
	const uint pixelIndex = pX + (pY * surface.getWidth());

	pixelOut[0] = pixels[(pixelIndex * 2) + 0];
	pixelOut[1] = pixels[(pixelIndex * 2) + 1];
}

// ========================================================

void setPixel_RGB_BGR_U8(ImageSurface & surface, const uint pX, const uint pY, const ubyte * pixelIn)
{
	ubyte * pixels = surface.getPixelData();
	const uint pixelIndex = pX + (pY * surface.getWidth());

	pixels[(pixelIndex * 3) + 0] = pixelIn[0];
	pixels[(pixelIndex * 3) + 1] = pixelIn[1];
	pixels[(pixelIndex * 3) + 2] = pixelIn[2];
}

void getPixel_RGB_BGR_U8(const ImageSurface & surface, const uint pX, const uint pY, ubyte * pixelOut)
{
	const ubyte * pixels = surface.getPixelData();
	const uint pixelIndex = pX + (pY * surface.getWidth());

	pixelOut[0] = pixels[(pixelIndex * 3) + 0];
	pixelOut[1] = pixels[(pixelIndex * 3) + 1];
	pixelOut[2] = pixels[(pixelIndex * 3) + 2];
}

// ========================================================

void setPixel_RGBA_BGRA_U8(ImageSurface & surface, const uint pX, const uint pY, const ubyte * pixelIn)
{
	uint32 * pixelsU32        = reinterpret_cast<uint32 *>(surface.getPixelData());
	const uint32 * inPixelU32 = reinterpret_cast<const uint32 *>(pixelIn);

	pixelsU32[pX + (pY * surface.getWidth())] = *inPixelU32;
}

void getPixel_RGBA_BGRA_U8(const ImageSurface & surface, const uint pX, const uint pY, ubyte * pixelOut)
{
	const uint32 * pixelsU32 = reinterpret_cast<const uint32 *>(surface.getPixelData());
	uint32 * outPixelU32     = reinterpret_cast<uint32 *>(pixelOut);

	*outPixelU32 = pixelsU32[pX + (pY * surface.getWidth())];
}

// ========================================================
// pixFormatLayouts[]: Register the above functions here!
// ========================================================

const PixelDataLayoutDesc pixFormatLayouts[] =
{
	// ---- Null ----
	{
		/* bytesPerPixel       = */ 0,
		/* channelCount        = */ 0,
		/* dataLayout          = */ PixelFormat::Null,
		/* isCompressed        = */ false,
		/* supportsPixelAccess = */ false,
		/* string              = */ "(null)",
		/* setPixelFunc        = */ nullptr,
		/* getPixelFunc        = */ nullptr
	},
	// ---- R8 ----
	{
		/* bytesPerPixel       = */ 1,
		/* channelCount        = */ 1,
		/* dataLayout          = */ PixelFormat::R8,
		/* isCompressed        = */ false,
		/* supportsPixelAccess = */ true,
		/* string              = */ "R8",
		/* setPixelFunc        = */ &setPixel_R8,
		/* getPixelFunc        = */ &getPixel_R8
	},
	// ---- RG8 ----
	{
		/* bytesPerPixel       = */ 2,
		/* channelCount        = */ 2,
		/* dataLayout          = */ PixelFormat::RG8,
		/* isCompressed        = */ false,
		/* supportsPixelAccess = */ true,
		/* string              = */ "RG8",
		/* setPixelFunc        = */ &setPixel_RG8,
		/* getPixelFunc        = */ &getPixel_RG8
	},
	// ---- RGB8 ----
	{
		/* bytesPerPixel       = */ 3,
		/* channelCount        = */ 3,
		/* dataLayout          = */ PixelFormat::RGB8,
		/* isCompressed        = */ false,
		/* supportsPixelAccess = */ true,
		/* string              = */ "RGB8",
		/* setPixelFunc        = */ &setPixel_RGB_BGR_U8,
		/* getPixelFunc        = */ &getPixel_RGB_BGR_U8
	},
	// ---- RGBA8 ----
	{
		/* bytesPerPixel       = */ 4,
		/* channelCount        = */ 4,
		/* dataLayout          = */ PixelFormat::RGBA8,
		/* isCompressed        = */ false,
		/* supportsPixelAccess = */ true,
		/* string              = */ "RGBA8",
		/* setPixelFunc        = */ &setPixel_RGBA_BGRA_U8,
		/* getPixelFunc        = */ &getPixel_RGBA_BGRA_U8
	},
	// ---- BGR8 ----
	{
		/* bytesPerPixel       = */ 3,
		/* channelCount        = */ 3,
		/* dataLayout          = */ PixelFormat::BGR8,
		/* isCompressed        = */ false,
		/* supportsPixelAccess = */ true,
		/* string              = */ "BGR8",
		/* setPixelFunc        = */ &setPixel_RGB_BGR_U8,
		/* getPixelFunc        = */ &getPixel_RGB_BGR_U8 // Assume input and image are already BGR
	},
	// ---- BGRA8 ----
	{
		/* bytesPerPixel       = */ 4,
		/* channelCount        = */ 4,
		/* dataLayout          = */ PixelFormat::BGRA8,
		/* isCompressed        = */ false,
		/* supportsPixelAccess = */ true,
		/* string              = */ "BGRA8",
		/* setPixelFunc        = */ &setPixel_RGBA_BGRA_U8,
		/* getPixelFunc        = */ &getPixel_RGBA_BGRA_U8 // Assume input and image are already BGRA
	}
};

COMPILE_TIME_CHECK(arrayLength(pixFormatLayouts) == PixelFormat::Count, "Keep this array in sync with the enum declaration!");

// ========================================================
// stbiLoadHelper() [LOCAL]:
// ========================================================

bool stbiLoadHelper(Image & destImage, const ImageLoadParams & imageParams,
                    ubyte * newImageData, const int width, const int height, const int comps,
                    ErrorInfo * errorInfo, const String & filePath, const String & stbiFunc)
{
	if (newImageData == nullptr)
	{
		makeError(errorInfo, "\'" + stbiFunc + "()\' failed with error: \'" +
				String(stbi_failure_reason()) + "\' for path \"" + filePath + "\".");
		return false;
	}

	Image::DataLayout dataLayout;
	if (imageParams.forceRgba)
	{
		dataLayout = PixelFormat::RGBA8;
	}
	else
	{
		switch (comps)
		{
		case 1 : // Grayscale (equivalent to R8 - 1 channel)
			dataLayout = PixelFormat::R8;
			break;

		case 2 : // Gray+alpha (equivalent to RG8 - 2 channels)
			dataLayout = PixelFormat::RG8;
			break;

		case 3 : // RGB (3 channels)
			dataLayout = PixelFormat::RGB8;
			break;

		case 4 : // RGBA (4 channels)
			dataLayout = PixelFormat::RGBA8;
			break;

		default : // Unsupported
			IMG_FREE(newImageData);
			makeError(errorInfo, "\"" + filePath + "\": Image format not supported! comps = " + toString(comps));
			return false;
		} // switch (comps)
	}

	if (!destImage.initWithExternalData(newImageData, width, height, dataLayout))
	{
		return false;
	}

	if (!math::isPowerOfTwo(width) || !math::isPowerOfTwo(height))
	{
		if (imageParams.roundDownToPowerOfTwo)
		{
			destImage.roundDownBaseSurfaceToPowerOfTwo();
		}
	}

	if (imageParams.generateMipmaps)
	{
		destImage.generateMipmapSurfaces();
	}

	return true;
}

} // namespace {}

// ========================================================
// PixelFormat::fromLayout():
// ========================================================

PixelFormat PixelFormat::fromLayout(const PixelFormat::DataLayout layout)
{
	DEBUG_CHECK(layout < arrayLength(pixFormatLayouts));

	PixelFormat pixFormat;
	pixFormat.dataLayoutDesc = &pixFormatLayouts[layout];

	return pixFormat;
}

// ================================================================================================
// Image class implementation:
// ================================================================================================

// ========================================================
// Image::Image():
// ========================================================

Image::Image()
	: dataBaseSurface(nullptr)
	, dataMipmapSurfaces(nullptr)
	, surfaceCount(0)
	, memoryUsage(0)
{
}

// ========================================================
// Image::~Image():
// ========================================================

Image::~Image()
{
	deallocate();
}

// ========================================================
// Image::deallocate():
// ========================================================

void Image::deallocate()
{
	if (!isValid())
	{
		return;
	}

	// Mipmap surfaces are usually allocated from a separate
	// block, but they might share the base surface's block too.
	if (dataMipmapSurfaces != dataBaseSurface)
	{
		IMG_FREE(dataMipmapSurfaces);
	}

	IMG_FREE(dataBaseSurface);

	for (uint s = 0; s < surfaceCount; ++s)
	{
		surfaces[s].invalidate();
	}

	dataBaseSurface    = nullptr;
	dataMipmapSurfaces = nullptr;
	surfaceCount       = 0;
	memoryUsage        = 0;
	pixelFormat.invalidate();
}

// ========================================================
// Image::initWithCheckerPattern();
// ========================================================

bool Image::initWithCheckerPattern(const uint squares)
{
	DEBUG_CHECK(squares >= 2 && squares <= 64);
	DEBUG_CHECK(!isValid() && "Image already initialized!");

	const uint imgSize = 64; // Image dimensions (WxH) in pixels.
	if (!initWithDimension(imgSize, imgSize, PixelFormat::RGBA8))
	{
		return false;
	}

	// One square black and one white.
	const Color colors[] =
	{
		Color(  0,   0,   0, 255),
		Color(255, 255, 255, 255)
	};

	Color * pixels = reinterpret_cast<Color *>(getPixelDataBaseSurface());
	fillBufferWithCheckerPattern(pixels, imgSize, imgSize, squares, colors);
	return true;
}

// ========================================================
// Image::initWithDimension():
// ========================================================

bool Image::initWithDimension(const uint width, const uint height, const DataLayout dataLayout)
{
	DEBUG_CHECK(width  != 0);
	DEBUG_CHECK(height != 0);
	DEBUG_CHECK(!isValid() && "Image already initialized!");

	const uint bytesPerPixel = PixelFormat::fromLayout(dataLayout).getBytesPerPixel();
	ubyte * newImageData = IMG_ALLOC(width * height * bytesPerPixel);
	return initWithExternalData(newImageData, width, height, dataLayout);
}

// ========================================================
// Image::initWithColorFill():
// ========================================================

bool Image::initWithColorFill(const uint width, const uint height, const DataLayout dataLayout, const Color fillColor)
{
	if (dataLayout != PixelFormat::RGB8  && dataLayout != PixelFormat::BGR8 &&
		dataLayout != PixelFormat::RGBA8 && dataLayout != PixelFormat::BGRA8)
	{
		// `Color` holds RGB[A]|BGR[A] 8bit values.
		return false;
	}

	if (!initWithDimension(width, height, dataLayout))
	{
		return false;
	}

	Color * pixels = reinterpret_cast<Color *>(getPixelDataBaseSurface());
	const uint pixelCount = width * height;

	for (uint p = 0; p < pixelCount; ++p)
	{
		pixels[p] = fillColor;
	}

	return true;
}

// ========================================================
// Image::initWithExternalData():
// ========================================================

bool Image::initWithExternalData(ubyte * baseSurface, const uint width, const uint height, const DataLayout dataLayout)
{
	DEBUG_CHECK(baseSurface != nullptr);
	DEBUG_CHECK(width  != 0);
	DEBUG_CHECK(height != 0);
	DEBUG_CHECK(!isValid() && "Image already initialized!");

	pixelFormat        = PixelFormat::fromLayout(dataLayout);
	dataBaseSurface    = baseSurface;
	dataMipmapSurfaces = nullptr;
	surfaceCount       = 1;
	memoryUsage        = width * height * pixelFormat.getBytesPerPixel();

	// Add a default surface:
	surfaces[0].pixelData    = baseSurface;
	surfaces[0].dimensions.x = width;
	surfaces[0].dimensions.y = height;

	return true;
}

// ========================================================
// Image::initFromFile():
// ========================================================

bool Image::initFromFile(const String & filePath, const ImageLoadParams & imageParams, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(!filePath.isEmpty());
	DEBUG_CHECK(!isValid() && "Image already initialized!");

	int width = 0, height = 0, comps = 0;
	const int requiredComps = (imageParams.forceRgba ? 4 : 0);

	stbi_set_flip_vertically_on_load(imageParams.flipVerticallyOnLoad);
	ubyte * newImageData = stbi_load(filePath.getCStr(), &width, &height, &comps, requiredComps);

	return stbiLoadHelper(*this, imageParams, newImageData,
			width, height, comps, errorInfo, filePath, "stbi_load");
}

// ========================================================
// Image::initFromFileInMemory():
// ========================================================

bool Image::initFromFileInMemory(const ByteArray & fileContents, const ImageLoadParams & imageParams,
                                 const String & filePathOpt, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(!fileContents.isEmpty());
	DEBUG_CHECK(!isValid() && "Image already initialized!");

	int width = 0, height = 0, comps = 0;
	const int requiredComps = (imageParams.forceRgba ? 4 : 0);

	const stbi_uc * dataPtr = fileContents.getData();
	const int dataSizeBytes = static_cast<int>(fileContents.getSize());

	stbi_set_flip_vertically_on_load(imageParams.flipVerticallyOnLoad);
	ubyte * newImageData = stbi_load_from_memory(dataPtr, dataSizeBytes, &width, &height, &comps, requiredComps);

	return stbiLoadHelper(*this, imageParams, newImageData,
			width, height, comps, errorInfo, filePathOpt, "stbi_load_from_memory");
}

// ========================================================
// Image::transferTo():
// ========================================================

void Image::transferTo(Image & dest)
{
	DEBUG_CHECK(!dest.isValid() && "Destination image contents would be lost!");

	dest.dataBaseSurface    = dataBaseSurface;
	dest.dataMipmapSurfaces = dataMipmapSurfaces;
	dest.pixelFormat        = pixelFormat;
	dest.surfaceCount       = surfaceCount;
	dest.memoryUsage        = memoryUsage;

	for (uint s = 0; s < surfaceCount; ++s)
	{
		dest.surfaces[s].pixelData  = surfaces[s].pixelData;
		dest.surfaces[s].dimensions = surfaces[s].dimensions;

		surfaces[s].invalidate();
	}

	dataBaseSurface    = nullptr;
	dataMipmapSurfaces = nullptr;
	surfaceCount       = 0;
	memoryUsage        = 0;
	pixelFormat.invalidate();
}

// ========================================================
// Image::writeToFile():
// ========================================================

bool Image::writeToFile(const String & filePath, const String & extension, const uint surface, ErrorInfo * errorInfo) const
{
	DEBUG_CHECK(!filePath.isEmpty());
	DEBUG_CHECK(isValid() && "Invalid image!");

	if (pixelFormat != PixelFormat::R8    && pixelFormat != PixelFormat::RG8  &&
		pixelFormat != PixelFormat::RGB8  && pixelFormat != PixelFormat::BGR8 &&
		pixelFormat != PixelFormat::RGBA8 && pixelFormat != PixelFormat::BGRA8)
	{
		makeError(errorInfo, "Image must be R, RG, RGB, BGR, RGBA or BGRA 8-bits!");
		return false;
	}

	String pathExt = getFilenameExtension(filePath);

	if (!pathExt.isEmpty())
	{
		// If both are provided, they must match.
		if (!extension.isEmpty() && pathExt != extension)
		{
			makeError(errorInfo, "Filename extension (" + pathExt +
					") differed from \'extension\' parameter (" + extension + ").");
			return false;
		}
	}
	else // Path/filename had no extension.
	{
		// Gotta provide either one.
		if (extension.isEmpty())
		{
			makeError(errorInfo, "No file extension provided!");
			return false;
		}
		pathExt = extension;
	}

	const String fullFilePath = stripFilenameExtension(filePath) + pathExt;

	// Image file writing:
	//
	if (pathExt == ".tga")
	{
		return stbiw_write_tga(fullFilePath.getCStr(), getWidth(surface), getHeight(surface),
				pixelFormat.getChannelCount(), getSurface(surface).getPixelData()) != 0;
	}
	else if (pathExt == ".bmp")
	{
		return stbiw_write_bmp(fullFilePath.getCStr(), getWidth(surface), getHeight(surface),
				pixelFormat.getChannelCount(), getSurface(surface).getPixelData()) != 0;
	}
	else if (pathExt == ".png")
	{
		const int strideInBytes = getWidth(surface) * pixelFormat.getBytesPerPixel();
		return stbiw_write_png(fullFilePath.getCStr(), getWidth(surface), getHeight(surface),
				pixelFormat.getChannelCount(), getSurface(surface).getPixelData(), strideInBytes) != 0;
	}
	else
	{
		makeError(errorInfo, "Unsupported image format \"" + pathExt +
				"\"! Use \".tga\", \".bmp\" or \".png\".");
		return false;
	}
}

// ========================================================
// Image::writeAllSurfacesToFiles():
// ========================================================

bool Image::writeAllSurfacesToFiles(const String & basePathName, const String & extension) const
{
	DEBUG_CHECK(!basePathName.isEmpty());
	DEBUG_CHECK(!extension.isEmpty());

	String surfaceName;
	int errorCount = 0;

	for (uint s = 0; s < surfaceCount; ++s)
	{
		surfaceName = basePathName + "_" + toString(s) + extension;
		if (!writeToFile(surfaceName, extension, s))
		{
			++errorCount;
			// Attempt to processed...
		}
	}

	return errorCount == 0;
}

// ========================================================
// Image::swapPixel():
// ========================================================

void Image::swapPixel(const uint pX0, const uint pY0, const uint pX1, const uint pY1, const uint surface)
{
	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	ubyte tempPixel0[MaxPixelBytes];
	ubyte tempPixel1[MaxPixelBytes];

	getPixel(pX0, pY0, tempPixel0, surface);
	getPixel(pX1, pY1, tempPixel1, surface);

	setPixel(pX0, pY0, tempPixel1, surface);
	setPixel(pX1, pY1, tempPixel0, surface);
}

// ========================================================
// Image::doForEveryPixel():
// ========================================================

void Image::doForEveryPixel(void (* func)(ubyte *, const PixelFormat &), const uint surface)
{
	DEBUG_CHECK(func != nullptr);
	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	ImageSurface & surf = getSurface(surface);

	ubyte * dataPtr = surf.getPixelData();
	const uint pixelCount = surf.getPixelCount();
	const uint bytesPerPixel = pixelFormat.getBytesPerPixel();

	for (uint p = 0; p < pixelCount; ++p)
	{
		func(dataPtr, pixelFormat);
		dataPtr += bytesPerPixel;
	}
}

// ========================================================
// Image::flipVInPlace():
// ========================================================

void Image::flipVInPlace(const uint surface)
{
	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	const uint width = getWidth(surface);
	const uint maxY  = getHeight(surface) - 1;
	const uint halfHeight = getHeight(surface) / 2;

	for (uint y = 0; y < halfHeight; ++y)
	{
		for (uint x = 0; x < width; ++x)
		{
			swapPixel(x, y, x, maxY - y, surface);
		}
	}
}

// ========================================================
// Image::flipHInPlace():
// ========================================================

void Image::flipHInPlace(const uint surface)
{
	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	const uint height = getHeight(surface);
	const uint maxX   = getWidth(surface) - 1;
	const uint halfWidth = getWidth(surface) / 2;

	for (uint y = 0; y < height; ++y)
	{
		for (uint x = 0; x < halfWidth; ++x)
		{
			swapPixel(x, y, (maxX - x), y, surface);
		}
	}
}

// ========================================================
// Image::swizzleRedBlue():
// ========================================================

void Image::swizzleRedBlue(const uint surface)
{
	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	// Change the pixel format data layout:
	switch (pixelFormat.getDataLayout())
	{
	case PixelFormat::RGB8 :
		pixelFormat = PixelFormat::fromLayout(PixelFormat::BGR8);
		break;

	case PixelFormat::RGBA8 :
		pixelFormat = PixelFormat::fromLayout(PixelFormat::BGRA8);
		break;

	case PixelFormat::BGR8 :
		pixelFormat = PixelFormat::fromLayout(PixelFormat::RGB8);
		break;

	case PixelFormat::BGRA8 :
		pixelFormat = PixelFormat::fromLayout(PixelFormat::RGBA8);
		break;

	default :
		// Not RGB/RGBA or compatible.
		return;
	} // switch (pixelFormat.getDataLayout())

	ImageSurface & surf = getSurface(surface);

	ubyte * dataPtr = surf.getPixelData();
	const uint pixelCount = surf.getPixelCount();
	const uint componentsPerPixel = pixelFormat.getChannelCount();

	for (uint p = 0; p < pixelCount; ++p)
	{
		swap(dataPtr[0], dataPtr[2]);
		dataPtr += componentsPerPixel;
	}
}

// ========================================================
// Image::byteFill():
// ========================================================

void Image::byteFill(const ubyte fillWith, const uint surface)
{
	DEBUG_CHECK(isValid() && "Invalid image!");

	ImageSurface & surf   = getSurface(surface);
	const uint pixelCount = surf.getPixelCount();
	ubyte * dataPtr       = surf.getPixelData();

	std::memset(dataPtr, fillWith, pixelCount * pixelFormat.getBytesPerPixel());
}

// ========================================================
// Image::generateMipmapSurfaces():
// ========================================================

void Image::generateMipmapSurfaces()
{
	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	if (getWidth() == 1 && getHeight() == 1)
	{
		// If the base surface happens to be a 1x1 pixels
		// image, then we can't subdivide it any further.
		// A 2x2 image can still generate one 1x1 mipmap level though.
		return;
	}

	// All sub-surfaces/mipmaps will be allocated in a contiguous
	// block of memory. We align the start of each portion belonging
	// to a surface to this many bytes.
	//
	// 16 is the usual alignment of `malloc` and also suitable
	// for SSE/SIMD instructions, even though we don't currently use them.
	//
	constexpr uint SurfBoundaryAlignment = 16;

	// Initial image is the base surface (mipmap level = 0).
	// We always use the initial image to generate all mipmaps
	// to avoid propagating errors.
	//
	const uint initialWidth    = getWidth();
	const uint initialHeight   = getHeight();
	const uint channelCount    = pixelFormat.getChannelCount();
	const uint bytesPerPixel   = pixelFormat.getBytesPerPixel();
	const ubyte * initialImage = getPixelDataBaseSurface();

	uint targetWidth  = initialWidth;
	uint targetHeight = initialHeight;
	uint mipmapCount  = 1; // Surface 0 is the initial image.
	uint mipmapBytes  = 0;

	// First, figure out how much memory to allocate.
	// Stop when any of the dimensions reach 1.
	//
	while (mipmapCount != MaxSurfaces)
	{
		targetWidth  = max(1u, targetWidth  / 2);
		targetHeight = max(1u, targetHeight / 2);

		mipmapBytes += alignSize(targetWidth * targetHeight * bytesPerPixel, SurfBoundaryAlignment);
		++mipmapCount;

		if (targetWidth == 1 && targetHeight == 1)
		{
			break;
		}
	}

	// Allocate exact needed memory:
	//
	DEBUG_CHECK(dataMipmapSurfaces == nullptr && "Would leak memory!");
	dataMipmapSurfaces = IMG_ALLOC(mipmapBytes);

	// Also update the image's memory statistics.
	memoryUsage += mipmapBytes;

	// Mipmap surface generation:
	//
	ubyte * mipDataPtr = dataMipmapSurfaces;
	targetWidth  = initialWidth;
	targetHeight = initialHeight;
	mipmapCount  = 1;

	while (mipmapCount != MaxSurfaces)
	{
		targetWidth  = max(1u, targetWidth  / 2);
		targetHeight = max(1u, targetHeight / 2);

		//
		// NOTE:
		// Should this be stbir_resize_uint8_srgb() instead?
		// Have a flag in Image telling if it is sRGB, maybe?
		//
		stbir_resize_uint8(
			// Source:
			initialImage,
			initialWidth,
			initialHeight,
			0,
			// Destination:
			mipDataPtr,
			targetWidth,
			targetHeight,
			0,
			// Flags/params:
			channelCount);

		surfaces[mipmapCount].pixelData    = mipDataPtr;
		surfaces[mipmapCount].dimensions.x = targetWidth;
		surfaces[mipmapCount].dimensions.y = targetHeight;

		mipDataPtr += targetWidth * targetHeight * bytesPerPixel;
		mipDataPtr  = reinterpret_cast<ubyte *>(alignPtr(mipDataPtr, SurfBoundaryAlignment));

		++mipmapCount;

		if (targetWidth == 1 && targetHeight == 1)
		{
			break;
		}
	}

	surfaceCount = mipmapCount;
}

// ========================================================
// Image::flipVCopyBaseSurface():
// ========================================================

void Image::flipVCopyBaseSurface(Image & flippedImage) const
{
	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	const uint width  = getWidth();
	const uint height = getHeight();

	flippedImage.deallocate();
	flippedImage.initWithDimension(width, height, pixelFormat.getDataLayout());

	const uint maxX = width  - 1;
	const uint maxY = height - 1;
	ubyte tempPixel[MaxPixelBytes];

	for (uint y = 0; y < height; ++y)
	{
		for (uint x = 0; x < width; ++x)
		{
			getPixel(maxX - x, y, tempPixel);
			flippedImage.setPixel(maxX - x, maxY - y, tempPixel);
		}
	}
}

// ========================================================
// Image::flipHCopyBaseSurface()
// ========================================================

void Image::flipHCopyBaseSurface(Image & flippedImage) const
{
	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	const uint width  = getWidth();
	const uint height = getHeight();

	flippedImage.deallocate();
	flippedImage.initWithDimension(width, height, pixelFormat.getDataLayout());

	ubyte tempPixel[MaxPixelBytes];

	for (uint y = 0; y < height; ++y)
	{
		for (uint x = 0; x < width;)
		{
			getPixel(x, y, tempPixel);
			++x;

			flippedImage.setPixel(width - x, y, tempPixel);
		}
	}
}

// ========================================================
// Image::resizeBaseSurfaceInPlace():
// ========================================================

void Image::resizeBaseSurfaceInPlace(const uint targetWidth, const uint targetHeight)
{
	DEBUG_CHECK(targetWidth  != 0);
	DEBUG_CHECK(targetHeight != 0);

	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	if (getWidth() == targetWidth && getHeight() == targetHeight)
	{
		return; // Nothing to be done here.
	}

	// Resulting image may be bigger or smaller. We always need a copy.
	Image resizedImage;
	resizeCopyBaseSurface(resizedImage, targetWidth, targetHeight);

	// Move resized image into this:
	deallocate();
	resizedImage.transferTo(*this);
}

// ========================================================
// Image::resizeCopyBaseSurface():
// ========================================================

void Image::resizeCopyBaseSurface(Image & destImage, const uint targetWidth, const uint targetHeight) const
{
	DEBUG_CHECK(targetWidth  != 0);
	DEBUG_CHECK(targetHeight != 0);

	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	if (getWidth() == targetWidth && getHeight() == targetHeight)
	{
		return; // Nothing to be done here.
	}

	destImage.deallocate();
	destImage.initWithDimension(targetWidth, targetHeight, pixelFormat.getDataLayout());

	//
	// NOTE:
	// Should this be stbir_resize_uint8_srgb() instead?
	// Have a flag in Image telling if it is sRGB, maybe?
	//
	stbir_resize_uint8(
		// Source:
		getPixelDataBaseSurface(),
		getWidth(),
		getHeight(),
		0,
		// Destination:
		destImage.getPixelDataBaseSurface(),
		destImage.getWidth(),
		destImage.getHeight(),
		0,
		// Flags/params:
		pixelFormat.getChannelCount());
}

// ========================================================
// Image::roundDownBaseSurfaceToPowerOfTwo():
// ========================================================

void Image::roundDownBaseSurfaceToPowerOfTwo()
{
	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	if (isPowerOfTwo())
	{
		return; // Nothing to do.
	}

	resizeBaseSurfaceInPlace(
		math::roundDownToPowerOfTwo(getWidth()),
		math::roundDownToPowerOfTwo(getHeight()));
}

// ========================================================
// Image::discardBaseSurfaceAlphaChannel():
// ========================================================

void Image::discardBaseSurfaceAlphaChannel()
{
	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	if (pixelFormat != PixelFormat::RGBA8 && pixelFormat != PixelFormat::BGRA8)
	{
		return;
	}

	// Resulting image is either RGB or BGR.
	const DataLayout newLayout = (pixelFormat == PixelFormat::RGBA8) ? PixelFormat::RGB8 : PixelFormat::BGR8;
	const uint width  = getWidth();
	const uint height = getHeight();

	// Resulting image will be smaller; We need a new one:
	Image rgbImage;
	rgbImage.initWithDimension(width, height, newLayout);

	// Discard alpha for each pixels of the source image:
	ubyte tempPixel[MaxPixelBytes];
	for (uint y = 0; y < height; ++y)
	{
		for (uint x = 0; x < width; ++x)
		{
			getPixel(x, y, tempPixel);
			// Since `rgbImage` is 3 channels, the alpha will be ignored by setPixel().
			rgbImage.setPixel(x, y, tempPixel);
		}
	}

	deallocate();
	rgbImage.transferTo(*this);
}

// ========================================================
// Image::copyBaseSurfaceRect():
// ========================================================

void Image::copyBaseSurfaceRect(Image & destImage, const uint xOffset, const uint yOffset,
                                const uint rectWidth, const uint rectHeight, const bool topLeft) const
{
	DEBUG_CHECK(rectHeight != 0);
	DEBUG_CHECK(rectWidth  != 0);

	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	// We might need to resize the destination image or change its pixel format.
	if (destImage.pixelFormat != pixelFormat ||
		destImage.getWidth()  < rectWidth    ||
		destImage.getHeight() < rectHeight)
	{
		destImage.deallocate();
		destImage.initWithDimension(rectWidth, rectHeight, pixelFormat.getDataLayout());
	}

	ubyte tempPixel[MaxPixelBytes];
	if (topLeft)
	{
		// Use the top-left corner of the image as the (0,0) origin; Invert Y.
		uint maxY = getHeight() - 1;
		for (uint y = 0; y < rectHeight; ++y)
		{
			for (uint x = 0; x < rectWidth; ++x)
			{
				getPixel(x + xOffset, maxY - yOffset, tempPixel);
				destImage.setPixel(x, y, tempPixel);
			}
			--maxY;
		}
	}
	else
	{
		// Use the bottom-left corner of the image as the (0,0) origin; Default Y.
		for (uint y = 0; y < rectHeight; ++y)
		{
			for (uint x = 0; x < rectWidth; ++x)
			{
				getPixel(x + xOffset, y + yOffset, tempPixel);
				destImage.setPixel(x, y, tempPixel);
			}
		}
	}
}

// ========================================================
// Image::setBaseSurfaceRect():
// ========================================================

void Image::setBaseSurfaceRect(const Image & srcImage, const uint xOffset, const uint yOffset,
                               const uint rectWidth, const uint rectHeight, const bool topLeft)
{
	DEBUG_CHECK(rectHeight != 0);
	DEBUG_CHECK(rectWidth  != 0);

	DEBUG_CHECK(srcImage.isValid() && "Invalid source image!");
	DEBUG_CHECK(srcImage.supportsPixelAccess());

	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	ubyte tempPixel[MaxPixelBytes];
	if (topLeft)
	{
		// Use the top-left corner of the image as the (0,0) origin; Invert Y.
		uint maxY = getHeight() - 1;
		for (uint y = 0; y < rectHeight; ++y)
		{
			for (uint x = 0; x < rectWidth; ++x)
			{
				srcImage.getPixel(x, y, tempPixel);
				setPixel(x + xOffset, maxY - yOffset, tempPixel);
			}
			--maxY;
		}
	}
	else
	{
		// Use the bottom-left corner of the image as the (0,0) origin; Default Y.
		for (uint y = 0; y < rectHeight; ++y)
		{
			for (uint x = 0; x < rectWidth; ++x)
			{
				srcImage.getPixel(x, y, tempPixel);
				setPixel(x + xOffset, y + yOffset, tempPixel);
			}
		}
	}
}

// ========================================================
// Image::setBaseSurfaceRect():
// ========================================================

void Image::setBaseSurfaceRect(const ubyte * RESTRICT image, const uint xOffset, const uint yOffset,
                               const uint rectWidth, const uint rectHeight, const bool topLeft)
{
	DEBUG_CHECK(image != nullptr);
	DEBUG_CHECK(rectHeight != 0);
	DEBUG_CHECK(rectWidth  != 0);

	DEBUG_CHECK(isValid() && "Invalid image!");
	DEBUG_CHECK(supportsPixelAccess());

	const uint bytesPerPixel = pixelFormat.getBytesPerPixel();
	ubyte tempPixel[MaxPixelBytes];

	if (topLeft)
	{
		// Use the top-left corner of the image as the (0,0) origin; Invert Y.
		uint maxY = getHeight() - 1;
		for (uint y = 0; y < rectHeight; ++y)
		{
			for (uint x = 0; x < rectWidth; ++x)
			{
				std::memcpy(tempPixel, (image + (x + y * rectWidth) * bytesPerPixel), bytesPerPixel);
				setPixel(x + xOffset, maxY - yOffset, tempPixel);
			}
			--maxY;
		}
	}
	else
	{
		// Use the bottom-left corner of the image as the (0,0) origin; Default Y.
		for (uint y = 0; y < rectHeight; ++y)
		{
			for (uint x = 0; x < rectWidth; ++x)
			{
				std::memcpy(tempPixel, (image + (x + y * rectWidth) * bytesPerPixel), bytesPerPixel);
				setPixel(x + xOffset, y + yOffset, tempPixel);
			}
		}
	}
}

// ================================================================================================
// Color buffer helper functions:
// ================================================================================================

inline bool validLineThickness(const int lineThickness)
{
	// 1 is OK, otherwise must be evenly divisible by 2.
	if (lineThickness <= 0) { return false; }
	return lineThickness == 1 || (lineThickness % 2) == 0;
}

inline int minOf4(const int a, const int b, const int c, const int d)
{
    return min(a, min(b, min(c, d)));
}

// ========================================================
// fillBufferWithCheckerPattern():
// ========================================================

void fillBufferWithCheckerPattern(Color * buffer, const uint width, const uint height,
                                  const uint squares, const Color colors[2])
{
	DEBUG_CHECK(buffer  != nullptr);
	DEBUG_CHECK(width   != 0);
	DEBUG_CHECK(height  != 0);

	DEBUG_CHECK(squares >= 2);
	DEBUG_CHECK((width  % squares) == 0);
	DEBUG_CHECK((height % squares) == 0);

	// Size of one checker square, in pixels.
	const uint checkerSize = width / squares;

	for (uint y = 0; y < height; ++y)
	{
		for (uint x = 0; x < width; ++x)
		{
			const uint colorIndex  = ((y / checkerSize) + (x / checkerSize)) % 2;
			const uint bufferIndex = x + (y * width);

			DEBUG_CHECK(colorIndex  < 2);
			DEBUG_CHECK(bufferIndex < width * height);

			buffer[bufferIndex] = colors[colorIndex];
		}
	}
}

// ========================================================
// fillBufferWithStripePattern():
// ========================================================

void fillBufferWithStripePattern(Color * buffer, const uint width, const uint height,
                                 const uint lineThickness, const bool vertical,
                                 const Color colors[2])
{
	DEBUG_CHECK(buffer != nullptr);
	DEBUG_CHECK(width  != 0);
	DEBUG_CHECK(height != 0);
	DEBUG_CHECK(validLineThickness(lineThickness));

	uint colorIndex = 0;

	if (vertical) // Vertical stripes:
	{
		for (uint y = 0; y < height; ++y)
		{
			for (uint x = 0; x < width; )
			{
				for (uint lineX = 0; lineX < lineThickness; ++lineX, ++x)
				{
					const uint bufferIndex = x + (y * width);
					DEBUG_CHECK(bufferIndex < width * height);

					buffer[bufferIndex] = colors[colorIndex];
				}
				colorIndex = !colorIndex;
			}
		}
	}
	else // Horizontal stripes:
	{
		for (uint y = 0; y < height; y += lineThickness)
		{
			for (uint lineY = y; lineY < y + lineThickness; ++lineY)
			{
				for (uint lineX = 0; lineX < width; ++lineX)
				{
					const uint bufferIndex = lineX + (lineY * width);
					DEBUG_CHECK(bufferIndex < width * height);

					buffer[bufferIndex] = colors[colorIndex];
				}
			}
			colorIndex = !colorIndex;
		}
	}
}

// ========================================================
// fillBufferWithBoxPattern():
// ========================================================

void fillBufferWithBoxPattern(Color * buffer, const uint width, const uint height,
                              const uint lineThickness, const Color colors[2])
{
	DEBUG_CHECK(buffer != nullptr);
	DEBUG_CHECK(width  != 0);
	DEBUG_CHECK(height != 0);
	DEBUG_CHECK(validLineThickness(lineThickness));

	for (uint y = 0; y < height; ++y)
	{
		for (uint x = 0; x < width; ++x)
		{
			// Is it closer to the top, bottom, left, or right?
			const int minDist = minOf4(
				x,              // distance from left side
				y,              // distance from top
				width  - x - 1, // distance from right side
				height - y - 1  // distance from bottom
			);

			const uint colorIndex  = (minDist / lineThickness) % 2;
			const uint bufferIndex = x + (y * width);

			DEBUG_CHECK(colorIndex  < 2);
			DEBUG_CHECK(bufferIndex < width * height);

			buffer[bufferIndex] = colors[colorIndex];
		}
	}
}

// ========================================================
// fillBufferWithBoundedRect():
// ========================================================

void fillBufferWithBoundedRect(Color * buffer, const uint width, const uint height, const Color colors[2])
{
	DEBUG_CHECK(buffer != nullptr);
	DEBUG_CHECK(width  == height); // Require a square image to make things simpler.
	DEBUG_CHECK(width  != 0);
	DEBUG_CHECK(height != 0);

	// Center (fill the whole image; easier this way):
	for (uint y = 0; y < height; ++y)
	{
		for (uint x = 0; x < width; ++x)
		{
			buffer[x + (y * width)] = colors[0];
		}
	}

	// Add 1px border:
	for (uint x = 0; x < width; ++x)
	{
		buffer[x]                         = colors[1]; // top line
		buffer[x * width]                 = colors[1]; // left line
		buffer[x * width  + (width - 1)]  = colors[1]; // right line
		buffer[x + (width * (width - 1))] = colors[1]; // bottom line
	}
}

// ========================================================
// fillBufferWithSolidColor():
// ========================================================

void fillBufferWithSolidColor(Color * buffer, const uint width, const uint height, const Color fillColor)
{
	DEBUG_CHECK(buffer != nullptr);
	DEBUG_CHECK(width  != 0);
	DEBUG_CHECK(height != 0);

	const uint bufferSize = width * height;
	for (uint i = 0; i < bufferSize; ++i)
	{
		buffer[i] = fillColor;
	}
}

} // namespace core {}
} // namespace atlas {}
