
// ================================================================================================
// -*- C++ -*-
// File: default_log.hpp
// Author: Guilherme R. Lampert
// Created on: 09/07/15
// Brief: Defines a default log stream that can be used by clients of the Core Library.
// ================================================================================================

#ifndef ATLAS_CORE_UTILS_DEFAULT_LOG_HPP
#define ATLAS_CORE_UTILS_DEFAULT_LOG_HPP

#include "atlas/core/strings/dynamic_string.hpp"
#include "atlas/core/strings/sized_string.hpp"
#include "atlas/core/utils/log_stream.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// struct/enum LogLevel:
// ========================================================

struct LogLevel
{
	enum Enum
	{
		Silent,
		Fatal,
		Error,
		Warning,
		Info
	};
};

// ========================================================
// AnsiColorCodes constants:
// ========================================================

struct AnsiColorCodes
{
	static const char * Red;
	static const char * Green;
	static const char * Yellow;
	static const char * Blue;
	static const char * Magenta;
	static const char * Cyan;
	static const char * White;
	static const char * Default; // Restore terminal's default.
};

// ========================================================
// Helper functions / Global log stream:
// ========================================================

// The global Atlas log. This default log stream is provided for users of the Core Library.
// The library itself doesn't make any use of this log stream. Check the DEFAULT_LOG_* macros
// for several compile-time configurations. NOTE: This log stream itself is not thread safe!
LogStream & getLog();

// Verbosity of the default log. Refer to the `LogLevel` enum above. Initial
// value for this is `LogLevel::Info`. NOTE: This variable is not thread safe!
int  getLogVerbosityLevel();
void setLogVerbosityLevel(int newLevel);

// Make a message prefix for the default log. `typeToken` is one of: "[f]", "[e]", "[w]", "[i]".
SizedString<256> makeLogMessagePrefix(const String & typeToken, const char * filename, int lineNum);

// Terminates the program with an error code and no cleanup. Similar to `std::abort()`.
ATTRIBUTE_NO_RETURN void errorAbort();

// ========================================================
// Log macros:
// ========================================================

#define LOG_FATAL_EX(logStream, verbosityLevel, message) \
	do { \
		if ((verbosityLevel) >= atlas::core::LogLevel::Fatal) \
		{ \
			(logStream) << atlas::core::makeLogMessagePrefix("[f]", __FILE__, __LINE__) << message << '\n'; \
		} \
		(logStream).flush(); \
		atlas::core::errorAbort(); \
	} while (0)

#define LOG_ERROR_EX(logStream, verbosityLevel, message) \
	do { \
		if ((verbosityLevel) >= atlas::core::LogLevel::Error) \
		{ \
			(logStream) << atlas::core::makeLogMessagePrefix("[e]", __FILE__, __LINE__) << message << '\n'; \
		} \
		(logStream).flush(); \
	} while (0)

#define LOG_WARN_EX(logStream, verbosityLevel, message) \
	do { \
		if ((verbosityLevel) >= atlas::core::LogLevel::Warning) \
		{ \
			(logStream) << atlas::core::makeLogMessagePrefix("[w]", __FILE__, __LINE__) << message << '\n'; \
		} \
	} while (0)

#define LOG_INFO_EX(logStream, verbosityLevel, message) \
	do { \
		if ((verbosityLevel) >= atlas::core::LogLevel::Info) \
		{ \
			(logStream) << atlas::core::makeLogMessagePrefix("[i]", __FILE__, __LINE__) << message << '\n'; \
		} \
	} while (0)

//
// Shorthand versions of the above, hardcoded to use the default log.
// Can be turned into no-ops by defining DEFAULT_LOG_NULL_IMPL.
//
#ifndef DEFAULT_LOG_NULL_IMPL
	#define LOG_FATAL(message) LOG_FATAL_EX(atlas::core::getLog(), atlas::core::getLogVerbosityLevel(), message)
	#define LOG_ERROR(message) LOG_ERROR_EX(atlas::core::getLog(), atlas::core::getLogVerbosityLevel(), message)
	#define LOG_WARN(message)  LOG_WARN_EX(atlas::core::getLog(),  atlas::core::getLogVerbosityLevel(), message)
	#define LOG_INFO(message)  LOG_INFO_EX(atlas::core::getLog(),  atlas::core::getLogVerbosityLevel(), message)
#else // DEFAULT_LOG_NULL_IMPL defined
	#define LOG_FATAL(message)
	#define LOG_ERROR(message)
	#define LOG_WARN(message)
	#define LOG_INFO(message)
#endif // DEFAULT_LOG_NULL_IMPL

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_UTILS_DEFAULT_LOG_HPP
