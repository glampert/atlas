
// ================================================================================================
// -*- C++ -*-
// File: log_stream.hpp
// Author: Guilherme R. Lampert
// Created on: 19/06/15
// Brief: C++streams-like class for textual debug logging/printing.
// ================================================================================================

#ifndef ATLAS_CORE_UTILS_LOG_STREAM_HPP
#define ATLAS_CORE_UTILS_LOG_STREAM_HPP

#include "atlas/core/fs/file_handling.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// class LogStream:
// ========================================================

class LogStream
	: private NonCopyable
{
public:

	LogStream & operator << (char   value);
	LogStream & operator << (bool   value);
	LogStream & operator << (float  value);
	LogStream & operator << (double value);
	LogStream & operator << (long   value);
	LogStream & operator << (ulong  value);
	LogStream & operator << (uint8  value);
	LogStream & operator << (int16  value);
	LogStream & operator << (uint16 value);
	LogStream & operator << (int32  value);
	LogStream & operator << (uint32 value);
	LogStream & operator << (int64  value);
	LogStream & operator << (uint64 value);
	LogStream & operator << (const void * ptr);
	LogStream & operator << (const char * cstr);
	LogStream & operator << (const StringBase & str);
	LogStream & operator << (const ErrorInfo & errorInfo);

	virtual LogStream & newline(); // Same as put('\n')
	virtual LogStream & put(char c) = 0;
	virtual LogStream & write(const char * cstr) = 0;
	virtual LogStream & flush() = 0;
	virtual ~LogStream();
};

// ========================================================
// class FileLogStream:
// ========================================================

class FileLogStream final
	: public LogStream
{
public:

	FileLogStream();
	FileLogStream(const String & filePath, bool unbuffered, ErrorInfo * errorInfo = nullptr);
	bool open(const String & filePath, bool unbuffered, ErrorInfo * errorInfo = nullptr);
	bool close();

	LogStream & put(char c) override;
	LogStream & write(const char * cstr) override;
	LogStream & flush() override;

private:

	void writeHeader();
	WritableFile file;
};

// ========================================================
// class StdLogStream:
// ========================================================

class StdLogStream final
	: public LogStream
{
public:

	enum StdStreamName
	{
		StdOut,
		StdErr
	};

	explicit StdLogStream(StdStreamName streamName);

	LogStream & put(char c) override;
	LogStream & write(const char * cstr) override;
	LogStream & flush() override;

private:

	FILE * stream; // stdout or stderr.
};

// ========================================================
// class NullLogStream:
// ========================================================

class NullLogStream final
	: public LogStream
{
public:

	LogStream & put(char c) override;
	LogStream & write(const char * cstr) override;
	LogStream & flush() override;
};

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_UTILS_LOG_STREAM_HPP
