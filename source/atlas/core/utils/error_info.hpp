
// ================================================================================================
// -*- C++ -*-
// File: error_info.hpp
// Author: Guilherme R. Lampert
// Created on: 19/06/15
// Brief: A simple pair of a system/platform specific error code and an error description string.
// ================================================================================================

#ifndef ATLAS_CORE_UTILS_SYSTEM_ERROR_HPP
#define ATLAS_CORE_UTILS_SYSTEM_ERROR_HPP

#include "atlas/core/strings/dynamic_string.hpp"
#include "atlas/core/strings/to_string.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// struct ErrorInfo:
// ========================================================

class ErrorInfo final
{
public:

	String errorDesc; // Printable error description.
	long   errorCode; // Platform specific integer error code/id.

	ErrorInfo()
		: errorDesc(), errorCode(0) { }

	ErrorInfo(const String & desc, const long code)
		: errorDesc(desc), errorCode(code) { }

	void clear()
	{
		errorDesc.clear();
		errorCode = 0;
	}
};

// ========================================================
// makeError():
// ========================================================

inline void makeError(ErrorInfo * errorInfo, const String & message, const long code = 0)
{
	if (errorInfo != nullptr)
	{
		errorInfo->errorDesc = message;
		errorInfo->errorCode = code;
	}
}

// ========================================================
// toString() for ErrorInfo:
// ========================================================

inline String toString(const ErrorInfo & errorInfo)
{
	String errorMessage;

	if (errorInfo.errorCode != 0)
	{
		errorMessage += "(error code " + toString(errorInfo.errorCode) + ")";
	}

	if (!errorInfo.errorDesc.isEmpty())
	{
		if (errorInfo.errorCode != 0)
		{
			errorMessage += " => ";
		}
		errorMessage += errorInfo.errorDesc;
	}

	if (errorMessage.isEmpty())
	{
		errorMessage = "No errors.";
	}

	return errorMessage;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_UTILS_SYSTEM_ERROR_HPP
