
// ================================================================================================
// -*- C++ -*-
// File: hash_funcs.hpp
// Author: Guilherme R. Lampert
// Created on: 08/01/15
// Brief: Common hash functions, such as CRC-32.
// ================================================================================================

#ifndef ATLAS_CORE_UTILS_HASH_FUNCS_HPP
#define ATLAS_CORE_UTILS_HASH_FUNCS_HPP

#include "atlas/core/utils/basic_types.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// Common hash functions:
// ========================================================

// Signature of a generic hash function.
typedef uint32 (* HashFuncType)(const void *, uint);

// Cyclic redundancy check (CRC-32) hash algorithm.
uint32 hashCrc32(const void * data, uint sizeBytes) ATTRIBUTE_PURE;

// One-at-a-Time hash (OAT) algorithm.
uint32 hashOat(const void * data, uint sizeBytes) ATTRIBUTE_PURE;

// Multiply (MU) and rotate (R) hash algorithm.
uint32 hashMurMur32(const void * data, uint sizeBytes) ATTRIBUTE_PURE;

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_UTILS_HASH_FUNCS_HPP
