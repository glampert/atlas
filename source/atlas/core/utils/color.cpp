
// ================================================================================================
// -*- C++ -*-
// File: color.cpp
// Author: Guilherme R. Lampert
// Created on: 25/06/15
// Brief: Our type for color/pixel representation and a table with some predefined colors.
// ================================================================================================

#include "atlas/core/utils/color.hpp"

namespace atlas
{
namespace core
{
namespace
{

const ubyte colorTable[][4] =
{
	//  R     G     B     A
	{   0,    0,    0,  255 }, // Black
	{   0,    0,  255,  255 }, // Blue
	{ 165,   42,   42,  255 }, // Brown
	{ 127,   31,    0,  255 }, // Copper
	{   0,  255,  255,  255 }, // Cyan
	{   0,    0,  139,  255 }, // DarkBlue
	{ 255,  215,    0,  255 }, // Gold
	{ 128,  128,  128,  255 }, // Gray
	{   0,  255,    0,  255 }, // Green
	{ 195,  223,  223,  255 }, // Ice
	{ 173,  216,  230,  255 }, // LightBlue
	{ 175,  175,  175,  255 }, // LightGray
	{ 135,  206,  250,  255 }, // LightSkyBlue
	{ 210,  105,   30,  255 }, // Lime
	{ 255,    0,  255,  255 }, // Magenta
	{ 128,    0,    0,  255 }, // Maroon
	{ 128,  128,    0,  255 }, // Olive
	{ 255,  165,    0,  255 }, // Orange
	{ 255,  192,  203,  255 }, // Pink
	{ 128,    0,  128,  255 }, // Purple
	{ 255,    0,    0,  255 }, // Red
	{ 192,  192,  192,  255 }, // Silver
	{   0,  128,  128,  255 }, // Teal
	{   0,    0,    0,    0 }, // Transparent
	{ 238,  130,  238,  255 }, // Violet
	{ 255,  255,  255,  255 }, // White
	{ 255,  255,    0,  255 }  // Yellow
};

ATTRIBUTE_TLS XorShiftRandom * colorRandGen = nullptr;

} // namespace {}

COMPILE_TIME_CHECK(sizeof(Color) == 4, "Color type should not be padded!");
COMPILE_TIME_CHECK(arrayLength(colorTable) == ColorIndex::Count, "Keep the color-table in sync with the enum declaration!");

// ========================================================
// getTableColor():
// ========================================================

Color getTableColor(const ColorIndex::Enum colorIndex)
{
	const uint idx = static_cast<uint>(colorIndex);
	DEBUG_CHECK(idx < arrayLength(colorTable));
	return Color(colorTable[idx]);
}

// ========================================================
// getRandomTableColor():
// ========================================================

Color getRandomTableColor()
{
	DEBUG_CHECK(colorRandGen != nullptr && "Call 'setColorRandGen()' first!");
	const uint idx = colorRandGen->nextInteger(0, ColorIndex::Count);
	DEBUG_CHECK(idx < arrayLength(colorTable));
	return Color(colorTable[idx]);
}

// ========================================================
// getRandomColor():
// ========================================================

Color getRandomColor()
{
	DEBUG_CHECK(colorRandGen != nullptr && "Call 'setColorRandGen()' first!");
	const ubyte r = static_cast<ubyte>(colorRandGen->nextInteger(0, 255));
	const ubyte g = static_cast<ubyte>(colorRandGen->nextInteger(0, 255));
	const ubyte b = static_cast<ubyte>(colorRandGen->nextInteger(0, 255));
	return Color(r, g, b, 255);
}

// ========================================================
// getRandomColorWithAlpha():
// ========================================================

Color getRandomColorWithAlpha()
{
	DEBUG_CHECK(colorRandGen != nullptr && "Call 'setColorRandGen()' first!");
	const ubyte r = static_cast<ubyte>(colorRandGen->nextInteger(0, 255));
	const ubyte g = static_cast<ubyte>(colorRandGen->nextInteger(0, 255));
	const ubyte b = static_cast<ubyte>(colorRandGen->nextInteger(0, 255));
	const ubyte a = static_cast<ubyte>(colorRandGen->nextInteger(0, 255));
	return Color(r, g, b, a);
}

// ========================================================
// setColorRandGen():
// ========================================================

void setColorRandGen(XorShiftRandom * randGen)
{
	// May be null. Note that this is a thread local
	// variable, so each thread has to set its own.
	colorRandGen = randGen;
}

} // namespace core {}
} // namespace atlas {}
