
// ================================================================================================
// -*- C++ -*-
// File: basic_types.cpp
// Author: Guilherme R. Lampert
// Created on: 17/12/14
// Brief: Static assertions for our custom sized integer types.
// ================================================================================================

#include "atlas/core/utils/basic_types.hpp"

namespace atlas
{
namespace core
{

void typeSizeCheck()
{
// Compile-time assert compatible with C++98 and C++11:
#if ATLAS_COMPILER_HAS_CPP11
	#define CT_CHECK_SIZE(type, size) \
		static_assert(sizeof(type) == (size), "Size of type '" # type "' was expected to be " # size "!")
#else // !ATLAS_COMPILER_HAS_CPP11
	#define CT_CHECK_SIZE(type, size) \
		typedef int static_assert_ ## type ## _size[(sizeof(type) == (size)) ? 1 : -1]
#endif // ATLAS_COMPILER_HAS_CPP11

// Ensure the types declared in our header match the promised sizes:
CT_CHECK_SIZE(int8   , 1);
CT_CHECK_SIZE(uint8  , 1);
CT_CHECK_SIZE(int16  , 2);
CT_CHECK_SIZE(uint16 , 2);
CT_CHECK_SIZE(int32  , 4);
CT_CHECK_SIZE(uint32 , 4);
CT_CHECK_SIZE(int64  , 8);
CT_CHECK_SIZE(uint64 , 8);
CT_CHECK_SIZE(float32, 4);
CT_CHECK_SIZE(float64, 8);
CT_CHECK_SIZE(int    , 4);
CT_CHECK_SIZE(uint   , 4);
CT_CHECK_SIZE(byte   , 1);
CT_CHECK_SIZE(ubyte  , 1);

// These assumptions are frequently made when manipulating
// pointers and should hold true in any sane compiler:
typedef void* void_pointer;
CT_CHECK_SIZE(void_pointer, sizeof(std::size_t));
CT_CHECK_SIZE(void_pointer, sizeof(std::uintptr_t));
CT_CHECK_SIZE(void_pointer, sizeof(std::ptrdiff_t));

// Clear the namespace.
#undef CT_CHECK_SIZE
}

} // namespace core {}
} // namespace atlas {}
