
// ================================================================================================
// -*- C++ -*-
// File: quick_sort.hpp
// Author: Guilherme R. Lampert
// Created on: 08/01/15
// Brief: Defines the `quickSort()` template function.
// ================================================================================================

#ifndef ATLAS_CORE_UTILS_QUICK_SORT_HPP
#define ATLAS_CORE_UTILS_QUICK_SORT_HPP

#include "atlas/core/utils/utility.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// template struct DefaultQuickSortPredicate<T>:
// ========================================================

// Default predicate that can be used with `quickSort()`.
// Applies operators < and > to type T.
template<class T>
struct DefaultQuickSortPredicate
{
	int operator() (const T & a, const T & b) const
	{
		if (a < b) { return -1; }
		if (a > b) { return +1; }
		return 0;
	}
};

// ========================================================
// quickSort<T, PRED>():
// ========================================================

// Standard quick-sort function for arrays.
// Predicate takes the form: `int pred(const T & a, const T & b)`, returning -1,0,+1.
template<class T, class PRED>
void quickSort(T * array, const uint elementCount, const PRED & pred)
{
	DEBUG_CHECK(array != nullptr);
	if (elementCount == 0)
	{
		return; // Empty array
	}

	// This should be defined to the fastest integer type available
	// in the platform. Should not be smaller in size than `uint`.
	typedef std::int_fast64_t quickInt;

	// Artificial stacks. Avoids recursion.
	static constexpr quickInt MaxLevels = 128;
	quickInt lo[MaxLevels];
	quickInt hi[MaxLevels];

	// `lo` is the lower index, `hi` is the upper index
	// of the region of the array that is being sorted.
	lo[0] = 0;
	hi[0] = elementCount - 1;

	for (quickInt level = 0; level >= 0;)
	{
		quickInt i = lo[level];
		quickInt j = hi[level];

		// Only use quick-sort when there are 4 or more elements in this region
		// and we are below `MaxLevels`. Otherwise fall back to an insertion-sort.
		if ((j - i) >= 4 && level < (MaxLevels - 1))
		{
			// Use the center element as the pivot.
			// The median of a multi point sample could be used
			// but simply taking the center works quite well.
			const quickInt p = (i + j) / 2;

			// Move the pivot element to the end of the region.
			swap(array[j], array[p]);

			// Get a reference to the pivot element.
			T & pivot = array[j--];

			// Partition the region:
			do
			{
				while (pred(array[i], pivot) < 0) { if (++i >= j) break; }
				while (pred(array[j], pivot) > 0) { if (--j <= i) break; }
				if (i >= j) { break; }
				swap(array[i], array[j]);
			}
			while (++i < --j);

			// Without these iterations sorting of arrays with many duplicates may
			// become really slow because the partitioning can be very unbalanced.
			// However, these iterations are unnecessary if all elements are unique.
			while (pred(array[i], pivot) <= 0 && i < hi[level]) { ++i; }
			while (pred(array[j], pivot) >= 0 && lo[level] < j) { --j; }

			// Move the pivot element in place:
			swap(pivot, array[i]);

			// Make sure we are not going overboard...
			DEBUG_CHECK(level < MaxLevels - 1);

			// Adjust the recursion stack:
			lo[level + 1] = i;
			hi[level + 1] = hi[level];
			hi[level] = j;
			++level;
		}
		else
		{
			// Insertion-sort of the remaining elements.
			for (; i < j; --j)
			{
				quickInt m = i;
				for (quickInt k = i + 1; k <= j; ++k)
				{
					if (pred(array[k], array[m]) > 0)
					{
						m = k;
					}
				}
				swap(array[m], array[j]);
			}
			--level;
		}
	}
}

// ========================================================
// quickInt<T, N, PRED>():
// ========================================================

// Overload of `quickSort()` that operates on statically allocated arrays.
template<class T, uint N, class PRED>
void quickSort(T (&array)[N], const PRED & pred)
{
	quickSort(array, N, pred);
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_UTILS_QUICK_SORT_HPP
