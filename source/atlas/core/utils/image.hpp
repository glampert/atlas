
// ================================================================================================
// -*- C++ -*-
// File: image.hpp
// Author: Guilherme R. Lampert
// Created on: 26/06/15
// Brief: Image loading and manipulation.
// ================================================================================================

#ifndef ATLAS_CORE_UTILS_IMAGE_HPP
#define ATLAS_CORE_UTILS_IMAGE_HPP

#include "atlas/core/containers/arrays.hpp"
#include "atlas/core/strings/dynamic_string.hpp"
#include "atlas/core/maths/vectors.hpp"
#include "atlas/core/utils/error_info.hpp"
#include "atlas/core/utils/color.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// Forward declarations:
// ========================================================

class Image;
class ImageSurface;
class PixelFormat;

// ========================================================
// struct PixelDataLayoutDesc:
// ========================================================

struct PixelDataLayoutDesc
{
	uint bytesPerPixel;
	uint channelCount;
	uint dataLayout;
	bool isCompressed;
	bool supportsPixelAccess;
	const char * string;

	void (* setPixelFunc)(ImageSurface &, uint, uint, const ubyte *);
	void (* getPixelFunc)(const ImageSurface &, uint, uint, ubyte *);
};

// ========================================================
// struct ImageLoadParams:
// ========================================================

struct ImageLoadParams
{
	bool forceRgba;             // Image is converted to RGBA if not already.
	bool flipVerticallyOnLoad;  // Image is flipped vertically if loading succeeds.
	bool roundDownToPowerOfTwo; // Image is rounded down to its nearest PoT size if not already.
	bool generateMipmaps;       // Trigger mipmap surface generation after loading.

	ImageLoadParams()
		: forceRgba(false)
		, flipVerticallyOnLoad(false)
		, roundDownToPowerOfTwo(true)
		, generateMipmaps(false)
	{ }
};

// ========================================================
// class PixelFormat:
// ========================================================

class PixelFormat
{
public:

	enum DataLayout
	{
		Null,  // Undefined/null layout. Not a valid image.

		R8,    // Red channel only, 8-bits per pixel (also used for grayscale).
		RG8,   // Red and green channels, 8-bits per channel, 16-bits per pixel.

		RGB8,  // Red, green and blue channels, 8-bits per channel, 24-bits per pixel.
		RGBA8, // Red, green, blue and alpha channels, 8-bits per channel, 32-bits per pixel.

		BGR8,  // Blue, green and red channels, 8-bits per channel, 24-bits per pixel.
		BGRA8, // Blue, green, red and alpha channels, 8-bits per channel, 32-bits per pixel.

		// Number of entries in this enum.
		// For internal use only.
		Count
	};

	static PixelFormat fromLayout(DataLayout layout);

	PixelFormat();

	DataLayout getDataLayout() const;
	uint getBytesPerPixel()    const;
	uint getChannelCount()     const;

	bool isValid()             const;
	bool isCompressed()        const;
	bool supportsPixelAccess() const;

	const char * toString() const;
	void invalidate();

	bool operator == (const PixelFormat & other) const;
	bool operator != (const PixelFormat & other) const;

	bool operator == (DataLayout dataLayout) const;
	bool operator != (DataLayout dataLayout) const;

	void setPixel(ImageSurface & surface, uint pX, uint pY, const ubyte * pixelIn)  const;
	void getPixel(const ImageSurface & surface, uint pX, uint pY, ubyte * pixelOut) const;

private:

	const PixelDataLayoutDesc * dataLayoutDesc;
};

// PixelFormat to a printable string. Provided for consistency with the global toString()s.
inline String toString(const PixelFormat & pixelFormat) { return pixelFormat.toString(); }

// ========================================================
// class ImageSurface:
// ========================================================

class ImageSurface final
	: private NonCopyable
{
public:

	ImageSurface();

	// Miscellaneous accessors:
	bool isValid()        const;
	bool isPowerOfTwo()   const;
	uint getWidth()       const;
	uint getHeight()      const;
	Vec2u getDimensions() const;
	uint getPixelCount()  const;

	// Access the pixel data as an opaque array of bytes:
	const ubyte * getPixelData() const;
	ubyte * getPixelData();

	// Sets the surface to invalid (null data and zero size).
	void invalidate();

private:

	friend class Image;

	// Opaque pixel data. Data layout depends on the pixel format of the parent image.
	// This comes from the Image that owns this surface, so it is not freed by ImageSurface.
	ubyte * pixelData;

	// Width(x) and height(y) of this surface.
	Vec2u dimensions;
};

// ========================================================
// class Image:
// ========================================================

class Image final
	: private NonCopyable
{
public:

	// A shorthand alias.
	typedef PixelFormat::DataLayout DataLayout;

	// The maximum number of mipmap levels for a single image.
	// This is more than enough for current-generation graphics cards.
	// A 65536 x 65536 (2^16) image has a maximum of 16 levels.
	static constexpr uint MaxSurfaces = 16;

	// Constructs an empty (and invalid) image.
	// Destructor frees any allocated resources.
	 Image();
	~Image();

	//
	// Image initialization and memory management:
	//

	// Deallocates the image storage and frees all surfaces,
	// setting this image to invalid. Called automatically by the destructor.
	void deallocate();

	// Creates a RGBA8 64 x 64 checkerboard pattern image. The number of checker squares
	// may range from 2 to 64, as long as it is a power-of-two. Single surface.
	bool initWithCheckerPattern(uint squares);

	// Allocate uninitialized memory for the image. Single surface.
	bool initWithDimension(uint width, uint height, DataLayout dataLayout);

	// Allocate and fill with given color value. Single surface.
	bool initWithColorFill(uint width, uint height, DataLayout dataLayout, Color fillColor);

	// Initialize from an externally allocated memory block. The Image will take ownership
	// of the pointer and will free it on destruction. Creates a single base surface.
	bool initWithExternalData(ubyte * baseSurface, uint width, uint height, DataLayout dataLayout);

	// Attempt to initialize the image from a file on the file system.
	// Will create a single base surface if the file succeeds to load.
	// Dimensions and pixel format are inferred from the image file.
	// Supports: TGA, JPEG, BMP, PNG and GIF(without animation).
	bool initFromFile(const String & filePath,
	                  const ImageLoadParams & imageParams,
	                  ErrorInfo * errorInfo = nullptr);

	// Initialize the image from the contents of an image file loaded
	// into main memory. Same behavior of `initFromFile()`.
	bool initFromFileInMemory(const ByteArray & fileContents,
	                          const ImageLoadParams & imageParams,
	                          const String & filePathOpt = "",
	                          ErrorInfo * errorInfo = nullptr);

	// Transfers the contents of this image into `dest`, making this image empty/invalid.
	// Will generate a runtime assertion if `dest` is not an empty image.
	void transferTo(Image & dest);

	//
	// Per-surface operations:
	//

	// Writes the given image surface to a file in the local file system. Overwrites any existing
	// file with the same name. If the file path has no extension, the param provided in
	// `extension` is used. Otherwise, if both are provided, they must match. You may also
	// pass an empty string to `extension` if the file path already contains one.
	// The format of the output image file will be inferred from the extension.
	// Supports: TGA, BMP and PNG.
	bool writeToFile(const String & filePath, const String & extension,
	                 uint surface = 0, ErrorInfo * errorInfo = nullptr) const;

	// Dumps all surfaces in this image to files. Used mostly for debugging the mipmap generation code.
	// Path/directories must already exits! Output file names will be in the format:
	//  <basePathName>_<surfaceIndex>.<extension>
	bool writeAllSurfacesToFiles(const String & basePathName, const String & extension) const;

	// Get/set individual pixels for a given image surface.
	void setPixel(uint pX, uint pY, const ubyte * pixelIn, uint surface = 0);
	void getPixel(uint pX, uint pY, ubyte * pixelOut, uint surface = 0) const;

	// Swap the pixel at pX0/pY0 with the one at pX1/pY1.
	void swapPixel(uint pX0, uint pY0, uint pX1, uint pY1, uint surface = 0);

	// Applies the user supplied function to every pixel of the given image surface.
	void doForEveryPixel(void (* func)(ubyte *, const PixelFormat &), uint surface = 0);

	// Flips the image surface vertically in-place without copying.
	void flipVInPlace(uint surface = 0);

	// Flips the image surface horizontally in-place without copying.
	void flipHInPlace(uint surface = 0);

	// Swizzle the fist and third color channels of the surface,
	// effectively changing from RGB[A] to BGR[A] or vice versa.
	void swizzleRedBlue(uint surface = 0);

	// Set every byte of the image's surface to the given value.
	// Equivalent to C's `memset()` function.
	void byteFill(ubyte fillWith, uint surface = 0);

	//
	// Operations on the image's base surface (surface=0):
	//

	// Generates a set of mipmap surface from the base surface (surface=0).
	// This will fail if the image is invalid (e.g. has no surfaces).
	// Uses an implementation defined filter. Might allocate some memory.
	void generateMipmapSurfaces();

	// Creates a vertically flipped copy of the base surface of this image.
	void flipVCopyBaseSurface(Image & flippedImage) const;

	// Creates a horizontally flipped copy of the base surface of this image.
	void flipHCopyBaseSurface(Image & flippedImage) const;

	// Resizes this image's base surface in-place.
	void resizeBaseSurfaceInPlace(uint targetWidth, uint targetHeight);

	// Creates a resized copy of this image's base surface.
	void resizeCopyBaseSurface(Image & destImage, uint targetWidth, uint targetHeight) const;

	// Rounds this image's base surface down to its nearest power-of-2 dimensions. No-op if already PoT.
	void roundDownBaseSurfaceToPowerOfTwo();

	// Discard the alpha component of an RGBA/BGRA image's base surface.
	void discardBaseSurfaceAlphaChannel();

	// Copy a portion of this image's base surface to the destination image.
	// If `topLeft` is true, the (0,0) origin of the image is assumed to be the top-left corner. Otherwise,
	// the origin point is the bottom-left corner. This applies to both the source and destination images.
	void copyBaseSurfaceRect(Image & destImage, uint xOffset, uint yOffset,
	                         uint rectWidth, uint rectHeight, bool topLeft) const;

	// Set a portion of this image's base surface from a source image.
	// If `topLeft` is true, the (0,0) origin of the image is assumed to be the top-left corner. Otherwise,
	// the origin point is the bottom-left corner. This applies to both the source and destination images.
	void setBaseSurfaceRect(const Image & srcImage, uint xOffset, uint yOffset,
	                        uint rectWidth, uint rectHeight, bool topLeft);

	// `setBaseSurfaceRect()` overload that takes a raw pointer as data source.
	// Data is assumed to be of the same pixel format as this image's base surface.
	void setBaseSurfaceRect(const ubyte * RESTRICT image, uint xOffset, uint yOffset,
	                        uint rectWidth, uint rectHeight, bool topLeft);

	//
	// Miscellaneous accessors:
	//
	const PixelFormat & getPixelFormat() const;
	DataLayout getDataLayout() const;
	uint getBytesPerPixel()    const;
	uint getChannelCount()     const;
	uint getMemoryBytes()      const;

	bool isValid() const;
	bool isCompressed() const;
	bool supportsPixelAccess() const;

	bool isPowerOfTwo(uint surface = 0)   const;
	uint getWidth(uint surface  = 0)      const;
	uint getHeight(uint surface = 0)      const;
	Vec2u getDimensions(uint surface = 0) const;

	//
	// Access the pixel data as an opaque array
	// of bytes (fist surface/level only):
	//
	const ubyte * getPixelDataBaseSurface() const;
	ubyte * getPixelDataBaseSurface();

	//
	// Surface access:
	//
	uint getSurfaceCount() const;
	const ImageSurface & getSurface(uint surface) const;
	ImageSurface & getSurface(uint surface);

private:

	// Pointer to the pixel data of the first surface. Usually the one loaded from file.
	ubyte * dataBaseSurface;

	// If this image has other surfaces (mipmaps), then they are all allocated from this chunk.
	ubyte * dataMipmapSurfaces;

	// How to interpret the pixel data for all surfaces.
	PixelFormat pixelFormat;

	// Entries used in the `surfaces` array. At least one for a valid image.
	uint surfaceCount;

	// Estimate bytes used to store this image (for all surfaces).
	uint memoryUsage;

	// Array of surfaces. Used to store image mipmap levels.
	SizedArray<ImageSurface, MaxSurfaces> surfaces;
};

// ========================================================
// Procedural image generation helpers:
// ========================================================

//
// Fills the color buffer with a checkerboard pattern:
// +--+--+--+--+--+
// |##|  |##|  |##|
// |--|--|--|--|--|
// |  |##|  |##|  |
// |--|--|--|--|--|
// |##|  |##|  |##|
// +--+--+--+--+--+
// `squares` is the number of checker squares in the image.
// It must be >= 2 and width/height must divide evenly by it.
//
void fillBufferWithCheckerPattern(Color * buffer, uint width, uint height,
                                  uint squares, const Color colors[2]);

//
// Fills the color buffer with a pattern of vertical (optionally horizontal) stripes:
// +-+-+-+-+-+-+-+-+
// |#| |#| |#| |#| |
// |#| |#| |#| |#| |
// |#| |#| |#| |#| |
// |#| |#| |#| |#| |
// |#| |#| |#| |#| |
// |#| |#| |#| |#| |
// +-+-+-+-+-+-+-+-+
// `lineThickness` is the thickness in pixels of each stripe.
// It must be >= 1 or evenly divisible by 2.
//
void fillBufferWithStripePattern(Color * buffer, uint width, uint height,
                                 uint lineThickness, bool vertical,
                                 const Color colors[2]);

//
// Fills the color buffer with a pattern of concentric boxes:
// +----------------+
// | +------------+ |
// | | +--------+ | |
// | | | +----+ | | |
// | | | | || | | | |
// | | | +----+ | | |
// | | +--------+ | |
// | +------------+ |
// +----------------+
// `lineThickness` is the thickness in pixels of each line making a box.
// It must be >= 1 or evenly divisible by 2.
//
void fillBufferWithBoxPattern(Color * buffer, uint width, uint height,
                              uint lineThickness, const Color colors[2]);

//
// Fills the color buffer with color[0] then adds a 1 pixel border to it using color[1].
// +----------------+
// | +------------+ |
// | |            | |
// | |            | |
// | |            | |
// | |            | |
// | +------------+ |
// +----------------+
// This function currently requires a square image, so it asserts that width == height.
//
void fillBufferWithBoundedRect(Color * buffer, uint width, uint height, const Color colors[2]);

//
// Fills the whole color buffer with the given color.
// +----------------+
// |################|
// |################|
// |################|
// |################|
// |################|
// |################|
// +----------------+
//
void fillBufferWithSolidColor(Color * buffer, uint width, uint height, Color fillColor);

// ================================================================================================
// PixelFormat inline methods:
// ================================================================================================

// ========================================================
// PixelFormat::PixelFormat():
// ========================================================

inline PixelFormat::PixelFormat()
	: dataLayoutDesc(nullptr)
{
}

// ========================================================
// PixelFormat::getDataLayout():
// ========================================================

inline PixelFormat::DataLayout PixelFormat::getDataLayout() const
{
	DEBUG_CHECK(dataLayoutDesc != nullptr);
	return static_cast<DataLayout>(dataLayoutDesc->dataLayout);
}

// ========================================================
// PixelFormat::getBytesPerPixel():
// ========================================================

inline uint PixelFormat::getBytesPerPixel() const
{
	DEBUG_CHECK(dataLayoutDesc != nullptr);
	return dataLayoutDesc->bytesPerPixel;
}

// ========================================================
// PixelFormat::getChannelCount():
// ========================================================

inline uint PixelFormat::getChannelCount() const
{
	DEBUG_CHECK(dataLayoutDesc != nullptr);
	return dataLayoutDesc->channelCount;
}

// ========================================================
// PixelFormat::isValid():
// ========================================================

inline bool PixelFormat::isValid() const
{
	return dataLayoutDesc != nullptr;
}

// ========================================================
// PixelFormat::isCompressed():
// ========================================================

inline bool PixelFormat::isCompressed() const
{
	DEBUG_CHECK(dataLayoutDesc != nullptr);
	return dataLayoutDesc->isCompressed;
}

// ========================================================
// PixelFormat::supportsPixelAccess():
// ========================================================

inline bool PixelFormat::supportsPixelAccess() const
{
	DEBUG_CHECK(dataLayoutDesc != nullptr);
	return dataLayoutDesc->supportsPixelAccess;
}

// ========================================================
// PixelFormat::toString():
// ========================================================

inline const char * PixelFormat::toString() const
{
	return (dataLayoutDesc != nullptr) ? dataLayoutDesc->string : "(null)";
}

// ========================================================
// PixelFormat::invalidate():
// ========================================================

inline void PixelFormat::invalidate()
{
	dataLayoutDesc = nullptr;
}

// ========================================================
// PixelFormat::operator == :
// ========================================================

inline bool PixelFormat::operator == (const PixelFormat & other) const
{
	return getDataLayout() == other.getDataLayout();
}

// ========================================================
// PixelFormat::operator != :
// ========================================================

inline bool PixelFormat::operator != (const PixelFormat & other) const
{
	return getDataLayout() != other.getDataLayout();
}

// ========================================================
// PixelFormat::operator == :
// ========================================================

inline bool PixelFormat::operator == (const DataLayout dataLayout) const
{
	return getDataLayout() == dataLayout;
}

// ========================================================
// PixelFormat::operator != :
// ========================================================

inline bool PixelFormat::operator != (const DataLayout dataLayout) const
{
	return getDataLayout() != dataLayout;
}

// ========================================================
// PixelFormat::setPixel():
// ========================================================

inline void PixelFormat::setPixel(ImageSurface & surface, const uint pX, const uint pY, const ubyte * pixelIn) const
{
	DEBUG_CHECK(surface.isValid());
	DEBUG_CHECK(pX < surface.getWidth());
	DEBUG_CHECK(pY < surface.getHeight());
	DEBUG_CHECK(pixelIn != nullptr);

	DEBUG_CHECK(dataLayoutDesc != nullptr);
	DEBUG_CHECK(dataLayoutDesc->setPixelFunc != nullptr && "Pixel access not supported for this data format!");

	dataLayoutDesc->setPixelFunc(surface, pX, pY, pixelIn);
}

// ========================================================
// PixelFormat::getPixel():
// ========================================================

inline void PixelFormat::getPixel(const ImageSurface & surface, const uint pX, const uint pY, ubyte * pixelOut) const
{
	DEBUG_CHECK(surface.isValid());
	DEBUG_CHECK(pX < surface.getWidth());
	DEBUG_CHECK(pY < surface.getHeight());
	DEBUG_CHECK(pixelOut != nullptr);

	DEBUG_CHECK(dataLayoutDesc != nullptr);
	DEBUG_CHECK(dataLayoutDesc->getPixelFunc != nullptr && "Pixel access not supported for this data format!");

	dataLayoutDesc->getPixelFunc(surface, pX, pY, pixelOut);
}

// ================================================================================================
// ImageSurface inline methods:
// ================================================================================================

// ========================================================
// ImageSurface::ImageSurface():
// ========================================================

inline ImageSurface::ImageSurface()
	: pixelData(nullptr)
	, dimensions(0,0)
{
}

// ========================================================
// ImageSurface::isValid():
// ========================================================

inline bool ImageSurface::isValid() const
{
	return dimensions.x != 0 &&
	       dimensions.y != 0 &&
	       pixelData != nullptr;
}

// ========================================================
// ImageSurface::isPowerOfTwo():
// ========================================================

inline bool ImageSurface::isPowerOfTwo() const
{
	return ((dimensions.x & (dimensions.x - 1)) == 0) &&
	       ((dimensions.y & (dimensions.y - 1)) == 0);
}

// ========================================================
// ImageSurface::getWidth():
// ========================================================

inline uint ImageSurface::getWidth() const
{
	return dimensions.x;
}

// ========================================================
// ImageSurface::getHeight():
// ========================================================

inline uint ImageSurface::getHeight() const
{
	return dimensions.y;
}

// ========================================================
// ImageSurface::getDimensions():
// ========================================================

inline Vec2u ImageSurface::getDimensions() const
{
	return dimensions;
}

// ========================================================
// ImageSurface::getPixelCount():
// ========================================================

inline uint ImageSurface::getPixelCount() const
{
	return dimensions.x * dimensions.y;
}

// ========================================================
// ImageSurface::getPixelData():
// ========================================================

inline const ubyte * ImageSurface::getPixelData() const
{
	return pixelData;
}

// ========================================================
// ImageSurface::getPixelData():
// ========================================================

inline ubyte * ImageSurface::getPixelData()
{
	return pixelData;
}

// ========================================================
// ImageSurface::invalidate():
// ========================================================

inline void ImageSurface::invalidate()
{
	pixelData = nullptr;
	dimensions.x = 0;
	dimensions.y = 0;
}

// ================================================================================================
// Image inline methods:
// ================================================================================================

// ========================================================
// Image::getPixelFormat():
// ========================================================

inline const PixelFormat & Image::getPixelFormat() const
{
	return pixelFormat;
}

// ========================================================
// Image::getDataLayout():
// ========================================================

inline Image::DataLayout Image::getDataLayout() const
{
	return pixelFormat.getDataLayout();
}

// ========================================================
// Image::getBytesPerPixel():
// ========================================================

inline uint Image::getBytesPerPixel() const
{
	return pixelFormat.getBytesPerPixel();
}

// ========================================================
// Image::getChannelCount():
// ========================================================

inline uint Image::getChannelCount() const
{
	return pixelFormat.getChannelCount();
}

// ========================================================
// Image::getMemoryBytes():
// ========================================================

inline uint Image::getMemoryBytes() const
{
	return memoryUsage;
}

// ========================================================
// Image::isValid():
// ========================================================

inline bool Image::isValid() const
{
	return dataBaseSurface != nullptr &&
	       surfaceCount    != 0       &&
	       pixelFormat.isValid();
}

// ========================================================
// Image::isCompressed():
// ========================================================

inline bool Image::isCompressed() const
{
	return pixelFormat.isCompressed();
}

// ========================================================
// Image::supportsPixelAccess():
// ========================================================

inline bool Image::supportsPixelAccess() const
{
	return pixelFormat.supportsPixelAccess();
}

// ========================================================
// Image::isPowerOfTwo():
// ========================================================

inline bool Image::isPowerOfTwo(const uint surface) const
{
	return getSurface(surface).isPowerOfTwo();
}

// ========================================================
// Image::getWidth():
// ========================================================

inline uint Image::getWidth(const uint surface) const
{
	return getSurface(surface).getWidth();
}

// ========================================================
// Image::getHeight():
// ========================================================

inline uint Image::getHeight(const uint surface) const
{
	return getSurface(surface).getHeight();
}

// ========================================================
// Image::getDimensions():
// ========================================================

inline Vec2u Image::getDimensions(const uint surface) const
{
	return getSurface(surface).getDimensions();
}

// ========================================================
// Image::getPixelDataBaseSurface():
// ========================================================

inline const ubyte * Image::getPixelDataBaseSurface() const
{
	DEBUG_CHECK(dataBaseSurface != nullptr);
	return dataBaseSurface;
}

// ========================================================
// Image::getPixelDataBaseSurface()
// ========================================================

inline ubyte * Image::getPixelDataBaseSurface()
{
	DEBUG_CHECK(dataBaseSurface != nullptr);
	return dataBaseSurface;
}

// ========================================================
// Image::getSurfaceCount():
// ========================================================

inline uint Image::getSurfaceCount() const
{
	return surfaceCount;
}

// ========================================================
// Image::getSurface():
// ========================================================

inline const ImageSurface & Image::getSurface(const uint surface) const
{
	DEBUG_CHECK(surface < surfaceCount && "Invalid surface index!");
	return surfaces[surface];
}

// ========================================================
// Image::getSurface():
// ========================================================

inline ImageSurface & Image::getSurface(const uint surface)
{
	DEBUG_CHECK(surface < surfaceCount && "Invalid surface index!");
	return surfaces[surface];
}

// ========================================================
// Image::setPixel():
// ========================================================

inline void Image::setPixel(const uint pX, const uint pY, const ubyte * pixelIn, const uint surface)
{
	pixelFormat.setPixel(getSurface(surface), pX, pY, pixelIn);
}

// ========================================================
// Image::getPixel():
// ========================================================

inline void Image::getPixel(const uint pX, const uint pY, ubyte * pixelOut, const uint surface) const
{
	pixelFormat.getPixel(getSurface(surface), pX, pY, pixelOut);
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_UTILS_IMAGE_HPP
