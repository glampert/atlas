
// ================================================================================================
// -*- C++ -*-
// File: log_stream.cpp
// Author: Guilherme R. Lampert
// Created on: 20/06/15
// Brief: C++streams-like class for textual debug logging/printing.
// ================================================================================================

#include "atlas/core/utils/log_stream.hpp"

namespace atlas
{
namespace core
{

// ================================================================================================
// LogStream class implementation:
// ================================================================================================

// ========================================================
// LogStream::LogStream():
// ========================================================

LogStream::~LogStream()
{
	// Empty.
}

// ========================================================
// LogStream::newline():
// ========================================================

LogStream & LogStream::newline()
{
	return put('\n');
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const char value)
{
	return put(value);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const bool value)
{
	return write(value ? "true" : "false");
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const float value)
{
	char buffer[128] = {0};
	strFromFloat(value, buffer, arrayLength(buffer), /* decimalDigits = */ -1);
	strTrimTrailingFloatZeros(buffer);
	return write(buffer);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const double value)
{
	char buffer[128] = {0};
	strFromFloat(value, buffer, arrayLength(buffer), /* decimalDigits = */ -1);
	strTrimTrailingFloatZeros(buffer);
	return write(buffer);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const long value)
{
	char buffer[128] = {0};
	strFromInt(static_cast<int64>(value), buffer, arrayLength(buffer), /* base = */ 10);
	return write(buffer);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const ulong value)
{
	char buffer[128] = {0};
	strFromInt(static_cast<uint64>(value), buffer, arrayLength(buffer), /* base = */ 10);
	return write(buffer);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const uint8 value)
{
	char buffer[128] = {0};
	strFromInt(value, buffer, arrayLength(buffer), /* base = */ 10);
	return write(buffer);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const int16 value)
{
	char buffer[128] = {0};
	strFromInt(value, buffer, arrayLength(buffer), /* base = */ 10);
	return write(buffer);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const uint16 value)
{
	char buffer[128] = {0};
	strFromInt(value, buffer, arrayLength(buffer), /* base = */ 10);
	return write(buffer);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const int32 value)
{
	char buffer[128] = {0};
	strFromInt(value, buffer, arrayLength(buffer), /* base = */ 10);
	return write(buffer);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const uint32 value)
{
	char buffer[128] = {0};
	strFromInt(value, buffer, arrayLength(buffer), /* base = */ 10);
	return write(buffer);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const int64 value)
{
	char buffer[128] = {0};
	strFromInt(value, buffer, arrayLength(buffer), /* base = */ 10);
	return write(buffer);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const uint64 value)
{
	char buffer[128] = {0};
	strFromInt(value, buffer, arrayLength(buffer), /* base = */ 10);
	return write(buffer);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const void * ptr)
{
	char buffer[128] = {0};
	strPrintF(buffer, arrayLength(buffer), "%p", ptr);
	return write(buffer);
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const char * cstr)
{
	return write((cstr != nullptr) ? cstr : "(null)");
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const StringBase & str)
{
	return write(str.getCStr());
}

// ========================================================
// LogStream::operator << :
// ========================================================

LogStream & LogStream::operator << (const ErrorInfo & errorInfo)
{
	const String errorMessage = toString(errorInfo);
	return write(errorMessage.getCStr());
}

// ================================================================================================
// FileLogStream class implementation:
// ================================================================================================

constexpr ulong LogFileOpenFlags = File::Flag::Writable | File::Flag::Text | File::Flag::Truncate;

// ========================================================
// FileLogStream::FileLogStream():
// ========================================================

FileLogStream::FileLogStream()
	: file()
{
}

// ========================================================
// FileLogStream::FileLogStream():
// ========================================================

FileLogStream::FileLogStream(const String & filePath, const bool unbuffered, ErrorInfo * errorInfo)
	: file(filePath, LogFileOpenFlags | (unbuffered ? File::Flag::Unbuffered : 0), errorInfo)
{
	writeHeader();
}

// ========================================================
// FileLogStream::open():
// ========================================================

bool FileLogStream::open(const String & filePath, const bool unbuffered, ErrorInfo * errorInfo)
{
	if (!file.open(filePath, LogFileOpenFlags | (unbuffered ? File::Flag::Unbuffered : 0), errorInfo))
	{
		return false;
	}

	writeHeader();
	return true;
}

// ========================================================
// FileLogStream::close():
// ========================================================

bool FileLogStream::close()
{
	return file.close();
}

// ========================================================
// FileLogStream::put():
// ========================================================

LogStream & FileLogStream::put(const char c)
{
	if (file.isOpen() && (ctype::isSpace(c) || ctype::isPrint(c)))
	{
		file.writeChar(c);
	}
	return *this;
}

// ========================================================
// FileLogStream::write():
// ========================================================

LogStream & FileLogStream::write(const char * cstr)
{
	// Log should be as fault tolerant as possible,
	// so ignore bad inputs silently.
	if (!file.isOpen() || cstr == nullptr || *cstr == '\0')
	{
		return *this;
	}

	#if ATLAS_FILTER_COLOR_CODES_FROM_LOG_OUTPUT
	while (*cstr != '\0')
	{
		if (*cstr == '\033')
		{
			while (*cstr != '\0')
			{
				if (*cstr == 'm')
				{
					++cstr;
					break;
				}
				++cstr;
			}
			if (*cstr == '\0')
			{
				break;
			}
		}
		put(*cstr);
		++cstr;
	}
	#else // !ATLAS_FILTER_COLOR_CODES_FROM_LOG_OUTPUT
	file.writeString(cstr);
	#endif // ATLAS_FILTER_COLOR_CODES_FROM_LOG_OUTPUT

	return *this;
}

// ========================================================
// FileLogStream::flush():
// ========================================================

LogStream & FileLogStream::flush()
{
	if (file.isOpen())
	{
		file.flush();
	}
	return *this;
}

// ========================================================
// FileLogStream::writeHeader():
// ========================================================

void FileLogStream::writeHeader()
{
	const String timestamp = File::timestampString(file.getFileStats().lastModificationTime);

	write("\n");
	write("========================================================\n");
	write("Log session started on: "); write(timestamp.getCStr()); write("\n");
	write("========================================================\n");
	write("\n");
}

// ================================================================================================
// StdLogStream class implementation:
// ================================================================================================

// ========================================================
// StdLogStream::StdLogStream():
// ========================================================

StdLogStream::StdLogStream(const StdStreamName streamName)
{
	switch (streamName)
	{
	case StdOut : { stream = stdout; } break;
	case StdErr : { stream = stderr; } break;
	default : ALWAYS_CHECK(false && "Stream must be either \'stdout\' or \'stderr\'!");
	} // switch (streamName)

	write("\n");
	write("========================================================\n");
	write("Starting new log session.\n");
	write("========================================================\n");
	write("\n");
}

// ========================================================
// StdLogStream::put():
// ========================================================

LogStream & StdLogStream::put(const char c)
{
	if (ctype::isSpace(c) || ctype::isPrint(c))
	{
		std::fputc(c, stream);
	}
	return *this;
}

// ========================================================
// StdLogStream::write():
// ========================================================

LogStream & StdLogStream::write(const char * cstr)
{
	// Log should be as fault tolerant as possible,
	// so ignore bad inputs silently.
	if (cstr == nullptr || *cstr == '\0')
	{
		return *this;
	}

	//
	// NOTE: ANSI color codes are not filtered out
	// here because it is assumed that when using this
	// log implementation the output is being directed
	// to a terminal (default STDOUT and STDERR output),
	// where the color codes are meaningful.
	//
	std::fputs(cstr, stream);
	return *this;
}

// ========================================================
// StdLogStream::flush():
// ========================================================

LogStream & StdLogStream::flush()
{
	std::fflush(stream);
	return *this;
}

// ================================================================================================
// NullLogStream class implementation:
// ================================================================================================

// Null log stream is a no-op that ignores all messages.
LogStream & NullLogStream::put(const char)     { return *this; }
LogStream & NullLogStream::write(const char *) { return *this; }
LogStream & NullLogStream::flush()             { return *this; }

} // namespace core {}
} // namespace atlas {}
