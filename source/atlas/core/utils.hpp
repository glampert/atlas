
// ================================================================================================
// -*- C++ -*-
// File: utils.hpp
// Author: Guilherme R. Lampert
// Created on: 08/01/15
// Brief: Master header of the utilities submodule.
//        Including this header makes all the utils functions and types available.
// ================================================================================================

#ifndef ATLAS_CORE_UTILS_HPP
#define ATLAS_CORE_UTILS_HPP

#include "atlas/core/utils/utility.hpp"
#include "atlas/core/utils/bin_search.hpp"
#include "atlas/core/utils/quick_sort.hpp"
#include "atlas/core/utils/hash_funcs.hpp"
#include "atlas/core/utils/error_info.hpp"
#include "atlas/core/utils/color.hpp"
#include "atlas/core/utils/image.hpp"
#include "atlas/core/utils/time.hpp"
#include "atlas/core/utils/log_stream.hpp"
#include "atlas/core/utils/default_log.hpp"

#endif // ATLAS_CORE_UTILS_HPP
