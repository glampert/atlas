
// ================================================================================================
// -*- C++ -*-
// File: file_handling.cpp
// Author: Guilherme R. Lampert
// Created on: 19/06/15
// Brief: File access classes and a portable FileSystem wrapper.
// ================================================================================================

#include "atlas/core/fs/file_handling.hpp"
#include "atlas/core/fs/path_utils.hpp"
#include "atlas/core/memory/mem_utils.hpp"
#include <errno.h>

namespace atlas
{
namespace core
{

// ================================================================================================
// Miscellaneous local helpers:
// ================================================================================================

namespace
{

// ========================================================
// asFILE() [LOCAL]:
// ========================================================

inline FILE * asFILE(File::Handle h)
{
	// File::Handle is a typedef to `void *`
	return reinterpret_cast<FILE *>(h);
}

// ========================================================
// canOpenFileWithFlags() [LOCAL]:
// ========================================================

bool canOpenFileWithFlags(const uint flags, const bool isTextFile,
                          const bool alreadyExists, char * fopenFlagsOut,
                          ErrorInfo * errorInfo)
{
	//
	// Convert a File::Flag set to a fopen() 'mode' string
	// and check if we can fopen() with the given flags.
	//

	int i = 0;
	fopenFlagsOut[0] = '\0';

	if ((flags & File::Flag::Readable) && (flags & File::Flag::Writable)) // Read & write:
	{
		if (flags & File::Flag::NoCreate)
		{
			// File not there, fail.
			if (!alreadyExists)
			{
				makeError(errorInfo, "Mode RW: File not found!");
				return false;
			}

			// "r+" Opens a file for update;
			// allows reading and writing. File must exist.
			fopenFlagsOut[i++] = 'r';
			fopenFlagsOut[i++] = '+';
			fopenFlagsOut[i++] = isTextFile ? 't' : 'b';
			fopenFlagsOut[i]   = '\0';
			return true;
		}

		// Can't overwriting existing file, fail.
		if (alreadyExists && (flags & File::Flag::NoOverwrite))
		{
			makeError(errorInfo, "Mode RW: File already exists and \'NoOverwrite\' was specified!");
			return false;
		}

		if (flags & File::Flag::Truncate)
		{
			// Create an empty file for both reading and writing.
			// If a file with the same name already exists its contents
			// are erased and the file is treated as a new empty file.
			fopenFlagsOut[i++] = 'w';
			fopenFlagsOut[i++] = '+';
			fopenFlagsOut[i++] = isTextFile ? 't' : 'b';
			fopenFlagsOut[i]   = '\0';
			return true;
		}

		if (flags & File::Flag::Append)
		{
			// Open a file for reading and appending. All writing operations are performed
			// at the end of the file, protecting the previous content from being overwritten.
			// You can reposition the internal pointer to anywhere in the file for reading, but writing
			// operations will move it back to the end of file. The file is created if it does not exist.
			fopenFlagsOut[i++] = 'a';
			fopenFlagsOut[i++] = '+';
			fopenFlagsOut[i++] = isTextFile ? 't' : 'b';
			fopenFlagsOut[i]   = '\0';
			return true;
		}
	}

	if (flags & File::Flag::Readable) // Read only:
	{
		ALWAYS_CHECK(!(flags & File::Flag::Truncate) && "\'File.Truncate\' is incompatible with read-only files!");
		ALWAYS_CHECK(!(flags & File::Flag::Append)   && "\'File.Append\' is incompatible with read-only files!");

		// File not there, fail.
		if (!alreadyExists)
		{
			makeError(errorInfo, "Mode Read: File not found!");
			return false;
		}

		// Open file for reading. The file must exist.
		fopenFlagsOut[i++] = 'r';
		fopenFlagsOut[i++] = isTextFile ? 't' : 'b';
		fopenFlagsOut[i]   = '\0';
		return true;
	}

	if (flags & File::Flag::Writable) // Write only:
	{
		if (alreadyExists && (flags & File::Flag::NoOverwrite))
		{
			makeError(errorInfo, "Mode Write: File already exists and \'NoOverwrite\' was specified!");
			return false;
		}
		if (!alreadyExists && (flags & File::Flag::NoCreate))
		{
			makeError(errorInfo, "Mode Write: File already exists and \'NoCreate\' was specified!");
			return false;
		}

		if (flags & File::Flag::Append)
		{
			// Append to a file. Writing operations append data at the end.
			// The file is created if it does not exist.
			fopenFlagsOut[i++] = 'a';
			fopenFlagsOut[i++] = isTextFile ? 't' : 'b';
			fopenFlagsOut[i]   = '\0';
			return true;
		}

		// Create an empty file for writing. If a file with the same name
		// already exists its content is erased and the file is treated as a new empty file.
		fopenFlagsOut[i++] = 'w';
		fopenFlagsOut[i++] = isTextFile ? 't' : 'b';
		fopenFlagsOut[i]   = '\0';
		return true;
	}

	makeError(errorInfo, "Files must specify \'File::Flag::Readable\', \'File::Flag::Writable\' or both!");
	return false;
}

} // namespace {}

// ================================================================================================
// File class implementation:
// ================================================================================================

// ========================================================
// File::File():
// ========================================================

File::File()
	: fileHandle(nullptr)
	, openingFlags(0)
{
	clearPodObject(fileStats);
}

// ========================================================
// File::~File():
// ========================================================

File::~File()
{
	close();
}

// ========================================================
// File::open():
// ========================================================

bool File::open(const String & filePath, const uint flags, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(!filePath.isEmpty());

	//
	// If creating a new file, path must already exist.
	// Current file is only closed at the end, upon successful completion.
	//

	char fopenFlags[32];
	const bool isTextFile = (flags & File::Flag::Text); // Otherwise assume binary.
	const bool alreadyExists = FileSystem::pathExistAndIsFile(filePath);

	if (!canOpenFileWithFlags(flags, isTextFile, alreadyExists, fopenFlags, errorInfo))
	{
		return false;
	}

	errno = 0;
	FILE * newFileHandle = std::fopen(filePath.getCStr(), fopenFlags);
	if (newFileHandle == nullptr)
	{
		makeError(errorInfo, String("\'fopen()\' failed: ") + std::strerror(errno), errno);
		return false;
	}

	// User might request an unbuffered file.
	if (flags & Flag::Unbuffered)
	{
		if (std::setvbuf(newFileHandle, nullptr, _IONBF, 0) != 0)
		{
			makeError(errorInfo, String("\'setvbuf()\' failed: ") + std::strerror(errno), errno);
			std::fclose(newFileHandle);
			return false;
		}
	}

	Stats newFileStats;
	if (!FileSystem::fileStatsForPath(filePath, newFileStats, errorInfo))
	{
		std::fclose(newFileHandle);
		return false;
	}

	// Now it is safe to close the current file, if any.
	close();

	// The new state is only "committed" if everything succeeds.
	fileHandle   = newFileHandle;
	fileStats    = newFileStats;
	openingFlags = flags;
	return true;
}

// ========================================================
// File::seek():
// ========================================================

bool File::seek(const long offset, const SeekWay::Enum origin, ErrorInfo * errorInfo)
{
	if (!isOpen())
	{
		makeError(errorInfo, "File not open!");
		return false;
	}

	int fseekDir;
	switch (origin)
	{
	case SeekWay::Beginning : fseekDir = SEEK_SET; break;
	case SeekWay::Current   : fseekDir = SEEK_CUR; break;
	case SeekWay::End       : fseekDir = SEEK_END; break;
	default : UNREACHABLE();
	} // switch (origin)

	errno = 0;
	// fseek() returns zero on success. So much for consistency, C library...
	const int result = std::fseek(asFILE(fileHandle), offset, fseekDir);
	if (result != 0)
	{
		makeError(errorInfo, String("\'fseek()\' failed: ") + std::strerror(errno), errno);
		return false;
	}

	return true;
}

// ========================================================
// File::getPosition():
// ========================================================

bool File::getPosition(ulong & pos, ErrorInfo * errorInfo)
{
	if (!isOpen())
	{
		makeError(errorInfo, "File not open!");
		return false;
	}

	errno = 0;
	// -1 on error, file position otherwise.
	const long result = std::ftell(asFILE(fileHandle));
	if (result == -1)
	{
		makeError(errorInfo, String("\'ftell()\' failed: ") + std::strerror(errno), errno);
		return false;
	}

	pos = result;
	return true;
}

// ========================================================
// File::close():
// ========================================================

bool File::close()
{
	if (!isOpen())
	{
		return false;
	}

	// Successful or not, after fclose() the file handle
	// is no longer valid, so always set it to null.
	const int result = std::fclose(asFILE(fileHandle));
	fileHandle = nullptr;

	// Reset internal states:
	clearPodObject(fileStats);
	openingFlags = 0;

	// 0 on success, EOF otherwise.
	return result == 0;
}

// ========================================================
// File::rewind():
// ========================================================

bool File::rewind()
{
	if (!isOpen())
	{
		return false;
	}
	std::rewind(asFILE(fileHandle));
	return true;
}

// ========================================================
// File::clearErrorState():
// ========================================================

bool File::clearErrorState()
{
	if (!isOpen())
	{
		return false;
	}
	std::clearerr(asFILE(fileHandle));
	return true;
}

// ========================================================
// File::isOpen():
// ========================================================

bool File::isOpen() const
{
	return fileHandle != nullptr;
}

// ========================================================
// File::isAtEndOfFile():
// ========================================================

bool File::isAtEndOfFile() const
{
	if (!isOpen())
	{
		return true;
	}
	return std::feof(asFILE(fileHandle)) != 0;
}

// ========================================================
// File::isInErrorState():
// ========================================================

bool File::isInErrorState() const
{
	if (!isOpen())
	{
		return true;
	}
	return std::ferror(asFILE(fileHandle)) != 0;
}

// ========================================================
// File::isReadable():
// ========================================================

bool File::isReadable() const
{
	return openingFlags & Flag::Readable;
}

// ========================================================
// File::isWritable():
// ========================================================

bool File::isWritable() const
{
	return openingFlags & Flag::Writable;
}

// ========================================================
// File::isReadWrite():
// ========================================================

bool File::isReadWrite() const
{
	return (openingFlags & Flag::Readable) &&
	       (openingFlags & Flag::Writable);
}

// ========================================================
// File::isText():
// ========================================================

bool File::isText() const
{
	return openingFlags & Flag::Text;
}

// ========================================================
// File::isBinary():
// ========================================================

bool File::isBinary() const
{
	return openingFlags & Flag::Binary;
}

// ========================================================
// File::getOpeningFlags():
// ========================================================

uint File::getOpeningFlags() const
{
	return openingFlags;
}

// ========================================================
// File::getSizeInBytes():
// ========================================================

ulong File::getSizeInBytes() const
{
	return fileStats.sizeInBytes;
}

// ========================================================
// File::getCreationTime():
// ========================================================

File::Timestamp File::getCreationTime() const
{
	return fileStats.creationTime;
}

// ========================================================
// File::getLastAccessTime():
// ========================================================

File::Timestamp File::getLastAccessTime() const
{
	return fileStats.lastAccessTime;
}

// ========================================================
// File::getLastModificationTime():
// ========================================================

File::Timestamp File::getLastModificationTime() const
{
	return fileStats.lastModificationTime;
}

// ========================================================
// File::getFileStats():
// ========================================================

const File::Stats & File::getFileStats() const
{
	return fileStats;
}

// ========================================================
// File::timestampString():
// ========================================================

String File::timestampString(const Timestamp timestamp)
{
	String timeStr;

	if (timestamp != 0)
	{
		timeStr = std::ctime(&timestamp);
		timeStr.popBack(); // Remove the default new line added by `ctime()`.
	}
	else
	{
		timeStr = "(null)";
	}

	return timeStr;
}

// ================================================================================================
// ReadableFile class implementation:
// ================================================================================================

// ========================================================
// ReadableFile::ReadableFile():
// ========================================================

ReadableFile::ReadableFile()
	: File()
{
}

// ========================================================
// ReadableFile::ReadableFile():
// ========================================================

ReadableFile::ReadableFile(const String & filePath, const uint flags, ErrorInfo * errorInfo)
	: File()
{
	DEBUG_CHECK((flags & Flag::Readable) && "You must specify \'Readable\' flag for \'ReadableFile\'!");
	open(filePath, flags, errorInfo);
}

// ========================================================
// ReadableFile::~ReadableFile():
// ========================================================

ReadableFile::~ReadableFile()
{
	// Empty.
}

// ========================================================
// ReadableFile::readBool():
// ========================================================

bool ReadableFile::readBool(bool & value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");

	// Make sure we only read/write a single byte for a boolean.
	// On some platforms they are the size of an integer.
	uint8 boolVal;
	if (readUint8(boolVal))
	{
		value = boolVal ? true : false;
		return true;
	}

	return (value = false);
}

// ========================================================
// ReadableFile::readInt8():
// ========================================================

bool ReadableFile::readInt8(int8 & value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");

	if (readBytes(&value, sizeof(value)) == sizeof(value))
	{
		return true;
	}
	value = 0;
	return false;
}

// ========================================================
// ReadableFile::readUint8():
// ========================================================

bool ReadableFile::readUint8(uint8 & value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");

	if (readBytes(&value, sizeof(value)) == sizeof(value))
	{
		return true;
	}
	value = 0;
	return false;
}

// ========================================================
// ReadableFile::readInt16():
// ========================================================

bool ReadableFile::readInt16(int16 & value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");

	if (readBytes(&value, sizeof(value)) == sizeof(value))
	{
		return true;
	}
	value = 0;
	return false;
}

// ========================================================
// ReadableFile::readUint16():
// ========================================================

bool ReadableFile::readUint16(uint16 & value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");

	if (readBytes(&value, sizeof(value)) == sizeof(value))
	{
		return true;
	}
	value = 0;
	return false;
}

// ========================================================
// ReadableFile::readInt32():
// ========================================================

bool ReadableFile::readInt32(int32 & value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");

	if (readBytes(&value, sizeof(value)) == sizeof(value))
	{
		return true;
	}
	value = 0;
	return false;
}

// ========================================================
// ReadableFile::readUint32():
// ========================================================

bool ReadableFile::readUint32(uint32 & value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");

	if (readBytes(&value, sizeof(value)) == sizeof(value))
	{
		return true;
	}
	value = 0;
	return false;
}

// ========================================================
// ReadableFile::readInt64():
// ========================================================

bool ReadableFile::readInt64(int64 & value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");

	if (readBytes(&value, sizeof(value)) == sizeof(value))
	{
		return true;
	}
	value = 0;
	return false;
}

// ========================================================
// ReadableFile::readUint64():
// ========================================================

bool ReadableFile::readUint64(uint64 & value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");

	if (readBytes(&value, sizeof(value)) == sizeof(value))
	{
		return true;
	}
	value = 0;
	return false;
}

// ========================================================
// ReadableFile::readFloat():
// ========================================================

bool ReadableFile::readFloat(float & value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");

	if (readBytes(&value, sizeof(value)) == sizeof(value))
	{
		return true;
	}
	value = 0.0f;
	return false;
}

// ========================================================
// ReadableFile::readDouble():
// ========================================================

bool ReadableFile::readDouble(double & value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");

	if (readBytes(&value, sizeof(value)) == sizeof(value))
	{
		return true;
	}
	value = 0.0;
	return false;
}

// ========================================================
// ReadableFile::readBytes():
// ========================================================

uint ReadableFile::readBytes(void * destBuffer, const uint bytesToRead)
{
	DEBUG_CHECK(destBuffer  != nullptr);
	DEBUG_CHECK(bytesToRead != 0);

	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");
	DEBUG_CHECK(isReadable() && "File not open for reading!");

	if (!isOpen())
	{
		return 0;
	}
	return static_cast<uint>(std::fread(destBuffer, 1, bytesToRead, asFILE(fileHandle)));
}

// ========================================================
// ReadableFile::readChar():
// ========================================================

bool ReadableFile::readChar(char & value)
{
	DEBUG_CHECK(isText() && "This method is only available for text files!");
	DEBUG_CHECK(isReadable() && "File not open for reading!");

	if (!isOpen())
	{
		return false;
	}

	const int result = std::fgetc(asFILE(fileHandle));
	if (result == EOF)
	{
		value = '\0';
		return false;
	}

	value = static_cast<char>(result);
	return true;
}

// ========================================================
// ReadableFile::readLine():
// ========================================================

bool ReadableFile::readLine(String & line)
{
	DEBUG_CHECK(isText() && "This method is only available for text files!");
	DEBUG_CHECK(isReadable() && "File not open for reading!");

	// This limits the length of a line but avoids additional
	// memory allocations. Should be fine for most use cases.
	char tempBuffer[4096];

	if (!readLine(tempBuffer, arrayLength(tempBuffer)))
	{
		return false;
	}

	line = tempBuffer;
	return true;
}

// ========================================================
// ReadableFile::readLine():
// ========================================================

bool ReadableFile::readLine(char * line, const uint maxCharsToRead)
{
	DEBUG_CHECK(line != nullptr);
	DEBUG_CHECK(maxCharsToRead != 0);

	DEBUG_CHECK(isText() && "This method is only available for text files!");
	DEBUG_CHECK(isReadable() && "File not open for reading!");

	if (!isOpen())
	{
		return false;
	}
	return std::fgets(line, maxCharsToRead, asFILE(fileHandle)) != nullptr;
}

// ========================================================
// ReadableFile::scanF():
// ========================================================

int ReadableFile::scanF(const char * format, ...)
{
	DEBUG_CHECK(format != nullptr);
	DEBUG_CHECK(isText() && "This method is only available for text files!");
	DEBUG_CHECK(isReadable() && "File not open for reading!");

	if (!isOpen())
	{
		return 0;
	}

	va_list vaList;
	va_start(vaList, format);
	const int result = std::vfscanf(asFILE(fileHandle), format, vaList);
	va_end(vaList);

	return result;
}

// ================================================================================================
// WritableFile class implementation:
// ================================================================================================

// ========================================================
// WritableFile::WritableFile():
// ========================================================

WritableFile::WritableFile()
	: File()
{
}

// ========================================================
// WritableFile::WritableFile():
// ========================================================

WritableFile::WritableFile(const String & filePath, const uint flags, ErrorInfo * errorInfo)
	: File()
{
	DEBUG_CHECK((flags & Flag::Writable) && "You must specify \'Writable\' flag for \'WritableFile\'!");
	open(filePath, flags, errorInfo);
}

// ========================================================
// WritableFile::~WritableFile():
// ========================================================

WritableFile::~WritableFile()
{
	// Empty.
}

// ========================================================
// WritableFile::writeBool():
// ========================================================

bool WritableFile::writeBool(const bool value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");

	// Make sure we only read/write a single byte for a boolean.
	// On some platforms they are the size of an integer.
	const uint8 boolVal = value ? 1 : 0;
	return writeUint8(boolVal);
}

// ========================================================
// WritableFile::writeInt8():
// ========================================================

bool WritableFile::writeInt8(const int8 value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");
	return writeBytes(&value, sizeof(value)) == sizeof(value);
}

// ========================================================
// WritableFile::writeUint8():
// ========================================================

bool WritableFile::writeUint8(const uint8 value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");
	return writeBytes(&value, sizeof(value)) == sizeof(value);
}

// ========================================================
// WritableFile::writeInt16():
// ========================================================

bool WritableFile::writeInt16(const int16 value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");
	return writeBytes(&value, sizeof(value)) == sizeof(value);
}

// ========================================================
// WritableFile::writeUint16():
// ========================================================

bool WritableFile::writeUint16(const uint16 value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");
	return writeBytes(&value, sizeof(value)) == sizeof(value);
}

// ========================================================
// WritableFile::writeInt32():
// ========================================================

bool WritableFile::writeInt32(const int32 value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");
	return writeBytes(&value, sizeof(value)) == sizeof(value);
}

// ========================================================
// WritableFile::writeUint32():
// ========================================================

bool WritableFile::writeUint32(const uint32 value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");
	return writeBytes(&value, sizeof(value)) == sizeof(value);
}

// ========================================================
// WritableFile::writeInt64():
// ========================================================

bool WritableFile::writeInt64(const int64 value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");
	return writeBytes(&value, sizeof(value)) == sizeof(value);
}

// ========================================================
// WritableFile::writeUint64():
// ========================================================

bool WritableFile::writeUint64(const uint64 value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");
	return writeBytes(&value, sizeof(value)) == sizeof(value);
}

// ========================================================
// WritableFile::writeFloat():
// ========================================================

bool WritableFile::writeFloat(const float value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");
	return writeBytes(&value, sizeof(value)) == sizeof(value);
}

// ========================================================
// WritableFile::writeDouble():
// ========================================================

bool WritableFile::writeDouble(const double value)
{
	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");
	return writeBytes(&value, sizeof(value)) == sizeof(value);
}

// ========================================================
// WritableFile::writeBytes():
// ========================================================

uint WritableFile::writeBytes(const void * srcBuffer, const uint bytesToWrite)
{
	DEBUG_CHECK(srcBuffer != nullptr);
	DEBUG_CHECK(bytesToWrite != 0);

	DEBUG_CHECK(isBinary() && "This method is only available for binary files!");
	DEBUG_CHECK(isWritable() && "File not open for writing!");

	if (!isOpen())
	{
		return 0;
	}

	const uint writeCount = static_cast<uint>(std::fwrite(srcBuffer, 1, bytesToWrite, asFILE(fileHandle)));
	fileStats.sizeInBytes += writeCount;
	return writeCount;
}

// ========================================================
// WritableFile::writeChar():
// ========================================================

bool WritableFile::writeChar(const char value)
{
	DEBUG_CHECK(isText() && "This method is only available for text files!");
	DEBUG_CHECK(isWritable() && "File not open for writing!");

	if (!isOpen())
	{
		return false;
	}

	if (std::fputc(value, asFILE(fileHandle)) == EOF)
	{
		return false;
	}

	fileStats.sizeInBytes += sizeof(char);
	return true;
}

// ========================================================
// WritableFile::writeString():
// ========================================================

bool WritableFile::writeString(const String & str)
{
	DEBUG_CHECK(isText() && "This method is only available for text files!");
	DEBUG_CHECK(isWritable() && "File not open for writing!");

	if (!isOpen() || str.isEmpty())
	{
		return false;
	}

	if (std::fputs(str.getCStr(), asFILE(fileHandle)) == EOF)
	{
		return false;
	}

	fileStats.sizeInBytes += str.getLength();
	return true;
}

// ========================================================
// WritableFile::writeString():
// ========================================================

bool WritableFile::writeString(const char * cstr)
{
	DEBUG_CHECK(cstr != nullptr);
	DEBUG_CHECK(isText() && "This method is only available for text files!");
	DEBUG_CHECK(isWritable() && "File not open for writing!");

	if (!isOpen() || *cstr == '\0')
	{
		return false;
	}

	if (std::fputs(cstr, asFILE(fileHandle)) == EOF)
	{
		return false;
	}

	fileStats.sizeInBytes += strLength(cstr);
	return true;
}

// ========================================================
// WritableFile::printF():
// ========================================================

int WritableFile::printF(const char * format, ...)
{
	DEBUG_CHECK(format != nullptr);
	DEBUG_CHECK(isText() && "This method is only available for text files!");
	DEBUG_CHECK(isWritable() && "File not open for writing!");

	if (!isOpen())
	{
		return 0;
	}

	va_list vaList;
	va_start(vaList, format);
	const int result = std::vfprintf(asFILE(fileHandle), format, vaList);
	va_end(vaList);

	if (result > 0)
	{
		fileStats.sizeInBytes += result;
	}

	return result;
}

// ========================================================
// WritableFile::vaPrintF():
// ========================================================

int WritableFile::vaPrintF(const char * format, va_list vaList)
{
	DEBUG_CHECK(format != nullptr);
	DEBUG_CHECK(isText() && "This method is only available for text files!");
	DEBUG_CHECK(isWritable() && "File not open for writing!");

	if (!isOpen())
	{
		return 0;
	}

	const int result = std::vfprintf(asFILE(fileHandle), format, vaList);
	if (result > 0)
	{
		fileStats.sizeInBytes += result;
	}

	return result;
}

// ========================================================
// WritableFile::flush():
// ========================================================

bool WritableFile::flush()
{
	DEBUG_CHECK(isWritable() && "File not open for writing!");

	if (!isOpen())
	{
		return false;
	}
	return std::fflush(asFILE(fileHandle)) != EOF;
}

// ================================================================================================
// ReadWriteFile class implementation:
// ================================================================================================

// ========================================================
// ReadWriteFile::ReadWriteFile():
// ========================================================

ReadWriteFile::ReadWriteFile()
	: File()
{
}

// ========================================================
// ReadWriteFile::ReadWriteFile():
// ========================================================

ReadWriteFile::ReadWriteFile(const String & filePath, const uint flags, ErrorInfo * errorInfo)
	: File()
{
	DEBUG_CHECK((flags & Flag::Readable) && "You must specify \'Readable\' flag for \'ReadWriteFile\'!");
	DEBUG_CHECK((flags & Flag::Writable) && "You must specify \'Writable\' flag for \'ReadWriteFile\'!");
	open(filePath, flags, errorInfo);
}

// ========================================================
// ReadWriteFile::~ReadWriteFile():
// ========================================================

ReadWriteFile::~ReadWriteFile()
{
	// Empty.
}

// ================================================================================================
// FileSystem class implementation [the common parts; more in the platform-specific files]:
// ================================================================================================

// ========================================================
// FileSystem::pathExist():
// ========================================================

bool FileSystem::pathExist(const String & path)
{
	File::Stats fileStats;
	if (!fileStatsForPath(path, fileStats))
	{
		return false; // Path doesn't exist.
	}
	return true; // Path is there.
}

// ========================================================
// FileSystem::pathExistAndIsFile():
// ========================================================

bool FileSystem::pathExistAndIsFile(const String & path)
{
	File::Stats fileStats;
	if (!fileStatsForPath(path, fileStats))
	{
		return false; // Path doesn't exist.
	}
	return fileStats.isNormalFile && !fileStats.isDirectory;
}

// ========================================================
// FileSystem::pathExistAndIsDirectory():
// ========================================================

bool FileSystem::pathExistAndIsDirectory(const String & path)
{
	File::Stats fileStats;
	if (!fileStatsForPath(path, fileStats))
	{
		return false; // Path doesn't exist.
	}
	return fileStats.isDirectory && !fileStats.isNormalFile;
}

// ========================================================
// FileSystem::queryFileSize():
// ========================================================

ulong FileSystem::queryFileSize(const String & filePath, ErrorInfo * errorInfo)
{
	File::Stats fileStats;
	if (!fileStatsForPath(filePath, fileStats, errorInfo))
	{
		return 0; // Path doesn't exist. `errorInfo` will carry additional info.
	}
	return fileStats.sizeInBytes;
}

// ========================================================
// FileSystem::makePath():
// ========================================================

bool FileSystem::makePath(const String & pathEndedWithSeparatorOrFilename, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(!pathEndedWithSeparatorOrFilename.isEmpty());
	DEBUG_CHECK(pathEndedWithSeparatorOrFilename.getLength() < MaxPathLen && "Pathname too long!");

	const char pathSepChar = getPathSeparatorChar();
	PathString dirPath = pathEndedWithSeparatorOrFilename;

	uint index = 0;
	while (index != dirPath.getLength())
	{
		if (dirPath[index] == pathSepChar)
		{
			dirPath[index] = '\0';
			if (!makeSingleDirectory(dirPath, errorInfo))
			{
				return false;
			}
			dirPath[index] = pathSepChar;
		}
		++index;
	}

	return true;
}

// ========================================================
// FileSystem::loadFile():
// ========================================================

bool FileSystem::loadFile(const String & filePath, ByteArray & fileContents, ErrorInfo * errorInfo)
{
	ReadableFile inFile;
	if (!inFile.open(filePath, File::Flag::Readable | File::Flag::Binary | File::Flag::Unbuffered, errorInfo))
	{
		return false;
	}

	if (inFile.getSizeInBytes() >= uintMax)
	{
		makeError(errorInfo, "File \"" + filePath + "\" is too big to be loaded into memory!");
		return false;
	}

	const uint byteCount = static_cast<uint>(inFile.getSizeInBytes());
	if (byteCount == 0)
	{
		fileContents.clear();
		return true; // Empty file!
	}

	fileContents.allocateExact(byteCount);
	fileContents.resize(byteCount);

	DEBUG_CHECK(fileContents.getSize() == byteCount);

	// Read in the data:
	const uint bytesRead = inFile.readBytes(fileContents.getData(), byteCount);
	if (bytesRead < byteCount)
	{
		const uint missingBytes = byteCount - bytesRead;
		makeError(errorInfo, "Failed to read " + toString(missingBytes) + " bytes from file \"" + filePath + "\"!");
		return false;
	}

	return true;
}

// ========================================================
// FileSystem::writeFile():
// ========================================================

bool FileSystem::writeFile(const String & filePath, const ByteArray & fileContents, ErrorInfo * errorInfo)
{
	WritableFile outFile;
	if (!outFile.open(filePath, File::Flag::Writable | File::Flag::Binary | File::Flag::Unbuffered, errorInfo))
	{
		return false;
	}

	// Would create an empty file, which we don't assume to be an error.
	if (fileContents.isEmpty())
	{
		return true;
	}

	// Write the data:
	const uint byteCount = fileContents.getSize();
	const uint bytesWritten = outFile.writeBytes(fileContents.getData(), byteCount);

	if (bytesWritten < byteCount)
	{
		const uint missingBytes = byteCount - bytesWritten;
		makeError(errorInfo, "Failed to write " + toString(missingBytes) + " bytes to file \"" + filePath + "\"!");
		return false;
	}

	return true;
}

// ========================================================
// FileSystem::loadTextFile():
// ========================================================

bool FileSystem::loadTextFile(const String & filePath, char *& fileContents, uint & fileLength, ErrorInfo * errorInfo)
{
	// Note: Still open as Binary, so we can readBytes().
	// This should make no difference, since a '\0' is
	// manually appended to the file contents.
	ReadableFile inFile;
	if (!inFile.open(filePath, File::Flag::Readable | File::Flag::Binary | File::Flag::Unbuffered, errorInfo))
	{
		fileContents = nullptr;
		fileLength   = 0;
		return false;
	}

	if (inFile.getSizeInBytes() >= uintMax)
	{
		makeError(errorInfo, "File \"" + filePath + "\" is too big to be loaded into memory!");

		fileContents = nullptr;
		fileLength   = 0;
		return false;
	}

	const uint byteCount = static_cast<uint>(inFile.getSizeInBytes());
	if (byteCount == 0)
	{
		// Empty file. Return an empty string (must be delete[]-able!).
		fileContents    = new char[1];
		fileContents[0] = '\0';
		fileLength      = 0;
		return true;
	}

	// +1 for the null terminator.
	fileContents = new char[byteCount + 1];

	// Read in the data:
	const uint bytesRead = inFile.readBytes(fileContents, byteCount);
	if (bytesRead < byteCount)
	{
		const uint missingBytes = byteCount - bytesRead;
		makeError(errorInfo, "Failed to read " + toString(missingBytes) + " bytes from file \"" + filePath + "\"!");

		fileLength = bytesRead;
		fileContents[fileLength] = '\0';
		return false;
	}

	fileLength = byteCount;
	fileContents[fileLength] = '\0';
	return true;
}

} // namespace core {}
} // namespace atlas {}
