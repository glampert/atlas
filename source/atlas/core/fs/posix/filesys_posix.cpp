
// ================================================================================================
// -*- C++ -*-
// File: filesys_posix.cpp
// Author: Guilherme R. Lampert
// Created on: 19/06/15
// Brief: Portions of FileSystem that rely on POSIX functionality.
// ================================================================================================

#include "atlas/core/fs/file_handling.hpp"
#include "atlas/core/memory/mem_utils.hpp"

#include <errno.h>     // `errno` and error codes
#include <unistd.h>    // Unix directory/FS functions
#include <sys/types.h> // System types
#include <sys/stat.h>  // stat()/fstat()

namespace atlas
{
namespace core
{

// ========================================================
// FileSystem::fileStatsForPath():
// ========================================================

bool FileSystem::fileStatsForPath(const String & path, File::Stats & statsOut, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(!path.isEmpty());
	clearPodObject(statsOut);

	errno = 0;
	struct stat statBuf;
	const int result = stat(path.getCStr(), &statBuf);

	// Successful.
	if (result == 0)
	{
		statsOut.creationTime         = statBuf.st_ctime;
		statsOut.lastAccessTime       = statBuf.st_atime;
		statsOut.lastModificationTime = statBuf.st_mtime;
		statsOut.sizeInBytes          = static_cast<ulong>(statBuf.st_size);
		statsOut.isDirectory          = S_ISDIR(statBuf.st_mode) ? true : false;
		statsOut.isNormalFile         = S_ISREG(statBuf.st_mode) ? true : false;
		return true;
	}

	// stat() failed. Return error string if object was provided.
	makeError(errorInfo, String("\'stat()\' failed: ") + std::strerror(errno), errno);
	return false;
}

// ========================================================
// FileSystem::getCurrentDirectory():
// ========================================================

FileSystem::PathString FileSystem::getCurrentDirectory()
{
	char tempPath[MaxPathLen];

	if (getcwd(tempPath, MaxPathLen))
	{
		tempPath[MaxPathLen - 1] = '\0';
	}
	else
	{
		tempPath[0] = '\0';
	}

	return tempPath;
}

// ========================================================
// FileSystem::setCurrentDirectory():
// ========================================================

bool FileSystem::setCurrentDirectory(const String & dirPath)
{
	DEBUG_CHECK(!dirPath.isEmpty());
	return chdir(dirPath.getCStr()) == 0;
}

// ========================================================
// FileSystem::makeSingleDirectory():
// ========================================================

bool FileSystem::makeSingleDirectory(const String & dirPath, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(!dirPath.isEmpty());

	errno = 0;
	struct stat dirStat;

	if (stat(dirPath.getCStr(), &dirStat) != 0)
	{
		if (mkdir(dirPath.getCStr(), 0777) != 0)
		{
			// Impossible to create the directory.
			// Probably the user doesn't have the permissions.
			makeError(errorInfo, String("\'mkdir(0777)\' failed: ") + std::strerror(errno), errno);
			return false;
		}
	}
	else // Path already exists:
	{
		if (!S_ISDIR(dirStat.st_mode))
		{
			// Looks like there is a file with the same name as
			// the directory we are attempting to create. This is an error.
			makeError(errorInfo, "Path \"" + dirPath + "\" appears to be a file!", errno);
			return false;
		}
	}

	return true;
}

// ========================================================
// FileSystem::removeFile():
// ========================================================

bool FileSystem::removeFile(const String & filePath, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(!filePath.isEmpty());

	if (unlink(filePath.getCStr()) != 0)
	{
		makeError(errorInfo, String("\'unlink()\' failed: ") + std::strerror(errno), errno);
		return false;
	}

	return true;
}

} // namespace core {}
} // namespace atlas {}
