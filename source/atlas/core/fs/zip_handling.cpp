
// ================================================================================================
// -*- C++ -*-
// File: zip_handling.cpp
// Author: Guilherme R. Lampert
// Created on: 23/06/15
// Brief: Zip archive handling and IO plus some data compression/decompression helpers.
// ================================================================================================

#include "atlas/core/fs/zip_handling.hpp"
#include "atlas/core/fs/path_utils.hpp"
#include "atlas/core/memory/mem_utils.hpp"

// ========================================================
// The header-only mini_z library (only included here):
// ========================================================

#define MZ_ASSERT(x)           DEBUG_CHECK(x)
#define MZ_MALLOC(count)       atlas::core::allocate<atlas::core::ubyte>(count)
#define MZ_REALLOC(ptr, count) atlas::core::reallocate<atlas::core::ubyte>((atlas::core::ubyte *)(ptr), (count))
#define MZ_FREE(ptr)           atlas::core::deallocate(ptr)

#define MINIZ_NO_ZLIB_COMPATIBLE_NAMES 1
#include "thirdparty/mini_z/mini_z.h"

// ========================================================

namespace atlas
{
namespace core
{

//
// Supply a reasonably large prime number as a default.
//
// This can be tuned according to the application to
// save memory or speed up file lookup on large zips.
// The closer this value is to the number of file entries
// in the zip, the faster file lookup will be. Best if a prime.
//
#ifndef ZIP_ARCHIVE_FILE_TABLE_BUCKETS
	#define ZIP_ARCHIVE_FILE_TABLE_BUCKETS 2503
#endif // ZIP_ARCHIVE_FILE_TABLE_BUCKETS

// ========================================================
// asMzArchive() [LOCAL]:
// ========================================================

inline mz_zip_archive * asMzArchive(ZipArchive::Handle h)
{
	return reinterpret_cast<mz_zip_archive *>(h);
}

// ================================================================================================
// ZipArchive class implementation:
// ================================================================================================

// ========================================================
// ZipArchive::ZipArchive():
// ========================================================

ZipArchive::ZipArchive()
	: zipHandle(nullptr)
	, zipFlags(0)
	, zipFileTable(true, ZIP_ARCHIVE_FILE_TABLE_BUCKETS)
{
	clearPodObject(zipStats);
}

// ========================================================
// ZipArchive::~ZipArchive():
// ========================================================

ZipArchive::~ZipArchive()
{
	//
	// NOTE:
	// Child classes are responsible for closing
	// the mini-z file handle and cleaning up any
	// additional allocated resources. Calling `close()`
	// here is not possible since it is a pure-virtual method.
	//
}

// ========================================================
// ZipArchive::commonCleanup():
// ========================================================

void ZipArchive::commonCleanup()
{
	mz_zip_archive * pZip = asMzArchive(zipHandle);
	delete pZip;

	zipHandle = nullptr;
	zipFlags  = 0;

	zipFileTable.clear();
	zipFileEntries.clear();

	clearPodObject(zipStats);
}

// ========================================================
// ZipArchive::addFileEntry():
// ========================================================

void ZipArchive::addFileEntry(const FileEntry & newEntry)
{
	zipFileEntries.pushBack(newEntry);
	zipFileTable.insert(zipFileEntries.back().filename, &zipFileEntries.back());
}

// ========================================================
// ZipArchive::isOpen():
// ========================================================

bool ZipArchive::isOpen() const
{
	return zipHandle != nullptr;
}

// ========================================================
// ZipArchive::getFileEntryCount():
// ========================================================

uint ZipArchive::getFileEntryCount() const
{
	DEBUG_CHECK(zipFileEntries.getSize() == zipFileTable.getSize());
	return zipFileEntries.getSize();
}

// ========================================================
// ZipArchive::getEntryByIndex():
// ========================================================

const ZipArchive::FileEntry * ZipArchive::getEntryByIndex(const uint index) const
{
	if (!isOpen() || index >= zipFileEntries.getSize())
	{
		return nullptr;
	}
	return &zipFileEntries[index];
}

// ========================================================
// ZipArchive::getEntryByName():
// ========================================================

const ZipArchive::FileEntry * ZipArchive::getEntryByName(const String & filename) const
{
	if (!isOpen())
	{
		return nullptr;
	}
	return zipFileTable.find(filename); // Null if not found.
}

// ========================================================
// ZipArchive::hasEntryAndIsFile():
// ========================================================

bool ZipArchive::hasEntryAndIsFile(const String & filename) const
{
	const FileEntry * entry = getEntryByName(filename);
	if (entry == nullptr || entry->isDirectory)
	{
		return false;
	}
	return true;
}

// ========================================================
// ZipArchive::getZipSizeInBytes():
// ========================================================

ulong ZipArchive::getZipSizeInBytes() const
{
	return zipStats.sizeInBytes;
}

// ========================================================
// ZipArchive::getZipOpeningFlags():
// ========================================================

uint ZipArchive::getZipOpeningFlags() const
{
	return zipFlags;
}

// ========================================================
// ZipArchive::getZipCreationTime():
// ========================================================

File::Timestamp ZipArchive::getZipCreationTime() const
{
	return zipStats.creationTime;
}

// ========================================================
// ZipArchive::getZipLastAccessTime():
// ========================================================

File::Timestamp ZipArchive::getZipLastAccessTime() const
{
	return zipStats.lastAccessTime;
}

// ========================================================
// ZipArchive::getZipLastModificationTime():
// ========================================================

File::Timestamp ZipArchive::getZipLastModificationTime() const
{
	return zipStats.lastModificationTime;
}

// ========================================================
// ZipArchive::getZipFileStats():
// ========================================================

const File::Stats & ZipArchive::getZipFileStats() const
{
	return zipStats;
}

// ========================================================
// ZipArchive::getAllFileEntries():
// ========================================================

const Array<ZipArchive::FileEntry> & ZipArchive::getAllFileEntries() const
{
	return zipFileEntries;
}

// ========================================================
// ZipArchive::getFileEntryTable():
// ========================================================

const IntrusiveHashTable<String, ZipArchive::FileEntry> & ZipArchive::getFileEntryTable() const
{
	return zipFileTable;
}

// ========================================================
// ZipArchive::FileEntry::setFilename():
// ========================================================

void ZipArchive::FileEntry::setFilename(const char * name, const uint flags)
{
	if (name == nullptr || *name == '\0')
	{
		filename.clear();
		return;
	}

	filename.assign(name);

	// Ensure terminated with a path separator if this is a directory name.
	if (isDirectory && filename.back() != getPathSeparatorChar())
	{
		filename.pushBack(getPathSeparatorChar());
	}

	if ((flags & ZipArchive::Flag::IgnoreDirPath) && !isDirectory)
	{
		//
		// Save only the filename and extension.
		// This might cause files to hash to the same table
		// bucket. In such cases, the first occurrence found
		// is returned when searching for a file. Use this
		// option if you know for sure the archive doesn't
		// store files with colliding names. It will use less
		// memory and the lookup should also be slightly faster.
		//
		// File entries representing directories are
		// stored unchanged, regardless of this flag.
		//
		filename = stripPath(filename);
	}

	if (flags & ZipArchive::Flag::CaseInsensitive)
	{
		// Save name as a lowercase string so that CamelCase names still compare equal.
		filename.toLower();
	}
}

// ================================================================================================
// ZipReader class implementation:
// ================================================================================================

// ========================================================
// ZipReader::ZipReader():
// ========================================================

ZipReader::ZipReader()
	: ZipArchive()
{
}

// ========================================================
// ZipReader::ZipReader():
// ========================================================

ZipReader::ZipReader(const String & filePath, const uint flags, ErrorInfo * errorInfo)
	: ZipArchive()
{
	openDefaultReader(filePath, flags, errorInfo);
}

// ========================================================
// ZipReader::~ZipReader():
// ========================================================

ZipReader::~ZipReader()
{
	closeDefaultReader();
}

// ========================================================
// ZipReader::open():
// ========================================================

bool ZipReader::open(const String & filePath, const uint flags, ErrorInfo * errorInfo)
{
	return openDefaultReader(filePath, flags, errorInfo);
}

// ========================================================
// ZipReader::close():
// ========================================================

bool ZipReader::close()
{
	return closeDefaultReader();
}

// ========================================================
// ZipReader::extractFileToMemoryByIndex():
// ========================================================

bool ZipReader::extractFileToMemoryByIndex(const uint index, ByteArray & fileContents, ErrorInfo * errorInfo) const
{
	const FileEntry * entry = getEntryByIndex(index);
	if (entry == nullptr)
	{
		makeError(errorInfo, "Zip file entry #" + toString(index) + " not found!");
		return false;
	}

	return extractFileToMemory(*entry, fileContents, errorInfo);
}

// ========================================================
// ZipReader::extractFileToMemoryByName():
// ========================================================

bool ZipReader::extractFileToMemoryByName(const String & filename, ByteArray & fileContents, ErrorInfo * errorInfo) const
{
	const FileEntry * entry = getEntryByName(filename);
	if (entry == nullptr)
	{
		makeError(errorInfo, "Zip file entry \"" + filename + "\" not found!");
		return false;
	}

	return extractFileToMemory(*entry, fileContents, errorInfo);
}

// ========================================================
// ZipReader::extractFileToMemory():
// ========================================================

bool ZipReader::extractFileToMemory(const FileEntry & fileEntry, ByteArray & fileContents, ErrorInfo * errorInfo) const
{
	if (!isOpen())
	{
		makeError(errorInfo, "No zip archive open!");
		return false;
	}

	if (fileEntry.isDirectory)
	{
		makeError(errorInfo, "Zip file entry \"" + fileEntry.filename + "\" is directory!");
		return false;
	}

	DEBUG_CHECK(hasEntryAndIsFile(fileEntry.filename) && "Zip file entry doesn't belong to this archive!");

	// Ensure allocated. Any existing data will be overwritten.
	//
	fileContents.allocateExact(fileEntry.uncompressedSizeInBytes);
	fileContents.resize(fileEntry.uncompressedSizeInBytes);

	// File extraction:
	//
	if (!mz_zip_reader_extract_to_mem(asMzArchive(zipHandle), fileEntry.index,
		fileContents.getData(), fileContents.getSize(), /* flags = */ 0))
	{
		fileContents.deallocate();
		makeError(errorInfo, "\'mz_zip_reader_extract_to_mem()\' failed!");
		return false;
	}

	return true;
}

// ========================================================
// ZipReader::extractWholeArchiveToFileSystem():
// ========================================================

bool ZipReader::extractWholeArchiveToFileSystem(const String & destBasePath, ErrorInfo * errorInfo) const
{
	if (!isOpen())
	{
		makeError(errorInfo, "No zip archive open!");
		return false;
	}

	String fullFilePath;
	int errorCount = 0;
	mz_zip_archive * pZip = asMzArchive(zipHandle);
	const uint fileCount  = zipFileEntries.getSize();
	const bool needExtraPathSep = (destBasePath.back() != getPathSeparatorChar()) ? true : false;

	for (uint f = 0; f < fileCount; ++f)
	{
		const FileEntry & entry = zipFileEntries[f];
		DEBUG_CHECK(entry.index == f);

		if (needExtraPathSep)
		{
			fullFilePath  = destBasePath;
			fullFilePath += getPathSeparatorChar();
			fullFilePath += entry.filename;
		}
		else
		{
			fullFilePath  = destBasePath;
			fullFilePath += entry.filename;
		}

		if (entry.isDirectory)
		{
			if (!FileSystem::makePath(fullFilePath))
			{
				++errorCount;
				// Attempt to continue.
			}
		}
		else // File:
		{
			if (!mz_zip_reader_extract_to_file(pZip, f, fullFilePath.getCStr(), /* flags = */ 0))
			{
				++errorCount;
				// Attempt to continue.
			}
		}
	}

	if (errorCount != 0)
	{
		makeError(errorInfo, "Failed to extract " + toString(errorCount) + " file entries from zip archive!");
		return false;
	}

	// All files were successfully extracted.
	return true;
}

// ========================================================
// ZipReader::openDefaultReader():
// ========================================================

bool ZipReader::openDefaultReader(const String & filePath, const uint flags, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(!filePath.isEmpty());

	File::Stats newStats;
	if (!FileSystem::fileStatsForPath(filePath, newStats, errorInfo))
	{
		return false;
	}
	if (!newStats.isNormalFile)
	{
		makeError(errorInfo, "File path \"" + filePath + "\" points to a directory!");
		return false;
	}

	mz_zip_archive * newZip = new mz_zip_archive();
	if (!mz_zip_reader_init_file(newZip, filePath.getCStr(), /* flags = */ 0))
	{
		delete newZip;
		makeError(errorInfo, "\'mz_zip_reader_init_file()\' failed!");
		return false;
	}

	// Now that the new archive is open we close any previously
	// open file and commit the new internal states.
	closeDefaultReader();

	zipHandle = newZip;
	zipFlags  = flags; // These are NOT the same flags passed mz_zip_reader_init()!
	zipStats  = newStats;

	return buildFileEntryTable(errorInfo);
}

// ========================================================
// ZipReader::closeDefaultReader():
// ========================================================

bool ZipReader::closeDefaultReader()
{
	if (!isOpen())
	{
		return false;
	}

	const bool result = !!mz_zip_reader_end(asMzArchive(zipHandle));
	commonCleanup();
	return result;
}

// ========================================================
// ZipReader::buildFileEntryTable():
// ========================================================

bool ZipReader::buildFileEntryTable(ErrorInfo * errorInfo)
{
	DEBUG_CHECK(zipHandle != nullptr);
	DEBUG_CHECK(zipFileTable.isEmpty());
	DEBUG_CHECK(zipFileEntries.isEmpty());

	mz_zip_archive * pZip = asMzArchive(zipHandle);
	const mz_uint fileCount = mz_zip_reader_get_num_files(pZip);

	// An empty zip is not considered an error.
	if (fileCount == 0)
	{
		return true;
	}

	// We can reserve memory beforehand.
	zipFileEntries.allocateExact(fileCount);

	int errorCount = 0;
	FileEntry newEntry;
	mz_zip_archive_file_stat archiveStats;

	for (mz_uint f = 0; f < fileCount; ++f)
	{
		clearPodObject(archiveStats);
		if (!mz_zip_reader_file_stat(pZip, f, &archiveStats))
		{
			// Keep track of errors, but attempt to continue.
			++errorCount;
			continue;
		}

		DEBUG_CHECK(archiveStats.m_file_index == f);

		//
		// NOTE:
		// At the moment we are not supporting large file inside the zip,
		// in favor of storing sizes as 32bit variables. If the need for huge files
		// ever arise, then this will have to be updated to use 64bit variables.
		//
		newEntry.compressedSizeInBytes   = static_cast<uint32>(archiveStats.m_comp_size);
		newEntry.uncompressedSizeInBytes = static_cast<uint32>(archiveStats.m_uncomp_size);
		newEntry.index                   = archiveStats.m_file_index;
		newEntry.crc32                   = archiveStats.m_crc32;
		newEntry.lastModificationTime    = archiveStats.m_time;
		newEntry.isDirectory             = !!mz_zip_reader_is_file_a_directory(pZip, f);
		newEntry.isEncrypted             = !!mz_zip_reader_is_file_encrypted(pZip, f);
		newEntry.setFilename(archiveStats.m_filename, zipFlags);

		addFileEntry(newEntry);
	}

	if (errorCount != 0)
	{
		makeError(errorInfo, "Failed to read " + toString(errorCount) + " file entries from zip archive!");
		return false;
	}

	// All entries were successfully read in.
	return true;
}

// ================================================================================================
// ZipReaderCached class implementation:
// ================================================================================================

// ========================================================
// ZipReaderCached::ZipReaderCached():
// ========================================================

ZipReaderCached::ZipReaderCached()
	: ZipReader()
{
}

// ========================================================
// ZipReaderCached::ZipReaderCached():
// ========================================================

ZipReaderCached::ZipReaderCached(const String & filePath, const uint flags, ErrorInfo * errorInfo)
	: ZipReader()
{
	openCachedReader(filePath, flags, errorInfo);
}

// ========================================================
// ZipReaderCached::~ZipReaderCached():
// ========================================================

ZipReaderCached::~ZipReaderCached()
{
	// `zipContents` has automatic cleanup.
	closeDefaultReader();
}

// ========================================================
// ZipReaderCached::open():
// ========================================================

bool ZipReaderCached::open(const String & filePath, const uint flags, ErrorInfo * errorInfo)
{
	return openCachedReader(filePath, flags, errorInfo);
}

// ========================================================
// ZipReaderCached::close():
// ========================================================

bool ZipReaderCached::close()
{
	zipContents.deallocate();
	return closeDefaultReader();
}

// ========================================================
// ZipReaderCached::openCachedReader():
// ========================================================

bool ZipReaderCached::openCachedReader(const String & filePath, const uint flags, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(!filePath.isEmpty());

	File::Stats newStats;
	if (!FileSystem::fileStatsForPath(filePath, newStats, errorInfo))
	{
		return false;
	}
	if (!newStats.isNormalFile)
	{
		makeError(errorInfo, "File path \"" + filePath + "\" points to a directory!");
		return false;
	}

	// Unlike the ZipReader, in the cached reader any potential
	// open files must be closed first. This is because we attempt
	// to reuse the existing byte array instead of allocating a fresh one.
	closeDefaultReader();

	// Load the whole file into main memory:
	if (!FileSystem::loadFile(filePath, zipContents, errorInfo))
	{
		zipContents.deallocate();
		return false;
	}

	mz_zip_archive * newZip = new mz_zip_archive();
	if (!mz_zip_reader_init_mem(newZip, zipContents.getData(), zipContents.getSize(), /* flags = */ 0))
	{
		zipContents.deallocate();
		delete newZip;

		makeError(errorInfo, "\'mz_zip_reader_init_mem()\' failed!");
		return false;
	}

	// Commit new states:
	zipHandle = newZip;
	zipFlags  = flags; // These are NOT the same flags passed mz_zip_reader_init_mem()!
	zipStats  = newStats;

	return buildFileEntryTable(errorInfo);
}

// ================================================================================================
// ZipWriter class implementation:
// ================================================================================================

// ========================================================
// ZipWriter::ZipWriter():
// ========================================================

ZipWriter::ZipWriter()
	: ZipArchive()
	, finalized(false)
{
}

// ========================================================
// ZipWriter::ZipWriter():
// ========================================================

ZipWriter::ZipWriter(const String & filePath, const uint flags, ErrorInfo * errorInfo)
	: ZipArchive()
	, finalized(false)
{
	openDefaultWriter(filePath, flags, errorInfo);
}

// ========================================================
// ZipWriter::~ZipWriter():
// ========================================================

ZipWriter::~ZipWriter()
{
	closeDefaultWriter();
}

// ========================================================
// ZipWriter::open():
// ========================================================

bool ZipWriter::open(const String & filePath, const uint flags, ErrorInfo * errorInfo)
{
	return openDefaultWriter(filePath, flags, errorInfo);
}

// ========================================================
// ZipWriter::close():
// ========================================================

bool ZipWriter::close()
{
	return closeDefaultWriter();
}

// ========================================================
// ZipWriter::finalizeArchive()
// ========================================================

bool ZipWriter::finalizeArchive()
{
	if (!isOpen())
	{
		return false;
	}

	const bool result = !!mz_zip_writer_finalize_archive(asMzArchive(zipHandle));
	finalized = true;
	return result;
}

// ========================================================
// ZipWriter::isFinalized():
// ========================================================

bool ZipWriter::isFinalized() const
{
	return finalized;
}

// ========================================================
// ZipWriter::addFileInMemory():
// ========================================================

bool ZipWriter::addFileInMemory(const String & filename, const ByteArray & fileContents,
                                const uint compressionLevel, const String * comment, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(!filename.isEmpty());

	if (!isOpen() || isFinalized())
	{
		makeError(errorInfo, "Zip archive is either closed or already finalized!");
		return false;
	}

	if (compressionLevel > Compression::Level::UberCompression)
	{
		makeError(errorInfo, "Compression level #" + toString(compressionLevel) + " not supported!");
		return false;
	}

	mz_zip_archive * pZip = asMzArchive(zipHandle);
	const ubyte * pData   = fileContents.isEmpty() ? nullptr : fileContents.getData();
	const uint dataLen    = fileContents.getSize();
	const char * pComment = (comment != nullptr) ? comment->getCStr()   : nullptr;
	const uint commentLen = (comment != nullptr) ? comment->getLength() : 0;

	if (!mz_zip_writer_add_mem_ex(pZip, filename.getCStr(), pData,
		dataLen, pComment, commentLen, compressionLevel, 0, 0))
	{
		makeError(errorInfo, "\'mz_zip_writer_add_mem_ex()\' failed!");
		return false;
	}

	if (!(zipFlags & ZipArchive::Flag::NoUpdateFileTable))
	{
		//
		// Update the pseudo file size and insert
		// a new entry in the file table.
		//
		// Note that the file timestamps are never updated
		// for a zip reader and thus are useless.
		//
		// We also don't compute the CRC for the 'fake' entry
		// added to the file table, since it is unlikely to
		// serve us any good, so avoid the computation.
		//
		zipStats.sizeInBytes += dataLen;

		FileEntry newEntry;
		newEntry.compressedSizeInBytes   = dataLen;
		newEntry.uncompressedSizeInBytes = dataLen;
		newEntry.index                   = zipFileEntries.getSize();
		newEntry.crc32                   = 0; /* Not computed for the zip writer */
		newEntry.lastModificationTime    = 0; /* Not computed for the zip writer */
		newEntry.isDirectory             = filename.back() == getPathSeparatorChar();
		newEntry.isEncrypted             = false;
		newEntry.setFilename(filename.getCStr(), zipFlags);

		addFileEntry(newEntry);
	}

	return true;
}

// ========================================================
// ZipWriter::addFileSystemFile():
// ========================================================

bool ZipWriter::addFileSystemFile(const String & filePath, const uint compressionLevel,
                                  const String * comment, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(!filePath.isEmpty());

	if (!isOpen() || isFinalized())
	{
		makeError(errorInfo, "Zip archive is either closed or already finalized!");
		return false;
	}

	ByteArray fileContents;
	if (!FileSystem::loadFile(filePath, fileContents, errorInfo))
	{
		return false;
	}

	return addFileInMemory(filePath, fileContents, compressionLevel, comment, errorInfo);
}

// ========================================================
// ZipWriter::openDefaultWriter():
// ========================================================

bool ZipWriter::openDefaultWriter(const String & filePath, const uint flags, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(!filePath.isEmpty());

	mz_zip_archive * newZip = new mz_zip_archive();
	if (!mz_zip_writer_init_file(newZip, filePath.getCStr(), /* size_to_reserve = */ 0))
	{
		delete newZip;
		makeError(errorInfo, "\'mz_zip_writer_init_file()\' failed!");
		return false;
	}

	// Now that the new archive is open we close any previously
	// open file and commit the new internal states.
	closeDefaultWriter();
	zipHandle = newZip;
	zipFlags  = flags;
	return true;
}

// ========================================================
// ZipWriter::closeDefaultWriter():
// ========================================================

bool ZipWriter::closeDefaultWriter()
{
	if (!isOpen())
	{
		return false;
	}

	if (!isFinalized())
	{
		finalizeArchive();
	}

	const bool result = !!mz_zip_writer_end(asMzArchive(zipHandle));
	commonCleanup();
	return result;
}

// ================================================================================================
// Compression class implementation:
// ================================================================================================

bool Compression::decompress(ubyte * dest, ulong * destSizeBytes, const ubyte * source,
                             const ulong sourceSizeBytes, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(dest != nullptr);
	DEBUG_CHECK(destSizeBytes != nullptr && *destSizeBytes != 0);

	DEBUG_CHECK(source != nullptr);
	DEBUG_CHECK(sourceSizeBytes != 0);

	const int result = mz_uncompress(dest, destSizeBytes, source, sourceSizeBytes);
	if (result != MZ_OK)
	{
		makeError(errorInfo, String("\'mz_uncompress()\' failed: ") + mz_error(result), result);
		return false;
	}

	return true;
}

bool Compression::compress(ubyte * dest, ulong * destSizeBytes, const ubyte * source,
                           const ulong sourceSizeBytes, const uint compressionLevel, ErrorInfo * errorInfo)
{
	DEBUG_CHECK(dest != nullptr);
	DEBUG_CHECK(destSizeBytes != nullptr && *destSizeBytes != 0);

	DEBUG_CHECK(source != nullptr);
	DEBUG_CHECK(sourceSizeBytes != 0);

	const int result = mz_compress2(dest, destSizeBytes, source, sourceSizeBytes, compressionLevel);
	if (result != MZ_OK)
	{
		makeError(errorInfo, String("\'mz_compress2()\' failed: ") + mz_error(result), result);
		return false;
	}

	return true;
}

} // namespace core {}
} // namespace atlas {}
