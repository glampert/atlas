
// ================================================================================================
// -*- C++ -*-
// File: path_utils.hpp
// Author: Guilherme R. Lampert
// Created on: 22/12/14
// Brief: Miscellaneous path and filename helpers.
// ================================================================================================

#ifndef ATLAS_CORE_FS_PATH_UTILS_HPP
#define ATLAS_CORE_FS_PATH_UTILS_HPP

#include "atlas/core/strings/string_base.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// getPathSeparatorStr():
// ========================================================

// Get the default path separator string used by the File System.
ATTRIBUTE_PURE
inline const char * getPathSeparatorStr()
{
	return "/";
}

// ========================================================
// getPathSeparatorChar():
// ========================================================

// Get the default path separator character used by the File System.
ATTRIBUTE_PURE
inline char getPathSeparatorChar()
{
	return '/';
}

// ========================================================
// stripPath():
// ========================================================

// Strips the path from a filename, returning only the filename,
// or the original input if no path to remove or an empty string if the input is a path with no filename.
template<class STR_TYPE>
STR_TYPE stripPath(const STR_TYPE & path, bool * hadPath = nullptr)
{
	STR_TYPE result;
	const int lastPathSep = path.lastIndexOf(getPathSeparatorChar());
	if (lastPathSep >= 0)
	{
		if (hadPath != nullptr)
		{
			*hadPath = true;
		}

		// Had path, return filename only, if there is one:
		const int pathChars = path.getLength() - (lastPathSep + 1);
		if (pathChars > 0)
		{
			result.assign(path, lastPathSep + 1, pathChars);
		}
	}
	else
	{
		// No path separator, just filename.
		if (hadPath != nullptr)
		{
			*hadPath = false;
		}
		result.assign(path);
	}
	return result;
}

// ========================================================
// stripFilename():
// ========================================================

// Strips the filename from a path, returning only the path, or the original input
// if no filename to strip. If the input is just a filename, returns an empty string.
template<class STR_TYPE>
STR_TYPE stripFilename(const STR_TYPE & path, bool * hadFilename = nullptr)
{
	STR_TYPE result;
	const int lastPathSep = path.lastIndexOf(getPathSeparatorChar());
	if (lastPathSep >= 0)
	{
		// Path will include the last separator.
		const int filenameChars = path.getLength() - (lastPathSep + 1);
		if (filenameChars > 0)
		{
			if (hadFilename != nullptr)
			{
				*hadFilename = true;
			}
			result.assign(path, path.getLength() - filenameChars);
		}
		else
		{
			if (hadFilename != nullptr)
			{
				*hadFilename = false;
			}
			result.assign(path);
		}
	}
	else
	{
		// No filename present. Will return an empty string.
		if (hadFilename != nullptr)
		{
			*hadFilename = true;
		}
	}
	return result;
}

// ========================================================
// stripFilenameExtension():
// ========================================================

// Strips the extension form a filename, returning just the filename.
// This also removes the last dot. Returns the unmodified input if no extension in the name.
template<class STR_TYPE>
STR_TYPE stripFilenameExtension(const STR_TYPE & filename, bool * hadExt = nullptr)
{
	STR_TYPE result;
	const int lastDot = filename.lastIndexOf('.');
	if (lastDot >= 0)
	{
		if (hadExt != nullptr)
		{
			*hadExt = true;
		}
		result.assign(filename, 0, lastDot);
	}
	else
	{
		if (hadExt != nullptr)
		{
			*hadExt = false;
		}
		result.assign(filename);
	}
	return result;
}

// ========================================================
// replaceFilenameExtension():
// ========================================================

// Replaces current filename extension or add a new one if none is present.
// Returns the new filename + extension string. `newExt` doesn't have to start with a dot.
template<class STR_TYPE>
STR_TYPE replaceFilenameExtension(const STR_TYPE & filename, const STR_TYPE & newExt)
{
	DEBUG_CHECK(!newExt.isEmpty());

	STR_TYPE result;
	const int lastDot = filename.lastIndexOf('.');
	if (lastDot >= 0)
	{
		result.assign(filename, lastDot);
		if (newExt.front() != '.')
		{
			result.append('.');
		}
		result.append(newExt);
	}
	else
	{
		result.assign(filename);
	}
	return result;
}

// ========================================================
// getFilenameExtension():
// ========================================================

// Returns the extension of a filename (everything after the last dot)
// or an empty string if no dot in the name. Can optionally exclude the dot from the output.
template<class STR_TYPE>
STR_TYPE getFilenameExtension(const STR_TYPE & filename, const bool includeDot = true)
{
	STR_TYPE result;
	const int lastDot = filename.lastIndexOf('.');
	if (lastDot >= 0)
	{
		const int extensionChars = filename.getLength() - lastDot;
		if (extensionChars > 0)
		{
			result.assign(filename, includeDot ? lastDot : (lastDot + 1), extensionChars);
		}
	}
	return result;
}

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_FS_PATH_UTILS_HPP
