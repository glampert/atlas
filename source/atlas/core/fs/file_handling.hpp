
// ================================================================================================
// -*- C++ -*-
// File: file_handling.hpp
// Author: Guilherme R. Lampert
// Created on: 19/06/15
// Brief: File access classes and a portable FileSystem wrapper.
// ================================================================================================

#ifndef ATLAS_CORE_FS_FILE_HANDLING_HPP
#define ATLAS_CORE_FS_FILE_HANDLING_HPP

#include "atlas/core/utils/utility.hpp"
#include "atlas/core/utils/error_info.hpp"
#include "atlas/core/strings/dynamic_string.hpp"
#include "atlas/core/containers/arrays.hpp"

#include <cstdio>
#include <cstdlib>
#include <ctime>

namespace atlas
{
namespace core
{

// ========================================================
// class File:
// ========================================================

class File
	: private NonCopyable
{
public:

	//
	// Constants and nested types:
	//

	typedef void *      Handle;    // Platform specific file handle type.
	typedef std::time_t Timestamp; // Platform specific timestamp type.

	struct SeekWay
	{
		enum Enum
		{
			Beginning, // Seek relative to the beginning of the file.
			Current,   // Seek relative to the current position.
			End        // Seek relative to the end of the file.
		};
	};

	struct Flag
	{
		enum Enum
		{
			Readable    = BIT(0), // File opened as a readable file. File must exist.
			Writable    = BIT(1), // File opened/created as a writable file. Creates file if it doesn't exist.
			Truncate    = BIT(2), // Any current content is discarded, assuming a length of zero at opening.
			Append      = BIT(3), // Set the file position indicator to the end-of-file before each output operation.
			NoCreate    = BIT(4), // If the file doesn't already exists, do no create it.
			NoOverwrite = BIT(5), // If file already exists, do not overwrite it.
			Unbuffered  = BIT(6), // Prefer an unbuffered file (hint only).
			Text        = BIT(7), // Open in text file mode.
			Binary      = BIT(8)  // Open in raw/binary file mode.
		};
	};

	struct Stats
	{
		Timestamp creationTime;         // Creation timestamp.
		Timestamp lastAccessTime;       // Last access on file.
		Timestamp lastModificationTime; // Last modification of file.
		ulong     sizeInBytes;          // File size in bytes.
		bool      isDirectory;          // True if the path provided actually pointed to a directory.
		bool      isNormalFile;         // True is the path provided pointed to a file.
	};

	// File Timestamp value to a printable string.
	static String timestampString(Timestamp timestamp);

	//
	// Common File interface:
	//
	bool open(const String & filePath, uint flags, ErrorInfo * errorInfo = nullptr);
	bool seek(long offset, SeekWay::Enum origin, ErrorInfo * errorInfo = nullptr);
	bool getPosition(ulong & pos, ErrorInfo * errorInfo = nullptr);
	bool close();
	bool rewind();
	bool clearErrorState();

	//
	// Miscellaneous queries:
	//
	bool isOpen() const;
	bool isAtEndOfFile() const;
	bool isInErrorState() const;
	bool isReadable() const;
	bool isWritable() const;
	bool isReadWrite() const;
	bool isText() const;
	bool isBinary() const;

	uint  getOpeningFlags() const;
	ulong getSizeInBytes() const;
	Timestamp getCreationTime() const;
	Timestamp getLastAccessTime() const;
	Timestamp getLastModificationTime() const;
	const Stats & getFileStats() const;

protected:

	// Files are not directly instantiated.
	// Only its child classes can be instantiated.
	File();

	// Protected and non-virtual. File instances are not
	// to be directly deleted. Destructor closes the file if open.
	~File();

	// Common file data:
	Handle fileHandle;
	Stats  fileStats;
	uint   openingFlags;
};

// ========================================================
// class ReadableFile:
// ========================================================

class ReadableFile
	: public virtual File
{
public:

	ReadableFile();
	ReadableFile(const String & filePath,
	             uint flags = File::Flag::Readable,
	             ErrorInfo * errorInfo = nullptr);

	virtual ~ReadableFile();

	// Binary data input:
	bool readBool(bool & value);
	bool readInt8(int8 & value);
	bool readUint8(uint8 & value);
	bool readInt16(int16 & value);
	bool readUint16(uint16 & value);
	bool readInt32(int32 & value);
	bool readUint32(uint32 & value);
	bool readInt64(int64 & value);
	bool readUint64(uint64 & value);
	bool readFloat(float & value);
	bool readDouble(double & value);

	// Read raw bytes (binary files). Returns the number of bytes successfully read.
	uint readBytes(void * destBuffer, uint bytesToRead);

	// Read a single character from a text file.
	bool readChar(char & value);

	// Line reading (up to a '\n', included), (text files only).
	bool readLine(String & line);
	bool readLine(char * line, uint maxCharsToRead);

	// Scan with format string (text files only). Same rules of `std::fscanf()`.
	int scanF(const char * format, ...) ATTRIBUTE_FORMAT_FUNC(scanf, 2, 3);
};

// ========================================================
// class WritableFile:
// ========================================================

class WritableFile
	: public virtual File
{
public:

	WritableFile();
	WritableFile(const String & filePath,
	             uint flags = File::Flag::Writable,
	             ErrorInfo * errorInfo = nullptr);

	virtual ~WritableFile();

	// Binary data output:
	bool writeBool(bool value);
	bool writeInt8(int8 value);
	bool writeUint8(uint8 value);
	bool writeInt16(int16 value);
	bool writeUint16(uint16 value);
	bool writeInt32(int32 value);
	bool writeUint32(uint32 value);
	bool writeInt64(int64 value);
	bool writeUint64(uint64 value);
	bool writeFloat(float value);
	bool writeDouble(double value);

	// Write raw bytes (binary files). Returns the number of bytes successfully written.
	uint writeBytes(const void * srcBuffer, uint bytesToWrite);

	// Write a single character to a text file.
	bool writeChar(char value);

	// Write strings (text files only).
	bool writeString(const String & str);
	bool writeString(const char * cstr);

	// Print format string (for text files only). Same rules of `std::fprintf()`.
	int printF(const char * format, ...) ATTRIBUTE_FORMAT_FUNC(printf, 2, 3);
	int vaPrintF(const char * format, va_list vaList) ATTRIBUTE_FORMAT_FUNC(printf, 2, 0);

	// Flush buffered writes.
	bool flush();
};

// ========================================================
// class ReadWriteFile:
// ========================================================

class ReadWriteFile
	: public virtual ReadableFile
	, public virtual WritableFile
{
public:

	ReadWriteFile();
	ReadWriteFile(const String & filePath,
	              uint flags = File::Flag::Readable | File::Flag::Writable,
	              ErrorInfo * errorInfo = nullptr);

	virtual ~ReadWriteFile();
};

// ========================================================
// class FileSystem:
// ========================================================

class FileSystem final
{
public:

	// Max length of a path string in characters, including one
	// last character for the null byte terminating the string.
	static constexpr uint MaxPathLen = 256;

	// String with a pre-allocated buffer that is long enough to hold a File System path.
	typedef SizedString<MaxPathLen> PathString;

	//
	// File and directory queries:
	//
	static bool pathExist(const String & path);
	static bool pathExistAndIsFile(const String & path);
	static bool pathExistAndIsDirectory(const String & path);
	static bool fileStatsForPath(const String & path, File::Stats & statsOut, ErrorInfo * errorInfo = nullptr);
	static ulong queryFileSize(const String & filePath, ErrorInfo * errorInfo = nullptr);

	//
	// File System helpers:
	//
	static PathString getCurrentDirectory();
	static bool setCurrentDirectory(const String & dirPath);
	static bool makeSingleDirectory(const String & dirPath, ErrorInfo * errorInfo = nullptr);
	static bool makePath(const String & pathEndedWithSeparatorOrFilename, ErrorInfo * errorInfo = nullptr);
	static bool removeFile(const String & filePath, ErrorInfo * errorInfo = nullptr);

	//
	// Load whole files into memory,
	// write memory buffers to file:
	//
	static bool loadFile(const String & filePath, ByteArray & fileContents, ErrorInfo * errorInfo = nullptr);
	static bool writeFile(const String & filePath, const ByteArray & fileContents, ErrorInfo * errorInfo = nullptr);

	// Opens the file in text mode, reads it into a char string and appends
	// a null terminator at the end. The output length is the length in chars,
	// not counting the appended null byte. The output buffer should be freed with `delete[]`.
	static bool loadTextFile(const String & filePath, char *& fileContents,
	                         uint & fileLength, ErrorInfo * errorInfo = nullptr);

private:

	// This class is is not instantiable.
	// All methods and data are static.
	 FileSystem() DEFAULTED_METHOD;
	~FileSystem() DEFAULTED_METHOD;

	FileSystem(const FileSystem &) DELETED_METHOD;
	FileSystem & operator = (const FileSystem &) DELETED_METHOD;
};

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_FS_FILE_HANDLING_HPP
