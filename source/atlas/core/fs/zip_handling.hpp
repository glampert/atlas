
// ================================================================================================
// -*- C++ -*-
// File: zip_handling.hpp
// Author: Guilherme R. Lampert
// Created on: 23/06/15
// Brief: Zip archive handling and IO plus some data compression/decompression helpers.
// ================================================================================================

#ifndef ATLAS_CORE_FS_ZIP_HANDLING_HPP
#define ATLAS_CORE_FS_ZIP_HANDLING_HPP

#include "atlas/core/fs/file_handling.hpp"
#include "atlas/core/containers/intrusive_hash_table.hpp"

namespace atlas
{
namespace core
{

// ========================================================
// class ZipArchive:
// ========================================================

// Common functionality shared by zip archive readers and writers.
class ZipArchive
	: private NonCopyable
{
public:

	//
	// Nested types and constants:
	//

	// Opaque handle to the underlaying zip library implementation.
	typedef void * Handle;

	// Archive opening flags.
	struct Flag
	{
		enum Enum
		{
			// File searches are NOT case-sensitive if this flag is specified.
			CaseInsensitive   = BIT(0),

			// Path is ignored in file searches (uses the filename+extension only).
			IgnoreDirPath     = BIT(1),

			// ZipWriter flag. Prevents the writer from updating the internal file
			// table when new files are added. Use this flag if you only care about
			// the output archive and won't query the added entries afterwards.
			NoUpdateFileTable = BIT(2)
		};
	};

	// Minimal information for a file or directory entry in the zip archive.
	class FileEntry final
		: public HashTableNode<FileEntry>
	{
	public:

		// Sizes in bytes of the file contents.
		// Zero if the entry is actually a directory.
		uint32 compressedSizeInBytes;
		uint32 uncompressedSizeInBytes;

		// Index that can be passed to `getEntryByIndex()` and the
		// CRC-32 of this file's contents (CRC is zero for a directory).
		uint32 index;
		uint32 crc32;

		// Last modification timestamp for this file.
		// Zero for a directory entry.
		File::Timestamp lastModificationTime;

		// Filename. If `Flag::IgnoreDirPath` was used and this is a file,
		// it WILL NOT include the directory path. If this is a directory
		// then the filename will always be terminated by a path separator char.
		String filename;

		// Only set if the entry is a directory dummy.
		bool isDirectory;

		// Individual files may be encrypted.
		bool isEncrypted;

		// Set the filename and transforms it according to the zip opening flags.
		void setFilename(const char * name, uint flags);
	};

	//
	// Public interface:
	//

	virtual ~ZipArchive();

	// Open or close zip archive in the file system:
	virtual bool open(const String & filePath, uint flags = 0, ErrorInfo * errorInfo = nullptr) = 0;
	virtual bool close() = 0;
	bool isOpen() const;

	// File-in-zip queries:
	uint getFileEntryCount() const;
	bool hasEntryAndIsFile(const String & filename) const;
	const FileEntry * getEntryByIndex(uint index) const;
	const FileEntry * getEntryByName(const String & filename) const;

	// Query the actual zip archive file stats:
	ulong getZipSizeInBytes() const;
	uint getZipOpeningFlags() const;
	File::Timestamp getZipCreationTime() const;
	File::Timestamp getZipLastAccessTime() const;
	File::Timestamp getZipLastModificationTime() const;
	const File::Stats & getZipFileStats() const;

	// Provided mainly for debugging and printing:
	const Array<FileEntry> & getAllFileEntries() const;
	const IntrusiveHashTable<String, FileEntry> & getFileEntryTable() const;

protected:

	// Zip Packages are not directly instantiated.
	// Only its child classes can be instantiated.
	ZipArchive();

	// Reset internal states, deletes the handle and clear the array & table.
	void commonCleanup();

	// Add an entry to the array and its corresponding link in the table.
	void addFileEntry(const FileEntry & newEntry);

	//
	// Common data:
	//

	// Opaque handle to the mini-z back-end implementation.
	Handle zipHandle;

	// Miscellaneous flags for file searching. See `ZipArchive::Flag`.
	uint zipFlags;

	// File table expanded from the zip's central directory (files + directories).
	IntrusiveHashTable<String, FileEntry> zipFileTable;

	// Linear array of file entries, since the hash-table only stores references.
	Array<FileEntry> zipFileEntries;

	// File stats for the whole zip archive in the FS.
	File::Stats zipStats;
};

// ========================================================
// class ZipReader:
// ========================================================

// Zip archive reader that reads data directly from file (stdio).
class ZipReader
	: public ZipArchive
{
public:

	ZipReader();
	ZipReader(const String & filePath, uint flags = 0, ErrorInfo * errorInfo = nullptr);
	virtual ~ZipReader(); // Closes the zip archive.

	virtual bool open(const String & filePath, uint flags = 0, ErrorInfo * errorInfo = nullptr) override;
	virtual bool close() override;

	//
	// File extraction and decompression to memory.
	// (Files only; fails for a directory entry).
	//
	bool extractFileToMemoryByIndex(uint index,
	                                ByteArray & fileContents,
	                                ErrorInfo * errorInfo = nullptr) const;

	bool extractFileToMemoryByName(const String & filename,
	                               ByteArray & fileContents,
	                               ErrorInfo * errorInfo = nullptr) const;

	bool extractFileToMemory(const FileEntry & fileEntry,
	                         ByteArray & fileContents,
	                         ErrorInfo * errorInfo = nullptr) const;

	//
	// Extracts every file inside the archive to a loose file in the
	// local file system. Will create directory paths as necessary.
	//
	bool extractWholeArchiveToFileSystem(const String & destBasePath,
	                                     ErrorInfo * errorInfo = nullptr) const;

protected:

	bool openDefaultReader(const String & filePath, uint flags, ErrorInfo * errorInfo);
	bool buildFileEntryTable(ErrorInfo * errorInfo);
	bool closeDefaultReader();
};

// ========================================================
// class ZipReaderCached:
// ========================================================

// Zip archive reader that fully caches the zip file contents into main memory.
// It will perform a single file IO operation to load the whole archive once.
class ZipReaderCached final
	: public ZipReader
{
public:

	ZipReaderCached();
	ZipReaderCached(const String & filePath, uint flags = 0, ErrorInfo * errorInfo = nullptr);
	~ZipReaderCached(); // Closes the zip archive.

	bool open(const String & filePath, uint flags = 0, ErrorInfo * errorInfo = nullptr) override;
	bool close() override;

private:

	// Overrides the archive opening to read from memory.
	bool openCachedReader(const String & filePath, uint flags, ErrorInfo * errorInfo);

	// Zip archive contents in memory.
	ByteArray zipContents;
};

// ========================================================
// class ZipWriter:
// ========================================================

// Zip archive writing and appending.
class ZipWriter final
	: public ZipArchive
{
public:

	ZipWriter();
	ZipWriter(const String & filePath, uint flags = 0, ErrorInfo * errorInfo = nullptr);
	~ZipWriter(); // Closes the zip archive.

	// Note: Opening an exiting archive in the file system will
	// cause its contents to be completely overwritten if `open()` succeeds.
	bool open(const String & filePath, uint flags = 0, ErrorInfo * errorInfo = nullptr) override;
	bool close() override;

	// Manually finalizes the current zip archive.
	// This is called automatically by `close()` or the destructor.
	// Note: After an archive is finalized, files can no longer be added to it.
	bool finalizeArchive();
	bool isFinalized() const;

	//
	// Add files or raw data to the currently open archive.
	// `compressionLevel` is one of the Compression::Level
	// flags or a value between 0 and 10 (inclusive).
	//
	bool addFileInMemory(const String & filename,
	                     const ByteArray & fileContents,
	                     uint compressionLevel,
	                     const String * comment = nullptr,
	                     ErrorInfo * errorInfo  = nullptr);

	bool addFileSystemFile(const String & filePath,
	                       uint compressionLevel,
	                       const String * comment = nullptr,
	                       ErrorInfo * errorInfo  = nullptr);

private:

	bool openDefaultWriter(const String & filePath, uint flags, ErrorInfo * errorInfo);
	bool closeDefaultWriter();

	bool finalized;
};

// ========================================================
// class Compression:
// ========================================================

// Helper functions for compression and decompression of raw data.
// Note: Use the ZipArchive classes to handle zip files.
class Compression final
{
public:

	// Data compression levels (a value between 0 and 10).
	// Note: Values compatible with ZLib and mini-z.
	struct Level
	{
		enum Enum
		{
			NoCompression      = 0,
			BestSpeed          = 1,
			DefaultCompression = 6,
			BestCompression    = 9,
			UberCompression    = 10
		};
	};

	//
	// Easy one call compression and decompression of raw data:
	//

	// `dest` is the decompression buffer; `source` is the compressed data.
	static bool decompress(ubyte * dest,
	                       ulong * destSizeBytes,
	                       const ubyte * source,
	                       ulong sourceSizeBytes,
	                       ErrorInfo * errorInfo = nullptr);

	// `dest` is the compressed output; `source` is the uncompressed input data.
	// `compressionLevel` is one of the `Level` flags or a value between 0 and 10.
	static bool compress(ubyte * dest,
	                     ulong * destSizeBytes,
	                     const ubyte * source,
	                     ulong sourceSizeBytes,
	                     uint compressionLevel,
	                     ErrorInfo * errorInfo = nullptr);

private:

	// This class is is not instantiable.
	// All methods and data are static.
	 Compression() DEFAULTED_METHOD;
	~Compression() DEFAULTED_METHOD;

	Compression(const Compression &) DELETED_METHOD;
	Compression & operator = (const Compression &) DELETED_METHOD;
};

} // namespace core {}
} // namespace atlas {}

#endif // ATLAS_CORE_FS_ZIP_HANDLING_HPP
