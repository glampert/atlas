
// ================================================================================================
// -*- C++ -*-
// File: common.hpp
// Author: Guilherme R. Lampert
// Created on: 17/12/14
// Brief: Compiler switches and common compile-time configurations/constants.
// ================================================================================================

#ifndef ATLAS_COMMON_HPP
#define ATLAS_COMMON_HPP

// Rely on the compiler for basic native types:
#include <cstdint>
#include <cstddef>
#include <climits> // INT_MAX/INT_MAX/etc

//
//TODO put all compile time switches and C++11 compatibility
//stuff in here. This header is referenced by most files, if not all.
//

#if ATLAS_COMPILER_HAS_CPP11
	#define DELETED_METHOD   = delete
	#define DEFAULTED_METHOD = default
#else //!ATLAS_COMPILER_HAS_CPP11
	#define DELETED_METHOD   /* nothing */
	#define DEFAULTED_METHOD { }
#endif // ATLAS_COMPILER_HAS_CPP11

//TODO also need a switch for this!
#define RESTRICT __restrict

//TODO temporary!
#include <cassert>
#define DEBUG_CHECK   assert
#define ALWAYS_CHECK  assert
#define UNREACHABLE() assert(false && "Unreachable path was executed!")
#define COMPILE_TIME_CHECK(cond, message) static_assert(cond, message)

//
// Appends two tokens into a single name/identifier.
// Normally used to declared internal/built-in functions and variables.
//
#define STRING_JOIN2_HELPER(x, y) x ## y
#define STRING_JOIN2(x, y) STRING_JOIN2_HELPER(x, y)

//
// Appends three tokens into a single name/identifier.
// Normally used to declared internal/built-in functions and variables.
//
#define STRING_JOIN3_HELPER(x, y, z) x ## y ## z
#define STRING_JOIN3(x, y, z) STRING_JOIN3_HELPER(x, y, z)

#define BIT(n) (1 << (n))

//
// Attributes:
//
#if ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE

#define ATTRIBUTE_PURE      __attribute__((pure))
#define ATTRIBUTE_CONST     __attribute__((const))
#define ATTRIBUTE_NO_RETURN __attribute__((noreturn))
#define ATTRIBUTE_PACKED    __attribute__((packed))

//#define ATTRIBUTE_ALIGNED(alignment, expr) __attribute__((aligned(alignment))) expr
#define ATTRIBUTE_ALIGNED(alignment, expr) alignas(alignment) expr

#define ALIGN_AS(T, expr) alignas(T) expr

// Can fallback to `thread_local` (C++11) or `__declspec(thread)` on Windows.
#define ATTRIBUTE_TLS __thread

//C++11!
#define ALIGNMENT_OF(expr) alignof(expr)

#define ATTRIBUTE_FORMAT_FUNC(baseFunc, stringIndex, firstToCheck) __attribute__((format(baseFunc, stringIndex, firstToCheck)))

#else // !ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE

//TODO fallback

#endif // ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE

//
// GCC/Clang define __func__, whereas Visual Studio defines __FUNCTION__.
// NOTE: On GCC/Clang __func__ is actually a local variable, thus only valid inside a function.
//
#ifndef __FUNCTION__
	#ifdef __GNUC__
		#define __FUNCTION__ __func__
	#else // !__GNUC__
		#define __FUNCTION__ "???"
	#endif // __GNUC__
#endif // __FUNCTION__

// For C++98 compat:
//#define nullptr   NULL
//#define constexpr const
//#define noexcept  /* nothing */
//#define override  /* nothing */
//#define final     /* nothing */

#endif // ATLAS_COMMON_HPP
