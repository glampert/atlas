
// ================================================================================================
// -*- C++ -*-
// File: key_codes.hpp
// Author: Guilherme R. Lampert
// Created on: 09/09/15
// Brief: Keyboard and mouse key codes and modifiers.
// ================================================================================================

#ifndef ATLAS_SHELL_KEY_CODES_HPP
#define ATLAS_SHELL_KEY_CODES_HPP

#include "atlas/core/utils/basic_types.hpp"
#include "atlas/core/strings/to_string.hpp"

namespace atlas
{
namespace shell
{

// ========================================================
// Mouse button ids:
// ========================================================

struct MouseButton
{
	enum Enum
	{
		Left   = BIT(0),
		Right  = BIT(1),
		Middle = BIT(2),
		LastButton = Middle // Sentry value. Internal use.
	};

	// Set of mouse buttons (possibly ORed together) to printable string.
	static core::String toString(int buttons);
};

// Accepts any of the MouseButton enum constants.
typedef core::int16 MouseButtonId;

// ========================================================
// Keyboard keys and modifiers:
// ========================================================

// Known keys in a standard QWERTY keyboard.
//
// ASCII keys are defined by their equivalent ASCII codes.
// Non-ASCII/function keys are numbered above 255.
//
// New keys may be added as needed, however, a key number should
// always be under 32767 (INT16_MAX)
struct Key
{
	enum Enum
	{
		Unknown           = -1,

		// Printable ASCII keys:
		Space             = ' ',
		Apostrophe        = '\'',
		Comma             = ',',
		Minus             = '-',
		Dot               = '.',
		Slash             = '/',
		Num0              = '0',
		Num1              = '1',
		Num2              = '2',
		Num3              = '3',
		Num4              = '4',
		Num5              = '5',
		Num6              = '6',
		Num7              = '7',
		Num8              = '8',
		Num9              = '9',
		Semicolon         = ';',
		Equal             = '=',
		A                 = 'A',
		B                 = 'B',
		C                 = 'C',
		D                 = 'D',
		E                 = 'E',
		F                 = 'F',
		G                 = 'G',
		H                 = 'H',
		I                 = 'I',
		J                 = 'J',
		K                 = 'K',
		L                 = 'L',
		M                 = 'M',
		N                 = 'N',
		O                 = 'O',
		P                 = 'P',
		Q                 = 'Q',
		R                 = 'R',
		S                 = 'S',
		T                 = 'T',
		U                 = 'U',
		V                 = 'V',
		W                 = 'W',
		X                 = 'X',
		Y                 = 'Y',
		Z                 = 'Z',
		LeftBracket       = '[',
		RightBracket      = ']',
		Backslash         = '\\',
		GraveAccent       = '`',

		// Non-ASCII char keys should be added in this gap.
		// 'GraveAccent' (`) is number 96.

		// Function keys / key pad:
		Escape            = 256,
		Return            = 257,
		Tab               = 258,
		Backspace         = 259,
		Insert            = 260,
		Delete            = 261,
		Right             = 262,
		Left              = 263,
		Down              = 264,
		Up                = 265,
		PageUp            = 266,
		PageDown          = 267,
		Home              = 268,
		End               = 269,
		CapsLock          = 280,
		ScrollLock        = 281,
		NumLock           = 282,
		PrintScreen       = 283,
		Pause             = 284,
		F1                = 290,
		F2                = 291,
		F3                = 292,
		F4                = 293,
		F5                = 294,
		F6                = 295,
		F7                = 296,
		F8                = 297,
		F9                = 298,
		F10               = 299,
		F11               = 300,
		F12               = 301,
		F13               = 302,
		F14               = 303,
		F15               = 304,
		// 305 to 319 Reserved for future expansion
		KeyPad0           = 320,
		KeyPad1           = 321,
		KeyPad2           = 322,
		KeyPad3           = 323,
		KeyPad4           = 324,
		KeyPad5           = 325,
		KeyPad6           = 326,
		KeyPad7           = 327,
		KeyPad8           = 328,
		KeyPad9           = 329,
		KeyPadDecimal     = 330,
		KeyPadDivide      = 331,
		KeyPadMultiply    = 332,
		KeyPadSubtract    = 333,
		KeyPadAdd         = 334,
		KeyPadReturn      = 335,
		KeyPadEqual       = 336,
		LeftShift         = 340,
		LeftControl       = 341,
		LeftAlt           = 342,
		LeftCommand       = 343, // AKA "Apple Key" (Left side)
		RightShift        = 344,
		RightControl      = 345,
		RightAlt          = 346,
		RightCommand      = 347, // AKA "Apple Key" (Right side)
		Menu              = 348,

		// Sentry value. Internal use.
		LastKey           = Menu
	};

	// Key constant to printable string.
	static core::String toString(int key);

	struct Modifier
	{
		enum Enum
		{
			// If this bit is set one or more Shift keys were held down when a key was pressed.
			Shift   = BIT(0),

			// If this bit is set one or more Control keys were held down when a key was pressed.
			Control = BIT(1),

			// If this bit is set one or more Alt keys were held down when a key was pressed.
			Alt     = BIT(2),

			// If this bit is set one or more Command (Apple Key) keys were held down when a key was pressed.
			Command = BIT(3)
		};

		// Set of key modifiers to printable string.
		static core::String toString(int mods);
	};
};

// Key code/id type. Accepts any of the Key enum constants.
typedef core::int16 KeyCode;

} // namespace shell {}
} // namespace atlas {}

#endif // ATLAS_SHELL_KEY_CODES_HPP
