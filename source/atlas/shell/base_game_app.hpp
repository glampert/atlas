
// ================================================================================================
// -*- C++ -*-
// File: base_game_app.hpp
// Author: Guilherme R. Lampert
// Created on: 08/09/15
// Brief: Base class for the game application.
// ================================================================================================

#ifndef ATLAS_SHELL_BASE_GAME_APP_HPP
#define ATLAS_SHELL_BASE_GAME_APP_HPP

#if ATLAS_SHELL_LIB_BACKEND_SDL
	#include "atlas/shell/sdl/render_window_sdlgl.hpp"
#endif // ATLAS_SHELL_LIB_BACKEND_SDL

#include "atlas/shell/app_events.hpp"

namespace atlas
{
namespace shell
{

// ========================================================
// class BaseGameApp:
// ========================================================

class BaseGameApp
	: private core::NonCopyable
{
public:

	// Late initialization (after construction).
	virtual void onInit();

	// Early shutdown (before destruction).
	virtual void onShutdown();

	// Pre-rendering update. Called with the frame's delta time.
	virtual void onFrameUpdate(const core::Time & elapsedTime);

	// Rendering update (called after onFrameUpdate).
	virtual void onFrameRender(const core::Time & elapsedTime);

	// Handle an application event. Return true if the event was handled, false otherwise.
	virtual bool onAppEvent(const AppEvent & event);

	// Return true when the application should quit to the host environment.
	virtual bool shouldQuit() const = 0;

	// Called by the shell to send a quit command to the game/application.
	// This happens if the user tries to close the game window, for example.
	// If the game wishes to comply, it must return true on shouldQuit().
	virtual void sendQuitMessage() = 0;

	// Implemented in the source for to anchor the vtable.
	virtual ~BaseGameApp();

	// Get the global simulation clock.
	const core::Clock & getGlobalClock() const;

	// Grab a reference to the render window object.
	RenderWindow & getRenderWindow();

protected:

	BaseGameApp();

	// Only one application instance per program!
	static bool instanceCreated;

	// Main simulation real-time clock.
	core::Clock globalClock;

#if ATLAS_SHELL_LIB_BACKEND_SDL
	RenderWindowSDLGL renderWindow;
#else // SHELL_LIB_BACKEND undefined
	#error "Define a SHELL_LIB_BACKEND for this platform!"
#endif // SHELL_LIB_BACKEND
};

// ========================================================
// Miscellaneous application helpers:
// ========================================================

// Set the argc/argv style command line, where argv[0]
// is assumed to always be the name of the program/executable.
void setAppCommandLine(int argc, const char * argv[]);

// Get the application command line as a single string.
// Each argument is separated by a whitespace. No program name in it.
const char * getAppCommandLine();

// Get the program name part of the command line (argv[0]).
const char * getAppProgramName();

// Wraps the system cursor back to the give position.
// Does nothing if the system has no cursor or if it is disabled.
// Coordinates are relative to the window/screen. Origin at the top-left corner.
void warpSystemCursor(const core::Vec2 & position);

// Enters the main application loop, only returning when the
// application quits. Fatal errors might still terminate the app.
// This function is not meant to be called by user code, only by our main().
void mainLoop(BaseGameApp & appInstance);

// ========================================================
// ATLAS_APPLICATION_CLASS macro / main entry point:
// ========================================================

template<class T>
inline void runUserApp()
{
	// Allocate on the heap so T can be of any size
	// without worrying about stressing the program stack.
	core::AutoPtr<T> appInstance(new T());
	appInstance->onInit();
	mainLoop(*appInstance);
	appInstance->onShutdown();
}

// Hides main() so we can port more easily (WinMain() and other differences come into play).
#define ATLAS_APPLICATION_CLASS(className) \
	int main(int argc, const char * argv[]) \
	{ \
		atlas::shell::setAppCommandLine(argc, argv); \
		atlas::shell::runUserApp<className>(); \
		return 0; \
	}

} // namespace shell {}
} // namespace atlas {}

#endif // ATLAS_SHELL_BASE_GAME_APP_HPP
