
// ================================================================================================
// -*- C++ -*-
// File: shell.hpp
// Author: Guilherme R. Lampert
// Created on: 10/09/15
// Brief: Main public include for the Atlas Shell library.
// ================================================================================================

#ifndef ATLAS_SHELL_SHELL_HPP
#define ATLAS_SHELL_SHELL_HPP

#include "atlas/shell/app_events.hpp"
#include "atlas/shell/base_game_app.hpp"
#include "atlas/shell/kbd_mouse_input.hpp"

#endif // ATLAS_SHELL_SHELL_HPP
