
// ================================================================================================
// -*- C++ -*-
// File: render_window.cpp
// Author: Guilherme R. Lampert
// Created on: 08/09/15
// Brief: An OpenGL/D3D capable render window.
// ================================================================================================

#include "atlas/shell/render_window.hpp"

namespace atlas
{
namespace shell
{

// ========================================================
// RenderWindow::~RenderWindow():
// ========================================================

RenderWindow::~RenderWindow()
{
	// Defining it here to anchor the vtable and
	// avoid the "weak-vtables" warning from Clang.
}

// ========================================================
// RenderWindow::getContextParams():
// ========================================================

const RenderContextParams & RenderWindow::getContextParams() const
{
	return contexParams;
}

} // namespace shell {}
} // namespace atlas {}
