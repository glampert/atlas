
// ================================================================================================
// -*- C++ -*-
// File: kbd_mouse_input.cpp
// Author: Guilherme R. Lampert
// Created on: 09/09/15
// Brief: Keyboard and mouse input helpers.
// ================================================================================================

#include "atlas/shell/kbd_mouse_input.hpp"
#include "atlas/shell/base_game_app.hpp"

namespace atlas
{
namespace shell
{

// ================================================================================================
// KeyboardInputHandler class implementation:
// ================================================================================================

KeyboardInputHandler::KeyboardInputHandler()
{
	clear();
}

bool KeyboardInputHandler::isKeyUp(const KeyCode key) const
{
	DEBUG_CHECK(unsigned(key) < core::arrayLength(keyboard));
	return keyboard[key].keyState ? false : true;
}

bool KeyboardInputHandler::isKeyDown(const KeyCode key) const
{
	DEBUG_CHECK(unsigned(key) < core::arrayLength(keyboard));
	return keyboard[key].keyState ? true : false;
}

void KeyboardInputHandler::getKeyState(const KeyCode key, bool & isDown, int & modifiers) const
{
	DEBUG_CHECK(unsigned(key) < core::arrayLength(keyboard));
	isDown    = keyboard[key].keyState ? true : false;
	modifiers = static_cast<int>(keyboard[key].modifiers);
}

bool KeyboardInputHandler::onAppEvent(const AppEvent & event)
{
	switch (event.type)
	{
	case AppEvent::KeyPress :
		DEBUG_CHECK(unsigned(event.value.keyboard.keyCode) < core::arrayLength(keyboard));
		keyboard[event.value.keyboard.keyCode].keyState  = 1;
		keyboard[event.value.keyboard.keyCode].modifiers = static_cast<core::ubyte>( // Actually only 7 bits!
		                                                   event.value.keyboard.modifiers);
		return true;

	case AppEvent::KeyRelease :
		DEBUG_CHECK(unsigned(event.value.keyboard.keyCode) < core::arrayLength(keyboard));
		keyboard[event.value.keyboard.keyCode].keyState  = 0;
		keyboard[event.value.keyboard.keyCode].modifiers = 0;
		return true;

	// Event not handled here.
	default : return false;
	} // switch (event.type)
}

void KeyboardInputHandler::clear()
{
	core::clearPodArray(keyboard);
}

// ================================================================================================
// MouseInputHandler class implementation:
// ================================================================================================

MouseInputHandler::MouseInputHandler()
{
	clear();
}

bool MouseInputHandler::isButtonUp(const MouseButtonId button) const
{
	DEBUG_CHECK(unsigned(button) < core::arrayLength(buttonsDown));
	return !buttonsDown[button];
}

bool MouseInputHandler::isButtonDown(const MouseButtonId button) const
{
	DEBUG_CHECK(unsigned(button) < core::arrayLength(buttonsDown));
	return buttonsDown[button];
}

core::Vec2 MouseInputHandler::getCursorPosition() const
{
	return mousePos;
}

core::Vec2 MouseInputHandler::getMouseWheelScroll() const
{
	return mouseScroll;
}

void MouseInputHandler::setCursorPosition(const core::Vec2 & warpPosition)
{
	warpSystemCursor(warpPosition);
	mousePos = warpPosition;
}

bool MouseInputHandler::onAppEvent(const AppEvent & event)
{
	switch (event.type)
	{
	case AppEvent::MouseMove :
		mousePos.x = event.getMouseMotionX();
		mousePos.y = event.getMouseMotionY();
		return true;

	case AppEvent::MouseScroll :
		mouseScroll.x = event.getMouseScrollX();
		mouseScroll.y = event.getMouseScrollY();
		return true;

	case AppEvent::MouseButtonClick :
		DEBUG_CHECK(unsigned(event.getMouseButton()) < core::arrayLength(buttonsDown));
		buttonsDown[event.getMouseButton()] = true;
		return true;

	case AppEvent::MouseButtonRelease :
		DEBUG_CHECK(unsigned(event.getMouseButton()) < core::arrayLength(buttonsDown));
		buttonsDown[event.getMouseButton()] = false;
		return true;

	// Event not handled here.
	default : return false;
	} // switch (event.type)
}

void MouseInputHandler::clear()
{
	mousePos    = core::Vec2::origin();
	mouseScroll = core::Vec2::origin();
	core::clearPodArray(buttonsDown);
}

} // namespace shell {}
} // namespace atlas {}
