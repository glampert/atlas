
// ================================================================================================
// -*- C++ -*-
// File: shell_log.hpp
// Author: Guilherme R. Lampert
// Created on: 08/09/15
// Brief: Log stream used by Shell library.
// ================================================================================================

#ifndef ATLAS_SHELL_SHELL_LOG_HPP
#define ATLAS_SHELL_SHELL_LOG_HPP

#include "atlas/core/utils/default_log.hpp"

namespace atlas
{
namespace shell
{

// FIXME This is temporary!!!
inline core::LogStream & getLog() { return atlas::core::getLog(); }
inline int getLogVerbosityLevel() { return core::LogLevel::Info;  }

// FIXME This is temporary!!!
#define SH_LOG_FATAL(message) LOG_FATAL_EX(atlas::core::getLog(), atlas::core::getLogVerbosityLevel(), "|ShellLog| " << message)
#define SH_LOG_ERROR(message) LOG_ERROR_EX(atlas::core::getLog(), atlas::core::getLogVerbosityLevel(), "|ShellLog| " << message)
#define SH_LOG_WARN(message)  LOG_WARN_EX(atlas::core::getLog(),  atlas::core::getLogVerbosityLevel(), "|ShellLog| " << message)
#define SH_LOG_INFO(message)  LOG_INFO_EX(atlas::core::getLog(),  atlas::core::getLogVerbosityLevel(), "|ShellLog| " << message)

} // namespace shell {}
} // namespace atlas {}

#endif // ATLAS_SHELL_SHELL_LOG_HPP
