
// ================================================================================================
// -*- C++ -*-
// File: app_events.hpp
// Author: Guilherme R. Lampert
// Created on: 09/09/15
// Brief: Application/system events.
// ================================================================================================

#ifndef ATLAS_SHELL_APP_EVENTS_HPP
#define ATLAS_SHELL_APP_EVENTS_HPP

#include "atlas/core/utils/basic_types.hpp"
#include "atlas/core/maths/vectors.hpp"

namespace atlas
{
namespace shell
{

// ========================================================
// struct AppEvent:
// ========================================================

struct AppEvent final
{
	enum TypeId
	{
		// Null event. All event values are meaningless.
		Null = 0,

		// Mouse events:
		MouseMove,
		MouseScroll,
		MouseButtonClick,
		MouseButtonRelease,

		// Keyboard events:
		KeyPress,
		KeyRelease,
		KeyChar // First byte of `keyboard.keyCode` has the char.
	};

	union ValueUnion
	{
		void * data[2];    // Raw event data as pointers.

		struct {
			float x;       // Origin at the top left corner of the window.
			float y;       // Origin at the top left corner of the window.
			int buttons;   // Bit vector with ORed MouseButton constants or zero.
		} mouseMotion;

		struct {
			float xAmount; // The amount scrolled horizontally, positive to the right and negative to the left.
			float yAmount; // The amount scrolled vertically, forward scroll is positive, back scroll negative.
		} mouseScroll;

		struct {
			int buttonId;  // One of the MouseButton constants.
			int clicks;    // Num clicks in a short succession. 2 for a double-click, etc (MouseButtonClick only).
		} mouseButton;

		struct {
			int keyCode;   // One of the Key constants or a key char if type == KeyChar.
			int modifiers; // ALT/CTRL/CMD and such modifiers ORed together.
		} keyboard;
	};

	ValueUnion value;      // Data associated with the event.
	TypeId     type;       // Event type identifier/tag.

	// Event types:
	bool isNullEvent()               const { return type == Null;               }
	bool isMouseMoveEvent()          const { return type == MouseMove;          }
	bool isMouseScrollEvent()        const { return type == MouseScroll;        }
	bool isMouseButtonClickEvent()   const { return type == MouseButtonClick;   }
	bool isMouseButtonReleaseEvent() const { return type == MouseButtonRelease; }
	bool isKeyPressEvent()           const { return type == KeyPress;           }
	bool isKeyReleaseEvent()         const { return type == KeyRelease;         }
	bool isKeyCharEvent()            const { return type == KeyChar;            }

	// MouseMove event:
	float getMouseMotionX()          const { return value.mouseMotion.x;        }
	float getMouseMotionY()          const { return value.mouseMotion.y;        }
	int   getMouseMotionButtons()    const { return value.mouseMotion.buttons;  }

	// MouseScroll event:
	float getMouseScrollX()          const { return value.mouseScroll.xAmount;  }
	float getMouseScrollY()          const { return value.mouseScroll.yAmount;  }

	// MouseButtonClick/Release events:
	int getMouseButton()             const { return value.mouseButton.buttonId; }
	int getMouseClicks()             const { return value.mouseButton.clicks;   }

	// KeyPress/KeyRelease/KeyChar events:
	int  getKeyCode()                const { return value.keyboard.keyCode;     }
	int  getKeyModifiers()           const { return value.keyboard.modifiers;   }
	char getKeyChar()                const { return static_cast<char>(value.keyboard.keyCode); }

	// Nullify this event and its data.
	void clear()
	{
		type = Null;
		value.data[0] = nullptr;
		value.data[1] = nullptr;
	}
};

} // namespace shell {}
} // namespace atlas {}

#endif // ATLAS_SHELL_APP_EVENTS_HPP
