
// ================================================================================================
// -*- C++ -*-
// File: test_window.cpp
// Author: Guilherme R. Lampert
// Created on: 07/09/15
// Brief: Test window/screen initialization and basic management.
// ================================================================================================

// Lib Core:
#include "atlas/core/utils.hpp"
#include "atlas/core/strings.hpp"

// Lib Shell:
#include "atlas/shell/shell.hpp"
#include "atlas/shell/shell_log.hpp"

namespace atlas
{
namespace shell
{
namespace unittest
{

// ========================================================
// class MyApp:
// ========================================================

class MyApp final
	: public BaseGameApp
{
public:

	 MyApp();
	~MyApp();

	bool shouldQuit() const override { return quit; }
	void sendQuitMessage()  override { quit = true; }

	void onInit()     override;
	void onShutdown() override;

	bool onAppEvent(const AppEvent & event) override;
	void onFrameUpdate(const atlas::core::Time & elapsedTime) override;
	void onFrameRender(const atlas::core::Time & elapsedTime) override;

private:

	MouseInputHandler    mouseInput;
	KeyboardInputHandler keyboardInput;
	bool quit;
};

// ========================================================

MyApp::MyApp() : quit(false)
{
	SH_LOG_INFO("---- MyApp() ----");
}

MyApp::~MyApp()
{
	SH_LOG_INFO("---- ~MyApp() ----");
}

void MyApp::onInit()
{
	SH_LOG_INFO("---- MyApp::onInit() ----");

	const RenderContextParams params = {
		/* screenWidthPx   = */ 800,
		/* screenHeightPx  = */ 600,
		/* depthBits       = */ 32,
		/* stencilBits     = */ 0,
		/* glVersionMajor  = */ 3,
		/* glVersionMinor  = */ 2,
		/* multisamples    = */ 0,
		/* glCoreProfile   = */ true,
		/* debugContext    = */ false,
		/* doubleBuffer    = */ true,
		/* allowResizing   = */ false,
		/* srgbFrameBuffer = */ false,
		/* fullScreen      = */ false,
		/* vSync           = */ true
	};

	const bool result = renderWindow.createWindow("Test window", params);
	ALWAYS_CHECK(result == true && "Can't create render window!");

	renderWindow.makeContextCurrent();
}

void MyApp::onShutdown()
{
	SH_LOG_INFO("---- MyApp::onShutdown() ----");
}

bool MyApp::onAppEvent(const AppEvent & event)
{
	if (event.isMouseMoveEvent())
	{
		SH_LOG_INFO("mouse.xy ( "
			<< static_cast<int>(event.getMouseMotionX()) << ", "
			<< static_cast<int>(event.getMouseMotionY()) << " ) "
			<< MouseButton::toString(event.getMouseMotionButtons()));
	}
	else if (event.isMouseButtonClickEvent())
	{
		SH_LOG_INFO("mouse.click " << MouseButton::toString(event.getMouseButton())
			<< " (" << event.getMouseClicks() << ")");
	}
	else if (event.isMouseButtonReleaseEvent())
	{
		SH_LOG_INFO("mouse.release " << MouseButton::toString(event.getMouseButton())
			<< " (" << event.getMouseClicks() << ")");
	}
	else if (event.isMouseScrollEvent())
	{
		SH_LOG_INFO("mouse.scroll ( "
			<< event.getMouseScrollX() << ", "
			<< event.getMouseScrollY() << " )");
	}
	else if (event.isKeyCharEvent())
	{
		SH_LOG_INFO("key.char \'" << event.getKeyChar() << "\'");
	}
	else if (event.isKeyPressEvent())
	{
		SH_LOG_INFO("key.press \'" << Key::toString(event.getKeyCode()) << "\' "
			<< Key::Modifier::toString(event.getKeyModifiers()));

		if (event.getKeyCode() == Key::Return)
		{
			// Position the pointer in the center of the window.
			warpSystemCursor(core::Vec2(400, 300));
		}
	}
	else if (event.isKeyReleaseEvent())
	{
		SH_LOG_INFO("key.release \'" << Key::toString(event.getKeyCode()) << "\'");
	}

	bool handled = mouseInput.onAppEvent(event);
	if (!handled)
	{
		handled = keyboardInput.onAppEvent(event);
	}

	// True if event handled. Doesn't propagate any further.
	return handled;
}

// Test window won't render anything.
void MyApp::onFrameUpdate(const atlas::core::Time & /* elapsedTime */) { }
void MyApp::onFrameRender(const atlas::core::Time & /* elapsedTime */) { }

} // namespace unittest {}
} // namespace shell {}
} // namespace atlas {}

using atlas::shell::unittest::MyApp;
ATLAS_APPLICATION_CLASS(MyApp);

