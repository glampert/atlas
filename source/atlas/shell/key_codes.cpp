
// ================================================================================================
// -*- C++ -*-
// File: key_codes.cpp
// Author: Guilherme R. Lampert
// Created on: 09/09/15
// Brief: To string helpers for Key/MouseButton.
// ================================================================================================

#include "atlas/shell/key_codes.hpp"

namespace atlas
{
namespace shell
{

// ========================================================
// MouseButton::toString():
// ========================================================

core::String MouseButton::toString(const int buttons)
{
	core::String str;
	if (buttons & MouseButton::Left  ) { str += "left ";   }
	if (buttons & MouseButton::Right ) { str += "right ";  }
	if (buttons & MouseButton::Middle) { str += "middle "; }
	return str.trimRight();
}

// ========================================================
// Key::toString():
// ========================================================

core::String Key::toString(const int key)
{
	if (key > 0 && key < 128) // ASCII chars print directly.
	{
		core::String str;
		if (key == ' ')
		{
			str = "space";
		}
		else
		{
			str += static_cast<char>(key);
		}
		return str;
	}

	switch (key)
	{
	case Key::Escape         : return "escape";
	case Key::Return         : return "return";
	case Key::Tab            : return "tab";
	case Key::Backspace      : return "backspace";
	case Key::Insert         : return "insert";
	case Key::Delete         : return "delete";
	case Key::Right          : return "right";
	case Key::Left           : return "left";
	case Key::Down           : return "down";
	case Key::Up             : return "up";
	case Key::PageUp         : return "page up";
	case Key::PageDown       : return "page down";
	case Key::Home           : return "home";
	case Key::End            : return "end";
	case Key::CapsLock       : return "caps lock";
	case Key::ScrollLock     : return "scroll lock";
	case Key::NumLock        : return "num lock";
	case Key::PrintScreen    : return "print screen";
	case Key::Pause          : return "pause";
	case Key::F1             : return "F1";
	case Key::F2             : return "F2";
	case Key::F3             : return "F3";
	case Key::F4             : return "F4";
	case Key::F5             : return "F5";
	case Key::F6             : return "F6";
	case Key::F7             : return "F7";
	case Key::F8             : return "F8";
	case Key::F9             : return "F9";
	case Key::F10            : return "F10";
	case Key::F11            : return "F11";
	case Key::F12            : return "F12";
	case Key::F13            : return "F13";
	case Key::F14            : return "F14";
	case Key::F15            : return "F15";
	case Key::KeyPad0        : return "keypad 0";
	case Key::KeyPad1        : return "keypad 1";
	case Key::KeyPad2        : return "keypad 2";
	case Key::KeyPad3        : return "keypad 3";
	case Key::KeyPad4        : return "keypad 4";
	case Key::KeyPad5        : return "keypad 5";
	case Key::KeyPad6        : return "keypad 6";
	case Key::KeyPad7        : return "keypad 7";
	case Key::KeyPad8        : return "keypad 8";
	case Key::KeyPad9        : return "keypad 9";
	case Key::KeyPadDecimal  : return "keypad .";
	case Key::KeyPadDivide   : return "keypad /";
	case Key::KeyPadMultiply : return "keypad *";
	case Key::KeyPadSubtract : return "keypad -";
	case Key::KeyPadAdd      : return "keypad +";
	case Key::KeyPadEqual    : return "keypad =";
	case Key::KeyPadReturn   : return "keypad return";
	case Key::LeftShift      : return "left shift";
	case Key::LeftControl    : return "left ctrl";
	case Key::LeftAlt        : return "left alt";
	case Key::LeftCommand    : return "left cmd";
	case Key::RightShift     : return "right shift";
	case Key::RightControl   : return "right ctrl";
	case Key::RightAlt       : return "right alt";
	case Key::RightCommand   : return "right cmd";
	case Key::Menu           : return "menu key";
	default                  : return "unknown";
	} // switch (key)
}

// ========================================================
// Key::Modifier::toString():
// ========================================================

core::String Key::Modifier::toString(const int mods)
{
	core::String str;
	if (mods & Key::Modifier::Shift  ) { str += "shift ";   }
	if (mods & Key::Modifier::Control) { str += "control "; }
	if (mods & Key::Modifier::Alt    ) { str += "alt ";     }
	if (mods & Key::Modifier::Command) { str += "command "; }
	return str.trimRight();
}

} // namespace shell {}
} // namespace atlas {}
