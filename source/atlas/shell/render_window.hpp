
// ================================================================================================
// -*- C++ -*-
// File: render_window.hpp
// Author: Guilherme R. Lampert
// Created on: 08/09/15
// Brief: An OpenGL/D3D capable render window.
// ================================================================================================

#ifndef ATLAS_SHELL_RENDER_WINDOW_HPP
#define ATLAS_SHELL_RENDER_WINDOW_HPP

#include "atlas/core/utils.hpp"
#include "atlas/core/memory.hpp"
#include "atlas/core/strings.hpp"

namespace atlas
{
namespace shell
{

// ========================================================
// struct RenderContextParams:
// ========================================================

struct RenderContextParams final
{
	core::uint screenWidthPx;
	core::uint screenHeightPx;

	core::uint depthBits;
	core::uint stencilBits;

	core::uint glVersionMajor;
	core::uint glVersionMinor;

	int  multisamples;
	bool glCoreProfile;
	bool debugContext;
	bool doubleBuffer;
	bool allowResizing;
	bool srgbFrameBuffer;
	bool fullScreen;
	bool vSync;
};

// ========================================================
// class RenderWindow:
// ========================================================

class RenderWindow
	: private core::NonCopyable
{
public:

	// Create/open the window. If the window is already created, you'll get an assertion.
	// If you'd like to have an initially minimize window, pass `showWin` as false.
	// `winName` will be the window name and title.
	virtual bool createWindow(const core::String & winName,
	                          const RenderContextParams & params,
	                          bool showWin = true) = 0;

	// Destroy the window. The window is automatically
	// destroyed in the class destructor if still alive by then.
	virtual void destroyWindow() = 0;

	// Makes the rendering context current in the calling thread. This must be
	// performed once before issuing any calls to the renderer back-end (OpenGL).
	// You'll usually bind the rendering context to the main thread and seldom, if ever, change it.
	virtual void makeContextCurrent() = 0;

	// Swap render framebuffers to present the drawings done to the window.
	virtual void swapBuffers() = 0;

	// Show/hide the window (maximize/minimize). After creation,
	// the window is visible by default, unless specified otherwise.
	virtual void showWindow() = 0;
	virtual void hideWindow() = 0;
	virtual bool isWindowHidden() const = 0;

	// Access the underlying window API object/handle.
	virtual void * getApiObject() const = 0;

	// Access the rendering context parameters. Avoid holding references
	// to it, since the rendering parameters might change during runtime.
	const RenderContextParams & getContextParams() const;

	// Cleans up and closes the window if still open.
	virtual ~RenderWindow();

protected:

	RenderContextParams contexParams;
};

} // namespace shell {}
} // namespace atlas {}

#endif // ATLAS_SHELL_RENDER_WINDOW_HPP
