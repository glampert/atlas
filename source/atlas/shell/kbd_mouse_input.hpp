
// ================================================================================================
// -*- C++ -*-
// File: kbd_mouse_input.hpp
// Author: Guilherme R. Lampert
// Created on: 09/09/15
// Brief: Keyboard and mouse input helpers.
// ================================================================================================

#ifndef ATLAS_SHELL_KBD_MOUSE_INPUT_HPP
#define ATLAS_SHELL_KBD_MOUSE_INPUT_HPP

#include "atlas/shell/key_codes.hpp"
#include "atlas/shell/app_events.hpp"

namespace atlas
{
namespace shell
{

// ========================================================
// class KeyboardInputHandler:
// ========================================================

class KeyboardInputHandler final
	: private core::NonCopyable
{
public:

	KeyboardInputHandler();

	// Test for up/down keys.
	bool isKeyUp(KeyCode key) const;
	bool isKeyDown(KeyCode key) const;

	// Key down state and modifiers.
	void getKeyState(KeyCode key, bool & isDown, int & modifiers) const;

	// Call this method to feed input events to the keyboard handler.
	// If the event is not handled, this method returns false.
	bool onAppEvent(const AppEvent & event);

	// Clear all key down states.
	void clear();

private:

	// Size of the keyboard[] array:
	enum { KeyCount = Key::LastKey + 1 };

	// Bit field for a key state (0=up, 1=down) plus 7 bits for modifiers.
	struct KeyStatePlusModifiers
	{
		core::ubyte keyState  : 1;
		core::ubyte modifiers : 7;
	};

	COMPILE_TIME_CHECK(sizeof(KeyStatePlusModifiers) == 1, "Bad KeyStatePlusModifiers size!");

	// Array with a slot for every entry in the 'Key' enumeration.
	// Each entry will store the up/down state of the key plus
	// the modifiers that were set when the event was generated.
	KeyStatePlusModifiers keyboard[KeyCount];
};

// ========================================================
// class MouseInputHandler:
// ========================================================

class MouseInputHandler final
	: private core::NonCopyable
{
public:

	MouseInputHandler();

	// Test for up/down mouse buttons:
	bool isButtonUp(MouseButtonId button) const;
	bool isButtonDown(MouseButtonId button) const;

	// Mouse cursor position and wheel scroll offsets.
	// Mouse cursor (0,0) origin is the top-left corner of the screen.
	core::Vec2 getCursorPosition()   const;
	core::Vec2 getMouseWheelScroll() const;
	void setCursorPosition(const core::Vec2 & warpPosition);

	// Call this method to feed input events to the mouse/pointer handler.
	// If the event is not handled, this method returns false.
	bool onAppEvent(const AppEvent & event);

	// Clear all button down states and reset position to origin.
	void clear();

private:

	// Current mouse position and wheel
	// scroll offsets. Initially zero.
	core::Vec2 mousePos;
	core::Vec2 mouseScroll;

	// Array of mouse button states; false=down, true=up.
	enum { ButtonCount = MouseButton::LastButton + 1 };
	bool buttonsDown[ButtonCount];
};

} // namespace shell {}
} // namespace atlas {}

#endif // ATLAS_SHELL_KBD_MOUSE_INPUT_HPP
