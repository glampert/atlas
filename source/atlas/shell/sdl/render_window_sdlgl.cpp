
// ================================================================================================
// -*- C++ -*-
// File: render_window_sdlgl.cpp
// Author: Guilherme R. Lampert
// Created on: 08/09/15
// Brief: SDL and OpenGL based RenderWindow.
// ================================================================================================

#include "atlas/shell/sdl/render_window_sdlgl.hpp"
#include "atlas/shell/shell_log.hpp"

namespace atlas
{
namespace shell
{

// ========================================================
// RenderWindowSDLGL::RenderWindowSDLGL():
// ========================================================

RenderWindowSDLGL::RenderWindowSDLGL()
	: sdlWindow(nullptr)
	, sdlGLContext(nullptr)
	, hiddenOrMinimized(false)
{
}

// ========================================================
// RenderWindowSDLGL::~RenderWindowSDLGL():
// ========================================================

RenderWindowSDLGL::~RenderWindowSDLGL()
{
	sdlWinShutdown();
}

// ========================================================
// RenderWindowSDLGL::sdlWinShutdown():
// ========================================================

void RenderWindowSDLGL::sdlWinShutdown()
{
	if (sdlWindow != nullptr)
	{
		SDL_GL_DeleteContext(sdlGLContext);
		SDL_DestroyWindow(sdlWindow);

		sdlGLContext = nullptr;
		sdlWindow    = nullptr;
	}

	SDL_Quit();
}

// ========================================================
// RenderWindowSDLGL::createWindow():
// ========================================================

bool RenderWindowSDLGL::createWindow(const core::String & winName,
                                     const RenderContextParams & params,
                                     const bool showWin)
{
	DEBUG_CHECK(sdlWindow == nullptr && "Duplicate initialization of RenderWindow!");
	SH_LOG_INFO("Creating SDL window and OpenGL context...");

	if (SDL_Init(SDL_INIT_EVENTS | SDL_INIT_VIDEO) != 0)
	{
		SH_LOG_ERROR("SDL_Init() failed: " << SDL_GetError());
		return false;
	}

	SDL_version version;
	SDL_GetVersion(&version);
	SH_LOG_INFO("SDL version: " << version.major << "." << version.minor << "." << version.patch);

	//
	// Set window creation hints:
	//

	int sdlGLProfile;
	if (params.glCoreProfile)
	{
		sdlGLProfile = SDL_GL_CONTEXT_PROFILE_CORE;
	}
	else
	{
		sdlGLProfile = SDL_GL_CONTEXT_PROFILE_COMPATIBILITY;
	}
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, sdlGLProfile);

	int sdlGLFlags = SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG;
	if (params.debugContext)
	{
		sdlGLFlags |= SDL_GL_CONTEXT_DEBUG_FLAG;
	}
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, sdlGLFlags);

	if (params.multisamples > 0)
	{
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, params.multisamples);
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,    params.glVersionMajor);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,    params.glVersionMinor);
	SDL_GL_SetAttribute(SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, params.srgbFrameBuffer);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,             params.doubleBuffer);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,               params.depthBits);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE,             params.stencilBits);

	//
	// Create window and OpenGL rendering context:
	//

	core::uint sdlWindowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI;
	if (params.allowResizing)
	{
		sdlWindowFlags |= SDL_WINDOW_RESIZABLE;
	}
	if (params.fullScreen)
	{
		sdlWindowFlags |= SDL_WINDOW_FULLSCREEN;
		sdlWindowFlags |= SDL_WINDOW_INPUT_GRABBED;
	}
	if (!showWin)
	{
		sdlWindowFlags |= SDL_WINDOW_MINIMIZED;
	}

	sdlWindow = SDL_CreateWindow(winName.getCStr(),
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			params.screenWidthPx, params.screenHeightPx,
			sdlWindowFlags);
	if (sdlWindow == nullptr)
	{
		SH_LOG_ERROR("Failed to create SDL window! " << SDL_GetError());
		return false;
	}

	// GL rendering context creation:
	sdlGLContext = SDL_GL_CreateContext(sdlWindow);
	if (sdlGLContext == nullptr)
	{
		SH_LOG_ERROR("Failed to create OpenGL context! " << SDL_GetError());
		return false;
	}

	// Optional vertical-sync:
	if (SDL_GL_SetSwapInterval(params.vSync) != 0)
	{
		SH_LOG_WARN("Failed to set swap interval! " << SDL_GetError());
	}

	// Save them for future queries.
	contexParams      = params;
	hiddenOrMinimized = showWin ? false : true;

	SH_LOG_INFO("Finished creating the window.");
	return true;
}

// ========================================================
// RenderWindowSDLGL::destroyWindow():
// ========================================================

void RenderWindowSDLGL::destroyWindow()
{
	sdlWinShutdown();
}

// ========================================================
// RenderWindowSDLGL::makeContextCurrent():
// ========================================================

void RenderWindowSDLGL::makeContextCurrent()
{
	DEBUG_CHECK(sdlWindow != nullptr);
	if (SDL_GL_MakeCurrent(sdlWindow, sdlGLContext) != 0)
	{
		SH_LOG_ERROR("Failed to make GL context current! " << SDL_GetError());
	}
}

// ========================================================
// RenderWindowSDLGL::swapBuffers():
// ========================================================

void RenderWindowSDLGL::swapBuffers()
{
	DEBUG_CHECK(sdlWindow != nullptr);
	SDL_GL_SwapWindow(sdlWindow);
}

// ========================================================
// RenderWindowSDLGL::showWindow():
// ========================================================

void RenderWindowSDLGL::showWindow()
{
	DEBUG_CHECK(sdlWindow != nullptr);
	SDL_RestoreWindow(sdlWindow);
	hiddenOrMinimized = false;
}

// ========================================================
// RenderWindowSDLGL::hideWindow():
// ========================================================

void RenderWindowSDLGL::hideWindow()
{
	DEBUG_CHECK(sdlWindow != nullptr);
	SDL_MinimizeWindow(sdlWindow);
	hiddenOrMinimized = true;
}

// ========================================================
// RenderWindowSDLGL::isWindowHidden():
// ========================================================

bool RenderWindowSDLGL::isWindowHidden() const
{
	return hiddenOrMinimized;
}

// ========================================================
// RenderWindowSDLGL::getApiObject():
// ========================================================

void * RenderWindowSDLGL::getApiObject() const
{
	return sdlWindow;
}

} // namespace shell {}
} // namespace atlas {}
