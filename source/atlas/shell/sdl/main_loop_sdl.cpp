
// ================================================================================================
// -*- C++ -*-
// File: main_loop_sdl.cpp
// Author: Guilherme R. Lampert
// Created on: 09/09/15
// Brief: SDL main application loop.
// ================================================================================================

#include "atlas/shell/base_game_app.hpp"
#include "atlas/shell/key_codes.hpp"

namespace atlas
{
namespace shell
{

// ========================================================
// Local helpers:
// ========================================================

namespace
{

int sdlKeyCodeMap(const SDL_Keysym sdlKey)
{
	switch (sdlKey.sym)
	{
	case SDLK_SPACE        : return Key::Space;
	case SDLK_QUOTE        : return Key::Apostrophe;
	case SDLK_COMMA        : return Key::Comma;
	case SDLK_MINUS        : return Key::Minus;
	case SDLK_PERIOD       : return Key::Dot;
	case SDLK_SLASH        : return Key::Slash;
	case SDLK_0            : return Key::Num0;
	case SDLK_1            : return Key::Num1;
	case SDLK_2            : return Key::Num2;
	case SDLK_3            : return Key::Num3;
	case SDLK_4            : return Key::Num4;
	case SDLK_5            : return Key::Num5;
	case SDLK_6            : return Key::Num6;
	case SDLK_7            : return Key::Num7;
	case SDLK_8            : return Key::Num8;
	case SDLK_9            : return Key::Num9;
	case SDLK_SEMICOLON    : return Key::Semicolon;
	case SDLK_EQUALS       : return Key::Equal;
	case SDLK_a            : return Key::A;
	case SDLK_b            : return Key::B;
	case SDLK_c            : return Key::C;
	case SDLK_d            : return Key::D;
	case SDLK_e            : return Key::E;
	case SDLK_f            : return Key::F;
	case SDLK_g            : return Key::G;
	case SDLK_h            : return Key::H;
	case SDLK_i            : return Key::I;
	case SDLK_j            : return Key::J;
	case SDLK_k            : return Key::K;
	case SDLK_l            : return Key::L;
	case SDLK_m            : return Key::M;
	case SDLK_n            : return Key::N;
	case SDLK_o            : return Key::O;
	case SDLK_p            : return Key::P;
	case SDLK_q            : return Key::Q;
	case SDLK_r            : return Key::R;
	case SDLK_s            : return Key::S;
	case SDLK_t            : return Key::T;
	case SDLK_u            : return Key::U;
	case SDLK_v            : return Key::V;
	case SDLK_w            : return Key::W;
	case SDLK_x            : return Key::X;
	case SDLK_y            : return Key::Y;
	case SDLK_z            : return Key::Z;
	case SDLK_LEFTBRACKET  : return Key::LeftBracket;
	case SDLK_RIGHTBRACKET : return Key::RightBracket;
	case SDLK_BACKSLASH    : return Key::Backslash;
	case SDLK_BACKQUOTE    : return Key::GraveAccent;
	case SDLK_ESCAPE       : return Key::Escape;
	case SDLK_RETURN       : return Key::Return;
	case SDLK_TAB          : return Key::Tab;
	case SDLK_BACKSPACE    : return Key::Backspace;
	case SDLK_INSERT       : return Key::Insert;
	case SDLK_DELETE       : return Key::Delete;
	case SDLK_RIGHT        : return Key::Right;
	case SDLK_LEFT         : return Key::Left;
	case SDLK_DOWN         : return Key::Down;
	case SDLK_UP           : return Key::Up;
	case SDLK_PAGEUP       : return Key::PageUp;
	case SDLK_PAGEDOWN     : return Key::PageDown;
	case SDLK_HOME         : return Key::Home;
	case SDLK_END          : return Key::End;
	case SDLK_CAPSLOCK     : return Key::CapsLock;
	case SDLK_SCROLLLOCK   : return Key::ScrollLock;
	case SDLK_NUMLOCKCLEAR : return Key::NumLock;
	case SDLK_PRINTSCREEN  : return Key::PrintScreen;
	case SDLK_PAUSE        : return Key::Pause;
	case SDLK_F1           : return Key::F1;
	case SDLK_F2           : return Key::F2;
	case SDLK_F3           : return Key::F3;
	case SDLK_F4           : return Key::F4;
	case SDLK_F5           : return Key::F5;
	case SDLK_F6           : return Key::F6;
	case SDLK_F7           : return Key::F7;
	case SDLK_F8           : return Key::F8;
	case SDLK_F9           : return Key::F9;
	case SDLK_F10          : return Key::F10;
	case SDLK_F11          : return Key::F11;
	case SDLK_F12          : return Key::F12;
	case SDLK_F13          : return Key::F13;
	case SDLK_F14          : return Key::F14;
	case SDLK_F15          : return Key::F15;
	case SDLK_KP_0         : return Key::KeyPad0;
	case SDLK_KP_1         : return Key::KeyPad1;
	case SDLK_KP_2         : return Key::KeyPad2;
	case SDLK_KP_3         : return Key::KeyPad3;
	case SDLK_KP_4         : return Key::KeyPad4;
	case SDLK_KP_5         : return Key::KeyPad5;
	case SDLK_KP_6         : return Key::KeyPad6;
	case SDLK_KP_7         : return Key::KeyPad7;
	case SDLK_KP_8         : return Key::KeyPad8;
	case SDLK_KP_9         : return Key::KeyPad9;
	case SDLK_KP_DECIMAL   : return Key::KeyPadDecimal;
	case SDLK_KP_DIVIDE    : return Key::KeyPadDivide;
	case SDLK_KP_MULTIPLY  : return Key::KeyPadMultiply;
	case SDLK_KP_MINUS     : return Key::KeyPadSubtract;
	case SDLK_KP_PLUS      : return Key::KeyPadAdd;
	case SDLK_KP_ENTER     : return Key::KeyPadReturn;
	case SDLK_KP_EQUALS    : return Key::KeyPadEqual;
	case SDLK_LSHIFT       : return Key::LeftShift;
	case SDLK_LCTRL        : return Key::LeftControl;
	case SDLK_LALT         : return Key::LeftAlt;
	case SDLK_LGUI         : return Key::LeftCommand;
	case SDLK_RSHIFT       : return Key::RightShift;
	case SDLK_RCTRL        : return Key::RightControl;
	case SDLK_RALT         : return Key::RightAlt;
	case SDLK_RGUI         : return Key::RightCommand;
	case SDLK_MENU         : return Key::Menu;
	default                : return Key::Unknown;
	} // switch (sdlKey.sym)
}

int sdlKeyModsMap(const int sdlKeyMod)
{
	int mods = 0;
	if (sdlKeyMod & KMOD_SHIFT) { mods |= Key::Modifier::Shift;   }
	if (sdlKeyMod & KMOD_CTRL ) { mods |= Key::Modifier::Control; }
	if (sdlKeyMod & KMOD_ALT  ) { mods |= Key::Modifier::Alt;     }
	if (sdlKeyMod & KMOD_GUI  ) { mods |= Key::Modifier::Command; }
	return mods;
}

bool sdlEventToAppEvent(const SDL_Event & sdlEvent, AppEvent & appEvent)
{
	// SDL_Event reference:
	//  https://wiki.libsdl.org/SDL_Event

	switch (sdlEvent.type)
	{
	case SDL_MOUSEBUTTONDOWN :
		{
			if (sdlEvent.button.button == SDL_BUTTON_LEFT)
			{
				appEvent.type = AppEvent::MouseButtonClick;
				appEvent.value.mouseButton.buttonId = MouseButton::Left;
				appEvent.value.mouseButton.clicks   = sdlEvent.button.clicks;
				return true;
			}
			else if (sdlEvent.button.button == SDL_BUTTON_RIGHT)
			{
				appEvent.type = AppEvent::MouseButtonClick;
				appEvent.value.mouseButton.buttonId = MouseButton::Right;
				appEvent.value.mouseButton.clicks   = sdlEvent.button.clicks;
				return true;
			}
			else if (sdlEvent.button.button == SDL_BUTTON_MIDDLE)
			{
				appEvent.type = AppEvent::MouseButtonClick;
				appEvent.value.mouseButton.buttonId = MouseButton::Middle;
				appEvent.value.mouseButton.clicks   = sdlEvent.button.clicks;
				return true;
			}
			return false;
		}
	case SDL_MOUSEBUTTONUP :
		{
			if (sdlEvent.button.button == SDL_BUTTON_LEFT)
			{
				appEvent.type = AppEvent::MouseButtonRelease;
				appEvent.value.mouseButton.buttonId = MouseButton::Left;
				appEvent.value.mouseButton.clicks   = 0;
				return true;
			}
			else if (sdlEvent.button.button == SDL_BUTTON_RIGHT)
			{
				appEvent.type = AppEvent::MouseButtonRelease;
				appEvent.value.mouseButton.buttonId = MouseButton::Right;
				appEvent.value.mouseButton.clicks   = 0;
				return true;
			}
			else if (sdlEvent.button.button == SDL_BUTTON_MIDDLE)
			{
				appEvent.type = AppEvent::MouseButtonRelease;
				appEvent.value.mouseButton.buttonId = MouseButton::Middle;
				appEvent.value.mouseButton.clicks   = 0;
				return true;
			}
			return false;
		}
	case SDL_MOUSEMOTION :
		{
			appEvent.type = AppEvent::MouseMove;
			appEvent.value.mouseMotion.buttons = 0;
			appEvent.value.mouseMotion.x = sdlEvent.motion.x;
			appEvent.value.mouseMotion.y = sdlEvent.motion.y;

			const unsigned int state = sdlEvent.motion.state;
			if (state & SDL_BUTTON_LMASK)
			{
				appEvent.value.mouseMotion.buttons |= MouseButton::Left;
			}
			if (state & SDL_BUTTON_RMASK)
			{
				appEvent.value.mouseMotion.buttons |= MouseButton::Right;
			}
			if (state & SDL_BUTTON_MMASK)
			{
				appEvent.value.mouseMotion.buttons |= MouseButton::Middle;
			}

			return true;
		}
	case SDL_MOUSEWHEEL :
		{
			appEvent.type = AppEvent::MouseScroll;
			// We want +Y forward and -Y back. SDL seems to get it the other way around.
			appEvent.value.mouseScroll.xAmount = sdlEvent.wheel.x * -1.0f;
			appEvent.value.mouseScroll.yAmount = sdlEvent.wheel.y * -1.0f;
			return true;
		}
	case SDL_KEYDOWN :
		{
			appEvent.type = AppEvent::KeyPress;
			appEvent.value.keyboard.keyCode   = sdlKeyCodeMap(sdlEvent.key.keysym);
			appEvent.value.keyboard.modifiers = sdlKeyModsMap(sdlEvent.key.keysym.mod);
			return true;
		}
	case SDL_KEYUP :
		{
			appEvent.type = AppEvent::KeyRelease;
			appEvent.value.keyboard.keyCode   = sdlKeyCodeMap(sdlEvent.key.keysym);
			appEvent.value.keyboard.modifiers = sdlKeyModsMap(sdlEvent.key.keysym.mod);
			return true;
		}
	case SDL_TEXTINPUT :
		{
			appEvent.type = AppEvent::KeyChar;
			appEvent.value.keyboard.keyCode   = sdlEvent.text.text[0];
			appEvent.value.keyboard.modifiers = 0;
			return true;
		}
	default : // Unhandled SDL event.
		return false;
	} // switch (sdlEvent.type)
}

//
// Local reference to the main render window,
// set when the app enters mainLoop().
//
static SDL_Window * mainWindowRef = nullptr;

} // namespace {}

// ========================================================
// shell::warpSystemCursor():
// ========================================================

void warpSystemCursor(const core::Vec2 & position)
{
	SDL_WarpMouseInWindow(mainWindowRef, position.x, position.y);
}

// ========================================================
// shell::mainLoop():
// ========================================================

void mainLoop(BaseGameApp & appInstance)
{
	SDL_Event sdlEvent;
	AppEvent  appEvent;

	mainWindowRef = reinterpret_cast<SDL_Window *>(
			appInstance.getRenderWindow().getApiObject());

	core::Time deltaTime = core::milliseconds(33); // Assume an initial ~30fps.
	const core::Clock & clock = appInstance.getGlobalClock();

	while (!appInstance.shouldQuit())
	{
		const core::Time t0 = clock.getTime();

		while (SDL_PollEvent(&sdlEvent))
		{
			if (sdlEvent.type == SDL_QUIT)
			{
				appInstance.sendQuitMessage();
				break; // Stop clearing the queue on a quit event.
			}
			else
			{
				if (sdlEventToAppEvent(sdlEvent, appEvent))
				{
					appInstance.onAppEvent(appEvent);
					appEvent.clear();
				}
			}
		}

		appInstance.onFrameUpdate(deltaTime);
		appInstance.onFrameRender(deltaTime);

		const core::Time t1 = clock.getTime();
		deltaTime = t1 - t0;
	}

	mainWindowRef = nullptr;
}

} // namespace shell {}
} // namespace atlas {}
