
// ================================================================================================
// -*- C++ -*-
// File: render_window_sdlgl.hpp
// Author: Guilherme R. Lampert
// Created on: 08/09/15
// Brief: SDL and OpenGL based RenderWindow.
// ================================================================================================

#ifndef ATLAS_SHELL_SDL_RENDER_WINDOW_SDLGL_HPP
#define ATLAS_SHELL_SDL_RENDER_WINDOW_SDLGL_HPP

#if !defined (ATLAS_SHELL_LIB_BACKEND_SDL)
	#error "This file shouldn't be included when not compiling against SDL!"
#endif // ATLAS_SHELL_LIB_BACKEND_SDL

// SDL v2.0
#include "thirdparty/sdl_2_0_3/include/SDL.h"
#include "atlas/shell/render_window.hpp"

namespace atlas
{
namespace shell
{

// ========================================================
// class RenderWindowSDLGL:
// ========================================================

class RenderWindowSDLGL final
	: public RenderWindow
{
public:

	 RenderWindowSDLGL();
	~RenderWindowSDLGL();

	bool createWindow(const core::String & winName,
	                  const RenderContextParams & params,
	                  bool showWin = true) override;

	void destroyWindow()      override;
	void makeContextCurrent() override;
	void swapBuffers()        override;

	void showWindow() override;
	void hideWindow() override;
	bool isWindowHidden() const override;
	void * getApiObject() const override;

private:

	void sdlWinShutdown();

	SDL_Window *  sdlWindow;
	SDL_GLContext sdlGLContext;
	bool hiddenOrMinimized;
};

} // namespace shell {}
} // namespace atlas {}

#endif // ATLAS_SHELL_SDL_RENDER_WINDOW_SDLGL_HPP
