
// ================================================================================================
// -*- C++ -*-
// File: base_game_app.cpp
// Author: Guilherme R. Lampert
// Created on: 08/09/15
// Brief: Base class for the game application.
// ================================================================================================

#include "atlas/shell/base_game_app.hpp"

namespace atlas
{
namespace shell
{

// ========================================================
// BaseGameApp default implementation:
// ========================================================

bool BaseGameApp::instanceCreated = false;

BaseGameApp::BaseGameApp()
{
	DEBUG_CHECK(instanceCreated == false && "Can only have one BaseGameApp!");
	instanceCreated = true;
}

BaseGameApp::~BaseGameApp()
{
	DEBUG_CHECK(instanceCreated == true && "Something wrong here!");
	instanceCreated = false;
}

bool BaseGameApp::onAppEvent(const AppEvent &)
{
	return false;
}

const core::Clock & BaseGameApp::getGlobalClock() const
{
	return globalClock;
}

RenderWindow & BaseGameApp::getRenderWindow()
{
	return renderWindow;
}

void BaseGameApp::onInit() { }
void BaseGameApp::onShutdown() { }

void BaseGameApp::onFrameUpdate(const core::Time &) { }
void BaseGameApp::onFrameRender(const core::Time &) { }

// ========================================================
// commandLine[]/programName[] locals:
// ========================================================

static constexpr int MaxCmdLineChars = 2048;
static char commandLine[MaxCmdLineChars];

static constexpr int MaxProgNameChars = 512;
static char programName[MaxProgNameChars];

// ========================================================
// setAppCommandLine():
// ========================================================

void setAppCommandLine(const int argc, const char * argv[])
{
	if (argc <= 0 || argv == nullptr)
	{
		commandLine[0] = '\0';
		programName[0] = '\0';
		return;
	}

	core::strCopy(programName, MaxProgNameChars, argv[0]);

	char * pCmdLine = commandLine;
	int charsWritten, charsLeft = MaxCmdLineChars;

	// First arg is the program name, as dictated by the C tradition.
	for (int i = 1; i < argc; ++i)
	{
		// Rebuild the command line, spacing each argument.
		charsWritten = core::strPrintF(pCmdLine, charsLeft, "%s ", argv[i]);
		if (charsWritten <= 0)
		{
			break;
		}

		pCmdLine  += charsWritten;
		charsLeft -= charsWritten;
		if (charsLeft <= 0)
		{
			break;
		}
	}

	// Ensure always null terminated.
	commandLine[MaxCmdLineChars - 1] = '\0';
}

// ========================================================
// getAppCommandLine():
// ========================================================

const char * getAppCommandLine()
{
	return commandLine;
}

// ========================================================
// getAppProgramName():
// ========================================================

const char * getAppProgramName()
{
	return programName;
}

} // namespace shell {}
} // namespace atlas {}
