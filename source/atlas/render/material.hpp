
// ================================================================================================
// -*- C++ -*-
// File: material.hpp
// Author: Guilherme R. Lampert
// Created on: 13/08/15
// Brief: Surfaces shading properties and textures.
// ================================================================================================

#ifndef ATLAS_RENDER_MATERIAL_HPP
#define ATLAS_RENDER_MATERIAL_HPP

#include "atlas/render/program.hpp"
#include "atlas/render/texture.hpp"

namespace atlas
{
namespace render
{

// ========================================================
// class Material:
// ========================================================

class Material final
	: private core::NonCopyable
{
public:

private:

	//TODO
};

} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_MATERIAL_HPP
