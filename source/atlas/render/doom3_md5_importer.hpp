
// ================================================================================================
// -*- C++ -*-
// File: doom3_md5_importer.hpp
// Author: Guilherme R. Lampert
// Created on: 14/08/15
// Brief: Importer for Doom 3 MD5 animated models.
// ================================================================================================

#ifndef ATLAS_RENDER_DOOM3_MD5_IMPORTER_HPP
#define ATLAS_RENDER_DOOM3_MD5_IMPORTER_HPP

#include "atlas/render/model3d.hpp"

namespace atlas
{
namespace render
{

// ========================================================
// DOOM3 MD5 model and animation data structures:
// ========================================================

struct DoomMd5Triangle
{
	int index[3];
};

struct DoomMd5Vertex
{
	core::Vec2 texCoords;
	int firstWeight;
	int weightCount;
};

struct DoomMd5Joint
{
	int parent;
	core::Vec3 pos;
	core::Quat orient;
	core::String name;
};

struct DoomMd5Weight
{
	int joint;
	float bias;
	core::Vec3 pos;
};

struct DoomMd5Mesh
{
	core::Array<DoomMd5Vertex>   verts;
	core::Array<DoomMd5Triangle> tris;
	core::Array<DoomMd5Weight>   weights;

	core::String name;
	core::String material;
};

// ========================================================
// class DoomMd5ModelImporter:
// ========================================================

class DoomMd5ModelImporter final
	: private core::NonCopyable
{
public:

	// Sets everything to null/zero. Allocates models/surfaces from provided cache.
	DoomMd5ModelImporter(Model3dCache & mdlCache);

	// Imports a ".md5mesh" file as a static render model.
	// New model is also added to cache if import succeeds.
	const Model3d * importMd5Static(const core::String & filename, core::String modelName);

	// Discard data from a previous model import. This must be called
	// if you plan on reusing the same importer instance. Note that this
	// WILL NOT remove previously imported models from the Model3dCache.
	void clearImportedData();

	// Sum of all vertexes and indexes from previous imports.
	core::uint getVertexCountTotal() const { return totalDrawVerts;   }
	core::uint getIndexCountTotal()  const { return totalDrawIndexes; }

private:

	Model3dCache & modelCache;

	// The data imported from file:
	core::PtrArray<DoomMd5Mesh> md5Meshes;
	core::Array<DoomMd5Joint>   md5Joints;

	// Vertex & index counts from all previous imports.
	// Useful for allocating the Vertex/Index buffers.
	core::uint totalDrawVerts;
	core::uint totalDrawIndexes;

	// Internal helpers:
	bool parseMd5Full(const core::String & filename);
	bool parseMd5Joints(core::Lexer & md5File, int jointCount);
	bool parseMd5Mesh(core::Lexer & md5File, int meshIndex, int jointCount, DoomMd5Mesh & mesh);
};

} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_DOOM3_MD5_IMPORTER_HPP
