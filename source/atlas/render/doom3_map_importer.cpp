
// ================================================================================================
// -*- C++ -*-
// File: doom3_map_importer.cpp
// Author: Guilherme R. Lampert
// Created on: 13/08/15
// Brief: Helper class to import Doom 3 .proc/.map files into our engine.
// ================================================================================================

#include "atlas/render/doom3_map_importer.hpp"
#include "atlas/render/render_log.hpp"

namespace atlas
{
namespace render
{

// ========================================================
// DoomMapImporter::DoomMapImporter():
// ========================================================

DoomMapImporter::DoomMapImporter(Model3dCache & mdlCache)
	: modelCache(mdlCache)
{
	modelsVertexBuffer.usageFlags  = Resource::Flag::UsageStatic;
	modelsVertexBuffer.vertexCount = 0;
	modelsVertexBuffer.indexCount  = 0;
	modelsVertexBuffer.vertexSize  = sizeof(DrawVertex);
	modelsVertexBuffer.indexSize   = sizeof(DrawIndex);
	modelsVertexBuffer.vertexPtr   = nullptr;
	modelsVertexBuffer.indexPtr    = nullptr;
}

// ========================================================
// DoomMapImporter::~DoomMapImporter():
// ========================================================

DoomMapImporter::~DoomMapImporter()
{
	clearImportedData();
}

// ========================================================
// DoomMapImporter::importProcFile():
// ========================================================

bool DoomMapImporter::importProcFile(const core::String & filename)
{
	DEBUG_CHECK(!filename.isEmpty());

	if (!models.isEmpty())
	{
		R_LOG_ERROR("DoomMapImporter: An import is still pending! getImportedData() first!");
		return false;
	}

	R_LOG_INFO("Beginning import of DOOM3 map PROC \"" << filename << "\"...");

	core::Token token;
	core::Lexer procFile(&render::getLog(), render::getLogVerbosityLevel());

	if (!procFile.initFromFile(filename, core::Lexer::Flag::NoStringConcat | core::Lexer::Flag::NoFatalErrors))
	{
		return false;
	}

	if (!procFile.readNextToken(token))
	{
		return procFile.error("Failed to read first token!");
	}
	if (token.getText().compareIgnoreCase("mapProcFile003") != 0)
	{
		return procFile.error("Bad proc id! Expected \'mapProcFile003\', got \'" + token.getText() + "\'.");
	}

	while (procFile.readNextToken(token))
	{
		if (token == "model")
		{
			if (!parseProcModel(procFile))
			{
				return procFile.error("Failed to parse proc \'model\' section!");
			}
		}
		else if (token == "shadowModel")
		{
			R_LOG_INFO("DoomMapImporter: \'shadowModel\' proc section currently not handled. Skipping...");
			procFile.skipBracedSection();
		}
		else if (token == "interAreaPortals")
		{
			R_LOG_INFO("DoomMapImporter: \'interAreaPortals\' proc section currently not handled. Skipping...");
			procFile.skipBracedSection();
		}
		else if (token == "nodes")
		{
			R_LOG_INFO("DoomMapImporter: \'nodes\' proc section currently not handled. Skipping...");
			procFile.skipBracedSection();
		}
		else
		{
			return procFile.error("Unknown proc token/section \'" + token.getText() + "\'!");
		}
	}

	setUpVertexBufferData();

	R_LOG_INFO("PROC file had " << models.getSize() << " render models.");
	R_LOG_INFO("Finished DOOM3 map PROC import.");
	return true;
}

// ========================================================
// DoomMapImporter::getImportedData():
// ========================================================

void DoomMapImporter::getImportedData(Model3dArray & modelsOut, MapVertexBuffer & modelsVertexBufferOut)
{
	models.transferTo(modelsOut);
	modelsVertexBufferOut = modelsVertexBuffer;

	// Now that we've transfered the pointers to modelsVertexBufferOut, these must be cleared!
	modelsVertexBuffer.vertexCount = 0;
	modelsVertexBuffer.indexCount  = 0;
	modelsVertexBuffer.vertexPtr   = nullptr;
	modelsVertexBuffer.indexPtr    = nullptr;
}

// ========================================================
// DoomMapImporter::clearImportedData():
// ========================================================

void DoomMapImporter::clearImportedData()
{
	models.clear();

	delete[] modelsVertexBuffer.vertexPtr;
	delete[] modelsVertexBuffer.indexPtr;

	modelsVertexBuffer.vertexCount = 0;
	modelsVertexBuffer.indexCount  = 0;
	modelsVertexBuffer.vertexPtr   = nullptr;
	modelsVertexBuffer.indexPtr    = nullptr;
}

// ========================================================
// fixTextureCoordinates() [LOCAL]:
// ========================================================

#ifdef ATLAS_DOOM3_MAP_IMPORT_TEXT_COORD_FIX

namespace
{
//
// This Texture coordinate smoothing/averaging was added on
// the Doom 3 BFG edition. The original "classic" Doom 3 code
// didn't have it. The reasons for this addition are not stated
// in the source code. This might have to do with the packing
// of DrawVerts to save memory on the Consoles, which results
// in less precision for the texCoords.
//
// I'm adding it here for future reference...
// The following is basically a copy-paste of the code found on
// neo/renderer/RenderWorld_load.cpp, function idRenderWorldLocal::ParseModel().
//
void fixTextureCoordinates(DrawVertexArray & verts, const DrawIndexArray & indexes)
{
	const core::uint vertexCount = verts.getSize();
	const core::uint indexCount  = indexes.getSize();

	core::U32Array  vertIslands(vertexCount, 0);
	core::BoolArray trisVisited(indexCount, false);

	// Find the island that each vertex belongs to:
	core::uint islandCount = 0;
	for (core::uint j = 0; j < indexCount; j += 3)
	{
		if (trisVisited[j])
		{
			continue;
		}

		const core::uint islandNum = ++islandCount;
		vertIslands[indexes[j + 0]] = islandNum;
		vertIslands[indexes[j + 1]] = islandNum;
		vertIslands[indexes[j + 2]] = islandNum;
		trisVisited[j] = true;

		core::U32Array queue;
		queue.pushBack(j);
		for (core::uint n = 0; n < queue.getSize(); ++n)
		{
			const core::uint t = queue[n];
			for (core::uint k = 0; k < indexCount; k += 3)
			{
				if (trisVisited[k])
				{
					continue;
				}

				const bool connected = (
					indexes[t + 0] == indexes[k + 0] || indexes[t + 0] == indexes[k + 1] || indexes[t + 0] == indexes[k + 2] ||
					indexes[t + 1] == indexes[k + 0] || indexes[t + 1] == indexes[k + 1] || indexes[t + 1] == indexes[k + 2] ||
					indexes[t + 2] == indexes[k + 0] || indexes[t + 2] == indexes[k + 1] || indexes[t + 2] == indexes[k + 2] );

				if (connected)
				{
					vertIslands[indexes[k + 0]] = islandNum;
					vertIslands[indexes[k + 1]] = islandNum;
					vertIslands[indexes[k + 2]] = islandNum;
					trisVisited[k] = true;
					queue.pushBack(k);
				}
			}
		}
	}

	// Center the texture coordinates for each island for maximum 16-bit precision:
	for (core::uint j = 1; j <= islandCount; ++j)
	{
		float minU =  core::math::Infinity;
		float minV =  core::math::Infinity;
		float maxU = -core::math::Infinity;
		float maxV = -core::math::Infinity;

		for (core::uint k = 0; k < vertexCount; ++k)
		{
			if (vertIslands[k] == j)
			{
				minU = core::min(minU, verts[k].texCoords.x);
				maxU = core::max(maxU, verts[k].texCoords.x);
				minV = core::min(minV, verts[k].texCoords.y);
				maxV = core::max(maxV, verts[k].texCoords.y);
			}
		}

		const float averageU = static_cast<int>((minU + maxU) * 0.5f);
		const float averageV = static_cast<int>((minV + maxV) * 0.5f);

		for (core::uint k = 0; k < vertexCount; ++k)
		{
			if (vertIslands[k] == j)
			{
				verts[k].texCoords.x -= averageU;
				verts[k].texCoords.y -= averageV;
			}
		}
	}
}

} // namespace  {}

#endif // ATLAS_DOOM3_MAP_IMPORT_TEXT_COORD_FIX

// ========================================================
// DoomMapImporter::parseProcModel():
// ========================================================

bool DoomMapImporter::parseProcModel(core::Lexer & procFile)
{
	//
	// Parse a 'model { }' braced section from the .proc file.
	//

	core::Token token;
	Model3dSurfaceArray modelSurfaces;
	core::String modelName, materialName, surfaceName;

	if (!procFile.expectTokenString("{")) { return false; }

	// Model name:
	if (!procFile.readNextToken(token))
	{
		return procFile.error("Failed to get proc model name/id!");
	}
	token.transferTo(modelName);

	const int modelSurfaceCount = procFile.scanInt();
	if (modelSurfaceCount <= 0)
	{
		// Looks like some clip models might have zero surfaces after all...
		procFile.expectTokenString("}");
		return true;
	}

	// Model surfaces/sub-meshes:
	for (int s = 0; s < modelSurfaceCount; ++s)
	{
		// Brace opening the surface { } section:
		if (!procFile.expectTokenString("{")) { return false; }

		// Material name (a file path without extension):
		if (!procFile.readNextToken(token))
		{
			return procFile.error("Failed to get proc model material!");
		}
		token.transferTo(materialName);

		const int vertexCount = procFile.scanInt();
		const int indexCount  = procFile.scanInt();

		if (vertexCount <= 0 || indexCount <= 0)
		{
			return procFile.error("Invalid proc model surface vertex/index counts: v="
					+ core::toString(vertexCount) + ", i=" + core::toString(indexCount));
		}

		// Vertex list follows:
		DrawVertexArray vertexes;
		vertexes.allocateExact(vertexCount);
		for (int v = 0; v < vertexCount; ++v)
		{
			float vert[8];
			procFile.scanFloatMatrix1(8, vert);

			DrawVertex drawVert;
			drawVert.position.x  = vert[0];
			drawVert.position.y  = vert[1];
			drawVert.position.z  = vert[2];
			drawVert.texCoords.x = vert[3];
			drawVert.texCoords.y = vert[4];
			drawVert.normal.x    = vert[5];
			drawVert.normal.y    = vert[6];
			drawVert.normal.z    = vert[7];
			vertexes.pushBack(drawVert);
		}

		const core::uint baseVertex = modelsVertexBuffer.vertexCount;
		modelsVertexBuffer.vertexCount += vertexCount; // Remember this to allocate the Vertex Buffer later.

		// Then the index list:
		DrawIndexArray indexes;
		indexes.allocateExact(indexCount);
		for (int i = 0; i < indexCount; ++i)
		{
			indexes.pushBack(static_cast<DrawIndex>(procFile.scanInt()));
		}

		const core::uint baseIndex = modelsVertexBuffer.indexCount;
		modelsVertexBuffer.indexCount += indexCount; // Remember this to allocate the Index Buffer later.

		#ifdef ATLAS_DOOM3_MAP_IMPORT_TEXT_COORD_FIX
		fixTextureCoordinates(vertexes, indexes);
		#endif // ATLAS_DOOM3_MAP_IMPORT_TEXT_COORD_FIX

		// Surface is finished. Store it:
		surfaceName = "surface-" + core::toString(s);
		modelSurfaces.pushBack(
			modelCache.createSurface(surfaceName, materialName,
			                         vertexes,    indexes,
			                         baseVertex,  baseIndex));

		// Brace closing the surface { } section:
		if (!procFile.expectTokenString("}")) { return false; }
	}

	if (!procFile.expectTokenString("}")) { return false; }

	// Valid model, if we get here.
	models.pushBack(modelCache.createStaticModel(modelName, modelSurfaces));
	return true;
}

// ========================================================
// DoomMapImporter::setUpVertexBufferData():
// ========================================================

void DoomMapImporter::setUpVertexBufferData()
{
	DEBUG_CHECK(modelsVertexBuffer.vertexCount != 0);
	DEBUG_CHECK(modelsVertexBuffer.indexCount  != 0);
	DEBUG_CHECK(modelsVertexBuffer.vertexPtr == nullptr);
	DEBUG_CHECK(modelsVertexBuffer.indexPtr  == nullptr);

	// Allocate our temporary system-side Vertex/Index
	// buffers that will contain all the map geometry:
	modelsVertexBuffer.vertexPtr = new DrawVertex[modelsVertexBuffer.vertexCount];
	modelsVertexBuffer.indexPtr  = new DrawIndex [modelsVertexBuffer.indexCount];

	DrawVertex * RESTRICT pVert = modelsVertexBuffer.vertexPtr;
	DrawIndex  * RESTRICT pIdx  = modelsVertexBuffer.indexPtr;

	const core::uint modelCount = models.getSize();
	for (core::uint m = 0; m < modelCount; ++m)
	{
		const Model3d & model = *models[m];
		const core::uint surfaceCount = model.getSurfaceCount();

		for (core::uint s = 0; s < surfaceCount; ++s)
		{
			const Model3dSurface & surface   = model.getSurface(s);
			const core::uint surfVertexCount = surface.getVertexCount();
			const core::uint surfIndexCount  = surface.getIndexCount();

			std::memcpy(pVert, surface.getVertexData(), surfVertexCount * sizeof(DrawVertex));
			pVert += surfVertexCount;

			std::memcpy(pIdx, surface.getIndexData(), surfIndexCount * sizeof(DrawIndex));
			pIdx += surfIndexCount;
		}
	}
}

} // namespace render {}
} // namespace atlas {}
