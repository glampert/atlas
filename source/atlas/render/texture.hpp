
// ================================================================================================
// -*- C++ -*-
// File: texture.hpp
// Author: Guilherme R. Lampert
// Created on: 04/07/15
// Brief: Common Texture type.
// ================================================================================================

#ifndef ATLAS_RENDER_TEXTURE_HPP
#define ATLAS_RENDER_TEXTURE_HPP

#include "atlas/render/resource.hpp"
#include "atlas/core/memory/object_pool.hpp"

namespace atlas
{
namespace render
{

// ========================================================
// class Texture:
// ========================================================

class Texture final
	: public Resource
	, public core::HashTableNode<Texture>
{
public:

	struct Target
	{
		enum Enum
		{
			Default,         // Defaulted texture due to load error or because it was never loaded.
			DiffuseMap,      // 2D diffuse texture map, or "color map".
			NormalMap,       // 2D texture that encodes surface normals. A.K.A. Normal Map.
			SpecularMap,     // 2D texture that encodes surface specularity. A.K.A. Specular Map.
			AlphaMap,        // 2D Texture used as an opacity mask when rendering the surface.
			HeightMap,       // Texture is a height map. Might also be interpreted as a normal map in some cases.
			EmissiveMap,     // Emissive map. Texture color is added to the final surface color for a glow effect.
			CubeMap,         // 6-sided cubemap texture. Used for skyboxes and environment mapping.
			DepthBuffer,     // Depth renderable texture. Used for shadow mapping and off-screen rendering.
			StencilBuffer,   // Stencil buffer texture. NOTE: Not available on all platforms.
			DepthStencil,    // Combined depth and stencil buffer texture. NOTE: Not available on all platforms.
			Renderer,        // Texture used internally by the renderer for render targets and scratch data.

			Count            // Number of entries in this enum. Internal use.
		};
	};

	struct Filter
	{
		enum Enum
		{
			Default,         // Current default filter defined by the renderer.
			Nearest,         // Nearest neighbor (or Manhattan Distance) filtering. Worst quality, best performance.
			Bilinear,        // Cheap bi-linear filtering. Low quality but good performance.
			Trilinear,       // Intermediate tri-linear filtering. Reasonable quality, average performance.
			Anisotropic,     // Anisotropic filtering. Best quality, most expensive. Paired with an anisotropy amount.

			Count            // Number of entries in this enum. Internal use.
		};
	};

	struct Addressing
	{
		enum Enum
		{
			Default,         // Default addressing mode. Same as `Repeat`.
			ClampToEdge,     // Clamp (or stretch) texture to fill surface using the color of the edge pixel.
			ClampToBlack,    // Clamp (or stretch) texture to fill surface filling the stretched part with opaque black.
			ClampToWhite,    // Clamp (or stretch) texture to fill surface filling the stretched part with opaque white.
			ClampToAlpha0,   // Clamp (or stretch) texture to fill surface filling the stretched part with transparency (0,0,0,0)
			Repeat,          // Repeat texture to fill surface.
			RepeatMirrored,  // Repeat texture to fill surface, but mirrored.

			Count            // Number of entries in this enum. Internal use.
		};
	};

	struct MappingUnit
	{
		enum Enum
		{
			// Expect the hardware to
			// have at least 10 TMUs.
			TMU0,
			TMU1,
			TMU2,
			TMU3,
			TMU4,
			TMU5,
			TMU6,
			TMU7,
			TMU8,
			TMU9,
			// Number of entries in this enum. Internal use.
			Count
		};
	};

	// Inherit from PixelFormat so we can use this enum-struct like the previous ones.
	struct DataLayout final
		: public core::PixelFormat
	{
		typedef core::PixelFormat::DataLayout Enum;
	};

	// Same max used by Image, which is around 16.
	static constexpr core::uint MaxMipmapSurfaces = core::Image::MaxSurfaces;

public:

	Texture();

	bool isCompressed() const;
	bool isPowerOfTwo() const;
	bool isDirty()      const;
	bool isValid()      const;

	core::uint  getWidth(core::uint  surface = 0)     const;
	core::uint  getHeight(core::uint surface = 0)     const;
	core::Vec2u getDimensions(core::uint surface = 0) const;

	Target::Enum getTarget()         const;
	Filter::Enum getFilter()         const;
	Addressing::Enum getAddressing() const;
	MappingUnit::Enum getTexUnit()   const;

	core::uint getAnisotropy()       const;
	core::uint getMipmapCount()      const;
	core::uint getFirstMipmapIndex() const;
	core::uint getLastMipmapIndex()  const;
	float getMipmapLodBias()         const;

	DataLayout::Enum getDataLayout() const;
	core::uint getBytesPerPixel()    const;
	core::uint getChannelCount()     const;
	core::uint getMemoryBytes()      const;
	const core::String & getName()   const;

	void setTexUnit(MappingUnit::Enum newTexUnit);
	void setTarget(Target::Enum newTarget);
	void setFilter(Filter::Enum newFilter);
	void setAddressing(Addressing::Enum newAddressing);
	void setAnisotropy(core::uint newAnisotropy);
	void setFirstMipmapIndex(core::uint newIndex);
	void setLastMipmapIndex(core::uint newIndex);
	void setMipmapLodBias(float newBias);
	void setName(const core::String & name);

private:

	friend RhiManager;

	// The OpenGL texture name/handle.
	GLuint texHandleGL;

	// Notes:
	//
	// 1) Due to our "dirty flag" setup, accessing texture parameters
	// should always be done using the provided get/set methods,
	// even for in-class usage. Reading is OK if done by directly
	// accessing the variables, but writing MUST always be done
	// using the appropriate set*() method, to ensure the dirty
	// flag get updated accordingly.
	//
	// 2) We use explicitly sized types in here instead of the enum
	// types to save as much memory as possible and improve data cache usage.
	//
	core::Vec2u   dimensions;  // Width and height in pixels of the base mipmap (surface=0).
	core::uint32  memoryUsage; // Estimate bytes used to store this texture (including mipmaps).
	core::float32 mipLodBias;  // Mipmap/LOD selection bias.
	core::uint8   target;      // Target usage, e.g.: 2D, normal-map, cube-map, etc.
	core::uint8   filter;      // Sampling filter. If anisotropic, paired with `anisotropy` amount.
	core::uint8   addressing;  // Addressing or wrapping mode.
	core::uint8   texUnit;     // Texture mapping unit (TMU) index.
	core::uint8   dataLayout;  // Layout of the underlaying pixel data, AKA the pixel format.
	core::uint8   anisotropy;  // Anisotropy amount if using anisotropic filter, zero otherwise.
	core::uint8   mipmapCount; // Total mipmaps available. From 1 to `MaxMipmapSurfaces` (inclusive).
	core::uint8   firstMipmap; // Base (lowest) mipmap surface to use when rendering. From 0 to `mipmapCount - 1`.
	core::uint8   lastMipmap;  // Max mipmap surface to use when rendering. From 0 to `mipmapCount - 1`.

	// "Dirty flags" are set when the corresponding
	// texture parameter is changed, meaning that it
	// must be re-submitted to the back-end RHI the
	// next time this texture is used for rendering.
	// Use a bitfield to save space. We don't need
	// a full byte per flag, but rather, just one bit.
	struct DirtyFlags
	{
		bool filter      : 1;
		bool addressing  : 1;
		bool anisotropy  : 1;
		bool firstMipmap : 1;
		bool lastMipmap  : 1;
		bool mipLodBias  : 1;
	};
	DirtyFlags dirtyFlags;

	// Name of this texture, which is the filename if this
	// texture was loaded from an image file. Some other user
	// supplied name if the texture was dynamically generated.
	core::String texName;
};

// ========================================================
// class TextureCache:
// ========================================================

// Supply a reasonable default if missing.
#ifndef TEXTURE_CACHE_MEMPOOL_GRANULARITY
	#define TEXTURE_CACHE_MEMPOOL_GRANULARITY 256
#endif // TEXTURE_CACHE_MEMPOOL_GRANULARITY

// Simple texture cache that retains ownership of every texture.
// Textures are indexed by filename, so no texture is loaded more
// than once. The cache frees all textures in its destructor.
class TextureCache final
	: private core::NonCopyable
{
public:

	 TextureCache(RhiManager & rhiMgr);
	~TextureCache();

	// Find or load a new texture. If an error occurs/the file can't be found,
	// this returns a default texture and sets the `isDefaulted` error flag if
	// the pointer was not null. Further error info is printed to the render log.
	const Texture * find(const core::String & texName, bool * isDefaulted = nullptr);

	// Get the default texture for the given target (or guess target from filename).
	// The default diffuse texture is an easy to spot checkerboard pattern.
	const Texture * getDefaultTexture(Texture::Target::Enum target);
	const Texture * getDefaultTexture(const core::String & texName);

	// This forces the cache to free every single texture.
	// Calling this will method will invalidate every pointer
	// previously handed out by the cache, so use it with caution!
	void freeAllTextures();

	core::uint getSize() const { return loadedTextures.getSize(); }
	bool isEmpty()       const { return loadedTextures.isEmpty(); }

private:

	static Texture::Target::Enum guessTexTargetFromName(const core::String & texName);

	// Back-end texture memory manager.
	RhiManager & rhiManager;

	// Texture table indexed by name:
	typedef core::IntrusiveHashTable<core::String, Texture> TextureTable;
	TextureTable loadedTextures;

	// The backing store of the texture table.
	// All pointers are allocated from this object pool.
	typedef core::ObjectPool<Texture, TEXTURE_CACHE_MEMPOOL_GRANULARITY> TextureMemPool;
	TextureMemPool memPool;

	// Even though not a traditional singleton (to avoid global access),
	// only one instance of a TextureCache is allowed for the whole engine.
	static bool instanceCreated;
};

// ========================================================
// Debug printing helpers:
// ========================================================

core::String toString(Texture::DataLayout::Enum layout);
core::String toString(Texture::Target::Enum target);
core::String toString(Texture::Filter::Enum filter);
core::String toString(Texture::Addressing::Enum addressing);
core::String toString(Texture::MappingUnit::Enum texUnit);

// ================================================================================================
// Texture inline methods:
// ================================================================================================

inline core::Vec2u Texture::getDimensions(const core::uint surface) const
{
	return core::Vec2u(getWidth(surface), getHeight(surface));
}

inline Texture::Target::Enum Texture::getTarget() const
{
	return static_cast<Target::Enum>(target);
}

inline Texture::Filter::Enum Texture::getFilter() const
{
	return static_cast<Filter::Enum>(filter);
}

inline Texture::Addressing::Enum Texture::getAddressing() const
{
	return static_cast<Addressing::Enum>(addressing);
}

inline Texture::MappingUnit::Enum Texture::getTexUnit() const
{
	return static_cast<MappingUnit::Enum>(texUnit);
}

inline core::uint Texture::getAnisotropy() const
{
	return anisotropy;
}

inline core::uint Texture::getMipmapCount() const
{
	return mipmapCount;
}

inline core::uint Texture::getFirstMipmapIndex() const
{
	return firstMipmap;
}

inline core::uint Texture::getLastMipmapIndex() const
{
	return lastMipmap;
}

inline float Texture::getMipmapLodBias() const
{
	return mipLodBias;
}

inline Texture::DataLayout::Enum Texture::getDataLayout() const
{
	return static_cast<DataLayout::Enum>(dataLayout);
}

inline core::uint Texture::getBytesPerPixel() const
{
	return core::PixelFormat::fromLayout(getDataLayout()).getBytesPerPixel();
}

inline core::uint Texture::getChannelCount() const
{
	return core::PixelFormat::fromLayout(getDataLayout()).getChannelCount();
}

inline core::uint Texture::getMemoryBytes() const
{
	return memoryUsage + sizeof(Texture);
}

inline const core::String & Texture::getName() const
{
	return texName;
}

inline void Texture::setName(const core::String & name)
{
	texName = name;
}

} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_TEXTURE_HPP
