
// ================================================================================================
// -*- C++ -*-
// File: texture.cpp
// Author: Guilherme R. Lampert
// Created on: 14/08/15
// Brief: Common Texture type.
// ================================================================================================

#include "atlas/render/texture.hpp"
#include "atlas/render/renderer.hpp"
#include "atlas/render/render_log.hpp"

namespace atlas
{
namespace render
{

// ================================================================================================
// Texture class implementation:
// ================================================================================================

// ========================================================
// Texture::Texture():
// ========================================================

Texture::Texture()
	: Resource(Resource::Flag::TypeTexture)
	, texHandleGL(0)
	, dimensions(0,0)
	, memoryUsage(0)
	, mipLodBias(0.0)
	, target(Target::Default)
	, filter(Filter::Default)
	, addressing(Addressing::Default)
	, texUnit(MappingUnit::TMU0)
	, dataLayout(DataLayout::Null)
	, anisotropy(0)
	, mipmapCount(0)
	, firstMipmap(0)
	, lastMipmap(0)
{
	core::clearPodObject(dirtyFlags);
}

// ========================================================
// Texture::isCompressed():
// ========================================================

bool Texture::isCompressed() const
{
	return core::PixelFormat::fromLayout(getDataLayout()).isCompressed();
}

// ========================================================
// Texture::isPowerOfTwo():
// ========================================================

bool Texture::isPowerOfTwo() const
{
	// This tests the base mipmap surface. If surface=0
	// is PoT, then all child surfaces should also be.
	return core::math::isPowerOfTwo(dimensions.x) &&
	       core::math::isPowerOfTwo(dimensions.y);
}

// ========================================================
// Texture::isDirty():
// ========================================================

bool Texture::isDirty() const
{
	return (dirtyFlags.filter      || dirtyFlags.addressing || dirtyFlags.anisotropy ||
	        dirtyFlags.firstMipmap || dirtyFlags.lastMipmap || dirtyFlags.mipLodBias);
}

// ========================================================
// Texture::isValid():
// ========================================================

bool Texture::isValid() const
{
	return (texHandleGL  != 0) &&
	       (dimensions.x != 0) && (dimensions.y != 0) &&
	       (mipmapCount  != 0) && (dataLayout   != 0) &&
	       (resourceFlags & Resource::Flag::TypeTexture);
}

// ========================================================
// Texture::getWidth():
// ========================================================

core::uint Texture::getWidth(const core::uint surface) const
{
	DEBUG_CHECK(surface < mipmapCount);
	DEBUG_CHECK(dimensions.x > 0);

	core::uint w = dimensions.x;
	for (int i = surface; i > 0; --i)
	{
		w = w / 2;
		if (w == 0)
		{
			w = 1;
			break;
		}
	}
	return w;
}

// ========================================================
// Texture::getHeight():
// ========================================================

core::uint Texture::getHeight(const core::uint surface) const
{
	DEBUG_CHECK(surface < mipmapCount);
	DEBUG_CHECK(dimensions.y > 0);

	core::uint h = dimensions.y;
	for (int i = surface; i > 0; --i)
	{
		h = h / 2;
		if (h == 0)
		{
			h = 1;
			break;
		}
	}
	return h;
}

// ========================================================
// Texture::setTexUnit():
// ========================================================

void Texture::setTexUnit(const MappingUnit::Enum newTexUnit)
{
	// No need to keep a dirty flag for this value.
	// It is only used when we need to bind a texture.
	texUnit = static_cast<core::uint8>(newTexUnit);
}

// ========================================================
// Texture::setTarget():
// ========================================================

void Texture::setTarget(const Target::Enum newTarget)
{
	// No need to keep a dirty flag for this value.
	// It is only used when we need to bind a texture.
	target = static_cast<core::uint8>(newTarget);
}

// ========================================================
// Texture::setFilter():
// ========================================================

void Texture::setFilter(const Filter::Enum newFilter)
{
	if (filter == newFilter) { return; }
	filter = static_cast<core::uint8>(newFilter);
	dirtyFlags.filter = true;
}

// ========================================================
// Texture::setAddressing():
// ========================================================

void Texture::setAddressing(const Addressing::Enum newAddressing)
{
	if (addressing == newAddressing) { return; }
	addressing = static_cast<core::uint8>(newAddressing);
	dirtyFlags.addressing = true;
}

// ========================================================
// Texture::setAnisotropy():
// ========================================================

void Texture::setAnisotropy(const core::uint newAnisotropy)
{
	DEBUG_CHECK(newAnisotropy <= 255);

	if (anisotropy == newAnisotropy) { return; }
	anisotropy = static_cast<core::uint8>(newAnisotropy);
	dirtyFlags.anisotropy = true;
}

// ========================================================
// Texture::setFirstMipmapIndex():
// ========================================================

void Texture::setFirstMipmapIndex(const core::uint newIndex)
{
	DEBUG_CHECK(newIndex < MaxMipmapSurfaces);
	DEBUG_CHECK(newIndex < mipmapCount);

	if (firstMipmap == newIndex) { return; }
	firstMipmap = static_cast<core::uint8>(newIndex);
	dirtyFlags.firstMipmap = true;
}

// ========================================================
// Texture::setLastMipmapIndex():
// ========================================================

void Texture::setLastMipmapIndex(const core::uint newIndex)
{
	DEBUG_CHECK(newIndex < MaxMipmapSurfaces);
	DEBUG_CHECK(newIndex < mipmapCount);

	if (lastMipmap == newIndex) { return; }
	lastMipmap = static_cast<core::uint8>(newIndex);
	dirtyFlags.lastMipmap = true;
}

// ========================================================
// Texture::setMipmapLodBias():
// ========================================================

void Texture::setMipmapLodBias(const float newBias)
{
	if (mipLodBias == newBias) { return; }
	mipLodBias = newBias;
	dirtyFlags.mipLodBias = true;
}

// ================================================================================================
// TextureCache class implementation:
// ================================================================================================

bool TextureCache::instanceCreated = false;

// ========================================================
// TextureCache::TextureCache():
// ========================================================

TextureCache::TextureCache(RhiManager & rhiMgr)
	: rhiManager(rhiMgr)
	, loadedTextures(/* allowDuplicateKeys = */ false)
{
	DEBUG_CHECK(!instanceCreated && "Only one instance of TextureCache is allowed!");
	instanceCreated = true;
}

// ========================================================
// TextureCache::~TextureCache():
// ========================================================

TextureCache::~TextureCache()
{
	DEBUG_CHECK(instanceCreated && "This flag should be set!");
	freeAllTextures();
	instanceCreated = false;
}

// ========================================================
// TextureCache::freeAllTextures():
// ========================================================

void TextureCache::freeAllTextures()
{
	R_LOG_INFO("---- TextureCache::freeAllTextures() ----");

	struct TextureDeallocator
	{
		TextureMemPool & memPool;
		TextureDeallocator(TextureMemPool & pool) : memPool(pool) { }

		void operator() (Texture * ptr)
		{
			memPool.destroy(ptr);
			memPool.deallocate(ptr);
		}
	} deallocator(memPool);

	loadedTextures.clearWithFunction(deallocator);
	memPool.drain();
}

// ========================================================
// TextureCache::find():
// ========================================================

const Texture * TextureCache::find(const core::String & texName, bool * isDefaulted)
{
	DEBUG_CHECK(!texName.isEmpty());

	Texture * texture = loadedTextures.find(texName);
	if (texture != nullptr)
	{
		if (isDefaulted != nullptr)
		{
			*isDefaulted = !(texture->getResourceFlags() & Resource::Flag::DefaultedResource);
		}
		return texture;
	}

	core::ImageLoadParams params;
	params.forceRgba             = false;
	params.flipVerticallyOnLoad  = false;
	params.roundDownToPowerOfTwo = true;
	params.generateMipmaps       = true;

	core::Image img;
	if (!img.initFromFile(texName, params))
	{
		R_LOG_ERROR("TextureCache failed to load image \"" << texName << "\". Returning a default...");

		if (isDefaulted != nullptr) { *isDefaulted = true; }
		return getDefaultTexture(texName);
	}

	texture = memPool.allocate();
	memPool.construct(texture);

	if (!rhiManager.allocateTexture(*texture, img,
	    Resource::Flag::UsageStatic, guessTexTargetFromName(texName), true))
	{
		memPool.destroy(texture);
		memPool.deallocate(texture);

		R_LOG_ERROR("TextureCache failed allocate texture for \"" << texName << "\". Returning a default...");

		if (isDefaulted != nullptr) { *isDefaulted = true; }
		return getDefaultTexture(texName);
	}

	// Successfully loaded new image and allocated new texture. Add to the cache.
	texture->setName(texName);
	if (!loadedTextures.insert(texName, texture))
	{
		R_LOG_FATAL("Duplicate key in TextureCache hash-table: " << texName);
	}

	R_LOG_INFO("TextureCache loaded new texture \"" << texName << "\".");

	if (isDefaulted != nullptr) { *isDefaulted = false; }
	return texture;
}

// ========================================================
// TextureCache::getDefaultTexture():
// ========================================================

const Texture * TextureCache::getDefaultTexture(const Texture::Target::Enum target)
{
	//TODO
	(void)target;
	return nullptr;
}

// ========================================================
// TextureCache::getDefaultTexture():
// ========================================================

const Texture * TextureCache::getDefaultTexture(const core::String & texName)
{
	return getDefaultTexture(guessTexTargetFromName(texName));
}

// ========================================================
// TextureCache::guessTexTargetFromName():
// ========================================================

Texture::Target::Enum TextureCache::guessTexTargetFromName(const core::String & texName)
{
	//TODO
	(void)texName;
	return Texture::Target::DiffuseMap;
}

} // namespace render {}
} // namespace atlas {}
