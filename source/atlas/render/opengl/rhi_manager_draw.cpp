
// ================================================================================================
// -*- C++ -*-
// File: rhi_manager_draw.cpp
// Author: Guilherme R. Lampert
// Created on: 20/07/15
// Brief: RhiManagerImpl methods that expose the supported OpenGL draw calls.
// ================================================================================================

#include "atlas/render/opengl/rhi_manager.hpp"
#include "atlas/render/render_log.hpp"

namespace atlas
{
namespace render
{
namespace ogl
{

// ================================================================================================
// ogl::RhiManagerImpl -- OpenGL draw calls.
// ================================================================================================

// ========================================================
// Local helper functions:
// ========================================================

namespace
{

inline GLenum getGLRenderMode(const RenderMode::Enum mode)
{
	switch (mode)
	{
	case RenderMode::Triangles : return GL_TRIANGLES;
	case RenderMode::Points    : return GL_POINTS;
	case RenderMode::LineLoop  : return GL_LINE_LOOP;
	case RenderMode::LineStip  : return GL_LINE_STRIP;
	case RenderMode::Lines     : return GL_LINES;
	default : R_LOG_FATAL("Invalid render mode!");
	} // switch (mode)
}

inline GLenum getGLTypeForIndexSize(const core::uint sizeOfAnIndex)
{
	switch (sizeOfAnIndex)
	{
	case 1 : return GL_UNSIGNED_BYTE;
	case 2 : return GL_UNSIGNED_SHORT;
	case 4 : return GL_UNSIGNED_INT;
	default : R_LOG_FATAL("Unsupported index size!");
	} // switch (sizeOfAnIndex)
}

} // namespace {}

// ========================================================
// RhiManagerImpl::drawVertexBufferUnindexed():
// ========================================================

void RhiManagerImpl::drawVertexBufferUnindexed(const RenderMode::Enum mode,
                                               const core::uint firstVertex,
                                               const core::uint vertexCount,
                                               const core::uint sizeOfAVertex)
{
	DEBUG_CHECK(isInitialized());
	DEBUG_CHECK(currentGLProg != 0);
	DEBUG_CHECK(currentGLVao  != 0);

	DEBUG_CHECK(vertexCount   != 0);
	DEBUG_CHECK(sizeOfAVertex != 0);
//	DEBUG_CHECK(firstVertex < vertexCount);

	#ifdef ATLAS_RELEASE
	(void)sizeOfAVertex;
	#endif // ATLAS_RELEASE

	//TODO glValidateProgram on debug mode?
	// perhaps also a extraDrawCallValidation flag to enable glGetError after each draw?

	glDrawArrays(getGLRenderMode(mode), firstVertex, vertexCount);
}

// ========================================================
// RhiManagerImpl::drawVertexBufferIndexed():
// ========================================================

void RhiManagerImpl::drawVertexBufferIndexed(const RenderMode::Enum mode,
                                             const core::uint firstIndex,
                                             const core::uint indexCount,
                                             const core::uint sizeOfAnIndex)
{
	DEBUG_CHECK(isInitialized());
	DEBUG_CHECK(currentGLProg != 0);
	DEBUG_CHECK(currentGLVao  != 0);

	DEBUG_CHECK(indexCount    != 0);
	DEBUG_CHECK(sizeOfAnIndex != 0);

	//TODO glValidateProgram on debug mode?
	// perhaps also a extraDrawCallValidation flag to enable glGetError after each draw?

	const GLenum glRenderMode = getGLRenderMode(mode);
	const GLenum glIndexType  = getGLTypeForIndexSize(sizeOfAnIndex);
	const std::uintptr_t offsetBytes = firstIndex * sizeOfAnIndex;

	glDrawElements(glRenderMode, indexCount, glIndexType,
		reinterpret_cast<const GLvoid *>(offsetBytes));
}

// ========================================================
// RhiManagerImpl::drawVertexBufferIndexedBaseVertex():
// ========================================================

void RhiManagerImpl::drawVertexBufferIndexedBaseVertex(const RenderMode::Enum mode,
                                                       const core::uint firstIndex,
                                                       const core::uint indexCount,
                                                       const core::uint sizeOfAnIndex,
                                                       const core::uint baseVertex)
{
	DEBUG_CHECK(isInitialized());
	DEBUG_CHECK(currentGLProg != 0);
	DEBUG_CHECK(currentGLVao  != 0);

	DEBUG_CHECK(indexCount    != 0);
	DEBUG_CHECK(sizeOfAnIndex != 0);

	//TODO glValidateProgram on debug mode?
	// perhaps also a extraDrawCallValidation flag to enable glGetError after each draw?

	const GLenum glRenderMode = getGLRenderMode(mode);
	const GLenum glIndexType  = getGLTypeForIndexSize(sizeOfAnIndex);
	const std::uintptr_t offsetBytes = firstIndex * sizeOfAnIndex;

	glDrawElementsBaseVertex(glRenderMode, indexCount, glIndexType,
		reinterpret_cast<const GLvoid *>(offsetBytes), baseVertex);
}

} // namespace ogl {}
} // namespace render {}
} // namespace atlas {}
