
// ================================================================================================
// -*- C++ -*-
// File: glsl_preprocessor.hpp
// Author: Guilherme R. Lampert
// Created on: 05/08/15
// Brief: A tiny GLSL preprocessor that resolves shader includes in a portable way.
// ================================================================================================

#ifndef ATLAS_RENDER_OPENGL_GLSL_PREPROCESSOR_HPP
#define ATLAS_RENDER_OPENGL_GLSL_PREPROCESSOR_HPP

// For the ProgramStage structure.
#include "atlas/render/program.hpp"

// For ZipReader/FileSystem/IntrusiveSList.
#include "atlas/core/containers/intrusive_slist.hpp"
#include "atlas/core/fs.hpp"

namespace atlas
{
namespace render
{
namespace ogl
{

// ========================================================
// class SimpleGlslPreprocessor:
// ========================================================

// Small GLSL preprocessor that does #include resolution and
// allows appending default definitions to shader source files.
//
// Currently, it only provides support for non-reentrant includes,
// so the included files cannot themselves include other files.
//
// Default definitions are added first in the output, followed
// by shader-specific definitions, then any include files found.
//
// The goal of this preprocessor is to keep memory allocations to
// a minimum, however, it will still allocate a buffer for each
// opened file and another output buffer for the preprocessed
// output file, which is at least as big as the original file plus
// all the included shaders and definitions. Other small allocations
// might also take place to store filename/path strings during the
// processing of files. Therefore, it is recommended to do this
// preprocessing of shader files as an offline step and store the
// resulting preprocessed GLSL in another file ready to use.
//
// You can also enable a simple minification of the output by defining
// the C++ macro `ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS`.
// If this macro is defined, the output preprocessed shader will have
// its trailing white spaces and blank lines removed. Lines starting
// with a C or C++ style comment are also stripped. This can make the
// output slightly smaller, saving some memory at the cost of a little
// extra processing.
//
class SimpleGlslPreprocessor final
	: private core::NonCopyable
{
public:

	// Construct a preprocessor that loads shaders from the local file system.
	SimpleGlslPreprocessor();

	// Construct a preprocessor that loads shaders from a zip archive.
	SimpleGlslPreprocessor(const core::ZipReader & zipReader);

	// Performs any necessary cleanup.
	~SimpleGlslPreprocessor();

	// Set the include path. This is initially an empty string. Input may
	// be null or an empty string to clear the default include path.
	void setIncludePath(const char * path);

	// Same as `setIncludePath()` but affects only the shaders loaded by `loadShader()`.
	void setShaderPath(const char * path);

	// Add a string that gets appended to the beginning of every shader.
	// Strings are appended in the order they get added, oldest at the top.
	void addDefaultDefinition(const char * text);

	// Undo any previous addDefaultDefinition()s (removes all default definitions).
	void clearDefaultDefinitions();

	// Preload specific include file. Will append the default include path, if any.
	bool preloadShaderInclude(const core::String & filename);

	// Scans the directory at the end of `path` to load all shader include files.
	// The default include path is append to `path` if it is set. Returns the number of files loaded.
	int preloadShaderIncludesInPath(const core::String & path, bool recursiveScan);

	// Frees all preloaded shader include files currently in memory.
	void clearPreloadedIncludes();

	// Loads the given shader file and performs any available preprocessing on it,
	// like #include resolution. The `optionalDefinitions` may contain an array of
	// strings that are appended to the beginning of this shader only. This array is
	// terminated by a nullptr. It is an optional param, so the pointer itself may
	// be null. If `keepIncludes` is set, any new #include files opened are kept in cache
	// after this method returns, as if they were loaded by `preloadShaderInclude()`.
	bool loadShader(core::ByteArray & shaderSourceOut, ProgramStage & programStageOut,
	                const core::String & filename, const char ** optionalDefinitions,
	                bool keepIncludes);

	// Prints a list of the currently loaded shader include files to the log output.
	void logPreloadedIncludes(core::LogStream & logStr) const;

private:

	class ShaderFile final
		: public core::SListNode<ShaderFile>
	{
	public:
		core::ByteArray fileContents;
		core::uint32    filenameHash;
		core::String    filename;
	};

	typedef core::IntrusiveSList<ShaderFile> ShaderFileList;

	const ShaderFile * findShaderInclude(const core::String & filename) const;
	bool loadShaderInclude(const core::String & filename, ShaderFile * optionalDestFile);

	bool resolveIncludesAndDefs(core::ByteArray & shaderSourceOut, const core::String & filename,
	                            const char ** optionalDefinitions, bool keepIncludes);

	bool loadFileHelper(const core::String & filename,
	                    core::ByteArray & fileContents,
	                    core::ErrorInfo * errorInfo) const;

private:

	// Zip archive to load shaders from. If null load from the local FS.
	// This is a pointer to an external objects, assumed to remain valid
	// for the lifetime of the preprocessor.
	const core::ZipReader * zip;

	// All of the following may be empty.
	ShaderFileList preloadedIncludes;
	core::String defaultDefinitions; // (#version, #pragmas, etc)
	core::String defaultIncludePath; // Path to shader includes (.glsl)
	core::String defaultShaderPath;  // Path to the shader source files (.vert/.frag)
};

} // namespace ogl {}
} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_OPENGL_GLSL_PREPROCESSOR_HPP
