
// ================================================================================================
// -*- C++ -*-
// File: rhi_manager_texture.cpp
// Author: Guilherme R. Lampert
// Created on: 18/07/15
// Brief: RhiManagerImpl methods related to texture management.
// ================================================================================================

#include "atlas/render/opengl/rhi_manager.hpp"
#include "atlas/render/render_log.hpp"

namespace atlas
{
namespace render
{
namespace ogl
{

// ================================================================================================
// ogl::RhiManagerImpl -- Texture management.
// ================================================================================================

#define R_BIND_GL_TEX_NO_CACHING_TEST(target, tex, tmu) \
	do { \
		currentGLTextures[(tmu)] = (tex); \
		glActiveTexture(GL_TEXTURE0 + (tmu)); \
		glBindTexture((target), (tex)); \
	} while (0)

//
// The following replacement constants are from:
//  https://www.opengl.org/registry/specs/EXT/texture_filter_anisotropic.txt
//

// Accepted by the `pname` parameter of glGetTexParameter and glTexParameter.
#ifndef GL_TEXTURE_MAX_ANISOTROPY_EXT
	#define GL_TEXTURE_MAX_ANISOTROPY_EXT 0x84FE
#endif // GL_TEXTURE_MAX_ANISOTROPY_EXT

// Accepted by the `pname` parameter of glGetBooleanv, glGetDoublev, glGetFloatv, and glGetIntegerv.
#ifndef GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT
	#define GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT 0x84FF
#endif // GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT

// This is assumed for the GL implementation only!
COMPILE_TIME_CHECK(Texture::MaxMipmapSurfaces >= 16, "Expected at least 16 max mip-surfaces!");

// ========================================================
// Local helper functions:
// ========================================================

namespace
{

GLenum getGLTexTarget(const Texture::Target::Enum target)
{
	if (target == Texture::Target::CubeMap)
	{
		return GL_TEXTURE_CUBE_MAP;
	}
	//
	// NOTE:
	// Expand this if we add support for GL_TEXTURE_1D or GL_TEXTURE_3D!
	//
	return GL_TEXTURE_2D;
}

void getGLTexFormats(const Texture::DataLayout::Enum layout, GLenum * glFormat,
                     GLenum * glInternalFormat, GLenum * glDataType)
{
	switch (layout)
	{
	case Texture::DataLayout::R8 :
		if (glFormat)         { *glFormat         = GL_RED;   }
		if (glInternalFormat) { *glInternalFormat = GL_R8;    }
		if (glDataType)       { *glDataType       = GL_UNSIGNED_BYTE; }
		break;

	case Texture::DataLayout::RG8 :
		if (glFormat)         { *glFormat         = GL_RG;    }
		if (glInternalFormat) { *glInternalFormat = GL_RG8;   }
		if (glDataType)       { *glDataType       = GL_UNSIGNED_BYTE; }
		break;

	case Texture::DataLayout::RGB8 :
		if (glFormat)         { *glFormat         = GL_RGB;   }
		if (glInternalFormat) { *glInternalFormat = GL_RGB8;  }
		if (glDataType)       { *glDataType       = GL_UNSIGNED_BYTE; }
		break;

	case Texture::DataLayout::RGBA8 :
		if (glFormat)         { *glFormat         = GL_RGBA;  }
		if (glInternalFormat) { *glInternalFormat = GL_RGBA8; }
		if (glDataType)       { *glDataType       = GL_UNSIGNED_BYTE; }
		break;

	case Texture::DataLayout::BGR8 :
		if (glFormat)         { *glFormat         = GL_BGR;   }
		if (glInternalFormat) { *glInternalFormat = GL_RGB8;  }
		if (glDataType)       { *glDataType       = GL_UNSIGNED_BYTE; }
		break;

	case Texture::DataLayout::BGRA8 :
		if (glFormat)         { *glFormat         = GL_BGRA;  }
		if (glInternalFormat) { *glInternalFormat = GL_RGBA8; }
		if (glDataType)       { *glDataType       = GL_UNSIGNED_INT_8_8_8_8_REV; }
		break;

	default :
		R_LOG_FATAL("Invalid texture data layout!");
	} // switch (layout)
}

GLuint estimateMipmapCount(const core::Vec2u dimensions)
{
	GLuint mipmapCount = 1; // Assume the initial base level.

	// Special case of 1x1 image.
	if (dimensions.x == 1 && dimensions.y == 1)
	{
		return mipmapCount;
	}

	GLuint width  = dimensions.x;
	GLuint height = dimensions.y;
	while (mipmapCount != Texture::MaxMipmapSurfaces)
	{
		width  = core::max(1u, width  / 2);
		height = core::max(1u, height / 2);
		++mipmapCount;

		if (width == 1 && height == 1)
		{
			break;
		}
	}

	return mipmapCount;
}

GLuint estimateTexMemoryUsed(const core::Vec2u dimensions,
                             const Texture::DataLayout::Enum layout,
                             const GLuint mipmapCount)
{
	const GLuint paddingEstimate = 128; // Estimate that the GL will add a few bytes of padding to each level.
	const GLuint bytesPerPixel   = core::PixelFormat::fromLayout(layout).getBytesPerPixel();

	GLuint memoryTotal = 0;
	GLuint width  = dimensions.x;
	GLuint height = dimensions.y;

	for (GLuint level = 0; level < mipmapCount; ++level)
	{
		memoryTotal += (width * height * bytesPerPixel) + paddingEstimate;
		width  = core::max(1u, width  / 2);
		height = core::max(1u, height / 2);
	}

	return memoryTotal;
}

} // namespace {}

// ========================================================
// RhiManagerImpl::allocateTexture():
// ========================================================

bool RhiManagerImpl::allocateTexture(Texture & destTexture, const core::Image & srcImage,
                                     const core::uint resourceFlags, const Texture::Target::Enum target,
                                     const bool autoGenMipmaps)
{
	DEBUG_CHECK(isInitialized());

	DEBUG_CHECK(srcImage.isValid() && "Invalid source image for texture allocation!");
	DEBUG_CHECK(srcImage.getSurfaceCount() >= 1);

	const Texture::DataLayout::Enum layout = srcImage.getDataLayout();
	const core::Vec2u dimensions = srcImage.getDimensions();

	if (dimensions.x > unsigned(maxGLTextureSize) ||
	    dimensions.y > unsigned(maxGLTextureSize))
	{
		R_LOG_ERROR("Max GL texture size is " << maxGLTextureSize
			<< "; Trying to allocate texture with "
			<< dimensions.x << "x" << dimensions.y << " pixels!");

		return false;
	}

	const GLuint surfaceCount = srcImage.getSurfaceCount();
	const GLuint mipmapCount  = (surfaceCount == 1 && autoGenMipmaps) ? estimateMipmapCount(dimensions) : surfaceCount;

	// Allocate uninitialized memory:
	if (!allocateTexture(destTexture, resourceFlags, dimensions, layout, target, mipmapCount))
	{
		return false;
	}

	const GLenum glTarget = getGLTexTarget(target);

	GLenum glPixelFormat, glDataType;
	getGLTexFormats(layout, &glPixelFormat, nullptr, &glDataType);

	// Upload image surfaces to GL (tex already bound to TMU=0 by the previous allocation):
	for (GLuint level = 0; level < surfaceCount; ++level)
	{
		const core::ImageSurface & imgSurf = srcImage.getSurface(level);

		const GLvoid * dataPtr = imgSurf.getPixelData();
		DEBUG_CHECK(dataPtr != nullptr);

		const GLuint w = imgSurf.getWidth();
		const GLuint h = imgSurf.getHeight();

		setGLPixelAlignment(GL_UNPACK_ALIGNMENT, layout, w);
		glTexSubImage2D(glTarget, level, 0, 0, w, h, glPixelFormat, glDataType, dataPtr);
	}

	R_CHECK_GL_ERRORS();

	// Optional automatic mipmap construction if the original image wasn't already mipmaped:
	if (surfaceCount == 1 && autoGenMipmaps)
	{
		generateMipmapSurfaces(destTexture);
	}

	return true;
}

// ========================================================
// RhiManagerImpl::allocateTexture():
// ========================================================

bool RhiManagerImpl::allocateTexture(Texture & destTexture, const core::uint resourceFlags,
                                     const core::Vec2u dimensions, const Texture::DataLayout::Enum layout,
                                     const Texture::Target::Enum target, const core::uint mipmapCount)
{
	DEBUG_CHECK(isInitialized());

	DEBUG_CHECK(dimensions.x != 0);
	DEBUG_CHECK(dimensions.y != 0);
	DEBUG_CHECK(mipmapCount  >= 1);
	DEBUG_CHECK(mipmapCount  <= Texture::MaxMipmapSurfaces);
	DEBUG_CHECK(layout       != Texture::DataLayout::Null);

	DEBUG_CHECK((destTexture.resourceFlags & Resource::Flag::TypeTexture) && "Inconsistent flags!");
	DEBUG_CHECK(!destTexture.isResourceLocked() && "A texture can't be in a locked state!");

	if (dimensions.x > unsigned(maxGLTextureSize) ||
	    dimensions.y > unsigned(maxGLTextureSize))
	{
		R_LOG_ERROR("Max GL texture size is " << maxGLTextureSize
			<< "; Trying to allocate texture with "
			<< dimensions.x << "x" << dimensions.y << " pixels!");

		return false;
	}

	// Recycle the GL handle if possible.
	GLuint glTexHandle = destTexture.texHandleGL;
	if (glTexHandle == 0)
	{
		glTexHandle = newGLTexHandle();
		if (glTexHandle == 0)
		{
			return false; // newGLTexHandle() already logs the GL errors.
		}
	}

	const GLenum glTarget = getGLTexTarget(target);
	R_BIND_GL_TEX_NO_CACHING_TEST(glTarget, glTexHandle, 0);

	GLenum glInternalFormat;
	getGLTexFormats(layout, nullptr, &glInternalFormat, nullptr);

	glTexStorage2D(glTarget, mipmapCount, glInternalFormat, dimensions.x, dimensions.y);
	R_CHECK_GL_ERRORS();

	// Special formats need texture swizzling.
	setGLTexSwizzle(layout, glTarget);

	destTexture.texHandleGL   = glTexHandle;
	destTexture.resourceFlags = resourceFlags | Resource::Flag::TypeTexture;
	destTexture.dimensions    = dimensions;
	destTexture.mipmapCount   = mipmapCount;
	destTexture.dataLayout    = layout;
	destTexture.memoryUsage   = estimateTexMemoryUsed(dimensions, layout, mipmapCount);

	// These will set the internal dirty flags.
	destTexture.setTarget(target);
	destTexture.setFirstMipmapIndex(0);
	destTexture.setLastMipmapIndex(mipmapCount - 1);
	destTexture.setTexUnit(Texture::MappingUnit::TMU0);

	R_LOG_INFO("New texture allocated: " << destTexture.getWidth() << "x"
		<< destTexture.getHeight() << ", " << toString(destTexture.getDataLayout())
		<< "/" << toString(destTexture.getTarget()) << ", "
		<< destTexture.getMipmapCount() << " mipmaps.");

	return true;
}

// ========================================================
// RhiManagerImpl::writeTextureSurface():
// ========================================================

bool RhiManagerImpl::writeTextureSurface(Texture & destTexture, const core::uint surface,
                                         const core::Vec2u offsets, const core::Vec2u dimensions,
                                         const core::Image & srcImage, const core::uint srcImageSurface)
{
	DEBUG_CHECK(isInitialized());

	DEBUG_CHECK(dimensions.x != 0);
	DEBUG_CHECK(dimensions.y != 0);
	DEBUG_CHECK(surface < destTexture.getMipmapCount());

	DEBUG_CHECK(srcImage.isValid());
	DEBUG_CHECK(destTexture.isValid());
	DEBUG_CHECK(destTexture.getDataLayout() == srcImage.getDataLayout() && "Expected same pixel format!");

	const core::ImageSurface & imgSurf = srcImage.getSurface(srcImageSurface);
	const GLenum glTarget  = getGLTexTarget(destTexture.getTarget());
	const GLvoid * dataPtr = imgSurf.getPixelData();

	DEBUG_CHECK(offsets.x + dimensions.x <= destTexture.getWidth(surface));
	DEBUG_CHECK(offsets.y + dimensions.y <= destTexture.getHeight(surface));
	DEBUG_CHECK(dimensions.x <= imgSurf.getWidth());
	DEBUG_CHECK(dimensions.y <= imgSurf.getHeight());
	DEBUG_CHECK(dataPtr != nullptr);

	GLenum glPixelFormat, glDataType;
	getGLTexFormats(destTexture.getDataLayout(), &glPixelFormat, nullptr, &glDataType);

	setTexture(destTexture);
	setGLPixelAlignment(GL_UNPACK_ALIGNMENT, destTexture.getDataLayout(), dimensions.x);

	glTexSubImage2D(glTarget, surface, offsets.x, offsets.y, dimensions.x,
			dimensions.y, glPixelFormat, glDataType, dataPtr);

	R_CHECK_GL_ERRORS();
	return true;
}

// ========================================================
// RhiManagerImpl::readTextureSurface():
// ========================================================

bool RhiManagerImpl::readTextureSurface(const Texture & srcTexture, const core::uint surface, core::Image & destImage)
{
	DEBUG_CHECK(isInitialized());

	DEBUG_CHECK(srcTexture.isValid());
	DEBUG_CHECK(surface < srcTexture.getMipmapCount());

	// See if we can reuse the image storage by any chance...
	bool needToReallocate;
	if (!destImage.isValid())
	{
		needToReallocate = true;
	}
	else if (destImage.getWidth()      != srcTexture.getWidth(surface)  ||
	         destImage.getHeight()     != srcTexture.getHeight(surface) ||
	         destImage.getDataLayout() != srcTexture.getDataLayout())
	{
		needToReallocate = true;
	}
	else
	{
		needToReallocate = false;
	}

	const Texture::DataLayout::Enum layout = srcTexture.getDataLayout();
	const GLenum glTarget = getGLTexTarget(srcTexture.getTarget());

	// Yep, will need to free the current and allocate a new one.
	if (needToReallocate)
	{
		destImage.deallocate();
		destImage.initWithDimension(srcTexture.getWidth(surface), srcTexture.getHeight(surface), layout);
	}

	GLenum glPixelFormat, glDataType;
	getGLTexFormats(layout, &glPixelFormat, nullptr, &glDataType);

	setTexture(srcTexture);
	setGLPixelAlignment(GL_PACK_ALIGNMENT, layout, srcTexture.getWidth(surface));

	glGetTexImage(glTarget, surface, glPixelFormat, glDataType, destImage.getPixelDataBaseSurface());

	R_CHECK_GL_ERRORS();
	return true;
}

// ========================================================
// RhiManagerImpl::generateMipmapSurfaces():
// ========================================================

void RhiManagerImpl::generateMipmapSurfaces(Texture & destTexture)
{
	DEBUG_CHECK(isInitialized());

	DEBUG_CHECK(destTexture.isValid());
	DEBUG_CHECK(destTexture.getMipmapCount() >= 1);

	setTexture(destTexture);
	glGenerateMipmap(getGLTexTarget(destTexture.getTarget()));

	//TODO should probably query the GL for the number of mipmaps
	// generated and then update the texture object!

	R_CHECK_GL_ERRORS();
}

// ========================================================
// RhiManagerImpl::copyColorBuffer():
// ========================================================

bool RhiManagerImpl::copyColorBuffer(Texture & destTexture, const core::Vec2u offsets, const core::Vec2u dimensions)
{
	DEBUG_CHECK(isInitialized());

	(void)destTexture;(void)offsets;(void)dimensions;
	return false;
	//TODO
}

// ========================================================
// RhiManagerImpl::copyDepthBuffer():
// ========================================================

bool RhiManagerImpl::copyDepthBuffer(Texture & destTexture, const core::Vec2u offsets, const core::Vec2u dimensions)
{
	DEBUG_CHECK(isInitialized());

	(void)destTexture;(void)offsets;(void)dimensions;
	return false;
	//TODO
}

// ========================================================
// RhiManagerImpl::copyStencilBuffer():
// ========================================================

bool RhiManagerImpl::copyStencilBuffer(Texture & destTexture, const core::Vec2u offsets,
                                       const core::Vec2u dimensions, const bool overdrawVisualization)
{
	DEBUG_CHECK(isInitialized());

	(void)destTexture;(void)offsets;(void)dimensions;(void)overdrawVisualization;
	return false;
	//TODO
}

// ========================================================
// RhiManagerImpl::setTexture():
// ========================================================

void RhiManagerImpl::setTexture(const Texture & texture)
{
	DEBUG_CHECK(isInitialized());

	if (!texture.isValid())
	{
		//TODO bind a default texture instead,
		// with an easy to notice pattern!
		return;
	}

	DEBUG_CHECK((texture.resourceFlags & Resource::Flag::TypeTexture) && "Inconsistent flags!");
	DEBUG_CHECK(!texture.isResourceLocked() && "A texture can't be in a locked state!");

	const bool   dirty    = texture.isDirty();
	const GLuint tmuIndex = texture.getTexUnit();
	const GLenum glTarget = getGLTexTarget(texture.getTarget());

	// Force a re-bind if the texture has any of the dirty
	// flags set to ensure we have the correct active TMU.
	if (texture.texHandleGL != currentGLTextures[tmuIndex] || dirty)
	{
		R_BIND_GL_TEX_NO_CACHING_TEST(glTarget, texture.texHandleGL, tmuIndex);
	}

	if (dirty)
	{
		// We hack the constness out of the object to be able to clear
		// the dirty flags. Other object states or texture data are not touched.
		refreshGLTexParameters(const_cast<Texture &>(texture), glTarget);
	}
}

// ========================================================
// RhiManagerImpl::newGLTexHandle():
// ========================================================

GLuint RhiManagerImpl::newGLTexHandle()
{
	GLuint newTexture = 0;
	glGenTextures(1, &newTexture);

	if (newTexture == 0)
	{
		R_LOG_ERROR("Failed to allocate a new GL texture handle! Possibly out-of-memory!");
		R_CHECK_GL_ERRORS();
	}
	return newTexture;
}

// ========================================================
// RhiManagerImpl::freeGLTexture():
// ========================================================

void RhiManagerImpl::freeGLTexture(Texture & texture)
{
	R_LOG_INFO("Freeing a GL Texture...");

	const GLuint glTexHandle = texture.texHandleGL;
	if (glTexHandle == 0)
	{
		return;
	}

	clearGLTexBindingIfCurrent(texture);
	glDeleteTextures(1, &glTexHandle);
	texture.texHandleGL = 0;
}

// ========================================================
// RhiManagerImpl::queryMaxGLTexAnisotropy():
// ========================================================

void RhiManagerImpl::queryMaxGLTexAnisotropy()
{
	glGetIntegerv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxGLAnisotropy);
	if (maxGLAnisotropy <= 0)
	{
		maxGLAnisotropy = 1;
	}
}

// ========================================================
// RhiManagerImpl::refreshGLTexParameters():
// ========================================================

void RhiManagerImpl::refreshGLTexParameters(Texture & texture, const GLenum glTarget)
{
	if (texture.dirtyFlags.addressing)
	{
		refreshGLTexAddressing(texture, glTarget);
		texture.dirtyFlags.addressing = false;
	}

	if (texture.dirtyFlags.filter || texture.dirtyFlags.anisotropy)
	{
		refreshGLTexFilter(texture, glTarget);
		texture.dirtyFlags.filter     = false;
		texture.dirtyFlags.anisotropy = false;
	}

	if (texture.dirtyFlags.firstMipmap || texture.dirtyFlags.lastMipmap)
	{
		refreshGLTexMipmapIndexes(texture, glTarget);
		texture.dirtyFlags.firstMipmap = false;
		texture.dirtyFlags.lastMipmap  = false;
	}

	if (texture.dirtyFlags.mipLodBias)
	{
		refreshGLTexMipLodBias(texture, glTarget);
		texture.dirtyFlags.mipLodBias = false;
	}

	R_CHECK_GL_ERRORS();
}

// ========================================================
// RhiManagerImpl::refreshGLTexFilter():
// ========================================================

void RhiManagerImpl::refreshGLTexFilter(Texture & texture, const GLenum glTarget)
{
	// The following applies to the currently bound texture!

	GLint filter = texture.getFilter();
	if (filter == Texture::Filter::Default)
	{
		// Use the global filter instead.
		filter = textureFilter;
	}

	const bool withMipmaps = (texture.getMipmapCount() > 1);
	switch (filter)
	{
	case Texture::Filter::Nearest :
		glTexParameteri(glTarget, GL_TEXTURE_MIN_FILTER, (withMipmaps ? GL_NEAREST_MIPMAP_NEAREST : GL_NEAREST));
		glTexParameteri(glTarget, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		break;

	case Texture::Filter::Bilinear :
		glTexParameteri(glTarget, GL_TEXTURE_MIN_FILTER, (withMipmaps ? GL_LINEAR_MIPMAP_NEAREST : GL_LINEAR));
		glTexParameteri(glTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		break;

	case Texture::Filter::Trilinear :
		glTexParameteri(glTarget, GL_TEXTURE_MIN_FILTER, (withMipmaps ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR));
		glTexParameteri(glTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		break;

	case Texture::Filter::Anisotropic :
		// Anisotropic filtering only works for mipmaped textures.
		// If not mipmaped, fall back to a linear filter.
		if (withMipmaps)
		{
			glTexParameteri(glTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(glTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			GLint anisotropy = texture.getAnisotropy();
			if (anisotropy > maxGLAnisotropy)
			{
				texture.setAnisotropy(maxGLAnisotropy);
				anisotropy = maxGLAnisotropy;
			}

			glTexParameterf(glTarget, GL_TEXTURE_MAX_ANISOTROPY_EXT, static_cast<GLfloat>(anisotropy));
		}
		else
		{
			glTexParameteri(glTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(glTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			// Reset to the initial value, in case the texture handle was recycled.
			glTexParameterf(glTarget, GL_TEXTURE_MAX_ANISOTROPY_EXT, 1.0f);
		}
		break;
	default :
		R_LOG_FATAL("Invalid texture filter!");
	} // switch (filter)
}

// ========================================================
// RhiManagerImpl::refreshGLTexAddressing():
// ========================================================

void RhiManagerImpl::refreshGLTexAddressing(const Texture & texture, const GLenum glTarget)
{
	// The following applies to the currently bound texture!

	GLenum glWrapMode;
	switch (texture.getAddressing())
	{
	case Texture::Addressing::Default :
	case Texture::Addressing::Repeat  :
		glWrapMode = GL_REPEAT;
		break;

	case Texture::Addressing::ClampToEdge :
		glWrapMode = GL_CLAMP_TO_EDGE;
		break;

	case Texture::Addressing::RepeatMirrored :
		glWrapMode = GL_MIRRORED_REPEAT;
		break;

	case Texture::Addressing::ClampToBlack :
		{
			const GLfloat black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
			glTexParameterfv(glTarget, GL_TEXTURE_BORDER_COLOR, black);
			glWrapMode = GL_CLAMP_TO_BORDER;
		}
		break;

	case Texture::Addressing::ClampToWhite :
		{
			const GLfloat white[] = { 1.0f, 1.0f, 1.0f, 1.0f };
			glTexParameterfv(glTarget, GL_TEXTURE_BORDER_COLOR, white);
			glWrapMode = GL_CLAMP_TO_BORDER;
		}
		break;

	case Texture::Addressing::ClampToAlpha0 :
		{
			const GLfloat alpha0[] = { 0.0f, 0.0f, 0.0f, 0.0f };
			glTexParameterfv(glTarget, GL_TEXTURE_BORDER_COLOR, alpha0);
			glWrapMode = GL_CLAMP_TO_BORDER;
		}
		break;

	default :
		R_LOG_FATAL("Invalid texture addressing mode!");
	} // switch (texture.getAddressing())

	glTexParameteri(glTarget, GL_TEXTURE_WRAP_S, glWrapMode);
	glTexParameteri(glTarget, GL_TEXTURE_WRAP_T, glWrapMode);
}

// ========================================================
// RhiManagerImpl::refreshGLTexMipmapIndexes():
// ========================================================

void RhiManagerImpl::refreshGLTexMipmapIndexes(const Texture & texture, const GLenum glTarget)
{
	// The following applies to the currently bound texture!
	glTexParameteri(glTarget, GL_TEXTURE_BASE_LEVEL, texture.getFirstMipmapIndex());
	glTexParameteri(glTarget, GL_TEXTURE_MAX_LEVEL,  texture.getLastMipmapIndex());
}

// ========================================================
// RhiManagerImpl::refreshGLTexMipLodBias():
// ========================================================

void RhiManagerImpl::refreshGLTexMipLodBias(const Texture & texture, const GLenum glTarget)
{
	// The following applies to the currently bound texture!
	glTexParameterf(glTarget, GL_TEXTURE_LOD_BIAS, texture.getMipmapLodBias());
}

// ========================================================
// RhiManagerImpl::clearGLTexBindingIfCurrent():
// ========================================================

void RhiManagerImpl::clearGLTexBindingIfCurrent(const Texture & texture)
{
	const GLuint glTexHandle = texture.texHandleGL;
	const GLenum glTarget    = getGLTexTarget(texture.getTarget());

	for (GLuint tmuIndex = 0; tmuIndex < currentGLTextures.getSize(); ++tmuIndex)
	{
		if (glTexHandle == currentGLTextures[tmuIndex])
		{
			R_BIND_GL_TEX_NO_CACHING_TEST(glTarget, 0, tmuIndex);
			// A texture could be bound to multiple TMUs, so walk the whole set to be sure.
		}
	}
}

// ========================================================
// RhiManagerImpl::setGLTexSwizzle():
// ========================================================

void RhiManagerImpl::setGLTexSwizzle(const Texture::DataLayout::Enum layout, const GLenum glTarget)
{
	// Operates on the current texture bound to glTarget!

	if (layout == Texture::DataLayout::R8)
	{
		// RED only texture, tell GL to fill GREEN and BLUE with
		// the first component (RED) and set alpha to 1:
		glTexParameteri(glTarget, GL_TEXTURE_SWIZZLE_R, GL_RED);
		glTexParameteri(glTarget, GL_TEXTURE_SWIZZLE_G, GL_RED);
		glTexParameteri(glTarget, GL_TEXTURE_SWIZZLE_B, GL_RED);
		glTexParameteri(glTarget, GL_TEXTURE_SWIZZLE_A, GL_ONE);
	}
	else if (layout == Texture::DataLayout::RG8)
	{
		// RED & GREEN texture, tell GL to set BLUE to 0 and alpha to 1:
		glTexParameteri(glTarget, GL_TEXTURE_SWIZZLE_R, GL_RED);
		glTexParameteri(glTarget, GL_TEXTURE_SWIZZLE_G, GL_GREEN);
		glTexParameteri(glTarget, GL_TEXTURE_SWIZZLE_B, GL_ZERO);
		glTexParameteri(glTarget, GL_TEXTURE_SWIZZLE_A, GL_ONE);
	}

	R_CHECK_GL_ERRORS();
}

// ========================================================
// RhiManagerImpl::setGLPixelAlignment():
// ========================================================

void RhiManagerImpl::setGLPixelAlignment(const GLenum packAlign,
                                         const Texture::DataLayout::Enum layout,
                                         const GLuint width)
{
	DEBUG_CHECK(packAlign == GL_PACK_ALIGNMENT || packAlign == GL_UNPACK_ALIGNMENT);
	const GLuint rowSizeBytes = width * core::PixelFormat::fromLayout(layout).getBytesPerPixel();

	// Set the row alignment to the highest value that
	// the size of a row divides evenly. Options are: 8,4,2,1.
	if ((rowSizeBytes % 8) == 0)
	{
		glPixelStorei(packAlign, 8);
	}
	else if ((rowSizeBytes % 4) == 0)
	{
		glPixelStorei(packAlign, 4);
	}
	else if ((rowSizeBytes % 2) == 0)
	{
		glPixelStorei(packAlign, 2);
	}
	else
	{
		glPixelStorei(packAlign, 1);
	}

	R_CHECK_GL_ERRORS();
}

} // namespace ogl {}
} // namespace render {}
} // namespace atlas {}
