
// ================================================================================================
// -*- C++ -*-
// File: rhi_manager.hpp
// Author: Guilherme R. Lampert
// Created on: 02/06/15
// Brief: OpenGL-based back-end implementation of the RhiManager.
// ================================================================================================

#ifndef ATLAS_RENDER_OPENGL_RHI_MANAGER_HPP
#define ATLAS_RENDER_OPENGL_RHI_MANAGER_HPP

#if !defined (ATLAS_RHI_BACKEND_OPENGL)
	#error "This file shouldn't be included when not compiling the OpenGL RHI!"
#endif // ATLAS_RHI_BACKEND_OPENGL

// Render Window has the OpenGL rendering context object.
#include "atlas/shell/render_window.hpp"

// RHI back-end independent interfaces:
#include "atlas/render/resource.hpp"
#include "atlas/render/program.hpp"
#include "atlas/render/texture.hpp"
#include "atlas/render/frame_buffer.hpp"
#include "atlas/render/vertex_buffer.hpp"

namespace atlas
{
namespace render
{
namespace ogl
{

// ========================================================
// class RhiManagerImpl:
// ========================================================

class RhiManagerImpl final
	: private core::NonCopyable
{
public:

	//
	// RHI initialization/shutdown:
	//

	 RhiManagerImpl();
	~RhiManagerImpl();

	// Back-end initialization. You may pass a required renderer version or -1 to
	// accept any available option. If you do specify a version and that version is
	// not available, then this function fails and the RHI Manager remain uninitialized.
	bool initRhiBackEnd(shell::RenderWindow * renderWin,
	                    int requiredVersionMajor = -1,
	                    int requiredVersionMinor = -1);

	// Test if the RHI Manager was successfully initialized once.
	bool isInitialized() const;

	// Shutdown may be performed manually, but will be done automatically by the destructor.
	void shutdown();

	//
	// RHI/Back-end queries:
	//

	// Width and height of the rendering screen, in pixels.
	core::uint  getScreenWidth()  const;
	core::uint  getScreenHeight() const;
	core::Vec2u getScreenDimensions() const;

	//
	// Texture management:
	//

	// Allocate RHI texture memory from the given Image object and copy the image to the texture memory.
	// The data contents of the output texture are overwritten, if any. Passing `autoGenMipmaps` as true
	// will generate a mipmap surface chain for the texture if the input Image is not already mipmaped.
	// If the image has mipmaps, then those will be used instead.
	bool allocateTexture(Texture & destTexture, const core::Image & srcImage,
	                     core::uint resourceFlags, Texture::Target::Enum target,
	                     bool autoGenMipmaps);

	// Allocate uninitialized RHI texture memory. `mipmapCount` is the number of surfaces to allocate,
	// each half the size of the previous, starting from `dimensions`. This must be at least 1.
	bool allocateTexture(Texture & destTexture, core::uint resourceFlags, core::Vec2u dimensions,
	                     Texture::DataLayout::Enum layout, Texture::Target::Enum target,
	                     core::uint mipmapCount);

	// Updates texture data for a given mipmap surface. Texture data must be already allocated.
	bool writeTextureSurface(Texture & destTexture, core::uint surface,
	                         core::Vec2u offsets, core::Vec2u dimensions,
	                         const core::Image & srcImage, core::uint srcImageSurface);

	// Reads texture data from the hardware to system memory, writing to a user provided image object.
	// This will always write to the base surface of the output, so it is recommended to pass an empty image.
	// Note: This can be a slow operation, since it forces the rendering pipeline to synchronize with the CPU.
	bool readTextureSurface(const Texture & srcTexture, core::uint surface, core::Image & destImage);

	// Generates a new mipmap chain for this texture, starting from the data at surface zero.
	// This will give preference to hardware mipmap generation if available, falling back to a
	// CPU/system-memory approach if hardware acceleration is not available. The input texture
	// must already have at the very least one level of data currently allocated and filled.
	void generateMipmapSurfaces(Texture & destTexture);

	// Creates a new texture image from the contents of the current color frame buffer.
	// Any exiting data in `destTexture` is overwritten.
	bool copyColorBuffer(Texture & destTexture, core::Vec2u offsets, core::Vec2u dimensions);

	// Creates a new texture image from the contents of the current depth buffer.
	// Any exiting data in `destTexture` is overwritten. Fails if no depth buffer is being used.
	bool copyDepthBuffer(Texture & destTexture, core::Vec2u offsets, core::Vec2u dimensions);

	// Creates a new texture image from the contents of the current stencil buffer.
	// Any exiting data in `destTexture` is overwritten. Fails if no stencil buffer is being used.
	// Set `overdrawVisualization` to true if you are copying the buffer to visualize rendering overdraw.
	// Setting this flag will linearize the output values to make the data proper for debug drawing.
	bool copyStencilBuffer(Texture & destTexture, core::Vec2u offsets,
	                       core::Vec2u dimensions, bool overdrawVisualization);

	// Binds the texture for subsequent rendering operations.
	// Does nothing if the given texture is already the current one.
	// Sets a default fallback texture if the input is not valid.
	void setTexture(const Texture & texture);

	//
	// Frame Buffer management:
	//

	// Allocates a new frame buffer object, overwriting the input if it is not empty.
	// On failure, `destFrameBuffer` remains unchanged. Setting `forceRhiSurfacesDS` to
	// true tells the back-end to use optimized drawing surfaces for depth and stencil
	// targets if possible. Those surfaces are not the same as ordinary textures, so they can
	// be rendered to, but can't be sampled from. The color target is always a normal Texture.
	bool allocateFrameBuffer(FrameBuffer & destFrameBuffer, core::uint resourceFlags,
	                         core::Vec2u rtDimensions, Texture::DataLayout::Enum rtLayout,
	                         Texture::Filter::Enum rtFilter, core::uint rtCount,
	                         bool withDepth, bool withStencil, bool forceRhiSurfacesDS);

	// Binds the frame buffer for subsequent rendering operations.
	// Does nothing if the given buffer is already the current one.
	// The default screen frame buffer is initially bound.
	void setFrameBuffer(const FrameBuffer & frameBuffer);

	// Set the current frame buffer to the default screen frame buffer.
	// Shorthand for `setFrameBuffer(getScreenFrameBuffer())`.
	void setFrameBufferToScreenFrameBuffer();

	// Get the default screen frame buffer used to present rendered scenes.
	// This is the only FrameBuffer object that returns true for `isScreenFrameBuffer()`.
	const FrameBuffer & getScreenFrameBuffer() const;

	// Clear the current frame buffer, which may be the screen.
	// You may selectively clear each render target, or all of them.
	void clearFrameBuffer(bool color, bool depth, bool stencil);

	// Set the values used by `clearFrameBuffer()` to clear the
	// corresponding color, depth and stencil render targets/buffers.
	void setClearColorValue(core::Color value);
	void setClearDepthValue(float value);
	void setClearStencilValue(int value);

	// Max color render targets and consequently the max number of
	// color texture attachments for any FrameBuffer. Minimum of 8
	// for PC targets, always 1 for OpenGL-ES 2.0.
	core::uint getColorRenderTargetCount() const;

	// Enables the given color render target output for the subsequent rendering operations.
	// The current frame buffer must naturally have a color render target texture at the same index.
	// Targets remain enabled until disabled again. Index ranges from 0 to `getColorRenderTargetCount() - 1`.
	// Render target 0 is initially enabled by default.
	void enableColorRenderTarget(core::uint index);
	void disableColorRenderTarget(core::uint index);

	//
	// Render Program management:
	//

	// Allocates a new shader program, overwriting the input program if it is not empty.
	// A valid render program requires at least two shader stages, Vertex and Fragment.
	// The last optional parameter will output program compilation results if an error occurs.
	bool allocateProgram(Program & destProgram, core::uint resourceFlags,
	                     const ProgramStage * stages, core::uint stageCount,
	                     ProgramSetupResult * optionalResult = nullptr);

	// Binds the program for subsequent rendering operations.
	// Does nothing if the given program is already the current one.
	// Sets a default fallback program if the input is not valid.
	// This will also commit any pending render parameters, so the parameters
	// should be updated before calling this method, to avoid having to manually
	// call `commitProgramRenderParams()` a second time.
	void setProgram(const Program & program);

	// Commits any recently updated render parameters to the back-end driver.
	// This is called automatically by `setProgram()` if a commit is pending.
	void commitProgramRenderParams(const Program & program);

	// Finds and return and existing identical vertex layout or
	// registers the new layout and returns a reference to it.
	const ProgramVertexLayout & findOrRegisterVertexLayout(const ProgramVertexLayout & templateLayout);

	//
	// Vertex/Index buffer management:
	//

	// Allocates a new vertex buffer, overwriting the input if it is not empty.
	// Resource flags must be one of the usage flags: Usage[Static|Dynamic|Stream].
	// `initialVertexData` may be null, in which case the buffer is allocated but not initialized.
	bool allocateVertexBuffer(VertexBuffer & destVertexBuffer, core::uint resourceFlags,
	                          core::uint vertexCount, core::uint sizeOfAVertex,
	                          const core::ubyte * initialVertexData);

	// Same as the above, but allocates an indexed VB instead.
	// Initial index and vertex data may be null to allocate uninitialized memory.
	bool allocateIndexedVertexBuffer(IndexedVertexBuffer & destVertexBuffer, core::uint resourceFlags,
	                                 core::uint vertexCount, core::uint sizeOfAVertex,
	                                 core::uint indexCount,  core::uint sizeOfAnIndex,
	                                 const core::ubyte * initialVertexData,
	                                 const core::ubyte * initialIndexData);

	// Updates a subset of a buffer object's data store. Buffer must be already allocated.
	// If `allowDiscardingCurrentData` is set, the back-end will not attempt to synchronize
	// the existing buffer data and will just allocate a new chunk of data. This might result
	// on faster data transfers between main memory and the back-end/GPU. This flag is just a hint.
	bool updateBufferVertexes(VertexBuffer & vertexBuffer, core::uint offsetBytes,
	                          core::uint sizeBytes, const core::ubyte * vertexData,
	                          bool allowDiscardingCurrentData);

	// Same as `updateBufferVertexes()` of VertexBuffer, but for the vertex data of an indexed VB.
	bool updateBufferVertexes(IndexedVertexBuffer & iVertexBuffer, core::uint offsetBytes,
	                          core::uint sizeBytes, const core::ubyte * vertexData,
	                          bool allowDiscardingCurrentData);

	// Same as `updateBufferVertexes()` of VertexBuffer, but for the index data of an indexed VB.
	bool updateBufferIndexes(IndexedVertexBuffer & iVertexBuffer, core::uint offsetBytes,
	                         core::uint sizeBytes, const core::ubyte * indexData,
	                         bool allowDiscardingCurrentData);

	// Binds the buffer for subsequent draw calls. Does nothing if the buffer is already current.
	// This will fail silently if the buffer is invalid, since no default fallback can be used
	// in its place, which will probably result on nothing being drawn to the screen.
	void setVertexBuffer(const VertexBuffer & vertexBuffer);
	void setVertexBuffer(const IndexedVertexBuffer & iVertexBuffer);

	//
	// Common resource management methods:
	//

	// Maps a portion of a resource to system memory, so that it can be directly accessed
	// via a pointer. The resource cannot be used for rendering until it is unlocked.
	// Note: This cannot be used with a texture or shader resource. Only buffers can be locked.
	core::ubyte * lockResource(Resource & resource, core::uint offsetBytes,
	                           core::uint sizeBytes, core::uint accessFlags);

	// Same as the above, but when applied to a buffer, locks the whole buffer data.
	// Flags are the Resource::Flag::Lock* set. Use `unlockResource()` to release the lock.
	core::ubyte * lockResource(Resource & resource, core::uint accessFlags);

	// Unlocks the resource data previously locked/mapped by `lockResource()`.
	bool unlockResource(Resource & resource, const core::ubyte * dataPtr);

	// Manually frees the underlaying rendering data of any type
	// of renderer resource. This will invalidate the resource
	// handle(s) but will not change any other object states.
	void freeResource(Resource & resource);

	//
	// Draw calls:
	//

	// Draws unindexed primitives sourcing data from the currently
	// bound VertexBuffer. Equivalent to glDrawArrays.
	void drawVertexBufferUnindexed(RenderMode::Enum mode,
	                               core::uint firstVertex,
	                               core::uint vertexCount,
	                               core::uint sizeOfAVertex);

	// Draws indexed primitives sourcing data from the currently
	// bound IndexedVertexBuffer. Equivalent to glDrawElements.
	void drawVertexBufferIndexed(RenderMode::Enum mode,
	                             core::uint firstIndex,
	                             core::uint indexCount,
	                             core::uint sizeOfAnIndex);

	// Draws indexed primitives sourcing data from the currently bound IndexedVertexBuffer.
	// Allows supplying a per-index offset (the base vertex). Equivalent to glDrawElementsBaseVertex.
	void drawVertexBufferIndexedBaseVertex(RenderMode::Enum mode,
	                                       core::uint firstIndex,
	                                       core::uint indexCount,
	                                       core::uint sizeOfAnIndex,
	                                       core::uint baseVertex);

	//
	// Error reporting/checking:
	//

	// Checks and prints any recent RHI back-end errors to the RenderLog, clearing the error list.
	// This function might require some expensive synchronization between the front-end
	// and back-end, so it should be called infrequently outside of a debug build.
	// Note: Setting `runtimeErrorChecks` to false turns this into a no-op.
	void checkRhiBackEndErrors(const char * function, const char * filename, int lineNum);

	// Clears the error list without logging any errors.
	void clearRhiBackEndErrors();

private:

	//
	// opengl/rhi_manager.cpp:
	//  Misc GL helpers | initialization/shutdown.
	//

	void internalReset();
	void queryGLInfo();
	void setDefaultGLStates();

	//
	// opengl/rhi_manager_texture.cpp:
	//  Internal texture and sampling helpers.
	//

	GLuint newGLTexHandle();
	void freeGLTexture(Texture & texture);

	void refreshGLTexParameters(Texture & texture, GLenum glTarget);
	void refreshGLTexFilter(Texture & texture, GLenum glTarget);
	void refreshGLTexAddressing(const Texture & texture, GLenum glTarget);
	void refreshGLTexMipmapIndexes(const Texture & texture, GLenum glTarget);
	void refreshGLTexMipLodBias(const Texture & texture, GLenum glTarget);

	void queryMaxGLTexAnisotropy();
	void clearGLTexBindingIfCurrent(const Texture & texture);
	void setGLTexSwizzle(Texture::DataLayout::Enum layout, GLenum glTarget);
	void setGLPixelAlignment(GLenum packAlign, Texture::DataLayout::Enum layout, GLuint width);

	//
	// opengl/rhi_manager_program.cpp:
	//  Internal program/shader helpers.
	//

	GLuint newGLProgHandle();
	GLuint newGLShaderHandle(GLenum type);
	void freeGLProgram(Program & program);

	void commitGLProgUniforms(Program & program);
	void queryGLProgUniformsAndAttributes(Program & program);
	void queryGLProgUniformVars(Program & program, GLint uniformVarCount, GLuint glProgHandle);
	void queryGLProgVertexAttributes(Program & program, GLint attributeCount, GLuint glProgHandle);

	bool gatherGLShaderInfoLogs(GLuint glProgHandle, GLuint glVsHandle,
	                            GLuint glFsHandle, ProgramSetupResult * result);

	//
	// opengl/rhi_manager_buffers.cpp:
	//  Internal vertex/index buffer helpers.
	//

	GLuint newGLVaoHandle();
	GLuint newGLVboHandle();
	GLuint newGLIboHandle();
	void freeGLVertexBuffer(VertexBuffer & vertexBuffer);
	void freeGLIndexedVertexBuffer(IndexedVertexBuffer & iVertexBuffer);

	bool allocGLBufferHandles(GLuint * glVaoHandle, GLuint * glVboHandle, GLuint * glIboHandle);
	void bindGLBuffersNoCachingTest(const GLuint * glVaoHandle, const GLuint * glVboHandle, const GLuint * glIboHandle);
	void setGLVertexLayout(const ProgramVertexLayout & layout, core::uint vertexStride);

	void helperLockGLBuffer(Resource & resource, core::uint accessFlags, core::uint * resourceLockModeFlags,
	                        GLenum * glTarget, GLenum * glAccessMapBuffer, GLbitfield * glAccessMapBufferRange);

	bool helperUpdateGLBuffer(GLenum target, GLuint glVaoHandle, GLuint glBufferHandle,
	                          GLenum bufferUsage, GLuint currentSizeBytes, GLuint offsetBytes,
	                          GLuint dataSizeBytes, const GLvoid * newData, bool discardCurrentData);

	//
	// opengl/rhi_manager_framebuf.cpp:
	//  Internal GL frame buffer helpers.
	//

	GLuint newGLFboHandle();
	GLuint newGLRboHandle();
	void freeGLFrameBuffer(FrameBuffer & frameBuffer);

	void initScreenFrameBuffer();
	bool validateCurrentGLFrameBuffer();

	bool createGLFrameBufferColorTextures(FrameBuffer & frameBuffer, core::Vec2u rtDimensions,
	                                      Texture::DataLayout::Enum rtLayout, Texture::Filter::Enum rtFilter,
	                                      core::uint rtCount);

	bool createGLFrameBufferDS(FrameBuffer & frameBuffer, core::Vec2u rtDimensions,
	                           Texture::Filter::Enum rtFilter, bool withDepth,
	                           bool withStencil, bool forceRhiSurfacesDS);

private:

	// We can only have one instance of this class,
	// since OpenGL is implicitly also a singleton.
	static bool initializedGL;
	static bool instanceCreated;

	// Render window we output to and a ref to the context params for quick access.
	shell::RenderWindow * renderWindow;
	const shell::RenderContextParams * contexParams;

	// GL feature set info:
	GLint majorGLVersion;
	GLint minorGLVersion;
	GLint versionNumGLSL;
	GLint maxGLAnisotropy;
	GLint maxGLTextureSize;
	GLint maxGLDrawBuffers;

	// Global renderer configurations:
	bool  runtimeErrorChecks;
	GLint textureFilter; // One of the Texture::Filter constants.

	// GL state caches:
	GLuint currentGLProg;
	GLuint currentGLVao;
	GLuint currentGLVbo;
	GLuint currentGLIbo;
	GLuint currentGLFbo;
	core::SizedArray<GLuint, Texture::MappingUnit::Count> currentGLTextures;

	// Pointer to one of the `vertexLayouts`. Initially null.
	const ProgramVertexLayout * currentVertexLayout;

	// Values used to clear the current frame buffer:
	core::Vec4 clearColor;   // default = [0, 0, 0, 1.0]
	GLfloat    clearDepth;   // default = 0
	GLint      clearStencil; // default = 0

	// This object is the dummy screen FrameBuffer, AKA glFramebuffer 0.
	FrameBuffer screenFrameBuffer;

	// Color attachments for glDrawBuffers. GL_NONE if disabled, GL_COLOR_ATTACHMENTi otherwise.
	// We limit this to 16, which is double the minimum, and so avoid a dynamic allocation.
	core::SizedArray<GLenum, 16> drawBuffers;

	// Vertex layout registry. This is useful since most vertex shaders
	// will share the exact same vertex layout. Same it true for most geometries.
	GLuint vertexLayoutCount;
	core::SizedArray<ProgramVertexLayout, 8> vertexLayouts;
};

// ========================================================
// R_CHECK_GL_ERRORS() internal use macro:
// ========================================================

//
// Can be optionally disabled permanently by defining the `RHI_NO_GL_ERROR_CHECKING`
// macro or during runtime by setting `runtimeErrorChecks` to false.
//
// Note that this can only be called from whiting RhiManagerImpl,
// which is where all GL calls must be anyways.
//
#ifndef RHI_NO_GL_ERROR_CHECKING
	#define R_CHECK_GL_ERRORS() \
		do { \
			if (runtimeErrorChecks) \
			{ \
				checkRhiBackEndErrors(__FUNCTION__, __FILE__, __LINE__); \
			} \
		} while (0)
#else // !RHI_NO_GL_ERROR_CHECKING
	#define R_CHECK_GL_ERRORS()
#endif // RHI_NO_GL_ERROR_CHECKING

} // namespace ogl {}
} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_OPENGL_RHI_MANAGER_HPP
