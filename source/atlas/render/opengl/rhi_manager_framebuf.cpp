
// ================================================================================================
// -*- C++ -*-
// File: rhi_manager_framebuf.cpp
// Author: Guilherme R. Lampert
// Created on: 20/07/15
// Brief: RhiManagerImpl methods related to frame buffer management.
// ================================================================================================

#include "atlas/render/opengl/rhi_manager.hpp"
#include "atlas/render/render_log.hpp"

namespace atlas
{
namespace render
{
namespace ogl
{

// ================================================================================================
// ogl::RhiManagerImpl -- Frame Buffer management.
// ================================================================================================

#define R_BIND_GL_FBO_IF_NOT_CURRENT(fbo) \
	do { \
		if ((fbo) != currentGLFbo) \
		{ \
			currentGLFbo = (fbo); \
			glBindFramebuffer(GL_FRAMEBUFFER, (fbo)); \
		} \
	} while (0)

#define R_CLEAR_GL_FBO_IF_CURRENT(fbo) \
	do { \
		if ((fbo) == currentGLFbo) \
		{ \
			currentGLFbo = 0; \
			glBindFramebuffer(GL_FRAMEBUFFER, 0); \
		} \
	} while (0)

#define R_BIND_GL_FBO_NO_CACHING_TEST(fbo) \
	do { \
		currentGLFbo = (fbo); \
		glBindFramebuffer(GL_FRAMEBUFFER, (fbo)); \
	} while (0)

// ========================================================
// RhiManagerImpl::allocateFrameBuffer():
// ========================================================

bool RhiManagerImpl::allocateFrameBuffer(FrameBuffer & destFrameBuffer, const core::uint resourceFlags,
                                         const core::Vec2u rtDimensions, const Texture::DataLayout::Enum rtLayout,
                                         const Texture::Filter::Enum rtFilter, const core::uint rtCount,
                                         const bool withDepth, const bool withStencil, const bool forceRhiSurfacesDS)
{
	DEBUG_CHECK(isInitialized());

	DEBUG_CHECK(rtDimensions.x != 0);
	DEBUG_CHECK(rtDimensions.y != 0);
	DEBUG_CHECK(rtCount        != 0);

	DEBUG_CHECK((destFrameBuffer.resourceFlags & Resource::Flag::TypeFrameBuffer) && "Inconsistent flags!");
	DEBUG_CHECK(!destFrameBuffer.isResourceLocked() && "A frame buffer can't be in a locked state!");

	if (rtCount > unsigned(maxGLDrawBuffers))
	{
		R_LOG_ERROR("Requested a new frame buffer with " << rtCount
			<< " color render targets, but the max supported is "
			<< maxGLDrawBuffers << ".");

		return false;
	}

	// Recycle the GL handle if possible.
	GLuint glFboHandle = destFrameBuffer.fboHandleGL;
	if (glFboHandle == 0)
	{
		glFboHandle = newGLFboHandle();
		if (glFboHandle == 0)
		{
			return false; // newGLFboHandle() already logs the GL errors.
		}
	}

	R_BIND_GL_FBO_NO_CACHING_TEST(glFboHandle);

	if (!createGLFrameBufferColorTextures(destFrameBuffer, rtDimensions, rtLayout, rtFilter, rtCount))
	{
		R_BIND_GL_FBO_NO_CACHING_TEST(0);
		glDeleteFramebuffers(1, &glFboHandle);

		R_LOG_WARN("Failed to allocate one or more frame buffer color textures!");
		return false;
	}

	if (!createGLFrameBufferDS(destFrameBuffer, rtDimensions, rtFilter, withDepth, withStencil, forceRhiSurfacesDS))
	{
		R_BIND_GL_FBO_NO_CACHING_TEST(0);
		glDeleteFramebuffers(1, &glFboHandle);

		R_LOG_WARN("Failed to allocate frame buffer depth/stencil render targets!");
		return false;
	}

	if (!validateCurrentGLFrameBuffer())
	{
		R_CHECK_GL_ERRORS();
		R_LOG_WARN("glCheckFramebufferStatus() failed for the new frame buffer! View log for more info.");

		// Allow it to continue anyways. Will not be valid for rendering.
		destFrameBuffer.validatedOk = false;
	}
	else
	{
		destFrameBuffer.validatedOk = true;
	}

	// Restore the default screen FBO/RBO:
	R_BIND_GL_FBO_NO_CACHING_TEST(0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	destFrameBuffer.fboHandleGL        = glFboHandle;
	destFrameBuffer.dimensions         = rtDimensions;
	destFrameBuffer.forceRhiSurfacesDS = forceRhiSurfacesDS;
	destFrameBuffer.isScreenFbo        = false; // User can't create a screen FBO!
	destFrameBuffer.resourceFlags      = resourceFlags | Resource::Flag::TypeFrameBuffer;

	R_LOG_INFO("New frame buffer allocated: " << rtDimensions.x << "x" << rtDimensions.y
		<< ", d=" << withDepth << ", s=" << withStencil << ", renderbuffers="
		<< forceRhiSurfacesDS << ", RTs=" << rtCount << ".");

	return true;
}

// ========================================================
// RhiManagerImpl::setFrameBuffer():
// ========================================================

void RhiManagerImpl::setFrameBuffer(const FrameBuffer & frameBuffer)
{
	DEBUG_CHECK(isInitialized());

	if (!frameBuffer.isValid())
	{
		// Can't set a default that would provide some visual debugging
		// aid, so we at least log a warning to let the caller know.
		R_LOG_WARN("Trying to bind an invalid FrameBuffer!");
		return;
	}

	DEBUG_CHECK((frameBuffer.resourceFlags & Resource::Flag::TypeFrameBuffer) && "Inconsistent flags!");
	DEBUG_CHECK(!frameBuffer.isResourceLocked() && "A frame buffer can't be in a locked state!");

	R_BIND_GL_FBO_IF_NOT_CURRENT(frameBuffer.fboHandleGL);

	// The screen has only one color render target, the back-buffer, which is always enabled.
	if (!frameBuffer.isScreenFbo)
	{
		glDrawBuffers(maxGLDrawBuffers, drawBuffers.getData());
	}
}

// ========================================================
// RhiManagerImpl::setFrameBufferToScreenFrameBuffer():
// ========================================================

void RhiManagerImpl::setFrameBufferToScreenFrameBuffer()
{
	DEBUG_CHECK(isInitialized());
	setFrameBuffer(screenFrameBuffer);
}

// ========================================================
// RhiManagerImpl::getScreenFrameBuffer():
// ========================================================

const FrameBuffer & RhiManagerImpl::getScreenFrameBuffer() const
{
	DEBUG_CHECK(isInitialized());
	return screenFrameBuffer;
}

// ========================================================
// RhiManagerImpl::clearFrameBuffer():
// ========================================================

void RhiManagerImpl::clearFrameBuffer(const bool color, const bool depth, const bool stencil)
{
	DEBUG_CHECK(isInitialized());

	GLbitfield clearFlags = 0;
	if (color)   { clearFlags |= GL_COLOR_BUFFER_BIT;   }
	if (depth)   { clearFlags |= GL_DEPTH_BUFFER_BIT;   }
	if (stencil) { clearFlags |= GL_STENCIL_BUFFER_BIT; }

	glClear(clearFlags);
}

// ========================================================
// RhiManagerImpl::setClearColorValue():
// ========================================================

void RhiManagerImpl::setClearColorValue(const core::Color value)
{
	DEBUG_CHECK(isInitialized());

	clearColor = value.toVec4();
	glClearColor(clearColor.x, clearColor.y, clearColor.z, clearColor.w);
}

// ========================================================
// RhiManagerImpl::setClearDepthValue():
// ========================================================

void RhiManagerImpl::setClearDepthValue(const float value)
{
	DEBUG_CHECK(isInitialized());

	clearDepth = value;
	glClearDepth(clearDepth);
}

// ========================================================
// RhiManagerImpl::setClearStencilValue():
// ========================================================

void RhiManagerImpl::setClearStencilValue(const int value)
{
	DEBUG_CHECK(isInitialized());

	clearStencil = value;
	glClearStencil(clearStencil);
}

// ========================================================
// RhiManagerImpl::getColorRenderTargetCount():
// ========================================================

core::uint RhiManagerImpl::getColorRenderTargetCount() const
{
	return maxGLDrawBuffers;
}

// ========================================================
// RhiManagerImpl::enableColorRenderTarget():
// ========================================================

void RhiManagerImpl::enableColorRenderTarget(const core::uint index)
{
	DEBUG_CHECK(index < unsigned(maxGLDrawBuffers) && "Invalid render target index!");
	drawBuffers[index] = GL_COLOR_ATTACHMENT0 + index;
}

// ========================================================
// RhiManagerImpl::disableColorRenderTarget():
// ========================================================

void RhiManagerImpl::disableColorRenderTarget(const core::uint index)
{
	DEBUG_CHECK(index < unsigned(maxGLDrawBuffers) && "Invalid render target index!");
	drawBuffers[index] = GL_NONE;
}

// ========================================================
// RhiManagerImpl::newGLFboHandle():
// ========================================================

GLuint RhiManagerImpl::newGLFboHandle()
{
	GLuint newFbo = 0;
	glGenFramebuffers(1, &newFbo);

	if (newFbo == 0)
	{
		R_LOG_ERROR("Failed to allocate a new GL FBO handle! Possibly out-of-memory!");
		R_CHECK_GL_ERRORS();
	}
	return newFbo;
}

// ========================================================
// RhiManagerImpl::newGLRboHandle():
// ========================================================

GLuint RhiManagerImpl::newGLRboHandle()
{
	GLuint newRbo = 0;
	glGenRenderbuffers(1, &newRbo);

	if (newRbo == 0)
	{
		R_LOG_ERROR("Failed to allocate a new GL RBO handle! Possibly out-of-memory!");
		R_CHECK_GL_ERRORS();
	}
	return newRbo;
}

// ========================================================
// RhiManagerImpl::freeGLFrameBuffer():
// ========================================================

void RhiManagerImpl::freeGLFrameBuffer(FrameBuffer & frameBuffer)
{
	R_LOG_INFO("Freeing a GL FrameBuffer...");

	if (frameBuffer.isScreenFbo)
	{
		return;
	}

	const GLuint glFboHandle = frameBuffer.fboHandleGL;
	if (glFboHandle == 0)
	{
		return;
	}

	R_CLEAR_GL_FBO_IF_CURRENT(glFboHandle);
	glDeleteFramebuffers(1, &glFboHandle);

	// Free the RT textures and/or GL renderbuffers:
	if (frameBuffer.forceRhiSurfacesDS)
	{
		GLuint glRboHandles[3];
		glRboHandles[0] = frameBuffer.depthRenderTarget.asRhiSurface;
		glRboHandles[1] = frameBuffer.stencilRenderTarget.asRhiSurface;
		glRboHandles[2] = frameBuffer.depthStencilRenderTarget.asRhiSurface;

		// Make sure a bound object doesn't hang around...
		glBindRenderbuffer(GL_RENDERBUFFER, 0);

		// According to documentation, it "silently ignores 0's and names that
		// do not correspond to existing renderbuffer objects", so this should be
		// fine even if the above were not in use by the FrameBuffer (= 0's).
		glDeleteRenderbuffers(3, glRboHandles);

		frameBuffer.depthRenderTarget.asRhiSurface        = 0;
		frameBuffer.stencilRenderTarget.asRhiSurface      = 0;
		frameBuffer.depthStencilRenderTarget.asRhiSurface = 0;
	}
	else
	{
		frameBuffer.freeDSTextures();
	}

	// Always free the color textures.
	frameBuffer.freeColorTextures();

	// We clear these anyway to be sure they are not accidentally reused.
	frameBuffer.fboHandleGL        = 0;
	frameBuffer.dimensions.x       = 0;
	frameBuffer.dimensions.y       = 0;
	frameBuffer.forceRhiSurfacesDS = false;
	frameBuffer.isScreenFbo        = false;
	frameBuffer.validatedOk        = false;
}

// ========================================================
// RhiManagerImpl::initScreenFrameBuffer():
// ========================================================

void RhiManagerImpl::initScreenFrameBuffer()
{
	screenFrameBuffer.freeColorTextures();

	screenFrameBuffer.fboHandleGL        = 0;
	screenFrameBuffer.forceRhiSurfacesDS = true;
	screenFrameBuffer.isScreenFbo        = true;
	screenFrameBuffer.validatedOk        = true;
	screenFrameBuffer.dimensions         = getScreenDimensions();
	screenFrameBuffer.resourceFlags      = Resource::Flag::TypeFrameBuffer;

	screenFrameBuffer.depthRenderTarget.asRhiSurface        = 0;
	screenFrameBuffer.stencilRenderTarget.asRhiSurface      = 0;
	screenFrameBuffer.depthStencilRenderTarget.asRhiSurface = 0;
}

// ========================================================
// RhiManagerImpl::createGLFrameBufferColorTextures():
// ========================================================

bool RhiManagerImpl::createGLFrameBufferColorTextures(FrameBuffer & frameBuffer, const core::Vec2u rtDimensions,
                                                      const Texture::DataLayout::Enum rtLayout,
                                                      const Texture::Filter::Enum rtFilter,
                                                      const core::uint rtCount)
{
	core::PtrArray<Texture> newTextures;
	newTextures.allocateExact(rtCount);
	newTextures.resize(rtCount);

	for (core::uint i = 0; i < rtCount; ++i)
	{
		newTextures[i] = new Texture();
		if (!allocateTexture(*newTextures[i], 0, rtDimensions, rtLayout, Texture::Target::Renderer, /* mipmaps = */ 1))
		{
			R_LOG_WARN("Failed to allocate frame buffer color render target #"
				<< i << "! Aborting FBO allocation.");

			return false;
		}

		newTextures[i]->setFilter(rtFilter);
		newTextures[i]->setAddressing(Texture::Addressing::ClampToEdge);
		newTextures[i]->setTexUnit(static_cast<Texture::MappingUnit::Enum>(i));

		// Ensure the sampling parameters are committed immediately.
		setTexture(*newTextures[i]);
	}

	// All went well if we get here. Attach the textures to the GL FBO:
	for (core::uint i = 0; i < newTextures.getSize(); ++i)
	{
		DEBUG_CHECK(newTextures[i] != nullptr);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i,
			GL_TEXTURE_2D, newTextures[i]->texHandleGL, /* level = */ 0);
	}
	R_CHECK_GL_ERRORS();

	// Move our application-side pointers to the
	// output frame buffer object and we are done.
	frameBuffer.freeColorTextures(); // Ensure cleared if the object is by any chance being recycled.
	newTextures.transferTo(frameBuffer.colorRenderTargets);

	return true;
}

// ========================================================
// RhiManagerImpl::createGLFrameBufferDS():
// ========================================================

bool RhiManagerImpl::createGLFrameBufferDS(FrameBuffer & frameBuffer, const core::Vec2u rtDimensions,
                                           const Texture::Filter::Enum rtFilter, const bool withDepth,
                                           const bool withStencil, const bool forceRhiSurfacesDS)
{
	if (forceRhiSurfacesDS)
	{
		// FIXME Hardcoded for now. Might need to infer these during runtime for some GL versions...
		constexpr GLenum bestGLDepthFormat        = GL_DEPTH_COMPONENT32;
		constexpr GLenum bestGLStencilFormat      = GL_STENCIL_INDEX8;
		constexpr GLenum bestGLDepthStencilFormat = GL_DEPTH_STENCIL;

		GLuint glRboDepth        = 0;
		GLuint glRboStencil      = 0;
		GLuint glRboDepthStencil = 0;

		if (withDepth && !withStencil)
		{
			// Allocate and attach optional depth buffer (no stencil):
			glRboDepth = newGLRboHandle();
			glBindRenderbuffer(GL_RENDERBUFFER, glRboDepth);

			glRenderbufferStorage(GL_RENDERBUFFER, bestGLDepthFormat, rtDimensions.x, rtDimensions.y);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, glRboDepth);
		}
		else if (withStencil && !withDepth)
		{
			// Allocate and attach optional stencil buffer (no depth buffer):
			glRboStencil = newGLRboHandle();
			glBindRenderbuffer(GL_RENDERBUFFER, glRboStencil);

			glRenderbufferStorage(GL_RENDERBUFFER, bestGLStencilFormat, rtDimensions.x, rtDimensions.y);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, glRboStencil);
		}
		else if (withDepth && withStencil)
		{
			// Allocate and attach optional depth AND stencil buffers:
			glRboDepthStencil = newGLRboHandle();
			glBindRenderbuffer(GL_RENDERBUFFER, glRboDepthStencil);

			glRenderbufferStorage(GL_RENDERBUFFER, bestGLDepthStencilFormat, rtDimensions.x, rtDimensions.y);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, glRboDepthStencil);
		}

		frameBuffer.depthRenderTarget.asRhiSurface        = glRboDepth;
		frameBuffer.stencilRenderTarget.asRhiSurface      = glRboStencil;
		frameBuffer.depthStencilRenderTarget.asRhiSurface = glRboDepthStencil;

		R_CHECK_GL_ERRORS();
		return true;
	}
	else // Use depth/stencil renderable textures.
	{
		//TODO
		// note: use targets DepthBuffer/StencilBuffer/etc
		(void)rtFilter;
		R_LOG_FATAL("Not implemented!");
	}
}

// ========================================================
// RhiManagerImpl::validateCurrentGLFrameBuffer():
// ========================================================

bool RhiManagerImpl::validateCurrentGLFrameBuffer()
{
	const GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status == GL_FRAMEBUFFER_COMPLETE)
	{
		return true;
	}

	switch (status)
	{
	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT :
		R_LOG_ERROR("FrameBuffer error: GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT :
		R_LOG_ERROR("FrameBuffer error: GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER :
		R_LOG_ERROR("FrameBuffer error: GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER :
		R_LOG_ERROR("FrameBuffer error: GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER");
		break;

	case GL_FRAMEBUFFER_UNSUPPORTED :
		R_LOG_ERROR("FrameBuffer error: GL_FRAMEBUFFER_UNSUPPORTED");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE :
		R_LOG_ERROR("FrameBuffer error: GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS :
		R_LOG_ERROR("FrameBuffer error: GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS");
		break;

	default :
		R_LOG_ERROR("Unknown FrameBuffer status: " << core::toString(status, 16));
		break;
	} // switch (status)

	return false;
}

} // namespace ogl {}
} // namespace render {}
} // namespace atlas {}
