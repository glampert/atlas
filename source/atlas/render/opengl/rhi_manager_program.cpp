
// ================================================================================================
// -*- C++ -*-
// File: rhi_manager_program.cpp
// Author: Guilherme R. Lampert
// Created on: 18/07/15
// Brief: RhiManagerImpl methods related to render program management.
// ================================================================================================

#include "atlas/render/opengl/rhi_manager.hpp"
#include "atlas/render/render_log.hpp"

namespace atlas
{
namespace render
{
namespace ogl
{

// ================================================================================================
// ogl::RhiManagerImpl -- Render Program management.
// ================================================================================================

#define R_BIND_GL_PROGRAM_IF_NOT_CURRENT(prog) \
	do { \
		if ((prog) != currentGLProg) \
		{ \
			currentGLProg = (prog); \
			glUseProgram((prog)); \
		} \
	} while (0)

#define R_CLEAR_GL_PROGRAM_IF_CURRENT(prog) \
	do { \
		if ((prog) == currentGLProg) \
		{ \
			currentGLProg = 0; \
			glUseProgram(0); \
		} \
	} while (0)

// ========================================================
// RhiManagerImpl::allocateProgram():
// ========================================================

bool RhiManagerImpl::allocateProgram(Program & destProgram, const core::uint resourceFlags,
                                     const ProgramStage * stages, const core::uint stageCount,
                                     ProgramSetupResult * optionalResult)
{
	DEBUG_CHECK(isInitialized());

	DEBUG_CHECK(stages != nullptr);
	DEBUG_CHECK(stageCount >= 2);

	DEBUG_CHECK((destProgram.resourceFlags & Resource::Flag::TypeProgram) && "Inconsistent flags!");
	DEBUG_CHECK(!destProgram.isResourceLocked() && "A render program can't be in a locked state!");

	GLint vertexStages   = 0;
	GLint fragmentStages = 0;
	GLint vsLength       = 0;
	GLint fsLength       = 0;
	const GLchar * vsSrc = nullptr;
	const GLchar * fsSrc = nullptr;

	// Validate the provided stages. We require a Vertex and a Fragment stage.
	for (core::uint s = 0; s < stageCount; ++s)
	{
		if (stages[s].stageType == ProgramStage::Vertex)
		{
			if (stages[s].nativeCode != nullptr && stages[s].nativeCodeSizeBytes != 0)
			{
				++vertexStages;
				vsSrc    = reinterpret_cast<const GLchar *>(stages[s].nativeCode);
				vsLength = stages[s].nativeCodeSizeBytes; // Length not counting the null terminating char.
			}
		}
		else if (stages[s].stageType == ProgramStage::Fragment)
		{
			if (stages[s].nativeCode != nullptr && stages[s].nativeCodeSizeBytes != 0)
			{
				++fragmentStages;
				fsSrc    = reinterpret_cast<const GLchar *>(stages[s].nativeCode);
				fsLength = stages[s].nativeCodeSizeBytes; // Length not counting the null terminating char.
			}
		}
		else
		{
			R_LOG_FATAL("Unknown render program stage type!");
		}
	}

	// Exactly one of each is required:
	if (vertexStages != 1 || fragmentStages != 1)
	{
		if (optionalResult != nullptr)
		{
			optionalResult->infoLog.clear();
			optionalResult->state = ProgramSetupResult::MissingStageError;
		}
		R_LOG_ERROR("Missing, invalid or excessive render program stages: vs=" << vertexStages << ", fs=" << fragmentStages);
		return false;
	}

	// Recycle the GL handle if possible.
	GLuint glProgHandle = destProgram.programHandleGL;
	if (glProgHandle == 0)
	{
		glProgHandle = newGLProgHandle();
		if (glProgHandle == 0)
		{
			if (optionalResult != nullptr)
			{
				optionalResult->infoLog.clear();
				optionalResult->state = ProgramSetupResult::OutOfMemoryError;
			}
			return false; // newGLProgHandle() already logs the GL errors.
		}
	}

	const GLuint glVsHandle = newGLShaderHandle(GL_VERTEX_SHADER);
	const GLuint glFsHandle = newGLShaderHandle(GL_FRAGMENT_SHADER);

	if (glVsHandle == 0 || glFsHandle == 0)
	{
		glDeleteProgram(glProgHandle);

		if (glIsShader(glVsHandle))
		{
			glDeleteShader(glVsHandle);
		}
		if (glIsShader(glFsHandle))
		{
			glDeleteShader(glFsHandle);
		}

		if (optionalResult != nullptr)
		{
			optionalResult->infoLog.clear();
			optionalResult->state = ProgramSetupResult::OutOfMemoryError;
		}
		R_LOG_ERROR("Issues allocating a GL program: Failed to create the shader handles!");
		return false;
	}

	R_LOG_INFO("Compiling shaders and linking new GL program...");

	// Vertex shader:
	glShaderSource(glVsHandle, 1, &vsSrc, &vsLength);
	glCompileShader(glVsHandle);
	glAttachShader(glProgHandle, glVsHandle);

	// Fragment shader:
	glShaderSource(glFsHandle, 1, &fsSrc, &fsLength);
	glCompileShader(glFsHandle);
	glAttachShader(glProgHandle, glFsHandle);

	// Link the Shader Program then check and print the info logs, if any.
	glLinkProgram(glProgHandle);
	const bool progOK = gatherGLShaderInfoLogs(glProgHandle, glVsHandle, glFsHandle, optionalResult);

	// After a program is linked the shader objects can be safely detached and deleted.
	// This is also recommended to save some memory that would be wasted by keeping the shaders alive.
	glDetachShader(glProgHandle, glVsHandle);
	glDetachShader(glProgHandle, glFsHandle);
	glDeleteShader(glVsHandle);
	glDeleteShader(glFsHandle);

	R_CHECK_GL_ERRORS();
	if (!progOK)
	{
		R_LOG_ERROR("Unable to allocate new render program due to previous errors.");
		glDeleteProgram(glProgHandle);
		return false;
	}

	destProgram.programHandleGL = glProgHandle;
	destProgram.resourceFlags   = resourceFlags | Resource::Flag::TypeProgram;

	// Fills renderParamValues and renderParamNames.
	queryGLProgUniformsAndAttributes(destProgram);

	R_LOG_INFO("New render program allocated: " << stageCount << " stages, "
		<< destProgram.getVertexLayout().vertexElementCount << " vertex inputs and "
		<< destProgram.getRenderParamCount() << " render params.");

	return true;
}

// ========================================================
// RhiManagerImpl::setProgram():
// ========================================================

void RhiManagerImpl::setProgram(const Program & program)
{
	DEBUG_CHECK(isInitialized());

	if (!program.isValid())
	{
		//TODO
		// bind a default program that can render something
		// easy to visualize as an error.
		return;
	}

	DEBUG_CHECK((program.resourceFlags & Resource::Flag::TypeProgram) && "Inconsistent flags!");
	DEBUG_CHECK(!program.isResourceLocked() && "A render program can't be in a locked state!");

	R_BIND_GL_PROGRAM_IF_NOT_CURRENT(program.programHandleGL);

	// Keep track of this for the vertex buffers.
	currentVertexLayout = program.vertexLayout;
	DEBUG_CHECK(currentVertexLayout != nullptr);

	// Might need to download the uniform variables to the GL.
	if (program.renderParamsDirty)
	{
		// We hack the constness out of the object to be able to clear the dirty flags
		// for each render param. Other object states or program data are not touched.
		commitGLProgUniforms(const_cast<Program &>(program));
	}
}

// ========================================================
// RhiManagerImpl::commitProgramRenderParams():
// ========================================================

void RhiManagerImpl::commitProgramRenderParams(const Program & program)
{
	DEBUG_CHECK(isInitialized());

	if (!program.isValid())
	{
		R_LOG_WARN("Can't commit render params for an invalid program!");
		return;
	}

	DEBUG_CHECK((program.resourceFlags & Resource::Flag::TypeProgram) && "Inconsistent flags!");
	DEBUG_CHECK(!program.isResourceLocked() && "A render program can't be in a locked state!");

	if (program.renderParamsDirty)
	{
		// Might need to bind it first.
		R_BIND_GL_PROGRAM_IF_NOT_CURRENT(program.programHandleGL);

		// Commit and clear the dirty flags.
		//
		// We hack the constness out of the object to be able to clear the dirty flags
		// for each render param. Other object states or program data are not touched.
		commitGLProgUniforms(const_cast<Program &>(program));
	}
}

// ========================================================
// RhiManagerImpl::findOrRegisterVertexLayout():
// ========================================================

const ProgramVertexLayout & RhiManagerImpl::findOrRegisterVertexLayout(const ProgramVertexLayout & templateLayout)
{
	DEBUG_CHECK(isInitialized());

	for (GLuint i = 0; i < vertexLayoutCount; ++i)
	{
		if (vertexLayouts[i] == templateLayout)
		{
			return vertexLayouts[i];
		}
	}

	// Got here if the input is not yet registered.
	if (vertexLayoutCount == vertexLayouts.getSize())
	{
		R_LOG_FATAL("No more vertex layouts can be registered!");
	}

	vertexLayouts[vertexLayoutCount] = templateLayout;
	return vertexLayouts[vertexLayoutCount++];
}

// ========================================================
// RhiManagerImpl::newGLProgHandle():
// ========================================================

GLuint RhiManagerImpl::newGLProgHandle()
{
	const GLuint newProg = glCreateProgram();
	if (newProg == 0)
	{
		R_LOG_ERROR("Failed to allocate a new GL program handle! Possibly out-of-memory!");
		R_CHECK_GL_ERRORS();
	}
	return newProg;
}

// ========================================================
// RhiManagerImpl::newGLShaderHandle():
// ========================================================

GLuint RhiManagerImpl::newGLShaderHandle(const GLenum type)
{
	const GLuint newShader = glCreateShader(type);
	if (newShader == 0)
	{
		R_LOG_ERROR("Failed to allocate a new GL shader handle! Possibly out-of-memory!");
		R_CHECK_GL_ERRORS();
	}
	return newShader;
}

// ========================================================
// RhiManagerImpl::freeGLProgram():
// ========================================================

void RhiManagerImpl::freeGLProgram(Program & program)
{
	R_LOG_INFO("Freeing a GL Render Program...");

	const GLuint glProgHandle = program.programHandleGL;
	if (glProgHandle == 0)
	{
		return;
	}

	R_CLEAR_GL_PROGRAM_IF_CURRENT(glProgHandle);
	glDeleteProgram(glProgHandle);
	program.programHandleGL = 0;
}

// ========================================================
// RhiManagerImpl::commitGLProgUniforms():
// ========================================================

void RhiManagerImpl::commitGLProgUniforms(Program & program)
{
	// Program must be already bound!

	const core::uint paramCount = program.renderParamValues.getSize();
	for (core::uint i = 0; i < paramCount; ++i)
	{
		Program::ParamData & rp = program.renderParamValues[i];
		DEBUG_CHECK(rp.isValid());

		if (!rp.isDirty)
		{
			continue;
		}

		switch (rp.type)
		{
		case Program::ParamData::Int  :
		case Program::ParamData::Bool :
		case Program::ParamData::SamplerIdx :
			glUniform1i(rp.uniformLocGL, rp.value.asInt);
			break;

		case Program::ParamData::Float :
			glUniform1f(rp.uniformLocGL, rp.value.asFloat);
			break;

		case Program::ParamData::V2F :
			glUniform2f(rp.uniformLocGL, rp.value.asVec2.x, rp.value.asVec2.y);
			break;

		case Program::ParamData::V3F :
			glUniform3f(rp.uniformLocGL, rp.value.asVec3.x, rp.value.asVec3.y, rp.value.asVec3.z);
			break;

		case Program::ParamData::V4F :
			glUniform4f(rp.uniformLocGL, rp.value.asVec4.x, rp.value.asVec4.y, rp.value.asVec4.z, rp.value.asVec4.w);
			break;

		case Program::ParamData::Matrix :
			{
				const Program::ParamData::MatrixRef & mat = rp.value.asMatrixRef;
				DEBUG_CHECK(mat.data != nullptr);
				switch (mat.squareSize)
				{
				case 2 : glUniformMatrix2fv(rp.uniformLocGL, 1, GL_FALSE, mat.data); break;
				case 3 : glUniformMatrix3fv(rp.uniformLocGL, 1, GL_FALSE, mat.data); break;
				case 4 : glUniformMatrix4fv(rp.uniformLocGL, 1, GL_FALSE, mat.data); break;
				default : R_LOG_FATAL("Square matrix size must be 2, 3 or 4!"); break;
				} // switch (mat.squareSize)
				break;
			}
		default :
			R_LOG_FATAL("Invalid render parameter type!");
		} // switch (rp.type)

		rp.isDirty = false;
	}

	program.renderParamsDirty = false;
	R_CHECK_GL_ERRORS();
}

// ========================================================
// RhiManagerImpl::queryGLProgUniformsAndAttributes():
// ========================================================

void RhiManagerImpl::queryGLProgUniformsAndAttributes(Program & program)
{
	program.renderParamValues.clear();
	program.renderParamNames.clear();

	const GLuint glProgHandle = program.programHandleGL;
	DEBUG_CHECK(glProgHandle != 0);

	// Query and store the GL uniform variables, AKA render params:
	GLint uniformVarCount = 0;
	glGetProgramiv(glProgHandle, GL_ACTIVE_UNIFORMS, &uniformVarCount);
	queryGLProgUniformVars(program, uniformVarCount, glProgHandle);
	R_CHECK_GL_ERRORS();

	// Query and store the GL vertex attributes for the vertex shader input layout:
	GLint attributeCount = 0;
	glGetProgramiv(glProgHandle, GL_ACTIVE_ATTRIBUTES, &attributeCount);
	queryGLProgVertexAttributes(program, attributeCount, glProgHandle);
	R_CHECK_GL_ERRORS();

	// Force an update of the newly queried uniform
	// variables to ensure the initial values are known.
	program.renderParamsDirty = true;
	setProgram(program);
}

// ========================================================
// RhiManagerImpl::queryGLProgUniformVars():
// ========================================================

void RhiManagerImpl::queryGLProgUniformVars(Program & program, const GLint uniformVarCount, const GLuint glProgHandle)
{
	static const float identityMat2x2[2*2] =
	{
		1.0f, 0.0,
		0.0f, 1.0f
	};
	static const float identityMat3x3[3*3] =
	{
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f
	};
	static const float identityMat4x4[4*4] =
	{
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	if (uniformVarCount <= 0)
	{
		return;
	}

	R_LOG_INFO("Gathering GL render params (" << uniformVarCount << ")...");

	program.renderParamValues.allocateExact(uniformVarCount);
	program.renderParamNames.allocateExact(uniformVarCount);

	constexpr GLsizei UniformNameMaxChars = 2048;
	GLchar uniformNameBuf[UniformNameMaxChars];

	Program::ParamData paramData;

	for (GLint index = 0; index < uniformVarCount; ++index)
	{
		core::clearPodArray(uniformNameBuf);

		GLint  uniformSize = 0;
		GLenum uniformType = 0;

		glGetActiveUniform(
			/* program = */ glProgHandle,
			/* index   = */ index,
			/* bufSize = */ UniformNameMaxChars - 1,
			/* length  = */ nullptr,
			/* size    = */ &uniformSize,
			/* type    = */ &uniformType,
			/* name    = */ uniformNameBuf);

		if (uniformSize == 0 || uniformType == 0 || uniformNameBuf[0] == '\0')
		{
			R_LOG_WARN("Invalid GL uniform variable data at index " << index << "!");
			continue;
		}

		const GLint uniformLocation = glGetUniformLocation(glProgHandle, uniformNameBuf);
		if (uniformLocation < 0)
		{
			R_LOG_WARN("Invalid GL uniform location at index " << index << "!");
			continue;
		}

		paramData.uniformLocGL = uniformLocation;
		paramData.isDirty = true;

		switch (uniformType)
		{
		case GL_INT :
			paramData.value.asInt = 0;
			paramData.type = Program::ParamData::Int;
			break;

		case GL_BOOL :
			paramData.value.asInt = 0;
			paramData.type = Program::ParamData::Bool;
			break;

		case GL_FLOAT :
			paramData.value.asFloat = 0.0f;
			paramData.type = Program::ParamData::Float;
			break;

		case GL_FLOAT_VEC2 :
			paramData.value.asVec2 = core::Vec2::origin();
			paramData.type = Program::ParamData::V2F;
			break;

		case GL_FLOAT_VEC3 :
			paramData.value.asVec3 = core::Vec3::origin();
			paramData.type = Program::ParamData::V3F;
			break;

		case GL_FLOAT_VEC4 :
			paramData.value.asVec4 = core::Vec4::origin();
			paramData.type = Program::ParamData::V4F;
			break;

		case GL_FLOAT_MAT2 :
			paramData.value.asMatrixRef.data = identityMat2x2;
			paramData.value.asMatrixRef.squareSize = 2;
			paramData.type = Program::ParamData::Matrix;
			break;

		case GL_FLOAT_MAT3 :
			paramData.value.asMatrixRef.data = identityMat3x3;
			paramData.value.asMatrixRef.squareSize = 3;
			paramData.type = Program::ParamData::Matrix;
			break;

		case GL_FLOAT_MAT4 :
			paramData.value.asMatrixRef.data = identityMat4x4;
			paramData.value.asMatrixRef.squareSize = 4;
			paramData.type = Program::ParamData::Matrix;
			break;

		case GL_SAMPLER_1D :
		case GL_SAMPLER_2D :
		case GL_SAMPLER_3D :
		case GL_SAMPLER_CUBE :
		case GL_SAMPLER_1D_SHADOW :
		case GL_SAMPLER_2D_SHADOW :
		case GL_SAMPLER_CUBE_SHADOW :
			paramData.value.asInt = 0;
			paramData.type = Program::ParamData::SamplerIdx;
			break;

		default :
			R_LOG_FATAL("Unhandled GL uniform variable type "
				<< core::toString(uniformType, 16) << ", size=" << uniformSize << ".");
		} // switch (uniformType)

		program.renderParamValues.pushBack(paramData);
		program.renderParamNames.pushBack(uniformNameBuf);
	}

	// Debug print the new RenderParams:
	const core::uint paramCount = program.renderParamValues.getSize();
	for (core::uint i = 0; i < paramCount; ++i)
	{
		static const char * typeStrings[] =
		{
			"Int", "Bool", "Float",
			"V2F", "V3F", "V4F",
			"Matrix", "SamplerIdx"
		};
		COMPILE_TIME_CHECK(core::arrayLength(typeStrings) == Program::ParamData::Count,
				"Keep this array in sync with the enum declaration!");

		R_LOG_INFO("RenderParam[" << i << "] = ("
			<< program.renderParamNames[i].getString()
			<< ", " << typeStrings[program.renderParamValues[i].type]
			<< ", " << program.renderParamValues[i].uniformLocGL
			<< ")");
	}
}

// ========================================================
// RhiManagerImpl::queryGLProgVertexAttributes():
// ========================================================

void RhiManagerImpl::queryGLProgVertexAttributes(Program & program, const GLint attributeCount, const GLuint glProgHandle)
{
	if (attributeCount <= 0)
	{
		return;
	}

	R_LOG_INFO("Gathering GL vertex layout attributes (" << attributeCount << ")...");

	constexpr GLsizei AttribNameMaxChars = 2048;
	GLchar attribNameBuf[AttribNameMaxChars];

	ProgramVertexLayout vertexLayout;

	for (GLint index = 0; index < attributeCount; ++index)
	{
		if (vertexLayout.vertexElementCount == vertexLayout.vertexElements.getSize())
		{
			R_LOG_WARN("Max vertex elements (" << vertexLayout.vertexElements.getSize() << ") exceeded!");
			break;
		}

		core::clearPodArray(attribNameBuf);

		GLint  attribSize = 0;
		GLenum attribType = 0;

		glGetActiveAttrib(
			/* program = */ glProgHandle,
			/* index   = */ index,
			/* bufSize = */ AttribNameMaxChars - 1,
			/* length  = */ nullptr,
			/* size    = */ &attribSize,
			/* type    = */ &attribType,
			/* name    = */ attribNameBuf);

		if (attribSize == 0 || attribType == 0 || attribNameBuf[0] == '\0')
		{
			R_LOG_WARN("Invalid GL vertex attribute data at index " << index << "!");
			continue;
		}

		const GLint attribLocation = glGetAttribLocation(glProgHandle, attribNameBuf);
		if (attribLocation < 0)
		{
			R_LOG_WARN("Invalid GL vertex attribute location at index " << index << "!");
			continue;
		}

		ProgramVertexLayout::VertexElement & vertElement =
				vertexLayout.vertexElements[vertexLayout.vertexElementCount++];

		core::strCopy(vertElement.name, ProgramVertexLayout::VertexElement::MaxNameChars, attribNameBuf);

		DEBUG_CHECK(attribLocation <= core::int16Max && "Need a bigger type for attrib index!");
		vertElement.index = static_cast<core::int16>(attribLocation);

		switch (attribType)
		{
		case GL_INT        : vertElement.type = ProgramVertexLayout::Int;   break;
		case GL_FLOAT      : vertElement.type = ProgramVertexLayout::Float; break;
		case GL_FLOAT_VEC2 : vertElement.type = ProgramVertexLayout::V2F;   break;
		case GL_FLOAT_VEC3 : vertElement.type = ProgramVertexLayout::V3F;   break;
		case GL_FLOAT_VEC4 : vertElement.type = ProgramVertexLayout::V4F;   break;
		default :
			R_LOG_FATAL("Unhandled GL vertex attribute type "
				<< core::toString(attribType, 16) << ", size=" << attribSize << ".");
		} // switch (attribType)
	}

	// Sort then based on the GL attribute location/index:
	struct SortPredicateElementIndex
	{
		typedef ProgramVertexLayout::VertexElement Element;
		int operator() (const Element & a, const Element & b) const
		{
			if (a.index < b.index) { return -1; }
			if (a.index > b.index) { return +1; }
			return 0;
		}
	} sortByElementIndex;
	core::quickSort(vertexLayout.vertexElements.getData(), vertexLayout.vertexElementCount, sortByElementIndex);

	// This will either add a new vertex layout or return one matching the give layout.
	program.setVertexLayout(findOrRegisterVertexLayout(vertexLayout));

	// Debug print the layout:
	const ProgramVertexLayout & progVertLayout = program.getVertexLayout();
	for (core::uint i = 0; i < progVertLayout.vertexElementCount; ++i)
	{
		static const char * typeStrings[] =
		{
			"Int", "Float",
			"V2F", "V3F", "V4F"
		};
		COMPILE_TIME_CHECK(core::arrayLength(typeStrings) == ProgramVertexLayout::Count,
				"Keep this array in sync with the enum declaration!");

		R_LOG_INFO("VertexElement[" << i << "] = ("
			<< progVertLayout.vertexElements[i].name
			<< ", " << typeStrings[progVertLayout.vertexElements[i].type]
			<< ", " << progVertLayout.vertexElements[i].index
			<< ")");
	}
}

// ========================================================
// RhiManagerImpl::gatherGLShaderInfoLogs():
// ========================================================

bool RhiManagerImpl::gatherGLShaderInfoLogs(const GLuint glProgHandle, const GLuint glVsHandle,
                                            const GLuint glFsHandle, ProgramSetupResult * result)
{
	core::String infoLogStr;
	GLsizei charsWritten;

	constexpr GLsizei InfoLogMaxChars = 4096;
	GLchar infoLogBuf[InfoLogMaxChars];

	charsWritten = 0;
	core::clearPodArray(infoLogBuf);
	glGetProgramInfoLog(glProgHandle, InfoLogMaxChars - 1, &charsWritten, infoLogBuf);

	if (charsWritten > 0)
	{
		infoLogStr += "------ GL PROGRAM INFO LOG ----------\n";
		infoLogStr += infoLogBuf;
		infoLogStr += "\n";
	}

	charsWritten = 0;
	core::clearPodArray(infoLogBuf);
	glGetShaderInfoLog(glVsHandle, InfoLogMaxChars - 1, &charsWritten, infoLogBuf);

	if (charsWritten > 0)
	{
		infoLogStr += "------ GL VERT SHADER INFO LOG ------\n";
		infoLogStr += infoLogBuf;
		infoLogStr += "\n";
	}

	charsWritten = 0;
	core::clearPodArray(infoLogBuf);
	glGetShaderInfoLog(glFsHandle, InfoLogMaxChars - 1, &charsWritten, infoLogBuf);

	if (charsWritten > 0)
	{
		infoLogStr += "------ GL FRAG SHADER INFO LOG ------\n";
		infoLogStr += infoLogBuf;
		infoLogStr += "\n";
	}

	GLint linkStatus = GL_FALSE;
	glGetProgramiv(glProgHandle, GL_LINK_STATUS, &linkStatus);

	if (linkStatus == GL_FALSE)
	{
		if (result != nullptr)
		{
			result->state = ProgramSetupResult::LinkFailed;
		}
		R_LOG_ERROR("Failed to link GL program!");
	}
	else
	{
		if (result != nullptr)
		{
			result->state = (infoLogStr.isEmpty() ?
			    ProgramSetupResult::Successful    :
			    ProgramSetupResult::SuccessfulWithWarnings);
		}
	}

	if (!infoLogStr.isEmpty())
	{
		// Split the info log into lines and print it as warnings:
		core::Array<core::String> lines;
		core::split(infoLogStr, "\r\n", lines);
		for (core::uint l = 0; l < lines.getSize(); ++l)
		{
			using core::AnsiColorCodes;
			R_LOG_WARN(">> " << AnsiColorCodes::Yellow << lines[l] << AnsiColorCodes::Default);
		}

		// Pass the string's ownership to the result, if provided:
		if (result != nullptr)
		{
			result->infoLog.clear();
			infoLogStr.transferTo(result->infoLog);
		}
	}
	else
	{
		// No errors or warnings, make sure the result info log is clear.
		if (result != nullptr)
		{
			result->infoLog.clear();
		}
	}

	return linkStatus != GL_FALSE;
}

} // namespace ogl {}
} // namespace render {}
} // namespace atlas {}
