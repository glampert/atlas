
// ================================================================================================
// -*- C++ -*-
// File: rhi_specific.hpp
// Author: Guilherme R. Lampert
// Created on: 05/08/15
// Brief: OpenGL main includes and the opaque typedef to the RhiManager.
// ================================================================================================

#ifndef ATLAS_RENDER_OPENGL_RHI_SPECIFIC_HPP
#define ATLAS_RENDER_OPENGL_RHI_SPECIFIC_HPP

#if !defined (ATLAS_RHI_BACKEND_OPENGL)
	#error "This file shouldn't be included when not compiling the OpenGL RHI!"
#endif // ATLAS_RHI_BACKEND_OPENGL

// OpenGL declarations and functions:
#include "thirdparty/gl3w/include/GL/gl3w.h"

namespace atlas
{
namespace render
{

// ========================================================
// The statically defined RhiManager implementation:
// ========================================================

namespace ogl {
class RhiManagerImpl;
} // ogl

//
// Expose the OpenGL `RhiManagerImpl` into the `render`
// namespace. We don't use virtual inheritance for the
// renderer, since the back-end implementation selection
// can be done at compile-time. This avoids the cost of
// virtual dispatch.
//
typedef ogl::RhiManagerImpl RhiManager;

} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_OPENGL_RHI_SPECIFIC_HPP
