
// ================================================================================================
// -*- C++ -*-
// File: rhi_manager_buffers.cpp
// Author: Guilherme R. Lampert
// Created on: 18/07/15
// Brief: RhiManagerImpl methods related to vertex/index buffers.
// ================================================================================================

#include "atlas/render/opengl/rhi_manager.hpp"
#include "atlas/render/render_log.hpp"

namespace atlas
{
namespace render
{
namespace ogl
{

// ================================================================================================
// ogl::RhiManagerImpl -- Vertex/Index Buffer management.
// ================================================================================================

// VBO/IBO/Generic GL buffers:

#define R_BIND_GL_BUFFER_IF_NOT_CURRENT(buffer, target, cached) \
	do { \
		if ((buffer) != (cached)) \
		{ \
			(cached) = (buffer); \
			glBindBuffer((target), (buffer)); \
		} \
	} while (0)

#define R_CLEAR_GL_BUFFER_IF_CURRENT(buffer, target, cached) \
	do { \
		if ((buffer) == (cached)) \
		{ \
			(cached) = 0; \
			glBindBuffer((target), 0); \
		} \
	} while (0)

#define R_BIND_GL_BUFFER_NO_CACHING_TEST(buffer, target, cached) \
	do { \
		(cached) = (buffer); \
		glBindBuffer((target), (buffer)); \
	} while (0)

// Vertex Array Object (VAO):

#define R_BIND_GL_VAO_IF_NOT_CURRENT(vao) \
	do { \
		if ((vao) != currentGLVao) \
		{ \
			currentGLVao = (vao); \
			glBindVertexArray((vao)); \
		} \
	} while (0)

#define R_CLEAR_GL_VAO_IF_CURRENT(vao) \
	do { \
		if ((vao) == currentGLVao) \
		{ \
			currentGLVao = 0; \
			glBindVertexArray(0); \
		} \
	} while (0)

#define R_BIND_GL_VAO_NO_CACHING_TEST(vao) \
	do { \
		currentGLVao = (vao); \
		glBindVertexArray((vao)); \
	} while (0)

// ========================================================
// Local helper functions:
// ========================================================

namespace
{

GLenum getGLBufferUsage(const core::uint resourceFlags)
{
	if (resourceFlags & Resource::Flag::UsageStatic)
	{
		return GL_STATIC_DRAW;
	}
	if (resourceFlags & Resource::Flag::UsageDynamic)
	{
		return GL_DYNAMIC_DRAW;
	}
	if (resourceFlags & Resource::Flag::UsageStream)
	{
		return GL_STREAM_DRAW;
	}

	R_LOG_FATAL("Invalid resource usage flags! Need at least one of: UsageStatic, UsageDynamic, UsageStream.");
}

const GLvoid * guessGLAttribOffset(const ProgramVertexLayout & layout,
                                   const core::uint elementIndex,
                                   const core::uint vertexStride)
{
	static const GLuint glElementSizeBytes[] =
	{
		sizeof(GLint),       // Int
		sizeof(GLfloat),     // Float
		sizeof(GLfloat) * 2, // V2F
		sizeof(GLfloat) * 3, // V3F
		sizeof(GLfloat) * 4  // V4F
	};
	COMPILE_TIME_CHECK(core::arrayLength(glElementSizeBytes) == ProgramVertexLayout::Count,
			"Keep this array in sync with the enum declaration!");

	std::uintptr_t offsetBytes = 0;
	for (core::uint i = 0; i < elementIndex; ++i)
	{
		offsetBytes += glElementSizeBytes[layout.vertexElements[i].type];
	}

	DEBUG_CHECK(offsetBytes < vertexStride && "This will not work if you're skipping vertex elements...");
	#ifdef ATLAS_RELEASE
	(void)vertexStride;
	#endif // ATLAS_RELEASE

	return reinterpret_cast<const GLvoid *>(offsetBytes);
}

} // namespace {}

// ========================================================
// RhiManagerImpl::allocateVertexBuffer():
// ========================================================

bool RhiManagerImpl::allocateVertexBuffer(VertexBuffer & destVertexBuffer, const core::uint resourceFlags,
                                          const core::uint vertexCount, const core::uint sizeOfAVertex,
                                          const core::ubyte * initialVertexData)
{
	DEBUG_CHECK(isInitialized());

	DEBUG_CHECK(resourceFlags != 0);
	DEBUG_CHECK(vertexCount   != 0);
	DEBUG_CHECK(sizeOfAVertex != 0);

	DEBUG_CHECK((destVertexBuffer.resourceFlags & Resource::Flag::TypeVertexBuffer) && "Inconsistent flags!");
	DEBUG_CHECK(!destVertexBuffer.isResourceLocked() && "Locked buffer!");

	// Recycle the GL handles if possible.
	GLuint glVaoHandle = destVertexBuffer.vaoHandleGL;
	GLuint glVboHandle = destVertexBuffer.vboHandleGL;
	if (!allocGLBufferHandles(&glVaoHandle, &glVboHandle, nullptr))
	{
		return false;
	}

	bindGLBuffersNoCachingTest(&glVaoHandle, &glVboHandle, nullptr);
	glBufferData(GL_ARRAY_BUFFER, (vertexCount * sizeOfAVertex), initialVertexData, getGLBufferUsage(resourceFlags));
	R_CHECK_GL_ERRORS();

	destVertexBuffer.vaoHandleGL   = glVaoHandle;
	destVertexBuffer.vboHandleGL   = glVboHandle;
	destVertexBuffer.vertexCount   = vertexCount;
	destVertexBuffer.sizeOfAVertex = sizeOfAVertex;
	destVertexBuffer.resourceFlags = resourceFlags | Resource::Flag::TypeVertexBuffer;

	R_LOG_INFO("New vertex buffer allocated: " << vertexCount
		<< " verts, " << sizeOfAVertex << " bytes per vertex.");

	return true;
}

// ========================================================
// RhiManagerImpl::allocateIndexedVertexBuffer():
// ========================================================

bool RhiManagerImpl::allocateIndexedVertexBuffer(IndexedVertexBuffer & destVertexBuffer, const core::uint resourceFlags,
                                                 const core::uint vertexCount, const core::uint sizeOfAVertex,
                                                 const core::uint indexCount,  const core::uint sizeOfAnIndex,
                                                 const core::ubyte * initialVertexData,
                                                 const core::ubyte * initialIndexData)
{
	DEBUG_CHECK(isInitialized());

	DEBUG_CHECK(resourceFlags != 0);
	DEBUG_CHECK(vertexCount   != 0);
	DEBUG_CHECK(sizeOfAVertex != 0);
	DEBUG_CHECK(indexCount    != 0);
	DEBUG_CHECK(sizeOfAnIndex != 0);

	DEBUG_CHECK((destVertexBuffer.resourceFlags & Resource::Flag::TypeIdxVertexBuffer) && "Inconsistent flags!");
	DEBUG_CHECK(!destVertexBuffer.isResourceLocked() && "Locked buffer!");

	// Recycle the GL handles if possible.
	GLuint glVaoHandle = destVertexBuffer.vaoHandleGL;
	GLuint glVboHandle = destVertexBuffer.vboHandleGL;
	GLuint glIboHandle = destVertexBuffer.iboHandleGL;
	if (!allocGLBufferHandles(&glVaoHandle, &glVboHandle, &glIboHandle))
	{
		return false;
	}

	bindGLBuffersNoCachingTest(&glVaoHandle, &glVboHandle, &glIboHandle);
	const GLenum glBufferUsage = getGLBufferUsage(resourceFlags);

	// Vertex data:
	glBufferData(GL_ARRAY_BUFFER, (vertexCount * sizeOfAVertex), initialVertexData, glBufferUsage);
	R_CHECK_GL_ERRORS();

	// Index data:
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (indexCount * sizeOfAnIndex), initialIndexData, glBufferUsage);
	R_CHECK_GL_ERRORS();

	destVertexBuffer.vaoHandleGL   = glVaoHandle;
	destVertexBuffer.vboHandleGL   = glVboHandle;
	destVertexBuffer.iboHandleGL   = glIboHandle;
	destVertexBuffer.vertexCount   = vertexCount;
	destVertexBuffer.indexCount    = indexCount;
	destVertexBuffer.sizeOfAVertex = sizeOfAVertex;
	destVertexBuffer.sizeOfAnIndex = sizeOfAnIndex;
	destVertexBuffer.resourceFlags = resourceFlags | Resource::Flag::TypeIdxVertexBuffer;

	R_LOG_INFO("New indexed vertex buffer allocated: " << vertexCount
		<< " verts, " << sizeOfAVertex << " bytes per vertex; "
		<< indexCount << " indexes, " << sizeOfAnIndex << " bytes per index.");

	return true;
}

// ========================================================
// RhiManagerImpl::updateBufferVertexes():
// ========================================================

bool RhiManagerImpl::updateBufferVertexes(VertexBuffer & vertexBuffer, const core::uint offsetBytes,
                                          const core::uint sizeBytes, const core::ubyte * vertexData,
                                          const bool allowDiscardingCurrentData)
{
	DEBUG_CHECK(isInitialized());
	DEBUG_CHECK(vertexBuffer.isValid());

	DEBUG_CHECK((vertexBuffer.resourceFlags & Resource::Flag::TypeVertexBuffer) && "Inconsistent flags!");
	DEBUG_CHECK(!vertexBuffer.isResourceLocked() && "Locked buffer!");

	const core::uint currentSizeBytes = vertexBuffer.vertexCount * vertexBuffer.sizeOfAVertex;
	const GLenum glBufferUsage = getGLBufferUsage(vertexBuffer.resourceFlags);

	return helperUpdateGLBuffer(
		GL_ARRAY_BUFFER,
		vertexBuffer.vaoHandleGL,
		vertexBuffer.vboHandleGL,
		glBufferUsage,
		currentSizeBytes,
		offsetBytes,
		sizeBytes,
		vertexData,
		allowDiscardingCurrentData);
}

// ========================================================
// RhiManagerImpl::updateBufferVertexes():
// ========================================================

bool RhiManagerImpl::updateBufferVertexes(IndexedVertexBuffer & iVertexBuffer, const core::uint offsetBytes,
                                          const core::uint sizeBytes, const core::ubyte * vertexData,
                                          const bool allowDiscardingCurrentData)
{
	DEBUG_CHECK(isInitialized());
	DEBUG_CHECK(iVertexBuffer.isValid());

	DEBUG_CHECK((iVertexBuffer.resourceFlags & Resource::Flag::TypeIdxVertexBuffer) && "Inconsistent flags!");
	DEBUG_CHECK(!iVertexBuffer.isResourceLocked() && "Locked buffer!");

	const core::uint currentSizeBytes = iVertexBuffer.vertexCount * iVertexBuffer.sizeOfAVertex;
	const GLenum glBufferUsage = getGLBufferUsage(iVertexBuffer.resourceFlags);

	return helperUpdateGLBuffer(
		GL_ARRAY_BUFFER,
		iVertexBuffer.vaoHandleGL,
		iVertexBuffer.vboHandleGL,
		glBufferUsage,
		currentSizeBytes,
		offsetBytes,
		sizeBytes,
		vertexData,
		allowDiscardingCurrentData);
}

// ========================================================
// RhiManagerImpl::updateBufferIndexes():
// ========================================================

bool RhiManagerImpl::updateBufferIndexes(IndexedVertexBuffer & iVertexBuffer, const core::uint offsetBytes,
                                         const core::uint sizeBytes, const core::ubyte * indexData,
                                         const bool allowDiscardingCurrentData)
{
	DEBUG_CHECK(isInitialized());
	DEBUG_CHECK(iVertexBuffer.isValid());

	DEBUG_CHECK((iVertexBuffer.resourceFlags & Resource::Flag::TypeIdxVertexBuffer) && "Inconsistent flags!");
	DEBUG_CHECK(!iVertexBuffer.isResourceLocked() && "Locked buffer!");

	const core::uint currentSizeBytes = iVertexBuffer.indexCount * iVertexBuffer.sizeOfAnIndex;
	const GLenum glBufferUsage = getGLBufferUsage(iVertexBuffer.resourceFlags);

	return helperUpdateGLBuffer(
		GL_ELEMENT_ARRAY_BUFFER,
		iVertexBuffer.vaoHandleGL,
		iVertexBuffer.iboHandleGL,
		glBufferUsage,
		currentSizeBytes,
		offsetBytes,
		sizeBytes,
		indexData,
		allowDiscardingCurrentData);
}

// ========================================================
// RhiManagerImpl::lockResource():
// ========================================================

core::ubyte * RhiManagerImpl::lockResource(Resource & resource, const core::uint offsetBytes,
                                           const core::uint sizeBytes, const core::uint accessFlags)
{
	DEBUG_CHECK(isInitialized());
	DEBUG_CHECK(resource.isResourceManagerValid());
	DEBUG_CHECK(!resource.isResourceLocked() && "Resource is already locked!");

	GLenum glTarget = 0;
	GLbitfield glAccessMapBufferRange = 0;
	core::uint resourceLockModeFlags  = 0;
	helperLockGLBuffer(resource, accessFlags, &resourceLockModeFlags, &glTarget, nullptr, &glAccessMapBufferRange);

	GLvoid * dataPtr = glMapBufferRange(glTarget, offsetBytes, sizeBytes, glAccessMapBufferRange);
	if (dataPtr != nullptr)
	{
		// Mapping successful, update lock flags:
		resource.resourceFlags = resource.resourceFlags | resourceLockModeFlags;
	}
	else
	{
		R_LOG_WARN("glMapBufferRange() failed with a null pointer! Resource not locked.");
		R_CHECK_GL_ERRORS();
	}

	return reinterpret_cast<core::ubyte *>(dataPtr);
}

// ========================================================
// RhiManagerImpl::lockResource():
// ========================================================

core::ubyte * RhiManagerImpl::lockResource(Resource & resource, const core::uint accessFlags)
{
	DEBUG_CHECK(isInitialized());
	DEBUG_CHECK(resource.isResourceManagerValid());
	DEBUG_CHECK(!resource.isResourceLocked() && "Resource is already locked!");

	GLenum glTarget = 0;
	GLenum glAccessMapBuffer = 0;
	core::uint resourceLockModeFlags = 0;
	helperLockGLBuffer(resource, accessFlags, &resourceLockModeFlags, &glTarget, &glAccessMapBuffer, nullptr);

	GLvoid * dataPtr = glMapBuffer(glTarget, glAccessMapBuffer);
	if (dataPtr != nullptr)
	{
		// Mapping successful, update lock flags:
		resource.resourceFlags = resource.resourceFlags | resourceLockModeFlags;
	}
	else
	{
		R_LOG_WARN("glMapBuffer() failed with a null pointer! Resource not locked.");
		R_CHECK_GL_ERRORS();
	}

	return reinterpret_cast<core::ubyte *>(dataPtr);
}

// ========================================================
// RhiManagerImpl::unlockResource():
// ========================================================

bool RhiManagerImpl::unlockResource(Resource & resource, const core::ubyte * dataPtr)
{
	DEBUG_CHECK(isInitialized());
	DEBUG_CHECK(resource.isResourceManagerValid());
	DEBUG_CHECK(resource.isResourceLocked() && "Resource is not locked!");

	if (dataPtr == nullptr)
	{
		// Not an assertion because lockResource() might fail with null,
		// so passing its return to unlockResource() would still be fine in that case.
		R_LOG_WARN("RhiManager::unlockResource() with a null pointer! Ignoring it...");
		return false;
	}

	GLenum glTarget;
	if (resource.resourceFlags & Resource::Flag::TypeVertexBuffer)
	{
		glTarget = GL_ARRAY_BUFFER;
		VertexBuffer & vertexBuffer = static_cast<VertexBuffer &>(resource);

		// Bind buffer if not current:
		R_BIND_GL_VAO_IF_NOT_CURRENT(vertexBuffer.vaoHandleGL);
		R_BIND_GL_BUFFER_IF_NOT_CURRENT(vertexBuffer.vboHandleGL, glTarget, currentGLVbo);
	}
	else if (resource.resourceFlags & Resource::Flag::TypeIdxVertexBuffer)
	{
		glTarget = (resource.resourceFlags & Resource::Flag::LockVertexData) ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;
		IndexedVertexBuffer & iVertexBuffer = static_cast<IndexedVertexBuffer &>(resource);

		// Bind buffer if not current:
		R_BIND_GL_VAO_IF_NOT_CURRENT(iVertexBuffer.vaoHandleGL);
		if (glTarget == GL_ARRAY_BUFFER)
		{
			R_BIND_GL_BUFFER_IF_NOT_CURRENT(iVertexBuffer.vboHandleGL, glTarget, currentGLVbo);
		}
		else
		{
			R_BIND_GL_BUFFER_IF_NOT_CURRENT(iVertexBuffer.iboHandleGL, glTarget, currentGLIbo);
		}
	}
	else
	{
		R_LOG_FATAL("Resource type is not lockable!");
	}

	const GLboolean result = glUnmapBuffer(glTarget);
	if (result == GL_FALSE)
	{
		R_LOG_WARN("glUnmapBuffer() failed!");
		R_CHECK_GL_ERRORS();
	}

	// Clear all locking flags:
	constexpr core::uint LockFlags = (
		Resource::Flag::LockForReading |
		Resource::Flag::LockForWriting |
		Resource::Flag::LockVertexData |
		Resource::Flag::LockIndexData);

	resource.resourceFlags = (resource.resourceFlags & ~LockFlags);

	return result == GL_TRUE;
}

// ========================================================
// RhiManagerImpl::setVertexBuffer():
// ========================================================

void RhiManagerImpl::setVertexBuffer(const VertexBuffer & vertexBuffer)
{
	DEBUG_CHECK(isInitialized());

	if (!vertexBuffer.isValid())
	{
		// Can't set a default that would provide some visual debugging
		// aid, so we at least log a warning to let the caller know.
		R_LOG_WARN("Trying to bind an invalid VertexBuffer!");
		return;
	}

	DEBUG_CHECK((vertexBuffer.resourceFlags & Resource::Flag::TypeVertexBuffer) && "Inconsistent flags!");
	DEBUG_CHECK(!vertexBuffer.isResourceLocked() && "Can't render with a locked buffer!");

	// This method is meant for drawing, so we only need to bind the VAO.
	R_BIND_GL_VAO_IF_NOT_CURRENT(vertexBuffer.vaoHandleGL);

	// Update vertex layout if it has changed since the last time this
	// buffer was used, or if the layout was not set yet for this buffer.
	if (vertexBuffer.vertexLayout != currentVertexLayout)
	{
		DEBUG_CHECK(currentVertexLayout != nullptr && "Null vertex layout! Set a render program first!");

		const_cast<VertexBuffer &>(vertexBuffer).vertexLayout = currentVertexLayout;
		R_BIND_GL_BUFFER_NO_CACHING_TEST(vertexBuffer.vboHandleGL, GL_ARRAY_BUFFER, currentGLVbo);

		setGLVertexLayout(*currentVertexLayout, vertexBuffer.sizeOfAVertex);
	}
}

// ========================================================
// RhiManagerImpl::setVertexBuffer():
// ========================================================

void RhiManagerImpl::setVertexBuffer(const IndexedVertexBuffer & iVertexBuffer)
{
	DEBUG_CHECK(isInitialized());

	if (!iVertexBuffer.isValid())
	{
		// Can't set a default that would provide some visual debugging
		// aid, so we at least log a warning to let the caller know.
		R_LOG_WARN("Trying to bind an invalid IndexedVertexBuffer!");
		return;
	}

	DEBUG_CHECK((iVertexBuffer.resourceFlags & Resource::Flag::TypeIdxVertexBuffer) && "Inconsistent flags!");
	DEBUG_CHECK(!iVertexBuffer.isResourceLocked() && "Can't render with a locked buffer!");

	// This method is meant for drawing, so we only need to bind the VAO.
	R_BIND_GL_VAO_IF_NOT_CURRENT(iVertexBuffer.vaoHandleGL);

	// Update vertex layout if it has changed since the last time this
	// buffer was used, or if the layout was not set yet for this buffer.
	if (iVertexBuffer.vertexLayout != currentVertexLayout)
	{
		DEBUG_CHECK(currentVertexLayout != nullptr && "Null vertex layout! Set a render program first!");

		const_cast<IndexedVertexBuffer &>(iVertexBuffer).vertexLayout = currentVertexLayout;
		R_BIND_GL_BUFFER_NO_CACHING_TEST(iVertexBuffer.vboHandleGL, GL_ARRAY_BUFFER, currentGLVbo);

		setGLVertexLayout(*currentVertexLayout, iVertexBuffer.sizeOfAVertex);
	}
}

// ========================================================
// RhiManagerImpl::newGLVaoHandle():
// ========================================================

GLuint RhiManagerImpl::newGLVaoHandle()
{
	GLuint newVao = 0;
	glGenVertexArrays(1, &newVao);

	if (newVao == 0)
	{
		R_LOG_ERROR("Failed to allocate a new GL VAO handle! Possibly out-of-memory!");
		R_CHECK_GL_ERRORS();
	}
	return newVao;
}

// ========================================================
// RhiManagerImpl::newGLVboHandle():
// ========================================================

GLuint RhiManagerImpl::newGLVboHandle()
{
	GLuint newVbo = 0;
	glGenBuffers(1, &newVbo);

	if (newVbo == 0)
	{
		R_LOG_ERROR("Failed to allocate a new GL VBO handle! Possibly out-of-memory!");
		R_CHECK_GL_ERRORS();
	}
	return newVbo;
}

// ========================================================
// RhiManagerImpl::newGLIboHandle():
// ========================================================

GLuint RhiManagerImpl::newGLIboHandle()
{
	GLuint newIbo = 0;
	glGenBuffers(1, &newIbo);

	if (newIbo == 0)
	{
		R_LOG_ERROR("Failed to allocate a new GL IBO handle! Possibly out-of-memory!");
		R_CHECK_GL_ERRORS();
	}
	return newIbo;
}

// ========================================================
// RhiManagerImpl::freeGLVertexBuffer():
// ========================================================

void RhiManagerImpl::freeGLVertexBuffer(VertexBuffer & vertexBuffer)
{
	R_LOG_INFO("Freeing a GL VertexBuffer...");

	const GLuint glVaoHandle = vertexBuffer.vaoHandleGL;
	if (glVaoHandle == 0)
	{
		return;
	}

	const GLuint glVboHandle = vertexBuffer.vboHandleGL;

	R_CLEAR_GL_VAO_IF_CURRENT(glVaoHandle);
	R_CLEAR_GL_BUFFER_IF_CURRENT(glVboHandle, GL_ARRAY_BUFFER, currentGLVbo);

	glDeleteVertexArrays(1, &glVaoHandle);
	glDeleteBuffers(1, &glVboHandle);

	vertexBuffer.vaoHandleGL = 0;
	vertexBuffer.vboHandleGL = 0;
}

// ========================================================
// RhiManagerImpl::freeGLIndexedVertexBuffer():
// ========================================================

void RhiManagerImpl::freeGLIndexedVertexBuffer(IndexedVertexBuffer & iVertexBuffer)
{
	R_LOG_INFO("Freeing a GL IndexedVertexBuffer...");

	const GLuint glVaoHandle = iVertexBuffer.vaoHandleGL;
	if (glVaoHandle == 0)
	{
		return;
	}

	const GLuint glVboHandle = iVertexBuffer.vboHandleGL;
	const GLuint glIboHandle = iVertexBuffer.iboHandleGL;

	R_CLEAR_GL_VAO_IF_CURRENT(glVaoHandle);
	R_CLEAR_GL_BUFFER_IF_CURRENT(glVboHandle, GL_ARRAY_BUFFER, currentGLVbo);
	R_CLEAR_GL_BUFFER_IF_CURRENT(glIboHandle, GL_ELEMENT_ARRAY_BUFFER, currentGLIbo);

	glDeleteVertexArrays(1, &glVaoHandle);
	glDeleteBuffers(1, &glVboHandle);
	glDeleteBuffers(1, &glIboHandle);

	iVertexBuffer.vaoHandleGL = 0;
	iVertexBuffer.vboHandleGL = 0;
	iVertexBuffer.iboHandleGL = 0;
}

// ========================================================
// RhiManagerImpl::allocGLBufferHandles():
// ========================================================

bool RhiManagerImpl::allocGLBufferHandles(GLuint * glVaoHandle, GLuint * glVboHandle, GLuint * glIboHandle)
{
	if (glVaoHandle != nullptr)
	{
		if (*glVaoHandle == 0)
		{
			*glVaoHandle = newGLVaoHandle();
			if (*glVaoHandle == 0)
			{
				return false; // newGLVaoHandle() already logs the GL errors.
			}
		}
	}
	if (glVboHandle != nullptr)
	{
		if (*glVboHandle == 0)
		{
			*glVboHandle = newGLVboHandle();
			if (*glVboHandle == 0)
			{
				return false; // newGLVboHandle() already logs the GL errors.
			}
		}
	}
	if (glIboHandle != nullptr)
	{
		if (*glIboHandle == 0)
		{
			*glIboHandle = newGLIboHandle();
			if (*glIboHandle == 0)
			{
				return false; // newGLIboHandle() already logs the GL errors.
			}
		}
	}
	return true;
}

// ========================================================
// RhiManagerImpl::bindGLBuffersNoCachingTest():
// ========================================================

void RhiManagerImpl::bindGLBuffersNoCachingTest(const GLuint * glVaoHandle, const GLuint * glVboHandle, const GLuint * glIboHandle)
{
	// Bind regardless of cached state, but also updates the cache with the new values.
	if (glVaoHandle != nullptr)
	{
		R_BIND_GL_VAO_NO_CACHING_TEST(*glVaoHandle);
	}
	if (glVboHandle != nullptr)
	{
		R_BIND_GL_BUFFER_NO_CACHING_TEST(*glVboHandle, GL_ARRAY_BUFFER, currentGLVbo);
	}
	if (glIboHandle != nullptr)
	{
		R_BIND_GL_BUFFER_NO_CACHING_TEST(*glIboHandle, GL_ELEMENT_ARRAY_BUFFER, currentGLIbo);
	}
}

// ========================================================
// RhiManagerImpl::setGLVertexLayout():
// ========================================================

void RhiManagerImpl::setGLVertexLayout(const ProgramVertexLayout & layout, const core::uint vertexStride)
{
	static const GLint glTypeSizes[] =
	{
		1, // Int
		1, // Float
		2, // V2F
		3, // V3F
		4  // V4F
	};
	COMPILE_TIME_CHECK(core::arrayLength(glTypeSizes) == ProgramVertexLayout::Count,
			"Keep this array in sync with the enum declaration!");

	static const GLenum glTypeEnums[] =
	{
		GL_INT,   // Int
		GL_FLOAT, // Float
		GL_FLOAT, // V2F
		GL_FLOAT, // V3F
		GL_FLOAT  // V4F
	};
	COMPILE_TIME_CHECK(core::arrayLength(glTypeEnums) == ProgramVertexLayout::Count,
			"Keep this array in sync with the enum declaration!");

	// Buffer must be already bound to GL_ARRAY_BUFFER!
	for (core::uint i = 0; i < layout.vertexElementCount; ++i)
	{
		const ProgramVertexLayout::VertexElement & element = layout.vertexElements[i];

		glEnableVertexAttribArray(element.index);
		glVertexAttribPointer(
			/* index     = */ element.index,
			/* size      = */ glTypeSizes[element.type],
			/* type      = */ glTypeEnums[element.type],
			/* normalize = */ GL_FALSE,
			/* stride    = */ vertexStride,
			/* offset    = */ guessGLAttribOffset(layout, i, vertexStride));
	}

	R_CHECK_GL_ERRORS();
}

// ========================================================
// RhiManagerImpl::helperLockGLBuffer():
// ========================================================

void RhiManagerImpl::helperLockGLBuffer(Resource & resource, const core::uint accessFlags, core::uint * resourceLockModeFlags,
                                        GLenum * glTarget, GLenum * glAccessMapBuffer, GLbitfield * glAccessMapBufferRange)
{
	DEBUG_CHECK(resourceLockModeFlags != nullptr);
	DEBUG_CHECK(glTarget != nullptr);

	if (accessFlags & Resource::Flag::LockVertexData) // VertexBuffer
	{
		*glTarget = GL_ARRAY_BUFFER;
	}
	else if (accessFlags & Resource::Flag::LockIndexData) // IndexedVertexBuffer
	{
		*glTarget = GL_ELEMENT_ARRAY_BUFFER;
	}
	else
	{
		R_LOG_FATAL("Missing buffer lock target!");
	}

	if (glAccessMapBuffer != nullptr)
	{
		if ((accessFlags & Resource::Flag::LockForReading) && (accessFlags & Resource::Flag::LockForWriting))
		{
			*resourceLockModeFlags = Resource::Flag::LockForReading | Resource::Flag::LockForWriting;
			*glAccessMapBuffer = GL_READ_WRITE;
		}
		else if (accessFlags & Resource::Flag::LockForReading)
		{
			*resourceLockModeFlags = Resource::Flag::LockForReading;
			*glAccessMapBuffer = GL_READ_ONLY;
		}
		else if (accessFlags & Resource::Flag::LockForWriting)
		{
			*resourceLockModeFlags = Resource::Flag::LockForWriting;
			*glAccessMapBuffer = GL_WRITE_ONLY;
		}
		else
		{
			R_LOG_FATAL("Specify one of the access mode flags for resource locking!");
		}
	}
	else if (glAccessMapBufferRange != nullptr)
	{
		*glAccessMapBufferRange = 0;

		if (accessFlags & Resource::Flag::LockInvalidate)
		{
			if (accessFlags & Resource::Flag::LockForReading)
			{
				R_LOG_FATAL("Can't combine Resource::LockForReading with Resource::LockInvalidate!");
			}
			*glAccessMapBufferRange |= GL_MAP_INVALIDATE_RANGE_BIT;
		}

		if ((accessFlags & Resource::Flag::LockForReading) && (accessFlags & Resource::Flag::LockForWriting))
		{
			*resourceLockModeFlags = Resource::Flag::LockForReading | Resource::Flag::LockForWriting;
			*glAccessMapBufferRange |= GL_MAP_READ_BIT;
			*glAccessMapBufferRange |= GL_MAP_WRITE_BIT;
		}
		else if (accessFlags & Resource::Flag::LockForReading)
		{
			*resourceLockModeFlags = Resource::Flag::LockForReading;
			*glAccessMapBufferRange |= GL_MAP_READ_BIT;
		}
		else if (accessFlags & Resource::Flag::LockForWriting)
		{
			*resourceLockModeFlags = Resource::Flag::LockForWriting;
			*glAccessMapBufferRange |= GL_MAP_WRITE_BIT;
		}
		else
		{
			R_LOG_FATAL("Specify one of the access mode flags for resource locking!");
		}
	}

	if (resource.resourceFlags & Resource::Flag::TypeVertexBuffer)
	{
		DEBUG_CHECK((accessFlags & Resource::Flag::LockVertexData) && "Can only lock vertex data for a VBO!");
		DEBUG_CHECK(*glTarget == GL_ARRAY_BUFFER);

		VertexBuffer & vertexBuffer = static_cast<VertexBuffer &>(resource);

		// Bind buffer if not current:
		R_BIND_GL_VAO_IF_NOT_CURRENT(vertexBuffer.vaoHandleGL);
		R_BIND_GL_BUFFER_IF_NOT_CURRENT(vertexBuffer.vboHandleGL, *glTarget, currentGLVbo);

		*resourceLockModeFlags |= Resource::Flag::LockVertexData;
	}
	else if (resource.resourceFlags & Resource::Flag::TypeIdxVertexBuffer)
	{
		DEBUG_CHECK((accessFlags & Resource::Flag::LockVertexData) || (accessFlags & Resource::Flag::LockIndexData));
		IndexedVertexBuffer & iVertexBuffer = static_cast<IndexedVertexBuffer &>(resource);

		// Bind buffer(s) if not current:
		R_BIND_GL_VAO_IF_NOT_CURRENT(iVertexBuffer.vaoHandleGL);

		if (*glTarget == GL_ARRAY_BUFFER)
		{
			R_BIND_GL_BUFFER_IF_NOT_CURRENT(iVertexBuffer.vboHandleGL, *glTarget, currentGLVbo);
			*resourceLockModeFlags |= Resource::Flag::LockVertexData;
		}
		else
		{
			DEBUG_CHECK(*glTarget == GL_ELEMENT_ARRAY_BUFFER);
			R_BIND_GL_BUFFER_IF_NOT_CURRENT(iVertexBuffer.iboHandleGL, *glTarget, currentGLIbo);
			*resourceLockModeFlags |= Resource::Flag::LockIndexData;
		}
	}
	else
	{
		R_LOG_FATAL("Resource type is not lockable!");
	}
}

// ========================================================
// RhiManagerImpl::helperUpdateGLBuffer():
// ========================================================

bool RhiManagerImpl::helperUpdateGLBuffer(const GLenum target, const GLuint glVaoHandle, const GLuint glBufferHandle,
                                          const GLenum bufferUsage, const GLuint currentSizeBytes, const GLuint offsetBytes,
                                          const GLuint dataSizeBytes, const GLvoid * newData, const bool discardCurrentData)
{
	DEBUG_CHECK(dataSizeBytes != 0);
	DEBUG_CHECK(newData != nullptr);

	R_BIND_GL_VAO_IF_NOT_CURRENT(glVaoHandle);

	if (target == GL_ARRAY_BUFFER)
	{
		R_BIND_GL_BUFFER_IF_NOT_CURRENT(glBufferHandle, target, currentGLVbo);
	}
	else if (target == GL_ELEMENT_ARRAY_BUFFER)
	{
		R_BIND_GL_BUFFER_IF_NOT_CURRENT(glBufferHandle, target, currentGLIbo);
	}
	else
	{
		R_LOG_FATAL("Invalid GL buffer target!");
	}

	// If the caller is going to overwrite the whole buffer, we can use
	// the buffer reallocation pattern to avoid a CPU<=>GPU synchronization.
	// Also allow the new size to be bigger than the previous in this case.
	if (discardCurrentData && offsetBytes == 0 && dataSizeBytes >= currentSizeBytes)
	{
		glBufferData(target, dataSizeBytes, newData, bufferUsage);
	}
	else
	{
		DEBUG_CHECK(dataSizeBytes <= currentSizeBytes);
		DEBUG_CHECK(offsetBytes    < currentSizeBytes);
		glBufferSubData(target, offsetBytes, dataSizeBytes, newData);
	}

	R_CHECK_GL_ERRORS();

	// No practical way for returning a failure in OpenGL. We disable
	// glGetError on release, since it can slow things down significantly,
	// but if we do get an error here, then it will at least show up in the debug logs.
	return true;
}

} // namespace ogl {}
} // namespace render {}
} // namespace atlas {}
