
// ================================================================================================
// -*- C++ -*-
// File: rhi_manager_common.cpp
// Author: Guilherme R. Lampert
// Created on: 03/06/15
// Brief: OpenGL-based back-end implementation of the RhiManager.
// ================================================================================================

#include "atlas/render/opengl/rhi_manager.hpp"
#include "atlas/render/render_log.hpp"
#include <cstdio> // For std::sscanf (GL version string parsing)

namespace atlas
{
namespace render
{
namespace ogl
{

// ================================================================================================
// ogl::RhiManagerImpl -- Common RHI methods | startup/shutdown.
// ================================================================================================

// ========================================================
// Local helper functions:
// ========================================================

namespace
{

const char * getGLErrorString(const GLenum errorCode)
{
	switch (errorCode)
	{
	case GL_NO_ERROR                      : return "GL_NO_ERROR";
	case GL_INVALID_ENUM                  : return "GL_INVALID_ENUM";
	case GL_INVALID_VALUE                 : return "GL_INVALID_VALUE";
	case GL_INVALID_OPERATION             : return "GL_INVALID_OPERATION";
	case GL_INVALID_FRAMEBUFFER_OPERATION : return "GL_INVALID_FRAMEBUFFER_OPERATION";
	case GL_OUT_OF_MEMORY                 : return "GL_OUT_OF_MEMORY";
	case GL_STACK_UNDERFLOW               : return "GL_STACK_UNDERFLOW"; // Legacy; not used on GL3+
	case GL_STACK_OVERFLOW                : return "GL_STACK_OVERFLOW";  // Legacy; not used on GL3+
	default                               : return "Unknown GL error";
	} // switch (errorCode)
}

} // namespace {}

//
// Static members of RhiManagerImpl:
//
bool RhiManagerImpl::initializedGL   = false;
bool RhiManagerImpl::instanceCreated = false;

// ========================================================
// RhiManagerImpl::RhiManagerImpl():
// ========================================================

RhiManagerImpl::RhiManagerImpl()
{
	DEBUG_CHECK(!instanceCreated && "Only one instance of RhiManager is allowed!");
	internalReset();
	instanceCreated = true;
}

// ========================================================
// RhiManagerImpl::~RhiManagerImpl():
// ========================================================

RhiManagerImpl::~RhiManagerImpl()
{
	DEBUG_CHECK(instanceCreated && "This flag should be set!");
	shutdown();
	instanceCreated = false;
}

// ========================================================
// RhiManagerImpl::initRhiBackEnd():
// ========================================================

bool RhiManagerImpl::initRhiBackEnd(shell::RenderWindow * renderWin,
                                    const int requiredVersionMajor,
                                    const int requiredVersionMinor)
{
	DEBUG_CHECK(renderWin != nullptr);

	R_LOG_INFO("---- RhiManager::initRhiBackEnd() ----");

	if (initializedGL)
	{
		R_LOG_WARN("Duplicate initialization of the OpenGL RHI!");
		return false;
	}

	if (!gl3wInit())
	{
		R_LOG_ERROR("\'gl3wInit()\' failed!");
		return false;
	}

	// Check version if the caller didn't pass -1.
	if (requiredVersionMajor > 0 && requiredVersionMinor >= 0)
	{
		if (!gl3wIsSupported(requiredVersionMajor, requiredVersionMinor))
		{
			R_LOG_ERROR("Required OpenGL version " << requiredVersionMajor << "."
				<< requiredVersionMinor << " is not supported!");

			return false;
		}
	}

	renderWindow = renderWin;
	contexParams = &renderWin->getContextParams();

	queryGLInfo();
	setDefaultGLStates();

	// Make sure we start out with a cleared screen as soon as possible.
	glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);
	glClear(GL_COLOR_BUFFER_BIT);
	glFlush();
	renderWindow->swapBuffers();

	// This pointer can be safely shared by all resources
	// since we only allow for a single RhiManager instance.
	Resource::rhiManager = this;

	// Set up the dummy FrameBuffer object for the screen FBO.
	initScreenFrameBuffer();

	initializedGL = true;
	R_CHECK_GL_ERRORS();
	return true;
}

// ========================================================
// RhiManagerImpl::isInitialized():
// ========================================================

bool RhiManagerImpl::isInitialized() const
{
	return instanceCreated && initializedGL;
}

// ========================================================
// RhiManagerImpl::shutdown():
// ========================================================

void RhiManagerImpl::shutdown()
{
	if (!initializedGL)
	{
		return;
	}

	R_LOG_INFO("---- RhiManager::shutdown() ----");

	gl3wShutdown();
	internalReset();

	Resource::rhiManager = nullptr;
	initializedGL = false;
}

// ========================================================
// RhiManagerImpl::internalReset():
// ========================================================

void RhiManagerImpl::internalReset()
{
	renderWindow        = nullptr;
	contexParams        = nullptr;
	majorGLVersion      = 0;
	minorGLVersion      = 0;
	versionNumGLSL      = 0;
	maxGLAnisotropy     = 0;
	maxGLTextureSize    = 0;
	maxGLDrawBuffers    = 0;
	runtimeErrorChecks  = true;
	textureFilter       = Texture::Filter::Bilinear;
	currentGLProg       = 0;
	currentGLVao        = 0;
	currentGLVbo        = 0;
	currentGLIbo        = 0;
	currentGLFbo        = 0;
	currentVertexLayout = nullptr;
	clearColor          = core::Vec4(0.0f, 0.0f, 0.0f, 1.0f);
	clearDepth          = 0.0f;
	clearStencil        = 0;
	vertexLayoutCount   = 0;

	currentGLTextures.fill(0);
	drawBuffers.fill(GL_NONE);

	// Default color output 0 is always enabled.
	drawBuffers[0] = GL_COLOR_ATTACHMENT0;
}

// ========================================================
// RhiManagerImpl::queryGLInfo():
// ========================================================

void RhiManagerImpl::queryGLInfo()
{
	// GL/GLSL version numbers:
	glGetIntegerv(GL_MAJOR_VERSION, &majorGLVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorGLVersion);

	int slMajor = 0;
	int slMinor = 0;
	const char * glslVersionStr = reinterpret_cast<const char *>(glGetString(GL_SHADING_LANGUAGE_VERSION));

	if (std::sscanf(glslVersionStr, "%d.%d", &slMajor, &slMinor) == 2)
	{
		versionNumGLSL = (slMajor * 100) + slMinor;
	}
	else // Fall back to the lowest acceptable version:
	{
		// Assume #version 150 - OpenGL 3.2
		versionNumGLSL = 150;
	}

	// Misc GL limits/max-values:
	queryMaxGLTexAnisotropy();
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxGLTextureSize);
	glGetIntegerv(GL_MAX_DRAW_BUFFERS, &maxGLDrawBuffers);

	if (maxGLDrawBuffers <= 0)
	{
		maxGLDrawBuffers = 1;
	}
	else if (unsigned(maxGLDrawBuffers) > drawBuffers.getSize())
	{
		maxGLDrawBuffers = drawBuffers.getSize();
	}

	R_LOG_INFO("GL_VENDOR_STR..............: \"" << reinterpret_cast<const char *>(glGetString(GL_VENDOR))  << "\"");
	R_LOG_INFO("GL_VERSION_STR.............: \"" << reinterpret_cast<const char *>(glGetString(GL_VERSION)) << "\"");
	R_LOG_INFO("GL_SL_VERSION_STR..........: \"" << reinterpret_cast<const char *>(glGetString(GL_SHADING_LANGUAGE_VERSION)) << "\"");
	R_LOG_INFO("GL_VERSION_NUM.............: " << majorGLVersion << "." << minorGLVersion);
	R_LOG_INFO("GL_SL_VERSION_NUM..........: " << versionNumGLSL);
	R_LOG_INFO("GL_MAX_TEXTURE_ANISOTROPY..: " << maxGLAnisotropy);
	R_LOG_INFO("GL_MAX_TEXTURE_SIZE........: " << maxGLTextureSize);
	R_LOG_INFO("GL_MAX_DRAW_BUFFERS........: " << maxGLDrawBuffers);
}

// ========================================================
// RhiManagerImpl::setDefaultGLStates():
// ========================================================

void RhiManagerImpl::setDefaultGLStates()
{
	// These are not the GL defaults, but required for our LH view/projection.
	clearDepth = 0.0f;
	glClearDepth(clearDepth);
	glDepthFunc(GL_GEQUAL);
	glEnable(GL_DEPTH_TEST);
}

// ========================================================
// RhiManagerImpl::checkRhiBackEndErrors():
// ========================================================

void RhiManagerImpl::checkRhiBackEndErrors(const char * function, const char * filename, int lineNum)
{
	DEBUG_CHECK(isInitialized());

	//
	// This method does not respect the definition of RHI_NO_GL_ERROR_CHECKING,
	// so even if the preprocessor flag is defined, this function can still be
	// called in a hypothetical place where the error checking should always be done
	// (granted that the `runtimeErrorChecks` flag is set!)
	//

	if (!runtimeErrorChecks)
	{
		return;
	}

	// Caller might omit these if they are not relevant. Supply defaults.
	if (function == nullptr) { function = __FUNCTION__; }
	if (filename == nullptr) { filename = __FILE__;     }
	if (lineNum  <  0)       { lineNum  = __LINE__;     }

	GLenum errorCode = glGetError();
	while (errorCode != GL_NO_ERROR)
	{
		using core::AnsiColorCodes;

		R_LOG_WARN("OpenGL error " << core::toString(errorCode, 16) << " ("
			<< AnsiColorCodes::Red << getGLErrorString(errorCode) << AnsiColorCodes::Default
			<< ") in " << function << "(), file " << filename << "(" << lineNum << ")");

		errorCode = glGetError();
	}
}

// ========================================================
// RhiManagerImpl::clearRhiBackEndErrors():
// ========================================================

void RhiManagerImpl::clearRhiBackEndErrors()
{
	DEBUG_CHECK(isInitialized());

	if (!runtimeErrorChecks)
	{
		return;
	}

	while (glGetError() != GL_NO_ERROR) { }
}

// ========================================================
// RhiManagerImpl::freeResource():
// ========================================================

void RhiManagerImpl::freeResource(Resource & resource)
{
	DEBUG_CHECK(isInitialized());
	DEBUG_CHECK(!resource.isResourceLocked() && "Can't free a locked resource!");
	DEBUG_CHECK(Resource::rhiManager == this && "Resource belongs to another RHI manager!");

	const core::uint resourceFlags = resource.resourceFlags;

	//
	// Forward to the specialized resource cleanup method:
	//
	if (resourceFlags & Resource::Flag::TypeProgram)
	{
		freeGLProgram(static_cast<Program &>(resource));
	}
	else if (resourceFlags & Resource::Flag::TypeTexture)
	{
		freeGLTexture(static_cast<Texture &>(resource));
	}
	else if (resourceFlags & Resource::Flag::TypeFrameBuffer)
	{
		freeGLFrameBuffer(static_cast<FrameBuffer &>(resource));
	}
	else if (resourceFlags & Resource::Flag::TypeVertexBuffer)
	{
		freeGLVertexBuffer(static_cast<VertexBuffer &>(resource));
	}
	else if (resourceFlags & Resource::Flag::TypeIdxVertexBuffer)
	{
		freeGLIndexedVertexBuffer(static_cast<IndexedVertexBuffer &>(resource));
	}
	else
	{
		// Deal with other types here if necessary.
		R_LOG_FATAL("Resource type is undefined!");
	}
}

// ========================================================
// RhiManagerImpl::getScreenWidth():
// ========================================================

core::uint RhiManagerImpl::getScreenWidth() const
{
	DEBUG_CHECK(contexParams != nullptr);
	return contexParams->screenWidthPx;
}

// ========================================================
// RhiManagerImpl::getScreenHeight():
// ========================================================

core::uint RhiManagerImpl::getScreenHeight() const
{
	DEBUG_CHECK(contexParams != nullptr);
	return contexParams->screenHeightPx;
}

// ========================================================
// RhiManagerImpl::getScreenDimensions():
// ========================================================

core::Vec2u RhiManagerImpl::getScreenDimensions() const
{
	DEBUG_CHECK(contexParams != nullptr);
	return core::Vec2u(contexParams->screenWidthPx,
	                   contexParams->screenHeightPx);
}

} // namespace ogl {}
} // namespace render {}
} // namespace atlas {}
