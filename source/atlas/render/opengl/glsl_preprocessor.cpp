
// ================================================================================================
// -*- C++ -*-
// File: glsl_preprocessor.cpp
// Author: Guilherme R. Lampert
// Created on: 06/08/15
// Brief: A tiny GLSL preprocessor that resolves shader includes in a portable way.
// ================================================================================================

#include "atlas/render/opengl/glsl_preprocessor.hpp"
#include "atlas/render/render_log.hpp"

namespace atlas
{
namespace render
{
namespace ogl
{

// ========================================================
// Local helpers / constants:
// ========================================================

namespace
{

struct ValidShaderFileExt
{
	const char * extString;
	ProgramStage::StageType extProgStage;
};

const ValidShaderFileExt validShaderFileExtensions[] =
{
	// NOTE: Add more here as needed!
	{ "vert", ProgramStage::Vertex   },
	{ "frag", ProgramStage::Fragment }
};

COMPILE_TIME_CHECK(core::arrayLength(validShaderFileExtensions) == ProgramStage::Count,
		"Keep this array in sync with the enum declaration!");

//
// This is the size of a stack allocated buffer that
// we use to read-in each line of a shader file. This is
// set to a fairly high value, but nevertheless arbitrary.
// So make sure shader file lines are not longer than
// this upper limit.
//
constexpr int MaxShaderFileLineChars = 4096;

// ========================================================
// readLine() [LOCAL]:
// ========================================================

//
// Read a "line" from the input. That is, copy chars to `destLineBuffer`
// until a '\n' or the end of `fileContents` is found, whichever comes
// first. The new line char IS included in the output.
//
// Returns false only if nothing was written into `destLineBuffer`.
//
bool readLine(core::uint & firstChar, core::uint & lineLength,
              core::ubyte * destLineBuffer, int maxCharsToRead,
              const core::ByteArray & fileContents)
{
	DEBUG_CHECK(fileContents.isValid());

	core::uint contentsLength = fileContents.getSize();
	core::ubyte * destPtr     = destLineBuffer;

	while (--maxCharsToRead)
	{
		if (firstChar >= contentsLength)
		{
			if (destPtr == destLineBuffer)
			{
				lineLength = 0;
				return false;
			}
			break;
		}

		const core::ubyte c = fileContents[firstChar++];
		if ((*destPtr++ = c) == '\n')
		{
			break;
		}
	}

	*destPtr = '\0';
	lineLength = static_cast<core::uint>(destPtr - destLineBuffer);
	return true;
}

// ========================================================
// prependText() [LOCAL]:
// ========================================================

//
// Prepend the default definitions and the optional user
// definitions to `preprocessedOutput`, if the input strings
// are not empty. `optionalDefinitions` array must be
// terminated by a null pointer!
//
void prependText(core::ByteArray & preprocessedOutput,
                 const core::String & defaultDefinitions,
                 const char ** optionalDefinitions)
{
	const core::ubyte * bytePtr;

	if (!defaultDefinitions.isEmpty())
	{
		#ifndef ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS
		static const core::ubyte comment[] = "\n// DEFAULT DEFINITIONS:\n";
		preprocessedOutput.pushBack(comment, core::arrayLength(comment) - 1);
		#endif // ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS

		bytePtr = reinterpret_cast<const core::ubyte *>(defaultDefinitions.getCStr());
		preprocessedOutput.pushBack(bytePtr, defaultDefinitions.getLength());
	}

	if (optionalDefinitions != nullptr)
	{
		#ifndef ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS
		static const core::ubyte comment[] = "\n// USER DEFINITIONS:\n";
		preprocessedOutput.pushBack(comment, core::arrayLength(comment) - 1);
		#endif // ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS

		for (int i = 0; optionalDefinitions[i] != nullptr; ++i)
		{
			bytePtr = reinterpret_cast<const core::ubyte *>(optionalDefinitions[i]);
			preprocessedOutput.pushBack(bytePtr, core::strLength(optionalDefinitions[i]));
			preprocessedOutput.pushBack('\n');
		}
	}
}

// ========================================================
// setUpProgramStage() [LOCAL]:
// ========================================================

//
// Sets up the ProgramStage type according
// to source filename extension.
//
bool setUpProgramStage(ProgramStage & programStageOut,
                       const core::String & filename,
                       const core::ByteArray & shaderSource)
{
	if (shaderSource.isEmpty())
	{
		R_LOG_ERROR("Shader stage file \"" << filename << "\" has no data!");
		return false;
	}

	programStageOut.nativeCode = shaderSource.getData();
	programStageOut.nativeCodeSizeBytes = shaderSource.getSize();

	const core::String fileExt = core::getFilenameExtension(filename, /* includeDot = */ false);
	for (core::uint i = 0; i < core::arrayLength(validShaderFileExtensions); ++i)
	{
		if (fileExt == validShaderFileExtensions[i].extString)
		{
			programStageOut.stageType = validShaderFileExtensions[i].extProgStage;
			return true;
		}
	}

	// Missing or wrong file extension in the filename.
	// Can't guess shader stage type without it.
	R_LOG_ERROR("Can't guess shader ProgramStage type from filename extension \'" << fileExt << "\'!");
	return false;
}

// ========================================================
// discardLine() [LOCAL]:
// ========================================================

#ifdef ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS

//
// Returns true if the line consist of a C/C++-style
// comment and thus should not be included in the output.
//
// Will also increment the in/out pointers past the comments.
//
bool discardLine(core::uint & firstChar, core::uint & lineLength, core::uint & lineNumber,
                 const char *& firstNonBlank, bool & brokenComment, const core::String & filename,
                 const core::ByteArray & shaderSourceOut)
{
	// Less than two non-blank chars, not enough for a comment.
	if (lineLength < 2)
	{
		return false;
	}

	// Single line C++-style comment that we can easily skip.
	if (firstNonBlank[0] == '/' && firstNonBlank[1] == '/')
	{
		return true;
	}

	// Multi-line C style comment:
	if (firstNonBlank[0] == '/' && firstNonBlank[1] == '*')
	{
		while (*firstNonBlank != '\0')
		{
			if (firstNonBlank[0] == '*' && firstNonBlank[1] == '/')
			{
				return true;
			}

			++firstNonBlank;
		}

		// End of comment was not on the same line; find the end.
		core::uint newLineNum = lineNumber;
		while (firstChar < shaderSourceOut.getSize())
		{
			if (shaderSourceOut[firstChar] == '\n')
			{
				++newLineNum;
			}

			if (shaderSourceOut[firstChar] == '*' &&
			    shaderSourceOut[firstChar + 1] == '/')
			{
				firstChar += 2;
				break;
			}

			++firstChar;
		}

		if (firstChar >= shaderSourceOut.getSize())
		{
			R_LOG_ERROR("Unexpected end of shader file \"" << filename
				<< "\" while skipping multi-line comment started on line "
				<< lineNumber << "!");

			brokenComment = true;
		}

		lineNumber = newLineNum;
		return true;
	}

	return false; // Keep this line.
}

#endif // ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS

} // namespace {}

// ================================================================================================
// SimpleGlslPreprocessor class implementation:
// ================================================================================================

// ========================================================
// SimpleGlslPreprocessor::SimpleGlslPreprocessor():
// ========================================================

SimpleGlslPreprocessor::SimpleGlslPreprocessor()
	: zip(nullptr)
{
}

// ========================================================
// SimpleGlslPreprocessor::SimpleGlslPreprocessor():
// ========================================================

SimpleGlslPreprocessor::SimpleGlslPreprocessor(const core::ZipReader & zipReader)
	: zip(&zipReader)
{
}

// ========================================================
// SimpleGlslPreprocessor::~SimpleGlslPreprocessor():
// ========================================================

SimpleGlslPreprocessor::~SimpleGlslPreprocessor()
{
	preloadedIncludes.clearAndDelete();
	// We don't own the `zip` pointer, so don't touch it.
}

// ========================================================
// SimpleGlslPreprocessor::setIncludePath():
// ========================================================

void SimpleGlslPreprocessor::setIncludePath(const char * path)
{
	if (path == nullptr || *path == '\0')
	{
		defaultIncludePath.clear();
		return;
	}

	defaultIncludePath = path;
	if (defaultIncludePath.back() != core::getPathSeparatorChar())
	{
		defaultIncludePath.pushBack(core::getPathSeparatorChar());
	}
}

// ========================================================
// SimpleGlslPreprocessor::setShaderPath():
// ========================================================

void SimpleGlslPreprocessor::setShaderPath(const char * path)
{
	if (path == nullptr || *path == '\0')
	{
		defaultShaderPath.clear();
		return;
	}

	defaultShaderPath = path;
	if (defaultShaderPath.back() != core::getPathSeparatorChar())
	{
		defaultShaderPath.pushBack(core::getPathSeparatorChar());
	}
}

// ========================================================
// SimpleGlslPreprocessor::addDefaultDefinition():
// ========================================================

void SimpleGlslPreprocessor::addDefaultDefinition(const char * text)
{
	DEBUG_CHECK(text != nullptr);
	defaultDefinitions += text;
	defaultDefinitions += "\n";
}

// ========================================================
// SimpleGlslPreprocessor::clearDefaultDefinitions():
// ========================================================

void SimpleGlslPreprocessor::clearDefaultDefinitions()
{
	defaultDefinitions.clear();
}

// ========================================================
// SimpleGlslPreprocessor::preloadShaderInclude():
// ========================================================

bool SimpleGlslPreprocessor::preloadShaderInclude(const core::String & filename)
{
	DEBUG_CHECK(!filename.isEmpty());

	core::String fullFilePath;
	const core::String * pPath;

	if (defaultIncludePath.isEmpty())
	{
		pPath = &filename;
	}
	else
	{
		fullFilePath.allocate(defaultIncludePath.getLength() + filename.getLength(), false);
		fullFilePath  = defaultIncludePath;
		fullFilePath += filename;
		pPath = &fullFilePath;
	}

	if (findShaderInclude(*pPath))
	{
		R_LOG_INFO("Preload request: shader include file \"" << *pPath << "\" is already loaded!");
		return true;
	}

	R_LOG_INFO("Preloading shader include file \"" << *pPath << "\"...");
	return loadShaderInclude(*pPath, nullptr);
}

// ========================================================
// SimpleGlslPreprocessor::preloadShaderIncludesInPath():
// ========================================================

int SimpleGlslPreprocessor::preloadShaderIncludesInPath(const core::String & path, const bool recursiveScan)
{
	DEBUG_CHECK(!path.isEmpty());

	//TODO
	// Need directory iteration for this!
	// Also must support zip directory listing!
	// NOTE: Remember to append the defaultIncludePath to path!
	(void)path;
	(void)recursiveScan;
	return 0;
}

// ========================================================
// SimpleGlslPreprocessor::clearPreloadedIncludes():
// ========================================================

void SimpleGlslPreprocessor::clearPreloadedIncludes()
{
	preloadedIncludes.clearAndDelete();
}

// ========================================================
// SimpleGlslPreprocessor::loadShader():
// ========================================================

bool SimpleGlslPreprocessor::loadShader(core::ByteArray & shaderSourceOut, ProgramStage & programStageOut,
                                        const core::String & filename, const char ** optionalDefinitions,
                                        const bool keepIncludes)
{
	DEBUG_CHECK(!filename.isEmpty());

	core::String fullFilePath;
	const core::String * pPath;

	if (defaultShaderPath.isEmpty())
	{
		pPath = &filename;
	}
	else
	{
		fullFilePath.allocate(defaultShaderPath.getLength() + filename.getLength(), false);
		fullFilePath  = defaultShaderPath;
		fullFilePath += filename;
		pPath = &fullFilePath;
	}

	shaderSourceOut.clear(); // Ensure empty.

	core::ErrorInfo errorInfo;
	if (!loadFileHelper(*pPath, shaderSourceOut, &errorInfo))
	{
		R_LOG_ERROR("Unable to load shader source file \"" << *pPath << "\": " << errorInfo);
		return false;
	}

	if (!resolveIncludesAndDefs(shaderSourceOut, *pPath, optionalDefinitions, keepIncludes))
	{
		R_LOG_ERROR("Failed to resolve includes for shader \"" << *pPath << "\"!");
		return false;
	}

	if (!setUpProgramStage(programStageOut, *pPath, shaderSourceOut))
	{
		R_LOG_ERROR("Failed to set up a ProgramStage for shader \"" << *pPath << "\"!");
		return false;
	}

	return true;
}

// ========================================================
// SimpleGlslPreprocessor::resolveIncludesAndDefs():
// ========================================================

bool SimpleGlslPreprocessor::resolveIncludesAndDefs(core::ByteArray & shaderSourceOut, const core::String & filename,
                                                    const char ** optionalDefinitions, const bool keepIncludes)
{
	static const char strInclude[] = "#include";

	char lineBuffer[MaxShaderFileLineChars];
	core::String tempStr, includeFilePath;

	core::uint lineNumber = 0;
	core::uint lineLength = 0;
	core::uint firstChar  = 0;
	core::ubyte * lineBytePtr = reinterpret_cast<core::ubyte *>(lineBuffer);

	// The output file will be at least as big as the input + default defs.
	core::ByteArray preprocessedFile;
	preprocessedFile.allocate(shaderSourceOut.getSize() + defaultDefinitions.getLength());

	// Additional #defines/directives go first.
	prependText(preprocessedFile, defaultDefinitions, optionalDefinitions);

	while (readLine(firstChar, lineLength, lineBytePtr,
	       MaxShaderFileLineChars, shaderSourceOut))
	{
		++lineNumber;

		// Skip leading white space.
		const char * firstNonBlank = lineBuffer;
		while (core::ctype::isSpace(*firstNonBlank))
		{
			++firstNonBlank;
		}

		// Optimize for blank lines and anything not starting with a '#'.
		if (*firstNonBlank == '\0' || *firstNonBlank != '#' ||
		    core::strCmpN(firstNonBlank, strInclude, core::arrayLength(strInclude) - 1) != 0)
		{
			//
			// Note:
			// If this preprocessor switch is defined, we don't add
			// blank lines and lines starting with a comment to the output
			// file. Leading blanks are also skipped. This should make
			// the output slightly smaller and perhaps save some memory,
			// so it is a simple and easy optimization you can enable.
			//
			#ifdef ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS

			lineLength = core::strLength(firstNonBlank);
			bool brokenComment = false;

			if (lineLength != 0 && !discardLine(firstChar, lineLength, lineNumber,
			    firstNonBlank, brokenComment, filename, shaderSourceOut))
			{
				// Not a comment or blank line; add it.
				preprocessedFile.pushBack(reinterpret_cast<const core::ubyte *>(firstNonBlank), lineLength);
			}

			if (brokenComment)
			{
				return false;
			}

			#else // !ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS

			preprocessedFile.pushBack(lineBytePtr, lineLength);

			#endif // ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS

			continue;
		}

		const char * incPtr = firstNonBlank;
		while (*incPtr != '\0' && *incPtr != '"')
		{
			++incPtr;
		}

		if (*incPtr == '\0')
		{
			R_LOG_ERROR("\"" << filename << "\": Empty/broken include directive at line " << lineNumber << ".");
			return false;
		}

		++incPtr; // Was pointing at the '"'

		// Copy the filename/path between double quotes:
		includeFilePath.clear();
		while (*incPtr != '\0' && *incPtr != '"')
		{
			includeFilePath.pushBack(*incPtr++);
		}

		if (*incPtr != '"')
		{
			R_LOG_ERROR("\"" << filename << "\": Include directive at line " << lineNumber << " is missing a closing quote!");
			return false;
		}

		// Might need to append the default include path.
		if (!defaultIncludePath.isEmpty())
		{
			tempStr = includeFilePath;
			includeFilePath  = defaultIncludePath;
			includeFilePath += tempStr;
		}

		R_LOG_INFO("(" << filename << ":" << lineNumber << ") Loading shader include \"" << includeFilePath << "\"...");

		const ShaderFile * shaderInclude = findShaderInclude(includeFilePath);
		if (shaderInclude != nullptr)
		{
			R_LOG_INFO("Shader include \"" << includeFilePath << "\" already in cache!");
		}
		else // Load a fresh one:
		{
			ShaderFile * newFile = new ShaderFile();

			if (!loadShaderInclude(includeFilePath, newFile))
			{
				R_LOG_ERROR("Failed to load new shader include \"" << includeFilePath << "\"!");
				delete newFile;
				return false;
			}

			// Optionally cache the new include for future use.
			if (keepIncludes)
			{
				preloadedIncludes.pushFront(newFile);
			}

			shaderInclude = newFile;
		}

		// Debug comment to mark where the #include was.
		#ifndef ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS
		tempStr = "\n// INCLUDED FILE: " + includeFilePath + "\n";
		preprocessedFile.pushBack(reinterpret_cast<const core::ubyte *>(tempStr.getCStr()),
		                          tempStr.getLength());
		#endif // ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS

		if (!shaderInclude->fileContents.isEmpty())
		{
			#if ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS

			core::uint includeLineNumber = 0;
			core::uint includeFirstChar  = 0;
			const core::ByteArray & includeContents = shaderInclude->fileContents;

			while (readLine(includeFirstChar, lineLength, lineBytePtr,
			       MaxShaderFileLineChars, includeContents))
			{
				++includeLineNumber;

				// Skip leading white space.
				firstNonBlank = lineBuffer;
				while (core::ctype::isSpace(*firstNonBlank))
				{
					++firstNonBlank;
				}

				lineLength = core::strLength(firstNonBlank);
				bool brokenComment = false;

				if (lineLength != 0 && !discardLine(includeFirstChar, lineLength, includeLineNumber,
				    firstNonBlank, brokenComment, includeFilePath, includeContents))
				{
					// Not a comment or blank line; add it.
					preprocessedFile.pushBack(reinterpret_cast<const core::ubyte *>(firstNonBlank), lineLength);
				}

				if (brokenComment)
				{
					return false;
				}
			}

			#else // !ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS

			preprocessedFile.pushBack(shaderInclude->fileContents.getData(),
			                          shaderInclude->fileContents.getSize());

			#endif // ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS
		}

		// #line directive for the GLSL compiler, so that eventual error messages
		// point to some place inside the original file, not the included one.
		tempStr = "#line " + core::toString(lineNumber) + "\n";
		preprocessedFile.pushBack(reinterpret_cast<const core::ubyte *>(tempStr.getCStr()),
		                          tempStr.getLength());
	}

	shaderSourceOut.clear();
	preprocessedFile.transferTo(shaderSourceOut);
	return true;
}

// ========================================================
// SimpleGlslPreprocessor::findShaderInclude():
// ========================================================

const SimpleGlslPreprocessor::ShaderFile * SimpleGlslPreprocessor::findShaderInclude(const core::String & filename) const
{
	// Note: Assumes the default include path is already prepended to the filename!

	const core::uint nameHash          = filename.hash();
	const ShaderFileList::Iterator it  = preloadedIncludes.begin();
	const ShaderFileList::Iterator end = preloadedIncludes.end();

	while (it != end)
	{
		if (it->filenameHash == nameHash)
		{
			return &(*it);
		}
		++it;
	}

	return nullptr;
}

// ========================================================
// SimpleGlslPreprocessor::loadShaderInclude():
// ========================================================

bool SimpleGlslPreprocessor::loadShaderInclude(const core::String & filename, ShaderFile * optionalDestFile)
{
	// Note: Assumes the default include path is already prepended to the filename!

	core::ErrorInfo errorInfo;
	core::ByteArray fileContents;
	if (!loadFileHelper(filename, fileContents, &errorInfo))
	{
		R_LOG_ERROR("Unable to load shader include \"" << filename << "\": " << errorInfo);
		return false;
	}

	// When this param is null, we just append the new include to `preloadedIncludes`.
	if (optionalDestFile == nullptr)
	{
		ShaderFile * newFile = new ShaderFile();

		newFile->filename     = filename;
		newFile->filenameHash = filename.hash();
		fileContents.transferTo(newFile->fileContents);

		// Make it the new head of the linked list.
		preloadedIncludes.pushFront(newFile);
	}
	else
	{
		optionalDestFile->filename     = filename;
		optionalDestFile->filenameHash = filename.hash();
		fileContents.transferTo(optionalDestFile->fileContents);
	}

	R_LOG_INFO("Successfully loaded new shader include \"" << filename << "\".");
	return true;
}

// ========================================================
// SimpleGlslPreprocessor::loadFileHelper():
// ========================================================

bool SimpleGlslPreprocessor::loadFileHelper(const core::String & filename,
                                            core::ByteArray & fileContents,
                                            core::ErrorInfo * errorInfo) const
{
	if (zip != nullptr)
	{
		return zip->extractFileToMemoryByName(filename, fileContents, errorInfo);
	}
	else
	{
		return core::FileSystem::loadFile(filename, fileContents, errorInfo);
	}
}

// ========================================================
// SimpleGlslPreprocessor::logPreloadedIncludes():
// ========================================================

void SimpleGlslPreprocessor::logPreloadedIncludes(core::LogStream & logStr) const
{
	using core::AnsiColorCodes;

	int fileNumber = 0;
	const ShaderFileList::Iterator it  = preloadedIncludes.begin();
	const ShaderFileList::Iterator end = preloadedIncludes.end();

	logStr << AnsiColorCodes::White << "[[ Preloaded includes list ]]"
		<< AnsiColorCodes::Default << "\n";

	while (it != end)
	{
		logStr << AnsiColorCodes::Yellow << "[" <<  fileNumber << "] id="
			<< AnsiColorCodes::Red << core::toString(it->filenameHash, 16).padRight(10, '0')
			<< AnsiColorCodes::Yellow << ", file=" << AnsiColorCodes::Red
			<< it->filename << AnsiColorCodes::Default << "\n";

		++it, ++fileNumber;
	}

	logStr << AnsiColorCodes::White << "[[ Listed " << fileNumber
		<< " files ]]" << AnsiColorCodes::Default << "\n";
}

} // namespace ogl {}
} // namespace render {}
} // namespace atlas {}
