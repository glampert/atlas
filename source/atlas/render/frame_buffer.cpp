
// ================================================================================================
// -*- C++ -*-
// File: frame_buffer.cpp
// Author: Guilherme R. Lampert
// Created on: 14/08/15
// Brief: Frame-buffer and off-screen render targets.
// ================================================================================================

#include "atlas/render/frame_buffer.hpp"

namespace atlas
{
namespace render
{

// ========================================================
// FrameBuffer::FrameBuffer():
// ========================================================

FrameBuffer::FrameBuffer()
	: Resource(Resource::Flag::TypeFrameBuffer)
	, fboHandleGL(0)
	, dimensions(0,0)
	, forceRhiSurfacesDS(false)
	, isScreenFbo(false)
	, validatedOk(false)
{
	depthRenderTarget.asTexture        = nullptr;
	stencilRenderTarget.asTexture      = nullptr;
	depthStencilRenderTarget.asTexture = nullptr;
}

// ========================================================
// FrameBuffer::freeDSTextures():
// ========================================================

void FrameBuffer::freeDSTextures()
{
	if (forceRhiSurfacesDS)
	{
		// The RHI surfaces are cleaned up by the RhiManager.
		return;
	}

	if (depthRenderTarget.asTexture != nullptr)
	{
		delete depthRenderTarget.asTexture;
		depthRenderTarget.asTexture = nullptr;
	}
	if (stencilRenderTarget.asTexture != nullptr)
	{
		delete stencilRenderTarget.asTexture;
		stencilRenderTarget.asTexture = nullptr;
	}
	if (depthStencilRenderTarget.asTexture != nullptr)
	{
		delete depthStencilRenderTarget.asTexture;
		depthStencilRenderTarget.asTexture = nullptr;
	}
}

// ========================================================
// RhiManager::freeColorTextures():
// ========================================================

void FrameBuffer::freeColorTextures()
{
	// PtrArray owns every pointer, and so deletes them when cleared.
	colorRenderTargets.clear();
}

// ========================================================
// FrameBuffer::hasDepthRenderTarget():
// ========================================================

bool FrameBuffer::hasDepthRenderTarget() const
{
	if (forceRhiSurfacesDS)
	{
		return depthRenderTarget.asRhiSurface != 0;
	}
	return depthRenderTarget.asTexture != nullptr;
}

// ========================================================
// FrameBuffer::hasStencilRenderTarget():
// ========================================================

bool FrameBuffer::hasStencilRenderTarget() const
{
	if (forceRhiSurfacesDS)
	{
		return stencilRenderTarget.asRhiSurface != 0;
	}
	return stencilRenderTarget.asTexture != nullptr;
}

// ========================================================
// FrameBuffer::hasDepthStencilRenderTarget():
// ========================================================

bool FrameBuffer::hasDepthStencilRenderTarget() const
{
	if (forceRhiSurfacesDS)
	{
		return depthStencilRenderTarget.asRhiSurface != 0;
	}
	return depthStencilRenderTarget.asTexture != nullptr;
}

// ========================================================
// FrameBuffer::getDepthRenderTarget():
// ========================================================

const Texture & FrameBuffer::getDepthRenderTarget() const
{
	DEBUG_CHECK(!forceRhiSurfacesDS);
	DEBUG_CHECK(depthRenderTarget.asTexture != nullptr);
	return *(depthRenderTarget.asTexture);
}

// ========================================================
// FrameBuffer::getStencilRenderTarget():
// ========================================================

const Texture & FrameBuffer::getStencilRenderTarget() const
{
	DEBUG_CHECK(!forceRhiSurfacesDS);
	DEBUG_CHECK(stencilRenderTarget.asTexture != nullptr);
	return *(stencilRenderTarget.asTexture);
}

// ========================================================
// FrameBuffer::getDepthStencilRenderTarget():
// ========================================================

const Texture & FrameBuffer::getDepthStencilRenderTarget() const
{
	DEBUG_CHECK(!forceRhiSurfacesDS);
	DEBUG_CHECK(depthStencilRenderTarget.asTexture != nullptr);
	return *(depthStencilRenderTarget.asTexture);
}

// ========================================================
// FrameBuffer::isValid():
// ========================================================

bool FrameBuffer::isValid() const
{
	if (isScreenFbo)
	{
		return true; // The dummy screen FrameBuffer is always valid.
	}

	return validatedOk && (fboHandleGL != 0)   &&
	       (colorRenderTargets.getSize() >= 1) &&
	       colorRenderTargets[0]->isValid()    &&
	       (resourceFlags & Resource::Flag::TypeFrameBuffer);
}

// ========================================================
// FrameBuffer::getMemoryBytes():
// ========================================================

core::uint FrameBuffer::getMemoryBytes() const
{
	core::uint memoryUsage;

	if (isScreenFbo) // Dummy object used to represent the screen.
	{
		memoryUsage  = dimensions.x * dimensions.y * 4; // 32bit color.
		memoryUsage += dimensions.x * dimensions.y * 3; // 24bit depth.
	}
	else
	{
		memoryUsage = 0;
		for (core::uint i = 0; i < colorRenderTargets.getSize(); ++i)
		{
			memoryUsage += colorRenderTargets[i]->getMemoryBytes();
		}

		// A rough estimate, since this is only used for debug printing.
		if (hasDepthStencilRenderTarget())
		{
			memoryUsage += dimensions.x * dimensions.y * 4; // 32bit depth/stencil buffer.
		}
		else
		{
			if (hasDepthRenderTarget())
			{
				memoryUsage += dimensions.x * dimensions.y * 3; // 24bit depth buffer.
			}
			if (hasStencilRenderTarget())
			{
				memoryUsage += dimensions.x * dimensions.y * 2; // 16bit stencil buffer.
			}
		}
	}

	return memoryUsage + colorRenderTargets.getMemoryBytes() + sizeof(FrameBuffer);
}

} // namespace render {}
} // namespace atlas {}
