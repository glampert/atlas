
// ================================================================================================
// -*- C++ -*-
// File: doom3_map_importer.hpp
// Author: Guilherme R. Lampert
// Created on: 13/08/15
// Brief: Helper class to import Doom 3 .proc/.map files into our engine.
// ================================================================================================

#ifndef ATLAS_RENDER_DOOM3_MAP_IMPORTER_HPP
#define ATLAS_RENDER_DOOM3_MAP_IMPORTER_HPP

#include "atlas/render/model3d.hpp"

namespace atlas
{
namespace render
{

// ========================================================
// class DoomMapImporter:
// ========================================================

class DoomMapImporter final
	: private core::NonCopyable
{
public:

	struct MapVertexBuffer
	{
		core::uint usageFlags;
		core::uint vertexCount;
		core::uint indexCount;
		core::uint vertexSize;
		core::uint indexSize;
		DrawVertex * vertexPtr;
		DrawIndex  * indexPtr;

		bool isValid() const { return vertexPtr != nullptr && indexPtr != nullptr; }
	};

	// Sets everything to null/zero. Allocates models/surfaces from provided cache.
	DoomMapImporter(Model3dCache & mdlCache);

	// If an import was done but the results were not
	// claimed via `getImportedData()`, then the destructor
	// will dispose all the remain data and models.
	~DoomMapImporter();

	// Loads and parses a `.proc` file from Doom 3. Some minimal validation is
	// done, but the filename extension is not checked to ensure it is = ".proc"
	// Upon successful completion, call `getImportedData()` to retrieve the loaded
	// models and vertex buffer, clearing the importer for another possible import.
	bool importProcFile(const core::String & filename);

	// Moves the contents of the previous import to the output parameters.
	// You cannot import again until this is done. Also, if the contents
	// of an import are not claimed until class destruction, the destructor
	// takes care of freeing the leftover data.
	void getImportedData(Model3dArray & modelsOut, MapVertexBuffer & modelsVertexBufferOut);

	// Manually discards the data from the previous import,
	// allowing the importer to be run again with new data.
	void clearImportedData();

private:

	Model3dCache & modelCache;

	// Contains the successfully parsed models from
	// a '.proc' file, regardless of errors encountered.
	Model3dArray models;

	// The vertex buffer for all the map geometry/models is only
	// allocated and filled if loading is successful. If an error
	// occurs mid-loading, it will remain null/invalid.
	MapVertexBuffer modelsVertexBuffer;

	// Internal helpers:
	bool parseProcModel(core::Lexer & procFile);
	void setUpVertexBufferData();
};

} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_DOOM3_MAP_IMPORTER_HPP
