
// ================================================================================================
// -*- C++ -*-
// File: resource.cpp
// Author: Guilherme R. Lampert
// Created on: 04/07/15
// Brief: Rendering Hardware Interface (RHI) resource types and handles.
// ================================================================================================

#include "atlas/render/renderer.hpp"

namespace atlas
{
namespace render
{

// ================================================================================================
// Resource class implementation:
// ================================================================================================

RhiManager * Resource::rhiManager = nullptr;

// ========================================================
// Resource::Resource():
// ========================================================

Resource::Resource(const core::uint32 flags)
	: resourceFlags(flags)
{
}

// ========================================================
// Resource::~Resource():
// ========================================================

Resource::~Resource()
{
	if (rhiManager != nullptr)
	{
		rhiManager->freeResource(*this);
	}
}

// ================================================================================================
// toString()s from 'program.hpp':
// ================================================================================================

core::String toString(const ProgramStage::StageType type)
{
	static const char * strings[] =
	{
		"Vertex",
		"Fragment"
	};
	COMPILE_TIME_CHECK(core::arrayLength(strings) == ProgramStage::Count,
			"Keep this array in sync with the enum declaration!");

	DEBUG_CHECK(unsigned(type) < ProgramStage::Count);
	return strings[type];
}

core::String toString(const ProgramSetupResult::State state)
{
	static const char * strings[] =
	{
		"Successful",
		"SuccessfulWithWarnings",
		"CompileFailed",
		"LinkFailed",
		"MissingStageError",
		"OutOfMemoryError",
		"UnknownError"
	};
	COMPILE_TIME_CHECK(core::arrayLength(strings) == ProgramSetupResult::Count,
			"Keep this array in sync with the enum declaration!");

	DEBUG_CHECK(unsigned(state) < ProgramSetupResult::Count);
	return strings[state];
}

// ================================================================================================
// toString()s from 'texture.hpp':
// ================================================================================================

core::String toString(const Texture::DataLayout::Enum layout)
{
	return core::PixelFormat::fromLayout(layout).toString();
}

core::String toString(const Texture::Target::Enum target)
{
	static const char * strings[] =
	{
		"Default",
		"DiffuseMap",
		"NormalMap",
		"SpecularMap",
		"AlphaMap",
		"HeightMap",
		"EmissiveMap",
		"CubeMap",
		"DepthBuffer",
		"StencilBuffer",
		"DepthStencil",
		"Renderer"
	};
	COMPILE_TIME_CHECK(core::arrayLength(strings) == Texture::Target::Count,
			"Keep this array in sync with the enum declaration!");

	DEBUG_CHECK(unsigned(target) < Texture::Target::Count);
	return strings[target];
}

core::String toString(const Texture::Filter::Enum filter)
{
	static const char * strings[] =
	{
		"Default",
		"Nearest",
		"Bilinear",
		"Trilinear",
		"Anisotropic"
	};
	COMPILE_TIME_CHECK(core::arrayLength(strings) == Texture::Filter::Count,
			"Keep this array in sync with the enum declaration!");

	DEBUG_CHECK(unsigned(filter) < Texture::Filter::Count);
	return strings[filter];
}

core::String toString(const Texture::Addressing::Enum addressing)
{
	static const char * strings[] =
	{
		"Default",
		"ClampToEdge",
		"ClampToBlack",
		"ClampToWhite",
		"ClampToAlpha0",
		"Repeat",
		"RepeatMirrored"
	};
	COMPILE_TIME_CHECK(core::arrayLength(strings) == Texture::Addressing::Count,
			"Keep this array in sync with the enum declaration!");

	DEBUG_CHECK(unsigned(addressing) < Texture::Addressing::Count);
	return strings[addressing];
}

core::String toString(const Texture::MappingUnit::Enum texUnit)
{
	static const char * strings[] =
	{
		"TMU0",
		"TMU1",
		"TMU2",
		"TMU3",
		"TMU4",
		"TMU5",
		"TMU6",
		"TMU7",
		"TMU8",
		"TMU9"
	};
	COMPILE_TIME_CHECK(core::arrayLength(strings) == Texture::MappingUnit::Count,
			"Keep this array in sync with the enum declaration!");

	DEBUG_CHECK(unsigned(texUnit) < Texture::MappingUnit::Count);
	return strings[texUnit];
}

// ================================================================================================
// toString()s from 'vertex_buffer.hpp':
// ================================================================================================

core::String toString(const RenderMode::Enum mode)
{
	static const char * strings[] =
	{
		"Triangles",
		"Points",
		"LineLoop",
		"LineStip",
		"Lines"
	};
	COMPILE_TIME_CHECK(core::arrayLength(strings) == RenderMode::Count,
			"Keep this array in sync with the enum declaration!");

	DEBUG_CHECK(unsigned(mode) < RenderMode::Count);
	return strings[mode];
}

} // namespace render {}
} // namespace atlas {}
