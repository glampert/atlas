
// ================================================================================================
// -*- C++ -*-
// File: program.hpp
// Author: Guilherme R. Lampert
// Created on: 13/07/15
// Brief: Interface for the rendering programs (AKA shaders).
// ================================================================================================

#ifndef ATLAS_RENDER_PROGRAM_HPP
#define ATLAS_RENDER_PROGRAM_HPP

#include "atlas/render/resource.hpp"
#include "atlas/render/texture.hpp"

namespace atlas
{
namespace render
{

// ========================================================
// struct ProgramVertexLayout:
// ========================================================

struct ProgramVertexLayout
{
	enum Type
	{
		// Scalar values:
		Int, Float,

		// Float vectors:
		V2F, V3F, V4F,

		// Number or entries in the enum. Internal use.
		Count
	};

	struct VertexElement
	{
		// Use sized types and a fixed length name to keep
		// ProgramVertexLayout in the 256 bytes neighborhood.
		core::int16 index;
		core::int16 type;

		static const core::uint MaxNameChars = 28;
		char name[MaxNameChars];
	};

	// Up to 8 elements per vertex.
	// The count is the number of elements used in the array.
	core::uint vertexElementCount;
	core::SizedArray<VertexElement, 8> vertexElements;

	// Constructor sets the vertex layout to empty, with 0 elements.
	ProgramVertexLayout();

	// Deep VertexElement-wise copy and comparison:
	ProgramVertexLayout & operator = (const ProgramVertexLayout & other);
	bool operator == (const ProgramVertexLayout & other) const;
};

// ========================================================
// struct ProgramStage:
// ========================================================

struct ProgramStage
{
	enum StageType
	{
		Vertex,
		Fragment,

		// Number of entries in this enum. Internal use.
		Count
	};

	const core::ubyte * nativeCode;
	core::uint nativeCodeSizeBytes;
	StageType stageType;
};

core::String toString(ProgramStage::StageType type);

// ========================================================
// struct ProgramSetupResult:
// ========================================================

struct ProgramSetupResult
{
	enum State
	{
		// Should render just fine:
		Successful,

		// Might still be valid for rendering (check infoLog):
		SuccessfulWithWarnings,

		// Can't be used for rendering (check infoLog for error messages):
		CompileFailed,
		LinkFailed,
		MissingStageError,
		OutOfMemoryError,
		UnknownError,

		// Number of entries in this enum. Internal use.
		Count
	};

	core::String infoLog;
	State state;
};

core::String toString(ProgramSetupResult::State state);

// ========================================================
// class Program:
// ========================================================

class Program final
	: public Resource
{
public:

	Program();

	//
	// Program rendering parameters:
	//

	int findRenderParamByName(const core::String & paramName) const;
	int findRenderParamByNameHash(core::uint32 paramNameHash) const;

	bool setRenderParamInt(int paramIndex, int value);
	bool setRenderParamBool(int paramIndex, bool value);
	bool setRenderParamFloat(int paramIndex, float value);
	bool setRenderParamSamplerUnit(int paramIndex, int tmuIndex);

	bool setRenderParamVec2(int paramIndex, const core::Vec2 & value);
	bool setRenderParamVec3(int paramIndex, const core::Vec3 & value);
	bool setRenderParamVec4(int paramIndex, const core::Vec4 & value);

	bool setRenderParamMatrix(int paramIndex, const float * m, core::uint squareSize);

	//
	// Vertex input layout (Vertex Shader stage):
	//

	const ProgramVertexLayout & getVertexLayout() const;
	void setVertexLayout(const ProgramVertexLayout & layout);

	//
	// Miscellaneous program queries:
	//

	bool isValid() const;
	bool needToCommitRenderParams()  const;
	core::uint getRenderParamCount() const;
	core::uint getMemoryBytes() const;

private:

	friend RhiManager;

	struct ParamData
	{
		struct MatrixRef
		{
			const float * data;
			core::uint squareSize;
		};

		union ValueUnion
		{
			// Data interpreted based on ParamData::type.
			int        asInt;
			float      asFloat;
			core::Vec2 asVec2;
			core::Vec3 asVec3;
			core::Vec4 asVec4;
			MatrixRef  asMatrixRef;

			// Zero fill this structure. All data is POD, so this is fine.
			ValueUnion() { std::memset(this, 0, sizeof(*this)); }
		};

		enum Type
		{
			Int, Bool, Float,
			V2F, V3F, V4F,
			Matrix, SamplerIdx,

			// Number or entries in the enum. Internal use.
			Count
		};

		ValueUnion  value;        // Value field selected according to type.
		GLint       uniformLocGL; // Uniform location queried from the program. Valid if >= 0.
		core::int16 type;         // One of Type constants, as an explicitly sized integer.
		core::int16 isDirty;      // Flag set if it must be synchronized with the back-end.

		ParamData() : uniformLocGL(-1), type(0), isDirty(0) { }
		bool isValid() const { return uniformLocGL >= 0; }
	};

	// Keep the name with its hash code to accelerate lookup.
	typedef core::HashedStr<core::String> ParamName;

	// The OpenGL shader program name/handle.
	GLuint programHandleGL;

	// Pointer to shared vertex shader input layout.
	// Set by the RhiManager on program allocation, based
	// on the type declared in the shader source code.
	// This is not owned by Program, thus never freed by it.
	const ProgramVertexLayout * vertexLayout;

	// Data is split into hot/cold arrays for better CPU cache usage.
	// The render param values are the "hot" data, accessed
	// very frequently, so those are kept tightly packed inside a
	// separate array. The parameter names are accessed infrequently,
	// mostly during initialization, so they can be placed on
	// a separate "cold" array. This allows us to put more
	// render param values in the cache when rendering.
	//
	// Both array should always have the same number of elements and
	// in the same order. E.g.: renderParamNames[0] is the name of
	// renderParamValues[0], and so on.
	core::Array<ParamData> renderParamValues; // Not sorted; may be empty.
	core::Array<ParamName> renderParamNames;  // String+hash for accelerated search. Same order of renderParamValues.
	bool renderParamsDirty;                   // Set whenever a render param is modified.
};

// ================================================================================================
// Program inline methods:
// ================================================================================================

inline const ProgramVertexLayout & Program::getVertexLayout() const
{
	DEBUG_CHECK(vertexLayout != nullptr);
	return *vertexLayout;
}

inline void Program::setVertexLayout(const ProgramVertexLayout & layout)
{
	vertexLayout = &layout;
}

inline core::uint Program::getRenderParamCount() const
{
	DEBUG_CHECK(renderParamValues.getSize() == renderParamNames.getSize());
	return renderParamValues.getSize();
}

inline bool Program::needToCommitRenderParams() const
{
	return renderParamsDirty;
}

} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_PROGRAM_HPP
