
// ================================================================================================
// -*- C++ -*-
// File: resource.hpp
// Author: Guilherme R. Lampert
// Created on: 02/06/15
// Brief: Rendering Hardware Interface (RHI) resource types and handles.
// ================================================================================================

#ifndef ATLAS_RENDER_RESOURCE_HPP
#define ATLAS_RENDER_RESOURCE_HPP

// Core Library dependencies:
#include "atlas/core/utils.hpp"
#include "atlas/core/maths.hpp"
#include "atlas/core/memory.hpp"
#include "atlas/core/strings.hpp"
#include "atlas/core/containers.hpp"

// The RHI implementation-specific resource data:
#if ATLAS_RHI_BACKEND_OPENGL
	#include "atlas/render/opengl/rhi_specific.hpp"
#else
	#error "Define a RHI back-end implementation for the current platform!"
#endif // RHI_BACKEND_XYZ

namespace atlas
{
namespace render
{

// ========================================================
// class Resource:
// ========================================================

class Resource
	: private core::NonCopyable
{
public:

	struct Flag
	{
		enum Enum
		{
			//
			// Resource data usage flags.
			// Required for resource allocation.
			//

			// The data store contents will be modified once and used many times.
			// (Very infrequent updates).
			UsageStatic  = BIT(0),

			// The data store contents will be modified repeatedly and used many times
			// (Once-per-frame of less updates).
			UsageDynamic = BIT(1),

			// The data store contents will be modified once and used once.
			// (Several updates per frame).
			UsageStream  = BIT(2),

			//
			// Resource locking access flags.
			// Required for RhiManager's lockResource()/unlockResource().
			//

			// Lock in read only mode. Can be combined with LockForWriting to allow RW.
			LockForReading = BIT(3),

			// Lock in write only mode. Can be combined with LockForReading to allow RW.
			LockForWriting = BIT(4),

			// Indicates that the previous contents of the resource may be discarded. Data within
			// the locked region is undefined with the exception of subsequently written data.
			// This flag cannot not be used in combination with `LockForReading`!
			LockInvalidate = BIT(5),

			// Locking flags necessary when locking a VertexBuffer or IndexedVertexBuffer,
			// to indicate which part of the buffer is to be mapped into main memory.
			LockVertexData = BIT(6),
			LockIndexData  = BIT(7),

			//
			// Resource type flags.
			// Used internally to distinguish from the available types.
			//

			TypeProgram         = BIT(8),  // render::Program
			TypeTexture         = BIT(9),  // render::Texture
			TypeFrameBuffer     = BIT(10), // render::FrameBuffer
			TypeVertexBuffer    = BIT(11), // render::VertexBuffer
			TypeIdxVertexBuffer = BIT(12), // render::IndexedVertexBuffer

			// Special flag used with resources that can be defaulted,
			// like textures and render programs. This is set if the
			// resource was set to a default fallback for whatever reasons.
			DefaultedResource = BIT(13)
		};
	};

	RhiManager & getResourceManager() const;
	core::uint getResourceFlags()     const;

	bool isResourceManagerValid() const;
	bool isResourceLocked()       const;

protected:

	friend RhiManager;

	// Renderer Resources are not directly instantiated.
	// Only its child classes can be instantiated.
	explicit Resource(core::uint32 flags);

	// Protected and non-virtual. Resource instances
	// are not meant to be directly deleted.
	~Resource();

	// Back-end that created and owns this resource.
	// Since we only allow a single RhiManager instance
	// during runtime, this is fine as a static variable.
	// Set by `RhiManager::initRhiBackEnd()` and cleared
	// by `RhiManager::shutdown()`.
	static RhiManager * rhiManager;

	// Miscellaneous resource flags. See the `Resource::Flag` enum above.
	core::uint32 resourceFlags;
};

// ================================================================================================
// Resource inline methods:
// ================================================================================================

inline RhiManager & Resource::getResourceManager() const
{
	DEBUG_CHECK(rhiManager != nullptr);
	return *rhiManager;
}

inline core::uint Resource::getResourceFlags() const
{
	return resourceFlags;
}

inline bool Resource::isResourceManagerValid() const
{
	return rhiManager != nullptr;
}

inline bool Resource::isResourceLocked() const
{
	return resourceFlags & (Flag::LockForReading | Flag::LockForWriting);
}

} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_RESOURCE_HPP
