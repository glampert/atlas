
// ================================================================================================
// -*- C++ -*-
// File: vertex_buffer.hpp
// Author: Guilherme R. Lampert
// Created on: 17/07/15
// Brief: VertexBuffer class and friends.
// ================================================================================================

#ifndef ATLAS_RENDER_VERTEX_BUFFER_HPP
#define ATLAS_RENDER_VERTEX_BUFFER_HPP

#include "atlas/render/resource.hpp"
#include "atlas/render/program.hpp"

namespace atlas
{
namespace render
{

// ========================================================
// Render mode constants:
// ========================================================

struct RenderMode
{
	enum Enum
	{
		// Supported modes for the RhiManager draw calls:
		Triangles,
		Points,
		LineLoop,
		LineStip,
		Lines,

		// Number of entries in this enum. Internal use.
		Count
	};
};

core::String toString(RenderMode::Enum mode);

// ========================================================
// class VertexBuffer:
// ========================================================

class VertexBuffer final
	: public Resource
{
public:

	VertexBuffer();
	bool isValid() const;

	core::uint getVertexCount()   const;
	core::uint getSizeOfAVertex() const;
	core::uint getMemoryBytes()   const;

private:

	friend RhiManager;

	GLuint vaoHandleGL;
	GLuint vboHandleGL;

	core::uint32 vertexCount;
	core::uint32 sizeOfAVertex;

	// Vertex input layout is set when the vertex buffer is first used for
	// rendering to the current vertex layout of the active render program.
	// Subsequent rendering calls might reset it to another value if the current
	// vertex layout changes. This pointer is owned and maintained by the RhiManager.
	const ProgramVertexLayout * vertexLayout;
};

// ========================================================
// class IndexedVertexBuffer:
// ========================================================

class IndexedVertexBuffer final
	: public Resource
{
public:

	IndexedVertexBuffer();
	bool isValid() const;

	core::uint getVertexCount()   const;
	core::uint getSizeOfAVertex() const;
	core::uint getIndexCount()    const;
	core::uint getSizeOfAnIndex() const;
	core::uint getMemoryBytes()   const;

private:

	friend RhiManager;

	GLuint vaoHandleGL;
	GLuint vboHandleGL;
	GLuint iboHandleGL;

	core::uint32 vertexCount;
	core::uint32 indexCount;
	core::uint32 sizeOfAVertex;
	core::uint32 sizeOfAnIndex;

	// See the comment on VertexBuffer.
	const ProgramVertexLayout * vertexLayout;
};

// ================================================================================================
// VertexBuffer inline methods:
// ================================================================================================

inline VertexBuffer::VertexBuffer()
	: Resource(Resource::Flag::TypeVertexBuffer)
	, vaoHandleGL(0)
	, vboHandleGL(0)
	, vertexCount(0)
	, sizeOfAVertex(0)
	, vertexLayout(nullptr)
{
}

inline bool VertexBuffer::isValid() const
{
	return (vaoHandleGL   != 0) &&
	       (vboHandleGL   != 0) &&
	       (vertexCount   != 0) &&
	       (sizeOfAVertex != 0) &&
	       (resourceFlags & Resource::Flag::TypeVertexBuffer);
}

inline core::uint VertexBuffer::getVertexCount() const
{
	return vertexCount;
}

inline core::uint VertexBuffer::getSizeOfAVertex() const
{
	return sizeOfAVertex;
}

inline core::uint VertexBuffer::getMemoryBytes() const
{
	return (vertexCount * sizeOfAVertex) + sizeof(VertexBuffer);
}

// ================================================================================================
// IndexedVertexBuffer inline methods:
// ================================================================================================

inline IndexedVertexBuffer::IndexedVertexBuffer()
	: Resource(Resource::Flag::TypeIdxVertexBuffer)
	, vaoHandleGL(0)
	, vboHandleGL(0)
	, iboHandleGL(0)
	, vertexCount(0)
	, indexCount(0)
	, sizeOfAVertex(0)
	, sizeOfAnIndex(0)
	, vertexLayout(nullptr)
{
}

inline bool IndexedVertexBuffer::isValid() const
{
	return (vaoHandleGL   != 0) &&
	       (vboHandleGL   != 0) &&
	       (iboHandleGL   != 0) &&
	       (vertexCount   != 0) &&
	       (indexCount    != 0) &&
	       (sizeOfAVertex != 0) &&
	       (sizeOfAnIndex != 0) &&
	       (resourceFlags & Resource::Flag::TypeIdxVertexBuffer);
}

inline core::uint IndexedVertexBuffer::getVertexCount() const
{
	return vertexCount;
}

inline core::uint IndexedVertexBuffer::getSizeOfAVertex() const
{
	return sizeOfAVertex;
}

inline core::uint IndexedVertexBuffer::getIndexCount() const
{
	return indexCount;
}

inline core::uint IndexedVertexBuffer::getSizeOfAnIndex() const
{
	return sizeOfAnIndex;
}

inline core::uint IndexedVertexBuffer::getMemoryBytes() const
{
	return (vertexCount * sizeOfAVertex) +
	       (indexCount  * sizeOfAnIndex) +
	       sizeof(IndexedVertexBuffer);
}

} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_VERTEX_BUFFER_HPP
