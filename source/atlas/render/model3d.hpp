
// ================================================================================================
// -*- C++ -*-
// File: model3d.hpp
// Author: Guilherme R. Lampert
// Created on: 13/08/15
// Brief: Base class and related helpers for 3D models, meshes and surfaces.
// ================================================================================================

#ifndef ATLAS_RENDER_MODEL3D_HPP
#define ATLAS_RENDER_MODEL3D_HPP

#include "atlas/render/material.hpp"
#include "atlas/render/draw_vertex.hpp"
#include "atlas/core/memory/object_pool.hpp"

namespace atlas
{
namespace render
{

// ========================================================
// class Model3dSurface:
// ========================================================

// 3D model surface or mesh. Just a set of indexed triangles
// together with a material. Surfaces can also be named.
class Model3dSurface final
	: private core::NonCopyable
{
public:

	//TODO make private to force using Model3dCache::createSurface!
	Model3dSurface(core::String & surfaceName,
	               core::String & materialName,
	               DrawVertexArray & surfaceVertexes,
	               DrawIndexArray  & surfaceIndexes,
	               core::uint baseVertex, core::uint baseIndex);

	const DrawVertex * getVertexData() const;
	const DrawIndex  * getIndexData()  const;

	core::uint getVertexCount() const;
	core::uint getIndexCount()  const;

	core::uint getFirstVbVertex() const;
	core::uint getFirstVbIndex()  const;

	const core::String & getName() const;

    //TODO make 'em private!
    // NOTE: once constructed, should remain constant!
    // unless for dynamic models, which can't be shared.

	DrawVertexArray vertexes;
	DrawIndexArray  indexes;

	core::uint firstVbVertex;
	core::uint firstVbIndex;

	core::String material; // TODO just a const pointer instead?
	core::String name;
};

// Surfaces are freed with operator `delete`, so a PtrArray is the best choice.
typedef core::PtrArray<Model3dSurface> Model3dSurfaceArray;

// ========================================================
// class Model3d:
// ========================================================

// Base model/geometry type. Holds an array of model surfaces.
// No methods allowing mutating the model after construction are provided.
class Model3d
	: private core::NonCopyable
	, public  core::HashTableNode<Model3d>
{
public:

	Model3d(core::String & modelName, Model3dSurfaceArray & modelSurfaces);
	virtual ~Model3d();

	// Model surface read-only access:
	core::uint getSurfaceCount() const;
	const Model3dSurface & getSurface(core::uint index) const;
	const Model3dSurface * findSurfaceByName(const core::String & surfName) const;

	// Miscellaneous getters:
	const core::String & getName() const;
	virtual bool isStatic()  const = 0;
	virtual bool isDynamic() const = 0;

protected:

	Model3dSurfaceArray surfaces;
	core::String name;
};

// A common pointer array because Model3dCache owns every model instance.
typedef core::Array<const Model3d *> Model3dArray;

// ========================================================
// class StaticModel3d:
// ========================================================

// 3D model type used for static scene geometry and world geometry.
// Cannot be changed after construction, thus a StaticModel3d is
// always shareable.
class StaticModel3d final
	: public Model3d
{
public:

	StaticModel3d(core::String & modelName, Model3dSurfaceArray & modelSurfaces)
		: Model3d(modelName, modelSurfaces)
	{ }

	~StaticModel3d();

	bool isStatic()  const override { return true;  }
	bool isDynamic() const override { return false; }

private:
};

// ========================================================
// class Model3dCache:
// ========================================================

// Pool of StaticModel3d. Supply a reasonable default if missing.
#ifndef MODEL_CACHE_MEMPOOL_GRANULARITY_STATIC
	#define MODEL_CACHE_MEMPOOL_GRANULARITY_STATIC 256
#endif // MODEL_CACHE_MEMPOOL_GRANULARITY_STATIC

// Simple model 3D cache that retains ownership of every model.
// Allows creating new models as well as loading models from file.
// Static models can be shared, since they are immutable. Retains
// ownership of every object that gets cached, freeing them in its destructor.
class Model3dCache final
	: private core::NonCopyable
{
public:

	 Model3dCache(RhiManager & rhiMgr);
	~Model3dCache();

	// Models surfaces are heavily used, so we provide a custom
	// pool allocator to optimize memory allocation times and reduce
	// fragmentation. It is safe to free the surfaces with operator
	// `delete`, since the class provides a custom set of new/delete.
	// overloads. These are not cached.
	Model3dSurface * createSurface(core::String & surfaceName,
	                               core::String & materialName,
	                               DrawVertexArray & surfaceVertexes,
	                               DrawIndexArray  & surfaceIndexes,
	                               core::uint baseVertex, core::uint baseIndex);

	// Allocate and cache a new StaticModel3d instance.
	const Model3d * createStaticModel(core::String & modelName, Model3dSurfaceArray & modelSurfaces);

	// Find or load a new model from file. If an error occurs/the file can't be found,
	// this returns a default cube model and sets the `isDefaulted` error flag if
	// the pointer was not null. Further error info is printed to the render log.
	const Model3d * find(const core::String & modelName, bool * isDefaulted = nullptr);

	// Returns the default cube model, used as fallback when we can't load a model.
	const Model3d * getDefaultModel();

	// This forces the cache to free every single model.
	// Calling this will method will invalidate every pointer
	// previously handed out by the cache, so use it with caution!
	void freeAllModels();

	// Size of the combined models caches:
	core::uint getSize() const { return loadedModels.getSize(); }
	bool isEmpty()       const { return loadedModels.isEmpty(); }

private:

	RhiManager & rhiManager;

	// Model table indexed by name.
	typedef core::IntrusiveHashTable<core::String, Model3d> ModelTable;
	ModelTable loadedModels;

	// Pool for StaticModel3d instances only.
	typedef core::ObjectPool<StaticModel3d, MODEL_CACHE_MEMPOOL_GRANULARITY_STATIC> StaticModelMemPool;
	StaticModelMemPool memPoolStaticModels;

	// Even though not a traditional singleton (to avoid global access),
	// only one instance of a Model3dCache is allowed for the whole engine.
	static bool instanceCreated;
};

// ================================================================================================
// Model3dSurface inline methods:
// ================================================================================================

inline const DrawVertex * Model3dSurface::getVertexData() const
{
	return vertexes.getData();
}

inline const DrawIndex * Model3dSurface::getIndexData() const
{
	return indexes.getData();
}

inline core::uint Model3dSurface::getVertexCount() const
{
	return vertexes.getSize();
}

inline core::uint Model3dSurface::getIndexCount() const
{
	return indexes.getSize();
}

inline core::uint Model3dSurface::getFirstVbVertex() const
{
	return firstVbVertex;
}

inline core::uint Model3dSurface::getFirstVbIndex() const
{
	return firstVbIndex;
}

inline const core::String & Model3dSurface::getName() const
{
	return name;
}

// ================================================================================================
// Model3d inline methods:
// ================================================================================================

inline core::uint Model3d::getSurfaceCount() const
{
	return surfaces.getSize();
}

inline const Model3dSurface & Model3d::getSurface(const core::uint index) const
{
	DEBUG_CHECK(surfaces[index] != nullptr);
	return *surfaces[index];
}

inline const core::String & Model3d::getName() const
{
	return name;
}

} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_MODEL3D_HPP
