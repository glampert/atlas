
// ================================================================================================
// -*- C++ -*-
// File: program.cpp
// Author: Guilherme R. Lampert
// Created on: 14/08/15
// Brief: Interface for the rendering programs (AKA shaders).
// ================================================================================================

#include "atlas/render/program.hpp"

namespace atlas
{
namespace render
{

// ================================================================================================
// ProgramVertexLayout class implementation:
// ================================================================================================

// ========================================================
// ProgramVertexLayout::ProgramVertexLayout():
// ========================================================

ProgramVertexLayout::ProgramVertexLayout()
	: vertexElementCount(0)
{
	vertexElements.zeroFill();
}

// ========================================================
// ProgramVertexLayout::operator = :
// ========================================================

ProgramVertexLayout & ProgramVertexLayout::operator = (const ProgramVertexLayout & other)
{
	if (&other == this)
	{
		return *this;
	}

	for (core::uint i = 0; i < other.vertexElementCount; ++i)
	{
		vertexElements[i].index = other.vertexElements[i].index;
		vertexElements[i].type  = other.vertexElements[i].type;
		core::strCopy(vertexElements[i].name, VertexElement::MaxNameChars, other.vertexElements[i].name);
	}

	vertexElementCount = other.vertexElementCount;
	return *this;
}

// ========================================================
// ProgramVertexLayout::operator == :
// ========================================================

bool ProgramVertexLayout::operator == (const ProgramVertexLayout & other) const
{
	if (&other == this)
	{
		return true; // Same object/self comparison.
	}

	if (vertexElementCount != other.vertexElementCount)
	{
		return false;
	}

	for (core::uint i = 0; i < vertexElementCount; ++i)
	{
		if (vertexElements[i].index != other.vertexElements[i].index) { return false; }
		if (vertexElements[i].type  != other.vertexElements[i].type)  { return false; }
		if (core::strCmp(vertexElements[i].name, other.vertexElements[i].name) != 0)
		{
			return false;
		}
	}

	return true; // Both are identical if we get here.
}

// ================================================================================================
// Program class implementation:
// ================================================================================================

// ========================================================
// Program::Program():
// ========================================================

Program::Program()
	: Resource(Resource::Flag::TypeProgram)
	, programHandleGL(0)
	, vertexLayout(nullptr)
	, renderParamsDirty(false)
{
}

// ========================================================
// Program::isValid():
// ========================================================

bool Program::isValid() const
{
	return (programHandleGL != 0)    &&
	       (vertexLayout != nullptr) &&
	       (resourceFlags & Resource::Flag::TypeProgram);
}

// ========================================================
// Program::findRenderParamByName():
// ========================================================

int Program::findRenderParamByName(const core::String & paramName) const
{
	DEBUG_CHECK(!paramName.isEmpty());

	const core::uint paramCount = getRenderParamCount();
	if (paramCount == 0)
	{
		return -1;
		 // Don't even compute the name hash if this program has no parameters.
	}

	const core::uint paramNameHash = paramName.hash();
	for (core::uint i = 0; i < paramCount; ++i)
	{
		const ParamName & name = renderParamNames[i];
		if (name.getHash() == paramNameHash)
		{
			DEBUG_CHECK(name.getString() == paramName);
			return i;
		}
	}
	return -1;
}

// ========================================================
// Program::findRenderParamByNameHash():
// ========================================================

int Program::findRenderParamByNameHash(const core::uint32 paramNameHash) const
{
	DEBUG_CHECK(paramNameHash != 0);

	const core::uint paramCount = getRenderParamCount();
	for (core::uint i = 0; i < paramCount; ++i)
	{
		if (renderParamNames[i].getHash() == paramNameHash)
		{
			return i;
		}
	}
	return -1;
}

// ========================================================
// Program::setRenderParamInt():
// ========================================================

bool Program::setRenderParamInt(const int paramIndex, const int value)
{
	if (paramIndex < 0 || unsigned(paramIndex) >= getRenderParamCount())
	{
		return false;
	}

	ParamData & rp = renderParamValues[paramIndex];
	DEBUG_CHECK(rp.isValid());

	rp.value.asInt = value;
	rp.type        = ParamData::Int;
	rp.isDirty     = true;

	return (renderParamsDirty = true);
}

// ========================================================
// Program::setRenderParamBool():
// ========================================================

bool Program::setRenderParamBool(const int paramIndex, const bool value)
{
	if (paramIndex < 0 || unsigned(paramIndex) >= getRenderParamCount())
	{
		return false;
	}

	ParamData & rp = renderParamValues[paramIndex];
	DEBUG_CHECK(rp.isValid());

	// Expand boolean to integer.
	rp.value.asInt = value;
	rp.type        = ParamData::Bool;
	rp.isDirty     = true;

	return (renderParamsDirty = true);
}

// ========================================================
// Program::setRenderParamFloat():
// ========================================================

bool Program::setRenderParamFloat(const int paramIndex, const float value)
{
	if (paramIndex < 0 || unsigned(paramIndex) >= getRenderParamCount())
	{
		return false;
	}

	ParamData & rp = renderParamValues[paramIndex];
	DEBUG_CHECK(rp.isValid());

	rp.value.asFloat = value;
	rp.type          = ParamData::Float;
	rp.isDirty       = true;

	return (renderParamsDirty = true);
}

// ========================================================
// Program::setRenderParamSamplerUnit():
// ========================================================

bool Program::setRenderParamSamplerUnit(const int paramIndex, const int tmuIndex)
{
	DEBUG_CHECK(tmuIndex >= 0 && tmuIndex < Texture::MappingUnit::Count);

	if (paramIndex < 0 || unsigned(paramIndex) >= getRenderParamCount())
	{
		return false;
	}

	ParamData & rp = renderParamValues[paramIndex];
	DEBUG_CHECK(rp.isValid());

	// Sets the integer field, but the type differentiates both kinds of parameters.
	rp.value.asInt = tmuIndex;
	rp.type        = ParamData::SamplerIdx;
	rp.isDirty     = true;

	return (renderParamsDirty = true);
}

// ========================================================
// Program::setRenderParamVec2():
// ========================================================

bool Program::setRenderParamVec2(const int paramIndex, const core::Vec2 & value)
{
	if (paramIndex < 0 || unsigned(paramIndex) >= getRenderParamCount())
	{
		return false;
	}

	ParamData & rp = renderParamValues[paramIndex];
	DEBUG_CHECK(rp.isValid());

	rp.value.asVec2 = value;
	rp.type         = ParamData::V2F;
	rp.isDirty      = true;

	return (renderParamsDirty = true);
}

// ========================================================
// Program::setRenderParamVec3():
// ========================================================

bool Program::setRenderParamVec3(const int paramIndex, const core::Vec3 & value)
{
	if (paramIndex < 0 || unsigned(paramIndex) >= getRenderParamCount())
	{
		return false;
	}

	ParamData & rp = renderParamValues[paramIndex];
	DEBUG_CHECK(rp.isValid());

	rp.value.asVec3 = value;
	rp.type         = ParamData::V3F;
	rp.isDirty      = true;

	return (renderParamsDirty = true);
}

// ========================================================
// Program::setRenderParamVec4():
// ========================================================

bool Program::setRenderParamVec4(const int paramIndex, const core::Vec4 & value)
{
	if (paramIndex < 0 || unsigned(paramIndex) >= getRenderParamCount())
	{
		return false;
	}

	ParamData & rp = renderParamValues[paramIndex];
	DEBUG_CHECK(rp.isValid());

	rp.value.asVec4 = value;
	rp.type         = ParamData::V4F;
	rp.isDirty      = true;

	return (renderParamsDirty = true);
}

// ========================================================
// Program::setRenderParamMatrix():
// ========================================================

bool Program::setRenderParamMatrix(int paramIndex, const float * m, const core::uint squareSize)
{
	DEBUG_CHECK(m != nullptr);
	DEBUG_CHECK(squareSize >= 2 && squareSize <= 4);

	if (paramIndex < 0 || unsigned(paramIndex) >= getRenderParamCount())
	{
		return false;
	}

	ParamData & rp = renderParamValues[paramIndex];
	DEBUG_CHECK(rp.isValid());

	//
	// Note that we only store a reference to the pointer, thus
	// avoiding copying a full matrix every time. Caller is
	// responsible for ensuring the pointer stays valid.
	//
	// To set the dirty flag when the matrix gets updated,
	// this method must be called again, with the same
	// matrix pointer as argument.
	//
	rp.value.asMatrixRef.data       = m;
	rp.value.asMatrixRef.squareSize = squareSize;

	rp.type    = ParamData::Matrix;
	rp.isDirty = true;

	return (renderParamsDirty = true);
}

// ========================================================
// Program::getMemoryBytes():
// ========================================================

core::uint Program::getMemoryBytes() const
{
	// Just a rough estimate by summing the size of this object
	// plus a guess of 1KB for the back-end counterpart of a Program.
	return renderParamValues.getMemoryBytes() +
	       renderParamNames.getMemoryBytes()  +
	       sizeof(Program) + (1024 * 1024);
}

} // namespace render {}
} // namespace atlas {}
