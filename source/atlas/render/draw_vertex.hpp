
// ================================================================================================
// -*- C++ -*-
// File: draw_vertex.hpp
// Author: Guilherme R. Lampert
// Created on: 13/08/15
// Brief: DrawVertex and DrawIndex, the common drawing vertex and index types.
// ================================================================================================

#ifndef ATLAS_RENDER_DRAW_VERTEX_HPP
#define ATLAS_RENDER_DRAW_VERTEX_HPP

// Core Library dependencies:
#include "atlas/core/utils.hpp"
#include "atlas/core/maths.hpp"
#include "atlas/core/strings.hpp"
#include "atlas/core/containers/arrays.hpp"

namespace atlas
{
namespace render
{

// ========================================================
// DrawVertex | DrawVertexArray types:
// ========================================================

struct DrawVertex
{
	core::Vec3 position;
	core::Vec3 normal;
	core::Vec2 texCoords;

	void clear();
};

typedef core::Array<DrawVertex> DrawVertexArray;

// ========================================================
// DrawVertex helper functions and inline methods:
// ========================================================

inline void DrawVertex::clear()
{
	position  = core::Vec3::origin();
	normal    = core::Vec3::origin();
	texCoords = core::Vec2::origin();
}

inline core::String toString(const DrawVertex & vert)
{
	return "{ " + core::toString(vert.position)  + ", " +
	              core::toString(vert.normal)    + ", " +
	              core::toString(vert.texCoords) + " }";
}

// ========================================================
// DrawIndex | DrawIndexArray types:
// ========================================================

#if ATLAS_RHI_DRAW_INDEX_USE_32BITS
	typedef core::uint32 DrawIndex;
#else // !ATLAS_RHI_DRAW_INDEX_USE_32BITS
	typedef core::uint16 DrawIndex;
#endif // ATLAS_RHI_DRAW_INDEX_USE_32BITS

typedef core::Array<DrawIndex> DrawIndexArray;

} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_DRAW_VERTEX_HPP
