
// ================================================================================================
// -*- C++ -*-
// File: render_log.hpp
// Author: Guilherme R. Lampert
// Created on: 10/07/15
// Brief: Log stream used by the back-end renderer thread.
// ================================================================================================

#ifndef ATLAS_RENDER_RENDER_LOG_HPP
#define ATLAS_RENDER_RENDER_LOG_HPP

#include "atlas/core/utils/default_log.hpp"

namespace atlas
{
namespace render
{

// FIXME This is temporary!!!
inline core::LogStream & getLog() { return atlas::core::getLog(); }
inline int getLogVerbosityLevel() { return core::LogLevel::Info;  }

// TODO
// At the moment we use the same log provided by the core library.
// Should create a specific log file for the renderer when switching
// to multi-thread rendering. The core log is currently redirected
// to the console, so this is equivalent to a `std::count`.
#define R_LOG_FATAL(message) LOG_FATAL_EX(atlas::core::getLog(), atlas::core::getLogVerbosityLevel(), "|RenderLog| " << message)
#define R_LOG_ERROR(message) LOG_ERROR_EX(atlas::core::getLog(), atlas::core::getLogVerbosityLevel(), "|RenderLog| " << message)
#define R_LOG_WARN(message)  LOG_WARN_EX(atlas::core::getLog(),  atlas::core::getLogVerbosityLevel(), "|RenderLog| " << message)
#define R_LOG_INFO(message)  LOG_INFO_EX(atlas::core::getLog(),  atlas::core::getLogVerbosityLevel(), "|RenderLog| " << message)

} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_RENDER_LOG_HPP
