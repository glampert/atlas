
// ================================================================================================
// -*- C++ -*-
// File: model3d.cpp
// Author: Guilherme R. Lampert
// Created on: 14/08/15
// Brief: Base class and related helpers for 3D models, meshes and surfaces.
// ================================================================================================

#include "atlas/render/model3d.hpp"
#include "atlas/render/render_log.hpp"

namespace atlas
{
namespace render
{

// ================================================================================================
// Model3dSurface class implementation:
// ================================================================================================

// ========================================================
// Model3dSurface::Model3dSurface():
// ========================================================

Model3dSurface::Model3dSurface(core::String & surfaceName,
                               core::String & materialName,
                               DrawVertexArray & surfaceVertexes,
                               DrawIndexArray  & surfaceIndexes,
                               const core::uint baseVertex,
                               const core::uint baseIndex)
{
	surfaceName.transferTo(name);
	materialName.transferTo(material);
	surfaceVertexes.transferTo(vertexes);
	surfaceIndexes.transferTo(indexes);

	firstVbVertex = baseVertex;
	firstVbIndex  = baseIndex;
}

// ================================================================================================
// Model3d class implementation:
// ================================================================================================

// ========================================================
// Model3d::Model3d():
// ========================================================

Model3d::Model3d(core::String & modelName, Model3dSurfaceArray & modelSurfaces)
{
	modelName.transferTo(name);
	modelSurfaces.transferTo(surfaces);
}

// ========================================================
// Model3d::~Model3d():
// ========================================================

Model3d::~Model3d()
{
	// Just defined here to anchor the vtable.
	// No manual cleanup needed.
}

// ========================================================
// Model3d::findSurfaceByName():
// ========================================================

const Model3dSurface * Model3d::findSurfaceByName(const core::String & surfName) const
{
	const core::uint surfaceCount = surfaces.getSize();
	for (core::uint s = 0; s < surfaceCount; ++s)
	{
		if (surfName == surfaces[s]->getName())
		{
			return surfaces[s];
		}
	}
	return nullptr; // Not found.
}

// ================================================================================================
// StaticModel3d class implementation:
// ================================================================================================

// ========================================================
// StaticModel3d::~StaticModel3d():
// ========================================================

StaticModel3d::~StaticModel3d()
{
	// Just defined here to anchor the vtable.
	// No manual cleanup needed.
}

// ================================================================================================
// Model3dCache class implementation:
// ================================================================================================

bool Model3dCache::instanceCreated = false;

// ========================================================
// Model3dCache::Model3dCache():
// ========================================================

Model3dCache::Model3dCache(RhiManager & rhiMgr)
	: rhiManager(rhiMgr)
	, loadedModels(/* allowDuplicateKeys = */ false)
{
	DEBUG_CHECK(!instanceCreated && "Only one instance of Model3dCache is allowed!");
	instanceCreated = true;

	(void)rhiManager; // Not currently used.
}

// ========================================================
// Model3dCache::~Model3dCache():
// ========================================================

Model3dCache::~Model3dCache()
{
	DEBUG_CHECK(instanceCreated && "This flag should be set!");
	freeAllModels();
	instanceCreated = false;
}

// ========================================================
// Model3dCache::freeAllModels():
// ========================================================

void Model3dCache::freeAllModels()
{
	R_LOG_INFO("---- Model3dCache::freeAllModels() ----");

	//TODO
	// careful to properly free static models from pool.
	// loadedModels may contain non-static stuff!
}

// ========================================================
// Model3dCache::createSurface():
// ========================================================

Model3dSurface * Model3dCache::createSurface(core::String & surfaceName,
                                             core::String & materialName,
                                             DrawVertexArray & surfaceVertexes,
                                             DrawIndexArray  & surfaceIndexes,
                                             const core::uint baseVertex,
                                             const core::uint baseIndex)
{
	//TODO there's good reason to provide a memory pool for this!
	// We use much more surfaces then models. Just need to override
	// operator new/delete so that they get feed properly.
	return new Model3dSurface(surfaceName, materialName,
			surfaceVertexes, surfaceIndexes, baseVertex, baseIndex);
}

// ========================================================
// Model3dCache::createStaticModel():
// ========================================================

const Model3d * Model3dCache::createStaticModel(core::String & modelName, Model3dSurfaceArray & modelSurfaces)
{
	StaticModel3d * smodel = memPoolStaticModels.allocate();
	memPoolStaticModels.construct(smodel, modelName, modelSurfaces);

	// Static models are always cached.
	if (!loadedModels.insert(smodel->getName(), smodel))
	{
		R_LOG_FATAL("Duplicate key in Model3dCache hash-table: " << smodel->getName());
	}

	return smodel;
}

// ========================================================
// Model3dCache::find():
// ========================================================

const Model3d * Model3dCache::find(const core::String & modelName, bool * isDefaulted)
{
	//TODO
	(void)modelName;
	(void)isDefaulted;
	return nullptr;
}

// ========================================================
// Model3dCache::getDefaultModel():
// ========================================================

const Model3d * Model3dCache::getDefaultModel()
{
	//TODO
	return nullptr;
}

} // namespace render {}
} // namespace atlas {}
