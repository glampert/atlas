
// ================================================================================================
// -*- C++ -*-
// File: frame_buffer.hpp
// Author: Guilherme R. Lampert
// Created on: 19/07/15
// Brief: Frame-buffer and off-screen render targets.
// ================================================================================================

#ifndef ATLAS_RENDER_FRAME_BUFFER_HPP
#define ATLAS_RENDER_FRAME_BUFFER_HPP

#include "atlas/render/resource.hpp"
#include "atlas/render/texture.hpp"

namespace atlas
{
namespace render
{

// ========================================================
// class FrameBuffer:
// ========================================================

class FrameBuffer final
	: public Resource
{
public:

	FrameBuffer();

	bool hasDepthRenderTarget() const;
	bool hasStencilRenderTarget() const;
	bool hasDepthStencilRenderTarget() const;

	const Texture & getDepthRenderTarget() const;
	const Texture & getStencilRenderTarget() const;
	const Texture & getDepthStencilRenderTarget() const;

	core::uint getColorRenderTargetCount() const;
	const Texture & getColorRenderTarget(core::uint index) const;

	bool isValid() const;
	bool isPowerOfTwo() const;
	bool isScreenFrameBuffer() const;
	bool isUsingOptimizedRhiSurfaces() const;

	core::Vec2u getDimensions() const;
	core::uint getMemoryBytes() const;

private:

	friend RhiManager;

	// Some platforms might optimize rendering to off-screen
	// surfaces other than textures (e.g. OpenGL renderbuffers).
	// If the user doesn't explicitly require the use of a texture
	// for the depth and stencil targets, then we default to the
	// optimized RHI render-buffer surface where available.
	union RenderTargetUnion
	{
		Texture * asTexture;
		GLuint asRhiSurface;
	};

	// freeDSTextures() deletes the `asTexture` pointers if they are in use.
	// These are called by `RhiManager::freeResource()`.
	void freeDSTextures();
	void freeColorTextures();

	// These are all optional, but some platforms might require them.
	RenderTargetUnion depthRenderTarget;        // Depth render target. Optional.
	RenderTargetUnion stencilRenderTarget;      // Stencil render target. Optional.
	RenderTargetUnion depthStencilRenderTarget; // If both depth and stencil buffers are used, this is set instead.

	// Must have at least one color output. Some platforms
	// also have Multiple Render Target (MRT) support.
	core::PtrArray<Texture> colorRenderTargets;

	// The OpenGL FBO name/handle.
	GLuint fboHandleGL;

	// All render target textures must have the same dimensions.
	core::Vec2u dimensions;

	// Forces the FrameBuffer to use the low-level optimized
	// RHI surfaces for the depth/stencil targets. Use the
	// `asRhiSurface` fields of RenderTargetUnion in this case.
	bool forceRhiSurfacesDS;

	// True only if this object is being used as a dummy
	// to represent the default screen frame buffer.
	bool isScreenFbo;

	// (GL only): True if the last glCheckFramebufferStatus was OK for this FBO.
	bool validatedOk;
};

// ================================================================================================
// FrameBuffer inline methods:
// ================================================================================================

inline bool FrameBuffer::isPowerOfTwo() const
{
	return core::math::isPowerOfTwo(dimensions.x) &&
	       core::math::isPowerOfTwo(dimensions.y);
}

inline bool FrameBuffer::isScreenFrameBuffer() const
{
	return isScreenFbo;
}

inline bool FrameBuffer::isUsingOptimizedRhiSurfaces() const
{
	return forceRhiSurfacesDS;
}

inline core::Vec2u FrameBuffer::getDimensions() const
{
	return dimensions;
}

inline core::uint FrameBuffer::getColorRenderTargetCount() const
{
	return colorRenderTargets.getSize();
}

inline const Texture & FrameBuffer::getColorRenderTarget(const core::uint index) const
{
	return *colorRenderTargets[index];
}

} // namespace render {}
} // namespace atlas {}

#endif // ATLAS_RENDER_FRAME_BUFFER_HPP
