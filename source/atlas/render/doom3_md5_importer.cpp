
// ================================================================================================
// -*- C++ -*-
// File: doom3_md5_importer.cpp
// Author: Guilherme R. Lampert
// Created on: 14/08/15
// Brief: Importer for Doom 3 MD5 animated models.
// ================================================================================================

#include "atlas/render/doom3_md5_importer.hpp"
#include "atlas/render/render_log.hpp"
#include "atlas/core/fs/path_utils.hpp"

namespace atlas
{
namespace render
{

// ========================================================
// DoomMd5ModelImporter::DoomMd5ModelImporter():
// ========================================================

DoomMd5ModelImporter::DoomMd5ModelImporter(Model3dCache & mdlCache)
	: modelCache(mdlCache)
	, totalDrawVerts(0)
	, totalDrawIndexes(0)
{
}

// ========================================================
// DoomMd5ModelImporter::clearImportedData():
// ========================================================

void DoomMd5ModelImporter::clearImportedData()
{
	md5Meshes.clear();
	md5Joints.clear();

	totalDrawVerts   = 0;
	totalDrawIndexes = 0;
}

// ========================================================
// DoomMd5ModelImporter::importMd5Static():
// ========================================================

const Model3d * DoomMd5ModelImporter::importMd5Static(const core::String & filename, core::String modelName)
{
	DEBUG_CHECK(!filename.isEmpty());

	// In case trying to reuse the same importer instance...
	if (!md5Meshes.isEmpty())
	{
		R_LOG_ERROR("DoomMd5ModelImporter: An import is still pending! Clear the previous import first!");
		return nullptr;
	}

	if (!parseMd5Full(filename))
	{
		R_LOG_ERROR("parseMd5Full() failed!");
		return nullptr;
	}

	//
	// Convert the base frame (bind pose) to a static render model.
	// Each DoomMd5Mesh becomes a Model3dSurface.
	//

	core::String surfaceName, materialName;
	Model3dSurfaceArray modelSurfaces;
	DrawVertexArray surfaceVertexes;
	DrawIndexArray  surfaceIndexes;

	const core::uint meshCount = md5Meshes.getSize();
	modelSurfaces.allocateExact(meshCount);

	for (core::uint m = 0; m < meshCount; ++m)
	{
		const DoomMd5Mesh & mesh = *md5Meshes[m];

		// Copy triangle/vertex indexes:
		const core::uint triangleCount = mesh.tris.getSize();
		surfaceIndexes.allocateExact(triangleCount * 3);

		for (core::uint t = 0; t < triangleCount; ++t)
		{
			surfaceIndexes.pushBack(mesh.tris[t].index[0]);
			surfaceIndexes.pushBack(mesh.tris[t].index[1]);
			surfaceIndexes.pushBack(mesh.tris[t].index[2]);
		}

		const core::uint baseIndex = totalDrawIndexes;
		totalDrawIndexes += triangleCount * 3;

		// Copy the vertexes:
		const core::uint vertexCount = mesh.verts.getSize();
		surfaceVertexes.allocateExact(vertexCount);

		for (core::uint v = 0; v < vertexCount; ++v)
		{
			const DoomMd5Vertex & vert = mesh.verts[v];
			core::Vec3 finalVertexPos(0.0f, 0.0f, 0.0f);

			// Calculate final vertex to draw with weights:
			for (int w = 0; w < vert.weightCount; ++w)
			{
				const DoomMd5Weight & weight = mesh.weights[vert.firstWeight + w];
				const DoomMd5Joint  & joint  = md5Joints[weight.joint];

				// Calculate transformed vertex for this weight:
				const core::Vec3 weightedVertexPos = joint.orient.rotatePoint(weight.pos);

				// The sum of all weight biases should be 1.0!
				finalVertexPos.x += (joint.pos.x + weightedVertexPos.x) * weight.bias;
				finalVertexPos.y += (joint.pos.y + weightedVertexPos.y) * weight.bias;
				finalVertexPos.z += (joint.pos.z + weightedVertexPos.z) * weight.bias;
			}

			DrawVertex drawVert;
			drawVert.clear();

			// Swizzle y/z. idSoftware historically used this different axis layout.
			drawVert.position.x = finalVertexPos.x;
			drawVert.position.y = finalVertexPos.z;
			drawVert.position.z = finalVertexPos.y;
			drawVert.texCoords  = vert.texCoords;
			// The rest is computed dynamically.

			surfaceVertexes.pushBack(drawVert);
		}

		const core::uint baseVertex = totalDrawVerts;
		totalDrawVerts += vertexCount;

		// Add the new surface to out list:
		surfaceName  = mesh.name;
		materialName = mesh.material;
		Model3dSurface * newSurface = modelCache.createSurface(
		                              surfaceName,     materialName,
		                              surfaceVertexes, surfaceIndexes,
		                              baseVertex,      baseIndex);

		DEBUG_CHECK(surfaceVertexes.isEmpty());
		DEBUG_CHECK(surfaceIndexes.isEmpty());

		modelSurfaces.pushBack(newSurface);
	}

	return modelCache.createStaticModel(modelName, modelSurfaces);
}

// ========================================================
// DoomMd5ModelImporter::parseMd5Full():
// ========================================================

bool DoomMd5ModelImporter::parseMd5Full(const core::String & filename)
{
	R_LOG_INFO("Beginning import of DOOM3 MD5 model \"" << filename << "\"...");

	constexpr int Md5Version = 10;
	constexpr int LexFlags   = (core::Lexer::Flag::AllowPathNames      |
	                            core::Lexer::Flag::NoStringEscapeChars |
	                            core::Lexer::Flag::NoFatalErrors);

	core::Token token;
	core::Lexer md5File(&render::getLog(), render::getLogVerbosityLevel());

	if (!md5File.initFromFile(filename, LexFlags))
	{
		return false;
	}

	md5File.expectTokenString("MD5Version");
	const int versionNumber = md5File.scanInt();

	if (versionNumber != Md5Version)
	{
		return md5File.error("Invalid MD5Version: " + core::toString(versionNumber) +
		                     "! Should be version " + core::toString(Md5Version));
	}

	// Skip the renderbump command line.
	md5File.expectTokenString("commandline");
	md5File.readNextToken(token);

	// numJoints and numMeshes follow:
	md5File.expectTokenString("numJoints");
	const int jointCount = md5File.scanInt();

	// A mesh will be the same as a Model3dSurface once converted.
	md5File.expectTokenString("numMeshes");
	const int meshCount = md5File.scanInt();

	if (jointCount <= 0)
	{
		md5File.warning("Bad \'numJoints\': " + core::toString(jointCount));
	}
	if (meshCount <= 0)
	{
		md5File.warning("Bad \'numMeshes\': " + core::toString(meshCount));
	}

	//
	// Joints braced section:
	//
	md5File.expectTokenString("joints");
	if (!md5File.expectTokenString("{")) { return false; }

	if (!parseMd5Joints(md5File, jointCount))
	{
		return md5File.error("Failed to parse MD5 \'joints\' section!");
	}

	if (!md5File.expectTokenString("}")) { return false; }

	//
	// Meshes follow:
	//
	for (int m = 0; m < meshCount; ++m)
	{
		md5File.expectTokenString("mesh");
		if (!md5File.expectTokenString("{")) { return false; }

		DoomMd5Mesh * mesh = new DoomMd5Mesh();
		if (!parseMd5Mesh(md5File, m, jointCount, *mesh))
		{
			delete mesh;
			return false;
		}
		md5Meshes.pushBack(mesh);

		if (!md5File.expectTokenString("}")) { return false; }
	}

	R_LOG_INFO("Finished DOOM3 MD5 import.");
	return true;
}

// ========================================================
// DoomMd5Mesh::parseMd5Joints():
// ========================================================

bool DoomMd5ModelImporter::parseMd5Joints(core::Lexer & md5File, const int jointCount)
{
	md5Joints.allocateExact(jointCount);
	for (int j = 0; j < jointCount; ++j)
	{
		DoomMd5Joint joint;

		// Joint name string:
		core::Token token;
		md5File.readNextToken(token);
		token.transferTo(joint.name);

		// Joint parent index (-1 for no parent):
		const int parentIndex = md5File.scanInt();
		if (parentIndex >= jointCount)
		{
			return md5File.error("MD5 joint with invalid parent index for \'" + joint.name + "\'!");
		}
		joint.parent = parentIndex;

		// Default pose quaternion and translation:
		md5File.scanFloatMatrix1(3, joint.pos.getData());
		md5File.scanFloatMatrix1(3, joint.orient.getData());
		joint.orient.w = joint.orient.computeW();

		md5Joints.pushBack(joint);
	}

	R_LOG_INFO("Parsed " << md5Joints.getSize() << " MD5 joints from model.");
	return true;
}

// ========================================================
// DoomMd5ModelImporter::parseMd5Mesh():
// ========================================================

bool DoomMd5ModelImporter::parseMd5Mesh(core::Lexer & md5File, const int meshIndex,
                                         const int jointCount, DoomMd5Mesh & mesh)
{
	core::Token token;

	// Parse name (optional):
	if (md5File.checkTokenString("name"))
	{
		md5File.readNextToken(token);
		token.transferTo(mesh.name);
	}

	// Parse "shader", AKA the material:
	md5File.expectTokenString("shader");
	md5File.readNextToken(token);
	token.transferTo(mesh.material);

	// Provide a default name if the file didn't have one.
	// (use the material + index, strip path)
	if (mesh.name.isEmpty())
	{
		mesh.name = core::stripPath(mesh.material);
		mesh.name += "-mesh-" + core::toString(meshIndex);
	}

	//
	// Read-in the mesh vertexes:
	//

	if (!md5File.expectTokenString("numverts"))
	{
		return md5File.error("Missing \'numverts\' in MD5 mesh!");
	}

	const int vertexCount = md5File.scanInt();
	if (vertexCount <= 0)
	{
		return md5File.error("Bad \'numverts\': " + core::toString(vertexCount));
	}

	int maxWeightIndex   = 0;
	int totalWeightCount = 0;
	mesh.verts.allocateExact(vertexCount);
	for (int v = 0; v < vertexCount; ++v)
	{
		DoomMd5Vertex vert;

		// Skip the number that immediately follows the "vert" decl.
		// No idea why it was added, since it's just a sequential index...
		md5File.expectTokenString("vert");
		/* int unused = */ md5File.scanInt();

		// Texture coordinate:
		md5File.scanFloatMatrix1(2, vert.texCoords.getData());

		// Weight indexes for the vertex:
		vert.firstWeight = md5File.scanInt();
		vert.weightCount = md5File.scanInt();

		if (vert.weightCount <= 0)
		{
			md5File.warning("Vertex without any joint weights! #" + core::toString(v));
		}

		totalWeightCount += vert.weightCount;
		if ((vert.firstWeight + vert.weightCount) > maxWeightIndex)
		{
			maxWeightIndex = vert.firstWeight + vert.weightCount;
		}

		mesh.verts.pushBack(vert);
	}

	//
	// Read the triangle indexes:
	//

	if (!md5File.expectTokenString("numtris"))
	{
		return md5File.error("Missing \'numtris\' in MD5 mesh!");
	}

	const int triCount = md5File.scanInt();
	if (triCount <= 0)
	{
		return md5File.error("Bad \'numtris\': " + core::toString(triCount));
	}

	mesh.tris.allocateExact(triCount);
	for (int t = 0; t < triCount; ++t)
	{
		DoomMd5Triangle tri;

		// Once again, the first number is just a 0-based index.
		md5File.expectTokenString("tri");
		/* int unused = */ md5File.scanInt();

		// Followed by the actual vertex indexes for this triangle.
		tri.index[0] = md5File.scanInt();
		tri.index[1] = md5File.scanInt();
		tri.index[2] = md5File.scanInt();

		mesh.tris.pushBack(tri);
	}

	//
	// Finally, read the vertex weights,
	// which incorporate the vertex displacement:
	//

	if (!md5File.expectTokenString("numweights"))
	{
		return md5File.error("Missing \'numweights\' in MD5 mesh!");
	}

	const int weightCount = md5File.scanInt();
	if (weightCount <= 0)
	{
		return md5File.error("Bad \'numweights\': " + core::toString(weightCount));
	}

	// We can also validate the range now.
	if (maxWeightIndex > weightCount)
	{
		md5File.warning("MD5 vert reference out of range! (" + core::toString(maxWeightIndex) +
		                " of " + core::toString(weightCount) + " weights).");
	}

	mesh.weights.allocateExact(weightCount);
	for (int w = 0; w < weightCount; ++w)
	{
		DoomMd5Weight weight;

		md5File.expectTokenString("weight");
		/* int unused = */ md5File.scanInt();

		int jointIndex = md5File.scanInt();
		if (jointIndex < 0 || jointIndex >= jointCount)
		{
			md5File.warning("MD5 joint Index out of range! " + core::toString(jointCount) + ": " + core::toString(jointIndex));
			jointIndex = 0;
		}

		weight.joint = jointIndex;
		weight.bias  = md5File.scanFloat();
		md5File.scanFloatMatrix1(3, weight.pos.getData());

		mesh.weights.pushBack(weight);
	}

	R_LOG_INFO("Parsed MD5 mesh \"" << mesh.name << "\": " << mesh.verts.getSize() << " verts, "
			<< mesh.tris.getSize() << " tris, " << mesh.weights.getSize() << " vertex weights.");

	return true;
}

} // namespace render {}
} // namespace atlas {}
