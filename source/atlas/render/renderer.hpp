
// ================================================================================================
// -*- C++ -*-
// File: renderer.hpp
// Author: Guilherme R. Lampert
// Created on: 02/06/15
// Brief: Exposes the proper RhiManager implementation based on compile-time configurations.
// ================================================================================================

#ifndef ATLAS_RENDER_RENDERER_HPP
#define ATLAS_RENDER_RENDERER_HPP

//
// This is the main public header for the Renderer module.
// Including it is enough to make all the public renderer
// interface visible to the users.
//

#if ATLAS_RHI_BACKEND_OPENGL
	#include "atlas/render/opengl/rhi_manager.hpp"
#else
	#error "Define a RHI back-end implementation for the current platform!"
#endif // RHI_BACKEND_XYZ

#endif // ATLAS_RENDER_RENDERER_HPP
