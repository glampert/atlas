
------------------------------------------------------
-- Global projects flags / defines:
------------------------------------------------------

-- Project-wide compiler flags for all builds:
local ATLAS_BUILD_OPTS = {
	-- Misc lang flags:
	"-std=c++11",
	"-fno-exceptions",
	"-fno-rtti",
	"-fstrict-aliasing",
	-- Warnings (GCC and Clang compatible):
	"-Wall",
	"-Wextra",
	"-Weffc++",
	"-Winit-self",
	"-Wformat=2",
	"-Wstrict-aliasing",
	"-Wuninitialized",
	"-Wunused",
	"-Wswitch",
	"-Wswitch-default",
	"-Wpointer-arith",
	"-Wwrite-strings",
	"-Wmissing-braces",
	"-Wparentheses",
	"-Wsequence-point",
	"-Wreturn-type",
	"-Wunknown-pragmas",
	"-Wshadow",
	"-Wdisabled-optimization",
	-- Extended warnings (Clang specific):
	"-Wgcc-compat",
	"-Wheader-guard",
	"-Waddress-of-array-temporary",
	"-Wglobal-constructors",
	"-Wexit-time-destructors",
	"-Wheader-hygiene",
	"-Woverloaded-virtual",
	"-Wself-assign",
	"-Wweak-vtables",
	"-Wweak-template-vtables",
	"-Wshorten-64-to-32"
}

-- Project-wide Debug build switches:
local ATLAS_DEBUG_DEFS = {
	"ATLAS_DEBUG=1",    -- Our own debug flag
	"DEBUG", "_DEBUG",  -- Enables assert()
	"_GLIBCXX_DEBUG",   -- GCC std lib debugging
	"_LIBCPP_DEBUG=0",  -- For Clang (libc++)
	"_LIBCPP_DEBUG2=0", -- Clang; See: http://stackoverflow.com/a/21847033/1198654
	"_SECURE_SCL"       -- For VS to enable STL bounds checking
}

-- Project-wide Release build switches:
local ATLAS_RELEASE_DEFS = {
	"ATLAS_RELEASE=1",
	"NDEBUG"
}

-- Project-wide configuration switches for all builds:
local ATLAS_COMMON_DEFS = {
	-- Compiler-related switches:
	"ATLAS_COMPILER_HAS_CPP11=1",
	"ATLAS_COMPILER_IS_GNUC_OR_COMPATIBLE=1",
	"ATLAS_COMPILER_GCC_PRAGMA_DIAGNOSTIC=1",

	-- Log output settings:
	"ATLAS_DEFAULT_LOG_STDOUT=1",
	"ATLAS_ENABLE_ANSI_COLOR_CODES=1",
	"ATLAS_FILTER_COLOR_CODES_FROM_LOG_OUTPUT=1",

	-- Misc switches:
	"ATLAS_SHELL_LIB_BACKEND_SDL=1",
	"ATLAS_RHI_BACKEND_OPENGL=1",
	"ATLAS_RHI_DRAW_INDEX_USE_32BITS=1",
	"ATLAS_DOOM3_MAP_IMPORT_TEXT_COORD_FIX=0",
	"ATLAS_GLSL_PREPROC_SKIP_LEADING_BLANKS_AND_COMMENTS=1"
}

-- System specific libraries:
local ATLAS_SYSTEM_LIBS = {
	-- Mac OSX frameworks:
	"OpenGL.framework",
	"Cocoa.framework",
	"IOKit.framework",
	"CoreServices.framework",
	"CoreFoundation.framework",
	"QuartzCore.framework",
	"Carbon.framework",        -- Needed by SDL
	"ForceFeedback.framework"  -- Needed by SDL
}

-- Target names:
local LIB_CORE_NAME   = "AtlasCore"
local LIB_SHELL_NAME  = "AtlasShell"
local LIB_RENDER_NAME = "AtlasRender"
local LIB_SDL_NAME    = "SDL2Static"

------------------------------------------------------
-- Common configurations for all projects:
------------------------------------------------------

workspace "Atlas"
	configurations { "Debug", "Release" }
	buildoptions   { ATLAS_BUILD_OPTS   }
	defines        { ATLAS_COMMON_DEFS  }
	language       "C++"
	location       "build"
	targetdir      "build"
	libdirs        "build"
	includedirs    "source"

	filter "configurations:Debug"
		optimize "Off"
		flags    "Symbols"
		defines  { ATLAS_DEBUG_DEFS }

	filter "configurations:Release"
		optimize "On"
		defines  { ATLAS_RELEASE_DEFS }

------------------------------------------------------
-- Core static library:
------------------------------------------------------

project (LIB_CORE_NAME)
	kind        "StaticLib"
	files       { "source/atlas/core/**.hpp", "source/atlas/core/**.cpp" }
	removefiles { "source/atlas/core/tests/**.cpp" } -- Don't build the unit tests with the library.

-- Core library unit test suite:
project (LIB_CORE_NAME .. "_Tests")
	kind  "ConsoleApp"
	files { "source/atlas/core/tests/**.cpp" }
	links { LIB_CORE_NAME }

------------------------------------------------------
-- Shell static library:
------------------------------------------------------

project (LIB_SHELL_NAME)
	kind        "StaticLib"
	files       { "source/atlas/shell/**.hpp", "source/atlas/shell/**.cpp" }
	removefiles { "source/atlas/shell/tests/**.cpp" } -- Don't build the unit tests with the library.

-- Shell library unit test suite:
project (LIB_SHELL_NAME .. "_Tests")
	kind  "ConsoleApp"
	files { "source/atlas/shell/tests/**.cpp" }
	links { ATLAS_SYSTEM_LIBS, LIB_SDL_NAME, LIB_CORE_NAME, LIB_SHELL_NAME }

------------------------------------------------------
-- Render static library:
------------------------------------------------------

project (LIB_RENDER_NAME)
	kind        "StaticLib"
	includedirs { "source/thirdparty/gl3w/include/" } -- For GL3W
	files {
		"source/atlas/render/**.hpp",
		"source/atlas/render/**.cpp",
		"source/thirdparty/gl3w/src/gl3w.cpp" -- GL3W compiles with Lib Render.
	}

